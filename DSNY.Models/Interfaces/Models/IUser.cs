﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Profile;

namespace DSNY.Core.Interfaces
{
    public interface IUser
    {
        Guid userId { get; set; }

        [Required(ErrorMessage = "Please provide a user name")]
        [DisplayName("User name")]
        string userName { get; set; }

        [DisplayName("Description")]
        string description { get; set; }

        [DisplayName("Address")]
        string address { get; set; }

        [DisplayName("Borough")]
        string borough { get; set; }

        [DisplayName("PhoneNumber")]
        string phoneNumber { get; set; }

        [DisplayName("PO Display Name")]
        string poDisplayName { get; set; }

        [DisplayName("District")]
        string district { get; set; }

        [DisplayName("Email")]
        string email { get; set; }

        [DisplayName("Roles")]
        List<IRole> roles { get; set; }

        [DisplayName("Active")]
        bool isActive { get; set; }

        [DisplayName("Locked")]
        bool isLocked { get; set; }

        [DisplayName("Is Distribution")]
        Boolean isDistribution { get; set; }

        [DisplayName("Email Messages")]
        Boolean isEmail { get; set; }

        [DisplayName("Last Login")]
        [DataType(DataType.DateTime)]
        DateTime lastLogin { get; set; }

        string fullName { get; set; }
    }
}
