﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Profile;

namespace DSNY.Core.Interfaces
{
    public interface IDSNYException
    {
        Guid userId { get; set; }

        [Required(ErrorMessage = "Please provide an exception")]
        [DisplayName("Exception")]
        string exception { get; set; }

        [DisplayName("Date")]
        DateTime date { get; set; }

        int categoryId { get; set; }

        [DisplayName("Category")]
        string categoryName { get; set; }
    }
}
