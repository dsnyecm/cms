﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DSNY.Data;
using DSNY.Common;

namespace DSNY.Core.Interfaces
{
    /// <summary>
    /// Interface defining all variables and methods required to implement an instance of a vender repository
    /// </summary>
    public interface IVendorRepository
    {
        List<Vendor> getAllVendors(bool includeInactive);
        List<Vendor> getStcVendors();
        List<Vendor> getStcVendors(bool includeInactive);
        List<Product_Vendor> getAllProductVendors(int ProductId, bool includeInactive);
        List<Vendor> getSortedVendors(string field, Enums.SortDirection direction, bool includeInactive);
        Vendor getVendor(int id);

        void addVendor(Vendor newVendor);

        void updateVendor(int vendorID, Vendor updateVendor);

        void deleteVendor(int vendorID);
    }
}
