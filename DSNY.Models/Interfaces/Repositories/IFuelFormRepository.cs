﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DSNY.Data;

namespace DSNY.Core.Interfaces
{
    /// <summary>
    /// Interface defining all variables and methods required to implement of a fuel form repository
    /// </summary>
    public interface IFuelFormRepository
    {
        Fuel_Form getFuelForm(int fuelformId);
        Fuel_Form getFuelForm(Guid userId, DateTime submissionDate);
        Fuel_Form getLastFuelForm(Guid userId, int? fuelFormId, int take);
        List<Fuel_Form_Details> getFuelFormDetails(int fuelFormId);
        List<Fuel_Form_Details_Delivery> getFuelFormDetailsDelivery(int fuelformDetailsId);
        List<Fuel_Form_Equipment_Failure> getFuelFormEquipFailure(int fuelFormId);

        void addFuelForm(Fuel_Form newFuelForm);
        void addFuelFormDetails(Fuel_Form_Details newFuelFormDetails);
        void addFuelFormDetailsDelivery(Fuel_Form_Details_Delivery newFuelFormDetailsDelivery);
        void addFuelFormEquipFailure(Fuel_Form_Equipment_Failure newFuelFormEquipFailure);

        void deleteFuelFormDetailsDelivery(Fuel_Form_Details_Delivery deleteFuelFormDetailsDelivery);
        void deleteFuelFormEquipFailure(Fuel_Form_Equipment_Failure deleteFuelFormEquipFailure);

        void updateFuelForm(int fuelFormId, Fuel_Form updateFuelForm, Guid userId);
        void updateFuelFormDetails(int fuelFormDetailsId, Fuel_Form_Details updateFuelFormDetails, Guid userId, ref int count);
        bool updateFuelFormDetailsDelivery(int fuelFormDetailsId, int orderNo, Fuel_Form_Details_Delivery updateFuelFormDetailsDelivery, Guid userId);
        bool updateFuelFormEquipFailure(int fuelFormEquipFailureId, Fuel_Form_Equipment_Failure updateFuelFormEquipFailure, Guid userId);
    }
}