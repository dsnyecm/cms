﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.Common;
using DSNY.Data;

namespace DSNY.Core.Interfaces
{
    /// <summary>
    /// Interface defining all variables and methods required to implement an instance of a role repository
    /// </summary>
    public interface IDSNYExceptionRepository
    {
        DSNYException getDSNYException(int DSNYExceptionId);
        List<DSNYException> getDSNYExceptions(int[] DSNYExceptionIds);
        DSNYExceptionCategory getDSNYExceptionCategory(int DSNYExceptionCategoryId);
        DSNYExceptionCategory getDSNYExceptionCategory(string DSNYExceptionCode);
        List<DSNYExceptionCategory> getAllDSNYExceptionCategories();
        List<DSNYException> getTableExceptions(string tableName, List<int> ids);
        int addException(DSNYException ex);
        bool addExceptions(List<DSNYException> exs);
        List<DSNYException> convertExceptions(List<DSNY.Core.Models.DSNYException> exs, Guid userId, string tableName);
    }
}
