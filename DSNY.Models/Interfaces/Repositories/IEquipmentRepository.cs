﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DSNY.Data;
using DSNY.Common;

namespace DSNY.Core.Interfaces
{
    /// <summary>
    /// Interface defining all variables and methods required to implement an instance of an equipment repository
    /// </summary>
    public interface IEquipmentRepository
    {
        List<Equipment> getAllEquipment(bool includeInactive);
        Equipment getEquipment(int equipmentId);
        List<Equipment_User> getEquipmentForUser(Guid userId, bool includeInactive, bool includeWater);
        List<Equipment> getSortedEquipment(string field, Enums.SortDirection direction, bool includeInactive);
        
        void addEquipment(Equipment newEquipment);
        void addUserToEquipment(int equipmentId, int productId, string description, int capacity, bool active, Guid userId);
        
        void updateEquipment(int equipmentId, Equipment updateEquipment);
        void updateEquipmentUser(int equipmentUserId, int equipmentId, int productId, string description, int capacity, bool active);

        void deleteEquipment(int equipmentId);
        void deleteAllEquipmentForUser(Guid userId);
        void deleteEquipmentUser(int equipmentUserId);
    }
}
