﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Web.Profile;

using DSNY.Common;

namespace DSNY.Core.Interfaces
{
    /// <summary>
    /// Interface defining all variables and methods required to implement an instance of a user repository
    /// </summary>
    public interface IUserRepository
    {
        IUser getUser(string username);
        IUser getUser(Guid userId);

        List<IUser> getAllUsers(bool light);
        List<IUser> getStcUsers(Guid currentUserId);
        List<IUser> getStcBoroughUserBySiteUserId(Guid currentUserId);
        List<IUser> getStcUsersWithActiveSiteOnHand();
        List<IUser> getStcUsersWithActiveSiteOnHand(int productId);
        List<IUser> getStcUsersWithActiveSiteOnHand(Guid currentUserId);
        List<IUser> getStcUsersWithActiveSiteOnHand(Guid currentUserId, int productId);
        List<IUser> getStcUsersWithActiveSiteOnHand(Guid currentUserId, int productId, int zoneId);
        List<IUser> getAllEmailAddressUsers();
        List<IUser> getAllMessageDistribUsers();
        List<IUser> getAllEmailDistribUsers();
        List<IUser> getRoleUsers(string roleName);
        List<IUser> getRolesUsers(List<string> roleName);
        List<IUser> getUserCommandChain(Guid userId);
        List<IUser> getReportToUsers(Guid userId);
        List<IUser> getMissingFuelForms(Guid userId);
        List<IUser> getSortedUsers(string field, Enums.SortDirection direction);

        bool IsUserInRole(Guid userId, string roleName);

        MembershipCreateStatus addUser(string userName, string password, string description, string address, string phoneNumber, string borough, string poDisplayName, string district, string email, List<IRole> roles, bool isDistribution, bool isEmail, bool isActive);

        void addUserToRole(string userName, string roleName);
        void addUserToCommandChain(Guid userId, Guid reportToId);

        bool updateUser(IUser user);
        void updateMessageDistForUser(Guid userId, bool inDist);
        void updateEmailDistForUser(Guid userId, bool inDist);
        void updatePassword(IUser user, string newPassword);

        bool deleteUser(string userName);
        bool deleteUser(IUser user);
        void deleteUserFromRole(string userName, string roleName);
        void deleteUserFromCommandChain(Guid userId);
        void deleteUserFromEquipment(Guid userId);
        void deleteUserFromProducts(Guid userId);
        //void importUsers();
    }
}
