﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DSNY.Data;
using DSNY.Common;

namespace DSNY.Core.Interfaces
{
    /// <summary>
    /// Interface defining all variables and methods required to implement an instance of a role repository
    /// </summary>
    public interface IPurchaseOrderRepository
    {
        List<DSNY.Data.GetPurchaseOrderItems_Result> getPurchaseOrder();

        Purchase_Orders getLastPurchaseOrder();

        void createPurchaseOrder(List<Purchase_Orders> pos);
    }
}
