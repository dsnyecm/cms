﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.Data;

namespace DSNY.Repository.Interfaces.Repositories
{
    public interface IRadioRepository
    {
        List<Radio> getAllRadio(bool includeInactive);
        Radio getRadio(int radioId);
        void addRadio(Radio radio);
        void deleteRadio(int radioId);
        void updateRadio(int radioId, Radio radio);
        List<Radio_Equipment_Type> GetEquipmentTypes();
        List<Radio_Repair_Problem_Type> GetRadioRepairCodes();
        List<Radio_Equipment_Status> GetRadioEquipmentStatus();
        List<Radio_Make> GetRadioMakes();
        List<Radio_Model> GetRadioModelsByMakeId(int makeId, bool includeInactive);
        List<Radio_History> GetRadioHistory(int radioId);
        List<Radio_Accessory> GetAllRadioAccessories();
    }
}
