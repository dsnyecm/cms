﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DSNY.Data;
using DSNY.Common;

namespace DSNY.Core.Interfaces
{
    /// <summary>
    /// Interface defining all variables and methods required to implement an instance of a product repository
    /// </summary>
    public interface IProductRepository
    {
        List<Product> getAllProducts(bool includeInactive, bool includeWater);
        List<Product> getSortedProducts(string field, Enums.SortDirection direction, bool includeInactive);
        Product getProduct(int productId);
        List<Product_User> getUsersOfProduct(int productId);
        List<Product_User> getProductsForUser(Guid userId, bool includeWater);
        List<Product_User> getProductsForUsers(bool includeWater);
        List<Product> getProductsVendorsForUser(Guid userId, bool includeWater);
        List<aspnet_Users> getUsersOfProducts();
        List<Product_Sub_Type> getProductSubTypes();

        int addProduct(string productName, bool isSubType, bool isActive, bool isDrums, bool isReadingRequired, int defaultCapacity, string measurementType);
        int addProduct(Product newProduct);
        void addUserToProduct(int productId, Guid userId, int capacity, int? productSubTypeId);

        bool updateProduct(int productId, Product updateProduct, bool removeUsers);
        bool updateProductUserCapacity(int productId, Guid userId, int capacity);
        bool updateProductUserSubType(int productId, Guid userId, int? productSubTypeId);
        bool updateProductUserVendor(int productId, Guid userId, int? vendorId);

        void deleteProduct(int productId);
        void deleteAllProductsForUser(Guid userId);
    }
}
