﻿using System.Collections.Generic;

using DSNY.Common;
using DSNY.Data;

namespace DSNY.Core.Interfaces
{
    /// <summary>
    /// Interface defining all variables and methods required to implement an instance of a role repository
    /// </summary>
    public interface IRoleRepository
    {
        string[] getAllRoles();
        List<IRole> getAllRolesList();
        List<aspnet_Roles> getAllAspNetRoles();
        Dictionary<string, string[]> getAllRolesWithUsers();
        List<IRole> getSortedRoles(string field, Enums.SortDirection direction);
        List<IRole> getRolesForUser(string userName);

        bool addRole(string roleName);
        void addUsersToRoles(string[] users, string[] roles);

        bool deleteRole(string roleName);
        void deleteUsersFromRoles(string[] users, string[] roles);
    }
}
