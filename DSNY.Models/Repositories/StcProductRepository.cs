﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.Data;

namespace DSNY.Core.Repository
{
    public class StcProductRepository
    {
        private DSNYContext _context;

        public StcProductRepository(DSNYContext context)
        {
            _context = context;
        }

        public List<STC_Product> GetAll()
        {
            return _context.DataContext.STC_Product.Where(x => x.is_Active == true).ToList();
        }

        public List<STC_Product> GetByUserIds(List<Guid> userIds)
        {
            return _context.DataContext.STC_Product.Where(x => x.is_Active == true && x.STC_Product_User.Where(y => userIds.Contains(y.STC_User_ID)).Any()).ToList();
        }

        public STC_Product GetByProductName(string name)
        {
            return _context.DataContext.STC_Product.Where(x => x.is_Active == true && x.STC_Product_Name == name).FirstOrDefault();
        }

        public STC_Product GetById(int id)
        {
            return _context.DataContext.STC_Product.Where(x => x.is_Active == true && x.STC_Product_id == id).FirstOrDefault();
        }
    }
}
