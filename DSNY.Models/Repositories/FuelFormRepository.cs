﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Data;
using DSNY.Core.Interfaces;
using DSNY.Common;

namespace DSNY.Core.Repository
{
    /// <summary>
    /// An implementation of the <see cref="IFuelFormRepository"/> interface
    /// </summary>
    public class FuelFormRepository : IFuelFormRepository
    {
        #region Variables
        
        private readonly ILogger _logger = null;
        private readonly IExceptionHandler _exceptionHandler = null;
        private DSNYContext _dataProvider = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FuelFormRepository"/> class.
        /// </summary>
        public FuelFormRepository() : this(null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FuelFormRepository"/> class.
        /// </summary>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public FuelFormRepository(DSNYContext IoCDataProvider, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _dataProvider = IoCDataProvider;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region Get

        /// <summary>
        /// Gets the fuel form.
        /// </summary>
        /// <param name="fuelformId">The fuelform id.</param>
        /// <returns></returns>
        public Fuel_Form getFuelForm(int fuelformId)
        {
            try
            {
                return _dataProvider.DataContext.Fuel_Form.SingleOrDefault(ff => ff.Fuel_Form_ID == fuelformId);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
            
        }

        /// <summary>
        /// Gets the fuel form.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="submissionDate">The submission date.</param>
        /// <returns></returns>
        public Fuel_Form getFuelForm(Guid userId, DateTime submissionDate)
        {
            try
            {
                List<Fuel_Form> forms = _dataProvider.DataContext.Fuel_Form.Where(ff => ff.Submission_Date.Value.Year == submissionDate.Year && ff.Submission_Date.Value.Month == submissionDate.Month &&
                                            ff.Submission_Date.Value.Day == submissionDate.Day && ff.UserId == userId).ToList();

                if (forms.Count > 0)
                    return forms[forms.Count - 1];
                else
                    return null;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Gets last completed fuel form.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="submissionDate">The submission date.</param>
        /// <returns></returns>
        public Fuel_Form getLastFuelForm(Guid userId, int? fuelFormId, int take = 1)
        {
            try
            {
                List<Fuel_Form> forms;

                if (fuelFormId == null)
                {
                    forms = _dataProvider.DataContext.Fuel_Form.Where(ff => ff.UserId == userId).OrderByDescending(ff => ff.Submission_Date).Take(take).ToList();
                }
                else
                {
                    Fuel_Form tempFuelForm = _dataProvider.DataContext.Fuel_Form.SingleOrDefault(ff => ff.Fuel_Form_ID == fuelFormId);
                    forms = _dataProvider.DataContext.Fuel_Form.Where(ff => ff.UserId == userId && ff.Submission_Date < tempFuelForm.Submission_Date).OrderByDescending(ff => ff.Submission_Date).Take(take).ToList();
                }

                if (forms.Count > 0)
                {
                    List<Fuel_Form_Equipment_Failure> previousFuelForms = forms[forms.Count - 1].Fuel_Form_Equipment_Failure.ToList();
                    return forms[forms.Count - 1];
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Gets the fuel form details.
        /// </summary>
        /// <param name="fuelFormId">The fuel form id.</param>
        /// <returns></returns>
        public List<Fuel_Form_Details> getFuelFormDetails(int fuelFormId)
        {
            try
            {
                return _dataProvider.DataContext.Fuel_Form_Details.Where(ffd => ffd.Fuel_Form_ID == fuelFormId)
                    .OrderBy(ffd => ffd.Product.Order_Num == null)
                    .ThenBy(ffd => ffd.Product.Order_Num).ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Gets the fuel form details delivery.
        /// </summary>
        /// <param name="fuelformDetailsId">The fuelform details id.</param>
        /// <returns></returns>
        public List<Fuel_Form_Details_Delivery> getFuelFormDetailsDelivery(int fuelformDetailsId)
        {
            try
            {
                return _dataProvider.DataContext.Fuel_Form_Details_Delivery.Where(ffd => ffd.Fuel_Form_Details_Id == fuelformDetailsId)
                    .OrderByDescending(ffd => ffd.Order_No).ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Gets the fuel form equip failure.
        /// </summary>
        /// <param name="fuelFormId">The fuel form id.</param>
        /// <returns></returns>
        public List<Fuel_Form_Equipment_Failure> getFuelFormEquipFailure(int fuelFormId)
        {
            try
            {
                return _dataProvider.DataContext.Fuel_Form_Equipment_Failure.Where(ffd => ffd.Fuel_Form_ID == fuelFormId)
                    .OrderBy(ffd => ffd.Equipment_User.Product.Order_Num == null)
                    .ThenBy(ffd => ffd.Equipment_User.Product.Order_Num).ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        #endregion

        #region Add

        /// <summary>
        /// Adds a fuel form.
        /// </summary>
        /// <param name="newFuelForm">The new fuel form.</param>
        public void addFuelForm(Fuel_Form newFuelForm)
        {
            try
            {
                _dataProvider.DataContext.Fuel_Form.AddObject(newFuelForm);
                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Adds the fuel form details.
        /// </summary>
        /// <param name="newFuelFormDetails">The new fuel form details.</param>
        public void addFuelFormDetails(Fuel_Form_Details newFuelFormDetails)
        {
            try
            {
                _dataProvider.DataContext.Fuel_Form_Details.AddObject(newFuelFormDetails);
                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Adds the fuel form details delivery.
        /// </summary>
        /// <param name="newFuelFormDetailsDelivery">The new fuel form details delivery.</param>
        public void addFuelFormDetailsDelivery(Fuel_Form_Details_Delivery newFuelFormDetailsDelivery)
        {
            try
            {
                _dataProvider.DataContext.Fuel_Form_Details_Delivery.AddObject(newFuelFormDetailsDelivery);
                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Adds the fuel form equip failure.
        /// </summary>
        /// <param name="newFuelFormEquipFailure">The new fuel form equip failure.</param>
        public void addFuelFormEquipFailure(Fuel_Form_Equipment_Failure newFuelFormEquipFailure)
        {
            try
            {
                _dataProvider.DataContext.Fuel_Form_Equipment_Failure.AddObject(newFuelFormEquipFailure);
                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates a fuel form.
        /// </summary>
        /// <param name="vendorID">The vendor ID.</param>
        /// <param name="updateVendor">The update vendor.</param>
        public void updateFuelForm(int fuelFormId, Fuel_Form updateFuelForm, Guid userId)
        {
            try
            {
                Fuel_Form foundFuelForm = _dataProvider.DataContext.Fuel_Form.SingleOrDefault(ff => ff.Fuel_Form_ID == fuelFormId);

                if (foundFuelForm != null)
                {
                    foundFuelForm.Submission_Date = updateFuelForm.Submission_Date;
                    foundFuelForm.Remarks = updateFuelForm.Remarks;
                    foundFuelForm.Modified_By = userId;

                    // add new fuel form equipment failures
                    updateFuelForm.Fuel_Form_Equipment_Failure.Where(f => f.Equipment_Failure_ID == 0).ToList().ForEach(f =>
                    {
                        Fuel_Form_Equipment_Failure newEquipFail = (Fuel_Form_Equipment_Failure) f;
                        foundFuelForm.Fuel_Form_Equipment_Failure.Add(newEquipFail);
                        _dataProvider.DataContext.Fuel_Form_Equipment_Failure.AddObject(newEquipFail);
                    });

                    // update fuel form equipment failures
                    updateFuelForm.Fuel_Form_Equipment_Failure.Where(f => f.Equipment_Failure_ID != 0).ToList().ForEach(f =>
                    {
                        Fuel_Form_Equipment_Failure dbEquipFailure = foundFuelForm.Fuel_Form_Equipment_Failure.FirstOrDefault(e => e.Equipment_Failure_ID == f.Equipment_Failure_ID);

                        if (dbEquipFailure != null)
                        {
                            dbEquipFailure.is_Equipment_Failure = f.is_Equipment_Failure;
                            dbEquipFailure.Failure_Date = f.Failure_Date;
                            dbEquipFailure.Fix_Date = f.Fix_Date;
                            dbEquipFailure.Remarks = f.Remarks;
                            dbEquipFailure.Modified_By = userId;
                        }
                    });

                    // find which ids aren't in the form and delete them from DB
                    List<int> formsEquipFailIds = updateFuelForm.Fuel_Form_Equipment_Failure.Select(m => m.Equipment_Failure_ID).ToList();

                    foundFuelForm.Fuel_Form_Equipment_Failure.Where(m => !formsEquipFailIds.Contains(m.Equipment_Failure_ID)).ToList().ForEach(m =>
                     {
                         if (m.Equipment_Failure_ID > 0)
                         {
                             foundFuelForm.Fuel_Form_Equipment_Failure.Remove(m);
                             _dataProvider.DataContext.Fuel_Form_Equipment_Failure.DeleteObject(m);
                         }
                     });

                    _dataProvider.DataContext.SaveChanges();

                    // add fuel form deliveries
                    int i = 0;
                    foreach (var d in updateFuelForm.Fuel_Form_Details)
                    {
                        updateFuelFormDetails(d.Fuel_Form_Details_Id, d, userId, ref i);
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Updates the fuel form details.
        /// </summary>
        /// <param name="vendorID">The vendor ID.</param>
        /// <param name="updateVendor">The update vendor.</param>
        public void updateFuelFormDetails(int fuelFormDetailsId, Fuel_Form_Details updateFuelFormDetails, Guid userId, ref int count)
        {
            try
            {
                Fuel_Form_Details foundFuelFormDetails = _dataProvider.DataContext.Fuel_Form_Details.SingleOrDefault(ffd => ffd.Fuel_Form_Details_Id == fuelFormDetailsId);

                if (foundFuelFormDetails != null)
                {
                    foundFuelFormDetails.On_Hand = updateFuelFormDetails.On_Hand;
                    foundFuelFormDetails.Spare_Drums = updateFuelFormDetails.Spare_Drums;
                    foundFuelFormDetails.Tank_Delivered = updateFuelFormDetails.Tank_Delivered;
                    foundFuelFormDetails.Tank_Dispensed = updateFuelFormDetails.Tank_Dispensed;
                    foundFuelFormDetails.Modified_By = userId;

                    _dataProvider.DataContext.SaveChanges();

                    // delete all delivery details and recreate
                    getFuelFormDetailsDelivery(fuelFormDetailsId)
                        .ForEach(m => _dataProvider.DataContext.Fuel_Form_Details_Delivery.DeleteObject(m));

                    _dataProvider.DataContext.SaveChanges();

                    // add fuel form delivery details
                    int deliveryCount = 0;
                    foreach (var item in updateFuelFormDetails.Fuel_Form_Details_Delivery)
                    {
                        if (deliveryCount > 0)
                            count++;

                        if (!updateFuelFormDetailsDelivery(fuelFormDetailsId, count, item, userId))
                        {
                            addFuelFormDetailsDelivery(new Fuel_Form_Details_Delivery()
                            {
                                Fuel_Form_Details_Id = item.Fuel_Form_Details_Id,
                                Invoice_Number = item.Invoice_Number,
                                Quantity = item.Quantity,
                                Vendor_ID = item.Vendor_ID,
                                Recieve_Date = item.Recieve_Date,
                                Order_No = count
                            });
                        }

                        deliveryCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Updates the fuel form details delivery.
        /// </summary>
        /// <param name="vendorID">The vendor ID.</param>
        /// <param name="updateVendor">The update vendor.</param>
        public bool updateFuelFormDetailsDelivery(int fuelFormDetailsId, int orderNo, Fuel_Form_Details_Delivery updateFuelFormDetailsDelivery, Guid userId)
        {
            try
            {
                Fuel_Form_Details_Delivery foundFuelFormDetailsDelivery = _dataProvider.DataContext.Fuel_Form_Details_Delivery.SingleOrDefault(ffdd => ffdd.Fuel_Form_Details_Id == fuelFormDetailsId &&
                                                                                                                                            ffdd.Order_No == orderNo);

                if (foundFuelFormDetailsDelivery != null)
                {
                    foundFuelFormDetailsDelivery.Invoice_Number = updateFuelFormDetailsDelivery.Invoice_Number;
                    foundFuelFormDetailsDelivery.Quantity = updateFuelFormDetailsDelivery.Quantity;
                    foundFuelFormDetailsDelivery.Recieve_Date = updateFuelFormDetailsDelivery.Recieve_Date;
                    foundFuelFormDetailsDelivery.Vendor_ID = updateFuelFormDetailsDelivery.Vendor_ID;

                    _dataProvider.DataContext.SaveChanges();

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Updates the fuel form equip failure.
        /// </summary>
        /// <param name="vendorID">The vendor ID.</param>
        /// <param name="updateVendor">The update vendor.</param>
        public bool updateFuelFormEquipFailure(int fuelFormEquipFailureId, Fuel_Form_Equipment_Failure updateFuelFormEquipFailure, Guid userId)
        {
            try
            {
                Fuel_Form_Equipment_Failure foundFuelFormEquipFailure = _dataProvider.DataContext.Fuel_Form_Equipment_Failure.SingleOrDefault(ffef => ffef.Equipment_Failure_ID == fuelFormEquipFailureId);

                if (foundFuelFormEquipFailure != null)
                {
                    foundFuelFormEquipFailure.Failure_Date = updateFuelFormEquipFailure.Failure_Date;
                    foundFuelFormEquipFailure.Fix_Date = updateFuelFormEquipFailure.Fix_Date;
                    foundFuelFormEquipFailure.is_Equipment_Failure = updateFuelFormEquipFailure.is_Equipment_Failure;
                    foundFuelFormEquipFailure.Remarks = updateFuelFormEquipFailure.Remarks;
                    foundFuelFormEquipFailure.Modified_By = userId;

                    _dataProvider.DataContext.SaveChanges();

                    return true;
                } else {
                    return false;
                }

            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a fuel form details delivery
        /// </summary>
        /// <param name="fuelformDetailsId">The fuelform details id.</param>
        /// <returns></returns>
        public void deleteFuelFormDetailsDelivery(Fuel_Form_Details_Delivery deleteFuelFormDetailsDelivery)
        {
            try
            {
                _dataProvider.DataContext.Fuel_Form_Details_Delivery.DeleteObject(deleteFuelFormDetailsDelivery);
                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Deletes a fuel form equip failure
        /// </summary>
        /// <param name="fuelformDetailsId">The fuelform details id.</param>
        /// <returns></returns>
        public void deleteFuelFormEquipFailure(Fuel_Form_Equipment_Failure deleteFuelFormEquipFailure)
        {
            try
            {
                _dataProvider.DataContext.Fuel_Form_Equipment_Failure.Attach(deleteFuelFormEquipFailure);
                _dataProvider.DataContext.Fuel_Form_Equipment_Failure.DeleteObject(deleteFuelFormEquipFailure);
                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        #endregion
    }
}