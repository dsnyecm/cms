﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Data;
using DSNY.Core.Interfaces;
using DSNY.Common;

namespace DSNY.Core.Repository
{
    /// <summary>
    /// An implementation of the <see cref="IDailyReportRepository"/> interface
    /// </summary>
    public class DailyReportRepository : IDailyReportRepository
    {
        #region Variables

        private readonly ILogger _logger = null;
        private readonly IExceptionHandler _exceptionHandler = null;
        private DSNYContext _dataProvider = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyReportRepository"/> class.
        /// </summary>
        public DailyReportRepository() : this(null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyReportRepository"/> class.
        /// </summary>
        /// <param name="IoCProvider">The inversion of control DailyReport provider.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public DailyReportRepository(DSNYContext IoCDataProvider, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _dataProvider = IoCDataProvider;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region Get

        /// <summary>
        /// Gets all DailyReports list.
        /// </summary>
        /// <returns></returns>
        public DailyReport getLastestReport()
        {
            try
            {
                return _dataProvider.DataContext.DailyReports.OrderByDescending(dr => dr.DailyReportDate).First();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Gets all DailyReports list.
        /// </summary>
        /// <returns></returns>
        public DailyReport getReport(int dailyReportId)
        {
            try
            {
                return _dataProvider.DataContext.DailyReports.FirstOrDefault(dr => dr.DailyReportId == dailyReportId);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        public DailyReport findReport(DateTime date)
        {
            try
            {
                return _dataProvider.DataContext.DailyReports.FirstOrDefault(dr => dr.DailyReportDate.Year == date.Year 
                    && dr.DailyReportDate.Month == date.Month && dr.DailyReportDate.Day == date.Day);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        #endregion

        #region Add

        /// <summary>
        /// Adds a fuel form.
        /// </summary>
        /// <param name="newFuelForm">The new fuel form.</param>
        public void addDailyReport(DailyReport newDailyReport)
        {
            try
            {
                _dataProvider.DataContext.DailyReports.AddObject(newDailyReport);
                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }

        }

        #endregion

        #region Update

        /// <summary>
        /// Adds a fuel form.
        /// </summary>
        /// <param name="newFuelForm">The new fuel form.</param>
        public bool updateDailyReport(DailyReport updatedDailyReport)
        {
            try
            {
                DailyReport dailyReport = this.getReport(updatedDailyReport.DailyReportId);

                if (dailyReport != null)
                {
                    dailyReport.DailyReportFunction = updatedDailyReport.DailyReportFunction;
                    dailyReport.DailyReportWeather = updatedDailyReport.DailyReportWeather;
                    dailyReport.DailyReportTempLow = updatedDailyReport.DailyReportTempLow;
                    dailyReport.DailyReportTempHigh = updatedDailyReport.DailyReportTempHigh;
                }
                else
                {
                    return false;
                }

                _dataProvider.DataContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        #endregion
    }
}