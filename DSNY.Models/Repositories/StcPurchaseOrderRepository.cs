﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.Data;

namespace DSNY.Core.Repository
{
    public class StcPurchaseOrderRepository
    {
        private DSNYContext _context;

        public StcPurchaseOrderRepository(DSNYContext context)
        {
            _context = context;
        }

        public List<STC_Purchase_Order> GetAll()
        {
            return _context.DataContext.STC_Purchase_Order.OrderByDescending(x => x.PO_Start_Date).ToList();
        }

        public STC_Purchase_Order GetById(int id)
        {
            return _context.DataContext.STC_Purchase_Order.Where(x => x.STC_Purchase_Orders_id == id).FirstOrDefault();
        }

        public List<STC_Purchase_Order> GetActiveByProductId(int productId)
        {
            return _context.DataContext.STC_Purchase_Order.Where(x => x.STC_Product_id == productId && !x.PO_Complete_Date.HasValue).ToList();
        }

        public List<STC_Purchase_Order> GetByProductId(int productId)
        {
            return _context.DataContext.STC_Purchase_Order.Where(x => x.STC_Product_id == productId).ToList();
        }

        public List<STC_Purchase_Order> GetByProductAndUser(int productId, Guid userId)
        {
            return _context.DataContext.aspnet_Users.Where(x => x.UserId == userId)
                .SelectMany(x => x.Zones.Where(y => y.is_Active.HasValue && y.is_Active.Value).SelectMany(y => y.STC_Purchase_Order
                    .Where(z => z.STC_Product_id == productId))).Distinct().ToList();
        }

        public void SubtractOrderedAmountRemaining(int purchaseOrder, decimal amount)
        {
            var po = _context.DataContext.STC_Purchase_Order.Where(x => x.STC_Purchase_Orders_id == purchaseOrder).FirstOrDefault();
            if (po == null) throw new Exception("Purchase order does not exist.");
            po.Current_Order_Qty_Amount -= amount;
        }

        public void SubtractDeliveryAmountRemaining(int purchaseOrder, decimal amount)
        {
            var po = _context.DataContext.STC_Purchase_Order.Where(x => x.STC_Purchase_Orders_id == purchaseOrder).FirstOrDefault();
            if (po == null) throw new Exception("Purchase order does not exist.");
            po.Current_Delivery_Qty_Amount -= amount;
            if (po.Current_Delivery_Qty_Amount <= 0)
            {
                po.PO_Complete_Date = DateTime.Now;
            }
        }
    }
}
