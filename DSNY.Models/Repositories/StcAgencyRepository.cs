using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.Data;

namespace DSNY.Core.Repository
{
    public class StcAgencyRepository
    {
        private DSNYContext _context;

        public StcAgencyRepository(DSNYContext context)
        {
            _context = context;
        }

        public List<STC_Agency> GetAll()
        {
            return _context.DataContext.STC_Agency.ToList();
        }

        public STC_Agency GetById(int id)
        {
            return _context.DataContext.STC_Agency.Where(x => x.STC_Agency_id == id).FirstOrDefault();
        }

        public STC_Agency GetByName(string name)
        {
            return _context.DataContext.STC_Agency.Where(x => x.Agency_Name == name).FirstOrDefault();
        }
    }
}
