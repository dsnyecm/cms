﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DSNY.Core.Interfaces;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Data;
using DSNY.Common;

namespace DSNY.Core.Repository
{
    /// <summary>
    /// An implementation of the <see cref="IVendorRepository"/> interface
    /// </summary>
    public class VendorRepository : IVendorRepository
    {
        #region Variables

        private readonly ILogger _logger = null;
        private readonly IExceptionHandler _exceptionHandler = null;
        private DSNYContext _dataProvider = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="VendorRepository"/> class.
        /// </summary>
        public VendorRepository() : this(null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VendorRepository"/> class.
        /// </summary>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public VendorRepository(DSNYContext IoCDataProvider, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _dataProvider = IoCDataProvider;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region Get

        /// <summary>
        /// Gets all vendors.
        /// </summary>
        /// <param name="includeInactive">if set to <c>true</c> [include inactive].</param>
        /// <returns></returns>
        public List<Vendor> getAllVendors(bool includeInactive = false)
        {
            try
            {
                if (includeInactive)
                    return _dataProvider.DataContext.Vendors.OrderBy(v => v.Vendor_Name).ToList();
                else
                    return _dataProvider.DataContext.Vendors.OrderBy(v => v.Vendor_Name).Where(v => v.is_Active == true).ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);

                return new List<Vendor>();
            }
        }

        /// <summary>
        /// Gets stc vendors.
        /// </summary>
        /// <param name="includeInactive">if set to <c>true</c> [include inactive].</param>
        /// <returns></returns>
        public List<Vendor> getStcVendors(bool includeInactive)
        {
            try
            {
                if (includeInactive)
                    return _dataProvider.DataContext.Vendors.Where(v => v.is_Stc == true).OrderBy(v => v.Vendor_Name).ToList();
                else
                    return _dataProvider.DataContext.Vendors.Where(v => v.is_Stc == true && v.is_Active == true).OrderBy(v => v.Vendor_Name).ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);

                return new List<Vendor>();
            }
        }

        public List<Vendor> getStcVendors()
        {
            return getStcVendors(false);
        }

        /// <summary>
        /// Gets all vendors.
        /// </summary>
        /// <param name="includeInactive">if set to <c>true</c> [include inactive].</param>
        /// <returns></returns>
        public List<Product_Vendor> getAllProductVendors(int ProductId, bool includeInactive = false)
        {
            try
            {
                List<Product_Vendor> productVendors =_dataProvider.DataContext.Product_Vendor
                    .Where(pv => pv.Product_ID ==  ProductId)
                    .OrderBy(v => v.Vendor.Vendor_Name)
                    .ToList();

                if (!includeInactive) {
                    productVendors = productVendors.Where(v => v.Vendor.is_Active == true).ToList();
                }

                productVendors.ForEach(pv =>
                {
                    _dataProvider.DataContext.Detach(pv);
                });

                return productVendors;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);

                return new List<Product_Vendor>();
            }
        }

        /// <summary>
        /// Gets the sorted vendors.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="direction">The direction.</param>
        /// <param name="includeInactive">if set to <c>true</c> [include inactive].</param>
        /// <returns></returns>
        public List<Vendor> getSortedVendors(string field, Enums.SortDirection direction, bool includeInactive)
        {
            try
            {
                List<Vendor> unSortedVendors = getAllVendors(includeInactive);
                List<Vendor> sortedVendors;

                if (direction == Enums.SortDirection.Descending)
                {
                    sortedVendors = (from Vendor in unSortedVendors
                                     orderby orderBy(field, Vendor) descending
                                     select Vendor).ToList();
                }
                else
                {
                    sortedVendors = (from Vendor in unSortedVendors
                                     orderby orderBy(field, Vendor) ascending
                                     select Vendor).ToList();
                }

                return sortedVendors;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Gets the vendor.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public Vendor getVendor(int id)
        {
            try
            {
                return _dataProvider.DataContext.Vendors.SingleOrDefault(v => v.Vendor_ID == id);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);

                return new Vendor();
            }
        }

        #endregion

        #region Add

        /// <summary>
        /// Adds the vendor.
        /// </summary>
        /// <param name="newVendor">The new vendor.</param>
        public void addVendor(Vendor newVendor)
        {
            try
            {
                _dataProvider.DataContext.Vendors.AddObject(newVendor);

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates the vendor.
        /// </summary>
        /// <param name="vendorID">The vendor ID.</param>
        /// <param name="updateVendor">The update vendor.</param>
        public void updateVendor(int vendorID, Vendor updateVendor)
        {
            try
            {
                Vendor foundVendor = _dataProvider.DataContext.Vendors.SingleOrDefault(v => v.Vendor_ID == vendorID);

                if (foundVendor != null)
                {
                    foundVendor.Vendor_Name = updateVendor.Vendor_Name;
                    foundVendor.Account_Number = updateVendor.Account_Number;
                    foundVendor.Email_Address= updateVendor.Email_Address;
                    foundVendor.Phone_Number = updateVendor.Phone_Number;
                    foundVendor.is_Active = updateVendor.is_Active;
                }

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes the vendor.
        /// </summary>
        /// <param name="vendorID">The vendor ID.</param>
        public void deleteVendor(int vendorID)
        {
            try
            {
                Vendor deleteVendor = _dataProvider.DataContext.Vendors.SingleOrDefault(v => v.Vendor_ID == vendorID);

                if (deleteVendor != null)
                {
                    deleteVendor.is_Active = false;

                    _dataProvider.DataContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                bool rethrow = _exceptionHandler.HandleException(ex);

                if (rethrow)
                    throw;
            }
        }

        #endregion

        #region Sort

        // Our sorter object, holds the field key and object type
        private object orderBy(string sortKey, Vendor vendor)
        {
            switch (sortKey)
            {
                case "VendorName":
                    return vendor.Vendor_Name;

                case "IsActive":
                    return vendor.is_Active;

                default:
                    return vendor.Vendor_Name;
            }
        }

        #endregion
    }
}
