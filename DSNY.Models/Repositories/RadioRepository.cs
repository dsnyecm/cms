﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.Data;
using DSNY.Repository.Interfaces.Repositories;

using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Data;
using DSNY.Core.Interfaces;
using DSNY.Common;

namespace DSNY.Repository.Repositories
{
    public class RadioRepository : IRadioRepository
    {
        #region Variables

        private readonly ILogger _logger = null;
        private readonly IExceptionHandler _exceptionHandler = null;
        private DSNYContext _dataProvider = null;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="EquipmentRepository"/> class.
        /// </summary>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public RadioRepository(DSNYContext IoCDataProvider, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _dataProvider = IoCDataProvider;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        public void addRadio(Radio radio)
        {
            try
            {
                _dataProvider.DataContext.Radios.AddObject(radio);
                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        public void deleteRadio(int radioId)
        {
            try
            {
                var radio = _dataProvider.DataContext.Radios.SingleOrDefault(e => e.Radio_id == radioId);

                if (radio != null)
                {
                    radio.is_Active = false;

                    _dataProvider.DataContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        public List<Radio> getAllRadio(bool includeInactive)
        {
            try
            {
                if (includeInactive)
                    return _dataProvider.DataContext.Radios.OrderBy(e => e.Radio_id).ToList();
                else
                    return _dataProvider.DataContext.Radios.OrderBy(e => e.Radio_id).Where(e => e.is_Active == null || e.is_Active == true).ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

       

        public Radio getRadio(int radioId)
        {
            try
            {
                return _dataProvider.DataContext.Radios.SingleOrDefault(e => e.Radio_id == radioId);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        public void updateRadio(int radioId, Radio radio)
        {
            throw new Exception("Not implemented");
        }

        public List<Radio_Equipment_Type> GetEquipmentTypes()
        {
            try
            {
                return _dataProvider.DataContext.Radio_Equipment_Type.ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        public List<Radio_Repair_Problem_Type> GetRadioRepairCodes()
        {
            try
            {
                return _dataProvider.DataContext.Radio_Repair_Problem_Type.ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        public List<Radio_Equipment_Status> GetRadioEquipmentStatus()
        {
            try
            {
                return _dataProvider.DataContext.Radio_Equipment_Status.ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        public List<Radio_Make> GetRadioMakes()
        {
            try
            {
                return _dataProvider.DataContext.Radio_Make.ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        public List<Radio_Model> GetRadioModelsByMakeId(int makeId, bool includeInactive)
        {
            try
            {
                if (includeInactive)
                    return _dataProvider.DataContext.Radio_Model.Where(x => x.Radio_Make_id == makeId).ToList();
                else
                    return _dataProvider.DataContext.Radio_Model.Where(x => x.Radio_Make_id == makeId && (x.is_Active == null || x.is_Active == true)).ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        public List<Radio_History> GetRadioHistory(int radioId)
        {
            try
            {
                return _dataProvider.DataContext.Radio_History.Where(x => x.Radio_id == radioId).ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        public List<Radio_Accessory> GetAllRadioAccessories()
        {
            try
            {
                return _dataProvider.DataContext.Radio_Accessory.ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }
    }
}
