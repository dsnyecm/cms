using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.Data;

namespace DSNY.Core.Repository
{
    public class ZoneRepository
    {
        private DSNYContext _context;

        public ZoneRepository(DSNYContext context)
        {
            _context = context;
        }

        public List<Zone> GetAll()
        {
            return _context.DataContext.Zones.ToList();
        }

        public List<Zone> GetActive()
        {
            return _context.DataContext.Zones.Where(x => !x.is_Active.HasValue || x.is_Active.Value).ToList();
        }

        public Zone GetById(int id)
        {
            return _context.DataContext.Zones.Where(x => x.Zone_id == id).FirstOrDefault();
        }
    }
}
