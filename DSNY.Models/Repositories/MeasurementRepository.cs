using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.Data;

namespace DSNY.Core.Repository
{
    public class MeasurementRepository
    {
        private DSNYContext _context;

        public MeasurementRepository(DSNYContext context)
        {
            _context = context;
        }

        public List<Measurement> GetAll()
        {
            return _context.DataContext.Measurements.ToList();
        }

        public Measurement GetById(int id)
        {
            return _context.DataContext.Measurements.Where(x => x.Measurement_id == id).FirstOrDefault();
        }
    }
}
