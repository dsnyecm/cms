﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DSNY.Core.Interfaces;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Data;
using System.Data.Objects.DataClasses;
using DSNY.Common;

namespace DSNY.Core.Repository
{
    /// <summary>
    /// An implementation of the <see cref="IProductRepository"/> interface
    /// </summary>
    public class ProductRepository : IProductRepository
    {
        #region Variables

        private readonly ILogger _logger = null;
        private readonly IExceptionHandler _exceptionHandler = null;
        private IUserRepository _userRepository = null;
        private DSNYContext _dataProvider = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductRepository"/> class.
        /// </summary>
        public ProductRepository() : this(null, null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductRepository"/> class.
        /// </summary>
        /// <param name="IoCUserRepository">The inversion of control user repository.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public ProductRepository(DSNYContext IoCDataProvider, IUserRepository IoCUserRepository, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _dataProvider = IoCDataProvider;
            _userRepository = IoCUserRepository;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region Get

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <param name="includeInactive">if set to <c>true</c> [include inactive].</param>
        /// <returns></returns>
        public List<Product> getAllProducts(bool includeInactive, bool includeWater)
        {
            try
            {
                List<Product> returnProducts;

                if (includeInactive)
                    returnProducts = _dataProvider.DataContext.Products
                        .OrderBy(p => p.Order_Num == null)
                        .ThenBy(p => p.Order_Num)
                        .ThenBy(p => p.Product_Name).ToList();
                else {
                    returnProducts = _dataProvider.DataContext.Products
                        .Where(p => p.is_Active == true)
                        .OrderBy(p => p.Order_Num == null)
                        .ThenBy(p => p.Order_Num)
                        .ThenBy(p => p.Product_Name).ToList();
                }

                if (!includeWater)
                {
                    returnProducts = returnProducts.Where(p => !p.is_Water).ToList();
                }

                return returnProducts;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        /// <summary>
        /// Gets the product.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns></returns>
        public Product getProduct(int productId)
        {
            try
            {
                return _dataProvider.DataContext.Products.SingleOrDefault(p => p.Product_ID == productId);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        /// <summary>
        /// Gets the sorted products.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="direction">The direction.</param>
        /// <param name="includeInactive">if set to <c>true</c> [include inactive].</param>
        /// <returns></returns>
        public List<Product> getSortedProducts(string field, Enums.SortDirection direction, bool includeInactive)
        {
            try
            {
                List<Product> unSortedProducts = getAllProducts(includeInactive, true);
                List<Product> sortedProduct;

                if (direction == Enums.SortDirection.Descending)
                {
                    sortedProduct = (from Product in unSortedProducts
                                     orderby orderBy(field, Product) descending
                                     select Product).ToList();
                }
                else
                {
                    sortedProduct = (from Product in unSortedProducts
                                     orderby orderBy(field, Product) ascending
                                     select Product).ToList();
                }

                _dataProvider.DataContext.Products.MergeOption = System.Data.Objects.MergeOption.NoTracking;

                return sortedProduct;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        /// <summary>
        /// Gets the users of product.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns></returns>
        public List<Product_User> getUsersOfProduct(int productId)
        {
            try
            {
                List<Product_User> productUsers = _dataProvider.DataContext.Product_User.Where(pu => pu.Product_ID == productId && pu.Product.is_Active == true).ToList();

                return productUsers;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        /// <summary>
        /// Gets the users of product.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns></returns>
        public List<aspnet_Users> getUsersOfProducts()
        {
            try
            {
                List<aspnet_Users> users = new List<aspnet_Users>();

                List<Product_User> productUsers = _dataProvider.DataContext.Product_User.Where(pu => pu.Product.is_Active == true).ToList();

                productUsers.ForEach(p => users.Add(p.aspnet_Users));

                return users.Distinct().ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }


        /// <summary>
        /// Gets the products for user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public List<Product_User> getProductsForUser(Guid userId, bool includeWater)
        {
            try
            {
                List<Product_User> returnProducts = _dataProvider.DataContext.Product_User.Where(pu => pu.UserId == userId && pu.Product.is_Active == true)
                    .OrderBy(pu => pu.Product.Order_Num == null)
                    .ThenBy(pu => pu.Product.Order_Num).ToList();

                if (!includeWater)
                {
                    returnProducts = returnProducts.Where(p => !p.Product.is_Water).ToList();
                }

                return returnProducts;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Gets the products for all users.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public List<Product_User> getProductsForUsers(bool includeWater)
        {
            try
            {
                List<Product_User> returnProducts = _dataProvider.DataContext.Product_User.Where(pu => pu.Product.is_Active == true)
                    .OrderBy(pu => pu.Product.Order_Num == null)
                    .ThenBy(pu => pu.Product.Order_Num).ToList();

                if (!includeWater)
                {
                    returnProducts = returnProducts.Where(p => !p.Product.is_Water).ToList();
                }

                return returnProducts;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        /// <summary>
        /// Gets the products for user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public List<Product> getProductsVendorsForUser(Guid userId, bool includeWater)
        {
            try
            {
                List<Product_User> products = _dataProvider.DataContext.Product_User.Where(pu => pu.UserId == userId && pu.Product.is_Active == true)
                    .OrderBy(pu => pu.Product.Order_Num == null)
                    .ThenBy(pu => pu.Product.Order_Num).ToList();

                if (!includeWater)
                {
                    products = products.Where(p => !p.Product.is_Water).ToList();
                }

                List<Product> returnProducts = new List<Product>();

                products.ForEach(pu =>
                {
                    var temp = new Product()
                    {
                        Product_ID = pu.Product_ID,
                        Product_Name = pu.Product.Product_Name,
                        Order_Delivery_Distribution = pu.Product.Order_Delivery_Distribution,
                        Order_Delivery_Type = pu.Product.Order_Delivery_Type,
                        Measurement_Type = pu.Product.Measurement_Type,
                        Default_Capacity = pu.Product.Default_Capacity,
                        is_Water = pu.Product.is_Water,
                        is_Drums = pu.Product.is_Drums,
                        is_Sub_Type = pu.Product.is_Sub_Type,
                        Product_Sub_Type = new EntityCollection<Product_Sub_Type>()
                    };

                    pu.Product.Product_Sub_Type.ToList().ForEach(pst => temp.Product_Sub_Type.Add(pst));
                    pu.Product.Product_Vendor.ToList().ForEach(pv => temp.Product_Vendor.Add(pv));

                    returnProducts.Add(temp);
                });

                return returnProducts;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        /// <summary>
        /// Gets all sub types for products
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public List<Product_Sub_Type> getProductSubTypes()
        {
            try
            {
                List<Product_Sub_Type> returnSubTypes = _dataProvider.DataContext.Product_Sub_Type.ToList();

                return returnSubTypes;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        #endregion

        #region Add

        /// <summary>
        /// Adds the product.
        /// </summary>
        /// <param name="productName">Name of the product.</param>
        /// <param name="isActive">if set to <c>true</c> [is active].</param>
        /// <param name="isDrums">if set to <c>true</c> [is drums].</param>
        /// <param name="isDrums">if set to <c>true</c> [is reading required].</param>
        /// <param name="defaultCapacity">The default capacity.</param>
        /// <param name="measurementType">Type of the measurement.</param>
        /// <returns></returns>
        public int addProduct(string productName, bool isSubType, bool isActive, bool isDrums, bool isReadingRequired, int defaultCapacity, string measurementType)
        {
            try
            {
                Product newProduct = new Product()
                {
                    Product_Name = productName,
                    is_Sub_Type = isSubType,
                    is_Active = isActive,
                    is_Drums = isDrums,
                    is_Reading_Required = isReadingRequired,
                    Default_Capacity = defaultCapacity,
                    Measurement_Type = measurementType
                };

                _dataProvider.DataContext.Products.AddObject(newProduct);
                _dataProvider.DataContext.SaveChanges();

                return newProduct.Product_ID;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Adds the product.
        /// </summary>
        /// <param name="newProduct">The new product.</param>
        /// <returns></returns>
        public int addProduct(Product newProduct)
        {
            try
            {
                _dataProvider.DataContext.Products.AddObject(newProduct);
                _dataProvider.DataContext.SaveChanges();

                return newProduct.Product_ID;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Adds the user to product.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="capacity">The capacity.</param>
        public void addUserToProduct(int productId, Guid userId, int capacity, int? productSubTypeId)
        {
            try
            {
                // search for product
                Product_User newProdUser = _dataProvider.DataContext.Product_User.SingleOrDefault(pu => pu.Product_ID == productId && pu.UserId == userId);

                if (newProdUser != null)
                {
                    newProdUser.Capacity = capacity > 0 ? capacity : 0;
                    newProdUser.Product_Sub_Type_Id = productSubTypeId;
                }
                else
                {
                    if (capacity > 0)
                        newProdUser = new Product_User() { Product_ID = productId, UserId = userId, Capacity = capacity, Product_Sub_Type_Id = productSubTypeId };
                    else
                        newProdUser = new Product_User() { Product_ID = productId, UserId = userId, Product_Sub_Type_Id = productSubTypeId };

                    _dataProvider.DataContext.Product_User.AddObject(newProdUser);
                }

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates the product.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <param name="updatedProduct">The updated product.</param>
        /// <param name="removeUsers">if set to <c>true</c> [remove users].</param>
        /// <returns></returns>
        public bool updateProduct(int productId, Product updatedProduct, bool removeUsers)
        {
            try
            {
                Product foundProduct = _dataProvider.DataContext.Products.Include("Product_User").SingleOrDefault(p => p.Product_ID == productId);

                if (foundProduct != null && foundProduct.Product_ID > 0)
                {
                    foundProduct.Product_Name = updatedProduct.Product_Name;
                    foundProduct.is_Sub_Type = updatedProduct.is_Sub_Type;
                    foundProduct.is_Active = updatedProduct.is_Active;
                    foundProduct.is_Drums = updatedProduct.is_Drums;
                    foundProduct.is_Reading_Required = updatedProduct.is_Reading_Required;
                    foundProduct.Order_Num = updatedProduct.Order_Num;
                    foundProduct.Default_Capacity = updatedProduct.Default_Capacity;
                    foundProduct.Measurement_Type = updatedProduct.Measurement_Type;
                    foundProduct.Order_Delivery_Type = updatedProduct.Order_Delivery_Type;
                    foundProduct.Order_Delivery_Distribution = updatedProduct.Order_Delivery_Distribution;

                    // find and remove sub types that should deleted
                    var deleteSubTypes = foundProduct.Product_Sub_Type
                        .Where(pst => updatedProduct.Product_Sub_Type.FirstOrDefault(st => st.Product_Sub_Type_ID == pst.Product_Sub_Type_ID) == null);

                    foreach (Product_Sub_Type pst in deleteSubTypes.ToList())
                    {
                        _dataProvider.DataContext.Product_Sub_Type.Attach(pst);
                        _dataProvider.DataContext.DeleteObject(pst);
                    }

                    // add / update sub types
                    updatedProduct.Product_Sub_Type.ToList().ForEach(st =>
                    {
                        if (st.Product_Sub_Type_ID > 0)
                        {
                            var foundSubType = foundProduct.Product_Sub_Type.FirstOrDefault(pst => pst.Product_ID == foundProduct.Product_ID && pst.Product_Sub_Type_ID == st.Product_Sub_Type_ID);
                            if (foundSubType != null)
                            {
                                foundSubType.Sub_Type = st.Sub_Type;
                            }
                            else
                            {
                                foundProduct.Product_Sub_Type.Add(st);
                            }
                        }
                        else
                        {
                            foundProduct.Product_Sub_Type.Add(st);
                        }
                    });

                    // remove all vendors, then add back what we have
                    foreach (Product_Vendor pv in foundProduct.Product_Vendor.ToList())
                    {
                        _dataProvider.DataContext.Product_Vendor.Attach(pv);
                        _dataProvider.DataContext.DeleteObject(pv);
                    }

                    // add / update vendors
                    updatedProduct.Product_Vendor.ToList().ForEach(pv =>
                    {
                        if (pv.Product_Vendor_ID > 0)
                        {
                            var foundVendor = foundProduct.Product_Vendor.FirstOrDefault(pvt => pvt.Product_ID == foundProduct.Product_ID && pvt.Vendor_ID == pv.Vendor_ID);
                            if (foundVendor != null)
                            {
                                foundVendor.Vendor_ID = pv.Vendor_ID;
                            }
                            else
                            {
                                foundProduct.Product_Vendor.Add(pv);
                            }
                        }
                        else
                        {
                            foundProduct.Product_Vendor.Add(pv);
                        }
                        
                    });

                    if (removeUsers)
                    {
                        foreach (Product_User user in foundProduct.Product_User.ToList())
                        {
                            foundProduct.Product_User.Remove(user);
                        }
                    }

                    _dataProvider.DataContext.SaveChanges();

                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        public bool updateProductUserCapacity(int productId, Guid userId, int capacity)
        {
            try
            {
                Product_User foundUserProduct = _dataProvider.DataContext.Product_User.SingleOrDefault(pu => pu.Product_ID == productId && pu.UserId == userId);

                if (foundUserProduct != null && foundUserProduct.Product_ID > 0)
                {
                    foundUserProduct.Capacity = capacity;

                    _dataProvider.DataContext.SaveChanges();

                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        public bool updateProductUserSubType(int productId, Guid userId, int? productSubTypeId)
        {
            try
            {
                Product_User foundUserProduct = _dataProvider.DataContext.Product_User.SingleOrDefault(pu => pu.Product_ID == productId && pu.UserId == userId);

                if (foundUserProduct != null && foundUserProduct.Product_ID > 0)
                {
                    foundUserProduct.Product_Sub_Type_Id = productSubTypeId;

                    _dataProvider.DataContext.SaveChanges();

                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        public bool updateProductUserVendor(int productId, Guid userId, int? vendorId)
        {
            try
            {
                Product_User foundUserProduct = _dataProvider.DataContext.Product_User.SingleOrDefault(pu => pu.Product_ID == productId && pu.UserId == userId);

                if (foundUserProduct != null && foundUserProduct.Product_ID > 0)
                {
                    foundUserProduct.Vendor_ID = vendorId;

                    _dataProvider.DataContext.SaveChanges();

                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }


        #endregion

        #region Delete

        /// <summary>
        /// Deletes the product.
        /// </summary>
        /// <param name="productId">The product id.</param>
        public void deleteProduct(int productId)
        {
            try
            {
                // Get all Product_User items which belong to this product and delete those references
                //IQueryable<Product_User> productUsers = _dataProvider.DataContext.Product_User.Where(pu => pu.Product_ID == productId);

                //foreach (Product_User pu in productUsers)
                //{
                //    _dataProvider.DataContext.Product_User.DeleteObject(pu);
                //}

                // Now delete the product itself and then save all changes
                Product deleteProduct = _dataProvider.DataContext.Products.Single(p => p.Product_ID == productId);

                if (deleteProduct != null)
                    deleteProduct.is_Active = false;

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                if (ex is ArgumentNullException || ex is InvalidOperationException)
                {
                    _dataProvider.DataContext.SaveChanges();
                }
                else
                {
                    bool rethrow = _exceptionHandler.HandleException(ex);

                    if (rethrow)
                        throw new Exception("There was an error deleting the product.");
                }
            }
        }

        /// <summary>
        /// Deletes all products for user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        public void deleteAllProductsForUser(Guid userId)
        {
            try
            {
                List<Product_User> productsForUser = _dataProvider.DataContext.Product_User.Where(pu => pu.UserId == userId).ToList();

                foreach (Product_User pu in productsForUser)
                {
                    _dataProvider.DataContext.Product_User.DeleteObject(pu);
                }

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        #endregion

        #region Sort

        // Our sorter object, holds the field key and object type
        /// <summary>
        /// Orders the by.
        /// </summary>
        /// <param name="sortKey">The sort key.</param>
        /// <param name="product">The product.</param>
        /// <returns></returns>
        private object orderBy(string sortKey, Product product)
        {
            switch (sortKey)
            {
                case "ProductName":
                    return product.Product_Name;

                case "IsActive":
                    return product.is_Active;

                case "IsDrum":
                    return product.is_Drums;

                case "IsReadingRequired":
                    return product.is_Reading_Required;

                case "MeasurementType":
                    return product.Measurement_Type;

                case "OrderNum":
                    return product.Order_Num;

                case "DefaultCapacity":
                    return product.Default_Capacity;

                default:
                    return product.Product_Name;
            }
        }

        #endregion
    }
}