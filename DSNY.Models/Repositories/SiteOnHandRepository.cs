﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.Data;
using DSNY.Core.Interfaces;

namespace DSNY.Core.Repository
{
    public class SiteOnHandRepository
    {
        private DSNYContext _context;

        public SiteOnHandRepository(DSNYContext context)
        {
            _context = context;
        }

        public List<STC_Site_On_Hand> GetAll()
        {
            return _context.DataContext.STC_Site_On_Hand.ToList();
        }

        public List<STC_Site_On_Hand> GetActive()
        {
            return _context.DataContext.STC_Site_On_Hand.Where(x => x.is_Active.HasValue && x.is_Active.Value).ToList();
        }

        public List<STC_Site_On_Hand> GetActiveByProduct(int productId)
        {
            try
            {
                return _context.DataContext.STC_Site_On_Hand.Where(x => x.is_Active.HasValue && x.is_Active.Value && x.STC_Product_id == productId).ToList();
            } catch (Exception ex)
            {
                string message = ex.Message;
                throw;
            }
        }

        public List<STC_Site_On_Hand> GetByUserId(Guid userId)
        {
            return _context.DataContext.STC_Site_On_Hand.Where(x => x.is_Active.HasValue && x.is_Active.Value && x.UserId == userId).ToList();
        }

        public List<STC_Site_On_Hand> GetByUsers(List<IUser> users)
        {
            var userIds = users.Select(x => x.userId).ToList();
            return _context.DataContext.STC_Site_On_Hand.Where(x => x.is_Active.HasValue && x.is_Active.Value && userIds.Contains( x.UserId ) ).ToList();
        }

        public void AddOnHand(Guid userId, int productId, decimal amount)
        {
            var onHand = _context.DataContext.STC_Product_User.Where(x => x.STC_User_ID == userId && x.STC_Product_ID == productId).FirstOrDefault();
            if (onHand == null) throw new Exception(string.Format("Site({0})/Product({1}) combination doe not exist.", userId.ToString(), productId));
            onHand.On_Hand_Qty += amount;
        }

        public void SubtractOnHand(Guid userId, int productId, decimal amount)
        {
            var onHand = _context.DataContext.STC_Product_User.Where(x => x.STC_User_ID == userId && x.STC_Product_ID == productId).FirstOrDefault();
            if (onHand == null) throw new Exception(string.Format("Site({0})/Product({1}) combination doe not exist.", userId.ToString(), productId));
            onHand.On_Hand_Qty -= amount;
            if (onHand.On_Hand_Qty <= 0)
            {
                onHand.On_Hand_Qty = 0;
            }
        }
    }
}
