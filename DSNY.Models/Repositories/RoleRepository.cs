﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Security;

using DSNY.Common.Exception;
using DSNY.Common.Logger;
using DSNY.Core.Interfaces;
using DSNY.Core.Models;
using DSNY.Common;
using DSNY.Data;

namespace DSNY.Core.Repository
{
    /// <summary>
    /// An implementation of the <see cref="IRoleRepository"/> interface
    /// </summary>
    public class RoleRepository : IRoleRepository
    {
        #region Variables

        private readonly RoleProvider _roleProvider = null;
        private DSNYContext _dataProvider = null;
        private readonly ILogger _logger = null;
        private readonly IExceptionHandler _exceptionHandler = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleRepository"/> class.
        /// </summary>
        /// <param name="IoCProvider">The inversion of control role provider.</param>
        /// <param name="IoCDataProvider">The inversion of control data provider.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public RoleRepository(RoleProvider IoCProvider, DSNYContext IoCDataProvider, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _roleProvider = IoCProvider;
            _dataProvider = IoCDataProvider;
            _logger = IoCLogger ;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region Get

        /// <summary>
        /// Gets all roles.
        /// </summary>
        /// <returns></returns>
        public string[] getAllRoles()
        {
            try
            {
                return _roleProvider.GetAllRoles();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        public List<aspnet_Roles> getAllAspNetRoles()
        {
            try
            {
                return _dataProvider.DataContext.aspnet_Roles.ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        /// <summary>
        /// Gets all roles list.
        /// </summary>
        /// <returns></returns>
        public List<IRole> getAllRolesList()
        {
            try
            {
                string[] rolesArray = _roleProvider.GetAllRoles();
                List<IRole> roles = new List<IRole>();

                foreach (string role in rolesArray)
                {
                    roles.Add(new Role() { roleName = role, usersInRole = _roleProvider.GetUsersInRole(role).ToList() });
                }

                return roles;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Gets the sorted roles.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="direction">The direction.</param>
        /// <returns></returns>
        public List<IRole> getSortedRoles(string field, Enums.SortDirection direction)
        {
            try
            {
                List<IRole> unSortedRoles = getAllRolesList();
                List<IRole> sorteRoles;

                if (direction == Enums.SortDirection.Descending)
                {
                    sorteRoles = (from IRole in unSortedRoles
                                  orderby orderBy(field, IRole) descending
                                  select IRole).ToList();
                }
                else
                {
                    sorteRoles = (from IRole in unSortedRoles
                                  orderby orderBy(field, IRole) ascending
                                  select IRole).ToList();
                }

                return sorteRoles;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Returns roles with which uses are in each role
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string[]> getAllRolesWithUsers()
        {
            try
            {
                // Get roles from provider
                string[] roles = _roleProvider.GetAllRoles();

                // Create the dictionary to hold role and role users
                Dictionary<string, string[]> rolesWithusers = new Dictionary<string, string[]>();

                // Loop through our roles from provider and parse into dictionary
                // each loop calls another method to retrieve users from each individual role
                foreach (string role in roles)
                {
                    rolesWithusers.Add(role, _roleProvider.GetUsersInRole(role));
                }

                return rolesWithusers;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        public List<IRole> getRolesForUser(string userName)
        {
            try
            {
                List<IRole> rolesList = new List<IRole>();
                string[] rolesArray = _roleProvider.GetRolesForUser(userName);

                foreach (string role in rolesArray)
                {
                    rolesList.Add(new Role() { roleName = role });
                }

                return rolesList;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        #endregion

        #region Add

        /// <summary>
        /// Adds a role
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public bool addRole(string roleName)
        {
            try
            {
                // Checks to see if role exists, then adds if not
                if (!_roleProvider.RoleExists(roleName))
                {
                    _roleProvider.CreateRole(roleName);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Add an array of users to an array of roles
        /// </summary>
        /// <param name="users"></param>
        /// <param name="roles"></param>
        public void addUsersToRoles(string[] users, string[] roles)
        {
            try
            {
                _roleProvider.AddUsersToRoles(users, roles);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes a role.  Telling the provider to *not* delete the role if users are attached.
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public bool deleteRole(string roleName)
        {
            try 
	        {
                _roleProvider.DeleteRole(roleName, true);
                return true;
	        }
	        catch (Exception ex)
	        {
                bool rethrow = _exceptionHandler.HandleException(ex);
                return false;
	        }
        }

        /// <summary>
        /// Removes an array of users from an array of roles
        /// </summary>
        /// <param name="users"></param>
        /// <param name="roles"></param>
        public void deleteUsersFromRoles(string[] users, string[] roles)
        {
            try
            {
                _roleProvider.RemoveUsersFromRoles(users, roles);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        #endregion

        #region Sort

        // Our sorter object, holds the field key and object type
        private object orderBy(string sortKey, IRole role)
        {
            switch (sortKey)
            {
                case "RoleName":
                    return role.roleName;

                default:
                    return role.roleName;
            }
        }

        #endregion
    }
}