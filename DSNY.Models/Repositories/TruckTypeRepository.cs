using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.Data;

namespace DSNY.Core.Repository
{
    public class TruckTypeRepository
    {
        private DSNYContext _context;

        public TruckTypeRepository(DSNYContext context)
        {
            _context = context;
        }

        public List<Truck_Type> GetAll()
        {
            return _context.DataContext.Truck_Type.ToList();
        }

        public Truck_Type GetById(int id)
        {
            return _context.DataContext.Truck_Type.Where(x => x.STC_Truck_Type_id == id).FirstOrDefault();
        }
    }
}
