﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Web.Profile;

using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Data;
using DSNY.Core.Interfaces;
using DSNY.Common;
using DSNY.Common.Utilities;
using System.Data.Objects;

namespace DSNY.Core.Repository
{
    /// <summary>
    /// An implementation of the <see cref="IMessageRepository"/> interface
    /// </summary>
    public class MessageRepository : IMessageRepository
    {
        #region Variables

        private IUserRepository _userRepository;
        private readonly ILogger _logger = null;
        private readonly IExceptionHandler _exceptionHandler = null;
        private DSNYContext _dataProvider = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageRepository"/> class.
        /// </summary>
        public MessageRepository() : this(null, null, null, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageRepository"/> class.
        /// </summary>
        /// <param name="IoCUserRepository">The inversion of control user repository.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public MessageRepository(DSNYContext IoCDataProvider, IUserRepository IoCUserRepository, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _dataProvider = IoCDataProvider;
            _userRepository = IoCUserRepository;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        public void Initialize()
        {
            
        }

        #region Get

        /// <summary>
        /// Gets messages for specific user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public List<Sent_Message> getMessagesForUser(Guid userId, DateTime startDate, DateTime endDate, bool fuelForms, int page, int pageSize, out int totalRecords, bool paged)
        {
            try
            {
                List<Sent_Message> messages = null;

                DateTime searchStartDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
                DateTime searchEndDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);

                _dataProvider.DataContext.Sent_Message.MergeOption = MergeOption.NoTracking;

                if (paged)
                {
                    if (fuelForms)
                    {
                        var returnedMessages = _dataProvider.DataContext.Sent_Message
                            .Where(sm => sm.UserId == userId && sm.Message.Message_Date >= searchStartDate && sm.Message.Message_Date <= searchEndDate)
                            .OrderByDescending(sm => sm.Message.Message_Date);

                        totalRecords = returnedMessages.Count();
                        messages = returnedMessages.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    }
                    else
                    {
                        var returnedMessages = _dataProvider.DataContext.Sent_Message
                            .Where(sm => sm.UserId == userId && sm.Message.Message_Date >= searchStartDate && sm.Message.Message_Date <= searchEndDate &&
                                sm.Message.is_Fuel_Form == false)
                            .OrderByDescending(sm => sm.Message.Message_Date);

                        totalRecords = returnedMessages.Count();
                        messages = returnedMessages.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    }
                }
                else
                {
                    if (fuelForms)
                    {
                        messages = _dataProvider.DataContext.Sent_Message
                            .Where(sm => sm.UserId == userId && sm.Message.Message_Date >= searchStartDate && sm.Message.Message_Date <= searchEndDate)
                            .OrderByDescending(sm => sm.Message.Message_Date).ToList();
                    }
                    else
                    {
                        messages = _dataProvider.DataContext.Sent_Message
                            .Where(sm => sm.UserId == userId && sm.Message.Message_Date >= searchStartDate && sm.Message.Message_Date <= searchEndDate &&
                                sm.Message.is_Fuel_Form == false)
                            .OrderByDescending(sm => sm.Message.Message_Date).ToList();
                    }

                    totalRecords = messages.Count();
                }

                return messages;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }

        }

        /// <summary>
        /// Gets sorted messages for user.
        /// </summary>
        /// <param name="field">The sort field.</param>
        /// <param name="direction">The direction.</param>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public List<Sent_Message> getSortedMessagesForUser(string field, Enums.SortDirection direction, Guid userId, DateTime startDate, DateTime endDate, string searchTerms,
            bool fuelForms, int page, int pageSize, out int totalRecords)
        {
            try
            {
                List<Sent_Message> unSortedMessages = getMessagesForUser(userId, startDate, endDate, fuelForms, page, pageSize, out totalRecords, false);
                List<Sent_Message> sortedMessages;

                _dataProvider.DataContext.Sent_Message.MergeOption = MergeOption.NoTracking;

                if (direction == Enums.SortDirection.Descending)
                {
                    sortedMessages = (from Sent_Message in unSortedMessages
                                      orderby orderBy(field, Sent_Message) descending
                                      select Sent_Message).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    sortedMessages = (from Sent_Message in unSortedMessages
                                      orderby orderBy(field, Sent_Message) ascending
                                      select Sent_Message).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                }

                return sortedMessages;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Gets a message.
        /// </summary>
        /// <param name="messageId">The message id.</param>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public Message getMessage(int messageId, Guid userId)
        {
            try
            {
                _dataProvider.DataContext.Sent_Message.MergeOption = MergeOption.NoTracking;

                Sent_Message sentMessage = _dataProvider.DataContext.Sent_Message.SingleOrDefault(sm => sm.Message_Id == messageId && sm.UserId == userId);

                if (sentMessage != null)
                    return sentMessage.Message;
                else
                    return null;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        #endregion

        #region Add

        /// <summary>
        /// Adds a message and can send to the distribution list
        /// </summary>
        /// <param name="newMessage">The new message.</param>
        /// <param name="sendToDistribution">if set to <c>true</c> [send to distribution].</param>
        public void addMessage(Message newMessage, bool sendToDistribution)
        {
            _dataProvider.DataContext.Sent_Message.MergeOption = MergeOption.AppendOnly;

            try
            {
                //newMessage.Message_Text = TextUtilities.stripTinyMCEHTML(newMessage.Message_Text);
                _dataProvider.DataContext.Messages.AddObject(newMessage);
                _dataProvider.DataContext.SaveChanges();

                if (sendToDistribution)
                {
                    List<IUser> distributionUsers = _userRepository.getAllMessageDistribUsers();

                    foreach (IUser user in distributionUsers)
                    {
                        _dataProvider.DataContext.Sent_Message.AddObject(new Sent_Message() { UserId = user.userId, Message_Id = newMessage.Message_Id });
                    }

                    _dataProvider.DataContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Adds a new message and can send it to the command chain
        /// </summary>
        /// <param name="newMessage">The new message.</param>
        /// <param name="sendToCommandChain">if set to <c>true</c> [send to distribution].</param>
        public void addCommandMessage(Message newMessage, bool sendToCommandChain)
        {
            _dataProvider.DataContext.Sent_Message.MergeOption = MergeOption.AppendOnly;

            try
            {
                newMessage.Message_Text = newMessage.Message_Text != null ? TextUtilities.stripTinyMCEHTML(newMessage.Message_Text) : null;
                _dataProvider.DataContext.Messages.AddObject(newMessage);
                _dataProvider.DataContext.SaveChanges();

                if (sendToCommandChain)
                {
                    List<IUser> reportToUsers = _userRepository.getReportToUsers((Guid)newMessage.Sent_By_UserId);

                    foreach (IUser user in reportToUsers)
                    {
                        _dataProvider.DataContext.Sent_Message.AddObject(new Sent_Message() { UserId = user.userId, Message_Id = newMessage.Message_Id });
                    }

                    _dataProvider.DataContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Adds a message and sends to a specific user
        /// </summary>
        /// <param name="newMessage">The new message.</param>
        /// <param name="userId">Id of user to send to</param>
        public void addMessageAndSendToUser(Message newMessage, Guid userId)
        {
            _dataProvider.DataContext.Sent_Message.MergeOption = MergeOption.AppendOnly;

            try
            {
                _dataProvider.DataContext.Messages.AddObject(newMessage);
                _dataProvider.DataContext.SaveChanges();

                _dataProvider.DataContext.Sent_Message.AddObject(new Sent_Message() { UserId = userId, Message_Id = newMessage.Message_Id });

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Adds a message and sends to a list of users
        /// </summary>
        /// <param name="newMessage">The new message.</param>
        /// <param name="userIds">List of user ids to send to</param>
        public void addMessageAndSendToUsers(Message newMessage, List<Guid> userIds)
        {
            _dataProvider.DataContext.Sent_Message.MergeOption = MergeOption.AppendOnly;

            try
            {
                _dataProvider.DataContext.Messages.AddObject(newMessage);
                _dataProvider.DataContext.SaveChanges();

                userIds.ForEach(uId =>
                {
                    _dataProvider.DataContext.Sent_Message.AddObject(new Sent_Message() { UserId = uId, Message_Id = newMessage.Message_Id });
                });

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }
        
        #endregion

        #region Update

        /// <summary>
        /// Updates a message timestamp after it's been opened.
        /// </summary>
        /// <param name="messageId">The message id.</param>
        /// <param name="userId">The user id.</param>
        public void updateMessageTimestamp(int messageId, Guid userId)
        {
            _dataProvider.DataContext.Sent_Message.MergeOption = MergeOption.AppendOnly;

            try
            {
                Sent_Message updateMessage = _dataProvider.DataContext.Sent_Message.SingleOrDefault(sm => sm.Message_Id == messageId && sm.UserId == userId);

                if (updateMessage != null)
                {
                    updateMessage.Open_Date = DateTime.Now;
                    _dataProvider.DataContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Updates a message timestamp after it's been opened.
        /// </summary>
        /// <param name="messageId">The message id.</param>
        /// <param name="userId">The user id.</param>
        public void updateNewUserWithCurrentMessages(Guid userId)
        {
            try
            {
                _dataProvider.DataContext.UpdateNewUserWithCurrentMessages(userId);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        #endregion

        #region Send
        
        /// <summary>
        /// Sends a specific message to a specific user
        /// </summary>
        /// <param name="messageId">The message id.</param>
        /// <param name="userId">The user id.</param>
        public void sendMessageToUser(int messageId, Guid userId)
        {
            try
            {
                _dataProvider.DataContext.Sent_Message.AddObject(new Sent_Message() { UserId = userId, Message_Id = messageId });

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Sends a specific message to list of users
        /// </summary>
        /// <param name="messageId">The message id.</param>
        /// <param name="userIds">List of user ids to send to.</param>
        public void sendMessageToUsers(int messageId, List<Guid> userIds)
        {
            try
            {
                userIds.ForEach(uId =>
                {
                    _dataProvider.DataContext.Sent_Message.AddObject(new Sent_Message() { UserId = uId, Message_Id = messageId });
                });

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        #endregion

        #region Search

        /// <summary>
        /// Searches messages.
        /// </summary>
        /// <param name="searchTerms">The search terms.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public List<Sent_Message> searchMessages(string searchTerms, DateTime startDate, DateTime endDate, Guid userId, bool isFuelForm, int pageNumber, int pageSize, string sortField,
            string sortDirection, out int totalRecords)
        {
            totalRecords = 0;

            try
            {
                _dataProvider.DataContext.Sent_Message.MergeOption = MergeOption.NoTracking;

                var outputParameter = new ObjectParameter("TotalRecords", typeof(int));

                List<Sent_Message> searchResults = _dataProvider.DataContext.spSearchMessages("\"" + searchTerms + "\"", startDate, endDate, userId, isFuelForm, pageNumber, pageSize,
                    sortField, sortDirection, outputParameter).ToList();

                int.TryParse(outputParameter.Value.ToString(), out totalRecords);

                return searchResults;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        #endregion

        #region Sort

        /// <summary>
        /// Our sorter object, holds the field key and object type
        /// </summary>
        /// <param name="sortKey">The sort key.</param>
        /// <param name="sentMessage">The sent message.</param>
        /// <returns></returns>
        private object orderBy(string sortKey, Sent_Message sentMessage)
        {
            switch (sortKey)
            {
                case "Status":
                    return sentMessage.Open_Date;

                case "Dept_Msg_No":
                    return sentMessage.Message.Dept_Msg_No;

                case "Code":
                    return sentMessage.Message.Message_Code;

                case "Subject":
                    return sentMessage.Message.Message_Subject;

                case "Date":
                    return sentMessage.Message.Message_Date;

                case "SentBy":
                    return sentMessage.Message.aspnet_Users.UserName;

                default:
                    return sentMessage.Message.Message_Text;
            }
        }

        #endregion
    }
}
