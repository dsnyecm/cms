using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.Data;

namespace DSNY.Core.Repository
{
    public class StcPickupRepository
    {
        private DSNYContext _context;

        public StcPickupRepository(DSNYContext context)
        {
            _context = context;
        }

        public List<STC_Pickup> GetAll()
        {
            return _context.DataContext.STC_Pickup.ToList();
        }

        public List<STC_Pickup> GetByUserIds(List<Guid> userIds)
        {
            return _context.DataContext.STC_Pickup.Where(x => userIds.Contains(x.Userid.Value)).ToList();
        }

        public STC_Pickup GetById(int id)
        {
            return _context.DataContext.STC_Pickup.Where(x => x.STC_Pickup_id == id).FirstOrDefault();
        }

        public List<STC_Pickup> GetByProductId(int productId)
        {
            return _context.DataContext.STC_Pickup.Where(x => x.STC_Product_id == productId).ToList();
        }
    }
}
