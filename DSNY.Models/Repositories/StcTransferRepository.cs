using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.Data;

namespace DSNY.Core.Repository
{
    public class StcTransferRepository
    {
        private DSNYContext _context;

        public StcTransferRepository(DSNYContext context)
        {
            _context = context;
        }

        public List<STC_Transfer> GetAll()
        {
            return _context.DataContext.STC_Transfer.ToList();
        }

        public STC_Transfer GetById(int id)
        {
            return _context.DataContext.STC_Transfer.Where(x => x.STC_Transfer_Id == id).FirstOrDefault();
        }

        public List<STC_Transfer> GetByProductId(int productId)
        {
            return _context.DataContext.STC_Transfer.Where(x => x.STC_Product_id == productId).ToList();
        }
    }
}
