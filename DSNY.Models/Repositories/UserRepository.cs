﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;

using DSNY.Common.Exception;
using DSNY.Common.Logger;
using DSNY.Core.Models;
using DSNY.Core.Interfaces;
using DSNY.Common;
using DSNY.Data;

namespace DSNY.Core.Repository
{
    /// <summary>
    /// An implementation of the <see cref="IUserRepository"/> interface
    /// </summary>
    public class UserRepository : IUserRepository
    {
        #region Variables

        private readonly MembershipProvider _userProvider = null;
        private readonly IRoleRepository _roleRepository = null;
        private readonly IFuelFormRepository _fuelFormRepository = null;
        private readonly ProfileProvider _profileProvider = null;
        private readonly RoleProvider _roleProvider = null;
        private DSNYContext _dataProvider = null;
        private ILogger _logger { get; set; }
        private IExceptionHandler _exceptionHandler { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        /// <param name="IoCMembershipProvider">The inversion of control membership provider.</param>
        /// <param name="IoCRoleRepository">The inversion of control role repository.</param>
        /// <param name="IoCProfileProvider">The inversion of control profile provider.</param>
        /// <param name="IoCRoleProvider">The inversion of control role provider.</param>
        /// <param name="IoCFuelFormRepo">The inversion of control fuel form repo.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public UserRepository(DSNYContext IoCDataProvider, MembershipProvider IoCMembershipProvider, IRoleRepository IoCRoleRepository, ProfileProvider IoCProfileProvider,
            RoleProvider IoCRoleProvider, IFuelFormRepository IoCFuelFormRepo, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _dataProvider = IoCDataProvider;
            _userProvider = IoCMembershipProvider;
            _roleRepository = IoCRoleRepository;
            _fuelFormRepository = IoCFuelFormRepo;
            _profileProvider = IoCProfileProvider;
            _roleProvider = IoCRoleProvider;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region Get

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns></returns>
        public IUser getUser(string userName)
        {
            try
            {
                MembershipUser tempUser = _userProvider.GetUser(userName, false);
                IUser user = new User();

                if (tempUser != null)
                    user = convertMembershipUserToIUser(tempUser);

                return user;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public IUser getUser(Guid userId)
        {
            try
            {
                MembershipUser tempUser = _userProvider.GetUser(userId, false);

                IUser user = new User();

                if (tempUser != null)
                    user = convertMembershipUserToIUser(tempUser);

                return user;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

      

        public List<IUser> getStcBoroughUserBySiteUserId(Guid userId)
        {
            List<IUser> returnList = new List<IUser>();

            try
            {
                aspnet_Users userToGet = _dataProvider.DataContext.aspnet_Users.SingleOrDefault(u => u.UserId == userId);

                if (userToGet != null)
                {
                    foreach (aspnet_Users user in userToGet.aspnet_Users1)
                    {
                        IUser tempUser = getUser(user.UserId);

                        if (!string.IsNullOrEmpty(tempUser.userName))
                        {
                            if (IsUserInRole(tempUser.userId, "STC Borough"))
                            {
                                returnList.Add(tempUser);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }

            return returnList;
        }

        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <returns></returns>
        public List<IUser> getAllUsers(bool light)
        {
            try
            {
                int totalRecords = 0;
                List<IUser> newUsers = new List<IUser>();

                if (light)
                {
                    List<aspnet_Users> users = _dataProvider.DataContext.aspnet_Users.Select(u => u).ToList();

                    foreach (aspnet_Users user in users)
                    {
                        newUsers.Add(new User() { userId = user.UserId, userName = user.UserName });
                    }
                }
                else
                {
                    MembershipUserCollection users = _userProvider.GetAllUsers(0, 800, out totalRecords);

                    foreach (MembershipUser user in users)
                    {
                        IUser newUser = new User();
                        ProfileBase profile = ProfileBase.Create(user.UserName);

                        newUser.userId = (Guid)user.ProviderUserKey;
                        newUser.description = profile["Description"] != null ? profile["Description"].ToString() : string.Empty;
                        //newUser.address = profile["Address"] != null ? profile["Address"].ToString() : string.Empty;
                        //newUser.borough = profile["Borough"] != null ? profile["Borough"].ToString() : string.Empty;
                        //newUser.poDisplayName = profile["PODisplayName"] != null ? profile["PODisplayName"].ToString() : string.Empty;
                        newUser.userName = user.UserName;
                        newUser.email = user.Email;
                        //newUser.roles = _roleRepository.getRolesForUser(newUser.userName);
                        //newUser.isDistribution = profile["IsDistribution"] != null ? (bool)profile["IsDistribution"] : false;
                        //newUser.isEmail = profile["IsEmail"] != null ? (bool)profile["IsEmail"] : false;
                        //newUser.isActive = user.IsApproved;
                        newUser.lastLogin = user.LastLoginDate;
                        //newUser.isLocked = user.IsLockedOut;

                        newUsers.Add(newUser);
                    }
                }

                return newUsers;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <returns></returns>
        public List<IUser> getStcUsersWithActiveSiteOnHandImpl(Guid currentUserId, int? productId, int? zoneId)
        {
            try
            {
                var currentUser = getUser(currentUserId);
                var roles = _roleRepository.getRolesForUser(currentUser.userName).Select(x => x.roleName);
                if (roles.Contains("STC HQ"))
                {
                    return getStcUsersWithActiveSiteOnHandImpl(productId, zoneId);
                }

                List<IUser> users = new List<IUser>();
                users.Add(currentUser);
                users.AddRange(getUserCommandChain(currentUserId));

                var siteOnHands = new SiteOnHandRepository(_dataProvider).GetByUsers(users);

                if (productId.HasValue)
                {
                    siteOnHands = siteOnHands.Where(x => x.STC_Product_id == productId.Value).ToList();
                }

                if (zoneId.HasValue)
                {
                    var sitesInZone = new ZoneRepository(_dataProvider).GetById(zoneId.Value).aspnet_Users.Select(x => x.UserId).ToList();
                    siteOnHands = siteOnHands.Where(x => sitesInZone.Contains(x.UserId)).ToList();
                }

                List<IUser> newUsers = new List<IUser>();

                foreach (var siteOnHand in siteOnHands)
                {
                    if (!newUsers.Select(x => x.userId).Contains(siteOnHand.UserId))
                    {
                        newUsers.Add(new User() { userId = siteOnHand.UserId, userName = siteOnHand.UserName });
                    }
                }

                return newUsers.OrderBy(x => x.userName).ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <returns></returns>
        public List<IUser> getStcUsersWithActiveSiteOnHand(Guid currentUserId)
        {
            return getStcUsersWithActiveSiteOnHandImpl(currentUserId, null, null);
        }

        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <returns></returns>
        public List<IUser> getStcUsersWithActiveSiteOnHand(Guid currentUserId, int productId)
        {
            return getStcUsersWithActiveSiteOnHandImpl(currentUserId, productId, null);
        }

        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <returns></returns>
        public List<IUser> getStcUsersWithActiveSiteOnHand(Guid currentUserId, int productId, int zoneId)
        {
            return getStcUsersWithActiveSiteOnHandImpl(currentUserId, productId, zoneId);
        }

        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <returns></returns>
        private List<IUser> getStcUsersWithActiveSiteOnHandImpl(int? productId, int? zoneId)
        {
            try
            {
                List<IUser> newUsers = new List<IUser>();

                List<aspnet_Users> users = _dataProvider.DataContext.aspnet_Users.Where(x => x.aspnet_Roles.Any(y => y.RoleName == "STC Site")).Select(u => u).ToList();
                
                foreach (aspnet_Users user in users)
                {
                    newUsers.Add(new User() { userId = user.UserId, userName = user.UserName });
                }

                var siteOnHands = new SiteOnHandRepository(_dataProvider).GetByUsers(newUsers);

                if (productId.HasValue)
                {
                    siteOnHands = siteOnHands.Where(x => x.STC_Product_id == productId.Value).ToList();
                }
                if (zoneId.HasValue)
                {
                    var sitesInZone = new ZoneRepository(_dataProvider).GetById(zoneId.Value).aspnet_Users.Select(x => x.UserId).ToList();
                    siteOnHands = siteOnHands.Where(x => sitesInZone.Contains(x.UserId)).ToList();
                }

                List<IUser> retUsers = new List<IUser>();

                foreach (var siteOnHand in siteOnHands)
                {
                    if (!retUsers.Select(x => x.userId).Contains(siteOnHand.UserId))
                    {
                        retUsers.Add(new User() { userId = siteOnHand.UserId, userName = siteOnHand.UserName });
                    }
                }

                return retUsers.OrderBy(x => x.userName).ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        /// <summary>
        /// Gets all users with site on hand and product id.
        /// </summary>
        /// <returns></returns>
        public List<IUser> getStcUsersWithActiveSiteOnHand(int productId)
        {
            return getStcUsersWithActiveSiteOnHandImpl(productId, null);
        }

        /// <summary>
        /// Gets all users with site on hand.
        /// </summary>
        /// <returns></returns>
        public List<IUser> getStcUsersWithActiveSiteOnHand()
        {
            return getStcUsersWithActiveSiteOnHandImpl(null, null);
        }

        /// <summary>
        /// Gets all stc users
        /// </summary>
        /// <returns></returns>
        public List<IUser> getStcUsers()
        {
            try
            {
                List<IUser> newUsers = new List<IUser>();
                var stcUsers = _dataProvider.DataContext.aspnet_Users.Where(x => x.aspnet_Roles.Any(y => y.RoleName == "STC Site")).Select(u => u).OrderBy(x => x.UserName).ToList();

                foreach (var user in stcUsers)
                {
                    newUsers.Add(new User() { userId = user.UserId, userName = user.UserName });
                }

                return newUsers;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        /// <summary>
        /// Gets all stc users for HQ, and users in command chain for borough and site users.
        /// </summary>
        /// <returns></returns>
        public List<IUser> getStcUsers(Guid currentUserId)
        {
            try
            {
                List<IUser> newUsers = new List<IUser>();
                var currentUser = getUser(currentUserId);
                var roles = _roleRepository.getRolesForUser(currentUser.userName).Select(x => x.roleName);
                var stcUsers = _dataProvider.DataContext.aspnet_Users.Where(x => x.aspnet_Roles.Any(y => y.RoleName == "STC Site")).Select(u => u).OrderBy(x => x.UserName).ToList();
                if (roles.Contains("STC HQ"))
                {
                    foreach (var user in stcUsers)
                    {
                        newUsers.Add(new User() { userId = user.UserId, userName = user.UserName });
                    }

                    return newUsers;
                }

                List<IUser> users = new List<IUser>();
                users.Add(currentUser);
                users.AddRange(getUserCommandChain(currentUserId));

                var userIds = users.Select(x => x.userId).ToList();
                var stcUserCommandChain = stcUsers.Where(x => userIds.Contains(x.UserId)).OrderBy(x => x.UserName).ToList();
                
                foreach (var stc in stcUserCommandChain)
                {
                    newUsers.Add(new User() { userId = stc.UserId, userName = stc.UserName });
                }

                return newUsers;

            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        /// <summary>
        /// Gets all users.
        /// </summary>s
        /// <returns></returns>
        public bool IsUserInRole(Guid userId, string roleName)
        {
            try
            {
                var roles = _dataProvider.DataContext.aspnet_Users.Where(x => x.UserId == userId)
                    .Select(x => x.aspnet_Roles.Where(y => y.RoleName == roleName).Select(y => y.RoleName)).ToList()[0];
                return roles.Any();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }
        }

        /// <summary>
        /// Gets all email address users.
        /// </summary>
        /// <returns></returns>
        public List<IUser> getAllEmailAddressUsers()
        {
            try
            {
                return getAllUsers(false).Where(u => !(string.IsNullOrEmpty(u.email))).ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Gets all message distrib users.
        /// </summary>
        /// <returns></returns>
        public List<IUser> getAllMessageDistribUsers()
        {
            try
            {
                int totalRecords = 0;

                MembershipUserCollection users = _userProvider.GetAllUsers(0, 800, out totalRecords);
                List<IUser> newUsers = new List<IUser>();

                ProfileBase userProfile = null;

                foreach (MembershipUser user in users)
                {
                    if (user.IsApproved)
                    {
                        userProfile = ProfileBase.Create(user.UserName);

                        if ((bool)userProfile["IsDistribution"])
                            newUsers.Add(convertMembershipUserToIUser(user));
                    }
                }

                return newUsers;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Gets all email distrib users.
        /// </summary>
        /// <returns></returns>
        public List<IUser> getAllEmailDistribUsers()
        {
            try
            {
                int totalRecords = 0;

                MembershipUserCollection users = _userProvider.GetAllUsers(0, 800, out totalRecords);
                List<IUser> newUsers = new List<IUser>();

                ProfileBase userProfile = null;

                foreach (MembershipUser user in users)
                {
                    if (user.IsApproved)
                    {
                        userProfile = ProfileBase.Create(user.UserName);

                        if ((bool)userProfile["IsEmail"])
                            newUsers.Add(convertMembershipUserToIUser(user));
                    }
                }

                return newUsers;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        public List<IUser>getRoleUsers(string roleName)
        {
            List<string> userNames = _roleProvider.GetUsersInRole(roleName).ToList();

            return userNames.Select(user => getUser(user)).ToList();
        }

        public List<IUser> getRolesUsers(List<string> roleNames)
        {
            IEnumerable<IUser> users = new List<IUser>();

            roleNames.ForEach(roleName =>
            {
                List<string> userNames = _roleProvider.GetUsersInRole(roleName).ToList();
                users = users.Concat(userNames.Select(user => getUser(user)));
            });

            return users.GroupBy(u => u.userId)
                .Select(u => u.FirstOrDefault()).ToList();
        }

        /// <summary>
        /// Gets the user command chain.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public List<IUser> getUserCommandChain(Guid userId)
        {
            List<IUser> returnList = new List<IUser>();

            try
            {
                aspnet_Users userToGet = _dataProvider.DataContext.aspnet_Users.SingleOrDefault(u => u.UserId == userId);

                if (userToGet != null)
                {
                    foreach (aspnet_Users user in userToGet.aspnet_Users2)
                    {
                        IUser tempUser = getUser(user.UserId);

                        if (!string.IsNullOrEmpty(tempUser.userName))
                        {
                            returnList.Add(tempUser);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }

            return returnList;
        }

        /// <summary>
        /// Gets the report to users.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public List<IUser> getReportToUsers(Guid userId)
        {
            List<IUser> returnList = new List<IUser>();

            try
            {
                aspnet_Users userToGet = _dataProvider.DataContext.aspnet_Users.SingleOrDefault(u => u.UserId == userId);

                if (userToGet != null)
                {
                    foreach (aspnet_Users user in userToGet.aspnet_Users1)
                    {
                        IUser tempUser = getUser(user.UserId);

                        if (!string.IsNullOrEmpty(tempUser.userName))
                        {
                            returnList.Add(getUser(user.UserId));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }

            return returnList;
        }

        /// <summary>
        /// Gets the missing fuel forms.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public List<IUser> getMissingFuelForms(Guid userId)
        {
            try
            {
                List<IUser> users = getUserCommandChain(userId);
                List<IUser> returnUsers = new List<IUser>();

                foreach (IUser user in users)
                {
                    if (!string.IsNullOrEmpty(user.userName) && _fuelFormRepository.getFuelForm(user.userId, DateTime.Now) == null)
                    {
                        returnUsers.Add(user);
                    }
                }

                // Order the users by description and username
                returnUsers.Sort(delegate (IUser user1, IUser user2)
                {
                    string userName1 = (!string.IsNullOrEmpty(user1.description) ? user1.description : user1.userName).Trim();
                    string userName2 = (!string.IsNullOrEmpty(user2.description) ? user2.description : user2.userName).Trim();

                    return userName1.CompareTo(userName2);
                });

                return returnUsers;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Gets the sorted users.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="direction">The direction.</param>
        /// <returns></returns>
        public List<IUser> getSortedUsers(string field, Enums.SortDirection direction)
        {
            try
            {
                List<IUser> unSortedUsers = getAllUsers(false);
                List<IUser> sortedUsers;

                if (direction == Enums.SortDirection.Descending)
                {
                    sortedUsers = (from IUser in unSortedUsers
                                   orderby orderBy(field, IUser) descending
                                   select IUser).ToList();
                }
                else
                {
                    sortedUsers = (from IUser in unSortedUsers
                                   orderby orderBy(field, IUser) ascending
                                   select IUser).ToList();
                }

                return sortedUsers;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        #endregion

        #region Add

        /// <summary>
        /// Adds the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <param name="desciption">The desciption.</param>
        /// <param name="email">The email.</param>
        /// <param name="roles">The roles.</param>
        /// <param name="isDistribution">if set to <c>true</c> [is distribution].</param>
        /// <param name="isEmail">if set to <c>true</c> [is email].</param>
        /// <param name="isActive">if set to <c>true</c> [is active].</param>
        /// <returns></returns>
        public MembershipCreateStatus addUser(string userName, string password, string desciption, string address, string phoneNumber, string borough, string poDisplayName, string district, string email, List<IRole> roles, bool isDistribution, bool isEmail, bool isActive)
        {
            try
            {
                MembershipCreateStatus status = MembershipCreateStatus.UserRejected;

                _userProvider.CreateUser(userName, password, email, null, null, isActive, null, out status);

                if (status == MembershipCreateStatus.Success)
                {
                    if (roles.Count > 0)
                    {
                        string[] roleArray = new string[roles.Count];

                        for (int i = 0; i < roles.Count; i++)
                        {
                            roleArray[i] = roles[i].roleName;
                        }

                        _roleRepository.addUsersToRoles(new string[1] { userName }, roleArray);
                    }

                    ProfileBase p = ProfileBase.Create(userName, true);
                    p.SetPropertyValue("Description", desciption);
                    p.SetPropertyValue("Address", address);
                    p.SetPropertyValue("PhoneNumber", phoneNumber);
                    p.SetPropertyValue("Borough", borough);
                    p.SetPropertyValue("PODisplayName", poDisplayName);
                    p.SetPropertyValue("District", district);
                    p.SetPropertyValue("IsDistribution", isDistribution);
                    p.SetPropertyValue("IsEmail", isEmail);
                    p.Save();
                }

                return status;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Adds the user to role.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="roleName">Name of the role.</param>
        public void addUserToRole(string userName, string roleName)
        {
            try
            {
                _roleRepository.addUsersToRoles(new string[1] { userName }, new string[1] { roleName });
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Adds the user to command chain.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="reportToId">The report to id.</param>
        public void addUserToCommandChain(Guid userId, Guid reportToId)
        {
            try
            {
                aspnet_Users updateUser = _dataProvider.DataContext.aspnet_Users.SingleOrDefault(u => u.UserId == userId);
                aspnet_Users reportToUser = _dataProvider.DataContext.aspnet_Users.SingleOrDefault(u => u.UserId == reportToId);

                if (updateUser != null && reportToUser != null)
                    updateUser.aspnet_Users2.Add(reportToUser);

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates the user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public bool updateUser(IUser user)
        {
            try
            {
                MembershipUser updateUser = convertIUserToMembershipUser(user);

                // Update the user properties
                _userProvider.UpdateUser(updateUser);

                if (user.isActive && updateUser.IsLockedOut) { _userProvider.UnlockUser(user.userName); }

                // Remove user from all he/she is in roles and then add back to the specified role
                string[] currentRoles = convertRolesListToStringArray(_roleRepository.getRolesForUser(user.userName));

                if (currentRoles.Length > 0)
                    _roleRepository.deleteUsersFromRoles(new string[1] { user.userName }, currentRoles);

                if (user.roles.Count > 0)
                    _roleRepository.addUsersToRoles(new string[1] { user.userName }, convertRolesListToStringArray(user.roles));

                ProfileBase profile = ProfileBase.Create(user.userName);

                if (profile == null)
                    profile = ProfileBase.Create(user.userName, true);

                profile.SetPropertyValue("Description", user.description);
                profile.SetPropertyValue("Address", user.address);
                profile.SetPropertyValue("PhoneNumber", user.phoneNumber);
                profile.SetPropertyValue("Borough", user.borough);
                profile.SetPropertyValue("PODisplayName", user.poDisplayName);
                profile.SetPropertyValue("District", user.district);
                profile.SetPropertyValue("IsDistribution", user.isDistribution);
                profile.SetPropertyValue("IsEmail", user.isEmail);
                profile.Save();

                return true;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Updates the message dist for user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="inDist">if set to <c>true</c> [in dist].</param>
        public void updateMessageDistForUser(Guid userId, bool inDist)
        {
            try
            {
                aspnet_Users updateUser = _dataProvider.DataContext.aspnet_Users.SingleOrDefault(u => u.UserId == userId);
                ProfileBase profile = ProfileBase.Create(updateUser.UserName);

                if (profile == null)
                    profile = ProfileBase.Create(updateUser.UserName, true);

                profile.SetPropertyValue("IsDistribution", inDist);
                profile.Save();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Updates the email dist for user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="inDist">if set to <c>true</c> [in dist].</param>
        public void updateEmailDistForUser(Guid userId, bool inDist)
        {
            try
            {
                aspnet_Users updateUser = _dataProvider.DataContext.aspnet_Users.SingleOrDefault(u => u.UserId == userId);
                ProfileBase profile = ProfileBase.Create(updateUser.UserName);

                if (profile == null)
                    profile = ProfileBase.Create(updateUser.UserName, true);

                profile.SetPropertyValue("IsEmail", inDist);
                profile.Save();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="newPassword">The new password.</param>
        public void updatePassword(IUser user, string newPassword)
        {
            try
            {
                MembershipUser passwordUser = convertIUserToMembershipUser(user);

                passwordUser.ChangePassword(passwordUser.ResetPassword(), newPassword);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns></returns>
        public bool deleteUser(string userName)
        {
            try
            {
                IUser user = getUser(userName);

                return deleteUser(user);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Deletes the user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public bool deleteUser(IUser user)
        {
            try
            {
                string[] currentRoles = convertRolesListToStringArray(_roleRepository.getRolesForUser(user.userName));

                // Remove user from roles, profile and then delete the user
                if (currentRoles.Length > 0)
                    _roleRepository.deleteUsersFromRoles(new string[1] { user.userName }, currentRoles);

                ProfileManager.DeleteProfile(user.userName);
                deleteUserFromCommandChain(user.userId);
                deleteUserFromEquipment(user.userId);
                deleteUserFromProducts(user.userId);
                //deleteUserFromSentMessages(user.userId);

                return _userProvider.DeleteUser(user.userName, false);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Deletes the user from role.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="roleName">Name of the role.</param>
        public void deleteUserFromRole(string userName, string roleName)
        {
            try
            {
                _roleRepository.deleteUsersFromRoles(new string[1] { userName }, new string[1] { roleName });
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Deletes the user from command chain.
        /// </summary>
        /// <param name="userId">The user id.</param>
        public void deleteUserFromCommandChain(Guid userId)
        {
            try
            {
                aspnet_Users updateUser = _dataProvider.DataContext.aspnet_Users.SingleOrDefault(u => u.UserId == userId);

                if (updateUser != null)
                {
                    updateUser.aspnet_Users2.Clear();
                }

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Deletes the user from equipment.
        /// </summary>
        /// <param name="userId">The user id.</param>
        public void deleteUserFromEquipment(Guid userId)
        {
            try
            {
                aspnet_Users updateUser = _dataProvider.DataContext.aspnet_Users.SingleOrDefault(u => u.UserId == userId);

                if (updateUser != null)
                {
                    updateUser.Equipment_User.Clear();
                }

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Deletes the user from products.
        /// </summary>
        /// <param name="userId">The user id.</param>
        public void deleteUserFromProducts(Guid userId)
        {
            try
            {
                aspnet_Users updateUser = _dataProvider.DataContext.aspnet_Users.SingleOrDefault(u => u.UserId == userId);

                if (updateUser != null)
                {
                    updateUser.Product_User.Clear();
                }

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        /// <summary>
        /// Deletes a user from the sent_messages table.
        /// </summary>
        /// <param name="userId"></param>
        public void deleteUserFromSentMessages(Guid userId)
        {
            try
            {
                aspnet_Users updateUser = _dataProvider.DataContext.aspnet_Users.SingleOrDefault(u => u.UserId == userId);

                if (updateUser != null)
                {
                    updateUser.Sent_Message.Clear();
                }

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Converts the membership user to I user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        private IUser convertMembershipUserToIUser(MembershipUser user)
        {
            try
            {
                IUser newUser = new User();
                ProfileBase profile = ProfileBase.Create(user.UserName);

                newUser.userId = (Guid)user.ProviderUserKey;
                newUser.description = profile["Description"] != null ? profile["Description"].ToString() : string.Empty;
                newUser.address = profile["Address"] != null ? profile["Address"].ToString() : string.Empty;
                newUser.phoneNumber = profile["PhoneNumber"] != null ? profile["PhoneNumber"].ToString() : string.Empty;
                newUser.borough = profile["Borough"] != null ? profile["Borough"].ToString() : string.Empty;
                newUser.poDisplayName = profile["PODisplayName"] != null ? profile["PODisplayName"].ToString() : string.Empty;
                newUser.district= profile["District"] != null ? profile["District"].ToString() : string.Empty;
                newUser.userName = user.UserName;
                newUser.email = user.Email;
                newUser.roles = _roleRepository.getRolesForUser(newUser.userName);
                newUser.isDistribution = profile["IsDistribution"] != null ? (bool)profile["IsDistribution"] : false;
                newUser.isEmail = profile["IsEmail"] != null ? (bool)profile["IsEmail"] : false;
                newUser.isActive = user.IsApproved && !user.IsLockedOut;
                newUser.lastLogin = user.LastLoginDate;
                newUser.isLocked = user.IsLockedOut;

                return newUser;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Converts the I user to membership user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        private MembershipUser convertIUserToMembershipUser(IUser user)
        {
            try
            {
                MembershipUser newUser = _userProvider.GetUser(user.userName, false);

                newUser.Email = user.email;
                newUser.IsApproved = user.isActive;

                return newUser;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        /// <summary>
        /// Converts the roles list to string array.
        /// </summary>
        /// <param name="roles">The roles.</param>
        /// <returns></returns>
        private string[] convertRolesListToStringArray(List<IRole> roles)
        {
            try
            {
                string[] stringArrayRoles = null;

                if (roles.Count > 0)
                {
                    stringArrayRoles = new string[roles.Count];

                    for (int i = 0; i < roles.Count; i++)
                    {
                        stringArrayRoles[i] = roles[i].roleName;
                    }
                }

                return stringArrayRoles == null ? new string[0] : stringArrayRoles;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw(ex);
            }
        }

        //<summary>
        // Special function to import users and roles from a table to the ASP.Net user tables
        //</summary>
        //public void importUsers()
        //{
        //    bool success = false;
        //    int userCount = 0, iBatch = 0;

        //try
        //{
        //    foreach (aspnet_Users user in _dataProvider.DataContext.aspnet_Users)
        //    {
        //            if (user.UserName != "superadmin")
        //            {
        //                deleteUserFromCommandChain(user.UserId);
        //                success = deleteUser(getUser(user.UserId));
        //            }

        //    }
        //}

        //foreach (User_Import user in _dataProvider.DataContext.User_Import)
        //{
        //    try
        //    {
        //        if (user.description.ToLower().Contains("garage"))
        //        {
        //            addUserToRole(user.UserName, "Garage");
        //        }
        //        else
        //        {
        //            switch (user.role.ToLower())
        //            {
        //                case "field":
        //                    addUserToRole(user.UserName, "Field");
        //                    break;

        //                case "admin":
        //                    addUserToRole(user.UserName, "Administrator");
        //                    break;

        //                case "superadmin":
        //                    addUserToRole(user.UserName, "System Administrator");
        //                    break;

        //                default:

        //                    break;

        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //
        //    }
        //}

        //foreach (aspnet_Users user in _dataProvider.DataContext.aspnet_Users)
        //{
        //    foreach (Message message in _dataProvider.DataContext.Messages)
        //    {
        //        _dataProvider.DataContext.Sent_Message.AddObject(new Sent_Message() { UserId = user.UserId, Message_Id = message.Message_Id });
        //        _dataProvider.DataContext.SaveChanges();

        //        iBatch++;
        //        if (iBatch % 25 == 0)
        //        {
        //            _dataProvider.DataContext.SaveChanges();
        //            iBatch = 0;
        //        }
        //    }

        //    _dataProvider.DataContext.SaveChanges();
        //}

        //}

        #endregion

        #region Sort

        // Our sorter object, holds the field key and object type
        private object orderBy(string sortKey, IUser user)
        {
            switch (sortKey)
            {
                case "UserName":
                    return user.userName;

                case "Description":
                    return user.description;

                case "Email":
                    return user.email;

                case "IsActive":
                    return user.isActive;

                case "IsDistribution":
                    return user.isDistribution;

                case "IsEmail":
                    return user.isEmail;

                case "LastLogin":
                    return user.lastLogin;

                default:
                    return user.userName;
            }
        }

        #endregion
    }
}