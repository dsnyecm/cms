﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.Profile;

using DSNY.Core.Interfaces;

namespace DSNY.Core.Models
{
    /// <summary>
    /// An implementation of the <see cref="IUser"/> interface
    /// </summary>
    public class DSNYException : IDSNYException
    {
        public Guid userId { get; set; }
        public string exception { get; set; }
        public int exceptionId { get; set; }
        public DateTime date { get; set; }
        public int categoryId { get; set; }
        public string categoryName { get; set; }
        public string categoryCode { get; set; }
        public string tableName { get; set; }
        public int tableId { get; set; }
    }
}
