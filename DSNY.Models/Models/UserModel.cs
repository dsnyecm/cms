﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.Profile;

using DSNY.Core.Interfaces;

namespace DSNY.Core.Models
{
    /// <summary>
    /// An implementation of the <see cref="IUser"/> interface
    /// </summary>
    public class User : IUser
    {
        public Guid userId { get; set; }
        public string description { get; set; }
        public string address { get; set; }
        public string borough { get; set; }
        public string phoneNumber { get; set; }
        public string poDisplayName { get; set; }
        public string district { get; set; }
        public string userName { get; set; }
        public string email { get; set; }
        public List<IRole> roles { get; set; }
        public bool isActive { get; set; }
        public bool isLocked { get; set; }
        public bool isDistribution { get; set; }
        public bool isEmail { get; set; }
        public DateTime lastLogin { get; set; }
        public string fullName { get { return !string.IsNullOrEmpty(description) ? description.Trim() + " (" + userName.Trim() + ")" : userName.Trim(); } set { fullName = value; } }
    }
}
