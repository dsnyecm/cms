﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DSNY.Core.Interfaces;

namespace DSNY.Core.Models
{
    /// <summary>
    /// An implementation of the <see cref="IRole"/> interface
    /// </summary>
    public class Role : IRole
    {
        public string roleName { get; set; }
        public List<string> usersInRole { get; set; }
    }
}
