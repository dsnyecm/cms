﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Printing;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

using DSNY.Common.Exception;
using Microsoft.Practices.Unity;

namespace DSNY.Common.Printing
{
    /// <summary>
    /// Custom print document class to send a message to a printer
    /// </summary>
    public class PrintCustomDocument : PrintDocument
    {
        /// <summary>
        /// Static variable to hold the current character
        /// we're currently dealing with.
        /// </summary>
        static int curChar;

        public string textToPrint { get; set; }
        public Font printFont { get; set; }
        private IUnityContainer container = new UnityContainer();
        private IExceptionHandler _exceptionHandler;

        #region  Class Constructors

        public PrintCustomDocument() : this("I am dummy text.", null)
        {
            _exceptionHandler = container.Resolve<IExceptionHandler>();
        }

        public PrintCustomDocument(string incomingText)
        {
            textToPrint = incomingText;
            _exceptionHandler = container.Resolve<IExceptionHandler>();
        }

        public PrintCustomDocument(string incomingText, IExceptionHandler IoCExceptionHandler)
        {
            textToPrint = incomingText;
            _exceptionHandler = IoCExceptionHandler ?? container.Resolve<IExceptionHandler>();
        }

        #endregion

        /// <summary>
        /// Override the default onbeginPrint method of the PrintDocument Object
        /// </summary>
        /// <param name=e></param>
        /// <remarks></remarks>
        protected override void OnBeginPrint(System.Drawing.Printing.PrintEventArgs e)
        {
            // Run base code
            base.OnBeginPrint(e);

            // read the configuration file and apply settings
            int fontSize = 10;
            if (! string.IsNullOrEmpty(ConfigurationManager.AppSettings["FontSize"]))
                int.TryParse(ConfigurationManager.AppSettings["FontSize"], out fontSize);

            bool printDuplex = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["PrintDuplex"]))
                bool.TryParse(ConfigurationManager.AppSettings["PrintDuplex"], out printDuplex);

            bool boldFont = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["FontBold"]))
                bool.TryParse(ConfigurationManager.AppSettings["FontBold"], out boldFont);

            // enables duplex printing if set
            if (printDuplex)
                this.PrinterSettings.Duplex = Duplex.Vertical;

            // sets font to monospace, font size and if bolded
            if (boldFont)
                printFont = new Font(FontFamily.GenericMonospace, fontSize, FontStyle.Bold);
            else
                printFont = new Font(FontFamily.GenericMonospace, fontSize);
        }

        /// <summary>
        /// Override the default OnPrintPage method of the PrintDocument
        /// </summary>
        /// <param name=e></param>
        /// <remarks>This provides the print logic for our document</remarks>
        protected override void OnPrintPage(System.Drawing.Printing.PrintPageEventArgs e)
        {
            // Run base code
            base.OnPrintPage(e);

            //Declare local variables needed and set print area size and margins
            int printHeight = 1000;
            int printWidth = 775;
            int leftMargin = 25;
            int rightMargin = 25;
            Int32 lines, chars;
            
            try
            {
                int.TryParse(ConfigurationManager.AppSettings["printHeight"], out printHeight);
                int.TryParse(ConfigurationManager.AppSettings["printWidth"], out printWidth);
                int.TryParse(ConfigurationManager.AppSettings["leftMargin"], out leftMargin);
                int.TryParse(ConfigurationManager.AppSettings["rightMargin"], out rightMargin);
            }
            catch (System.Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }

            //Check if the user selected to print in Landscape mode if they did then we need to swap height/width parameters
            if (base.DefaultPageSettings.Landscape)
            {
                int tmp;
                tmp = printHeight;
                printHeight = printWidth;
                printWidth = tmp;
            }

            //Now we need to determine the total number of lines we're going to be printing
            Int32 numLines = (int)printHeight / printFont.Height;

            //Create a rectangle printing are for our document
            RectangleF printArea = new RectangleF(leftMargin, rightMargin, printWidth, printHeight);

            //Use the StringFormat class for the text layout of our document
            StringFormat format = new StringFormat(StringFormatFlags.LineLimit);

            float[] tabStops = { 75.0f };

            format.SetTabStops(0.0f, tabStops);            

            //Fit as many characters as we can into the print area
            e.Graphics.MeasureString(textToPrint.Substring(curChar), printFont, new SizeF(printWidth, printHeight), format, out chars, out lines);

            //Print the page
            e.Graphics.DrawString(textToPrint.Substring(curChar), printFont, Brushes.Black, printArea, format);

            //Increase current char count
            curChar += chars;

            //Detemine if there is more text to print, if there is the tell the printer there is more coming
            if (curChar + 1 < textToPrint.Length)
                e.HasMorePages = true;
            else
            {
                e.HasMorePages = false;
                curChar = 0;
            }
        }
    }
}