﻿using SelectPdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DSNY.Common.Utilities
{
   public class PDFUtility
    {
        public static byte[] ConvertHTMLToPdf(string html)
        {
            var converter = new HtmlToPdf();

            // set links options
            converter.Options.InternalLinksEnabled = true;
            converter.Options.ExternalLinksEnabled = true;

            // create a new pdf document converting an url
            var pdfDoc = converter.ConvertHtmlString(html);
            try
            {

                // save pdf document
                var pdfStream = new MemoryStream();
                pdfDoc.Save(pdfStream);

                pdfStream.Position = 0;
                return pdfStream.ToArray();
            }
            finally
            {
                pdfDoc.Close();
            }
        }
    }
}
