﻿using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace DSNY.Common.Utilities
{
    /// <summary>
    /// Class to run a check on an email address
    /// </summary>
    public static class EmailUtilities
    {
        /// <summary>
        /// Performs a check on an email address with a regular expression
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public static bool SendEmail(MailMessage email)
        {
            bool bSendEmails = false;
            if (!bool.TryParse(ConfigurationManager.AppSettings["SendEmails"], out bSendEmails)) { bSendEmails = false; }
            if (!bSendEmails) return true;
            string mailServer = ConfigurationManager.AppSettings["Mail Server"];
            string mailUser = ConfigurationManager.AppSettings["Mail User"];
            string mailPassword = ConfigurationManager.AppSettings["Mail Password"];
            string mailFrom = ConfigurationManager.AppSettings["Mail From Address"];
            bool useSSL = false;
            int mailPort = 25;

            bool.TryParse(ConfigurationManager.AppSettings["Mail SSL"], out useSSL);
            int.TryParse(ConfigurationManager.AppSettings["Mail Port"], out mailPort);

            if (!string.IsNullOrEmpty(mailServer))
            {
                SmtpClient sc = new SmtpClient(mailServer);

                if (!string.IsNullOrEmpty(mailUser) && !string.IsNullOrEmpty(mailPassword))
                {
                    NetworkCredential nc = new NetworkCredential(mailUser, mailPassword);
                    sc.UseDefaultCredentials = false;
                    sc.Credentials = nc;
                }
                else
                    sc.UseDefaultCredentials = true;

                sc.EnableSsl = useSSL;
                sc.Port = mailPort;

                try
                {
                    if (email.From == null)
                    {
                        email.From = new MailAddress(mailFrom, "DSNY CMS");
                    }

                    sc.Send(email);    // send mail message
                    email.Dispose();   // dispose of our objects

                    return true;
                }
                catch (SmtpFailedRecipientException ex)
                {
                    string policy = ConfigurationManager.AppSettings["Exception Policy Group"];

                    Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.ExceptionPolicy.HandleException(ex, policy);
                }
            }

            return false;
        }
    }
}
