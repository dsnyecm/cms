﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DSNY.Common.Utilities
{
    /// <summary>
    /// Methods to handle getting and setting 
    /// </summary>
    class ConfigUtilities
    {
        /// <summary>
        /// Using reflection, get assembly direction location and format correct
        /// </summary>
        /// <param name="assemblyPath"></param>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        public static string getPathToConfigDir(string assemblyPath, string assemblyName)
        {
            return Path.GetDirectoryName(assemblyPath).Replace(assemblyName, string.Empty).Replace("file:\\", string.Empty);
        }
    }
}
