﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using Microsoft.CSharp;
using System.CodeDom;

namespace DSNY.Common.Utilities
{
    /// <summary>
    /// Utilities for handling string operations
    /// </summary>
    public static class TextUtilities
    {

        /// <summary>
        /// Cleans a formatted messages by removing extra new lines and carriage returns
        /// </summary>
        /// <param name="msgText"></param>
        /// <returns></returns>
        public static string cleanMessage(string msgText)
        {
            if (msgText.Contains("\n\r "))
            {
                // this means we have 'reversed' \n\r, which should really be \r\n, after the first replace, 
                // then replace all 3 new lines in a row with only 2 new lines in a row.
                msgText = msgText.Replace("\n\r", "\r\n").Replace("\r\n\r\n\r\n", "\r\n\r\n");
            }
            else
            {
                if (msgText.Contains("\n\n\n"))
                {
                    // sometimes a \n\n\n is encountered, which really is 2 new lines, as well, \r\n\n\n is encountered,
                    // which is replaces with 2 new lines, lastly, \n\n is 1 new line
                    msgText = msgText.Replace("\n\n\n", "\r\n\r\n").Replace("\r\n\n\n", "\r\n\r\n").Replace("\n\n", "\r\n");
                }
                else
                {
                    // removes one extra new line
                    msgText = msgText.Replace("\r\n\r\n", "\r\n");
                }
            }

            return msgText;
        }

        /// <summary>
        /// Strips HTML from a text string
        /// </summary>
        /// <param name="HTMLtext"></param>
        /// <returns></returns>
        public static string stripTinyMCEHTML(string HTMLtext)
        {
            string tempText = HttpUtility.HtmlDecode(HTMLtext);
            List<string> wrapText;
            bool resent = false;

            // replace the paragraph end tag with carriage return, then remove all html from string through regular expression
            if (tempText.Contains("<pre style=\"white-space: pre-wrap; word-wrap: break-word;\">"))
            {
                resent = true;
            }

            tempText = tempText.Replace("<p>&nbsp;</p>", string.Empty).Replace("</p>", "\r\n\r\n")
                        .Replace("<br />", "\r\n\r\n").Replace("&nbsp;", string.Empty);

            tempText = Regex.Replace(tempText, @"</?\w+((\s+\w+(\s*=\s*(?:"".*?""|'.*?'|[^'"">\s]+))?)+\s*|\s*)/?>", string.Empty);
            
            // now break the words on every 100 characters to get it to wrap correctly
            wrapText = WordWrap(tempText, 80, resent);

            tempText = string.Empty;

            foreach (string row in wrapText)
            {
                if (row.Length == 0)
                {
                    tempText += "\r\n";
                }
                else
                {
                    tempText += row;
                }
            }

            return tempText;
        }

        /// <summary>
        /// Adds word wrapping to text after each number of characters from 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="maxLineLength">Max number of characters per each line</param>
        /// <returns></returns>
        public static List<string> WordWrap(string text, int maxLineLength, bool resent)
        {
            var list = new List<string>();

            int currentIndex, lastNewline = 0;
            var lastWrap = 0;
            var whitespace = new[] { ' ', '\t' };
            string newLineText = string.Empty;

            do
            {
                // get the current position of the string we're working on
                currentIndex = lastWrap + maxLineLength > text.Length ? text.Length :
                    (text.LastIndexOfAny(new[] { ' ', ',', '.', '?', '!', ':', ';', '-', '\n', '\r', '\t' }, Math.Min(text.Length - 1, lastWrap + maxLineLength)) + 1);
                
                // if we haven't passed the last place we worked on, then go for next line
                if (currentIndex <= lastWrap)
                    currentIndex = Math.Min(lastWrap + maxLineLength, text.Length);

                if (text.Substring(lastWrap, currentIndex - lastWrap).Contains("\r\n"))
                {
                    // if we find one next line group
                    newLineText = text.Substring(lastWrap, currentIndex - lastWrap);
                    lastNewline = newLineText.IndexOf("\r\n") + 2;
                    if (resent)
                    {
                        list.Add(text.Substring(lastWrap, newLineText.IndexOf("\r\n")).Trim(whitespace) + "\r\n");
                    }
                    else
                    {
                        list.Add(text.Substring(lastWrap, newLineText.IndexOf("\r\n")).Trim(whitespace));
                    }
                    currentIndex = lastWrap + lastNewline;
                }
                else
                    list.Add(text.Substring(lastWrap, currentIndex - lastWrap).Trim(whitespace));
                
                lastWrap = currentIndex;
            } while (currentIndex < text.Length);

            return list;
        }
    }
}