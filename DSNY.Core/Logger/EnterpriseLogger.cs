﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

using DSNY.Common.Utilities;

namespace DSNY.Common.Logger
{
    /// <summary>
    /// Microsoft Enterprise Logging class which implements ILogger.
    /// </summary>
    public class EnterpriseLogger : ILogger
    {
        private LogWriter writer;

        /// <summary>
        /// Constructor which grabs config from configuration file and creates a logger insance
        /// </summary>
        public EnterpriseLogger()
        {            
            // Get path to the Core.App.Config File
            string path = ConfigUtilities.getPathToConfigDir(this.GetType().Assembly.CodeBase, this.GetType().Assembly.FullName);
            string configFilename = path + @"\\Common.Config";

            IConfigurationSource configSource = new FileConfigurationSource(configFilename);
            EnterpriseLibraryContainer.Current = EnterpriseLibraryContainer.CreateDefaultContainer(configSource);

            writer = EnterpriseLibraryContainer.Current.GetInstance<LogWriter>();
        }

        /// <summary>
        /// Log an info message
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="priority"></param>
        public void LogInfo(string title, string message, int priority)
        {
            LogEntry logEntry = new LogEntry() { Title = title, Message = message, Priority = priority, Severity = TraceEventType.Information};
            writer.Write(logEntry);
        }

        /// <summary>
        /// Log a warn message
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="priority"></param>
        public void LogWarn(string title, string message, int priority)
        {
            LogEntry logEntry = new LogEntry() { Title = title, Message = message, Priority = priority, Severity = TraceEventType.Warning };
            writer.Write(logEntry);
        }

        /// <summary>
        /// Log an error message
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="priority"></param>
        public void LogError(string title, string message, int priority)
        {
            LogEntry logEntry = new LogEntry() { Title = title, Message = message, Priority = priority, Severity = TraceEventType.Error };
            writer.Write(logEntry);
        }
    }
}