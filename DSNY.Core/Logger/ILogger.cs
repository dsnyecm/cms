﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace DSNY.Common.Logger
{
    public interface ILogger
    {
        void LogInfo(string title, string message, int priority);

        void LogWarn(string title, string message, int priority);

        void LogError(string title, string message, int priority);
    }
}