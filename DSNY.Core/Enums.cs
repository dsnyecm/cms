﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.Common
{
    /// <summary>
    /// Contains any enums used throughout application
    /// </summary>
    public class Enums
    {
        public enum SortDirection { 
            Ascending, Descending 
        };
    }
}
