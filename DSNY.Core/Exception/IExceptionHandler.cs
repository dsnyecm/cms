﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.Common.Exception
{
    public interface IExceptionHandler
    {
        bool HandleException(System.Exception ex);

        bool HandleException(System.Exception ex, string policy);
    }
}
