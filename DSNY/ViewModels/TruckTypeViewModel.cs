using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class TruckTypeViewModel
    {
        public TruckType BlankTruckType { get; set; }
        public List<TruckType> TruckTypes { get; set; }
        public string Json { get; set; }
    }
}
