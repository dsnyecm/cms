﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class SiteOnHand
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Measurement { get; set; }
        public bool Active { get; set; }
        public string Qty { get; set; }
        public int? Capacity { get; set; }
        public bool Deleted { get; set; }
    }
}