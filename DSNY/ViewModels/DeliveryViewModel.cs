﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class DeliveryViewModel
    {
        public List<StcProduct> Products { get; set; }
        public StcProduct selectedProduct { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<Dropdown> Boroughs { get; set; }
        public int? selectedBorough { get; set; }
        public Dictionary<string, List<Dropdown>> Sites { get; set; }
        public string selectedSite { get; set; }
        public List<StcPurchaseOrder> PurchaseOrders { get; set; }
        public bool Finalized { get; set; }
        public bool Addable { get; set; }
        public bool Finalizable { get; set; }
        public Delivery BlankDelivery { get; set; }
        public List<Delivery> Deliveries { get; set; }
        public bool Restricted { get; set; }
        public string Message { get; set; }
        public int ErrorCode { get; set; }
        public string Json { get; set; }
    }
}