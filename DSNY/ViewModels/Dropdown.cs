using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.Core.Interfaces;
using DSNY.Data.Validators;
using DSNY.Data;
using DSNY.Models;

namespace DSNY.ViewModels
{
    public class Dropdown
    {
        public string id { get; set; }
        public string description { get; set; }
        public bool visible { get; set; } = false;
    }
}