﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class RadioHistoryViewModel
    {
        public string Status { get; set; }
        public string SerialNumber { get; set; }
        public string DSNYRadioId { get; set; }
        public string TrunkId { get; set; }
        public string Location { get; set; }
        public string EquipmentType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string VehicleNumber { get; set; }
        public string InventoryDate { get; set; }
        public string ServiceDate { get; set; }
        public bool IsSpare { get; set; }
        public bool IsActive { get; set; }
        public string Comments { get; set; }
        public string ChangeDate { get; set; }
    }
}