﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.Core.Interfaces;
using DSNY.Data.Validators;
using DSNY.Data;
using DSNY.Models;

namespace DSNY.ViewModels
{
    public class ZoneViewModel
    {
        public List<Dropdown> Sites { get; set; }
        public List<int> selectedSites { get; set; }
        public List<Dropdown> CurrentZoneSites { get; set; }
        public List<int> currentZoneSelectedSites { get; set; }
        public List<Zone> Zones { get; set; }
        public int selectedZone { get; set; }
        public int previousZone { get; set; }
        public string Json { get; set; }
    }
}