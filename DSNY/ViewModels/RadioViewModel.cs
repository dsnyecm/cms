﻿using DSNY.Core.Interfaces;
using DSNY.Core.Models;
using DSNY.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class RadioViewModel
    {
       // public List<Borough> Boroughs { get; set; }
        public List<Dropdown> Boroughs { get; set; }
        public List<IUser> Locations { get; set; }
        public List<Dropdown> Statuses { get; set; }
        public List<Dropdown> EquipmentTypes { get; set; }
        public List<Dropdown> Makes { get; set; }
        public List<Dropdown> Models { get; set; }
        public bool ShowInActive { get; set; }
        public List<RadioItemViewModel> Radios { get; set; }
        public List<RadioAccessoryViewModel> RadioAccessories { get; set; }

        public RadioItemViewModel BlankRadio { get; set; } = new RadioItemViewModel() { IsActive = true };
        public RadioAccessoryViewModel BlankRadioAccessory { get; set; } = new RadioAccessoryViewModel();

        public string Json { get; set; }
    }

    public class RadioItemViewModel
    {
        public bool Selected { get; set; } // to monitor if this radio is selected on the UI using a checkbox
        public int Id { get; set; }
        public string SerialNumber { get; set; }
        public string DSNYRadioId { get; set; }
        public string TrunkId { get; set; }
        public Guid? IssueToUserId { get; set; }
        public int? RadioEquipmentTypeId { get; set; }
        public int? RadioMakeId { get; set; }
        public int? RadioModelId { get; set; }
        public string AssignedToVehicle { get; set; }
        public string InventoryDate { get; set; }
        public string ServiceDate { get; set; }
        public bool IsSpare { get; set; }
        public bool IsActive { get; set; }
        public int? StatusId { get; set; }
        public int? OriginalStatusId { get { return this.StatusId; } } // This is to keep track what was the value of Status Id when Page was loaded.. 
        public int? DistInvStatusId { get; set; }
        public int? LastDistInvStatusId { get; set; }
        public string Comments { get; set; }
        public bool IsDeleted { get; set; }
        public List<Dropdown> Models { get; set; } = new List<Dropdown>(); // List of models of make that this Radio is
        public string DistrictInventoryStatus { get; set; }
        public string LastDistInventoryDate { get; set; }
        public string Borough { get; set; }
        public bool   IsInventoryRequested { get; set; }
        public DateTime? InventoryRequestDate { get; set; }
        public bool IsDistrictInventoryDateOlderThanRequestDate
        {
            get
            {
                return InventoryRequestDate != null && !string.IsNullOrEmpty(LastDistInventoryDate) && InventoryRequestDate.Value.Date > DateTime.Parse(LastDistInventoryDate);                
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            var radioItem = obj as RadioItemViewModel;
            if (radioItem == null)
                return false;

            if (this.SerialNumber != radioItem.SerialNumber ||
               this.DSNYRadioId != radioItem.DSNYRadioId ||
               this.TrunkId != radioItem.TrunkId ||
               this.IssueToUserId != radioItem.IssueToUserId ||
               this.RadioEquipmentTypeId != radioItem.RadioEquipmentTypeId ||
               this.RadioMakeId != radioItem.RadioMakeId ||
               this.RadioModelId != radioItem.RadioModelId ||
               this.AssignedToVehicle != radioItem.AssignedToVehicle ||
               this.InventoryDate != radioItem.InventoryDate ||
               this.ServiceDate != radioItem.ServiceDate ||
               this.IsSpare != radioItem.IsSpare ||
               this.IsActive != radioItem.IsActive ||
               this.StatusId != radioItem.StatusId ||
               this.LastDistInventoryDate != radioItem.LastDistInventoryDate ||
               this.LastDistInvStatusId != radioItem.LastDistInvStatusId)

                return false;
            else
                return true;

        }
    }


}