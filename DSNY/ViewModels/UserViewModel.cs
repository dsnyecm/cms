﻿using System;
using System.Collections.Generic;
using DSNY.Data;

namespace DSNY.ViewModels
{
    /// <summary>
    /// View model defining fields, names and validation for editing a user
    /// </summary>
    public class UserViewModel
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
    }
}