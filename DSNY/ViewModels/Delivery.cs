﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class Delivery
    {
        public int? Id { get; set; }
        public int ProductId { get; set; }
        public List<StcPurchaseOrder> PurchaseOrders { get; set; }
        public int selectedPurchaseOrder { get; set; }
        public int? VendorId { get; set; }
        public string TicketNumber { get; set; }
        public string TruckNumber { get; set; }
        public string PlateNumber { get; set; }
        public List<Dropdown> Sites { get; set; }
        public string selectedSite { get; set; }
        public string selectedSiteName { get; set; }
        public string DeliveryDate { get; set; }
        public string LeaveWeight { get; set; }
        public string LeaveTime { get; set; }
        public string ReceiveWeight { get; set; }
        public string ReceiveTime { get; set; }
        public string WeightMasterName { get; set; }
        public string WeightMasterBadge { get; set; }
        public string ReceiveSuperName { get; set; }
        public string ReceiveSuperBadge { get; set; }
        public string BoroSuperName { get; set; }
        public string BoroSuperBadge { get; set; }
        public bool Finalized { get; set; }
        public bool Edit { get; set; }
        public bool Deleted { get; set; }
        public bool NewEmail { get; set; }
        public bool UpdateEmail { get; set; }
    }
}