﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class RadioCodesViewModel
    {
        public RadioCodesViewModel()
        {
            EquipmentTypes = new List<EquipmentTypeModel>();
            RepairProblemType = new List<RepairProblemTypeModel>();
            EquipmentStatuses = new List<EquipmentStatusModel>();
        }
        public List<EquipmentTypeModel> EquipmentTypes { get; set; }
        public List<RepairProblemTypeModel> RepairProblemType { get; set; }
        public List<EquipmentStatusModel> EquipmentStatuses { get; set; }
        public List<RadioMakeModel> RadioMakes { get; set; } = new List<RadioMakeModel>();
        public RepairProblemTypeModel BlankRepairProblemType { get; set; } = new RepairProblemTypeModel {Type="" , IsDeleted = false, IsActive = true};
        public EquipmentStatusModel BlankEquipmentStatus { get; set; } = new EquipmentStatusModel { Name="" , IsDeleted = false };
        public EquipmentTypeModel BlankEquipmentType { get; set; } = new EquipmentTypeModel { Type = "" , IsDeleted = false, IsActive = true };
        public RadioMakeModel BlankMake { get; set; } = new RadioMakeModel { Name = "", IsDeleted = false, IsActive = true};
        public RadioModelVM BlankModel { get; set; } = new RadioModelVM { Name = "" , IsDeleted = false,IsActive = true };
        public string Json { get; set; }
    }


    public class RadioMakeModel
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int? ID { get; set; }
        public bool IsDeleted { get; set; }
        public List<RadioModelVM> Models { get; set; } = new List<RadioModelVM>();
        public RadioModelVM BlankModel { get; set; } = new RadioModelVM { Name = "" , IsDeleted = false, IsActive = true };
    }

    public class RadioModelVM
    {
        public string Name { get; set; }
        public bool IsActive { get; set; } 
        public int? MakeId { get; set; }
        public int? ID { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class EquipmentTypeModel
    {
        public string Type { get; set; }
        public bool DistrictInventory { get; set; }
        public bool IsActive { get; set; } 
        public int? ID { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class EquipmentStatusModel
    {
        public int? ID { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; } 
        public bool IsDeleted { get; set; }
        public bool WorkFlow { get; set; }
        public bool Inventory { get; set; }
    }
    public class RepairProblemTypeModel
    {
        public int? ID { get; set; }
        public bool IsActive { get; set; }
        public string Type { get; set; }
        public bool IsDeleted { get; set; }
    }
}