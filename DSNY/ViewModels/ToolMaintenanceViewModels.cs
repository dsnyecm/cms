﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

using DSNY.Core.Interfaces;
using DSNY.Data.Validators;
using DSNY.Data;
using DSNY.Models;

namespace DSNY.ViewModels
{
    #region View Models
    public class ToolMaintenanceViewModel
    {
        public List<MeasurementViewModel> measurements{ get; set; }
        public List<ToolTypeViewModel> toolTypes { get; set; }
        public List<StandardToolsInventoryViewModel> standardInventory { get; set; }
        public List<IUser> users { get; set; }
        public List<WorkflowActionViewModel> workflowActions { get; set; }
        public string Json { get; set; }
    }

    public class LocationToolsViewModel
    {
        public List<ToolUserViewModel> siteQuotas { get; set; }
        public List<PowerToolInventoryViewModel> powerToolInventory { get; set; }
    }

    public class ToolUserViewModel
    {
        public int Tool_User_Id { get; set; }
        public int Tool_Type_id { get; set; }
        public ToolTypeViewModel Tool_Type { get; set; }
        public Guid Userid { get; set; }
        public UserViewModel aspnet_Users { get; set; }
        public int Site_Quota { get; set; }
        public bool is_Active { get; set; }
    }

    public class ToolInventoryViewModel
    {
        public int Tool_Inventory_id { get; set; }
        public DateTime Inventory_Date { get; set; }
        public string Inventory_Instruction { get; set; }
    }

    public class ToolInventoryPowertoolViewModel
    {
        public int Tool_Inventory_PT_id { get; set; }
        public int Tool_Inventory_id { get; set; }
        public int Power_Tool_Inventory_id { get; set; }
        public bool is_Active { get; set; }
        public bool is_Down { get; set; }
        public bool is_Stolen { get; set; }
        public bool is_Dispose { get; set; }
    }

    public class ToolInventoryStandardViewModel
    {
        public int Tool_Inventory_Std_Id { get; set; }
        public int Tool_Inventory_id { get; set; }
        public int Tool_User_Id { get; set; }
        public int? Damaged_Qty { get; set; }
        public int? Lost_Qty { get; set; }
        public int? On_Hand_Qty { get; set; }
        public int? Issue_Qty { get; set; }
        public int? Boro_Approved_Qty { get; set; }
        public int? Boro_Shipped_Qty { get; set; }
    }

    public class ToolInventoryWarehouseViewModel
    {
        public int Tool_Inventory_WH_Id { get; set; }
        public int Tool_Inventory_id { get; set; }
        public Guid Userid { get; set; }
        public int? Requested_Qty { get; set; }
        public int? Approved_Qty { get; set; }
        public DateTime Approved_Datetime { get; set; }
        public ToolTypeViewModel Tool_Type { get; set; }
    }

    public class StandardToolsInventoryViewModel
    {
        public int Standard_Tools_Inventory_id { get; set; }
        public ToolUserViewModel Tool_User { get; set; }
        public int Current_On_Hand { get; set; }
        public DateTime Last_Update_Date { get; set; }
    }

    public class StandardToolsLedgerViewModel
    {
        public int Standard_Tools_Ledger_id { get; set; }
        public int Standard_Tools_Inventory_id { get; set; }
        public int Action_Id { get; set; }
        public int Qty { get; set; }
        public DateTime InsertDateTime { get; set; }
        public Guid InsertUserid { get; set; }
        public Guid Send_Userid { get; set; }
        public string Comment { get; set; }
    }

    public class PowerToolInventoryViewModel
    {
        public bool is_Active { get; set; }
        public int Power_Tool_Inventory_Id { get; set; }
        public int Tool_Type_id { get; set; }
        public ToolTypeViewModel Tool_Type { get; set; }
        public int Make_id { get; set; }
        public ToolTypeMakeViewModel Make { get; set; }
        public int Model_id { get; set; }
        public ToolTypeMakeModelViewModel Model { get; set; }
        public Guid Userid { get; set; }
        public UserViewModel aspnet_Users { get; set; }
        public string Serial_Number { get; set; }
        public string DSNY_Number { get; set; }
    }

    public class RoleViewModel
    {
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
    }

    public class InventorySubmissionViewModel
    {
        public Guid currentUserId { get; set; }
        public List<IUser> users { get; set; }
        public List<Guid> commandUsers { get; set; }
        public List<RoleViewModel> roles { get; set; }
        public List<WorkflowTrackingViewModel> workflows { get; set; }
        public List<WorkflowTrackingViewModel> workflowsHistory { get; set; }
        public List<StandardToolsInventoryViewModel> tools { get; set; }
        public List<ToolInventoryStandardViewModel> standardInventory { get; set; }
        public List<PowerToolInventoryViewModel> powertools { get; set; }
        public List<ToolInventoryPowertoolViewModel> powertoolInventory { get; set; }
        public List<ToolInventoryWarehouseViewModel> warehouseInventory { get; set; }
        public List<WorkflowActionRoleViewModel> workflowActionRoles { get; set; }
        public List<WorkflowActionFlowViewModel> workflowActionFlows { get; set; }
        public string Json { get; set; }
    }

    public class WorkflowActionViewModel
    {
        public int Workflow_Action_id { get; set; }
        public string Workflow_cd { get; set; }
        public string Workflow_Action_Name { get; set; }
        public bool is_End_Workflow { get; set; }
    }

    public class WorkflowActionFlowViewModel
    {
        public int Workflow_Action_Flow_id { get; set; }
        public int Workflow_id { get; set; }
        public WorkflowActionViewModel Workflow_Action { get; set; }
        public int Next_Workflow_id { get; set; }
        public WorkflowActionViewModel Workflow_Action1 { get; set; }
    }

    public class WorkflowActionRoleViewModel
    {
        public int Workflow_Action_Role_id { get; set; }
        public int Workflow_Action_id { get; set; }
        public WorkflowActionViewModel Workflow_Action { get; set; }
        public Guid Role_id { get; set; }
        public RoleViewModel aspnet_Roles { get; set; }
    }

    public class WorkflowTrackingViewModel
    {
        public int Workflow_Tracking_id { get; set; }
        public int Workflow_Action_id { get; set; }
        public WorkflowActionViewModel Workflow_Action { get; set; }
        public Guid Userid { get; set; }
        public string Comment{ get; set; }
        public DateTime Workflow_Datetime { get; set; }
        public string Table_Association { get; set; }
        public string Table_Id { get; set; }
    }

    public class WorkflowNextAction
    {
        public int Workflow_Tracking_id { get; set; }
        public string comment { get; set; }
        public Guid userId { get; set; }
        public int Workflow_Next_Action_id { get; set; }
    }
    #endregion

    #region Save Models
    public class InventoryRequestSaveModel
    {
        public DateTime inventoryDate { get; set; }
        public string additionalRequest { get; set; }
    }

    public class ToolMaintenanceSaveModel
    {
        public List<PowerToolInventoryViewModel> powerToolInventory { get; set; }
        public List<ToolUserViewModel> siteQuotas { get; set; }
        public List<WarehouseRecieveSaveModel> warehouseRecieved { get; set; }
        public List<WarehouseSendSaveModel> warehouseSent { get; set; }
    }

    public class WarehouseRecieveSaveModel
    {
        public int Standard_Tools_Inventory_id { get; set; }
        public int qty { get; set; }
        public string comment { get; set; }
    }

    public class WarehouseSendSaveModel
    {
        public int Standard_Tools_Inventory_id { get; set; }
        public int qty { get; set; }
        public string comment { get; set; }
        public Guid sentTo { get; set; }
    }

    public class InventorySubmissionSaveModel
    {
        public List<WorkflowNextAction> workflowActions { get; set; }
        public List<ToolInventoryStandardViewModel> standardInventory { get; set; }
        public List<ToolInventoryPowertoolViewModel> powertoolInventory { get; set; }
        public List<PowerToolInventoryUpdateModel> powertoolComments { get; set; }
        public List<ToolInventoryWarehouseViewModel> warehouseInventory { get; set; }
    }
    #endregion

    #region Update Models
    public class PowerToolInventoryUpdateModel
    {
        public int Power_Tool_Inventory_Id { get; set; }
        public Guid Userid { get; set; }
        public string Comment { get; set; }
    }
    #endregion
}