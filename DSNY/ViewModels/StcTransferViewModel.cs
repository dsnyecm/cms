using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class StcTransferViewModel
    {
        public List<StcProduct> Products { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<TruckType> TruckTypes { get; set; }
        public Dictionary<string, List<Dropdown>> Sites { get; set; }
        public string selectedSite { get; set; }
        public bool Finalized { get; set; }
        public StcTransfer BlankStcTransfer { get; set; }
        public List<StcTransfer> StcTransfers { get; set; }
        public bool Finalizable { get; set; }
        public string Json { get; set; }
    }
}
