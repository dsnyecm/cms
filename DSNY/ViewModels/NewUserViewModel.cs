﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

using DSNY.Core.Interfaces;
using DSNY.Data;
using DSNY.Data.Validators;
using DSNY.Models;

namespace DSNY.ViewModels
{
    /// <summary>
    /// View model defining fields, names and validation for creating a new user
    /// </summary>
    [PropertiesMustMatch("Password", "ConfirmPassword", ErrorMessage = "The password and confirmation password do not match.")]
    public class NewUserModel
    {
        [Required(ErrorMessage = "Please provide a user name")]
        [DisplayName("User name")]
        public string userName { get; set; }

        [DisplayName("Description")]
        public string description { get; set; }

        [DisplayName("Address")]
        public string address { get; set; }

        [DisplayName("Phone Number")]
        public string phoneNumber { get; set; }

        [DisplayName("Borough")]
        public string borough { get; set; }

        [DisplayName("PO Display Name")]
        public string poDisplayName { get; set; }

        [DisplayName("District")]
        public string district { get; set; }

        [DisplayName("Email")]
        [EmailValidation(ErrorMessage = "Not a valid email address")]
        public string email { get; set; }

        [DisplayName("Role")]
        public List<IRole> role { get; set; }

        [DisplayName("Active")]
        public bool isActive { get; set; }

        [DisplayName("Message Distribution")]
        public bool isDistribution { get; set; }

        [DisplayName("Email Distribution")]
        public bool isEmail { get; set; }

        [Required]
        [ValidatePasswordLength]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Confirm password")]
        public string confirmPassword { get; set; }

        public List<Product_User> userProducts { get; set; }
        public List<Equipment_User> userEquipment { get; set; }
        public List<IUser> userCommandChain { get; set; }
    }
}