﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

using DSNY.Core.Interfaces;
using DSNY.Data.Validators;
using DSNY.Data;
using DSNY.Models;

namespace DSNY.ViewModels
{
    /// <summary>
    /// View model defining fields, names and validation for editing a user
    /// </summary>
    public class ProductUserModel
    {
        public int Product_ID {get;set;}
        public string Product_Name { get; set; }
        public string Order_Delivery_Distribution { get; set; }
        public string Order_Delivery_Type { get; set; }
        public string Measurement_Type { get; set; }
        public int? Default_Capacity { get; set; }
        public bool is_Water { get; set; }
        public bool is_Drums { get; set; }
        public bool is_Sub_Type { get; set; }
        public int? Sub_Type_Id { get; set; }
        public List<Product_Sub_Type> Sub_Types { get; set; }
        public List<Vendor> Vendors { get; set; }
    }
}