﻿using DSNY.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class InventroyViewModel
    {
        public List<RadioItemViewModel> Radios { get; set; }
        public List<Dropdown> Statuses { get; set; }
        public string Json { get; set; }
        public List<Dropdown> EquipmentTypes { get; internal set; }
        public List<Dropdown> Makes { get; internal set; }
        public List<Dropdown> Models { get; internal set; }
        public List<RadioAccessoryViewModel> RadioAccessories { get; internal set; }
    }
}