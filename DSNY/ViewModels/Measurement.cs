using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class Measurement
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public bool Stc { get; set; }
        public bool Tool { get; set; }
        public bool Deleted { get; set; }
    }
}
