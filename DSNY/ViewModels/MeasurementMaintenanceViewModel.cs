using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class MeasurementMaintenanceViewModel
    {
        public Measurement BlankMeasurement { get; set; }
        public List<Measurement> Measurements { get; set; }
        public string Json { get; set; }
    }
}
