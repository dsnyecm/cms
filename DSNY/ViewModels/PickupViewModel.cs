﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class PickupViewModel
    {
        public List<Pickup> Pickups { get; set; }
    }
}