﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.Core.Interfaces;
using DSNY.Data.Validators;
using DSNY.Data;
using DSNY.Models;

namespace DSNY.ViewModels
{
    public class SiteOnHandViewModel
    {
        public List<Dropdown> Sites { get; set; }
        public string selectedSite { get; set; }
        public List<SiteOnHand> SiteOnHands { get; set; }
        public string Json { get; set; }
    }
}