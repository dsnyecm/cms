﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.Core.Interfaces;
using DSNY.Data.Validators;
using DSNY.Data;
using DSNY.Models;

namespace DSNY.ViewModels
{
    public class NeedsRequestViewModel
    {
        public Dropdown CurrentUser { get; set; }
        public List<Dropdown> Products { get; set; }
        public List<Dropdown> Sites { get; set; }
        public string SelectedSite { get; set; }
        public string RequestDate { get; set; }
        public bool Finalized { get; set; }
        public List<SiteOnHand> SiteOnHands { get; set; }
        public List<NeedsRequest> NeedsRequests { get; set; }
        public NeedsRequest BlankNeedsRequest { get; set; }
        public string Json { get; set; }
    }
}