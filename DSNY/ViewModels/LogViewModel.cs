﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

using DSNY.Core.Interfaces;
using DSNY.Data.Validators;
using DSNY.Data;
using DSNY.Models;

namespace DSNY.ViewModels
{
    /// <summary>
    /// View model defining fields, names and validation for editing a user
    /// </summary>
    public class LogViewModel
    {
        public List<Log> logs{ get; set; }
        public DateTime? searchDate { get; set; }
    }
}