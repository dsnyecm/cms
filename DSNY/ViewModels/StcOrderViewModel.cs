﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class StcOrderViewModel
    {
        public List<Dropdown> PreviousOrderDates { get; set; }
        public string selectedOrderDate { get; set; }
        public List<StcProduct> Products{ get; set; }
        public StcProduct selectedProduct { get; set; }
        public List<StcOrder> Orders { get; set; }
        public string Json { get; set; }
    }
}