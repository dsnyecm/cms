using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class StcAgencyViewModel
    {
        public bool OpenOnly { get; set; }
        public List<Dropdown> Products { get; set; }
        public List<Dropdown> Vendors { get; set; }
        public List<Dropdown> Zones { get; set; }
        public StcAgency BlankStcAgency { get; set; }
        public List<StcAgency> StcAgencies { get; set; }
        public string Json { get; set; }
    }
}
