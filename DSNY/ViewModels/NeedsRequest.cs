﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DSNY.ViewModels;

namespace DSNY.ViewModels
{
    public class NeedsRequest
    {
        public int? Id { get; set; }
        public string RequestDate { get; set; }
        public int? ProductId { get; set; }
        public string Product { get; set; }
        public string UserId { get; set; }
        public string User { get; set; }
        public string Qty { get; set; }
        public string Supervisor { get; set; }
        public string Badge { get; set; }
        public string ConfirmDate { get; set; }
        public bool New { get; set; }
        public bool Deleted { get; set; }
        public List<Dropdown> Products { get; set; }
        public List<Dropdown> Sites { get; set; }
    }
}