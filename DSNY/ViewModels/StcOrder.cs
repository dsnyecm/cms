﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class StcOrder
    {
        public int? Id { get; set; }
        public int BoroughId { get; set; }
        public string Borough { get; set; }
        public string District { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public string UserId { get; set; }
        public string User { get; set; }
        public string Vendor { get; set; }
        public int selectedVendor { get; set; }
        public List<StcPurchaseOrder> PurchaseOrders { get; set; }
        public int selectedPurchaseOrder { get; set; }
        public string Qty { get; set; }
        public int? Capacity { get; set; }
        public decimal? OnHand { get; set; }
        public bool Deleted { get; set; }
    }
}