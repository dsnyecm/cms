﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using DSNY.Core.Interfaces;
using DSNY.Models;
using DSNY.Data;

namespace DSNY.ViewModels
{
    /// <summary>
    /// View model defining fields, names and validation for holding data on a single purchase order line
    /// </summary>
    public class PurchaseOrderViewModel
    {
        public Purchase_Orders pos { get; set; }
        public int avgDispensed { get; set; }
        public int startOnHand { get; set; }
        public int endOnHand { get; set; }

        public string exceptionReason { get; set; }
        
        [Required(ErrorMessage = "*")]
        [Range(0, int.MaxValue, ErrorMessage = "*")]
        public int orderAmount { get; set; }

        public int capacity { get; set; }

        public bool isOrdered { get; set; }
        
        public Guid userId { get; set; }
        public string address { get; set; }
        public string borough { get; set; }
        public string district { get; set; }
        public string poDisplayName { get; set; }
        
        public int productId { get; set; }
        public string productName { get; set; }
        public int? productSubTypeId { get; set; }

        public int vendorId { get; set; }
        public string vendorName { get; set; }
        public string vendorAccountNumber { get; set; }
        public string vendorPhoneNumber { get; set; }
    }

    /// <summary>
    /// View model defining fields, names and validation for creating an entire Purchase Order
    /// </summary>
    public class PurchaseOrdersViewModel
    {
        public List<PurchaseOrderViewModel> pos { get; set; }
    }
}