﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class StcPurchaseOrder
    {
        public int? Id { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public string Amount { get; set; }
        public string AmountRemaining { get; set; }
        public string AmountRemainingForDelivery { get; set; }
        public int? VendorId { get; set; }
        public string Vendor { get; set; }
        public List<Dropdown> Zones { get; set; }
        public int? ZoneId { get; set; }
        public int? Zone2Id { get; set; }
        public string Zone { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CompleteDate { get; set; }
        public bool New { get; set; }
        public bool Deleted { get; set; }
    }
}