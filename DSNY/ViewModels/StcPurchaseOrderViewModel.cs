﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class StcPurchaseOrderViewModel
    {
        public bool OpenOnly { get; set; }
        public List<Dropdown> Products { get; set; }
        public List<Dropdown> Vendors { get; set; }
        public List<Dropdown> Zones { get; set; }
        public StcPurchaseOrder BlankPurchaseOrder { get; set; }
        public List<StcPurchaseOrder> PurchaseOrders { get; set; }
        public string Json { get; set; }
    }
}