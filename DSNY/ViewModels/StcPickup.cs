using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class StcPickup
    {
        public int? Id { get; set; }
        public int ProductId { get; set; }
        public int AgencyId { get; set; }
        public string DriverName { get; set; }
        public string TruckNumber { get; set; }
        public string PlateNumber { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string PickupDate { get; set; }
        public string PickupTime { get; set; }
        public string Qty { get; set; }
        public string DistrictSuperName { get; set; }
        public string DistrictSuperBadge { get; set; }
        public string BoroSuperName { get; set; }
        public string BoroSuperBadge { get; set; }
        public bool Finalized { get; set; }
        public bool Edit { get; set; }
        public bool Deleted { get; set; }
    }
}
