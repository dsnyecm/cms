using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class TruckType
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int? Capacity { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }
    }
}
