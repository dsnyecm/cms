using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class StcPickupViewModel
    {
        public bool External { get; set; }
        public List<StcProduct> Products { get; set; }
        public List<StcAgency> Agencies { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<Dropdown>[] Sites { get; set; }
        public string selectedSite { get; set; }
        public bool Finalized { get; set; }
        public StcPickup BlankStcPickup { get; set; }
        public List<StcPickup> StcPickups { get; set; }
        public bool BoroSignable { get; set; }
        public bool Finalizable { get; set; }
        public string Json { get; set; }
    }
}
