﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

using DSNY.Core.Interfaces;
using DSNY.Data.Validators;
using DSNY.Data;
using DSNY.Models;

namespace DSNY.ViewModels
{
    /// <summary>
    /// View model defining fields, names and validation for editing a user
    /// </summary>
    public class ToolTypesViewModel
    {
        public List<MeasurementViewModel> measurements{ get; set; }
        public List<ToolTypeViewModel> toolTypes { get; set; }
        public string Json { get; set; }
    }

    public class MeasurementViewModel
    {
        public int Measurement_id { get; set; }
        public string Measurement_Name { get; set; }
        public bool is_Active { get; set; }
        public bool is_STC { get; set; }
        public bool is_Tool { get; set; }
    }

    public class ToolTypeViewModel
    {
        public int Tool_Type_id { get; set; }
        public bool is_Active { get; set; }
        public string Tool_Type_Name { get; set; }
        public bool is_PowerTool { get; set; }
        public int Measurement_id { get; set; }
        public MeasurementViewModel Measurement { get; set; }
        public int Quantity_Per_Measurement { get; set; }
        public List<ToolTypeMakeViewModel> Makes { get; set; }
    }

    public class ToolTypeMinViewModel
    {
        public int Tool_Type_id { get; set; }
        public bool is_Active { get; set; }
        public string Tool_Type_Name { get; set; }
        public bool is_PowerTool { get; set; }
        public int Quantity_Per_Measurement { get; set; }
    }

    public class ToolTypeMakeViewModel
    {
        public int Make_id { get; set; }
        public int Tool_Type_id { get; set; }
        public bool is_Active { get; set; }
        public string Make_Name { get; set; }
        public List<ToolTypeMakeModelViewModel> Models { get; set; }
    }

    public class ToolTypeMakeModelViewModel
    {
        public int Model_id { get; set; }
        public int Make_id { get; set; }
        public string Model_Name { get; set; }
    }
}