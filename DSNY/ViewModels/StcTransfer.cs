using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class StcTransfer
    {
        public int? Id { get; set; }
        public int ProductId { get; set; }
        public string FromUserId { get; set; }
        public string FromUserName { get; set; }
        public string ToUserId { get; set; }
        public string ToUserName { get; set; }
        public int TruckTypeId { get; set; }
        public int? Loads { get; set; }
        public string Qty { get; set; }
        public string TransferDate { get; set; }
        public string TransferTime { get; set; }
        public string BoroSuperName { get; set; }
        public string BoroSuperBadge { get; set; }
        public string SanSuperName { get; set; }
        public string SanSuperBadge { get; set; }
        public bool Finalized { get; set; }
        public bool Edit { get; set; }
        public bool Deleted { get; set; }
    }
}
