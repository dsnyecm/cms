﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class StcProductOrders
    {
        public int ProductId { get; set; }
        public string Product { get; set; }
        public List<StcOrder> Orders { get; set; }
    }
}