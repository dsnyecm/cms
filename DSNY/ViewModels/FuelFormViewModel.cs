﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.Core.Interfaces;
using DSNY.Models;
using DSNY.Data;

namespace DSNY.ViewModels
{
    /// <summary>
    /// View model defining fields, names and validation for creating a fuel form
    /// </summary>
    public class FuelFormViewModel
    {
        public bool isEdit { get; set;  }
        public bool isAdmin { get; set; }
        public IUser submittedBy { get; set; }

        public Fuel_Form fuelForm { get; set; }
        public List<Fuel_Form_Details> fuelFormDetails { get; set; }
        public List<Fuel_Form_Details_Delivery> fuelFormDetailsDelivery { get; set; }
        public List<Fuel_Form_Equipment_Failure> fuelFormEquipFailure { get; set; }
        public List<DSNY.Core.Models.DSNYException> exceptions { get; set; }
    }
}