﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

using DSNY.Core.Interfaces;
using DSNY.Data.Validators;
using DSNY.Data;
using DSNY.Models;

namespace DSNY.ViewModels
{
    /// <summary>
    /// View model defining fields, names and validation for editing a user
    /// </summary>
    public class DSNYExceptionModel
    {
        [DisplayName("Exception Id")]
        public int exceptionId { get; set; }

        [DisplayName("Category Id")]
        public int exceptionCategoryId { get; set; }

        [DisplayName("Category Name")]
        public string exceptionCategoryCode { get; set; }

        [DisplayName("Date")]
        public DateTime exceptionDate { get; set; }

        [DisplayName("User")]
        public IUser exceptionUser { get; set; }
    }
}