﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.ViewModels
{
    public class RadioAccessoryViewModel
    {
        public int Id { get; set; }
        public string PartNumber { get; set; }
        public Guid? UserId { get; set; }
        public string Borough { get; set; }
        public int? EquipmentTypeId { get; set; }
        public int? MakeId { get; set; }
        public string Description { get; set; }
        public int? CurrentQty { get; set; }
        public string LastDistInvDate { get; set; }
        public int? LastDistInvQty { get; set; }
        public bool IsSpare { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? InventoryRequestDate { get; set; }
        public bool IsDistrictInventoryDateOlderThanRequestDate
        {
            get
            {
                return InventoryRequestDate != null && !string.IsNullOrEmpty(LastDistInvDate) && InventoryRequestDate.Value.Date > DateTime.Parse(LastDistInvDate);
            }
        }
    }
}