﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

    <div style="float: right; vertical-align: middle; width: 365px;">
        <div>
            <div style="float: left; margin: 7px 10px 0px 0px; ">
                Search: <input type="text" id="searchTerms" name="searchTerms" value="<%: ViewData["searchTerms"] != null ? ViewData["searchTerms"].ToString() : string.Empty %>" 
                    style="width: 244px;" onkeydown="if (event.keyCode == 13) { document.forms[0].submit(); }" />
            </div>
            <div style="float: left; text-align: center; font-size: .85em; width: 50px;">
                <a href="#" id="searchSubmit" style="text-decoration: none; color: #696969; ">
                    <img border="0" src="~/Content/Images/Icons/search.png" runat="server" /><br />Search</a>
            </div>
        </div>
                
        <div style="margin-top: 35px">
            <div style="width: 150px; float: left; padding-left: 12px;">
                From: <input name="fromDate" id="fromDate" style="width: 70px;" readonly
                    value="<%: ViewData["fromDate"] != null ? ViewData["fromDate"].ToString() : DateTime.Now.AddDays(-90).ToShortDateString() %>" />
                <script type="text/javascript">
                    new tcal({ 'formname': 'messageForm', 'controlname': 'fromDate' });
		        </script>
            </div>
            <div style="width: 133px; float: left; padding-left: 4px;">
                To: <input name="toDate" id="toDate" style="width: 70px;" readonly
                    value="<%: ViewData["toDate"] != null ? ViewData["toDate"].ToString() : DateTime.Now.AddDays(1).ToShortDateString() %>" />
                <script type="text/javascript">
                    new tcal({ 'formname': 'messageForm', 'controlname': 'toDate' });
		        </script>
            </div>
            <div style="font-size: .75em; float: left; padding-top: 3px;">
                <input type="checkbox" name="isFuelForm" <%: ViewData["searchFuelForm"] != null && !((bool) ViewData["searchFuelForm"]) ? string.Empty : "checked" %> id="isFuelForm"  />Fuel Form
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {

            $("#searchSubmit").click(function () {
                $("#page").val(1);
                document.forms[0].submit();
            });
        });
    </script>