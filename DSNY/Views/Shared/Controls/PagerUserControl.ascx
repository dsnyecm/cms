﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>


    <div style="width: 910px; text-align: center;" id="pager">
    <%
        int pageSize = DSNY.MvcApplication.pageSize;
        int totalRecords = 0;

        if (TempData["totalRecords"] != null)
            int.TryParse(TempData["totalRecords"].ToString(), out totalRecords);

        if (totalRecords > pageSize) {
            // attempt to get the current page number
            int page = 1;
                    
            if (Request["page"] != null && !string.IsNullOrEmpty(Request["page"])) {
                int.TryParse(Request["page"], out page);    
            }

            if (page != 1) { %>
                <input type="hidden" id="page" name="page" value="<%: page %>" />
                <a href="" onclick="return false;" id="pageLinkPrev"><< Prev</a>
            <% } else { %>
                <input type="hidden" id="page" name="page" value="1" />
            <% }

            int tempCount = totalRecords, printPage = 1;

            if (totalRecords > 0)
            {
                do
                {
                    if (printPage != page)
                    { %>
                        <a href="" onclick="return false;" class="pagerNumber"><%: printPage %></a>
                    <% } else { %>
                    <%: printPage %>
                    <% }

                    printPage++;
                    tempCount -= pageSize;
                }
                while (tempCount > 0);

                if (Math.Ceiling((double)(totalRecords / pageSize) + 1.0) != (double)page && totalRecords != page * pageSize) { %>
                    <a href="" onclick="return false;" id="pageLinkNext">Next >></a>
            <%  }
            }
        } %>
    </div>


<script type="text/javascript">
    $(document).ready(function () {

        $("#pageLinkPrev").click(function () {
            $("#page").val(parseInt($("#page").val()) - 1);
            document.forms[0].submit();
        });

        $("#pageLinkNext").click(function () {
            $("#page").val(parseInt($("#page").val()) + 1);
            document.forms[0].submit();
        });

        $(".pagerNumber").click(function () {
            $("#page").val(parseInt($(this).text()));
            document.forms[0].submit();
        });
    });
</script>