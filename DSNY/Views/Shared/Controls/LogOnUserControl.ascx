﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% if (Request.IsAuthenticated) { %>
        Welcome <b><%: Page.User.Identity.Name %></b>
        [ 
        <% if (Roles.IsUserInRole("System Administrator") || Roles.IsUserInRole("Reports")) { %>
            <%: Html.ActionLink("Site Administration", "Index", "Admin", null, new { @class = "menu" })%>  | 
        <% }

            if (Roles.IsUserInRole("Administrator")) { %>
            <%: Html.ActionLink("Administrator Dashboard", "Admin", "Dashboard", null, new { @class = "menu" })%>  | 
        <% }
       
            if (Roles.IsUserInRole("Field") || Roles.IsUserInRole("Garage")) { %>
            <%: Html.ActionLink("Field Dashboard", "Field", "Dashboard", null, new { @class = "menu" })%>  | 
        <% }

            if (Roles.IsUserInRole("STC Site") || Roles.IsUserInRole("STC Borough") || Roles.IsUserInRole("STC HQ")) { %>
            <%: Html.ActionLink("STC", "Index", "Stc", null, new { @class = "menu" })%>  | 
        <% }

            if (Roles.IsUserInRole("GS Warehouse") || Roles.IsUserInRole("GS Borough") || Roles.IsUserInRole("GS District") || Roles.IsUserInRole("GS HQ")) { %>
            <%: Html.ActionLink("Garage Supplies", "Index", "GarageSupplies", null, new { @class = "menu" })%>  | 
        <% } 
            
            if (Roles.IsUserInRole("Radio Admin") || Roles.IsUserInRole("Radio District")) { %>
            <%: Html.ActionLink("Radio Administration", "Admin", "Radio", null, new { @class = "menu" })%>  | 
        <% }  %>

        <%: Html.ActionLink("Log Off", "LogOff", "Home", null, new { @class = "menu" })%> ]
<% } %> 