﻿<div id="end-book-issue" class="modal" title="Inventory Level Warning">
	<p>
		<span class="ui-icon ui-icon-alert"></span>
		<span class="modal-text">
			Validation warning for <span id="end-book-product"></span>. The previous quantity on hand [<span id="end-book-prev-on-hand-qty"></span>] + the delivered amount [<span id="end-book-delivered"></span>] - the dispense amount [<span id="end-book-dispensed"></span>] should equal your current quantity on hand [<span id="end-book-on-hand-qty"></span>]. It does not. Consider changing the <span id="end-book-diff-type"></span> amount to approx (<span id="end-book-diff"></span>)<span id="end-book-amount"></span>. Would you like to override this exception?
		</span>
	</p>
	<p class="reason">
		Please provide reason:<br />
		<textarea rows="2" cols="45" id="bookEndReason"></textarea>
	</p>
</div>

<%--<div id="delivery-issue" class="modal" title="Delivery Issue">
	<p>
		<span class="ui-icon ui-icon-alert"></span>
		For Product <span id="delivery-issue-product"></span> for amount of <span id="delivery-issue-amount"></span>, but expecting a delivery amount of <span id="delivery-issue-prod-amount"></span>. Is this correct?
	</p>
	<p class="reason">
		Please provide reason:<br />
		<textarea rows="2" cols="45" id="delieryIssueReason"></textarea>
	</p>
</div>

<div id="expected-delivery-issue" class="modal"  title="Expected Delivery Issue">
	<p>
		<span class="ui-icon ui-icon-alert"></span>
		Expecting a delivery of <span id="expected-delivery-issue-product"></span> for amount of less than or equal to <span id="expected-delivery-issue-expected-amount"></span>, you entered a delivery amount of <span id="expected-delivery-issue-amount"></span>.  Is this correct?
	</p>
	<p class="reason">
		Please provide reason:<br />
		<textarea rows="2" cols="45" id="expectedDeliveryReason"></textarea>
	</p>
</div>--%>

<div id="on-hand-grtr-than-90" class="modal"  title="Exceeding 90% of Capacity">
	<p>
		<span class="ui-icon ui-icon-alert"></span>
		<span class="modal-text">
			Validation error for <span id="on-hand-grtr-than-90-quanty-product"></span>. You entered a quantity on hand amount of <span id="on-hand-grtr-than-90-quanty-on-hand"></span>, this exceeds 90% of the tanks' capacity of <span id="on-hand-grtr-than-90-tank-capacity-at-90"></span><span id="on-hand-grtr-than-90-water"></span>.  This must be fixed in order to proceed.
		</span>
	</p>
</div>

<div id="qty-delivery-difference" class="modal"  title="Quantity vs Delivered Deviation">
	<p>
		<span class="ui-icon ui-icon-alert"></span>
		<span class="modal-text">
			You have deliveries for <span id="qty-delivery-difference-product"></span> in the amount of <span id="qty-delivery-difference-qty"></span>, but the amount put into <span id="qty-delivery-difference-product"></span> tanks is <span id="qty-delivery-difference-delivery"></span>.  Do you want to override this exception?
		</span>
	</p>
	<p class="reason">
		Please provide reason:<br />
		<textarea rows="2" cols="45" id="qtyDeliveryDifferenceReason"></textarea>
	</p>
</div>

<div id="confirm-send" class="modal" title="Submit Fuel Form" style="overflow: hidden;">
	<p>
		<span class="ui-icon ui-icon-alert"></span>
		<span class="modal-text">
			Are you sure you want to submit this fuel form?
		</span>
	</p>
</div>