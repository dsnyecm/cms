﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DSNY.ViewModels.FuelFormViewModel>" %>

<% List<DSNY.Data.Equipment_User> userProductEquip = ViewData["AvailableEquipment"] as List<DSNY.Data.Equipment_User>;
	List<DSNY.Data.Product_User> userProducts = new List<DSNY.Data.Product_User>();
	List<DSNY.Data.Fuel_Form_Details_Delivery> fuelDeliveries = null;
    List<DSNY.Data.Vendor> productVendors = null;
    List<DSNY.Data.Vendor> vendors = ViewData["vendors"] as List<DSNY.Data.Vendor>;
	DSNY.Data.Product_User foundProduct = null;
	DSNY.Data.Equipment_User relatedEquip = null;
    bool isDown = false;

	if (ViewData["UserProducts"] != null && ((List<DSNY.Data.Product_User>)ViewData["UserProducts"]).Count > 0)
		userProducts = (List<DSNY.Data.Product_User>)ViewData["UserProducts"];

	if (Model.fuelFormDetails.Count > 0) { %>
	<table id="fuelReqTable" style="width: 1080px;">
		<thead>
			<tr>
				<th style="width: 282px;">Product</th>
				<th>Capacity</th>
				<th style="width: 95px; text-align: center;">Qty On Hand</th>
				<th style="width: 97px; text-align: center;">Spare Drums</th>
				<th style="width: 60px;">Delivered</th>
				<th style="width: 65px;">Dispensed</th>
				<th style="width: 250px; text-align: center;">Vendor</th>
				<th style="width: 87px; text-align: center;">Invoice #</th>
				<th style="width: 205px; text-align: center;">Date</th>
				<th>Quantity</th>
			</tr>
		</thead>
		<tbody>
	<%  int rowCount = 0, deliveryCount = 0, previousProductId = 0;
        foreach (var ffd in Model.fuelFormDetails)
        {
            foundProduct = userProducts.SingleOrDefault(up => up.Product_ID == ffd.Product_ID);
            var vendorIds = foundProduct.Product.Product_Vendor.Select(v => v.Vendor_ID).ToList();
            productVendors = vendors.Where(v => vendorIds.Contains(v.Vendor_ID)).ToList();
            fuelDeliveries = null;
            relatedEquip = ffd.Equipment_User_ID > -1 ? userProductEquip.SingleOrDefault(pe => pe.Equipment_User_ID == ffd.Equipment_User_ID && pe.Equipment.has_Capacity) : null;
            isDown = relatedEquip != null ? relatedEquip.Fuel_Form_Equipment_Failure.FirstOrDefault(f => f.Equipment_User_ID == ffd.Equipment_User_ID && f.is_Equipment_Failure == true && f.Fix_Date == null) != null : false;
            var capacity = relatedEquip != null && relatedEquip.Capacity > 0 ? relatedEquip.Capacity : foundProduct != null && foundProduct.Capacity > 0 ? foundProduct.Capacity : ffd.Product.Default_Capacity;
    %>
			    <tr productId="<%: ffd.Product_ID %>" class="<%: rowCount % 2 == 0 ? "alt" : string.Empty %>">
				    <td>
					    <%: Html.HiddenFor(p => p.fuelFormDetails[rowCount].Fuel_Form_ID) %>
					    <%: Html.HiddenFor(p => p.fuelFormDetails[rowCount].Fuel_Form_Details_Id) %>
					    <%: Html.HiddenFor(p => p.fuelFormDetails[rowCount].Product_ID) %>
                        <%: Html.HiddenFor(p => p.fuelFormDetails[rowCount].Equipment_User_ID) %>
					    <%: ffd.Product.Product_Name%>
					
					    <% if (relatedEquip != null && relatedEquip.Equipment_Description != string.Empty)
						    {
							    ;%>
						    - <%: relatedEquip.Equipment_Description%>
						    <%: Html.HiddenFor(i => i.fuelFormDetails[rowCount].Equipment_User_ID) %>
					    <% } %>
					    <%: !string.IsNullOrEmpty(ffd.Product.Measurement_Type) ? " (" + ffd.Product.Measurement_Type + ")" : "" %>
				    </td>
				    <td style="text-align: center;"><%: capacity %></td>
				    <td style="text-align: center;">
					    <%: Html.TextBoxFor(m => m.fuelFormDetails[rowCount].On_Hand, new { style = "width: 50px; text-align: right;", data_capacity = capacity })%>
				    </td>
					
				    <td style="text-align: center;">
					    <% if (ffd.Product.is_Drums)
						    {%>
						    <%: Html.TextBoxFor(m => m.fuelFormDetails[rowCount].Spare_Drums, new { style = "width: 50px; text-align: right;" })%>
					    <% } %>
				    </td>
				    <td style="text-align: center;">
					    <% if (ffd.Product.is_Reading_Required)
						    {%>
						    <%: Html.TextBoxFor(m => m.fuelFormDetails[rowCount].Tank_Delivered, new { style = "width: 50px; text-align: right;" })%>
					    <% } %>
				    </td>
				    <td style="text-align: center;">
					    <% if (ffd.Product.is_Reading_Required)
						    {%>
						    <%: Html.TextBoxFor(m => m.fuelFormDetails[rowCount].Tank_Dispensed, new { style = "width: 50px; text-align: right;" })%>
					    <% } %>
				    </td>
				    <%
					    if (!ffd.Product.is_Water)
					    {
						    fuelDeliveries = ffd.Fuel_Form_Details_Delivery.Where(dd => dd.Fuel_Form_Details_Id == ffd.Fuel_Form_Details_Id).OrderBy(dd => dd.Order_No).ToList();
						    deliveryCount = 0;

						    String uniqueId = String.Empty, uniqueName = String.Empty;

						    if (fuelDeliveries.Count == 0)
						    {
							    DSNY.Data.Fuel_Form_Details_Delivery tempDdff = new DSNY.Data.Fuel_Form_Details_Delivery();

							    fuelDeliveries.Add(tempDdff);
							    ffd.Fuel_Form_Details_Delivery.Add(tempDdff);
						    }

						    foreach (var fd in fuelDeliveries)
						    {
							    uniqueId = "fuelFormDetails_" + rowCount.ToString() + "__Fuel_Form_Details_Delivery_" + deliveryCount.ToString() + "__{0}";
							    uniqueName = "fuelFormDetails[" + rowCount.ToString() + "].Fuel_Form_Details_Delivery[" + deliveryCount.ToString() + "].{0}";

							    if (deliveryCount > 0) {
					    %>
						    <tr subProductId="<%: ffd.Product_ID %>" class="<%: rowCount % 2 == 0 ? "alt" : string.Empty %>">
							    <td></td>
							    <td></td>
							    <td></td>
							    <td></td>
							    <td></td>
							    <td></td>
				    <%      }

                              if ((ffd.Product_ID != previousProductId || fuelDeliveries.Count() > 1) && productVendors.Count() > 0) { %>
						    <td style="text-align: center;">
							    <%: Html.HiddenFor(m => m.fuelFormDetails[rowCount].Fuel_Form_Details_Delivery.ElementAt(deliveryCount).Fuel_Form_Details_Id,
									new { @id = string.Format(uniqueId, "Fuel_Form_Details_Id", deliveryCount), Name = string.Format(uniqueName, "Fuel_Form_Details_Id", deliveryCount) }) %>
							    <%: Html.HiddenFor(m => m.fuelFormDetails[rowCount].Fuel_Form_Details_Delivery.ElementAt(deliveryCount).Order_No,
									new { @id = string.Format(uniqueId, "Order_No", deliveryCount), Name = string.Format(uniqueName, "Order_No", deliveryCount) }) %>
							    <%: Html.DropDownListFor(m => m.fuelFormDetails[rowCount].Fuel_Form_Details_Delivery.ElementAt(deliveryCount).Vendor_ID,
								    new SelectList(productVendors, "Vendor_ID", "Vendor_Name", Model.fuelFormDetails[rowCount].Fuel_Form_Details_Delivery.ElementAt(deliveryCount).Vendor_ID),
								    "SELECT VENDOR", new { @id = string.Format(uniqueId, "Vendor_ID"), Name = string.Format(uniqueName, "Vendor_ID"), style = "width: 250px" }) %>
						    </td>
						    <td>
							    <%: Html.TextBoxFor(m => m.fuelFormDetails[rowCount].Fuel_Form_Details_Delivery.ElementAt(deliveryCount).Invoice_Number,
											    new { @id = string.Format(uniqueId, "Invoice_Number"), Name = string.Format(uniqueName, "Invoice_Number"), style = "width: 68px; text-align: right;" })%>
						    </td>
						    <td style="text-align: center;">
							    <%: Html.TextBoxFor(m => m.fuelFormDetails[rowCount].Fuel_Form_Details_Delivery.ElementAt(deliveryCount).Recieve_Date,
											    new
											    {
												    @id = string.Format(uniqueId, "Recieve_Date"),
												    Name = string.Format(uniqueName, "Recieve_Date"),
												    @class = "tcalReadOnly",
												    style = "width: 70px;",
												    size = "10",
												    @readonly = "true",
												    @Value = Model.fuelFormDetails[rowCount].Fuel_Form_Details_Delivery.ElementAt(deliveryCount).Recieve_Date.HasValue ?
												    Model.fuelFormDetails[rowCount].Fuel_Form_Details_Delivery.ElementAt(deliveryCount).Recieve_Date.Value.ToString("MM/dd/yyyy") : ""
											    })%>
							    <script type="text/javascript">
								    new tcal({ 'formname': 'fuelForm', 'controlname': '<%: string.Format(uniqueId, "Recieve_Date")%>', 'id': '<%: string.Format(uniqueId, "Recieve_Date") %>' });
							    </script> 
						    </td>
						    <td>
							    <%: Html.TextBoxFor(m => m.fuelFormDetails[rowCount].Fuel_Form_Details_Delivery.ElementAt(deliveryCount).Quantity,
											    new { @id = string.Format(uniqueId, "Quantity"), Name = string.Format(uniqueName, "Quantity"), style = "width: 50px; text-align: right;" })%>
						    </td>
				    <% if (deliveryCount == 0) { %>
						    <td style="text-align: center;">
							    <a href="" class="addRow" onclick="return false;">
								    <img border="0" src="<%=ResolveUrl("~/Content/Images/Icons/add.png")%>" />
							    </a>
						    </td>
				    <%  } else { %>
						    <td style="text-align: center;">
							    <a href="" class="removeRow" onclick="return false;">
								    <img border="0" src="<%=ResolveUrl("~/Content/Images/Icons/delete.png")%>" />
							    </a>
						    </td>
				    <% }
                        } else { %>
						    <td></td>
						    <td></td>
						    <td></td>
						    <td></td>
                    <% }
                            deliveryCount++;
                            previousProductId = ffd.Product_ID;
                        } %>
				    </tr>
				    <% } else { %>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
					    <td>&nbsp;</td>
				    </tr>
		    <%  }
                rowCount++;
            } %>
		</tbody>
	</table>

	<div id="exceptions">
<%  
		if (Model.exceptions != null && Model.exceptions.Count > 0)
		{
			int exceptionCount = 0;
			foreach (var item in Model.exceptions)
			{  %>
			<%: Html.HiddenFor(m => m.exceptions[exceptionCount].categoryCode) %>
			<%: Html.HiddenFor(m => m.exceptions[exceptionCount].categoryId) %>
			<%: Html.HiddenFor(m => m.exceptions[exceptionCount].categoryName) %>
			<%: Html.HiddenFor(m => m.exceptions[exceptionCount].date) %>
			<%: Html.HiddenFor(m => m.exceptions[exceptionCount].exception) %>
			<%: Html.HiddenFor(m => m.exceptions[exceptionCount].userId) %>
		
<%      exceptionCount++;
			}
		}%>
	</div>
<%  } else { %>
	<h3>No products have been assigned to this site.</h3>
<% } %>