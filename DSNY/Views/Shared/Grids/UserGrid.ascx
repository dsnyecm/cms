﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<DSNY.Core.Interfaces.IUser>>" %>

    <% if (Model.ToList().Count > 0) { %>
        <table id="userTable" class="tablesorter" style="width: 1050px;">
            <thead>
            <tr>
                <th style="width: 40px;"></th>
                <th style="width: 100px;"><a href="#" class="sortable" id="userName">User Name</a></th>
                <th style="width: 160px;"><a href="#" class="sortable" id="description">Description</a></th>
                <th style="width: 125px;"><a href="#" class="sortable" id="email">Email</a></th>
<%--                <th style="width: 100px;">Roles</th>
                <th style="width: 80px;"><a href="#" class="sortable" id="isDistribution">Distribution</a></th>
                <th style="width: 60px;"><a href="#" class="sortable" id="isEmail">Email</a></th>
                <th style="width: 40px;"><a href="#" class="sortable" id="isActive">Active</a></th>--%>
                <th style="width: 150px;"><a href="#" class="sortable" id="LastLogin">Last Login</a></th>
            </tr>
            </thead>
            <tbody>
        <%  int asciiCode = 65;
            char currentLetter = ' ', lastLetter = ' ';
            foreach (var item in Model) { %>
                <tr id="row-<%: item.userName %>">
                    <td>
                        <%  currentLetter = item.userName.ToUpper().ToCharArray(0, 1)[0];
                            if (currentLetter != lastLetter)
                            { %> 
                               <% if ((int)currentLetter == asciiCode)
                                { %>
                                    <A name="<%: Convert.ToChar(asciiCode) %>"></A>
                                <%  lastLetter = item.userName.ToUpper().ToCharArray(0, 1)[0];
                                }

                                if (((int)currentLetter >= 65) && ((int)currentLetter <= 90))
                                    asciiCode++;
                            }%>
                            <a href="<%: Url.RouteUrl("User Edit", new { action="Edit", name = item.userName }) %>">
                                <img class="editButton" border="0" src="<%=ResolveUrl("~/Content/Images/Icons/pencil.png")%>" alt="Edit <%: item.userName %>" name="<%: item.userName %>" /></a>
                            <a href="" onclick="return false;">
                                <img class="deleteButton" border="0" src="<%=ResolveUrl("~/Content/Images/Icons/remove.png")%>" alt="Remove <%: item.userName %>" name="<%: item.userName %>" />
                            </a>
                    </td>
                    <td><%: item.userName %></td>
                    <td><%: item.description %></td>
                    <td><%: item.email %></td>
               <%--     <td style="font-size: .85em;">
                        <%  int i = 1;
                            foreach (var role in item.roles) { 
                                if (i == item.roles.Count) {%>
                                    <%: role.roleName %>
                                <% } 
                                else {%>
                                    <%: role.roleName %>,
                                <% }
                                i++;
                        } %>
                    </td>
                    <td><%: item.isDistribution %></td>
                    <td><%: item.isEmail %></td>
                    <td><%: item.isActive %></td>--%>
                    <td><%: String.Format("{0:g}", item.lastLogin) %></td>
                </tr>
    
        <% } %>
            </tbody>
        </table>
    <% } else { %>
        <div>There are no users.</div>
    <% } %>