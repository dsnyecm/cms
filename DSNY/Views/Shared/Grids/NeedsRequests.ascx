<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<DSNY.ViewModels.NeedsRequest>>" %>
    <div data-bind="visible: filterDisplay().length > 0">
        <table id="purchaseOrderTable" style="width: 100%">
            <thead>
            <tr>
                <th>Request Date</th>
                <th>Commodity</th>
                <th>Site</th>
                <th>Qty</th>
                <th>Supervisor</th>
                <th>Badge</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody data-bind="foreach: filterDisplay">
                <tr>
                    <td><input type="input" class="RequestDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { name: 'RequestDate' }" /></td>
                    <td><select data-bind="options: Products,
                                           optionsValue: 'id',
                                           optionsText: 'description',
                                           value: ProductId"></select></td>
                    <td><select data-bind="options: Sites,
                                           optionsValue: 'id',
                                           optionsText: 'description',
                                           value: UserId, event: { change: function(){ $root.changeSite(UserId(), Products); } }"></select></td>
                    <td><input type="text" class="Qty" data-bind="money, value: Qty"  data-required="true" /></td>
                    <td><input type="text" class="Supervisor" data-bind="value: Supervisor" /></td>
                    <td><input type="text" class="Badge" data-bind="value: Badge"  /></td>
                    <td><input type="button" value="Confirm" data-bind="click: $root.confirm" /></td>
                    <td><img src="/content/images/icons/delete.png" data-bind="click: function(){ Deleted(true); }" class="tcalIcon" alt="Delete Needs Request" title="Delete Needs Request"></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div data-bind="visible: filterDisplay().length === 0">
        There are no needs requests.
    </div>
