﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<DSNY.Data.Vendor>>" %>

    <% if (Model.ToList().Count > 0) { %>
        <table id="vendorTable" class="tablesorter">
            <thead>
            <tr>
                <th></th>
                <th style="width: 300px;"><a href="#" class="sortable" id="VendorName">Vendor</a></th>
                <th><a href="#" class="sortable" id="isActive">Active</a></th>
            </tr>
            </thead>
            <tbody>
        <% foreach (var item in Model) { %>
                <tr id="row-<%: item.Vendor_ID %>">
                    <td>
                        <a href="<%: Url.RouteUrl(new { controller = "Vendor", action = "Edit", id = item.Vendor_ID }) %>">
                            <img class="editButton" border="0" src="<%=ResolveUrl("~/Content/Images/Icons/pencil.png")%>" alt="Edit <%: item.Vendor_Name %>" name="<%: item.Vendor_Name %>" /></a>
                        <a href="#">
                            <img class="deleteButton" border="0" src="<%=ResolveUrl("~/Content/Images/Icons/remove.png")%>" alt="Remove <%: item.Vendor_Name %>" id="<%: item.Vendor_ID %>" name="<%: item.Vendor_Name %>" />
                        </a>
                    </td>
                    <td><%: item.Vendor_Name %></td>
                    <td><%: item.is_Active %></td>
                </tr>
        <% } %>
            </tbody>
        </table>
    <% } else {  %>
        <div>There are no vendors.</div>
    <% } %>