<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DSNY.ViewModels.DeliveryViewModel>" %>
				<div class="row" style="padding-left: 20px;">
					<div style="width: 5%; display: table-cell; " style="padding-right: 0">
						<div>
							Ticket #
						</div>
						<div>
							<input type="text" class="Small" data-bind="value: TruckNumber, enable: Edit" data-required="true" maxlength="50"/>
						</div>
					</div>
					<div style="width: 5%; display: table-cell; " style="padding-right: 0">
						<div>
							Purchase Order #
						</div>
						<div>
							<select data-bind="options: PurchaseOrders,
											   optionsValue: 'Id',
											   optionsText: 'PurchaseOrderNumber',
											   value: selectedPurchaseOrder, enable: Edit"></select>
						</div>
					</div>
					<div style="width: 5%; display: table-cell; " style="padding-right: 0">
						<div>
							Vendor
						</div>
						<div>
							<span data-bind="text: $root.GetPurchaseOrder(PurchaseOrders, selectedPurchaseOrder).Vendor()"></span>
						</div>
					</div>
					<div style="width: 5%; display: table-cell; ">
						<div>
							Destination
						</div>
						<div>
							<select data-bind="options: Sites,
											   optionsValue: 'id',
											   optionsText: 'description',
											   value: selectedSite, enable: Edit"></select>
						</div>
					</div>
					<div style="width: 7%; display: table-cell; ">
						<div>
							Delivery Date
						</div>
						<div>
							<input type="input" class="StartDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { name: 'DeliveryDate' }" data-required="true"  />
						</div>
					</div>
					<div style="width: 5%; display: table-cell; ">
						<div>
							Qty
						</div>
						<div>
							<input type="text" class="Small" data-bind="money, value: ReceiveWeight, enable: Edit" data-required="true" />
						</div>
					</div>
					<div style="width: 10%; display: table-cell;">
						<div>
							Dump Super
						</div>
						<div>
							<input type="text" class="Small" data-bind="value: ReceiveSuperName, enable: Edit" maxlength="50"/>
							<input type="text" class="Small" data-bind="value: ReceiveSuperBadge, enable: Edit" maxlength="20"/>
						</div>
						<div style="display: inline-block; width: 102px;">
							Name
						</div>
						<div style="display: inline-block; width: 102px;">
							Badge
						</div>
					</div>
					<div style="width: 10%; display: table-cell;">
						<div>
							Boro Super
						</div>
						<div>
							<input type="text" class="Small" data-bind="value: BoroSuperName, enable: Edit" maxlength="50"/>
							<input type="text" class="Small" data-bind="value: BoroSuperBadge, enable: Edit" maxlength="20"/>
						</div>
						<div style="display: inline-block; width: 102px;">
							Name
						</div>
						<div style="display: inline-block; width: 102px;">
							Badge
						</div>
					</div>
					<div style="width: 2%; display: table-cell;">
						<div>&nbsp;</div>
						<a href="#" class="iconLink" data-bind="click: $root.edit">
							<span class="ui-icon ui-icon-pencil" style="font-size: 1.5em; cursor: pointer;"></span>
						</a>
					</div>
					<div style="width: 5%; display: table-cell;">
						<div>&nbsp;</div>
						<div>
							<input type="button" class="Small validate-finalize" data-bind="click: $root.finalize, enable: Edit" value="Finalize" />
						</div>
					</div>
				</div>
			</div>