<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<DSNY.ViewModels.SiteOnHand>>" %>
    <div data-bind="visible: filterOnHand().length > 0">
        <table id="onHandTable">
            <thead>
            <tr>
                <th>Active</th>
                <th>Site</th>
                <th>Commodity</th>
                <th>Measurement</th>
                <th>On Hand</th>
                <th>Capacity</th>
            </tr>
            </thead>
            <tbody data-bind="foreach: filterOnHand">
                <tr data-bind="css: { 'alt': $index() % 2 == 1 }">
                    <td class="center"><input type="checkbox" class="OnHandQty" data-bind="checked: Active, event: { click: function(){ $root.toggle($data); return true; } }"/></td>
                    <td data-bind="text: UserName"></td>
                    <td data-bind="text: ProductName"></td>
                    <td data-bind="text: Measurement"></td>
                    <td><input type="text" class="Qty" data-bind="money, value: Qty, enable: Active, attr: { 'data-required': Active }" /></td>
                    <td><input type="text" class="Capacity" data-bind="numeric, value: Capacity, enable: Active, attr: { 'data-required': Active }" /></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div data-bind="visible: filterOnHand().length === 0">
        There are no sites configured.
    </div>
