﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<DSNY.Data.Sent_Message>>" %>

    <% if (Model.ToList().Count > 0) { %>
        <table id="messageTable" class="tablesorter" style="width: 910px;">
            <thead>
            <tr>
                <th style="width: 35px;">Print</th>
                <th style="width: 50px;">Open</th>
                <th style="width: 60px;"><a href="#" id="Status" class="sortable">Status</a></th>
                <th style="width: 130px;"><a href="#" id="Dept_Msg_No" class="sortable">Dept Message #</a></th>
                <th style="width: 100px;"><a href="#" id="Code" class="sortable">Code</a></th>
                <th style="width: 230px;"><a href="#" id="Subject" class="sortable">Subject</a></th>
                <th style="width: 150px;"><a href="#" id="Date" class="sortable">Date</a></th>
                <th style="width: 100px;"><a href="#" id="SentBy" class="sortable">Sent By</a></th>
            </tr>
            </thead>
            <tbody>
            <% int tableCount = 1;
                foreach (var item in Model)
                {
                    if (item.Message != null)
                    { %>
                <tr>
                    <% if (!item.Message.is_Fuel_Form)
                       { %>
                        <td style="text-align: center;">
                            <input type="checkbox" id="printMessage" name="printMessage" class="printMessage" value="<%: item.Message.Message_Id %>" />
                        </td>
                    <% }
                       else
                       { %>
                        <td></td>
                    <% } %>
                    <td>
                        <div style="width: 42px; float: left; text-align: center; font-size: .85em;">
                        <% if (!item.Message.is_Fuel_Form)
                           {
                               if (TempData["view"] != null && TempData["view"] == "admin")
                               { %>
                                    <a href="<%: Url.RouteUrl(new { controller = "Message", action = "Admin", id = item.Message.Message_Id}) %>" class="messageLink">
                            <% }
                               else
                               { %>
                                    <a href="<%: Url.RouteUrl(new { controller = "Message", action = "Field", id = item.Message.Message_Id}) %>" class="messageLink">
                             <% }
                           }
                           else
                           { %>
                            <a href="<%: Url.RouteUrl(new { controller = "FuelForm", action = "View", id = item.Message.Message_Id}) %>" class="messageLink">
                        <% } %> 
                                <img border="0" src="<%=ResolveUrl("~/Content/Images/Icons/readEmail.png")%>" alt="Open" /><br />Open
                            </a>
                        </div>

                        <div class="clear"></div>
                    </td>
                    <td messageID="<%: item.Message_Id %>">
                        <% if (item.Open_Date == null)
                           { %> 
                        Received
                        <% }
                           else
                           { %>
                        Read
                        <%} %>
                    </td>
                    <td><%: Server.HtmlDecode(item.Message.Dept_Msg_No)%></td>
                    <td><%: Server.HtmlDecode(item.Message.Message_Code)%></td>
                    <td><%: Server.HtmlDecode(item.Message.Message_Subject)%></td>
                    <td>
                        <% if (item.Message.Message_Date.Value.TimeOfDay == new TimeSpan(0, 0, 0))
                           {%>
                            <%: item.Message.Message_Date.Value.ToString("M/d/yyyy")%>
                        <% }
                           else
                           { %>
                            <%: item.Message.Message_Date.Value.ToString("M/d/yyyy h:mm tt")%>
                        <% } %>
                    </td>
                    <td>
                    <% if (item.Message.aspnet_Users != null) { %>
                        <%: item.Message.aspnet_Users.UserName%>
                    <% } %>
                    </td>
                <% } %>
                </tr>
    
            <% tableCount++;
                } %>
            </tbody>
        </table>

        <% Html.RenderPartial("Controls/PagerUserControl"); %>
        <input type="hidden" name="sortColumn" id="sortColumn" value="<%: TempData["sortField"] != null ? TempData["sortField"].ToString() : string.Empty  %>" />
        <input type="hidden" name="direction" id="direction" value="<%: TempData["direction"] != null ? TempData["direction"].ToString() : string.Empty  %>" />
    <% } else { %>
        <div><h3>No results were returned or you have no messages.</h3></div>
    <% } %>