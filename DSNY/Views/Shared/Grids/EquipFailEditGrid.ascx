﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DSNY.ViewModels.FuelFormViewModel>" %>

	<% if (Model.fuelFormEquipFailure.Count > 0) { %>
		<table id="equipmentTable" style="width: 1080px;">
			<thead>
			<tr>
				<th style="width: 300px;">Equipment</th>
				<th style="width: 64px;">Is Down</th>
				<th style="width: 110px;">Failure Date</th>
				<th style="width: 110px;">Fix Date</th>
				<th style="width: 420px;">Remarks</th>
			</tr>
			</thead>
			<tbody>
			<%  int rowCount = 0;
				foreach (var item in Model.fuelFormEquipFailure) { %>
					<tr id="fail<%: item.Equipment_User_ID%>"  class="<%: rowCount % 2 == 0 ? "alt" : string.Empty %>">
					<td>
						<%: item.Equipment_User.Product.Product_Name%>
						<%: !string.IsNullOrEmpty(item.Equipment_User.Product.Measurement_Type) ? " (" + item.Equipment_User.Product.Measurement_Type + ")" : "" %>
						&nbsp;-&nbsp;<%: item.Equipment_User.Equipment_Description%>
						<%: Html.HiddenFor(m => m.fuelFormEquipFailure[rowCount].Equipment_Failure_ID) %>
						<%: Html.HiddenFor(m => m.fuelFormEquipFailure[rowCount].Equipment_User_ID) %>
						<%: Html.HiddenFor(m => m.fuelFormEquipFailure[rowCount].Fuel_Form_ID) %>
					</td>

					<td style="text-align: center;">
						<%: Html.CheckBoxFor(m => m.fuelFormEquipFailure[rowCount].is_Equipment_Failure) %>
					</td>

					<td style=""text-align: center;">
						<%: Html.TextBoxFor(m => m.fuelFormEquipFailure[rowCount].Failure_Date, new { @class = "tcalReadOnly", style = "width: 70px;", size = "10", @readonly = "true",
								@Value = Model.fuelFormEquipFailure[rowCount].Failure_Date.HasValue ?  Model.fuelFormEquipFailure[rowCount].Failure_Date.Value.ToString("MM/dd/yyyy") : "" })%>
						<script type="text/javascript">
							new tcal({ 'formname': 'fuelForm', 'controlname': 'fuelFormEquipFailure[<%: rowCount%>].Failure_Date', 'id': 'fuelFormEquipFailure_<%: rowCount%>__Failure_Date' });
						</script> 
					</td>
					<td>
						<%: Html.TextBoxFor(m => m.fuelFormEquipFailure[rowCount].Fix_Date, new { @class = "tcalReadOnly", style = "width: 70px;", size = "10", @readonly = "true",
								@Value = Model.fuelFormEquipFailure[rowCount].Fix_Date.HasValue ?  Model.fuelFormEquipFailure[rowCount].Fix_Date.Value.ToString("MM/dd/yyyy") : "" })%>
						<script type="text/javascript">
							new tcal({ 'formname': 'fuelForm', 'controlname': 'fuelFormEquipFailure[<%: rowCount%>].Fix_Date', 'id': 'fuelFormEquipFailure_<%: rowCount%>__Fix_Date' });
						</script> 
					</td>
					<td>
						<%: Html.TextBoxFor(m => m.fuelFormEquipFailure[rowCount].Remarks, new { style = "width: 450px;" }) %>
					</td>
				</tr>
			<% rowCount++;
				} %>
			</tbody>
		</table>
<% } else { %>
	<h3>No equipment has been assigned to this site.</h3>
<% } %>