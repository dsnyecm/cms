<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DSNY.ViewModels.DeliveryViewModel>" %>
				<div class="nopadding">
					<div class="row">
						<div class="col-xs-7"></div>
						<div class="col-xs-2"><h3>Weight Master</h3></div>
						<div class="col-xs-2"><h3>Borough Supervisor</h3></div>
					</div>
					<div class="row" style="padding-left: 20px;">
						<div class="col-xs-2" style="padding-right: 0">
							<div class="col-xs-12" style="padding-left: 0">
								Purchase Order #
							</div>
							<div class="col-xs-12" style="padding-left: 0">
								<select data-bind="options: PurchaseOrders,
												   optionsValue: 'Id',
												   optionsText: 'PurchaseOrderNumber',
												   value: selectedPurchaseOrder, enable: Edit"></select>
							</div>
						</div>
						<div class="col-xs-1" style="padding-left: 0">
							<div class="col-xs-12" style="padding-left: 0">
								Vendor
							</div>
							<div class="col-xs-12" style="padding-left: 0">
								<span data-bind="text: $root.GetPurchaseOrder(PurchaseOrders, selectedPurchaseOrder).Vendor()"></span>
							</div>
						</div>
						<div class="col-xs-1">
							<div>
								Truck No.
							</div>
							<div>
								<input type="text" class="Small" data-bind="value: TruckNumber, enable: Edit" data-required="true"/>
							</div>
						</div>
						<div class="col-xs-1">
							<div>
								Plate No.
							</div>
							<div>
								<input type="text" class="Small" data-bind="value: PlateNumber, enable: Edit" data-required="true"/>
							</div>
						</div>
						<div class="col-xs-1">
							<div>
								Destination
							</div>
							<div>
								<select data-bind="options: Sites,
												   optionsValue: 'id',
												   optionsText: 'description',
												   value: selectedSite, enable: Edit"></select>
							</div>
						</div>
						<div class="col-xs-1">
							<div>
								Delivery Date
							</div>
							<div>
								<input type="input" class="StartDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { name: 'DeliveryDate' }" />
							</div>
						</div>
						<div class="col-xs-1">
							<div>
								Weight
							</div>
							<div>
								<input type="text" class="Small" data-bind="money, value: LeaveWeight, enable: Edit" data-required="true"/>
							</div>
						</div>
						<div class="col-xs-1">
							<div>
								Leave Time
							</div>
							<div>
								<input type="text" class="Small" placeholder="12:00 AM" data-bind="timeFormat: LeaveTime, enable: Edit"/>
							</div>
						</div>
						<div class="col-xs-1">
							<div>
								Weight
							</div>
							<div>
								<input type="text" class="Small" data-bind="money, value: ReceiveWeight, enable: Edit" data-required="true"/>
							</div>
						</div>
						<div class="col-xs-1">
							<div>
								Received Time
							</div>
							<div>
								<input type="text" class="Small" placeholder="12:00 AM" data-bind="timeFormat: ReceiveTime, value: ReceiveTime, enable: Edit"/>
							</div>
						</div>
						<div class="col-xs-1">
							<a href="#" class="iconLink" data-bind="click: $root.edit">
								<span class="ui-icon ui-icon-pencil" style="font-size: 1.5em; cursor: pointer;"></span>
							</a>
						</div>
					</div>
				</div>
				<fieldset style="width: 1200px" class="padding10px">
					<legend>Sign Off</legend>
					<div class="row" style="border: 1px">
						<div class="col-xs-1">
							Weight Master
						</div>
						<div class="col-xs-1">
							<div>
								<input type="text" class="Small" data-bind="value: WeightMasterName, enable: Edit"/>
							</div>
							<div>
								Name
							</div>
						</div>
						<div class="col-xs-1">
							<div>
								<input type="text" class="Small" data-bind="value: WeightMasterBadge, enable: Edit"/>
							</div>
							<div>
								Badge
							</div>
						</div>
						<div class="col-xs-1">
							Receiving Super
						</div>
						<div class="col-xs-1">
							<div>
								<input type="text" class="Small" data-bind="value: ReceiveSuperName, enable: Edit"/>
							</div>
							<div>
								Name
							</div>
						</div>
						<div class="col-xs-1">
							<div>
								<input type="text" class="Small" data-bind="value: ReceiveSuperBadge, enable: Edit"/>
							</div>
							<div>
								Badge
							</div>
						</div>
						<div class="col-xs-1">
							Boro Super
						</div>
						<div class="col-xs-1">
							<div>
								<input type="text" class="Small" data-bind="value: BoroSuperName, enable: Edit"/>
							</div>
							<div>
								Name
							</div>
						</div>
						<div class="col-xs-1">
							<div>
								<input type="text" class="Small" data-bind="value: BoroSuperBadge, enable: Edit"/>
							</div>
							<div>
								Badge
							</div>
						</div>
						<div class="col-xs-1">
							<input type="button" class="Small" data-bind="click: $root.finalize, enable: Edit" value="Finalize" />
						</div>
					</div>
				</fieldset>