﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<DSNY.Core.Interfaces.IRole>>" %>

    <% if (Model.ToList().Count > 0) { %>
        <table id="roleTable" class="tablesorter">
            <thead>           
            <tr>
                <th style="width: 250px;"><a href="#" class="sortable" id="RoleName">Role</a></th>
                <th style="width: 100px; text-align: center;"># of Users</th>
            </tr>
            </thead>
            <tbody>
        <% foreach (var item in Model) { %>
                <tr id="row-<%: item.roleName %>">
                    <td><%: item.roleName %></td>
                    <td style="text-align: center;"><%: item.usersInRole.Count %></td>
                </tr>
        <% } %>
            </tbody>
        </table>
    <% } else { %>
        <div>There are no roles.</div>
    <% } %>