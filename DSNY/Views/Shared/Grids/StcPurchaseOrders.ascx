<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<DSNY.ViewModels.StcPurchaseOrder>>" %>
	<div data-bind="visible: filterDisplay().length > 0">
		<table id="purchaseOrderTable" style="width: 100%">
			<thead>
			<tr>
				<th>Commodity Type</th>
				<th>Purchase Order #</th>
				<th>Vendor</th>
				<th>Order Amount</th>
				<th>Amount Remaining (Ordered)</th>
				<th>Amount Remaining (Delivered)</th>
				<th>1st Zone</th>
				<th>2nd Zone</th>
				<th>PO Start Date</th>
				<th>PO Complete Date</th>
				<th></th>
			</tr>
			</thead>
			<tbody data-bind="foreach: filterDisplay">
                <tr data-bind="css: { 'alt': $index() % 2 == 1 }">
					<td><select data-bind="options: $root.Products,
										   optionsValue: 'id',
										   optionsText: 'description',
										   value: ProductId"></select></td>
					<td><input type="text" class="PurchaseOrderNumber" data-bind="value: PurchaseOrderNumber" data-required="true" /></td>
					<td><select data-bind="options: $root.Vendors,
											   optionsValue: 'id',
											   optionsText: 'description',
											   value: VendorId"></select></td>
					<td><input type="text" class="Amount" data-bind="money, value: Amount, valueUpdate: 'afterkeydown'" data-required="true" /></td>
					<td style="text-align: right"><span data-bind="text: New() ? Amount() : AmountRemaining()" /></td>
					<td style="text-align: right"><span data-bind="text: New() ? Amount() : AmountRemainingForDelivery()" /></td>
					<td><select data-bind="options: Zones,
										   optionsValue: 'id',
										   optionsText: 'description',
										   value: ZoneId"></select></td>
					<td><select data-bind="options: Zones().filter(function(item){ return item.id() != ZoneId(); }),
										   optionsValue: 'id',
										   optionsText: 'description',
										   optionsCaption: 'None',
										   value: Zone2Id"></select></td>
					<td><div style="min-width: 100px"><input type="input" class="StartDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { name: 'StartDate', required: true }" /></div></td>
					<td><div style="min-width: 110px"><input type="input" class="EndDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { name: 'EndDate' }" /></div></td>
					<td><img src="/content/images/icons/delete.png" data-bind="click: $root.delete" class="tcalIcon" alt="Delete Purchase Order" title="Delete Purchase Order"></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div data-bind="visible: filterDisplay().length === 0">
		There are no purchase orders.
	</div>
