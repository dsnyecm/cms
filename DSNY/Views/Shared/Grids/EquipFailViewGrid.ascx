﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<DSNY.Data.Fuel_Form_Equipment_Failure>>" %>

    <% if (Model.Count() > 0) { %>
        <table id="equipmentTable" style="width: 1080px;">
            <thead>
            <tr>
                <th style="width: 350px;">Equipment</th>
                <th>Is Down</th>
                <th>Failure Date</th>
                <th>Fix Date</th>
                <th style="width: 520px;">Remarks</th>
            </tr>
            </thead>
            <tbody>
        <%  int rowCount = 0;
            foreach (var item in Model)
            {   
                if (rowCount % 2 == 0) {%>
                    <tr>
                <% } else { %>
                    <tr class="alt">
                <% } %>
                    <td>
						<%: item.Equipment_User.Product.Product_Name%>
						<%: !string.IsNullOrEmpty(item.Equipment_User.Product.Measurement_Type) ? " (" + item.Equipment_User.Product.Measurement_Type + ")" : "" %>
						&nbsp;-&nbsp;<%: item.Equipment_User.Equipment_Description%>
                    </td>
                    <td style="text-align: center;">
                        <%: item.is_Equipment_Failure ? "Yes"  : "No" %>
                    </td>
                    <td style="text-align: center;">
                        <% if (item.Failure_Date != null) { %>
                            <%: item.Failure_Date.Value.ToString("M/d/yyyy")%>
                        <% } else { %>
                            N/A
                        <% }  %>
                    </td>
                    <td style="text-align: center;">
                        <% if (item.Fix_Date != null) { %>
                            <%: item.Fix_Date.Value.ToString("M/d/yyyy")%>
                        <% } else { %>
                            N/A
                        <% }  %>
                    </td>
                    <td>
                        <%: item.Remarks %>
                    </td>
                </tr>
            <% rowCount++;
            } %>
            </tbody>
        </table>
    <% } else { %>
        <h3>No equipment failures were reported from site.</h3>
    <% } %>