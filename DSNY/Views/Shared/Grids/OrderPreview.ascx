<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DSNY.ViewModels.StcOrderViewModel>" %>
	<div id="orderPreview" class="modal" title="Preview Order" style="overflow: hidden;">
		<div class="col-xs-12" style="text-align: center;">
			<h1>New York City<br/>
			Department of Sanitation</h1>
		</div>
		<table id="orderPreviewTable" style="width: 100%">
			<thead>
			<tr>
				<th>Borough</th>
				<th>District</th>
				<th>Location</th>
				<th>Purchase Order #</th>
    <!-- ko if: $root.selectedProduct.Name() == 'Sand' -->
				<th>Yards</th>
				<th>Loads</th>
    <!-- /ko -->
    <!-- ko if: $root.selectedProduct.Name() == 'Calcium' -->
    <!-- ko if: 1==0 -->
				<th>Number of Tanks</th>
				<th>Tank Capacity</th>
    <!-- /ko -->
				<th>Gallons Ordered</th>
    <!-- /ko -->
    <!-- ko if: $root.selectedProduct.Name() == 'Salt' -->
				<th>Tons</th>
    <!-- /ko -->
			</tr>
			</thead>
			<tbody data-bind="foreach: filterOrders">
                <tr data-bind="css: { 'alt': $index() % 2 == 1 }">
					<td><span data-bind="text: Borough"></span></td>
					<td><span data-bind="text: District"></span></td>
					<td><span data-bind="text: Address"></span></td>
					<td><span data-bind="text: $root.GetPurchaseOrder(PurchaseOrders, selectedPurchaseOrder).PurchaseOrderNumber"></span></td>
    <!-- ko if: $root.selectedProduct.Name() == 'Sand' -->
					<td style="text-align: right"><span data-bind="text: Qty"></span></td>
					<td style="text-align: right"><span data-bind="text: Qty() / 40"></span></td>
    <!-- /ko -->
    <!-- ko if: $root.selectedProduct.Name() == 'Calcium' -->
    <!-- ko if: 1==0 -->
					<td><span data-bind="text: Qty"></span></td>
					<td><span data-bind="text: Qty"></span></td>
    <!-- /ko -->
					<td><span data-bind="text: Qty"></span></td>
    <!-- /ko -->
    <!-- ko if: $root.selectedProduct.Name() == 'Salt' -->
					<td><span data-bind="text: Qty"></span></td>
    <!-- /ko -->
				</tr>
			</tbody>
		</table>
	</div>
