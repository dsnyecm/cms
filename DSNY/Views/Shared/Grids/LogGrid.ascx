﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<DSNY.Data.Log>>" %>

    <% if (Model.ToList().Count > 0) { %>
        <table id="logTable" class="tablesorter">
            <thead>
            <tr>
                <th style="width: 175px;">Date</th>
                <th style="width: 975px;">Message</th>
            </tr>
            </thead>
            <tbody>
        <% foreach (var item in Model) { %>
                <tr id="row-<%: item.LogID %>">
                    <td><%: item.Timestamp.ToLocalTime().ToString() %></td>
                    <td style="width: 975px;"><%: item.FormattedMessage %></td>
                </tr>
        <% } %>
            </tbody>
        </table>
    <% } %>