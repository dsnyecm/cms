﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<DSNY.Data.Fuel_Form_Details>>" %>

    <%  if (Model.Count() > 0) { %>
        <table id="fuelReqTable" style="width: 1080px;">
            <thead>
            <tr>
                <th style="width: 250px;">Product</th>
                <th style="width: 75px;">Capacity</th>
                <th style="width: 95px;">Qty On Hand</th>
                <th style="width: 97px;">Spare Drums</th>
                <th style="width: 64px;">Delivered</th>
                <th style="width: 69px;">Dispensed</th>
                <th>Vendor</th>
                <th style="width: 100px; text-align: center;">Invoice #</th>
                <th style="width: 90px; text-align: center;">Date</th>
                <th style="width: 50px;">Quantity</th>
            </tr>
            </thead>
            <tbody>
        <%  int rowCount = 0;
            bool foundProduct = false;
            List<DSNY.Data.Product_User> userProducts = new List<DSNY.Data.Product_User>();
            List<DSNY.Data.Equipment_User> userProductEquip = new List<DSNY.Data.Equipment_User>();
            DSNY.Data.Equipment_User relatedEquip = null;

            if (ViewData["UserProducts"] != null && ((List<DSNY.Data.Product_User>)ViewData["UserProducts"]).Count > 0)
                userProducts = (List<DSNY.Data.Product_User>)ViewData["UserProducts"];

            if (ViewData["UserEquipProducts"] != null && ((List<DSNY.Data.Equipment_User>)ViewData["UserEquipProducts"]).Count > 0)
                userProductEquip = (List<DSNY.Data.Equipment_User>)ViewData["UserEquipProducts"];
                                
            foreach (var item in Model) {
                relatedEquip = userProductEquip.SingleOrDefault(pe => pe.Equipment_User_ID == item.Equipment_User_ID && pe.Equipment.has_Capacity); %>
                <tr class="<%: rowCount % 2 == 0 ? "alt" : string.Empty %>">
                        <td>
                        <%: item.Product.Product_Name%>
                        <%if (relatedEquip != null) { %> - <%: relatedEquip.Equipment_Description  %><% } %>
                        <% if (! string.IsNullOrEmpty(item.Product.Measurement_Type)) { %>
                            (<%: item.Product.Measurement_Type %>)
                        <% } %>
                    </td>
                    <td style="text-align: center;">
                    <%if (relatedEquip != null && relatedEquip.Capacity > 0)
                      { %> 
                        <%: relatedEquip.Capacity %>
                   <% }
                      else
                      {
                          foreach (DSNY.Data.Product_User userProduct in userProducts)
                          {
                              if (userProduct.Product_ID == item.Product_ID && userProduct.Capacity != null)
                              { %>
                               <%: userProduct.Capacity%>
	                   <%      foundProduct = true;
                            break;
                              }
                          } %>

                    <% if (!foundProduct)
                       { %>
                        <%: item.Product.Default_Capacity%>
                    <% }
                       foundProduct = false; %>
                    <% } %>
                    </td>
                    <td style="text-align: center;"><%: item.On_Hand %></td>
                    
                    <td style="text-align: center;">
                        <% if (item.Product.is_Drums && item.Spare_Drums != null) { %>
                            <%: item.Spare_Drums %>
                        <% } %>
                    </td>

                    <td style="text-align: center;">
                        <% if (item.Product.is_Reading_Required && item.Tank_Delivered != null) { %>
                            <%: item.Tank_Delivered %>
                        <% } %>
                    </td>

                    <td style="text-align: center;">
                        <% if (item.Product.is_Reading_Required && item.Tank_Dispensed != null) { %>
                            <%: item.Tank_Dispensed %>
                        <% } %>
                    </td>

                <% if (item.Fuel_Form_Details_Delivery.Count == 0) { %>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                <% }

                    bool isFirst = true;
                    foreach (var deliveryItem in item.Fuel_Form_Details_Delivery)
                    {
                        if (!isFirst) { %>
                            <tr class="<%: rowCount % 2 == 0 ? "alt" : string.Empty %>">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                    <% } %>
                            <td style="text-align: center;">
                            <% if (deliveryItem.Vendor != null) { %>
                                <%: deliveryItem.Vendor.Vendor_Name %>
                            <% } %> 
                            </td>
                                
                            <td style="text-align: center;">
                            <% if (deliveryItem.Invoice_Number != null) { %>
                                <%: deliveryItem.Invoice_Number %>
                            <% } %> 
                            </td>
                                
                            <td style="text-align: center;">
                            <% if (deliveryItem.Recieve_Date != null)
                                { %>
                                <%: deliveryItem.Recieve_Date.Value.ToString("M/d/yyyy")%>
                            <% } else { %>
                                N/A
                            <% }  %>
                            </td>
                                
                            <td style="text-align: center;">
                            <% if (deliveryItem.Quantity != null && deliveryItem.Quantity != 0) { %>
                                <%: deliveryItem.Quantity %>
                            <% } %>                                
                            </td>
                        </tr>
                <% isFirst = false;
                    }

                    rowCount++;
                } %>
            </tbody>
        </table>
    <% } else { %>
        <h3>No products have been assigned to this site assigned.</h3>
    <% } %>