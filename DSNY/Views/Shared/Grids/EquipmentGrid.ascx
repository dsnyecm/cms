﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<DSNY.Data.Equipment>>" %>

    <% if (Model.ToList().Count > 0) { %>
        <table id="equipmentTable" class="tablesorter">
            <thead>
            <tr>
                <th></th>
                <th style="width: 250px;"><a href="#" class="sortable" id="EquipmentName">Equipment</a></th>
                <th><a href="#" class="sortable" id="IsActive">Active</a></th>
            </tr>
            </thead>
            <tbody>
        <% foreach (var item in Model) { %>
                <tr id="row-<%: item.Equipment_ID %>">

                    <td>
                        <a href="<%: Url.RouteUrl( new { controller = "Equipment", action = "Edit", id = item.Equipment_ID }) %>">
                            <img class="editButton" border="0" src="<%=ResolveUrl("~/Content/Images/Icons/pencil.png")%>" alt="Edit <%: item.Equipment_Name %>" name="<%: item.Equipment_Name %>" /></a>
                        <a href="#">
                            <img class="deleteButton" border="0" src="<%=ResolveUrl("~/Content/Images/Icons/remove.png")%>" alt="Remove <%: item.Equipment_Name %>" id="<%: item.Equipment_ID %>" name="<%: item.Equipment_Name %>" />
                        </a>
                    </td>
                    <td><%: item.Equipment_Name%></td>
                    <td><%: item.is_Active%></td>
                </tr>
    
        <% } %>
            </tbody>
        </table>

    <% } else { %>
        <div>There are no equipment items assigned to this site.</div>
    <% } %>