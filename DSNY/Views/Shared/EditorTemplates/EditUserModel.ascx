﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<DSNY.ViewModels.EditUserModel>" %>

    <div class="editor-label"><%: Html.LabelFor(model => model.userName) %>:</div>
    <div class="editor-field" style="margin: 0.7em 0">
        <%: Html.DisplayFor(model => model.userName) %>
        <%: Html.HiddenFor(model => model.userName) %>
    </div>
    <div class="clear"></div>

    <div class="editor-label"><%: Html.LabelFor(model => model.description) %>:</div>
    <div class="editor-field">
        <%: Html.TextBoxFor(model => model.description) %>
        <%: Html.ValidationMessageFor(model => model.description) %>
    </div>
    <div class="clear"></div>

    <div class="editor-label"><%: Html.LabelFor(model => model.email) %>:</div>
    <div class="editor-field">
        <%: Html.TextBoxFor(model => model.email) %>
        <%: Html.ValidationMessageFor(model => model.email) %>
    </div>
    <div class="clear"></div>

    <div class="editor-label"><%: Html.LabelFor(model => model.address) %>:</div>
    <div class="editor-field">
        <%: Html.TextBoxFor(model => model.address) %>
        <%: Html.ValidationMessageFor(model => model.address) %>
    </div>
    <div class="clear"></div>

    <div class="editor-label"><%: Html.LabelFor(model => model.phoneNumber) %>:</div>
    <div class="editor-field">
        <%: Html.TextBoxFor(model => model.phoneNumber) %>
        <%: Html.ValidationMessageFor(model => model.phoneNumber) %>
    </div>
    <div class="clear"></div>

    <div class="editor-label"><%: Html.LabelFor(model => model.borough) %>:</div>
    <div class="editor-field">
        <%: Html.DropDownListFor(model => model.borough, new SelectList(new string[] { "Bronx", "Brooklyn", "Manhattan", "Queens", "Staten Island"})) %>
        <%: Html.ValidationMessageFor(model => model.borough) %>
    </div>
    <div class="clear"></div>

    <div class="editor-label"><%: Html.LabelFor(model => model.poDisplayName) %>:</div>
    <div class="editor-field">
        <%: Html.TextBoxFor(model => model.poDisplayName) %>
        <%: Html.ValidationMessageFor(model => model.poDisplayName) %>
    </div>
    <div class="clear"></div>

    <div class="editor-label"><%: Html.LabelFor(model => model.district) %>:</div>
    <div class="editor-field">
        <%: Html.TextBoxFor(model => model.district) %>
        <%: Html.ValidationMessageFor(model => model.district) %>
    </div>
    <div class="clear"></div>


    <div class="editor-label">Change Password:</div>
    <div class="editor-field">
        <%: Html.PasswordFor(model => model.password)%>
        <%: Html.ValidationMessageFor(model => model.password) %>
    </div>
    <div class="clear"></div>

    <div class="editor-label">Verify Password:</div>
    <div class="editor-field">
        <%: Html.PasswordFor(model => model.confirmPassword) %>            
        <%: Html.ValidationMessageFor(model => model.confirmPassword) %>
    </div>
    <div class="clear"></div>

    <div class="editor-label">Role:</div>
    <div class="editor-field">
        <% string[] selectedRole = Request.Form["role"] != null ? Request.Form["role"].Split(',') : null; %>
        <% foreach (string role in (string[])ViewData["Roles"]) { %>
            <% if (selectedRole != null) { 
                    if (selectedRole.Contains(role)) {%>
                        <div style="margin: 5px 0px"><input type="checkbox" checked="checked" name="role" value="<%: role %>" />&nbsp;<%: role%></div>
            <%     } else { %>
                        <div style="margin: 5px 0px"><input type="checkbox" name="role" value="<%: role %>" />&nbsp;<%: role%></div>
            <%     } 
                } else {
                    if (Model.role != null && Model.role.Exists(r => r.roleName == role)) { %>
                    <div style="margin: 5px 0px"><input type="checkbox" checked="checked" name="role" value="<%: role %>" />&nbsp;<%: role%></div>
            <%      } else { %>
                    <div style="margin: 5px 0px"><input type="checkbox" name="role" value="<%: role %>" />&nbsp;<%: role%></div>
            <%      }
                }
                } %>
    </div>
    <div class="clear"></div>

    <div class="editor-label"><%: Html.LabelFor(model => model.isDistribution) %>:</div>
    <div class="editor-field"><%: Html.CheckBoxFor(model => model.isDistribution) %></div>
    <div class="clear"></div>

    <div class="editor-label"><%: Html.LabelFor(model => model.isEmail) %>:</div>
    <div class="editor-field"><%: Html.CheckBoxFor(model => model.isEmail, new { @checked = "true" })%></div>
    <div class="clear"></div>

    <div class="editor-label"><%: Html.LabelFor(model => model.isActive) %>:</div>
    <div class="editor-field"><%: Html.CheckBoxFor(model => model.isActive) %></div>
    <div class="clear"></div>

    <div class="editor-label"><%: Html.LabelFor(model => model.isLocked) %>:</div>
    <div class="editor-field"><%: Model.isLocked.ToString() %></div>
    <div class="clear"></div>