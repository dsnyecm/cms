﻿<%@ Control Language="C#" %>
<%@ Import Namespace="System.Configuration" %>

	$(document).ready(function () {
		var imgPath = getUrlPath(),
			  details = [],
			  waterProducts = [],
			  exceptions = [],
              errors = [],
              submitting = false;

		$("#tabsContainer").tabs({ tab: 1 });

		preload(["<%=ResolveUrl("~/Content/Images/Icons/delete.png")%>"]);

		$.validator.addMethod("notGreatherThanToday", function(value, element) {
			if (value) {
				var elDate = new Date(value.substring(value.lastIndexOf('/') + 1, value.length), parseInt(value.substring(0, value.indexOf('/'))) - 1, value.substring(value.indexOf('/') + 1, value.lastIndexOf('/')));
				var subDate = new Date($('#submission-date').text());
				subDate.setDate(subDate.getDate() + 1);
				subDate.setHours(0,0,0,0);

				return elDate < subDate;
			}

			return true;
		}, '');

		// add new delivery row
		$('.addRow').live('click', function () {
			var rowIndex = $(this).closest("tr").prevAll("tr").length + 1,
				row = $(this).parents("table").find("tr:eq(" + rowIndex + ")"),
				productId = row.attr("productId"),
				id = null, name = null
				copiedRow = row.clone(),
				numOfProductRows = $("#fuelReqTable").find("tr[subProductId=" + productId + "]").length;

			copiedRow.find(".addRow > img").attr("src", "<%=ResolveUrl("~/Content/Images/Icons/delete.png")%>").parent().attr("class", "removeRow");
			copiedRow.find("script").remove();

			copiedRow.attr('subProductId', productId).removeAttr('productId');

			// clears first two cells on a copy
			copiedRow.find("td").each(function (index) {
				if (index < 2) {
					$(this).text("");
				}
			});

			// removes input boxes in cells 3 & 4 on copied row or changes the id of control
			copiedRow.find(":input").each(function (index) {
				var inputName = $(this).attr('name');
				$(this).removeClass('input-validation-error');
				
				if (inputName.indexOf('On_Hand') > -1 || inputName.indexOf('Spare_Drums') > -1 || inputName.indexOf('Tank_Delivered') > -1 || inputName.indexOf('Tank_Dispensed') > -1) {
					$(this).remove();
				} else {                   
					$(this).attr('id', $(this).attr('id').replace(/Fuel_Form_Details_Delivery_[0-9]+/, 'Fuel_Form_Details_Delivery_' + (numOfProductRows + 1)));
					$(this).attr('name', inputName.replace(/Fuel_Form_Details_Delivery\[[0-9]+]/, 'Fuel_Form_Details_Delivery[' + (numOfProductRows + 1) + ']'));
					
					// dont want to remove the fuel form details id ref
					if ($(this).attr('id').indexOf('Fuel_Form_Details_Id') === -1) {
						$(this).attr('value', '');
					}
				}
			});

			copiedRow.find("img").each(function (index) {
				if ($(this).attr('id').indexOf("tcalico") > -1) {
					id = $(this).attr('id').replace('tcalico_', '').replace(/Fuel_Form_Details_Delivery_[0-9]+/, 'Fuel_Form_Details_Delivery_' + (numOfProductRows + 1));
					$(this).replaceWith('<img src="<%=ResolveUrl("~/Content/images/calendar/cal.gif")%>" id="tcalico_' + id + '" onclick="A_TCALS[\''+ id + '\'].f_toggle()" class="tcalIcon" alt="Open Calendar"  />');
				}
			});

		   copiedRow.removeAttr("style");
		   copiedRow.insertAfter($(this).parents("table").find("tr:eq(" + (rowIndex + numOfProductRows) + ")"));
		   
		   new tcal({
			   'formname': 'fuelForm',
			   'controlname': id.replace(/__/g, '.').replace(/_[0-9]+/g, function(match, $1, $2, offset, original) { return '[' + match.replace('_', '') + ']' }),
			   'id': id
		   });

			checkFormValidity();
		});

		// remove a delivery row
		$('.removeRow').live('click', function () {
			var rowIndex = $(this).closest("tr").prevAll("tr").length + 1;
			var row = $(this).parents("table").find("tr:eq(" + rowIndex + ")");
			row.remove();
			checkFormValidity();
		});

		// show reason section on modal
		var showReason = function() {
			$(".ui-button-text").each(function (index) {
				if ($(this).text() == "Yes" || $(this).text() == "No") {
					$(this).parent().hide();
				}

				if ($(this).text() == "Ok" || $(this).text() == "Cancel") {
					$(this).parent().show();
				}
			});

			$('.reason').show();
		}

		// modal buttons
		var buttons = {
			No: function () {
				$('.reason').hide();
				$(this).dialog("close");
			},
			Yes: function () {
				showReason();
			},
			Cancel: function () {
				$(".ui-button-text").each(function (index) {
					if ($(this).text() == "Yes" || $(this).text() == "No") {
						$(this).parent().show();
					}

					if ($(this).text() == "Ok" || $(this).text() == "Cancel") {
						$(this).parent().hide();
					}
				});

				$('.reason').hide();
			}
		};

		// enable/disable Ok button
		$(".reason textarea").bind('input propertychange', function() {
			if (this.value.length){
				$(".ui-button:contains('Ok')").button("enable");
			} else {
				$(".ui-button:contains('Ok')").button("disable");
			}
		});

		// validations
		$("#fuelForm").validate({
			errorClass: "input-validation-error",
			errorPlacement: function(error, element) {
				error.hide();
			}
		});

		var requiredValidations = $('input[name*="Supervisor_Full_Name"], input[name*="Supervisor_Badge_Number"]').each(function() {
				$(this).rules('add', { required: true, messages: { required: '' } });
				$(this).valid();
			}).live('input', function () {
				$(this).valid();
				checkFormValidity();
			});

		var requiredDigitValidations = $('input[name*="On_Hand"], input[name*="Spare_Drums"], input[name*="Tank_Delivered"], input[name*="Tank_Dispensed"]')
			.each(function() {
					if ($(this).attr('data_capacity')) {
						var capacity = parseInt($(this).attr('data_capacity'));
						$(this).rules('add', { required: true, digits: true, max: capacity, messages: { required: '', digits: '', max: '' } });
					} else {
						$(this).rules('add', { required: true, digits: true, messages: { required: '', digits: '' } });
					}

					$(this).valid();
				}).live('input', function () {
					$(this).valid();
					checkFormValidity();
				});

		var fuelFormDeliveryValidations = $('input[name*="Fuel_Form_Details_Delivery"], select[name*="Fuel_Form_Details_Delivery"]').live('input change', function() {
			var commonDeliveryName = $(this).attr('name').match(/fuelformDetails\[[0-9]+].Fuel_Form_Details_Delivery\[[0-9]+]/i);

			if (commonDeliveryName && commonDeliveryName.length > 0) {
				if (checkFuelFormDeliveryRow(commonDeliveryName[0])) {
					$('input[name*="' + commonDeliveryName[0] +'"], select[name*="' + commonDeliveryName[0] +'"]').each(function() {
						var name = $(this).attr('name');
                        if (name.match(/Recieve_Date/i)) {
                            $(this).rules('add', { required: true, notGreatherThanToday: true, messages: { required: '', notGreatherThanToday: '' } });
                        } else if (name.match(/Invoice_Number/i)) {
							$(this).rules('add', { required: true, messages: { required: '' } });
						} else if (!name.match(/Details_Id/i) && !name.match(/Order_No/i)) {
							$(this).rules('add', { required: true, digits: true, messages: { required: '', digits: '' } });
						}
						
						$(this).valid();
					});
				} else {
					$('input[name*="' + commonDeliveryName[0] +'"], select[name*="' + commonDeliveryName[0] +'"]').each(function() {
						$(this).rules('remove');
						$(this).valid();
					});
				}

				checkFormValidity();
			}
		});

		$('input[name*=".is_Equipment_Failure"]').live('click', function () {
			var namePrefix = this.name.substr(0, this.name.indexOf('.is_Equipment_Failure'));
			var failureDate = $('input[name="' + namePrefix + '.Failure_Date"]');
			var remarks = $('input[name="' + namePrefix + '.Remarks"]');

			if ($(this).is(':checked')) {
				failureDate.rules('add', { required: true, messages: { required: '' } });
				remarks.rules('add', { required: true, messages: { required: '' } });
			} else {
				failureDate.rules('remove');
				remarks.rules('remove');
			}

			failureDate.valid();    // validate input
			remarks.valid();
			checkFormValidity();
		});

		$('input[name*=".Failure_Date"], input[name*=".Fix_Date"], input[name*=".Remarks"]').live('input change', function () {
			$(this).valid();
			checkFormValidity();
		});

		$('input[name*=".Failure_Date"], input[name*=".Fix_Date"]').each(function () {
			$(this).rules('add', { notGreatherThanToday: true, messages: { notGreatherThanToday: '' } });
		});

		function checkFuelFormDeliveryRow(rowName) {
			var needsValidation = false;

			$('input[name*="' + rowName +'"], select[name*="' + rowName +'"]').each(function() {
				var element = $(this),
					  name = element.attr('name');
				if (name.indexOf('Details_Id') == -1 && name.indexOf('Order_No') == -1 && element.val() !== '' || (element[0].type.indexOf('select') > -1 && element.val() > 0)) {
					needsValidation = true;
				}
			});

			return needsValidation;
		}

        function hasException(exceptions, productId, code) {
            return _.find(exceptions, function (e) {
                return e.item.productId === productId && e.code === code;
            }) !== undefined;
        }
		
		// form submit button click
		$("#submitButton").click(function () {
			$("#fuelForm").validate();

			if ($("#fuelForm").valid() && !submitting) {
				var lastCheckedProdId = 0;

				details = [];   // clear details
				exceptions = [];    // clear exceptions
                errors = [];    // clear errors

				// converts data to json for exception tests
				$("#fuelReqTable").find("tr[productId], tr[subproductId]").each(function (index) {
					var el = $(this),
						  obj = {},
						  prev = (details.length > 0) ? details[details.length -1] : null,
						  prodId = parseInt(el.attr('productId') || el.attr('subproductId'));

					// grab product row
					obj.productId = prodId;
					obj.productName = el.find('td:eq(0)').text().replace(/(\r\n|\n|\r)/gm,'').replace(/\s+/g,' ').replace(/^\s+|\s+$/g, '');
                    obj.equipUserId = parseInt(el.find('input[name*="Equipment_User_ID"]').val(), 10);
					obj.capacity = parseInt(el.find('td:eq(1)').text(), 10);
					obj.qtyOnHand = parseInt(el.find('input[name*="On_Hand"]').val(), 10);
					obj.spareDrums = parseInt(el.find('input[name*="Spare_Drums"]').val(), 10);

					if (el.find('input[name*="Tank_Delivered"]').length > 0) {
						obj.tankDelivered = parseInt(el.find('input[name*="Tank_Delivered"]').val(), 10);
						obj.tankDispensed = parseInt(el.find('input[name*="Tank_Dispensed"]').val(), 10);
						obj.isReadingRequired = true;
					} else {
						obj.isReadingRequired = false;
					}

					// grab delivery details
					obj.vendorId = parseInt(el.find('input[name*="Vendor_Id"]').val(), 10);
					obj.invoiceNumber = el.find('input[name*="Invoice_Number"]').val();
					obj.recieveDate = new Date(el.find('input[name*="Recieve_Date"]').val());
					obj.qty = parseInt(el.find('input[name*="Quantity"]').val(), 10);
					obj.fuelFormDetailsId = parseInt(el.find('input[name*="Fuel_Form_Details_Id"]').val(), 10);

					details.push(obj);
				});

				waterProducts = _.filter(details, function(i) { return i.productName.toLowerCase().indexOf('water') > -1 });

				// check for exceptions
				_.forEach(details, function(d) {
					if (d.productId && d.productName.toLowerCase().indexOf('water') === -1) {
						var prev = _.find(previousFuelForm, function(ff) { return ff.equipUserId ? ff.equipUserId == d.equipUserId : false; }),
							  tankName = d.productName.match(/TANK [0-9]+/i),
							  sumOfQty = _.sum(details, function(de) { return (de.productId === d.productId) ? de.qty : 0; }),
							  sumOfDelivered = _.sum(details, function(de) { return (de.productId === d.productId) ? de.tankDelivered: 0; }),
							  sumOfDispensed =  _.sum(details, function(de) { return (de.productId === d.productId) ? de.tankDispensed: 0; }),
                              dispensedAmount = d.tankDispensed,
							  deliveryAmount = d.tankDelivered,
							  waterProduct = null,
							  prevQtyOnHand = prev && prev.qtyOnHand ? prev.qtyOnHand : 0;
						
						if (tankName !== null && tankName.length > 0) {
							tankName = tankName[0].toLowerCase();
							waterProduct = _.find(waterProducts, function(i) { return i.productName.toLowerCase().indexOf(tankName) > -1 });
						}

						var tankCapacity = (d.capacity * (d.isReadingRequired ? .9 : 1)) - (waterProduct ? waterProduct.qtyOnHand : 0); //- [get water for the same tank name if it exist, if it doesnt this value is zero];

						// book end exception check
						var diff = prevQtyOnHand + deliveryAmount - dispensedAmount,
								comp = d.qtyOnHand - diff,
								deviation = <%= ConfigurationManager.AppSettings["BookEndExceptionDeviation"] %>;

						if (comp >= deviation || comp <= (deviation * -1)) {
							exceptions.push({
								name: 'end-book-issue',
								code: 'EBI',
								prevItem: prev || null,
								item: d
							});
						}
							
<%--                            // expected delivery exception check
							diff = prevQtyOnHand * .95 > d.qtyOnHand < prevQtyOnHand * 1.05;

							if (diff) {
								exceptions.push({
									name: 'delivery-issue',
									code: 'DI',
									prevItem: prev || null,
									item: d
								});
							}

							// delivery exception check
							diff = (prevQtyOnHand + deliveryAmount - (dispensedAmount || 0)) < tankCapacity;
							
							if (diff) {
								exceptions.push({
									name: 'expected-delivery-issue',
									code: 'EDI',
									prevItem: prev || null,
									item: d
								});
							}
--%>

						// tank capacity check
						diff = d.qtyOnHand > tankCapacity;
							
						if (diff) {
						    errors.push({
							    name: 'on-hand-grtr-than-90',
							    prevItem: prev || null,
							    item: d
						    });
						};

                        // only check once per product type
                        if (lastCheckedProdId != d.productId) {
						    // delivery vs qty check
						    diff = sumOfQty > 0 && (sumOfQty * .95 > sumOfDelivered || sumOfQty * 1.05 < sumOfDelivered);

						    if (diff && d.isReadingRequired && !hasException(exceptions, d.productId, 'QDD')) {
							    exceptions.push({
								    name: 'qty-delivery-difference',
								    code: 'QDD',
								    prevItem: prev || null,
								    item: d
							    });
						    }
                        }

						lastCheckedProdId = d.productId;
					}
				});

                if (errors.length > 0) {
                    showErrorModal(errors[0]);
                } else if(exceptions.length > 0) {
                    console.log(exceptions);
					var okCount = 0,
						  reasons = [];

					buttons.Ok = function () {
						$(".ui-button-text").each(function (index) {
							if ($(this).text() == "Ok" || $(this).text() == "Cancel") {
								$(this).parent().hide();
							}

							if ($(this).text() == "Yes" || $(this).text() == "No") {
								$(this).parent().show();
							}

							if ($(this).text() == "Ok") {
								$(this).parent().button("disable");
							}
						});

						reasons.push({
							exception: $('.ui-dialog-title:visible').text() + ': ' + $('.reason:visible textarea').val()
						});

						exceptions[okCount].reason = $('.reason:visible textarea').val();

						$('.reason:visible textarea').val('');
						$('.reason').hide();
						$(this).dialog("close");
						okCount++;
							
						if (okCount === exceptions.length) {
							$('#exceptions').empty();

							// loop over exceptions, creating hidden inputs .net can serialize
							for (var i = 0; i < exceptions.length; i++) {
								var id = 'exceptions_' + i + '__', name = 'exceptions[' + i + '].';
								var fuelFormId = exceptions[i].item.fuelFormDetailsId || '-' + exceptions[i].item.productId;

								$('<input>').attr('id', id + 'categoryCode').attr('name', name + 'categoryCode').attr('type','hidden').val(exceptions[i].code).appendTo('#exceptions');
								$('<input>').attr('id', id + 'exception').attr('name', name + 'exception').attr('type','hidden').val(reasons[i].exception).appendTo('#exceptions');
								$('<input>').attr('id', id + 'tableId').attr('name', name + 'tableId').attr('type','hidden').val(fuelFormId ).appendTo('#exceptions');
							}

							showConfirmSendModal();
						} else {
							showDialog(exceptions[okCount]);
						}
					};

					showDialog(exceptions[0]);
                } else {
                    showConfirmSendModal();
                }
			} 
		});

		function submitForm() {
			var rowCount = 0, deliveryCount = 0, productCount = 0,
					isEdit = ($('#isEdit').val() === 'True'),
					productId = 0, row = null;

			// massage our inputs to match .net's viewmodel
			$(document.forms[0]).find("input[id*='Fuel_Form_Details_Delivery'], select[id*='Fuel_Form_Details_Delivery']").each(function (index) {
				$(this).attr('id', $(this).attr('id').replace(/fuelFormDetails_[[0-9]+]__Fuel_Form_Details_Delivery_[[0-9]+]/, 'fuelFormDetailsDelivery[' + rowCount + ']'));
				$(this).attr('name', $(this).attr('name').replace(/fuelFormDetails\[[0-9]+].Fuel_Form_Details_Delivery\[[0-9]+]/, 'fuelFormDetailsDelivery[' + rowCount + ']'));

				// if creating, set the fuel form details id to the product row
				if (!isEdit && $(this).attr('id').indexOf('Fuel_Form_Details_Id') > -1) {
					row = $(this).parents("table").find("tr:eq(" + ($(this).closest("tr").prevAll("tr").length + 1) + ")");

					if (row.attr("productId") !== undefined && productId != row.attr("productId")) {
						productId = row.attr("productId");
						productCount++;
					}

					$(this).val(productCount);
				}

				// reached end of inputs for that row
				if ($(this).attr('id').indexOf('Quantity') > -1) {
					rowCount++;
				}

				// set order number
				if ($(this).attr('id').indexOf('Order_No') > -1) {
					$(this).val(deliveryCount++);
				}
			});

            $('#submitButton').parent().css('opacity', '.5');
            $('#submitting').show();
            submitting = true;
            animateText('submitting', '.', 3, 750);

			document.forms[0].submit();
		}

		function checkFormValidity() {
			if ($("#fuelForm").valid()) {
				$('#submitButton').parent().css('opacity', '1');
			} else {
				$('#submitButton').parent().css('opacity', '.5');
			}
		};

		function showDialog(ex) {
			if (ex.hasOwnProperty('name')) { 
				var sumOfQty = _.sum(details, function(de) { return (de.productId === ex.item.productId) ? de.qty : 0; }),
					  sumOfDelivered = _.sum(details, function(de) { return (de.productId === ex.item.productId) ? de.tankDelivered: 0; }),
					  sumOfDispensed = _.sum(details, function(de) { return (de.productId === ex.item.productId) ? de.tankDispensed: 0; })    
                      dispensedAmount = ex.item.tankDispensed,
                      deliveredAmount = ex.item.tankDelivered,
                      prevQtyOnHand = ex.prevItem && ex.prevItem.qtyOnHand ? ex.prevItem.qtyOnHand : 0;

				switch(ex.name) {
					case 'end-book-issue':
						var diff = (prevQtyOnHand + deliveredAmount) - ex.item.qtyOnHand;
                        var dispensed = (prevQtyOnHand + deliveredAmount) - ex.item.qtyOnHand;
                        var delivered = prevQtyOnHand - dispensedAmount - ex.item.qtyOnHand;
                        var prodName = ex.item.productName;

                        if (ex.item.productName.indexOf(' (')) {
                            prodName = ex.item.productName.substring(0, ex.item.productName.indexOf(' ('));
                        }

						$("#end-book-product").text(prodName);
						$("#end-book-on-hand-qty").text(ex.item.qtyOnHand);
						$("#end-book-prev-on-hand-qty").text(prevQtyOnHand);
						$("#end-book-delivered").text(deliveredAmount);
						$("#end-book-dispensed").text(dispensedAmount);
						$("#end-book-diff").text(diff > 0 ? dispensed : Math.abs(delivered));
						$("#end-book-diff-type").text(diff > 0 ? 'dispensed' : 'delivered');

                        if ((prevQtyOnHand + deliveredAmount - dispensedAmount) > 0) {
						    $("#end-book-amount").text(" or a quantity on hand of approx (" + (prevQtyOnHand + deliveredAmount - dispensedAmount) + ")");
                        } else {
                            $("#end-book-amount").text("");
                        }

						break;

<%--					case 'delivery-issue':
                        var prodName = ex.item.productName;

                        if (ex.item.productName.indexOf(' (')) {
                            prodName = ex.item.productName.substring(0, ex.item.productName.indexOf(' ('));
                        }

						$("#delivery-issue-amount").text(ex.item.qtyOnHand + deliveryAmount);
						$("#delivery-issue-product").text(prodName);
						$("#delivery-issue-prod-amount").text(prevQtyOnHand - ex.item.qtyOnHand + deliveryAmount);
						//$("#delivery-issue-prod-amount").text(ex.item.qtyOnHand + deliveryAmount);
						break;

					case 'expected-delivery-issue':
                        var prodName = ex.item.productName;

                        if (ex.item.productName.indexOf(' (')) {
                            prodName = ex.item.productName.substring(0, ex.item.productName.indexOf(' ('));
                        }

						$("#expected-delivery-issue-amount").text(deliveryAmount);
						$("#expected-delivery-issue-expected-amount").text(tankCapacity - (prevQtyOnHand - dispensedAmount));
						$("#expected-delivery-issue-product").text(prodName);
						break;--%>

					case 'qty-delivery-difference':
                        var prodName = ex.item.productName;

                        if (prodName.indexOf(' -') > -1) {
                            prodName = prodName.substring(0, prodName.indexOf(' -'));
                        }

                        if (prodName.indexOf(' (') > -1) {
                            prodName = prodName.substring(0, prodName.indexOf(' ('));
                        }

						$("#qty-delivery-difference-product").text(prodName);
						$("#qty-delivery-difference-delivery").text(sumOfDelivered);
						$("#qty-delivery-difference-qty").text(sumOfQty);
						break;
				}

				$("#" + ex.name).dialog({
					resizable: false,
					dragable: false,
					width: 400,
					modal: true,
					buttons: buttons
				});

				// search for previous exceptions
				var prevReason = previousExceptions  ? _.find(previousExceptions, function(e) {
					if (ex.code == e.categoryCode) {
						return _.isObject(_.find(details, function(p) { return p.fuelFormDetailsId == e.tableId && p.productId == ex.item.productId }));
					} else {
						return null;
					}
				}) : undefined;

				if (prevReason !== undefined && prevReason.hasOwnProperty('exception') && prevReason.exception!== '') {
					$('.reason').show();
					$('.reason:visible textarea').val(prevReason.exception.substring(prevReason.exception.indexOf(':') + 1, prevReason.exception.length));

					$(".ui-button-text").each(function (index) {
						if (($(this).text() == "Ok" || $(this).text() == "Cancel")) {
							$(this).parent().show();
						} else {
							$(this).parent().hide();
						}
					});
				} else {
					$('.reason').show();
					$('.reason:visible textarea').val('');
					$('.reason').hide();

					$(".ui-button-text").each(function (index) {
						if (($(this).text() == "Ok" || $(this).text() == "Cancel")) {
							$(this).parent().hide();
						}

						if ($(this).text() == "Ok") {
							$(this).parent().button("disable");
						}
					});
				}
			}
		}

		function showConfirmSendModal() {
			var confirmDialog = $("#confirm-send").dialog({
				autoOpen: true,
				height: 175,
				width: 300,
				modal: true,
				buttons: {
					No: function () {
						confirmDialog.dialog("close");
					},
					Yes: function () {
						confirmDialog.dialog("close");
						submitForm();
					}
				}
			});
		}

		function showErrorModal(error) {
            var prodName = error.item.productName || error.prevItem.productName;
            var tankName = error.item.productName.match(/TANK [0-9]+/i);
            var waterProduct = null;
						
			if (tankName !== null && tankName.length > 0) {
				tankName = tankName[0].toLowerCase();
				waterProduct = _.find(waterProducts, function(i) { return i.productName.toLowerCase().indexOf(tankName) > -1 });
			}

            if (error.item.productName.indexOf(' (')) {
                prodName = error.item.productName.substring(0, error.item.productName.indexOf(' ('));
            }

            var tankCapacity = (error.item.capacity * (error.item.isReadingRequired ? .9 : 1)) - (waterProduct ? waterProduct.qtyOnHand : 0); //- [get water for the same tank name if it exist, if it doesnt this value is zero];

		    $("#on-hand-grtr-than-90-quanty-product").text(prodName);
		    $("#on-hand-grtr-than-90-quanty-on-hand").text(error.item.qtyOnHand);
		    $("#on-hand-grtr-than-90-tank-capacity-at-90").text(tankCapacity);
		    $("#on-hand-grtr-than-90-water")
			    .text(waterProduct && waterProduct.qtyOnHand > 0 ? ' (' + (error.item.capacity *.9) + ' tank capacity minus ' + waterProduct.qtyOnHand + ' water in tank)' : '');

			var errorDialog = $("#" + error.name).dialog({
				autoOpen: true,
				height: 225,
				width: 400,
				modal: true,
				buttons: {
					Ok: function () {
						errorDialog.dialog("close");
					}
				}
			});
		}


		checkFormValidity();
	});