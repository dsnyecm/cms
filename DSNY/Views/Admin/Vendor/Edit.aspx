﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.Data.Vendor>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit <%: Model.Vendor_Name %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Edit <%: Model.Vendor_Name %></h2>
    <p class="dottedLine"></p>

    <div class="adminIconContainer">
        <div class="iconLeft">
            <a href="<%: Url.RouteUrl(new { controller = "Vendor", action = "List"}) %>" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/Cancel.png" alt="Cancel" runat="server" /><br />Cancel
            </a>
        </div>
        <div class="iconLeft"">
            <a onclick="document.forms[0].submit();" href="#" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/create.png" alt="Create" runat="server" /><br />Save Vendor
            </a>
        </div>
    </div>

    <div class="clear"></div>

    <% using (Html.BeginForm()) {%>
        <%: Html.ValidationSummary(true) %>
        
        <fieldset>
            <legend>Vendor Information</legend>
            
            <div class="editor-label"><%: Html.LabelFor(model => model.Vendor_Name) %>:</div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Vendor_Name) %>
                <%: Html.ValidationMessageFor(model => model.Vendor_Name) %>
            </div>
            
            <div class="clear"></div>

            <div class="editor-label"><%: Html.LabelFor(model => model.Account_Number) %>:</div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Account_Number) %>
                <%: Html.ValidationMessageFor(model => model.Account_Number) %>
            </div>
            
            <div class="clear"></div>

            <div class="editor-label"><%: Html.LabelFor(model => model.Email_Address) %>:</div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Email_Address) %>
                <%: Html.ValidationMessageFor(model => model.Email_Address) %>
            </div>
            
            <div class="clear"></div>

            <div class="editor-label"><%: Html.LabelFor(model => model.Phone_Number) %>:</div>
            <div class="editor-field">
                <%: Html.TextBoxFor(model => model.Phone_Number) %>
                <%: Html.ValidationMessageFor(model => model.Phone_Number) %>
            </div>
            
            <div class="clear"></div>

            <div class="editor-label"><%: Html.LabelFor(model => model.is_Active) %>:</div>
            <div class="editor-field">
                <%: Html.CheckBoxFor(model => model.is_Active)%>
                <%: Html.ValidationMessageFor(model => model.is_Active) %>
            </div>
            
            <div class="clear"></div>

        </fieldset>

    <% } %>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 690px;">&nbsp;</div>
</asp:Content>