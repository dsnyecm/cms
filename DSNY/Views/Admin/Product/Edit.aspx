﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.ProductViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit <%: Model.Product.Product_Name %>
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/MicrosoftAjax.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/MicrosoftMvcValidation.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery.validate.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/Tabs.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Edit <%: Model.Product.Product_Name%></h2>
    <p style="width: 875px;" class="dottedLine"></p>

    <div class="adminIconContainer">
        <div class="iconLeft">
            <a href="<%: Url.RouteUrl(new { controller = "Products", action = "List"}) %>" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/Cancel.png" alt="Cancel" runat="server" /><br />Cancel
            </a>
        </div>
        <div class="iconLeft">
            <a id="submitForm" href="#" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/create.png" alt="Create" runat="server" /><br />Save Product
            </a>
        </div>
    </div>

    <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

    <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm(null, null, FormMethod.Post, new { id = "product-form" })) {%>
        <%: Html.ValidationSummary() %>
        <% List<DSNY.Data.Vendor> vendors = (List<DSNY.Data.Vendor>)ViewData["Vendors"]; %>
        
		<ul id="tabnav">
			<li class="selectedTab"><a href="#">Product Information</a></li>
			<li><a href="#">Vendors</a></li>
		</ul>

		<div id="tabsContainer" style="width: 870px;">

			<div class="tabContent">

                <div class="editor-label"><%: Html.LabelFor(model => model.Product.Product_Name)%>:</div>
                <div class="editor-field">
                    <%: Html.HiddenFor(model => model.Product.Product_ID)%>
                    <%: Html.TextBoxFor(model => model.Product.Product_Name)%>
                    <%: Html.ValidationMessageFor(model => model.Product.Product_Name)%>
                </div>

                <div class="clear"></div>

                <div class="editor-label"><%: Html.LabelFor(model => model.Product.Default_Capacity)%>:</div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.Product.Default_Capacity, new { style = "width: 50px;" })%>
                    <%: Html.ValidationMessageFor(model => model.Product.Default_Capacity)%>
                </div>
            
                <div class="clear"></div>

                <div class="editor-label"><%: Html.LabelFor(model => model.Product.Measurement_Type)%>:</div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.Product.Measurement_Type, new { style = "width: 75px;" })%>
                    <%: Html.ValidationMessageFor(model => model.Product.Measurement_Type)%>
                </div>

                <div class="clear"></div>

                <div class="editor-label"><%: Html.LabelFor(model => model.Product.is_Drums)%>:</div>
                <div class="editor-field">
                    <%: Html.CheckBoxFor(model => model.Product.is_Drums)%>
                    <%: Html.ValidationMessageFor(model => model.Product.is_Drums)%>
                </div>

                <div class="clear"></div>

                <div class="editor-label"><%: Html.LabelFor(model => model.Product.is_Water)%>:</div>
                <div class="editor-field">
                    <%: Html.CheckBoxFor(model => model.Product.is_Water)%>
                    <%: Html.ValidationMessageFor(model => model.Product.is_Water)%>
                </div>

                <div class="clear"></div>

                <div class="editor-label"><%: Html.LabelFor(model => model.Product.is_Reading_Required)%>:</div>
                <div class="editor-field">
                    <%: Html.CheckBoxFor(model => model.Product.is_Reading_Required)%>
                    <%: Html.ValidationMessageFor(model => model.Product.is_Reading_Required)%>
                </div>

                <div class="clear"></div>

                <div class="editor-label"><%: Html.LabelFor(model => model.Product.is_Sub_Type)%>:</div>
                <div class="editor-field">
                    <%: Html.CheckBoxFor(model => model.Product.is_Sub_Type)%>
                    <%: Html.ValidationMessageFor(model => model.Product.is_Sub_Type)%>
                </div>

                <div class="clear"></div>

                <div id="sub-types" style="display: none;">
                    <div class="editor-label">Sub Types</div>
                    <div class="editor-field">
                        <% for (int i = 0; i < Model.SubTypes.Count(); i++) { %>
                            <div class="sub-type" style="<%= (i > 0 ? "margin-top: 5px;" : "")%>">
                                <input type="hidden" id="SubTypes_<%: i %>__Product_Sub_Type_ID" name="SubTypes[<%: i %>].Product_Sub_Type_ID" value="<%: Model.SubTypes.ElementAt(i).Product_Sub_Type_ID %>" />
                                <input type="hidden" id="SubTypes_<%: i %>__Product_ID" name="SubTypes[<%: i %>].Product_ID" value="<%: Model.SubTypes.ElementAt(i).Product_ID%>" />
                                <input type="text" id="SubTypes_<%: i %>__Sub_Type" name="SubTypes[<%: i %>].Sub_Type" value="<%: Model.SubTypes.ElementAt(i).Sub_Type %>" />
                                <% if (i == 0) { %>
					                <img border="0" class="add pointer" style="height: 16px; width: 16px;" src="<%=ResolveUrl("~/Content/Images/Icons/add.png")%>" />    
                                <% } else { %>
                                    <img border="0" class="remove pointer" style="height: 16px; width: 16px;" src="<%=ResolveUrl("~/Content/Images/Icons/delete.png")%>" />
                                <% } %>
                            </div>
                        <% } %>

                        <% if (Model.SubTypes.Count() == 0) { %>
                            <div class="sub-type">
                                <input type="hidden" id="SubTypes_0__Product_Sub_Type_ID" name="SubTypes[0].Product_Sub_Type_ID" value="" />
                                <input type="hidden" id="SubTypes_0__Product_ID" name="SubTypes[0].Product_ID" value="" />
                                <input type="text" id="SubTypes_0__Sub_Type" name="SubTypes[0].Sub_Type" value="" />
					            <img border="0" class="add pointer" style="height: 16px; width: 16px;" src="<%=ResolveUrl("~/Content/Images/Icons/add.png")%>" />
                            </div>
                        <% } %>

                        <div class="template" style="display: none;">
                            <input id="Product_Sub_Type_ID" type="hidden" />
                            <input id="Product_ID" type="hidden" />
                            <input id="Sub_Type" type="text" />
					        <img border="0" class="remove pointer" style="height: 16px; width: 16px;" src="<%=ResolveUrl("~/Content/Images/Icons/delete.png")%>" />
                        </div>
                    </div>
                </div>

                <div class="clear"></div>

                <div class="editor-label"><%: Html.LabelFor(model => model.Product.Order_Num)%>:</div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.Product.Order_Num, new { style = "width: 30px;" })%>
                    <%: Html.ValidationMessageFor(model => model.Product.Order_Num)%>
                </div>

                <div class="clear"></div>

                <div class="editor-label"><%: Html.LabelFor(model => model.Product.is_Active)%>:</div>
                <div class="editor-field">
                    <%: Html.CheckBoxFor(model => model.Product.is_Active)%>
                    <%: Html.ValidationMessageFor(model => model.Product.is_Active)%>
                </div>

                <div class="clear"></div>

                <div class="editor-label"><%: Html.LabelFor(model => model.Product.Order_Delivery_Distribution)%>:</div>
                <div class="editor-field">
                    <%: Html.TextBoxFor(model => model.Product.Order_Delivery_Distribution)%>
                </div>

                <div class="clear"></div>
			</div>

			<div class="clear"></div>

			<div class="tabContent">
				<div style="float: left; padding-left: 25px;">
				<% if (vendors != null && vendors.Count() > 0) { %>
					<div style="float: left;">
						<h3 style="margin-top: 5px;">Available Vendors</h3>
                        <%: Html.DropDownList("vendorsAvailable", 
                            new SelectList(vendors.Where(v => Model.Product.Product_Vendor.SingleOrDefault(pv => pv.Vendor_ID == v.Vendor_ID) == null),
                            "Vendor_ID", "Vendor_Name"), new { @class = "selectList", @multiple = "multiple" })%>
					</div>
					<div style="width: 75px; height: 175px; padding-top: 100px; padding-left: 25px; float: left;">
						<a href="#" style="margin-bottom: 50px;">
							<img id="addVendor" border="0" src="~/Content/Images/Icons/moveRight.png" alt="Add Vendor" runat="server" />
						</a>
						<div style="height: 50px;"></div>
						<a href="#">
							<img id="removeVendor" border="0" src="~/Content/Images/Icons/moveLeft.png" alt="Add Vendor" runat="server" />
						</a>
					</div>
					<div style="float: left;">
						<h3 style="margin-top: 5px;">Selected Vendors</h3>
						<%: Html.DropDownList("vendorsSelected", 
                            new SelectList(vendors.Where(v => Model.Product.Product_Vendor.SingleOrDefault(pv => pv.Vendor_ID == v.Vendor_ID) != null),
                            "Vendor_ID", "Vendor_Name"), new { @class = "selectList", @multiple = "multiple" })%>
					</div>
				<% } %>
				</div>

                <div class="clear"></div>
            </div>
        </div>
    <% } %>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 690px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="JavascriptSource" runat="server">
	<script type="text/javascript">
	    $(document).ready(function () {
	        var submitting = false;
	        var subTypeCount = <%: Model.SubTypes.Count() > 0 ? Model.SubTypes.Count() : 1%> -1; // 0 index

	        $("#tabsContainer").tabs({ tab: 1 });

	        // inital sort
	        SortListbox("vendorsAvailable");
	        SortListbox("vendorsSelected");

	        // takes all selected options in 'usersAvailable' and moves to 'usersSelected'
	        $("#MainContent_addVendor").click(function () {
	            $('#vendorsAvailable option:selected').appendTo('#vendorsSelected');
	            $('#vendorsAvailable option:selected').remove();

	            SortListbox("vendorsAvailable");
	            SortListbox("vendorsSelected");
	        });

	        // takes all selected options in 'usersSelected' and moves to 'usersAvailable'
	        $("#MainContent_removeVendor").click(function () {
	            $('#vendorsSelected option:selected').appendTo('#vendorsAvailable');
	            $('#vendorsSelected option:selected').remove();

	            SortListbox("vendorsAvailable");
	            SortListbox("vendorsSelected");
	        });

	        $("#product-form").validate({
	            errorClass: "input-validation-error",
	            errorPlacement: function (error, element) {
	                error.hide();
	            }
	        });

	        function checkFormValidity() {
	            if ($("#product-form").valid()) {
	                $('#submitForm').parent().css('opacity', '1');
	            } else {
	                $('#submitForm').parent().css('opacity', '.5');
	            }
	        };

	        $('input[name*=".Sub_Type"]').each(function (index) {
	            $(this).rules('add', { required: true, messages: { required: '' } });
	        }).live('input', function () {
	            $(this).valid();
	            checkFormValidity();
	        });

	        if ($("#Product_is_Sub_Type").is(':checked')) {
	            $("#sub-types").show();
	        } else {
	            $('input[name*=".Sub_Type"]').rules('remove');
	            $('input[name*=".Sub_Type"]').valid();
	        }

	        checkFormValidity();

	        $("#sub-types img").live("click", function () {
	            var el = $(this);
	            if (el.hasClass("add")) {
	                addSubType();
	            }

	            if (el.hasClass("remove")) {
	                if (subTypeCount > 0) {
	                    subTypeCount--;

	                    el.parent('div').remove();

	                    if (subTypeCount === 0) {
	                        $('input[name*=".Sub_Type"]').rules('remove');
	                        $('input[name*=".Sub_Type"]').valid();
	                    }

	                    checkFormValidity();
	                }
	            }
	        });

	        function addSubType() {
	            // copy subtype template
	            subTypeCount++;
	            var id = "SubTypes_" + subTypeCount + "__";
	            var name = "SubTypes[" + subTypeCount + "].";
	            var subTypeTemplate = $("#sub-types .editor-field .template").clone();

	            subTypeTemplate.attr("style", "margin-top: 5px;").attr("class", "sub-type");
	            subTypeTemplate.find("input#Product_Sub_Type_ID").attr("id", id + "Product_Sub_Type_ID").attr("name", name + "Product_Sub_Type_ID").val("0");
	            subTypeTemplate.find("input#Product_ID").attr("id", id + "Product_ID").attr("name", name + "Product_ID").val("<%: Model.Product.Product_ID %>");
	            subTypeTemplate.find("input#Sub_Type").attr("id", id + "Sub_Type").attr("name", name + "Sub_Type");
	            $("#sub-types .editor-field").append(subTypeTemplate);
	            
	            subTypeTemplate.find("input#" + id + "Sub_Type").rules('add', { required: true, messages: { required: '' } });
	            checkFormValidity();
	        }

	        $("#Product_is_Sub_Type").change(function () {
	            if (this.checked) {
	                $('input[name*=".Sub_Type"]').each(function (index) {
	                    $(this).rules('add', { required: true, messages: { required: '' } });
	                });

	                $("#sub-types").show();
	            } else {
	                $('input[name*=".Sub_Type"]').each(function (index) {
	                    $(this).rules('remove');
	                });
	                $("#sub-types").hide();
	            }

	            checkFormValidity();
	        });

	        $("#submitForm").click(function () {
	            if ($('#product-form').valid() && !submitting) {
	                var subTypes = $('.sub-type');
	                var updateSubType = function (el, index) {
	                    var id = "SubTypes_" + index + "__";
	                    var name = "SubTypes[" + index + "].";

	                    if (el.find('input[name*=".Sub_Type"]').val() === '') {
	                        el.remove();
	                    } else {
	                        el.find('input[name*=".Product_Sub_Type_ID"]').attr("id", id + "Product_Sub_Type_ID").attr("name", name + "Product_Sub_Type_ID");
	                        el.find('input[name*=".Product_ID"]').attr("id", id + "Product_ID").attr("name", name + "Product_ID");
	                        el.find('input[name*=".Sub_Type"]').attr("id", id + "Sub_Type").attr("name", name + "Sub_Type");
	                    }
	                }

	                // update order of sub types
	                if (subTypes.length === 1) {
	                    var el = $(subTypes[0]);
	                    var input = el.find('input[name*=".Sub_Type"]').attr("id", "SubTypes_0__Sub_Type").attr("name", "SubTypes[0].Sub_Type");

	                    if (input.val() === '') {
	                        subTypes.remove();
	                    } else {
	                        updateSubType(el, 0);
	                    }

	                } else {
	                    subTypes.each(function (index) {
	                        updateSubType($(this), index);
	                    });
	                }

	                $('#vendorsSelected option').attr('selected', 'selected');

	                $('#submitForm').parent().css('opacity', '.5');
	                $('#submitting').show();
	                submitting = true;
	                document.forms[0].submit();
	            }
	        });
	    });
    </script>
</asp:Content>