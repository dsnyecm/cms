﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<DSNY.Data.Product>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Products
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery.tablesorter.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!--<h2>Products&nbsp;&nbsp;&nbsp;[<a href="#" onclick="helpPopup('ManageProduct')">?</a>]</h2>-->
    <h2>Products</h2>
    <p style="width: 770px;" class="dottedLine"></p>

    <div id="productGrid" style="width: 775px;">
        <div class="iconLeft">
            <a href="<%: Url.RouteUrl(new { controller = "Admin", action = "Index"}) %>" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/back.png" alt="Admin List" runat="server" /><br />Admin<br />Menu
            </a>
        </div>
        <div class="iconLeft">
            <a href="<%: Url.RouteUrl(new { controller = "Product", action = "Create"}) %>" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/create.png" alt="Create Product" runat="server" /><br />Create Product
            </a>
        </div>

        <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <% Html.RenderPartial("Grids/ProductGrid"); %>
    </div>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 710px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            // set up our table sorter
            $("#productTable").tablesorter({ widgets: ["zebra"],
                widgetZebra: { css: ["alt", ""] }
            });

            // AJAX event which deletes the selected item
            $(".deleteButton").live('click', function () {
                var data = { id: $(this).attr("id"), name: $(this).attr("name") };

                if (confirm('Press \'OK\' to delete ' + data.name + '.')) {

                    $.get("/Products/Delete/", data, function (data) {
                        if (data.status == "success") {
                            location.reload();
                        }
                        else {
                            $("#errorMessage").html("There was an error deleting " + data.name + ".<br /><br />");
                        }
                    }, 'json');
                }
            });
        });
    </script>
</asp:Content>