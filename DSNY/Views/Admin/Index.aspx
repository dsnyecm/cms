﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Site Administration
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.5.custom.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/jqueryUI.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Site Administration</h2>
    <p class="dottedLine" style="width: 510px;"></p>

    <div>
        <% if (Roles.IsUserInRole("System Administrator")) { %>
                <div style="width: 250px; float: left; margin-left: 10px;">
                    <h3 style="margin: 0;">User Administration</h3>
                    <ul>
                        <li><%: Html.ActionLink("Manage Users", "List", "Users") %></li>
                        <li><%: Html.ActionLink("Manage Message Distribution", "MessageDist", "Users")%></li>
                        <li><%: Html.ActionLink("Manage E-mail Distribution", "EmailDist", "Users")%></li>
                        <li><%: Html.ActionLink("View Roles", "List", "Roles") %></li>
                    </ul>
                </div>
                <div style="width: 250px; float: left;">
                    <h3 style="margin: 0;">Product / Equipment Administration</h3>
                    <ul>
                        <li><%: Html.ActionLink("Manage Products", "List", "Products") %></li>
                        <li><%: Html.ActionLink("Manage Equipment", "List", "Equipment") %></li>
                        <li><%: Html.ActionLink("Manage Vendors", "List", "Vendor") %></li>
                    </ul>
                </div>

                <div class="clear"></div>
                
                <div style="width: 250px; float: left; margin-left: 10px; padding-top: 10px;">
                    <h3 style="margin: 0;">Purchase Orders</h3>
                    <ul>
                        <li>
                            <%: Html.ActionLink("Process Purchase Order", "Create", "PurchaseOrder", null, new { @id= "purchase-order" }) %>
                            <div id="loading" style="display: none;">Loading</div>
                            <% if (ViewData["purchaseOrder"] != null) {
                                    DSNY.Data.Purchase_Orders po = (DSNY.Data.Purchase_Orders) ViewData["purchaseOrder"];
                            %>
                            <div>Last processed on <%= po.Order_Date.Value.ToShortDateString() %></div>
                            <% } %>
                        </li>
                    </ul>
                </div>
                <div style="width: 250px; float: left; padding-top: 10px;">
                    <h3 style="margin: 0;">Data Correction</h3>
                    <ul>
                        <li><%: Html.ActionLink("Modify Fuel Form", "Search", "FuelForm") %></li>
                        <li><%: Html.ActionLink("Modify Daily Weather Report", "Search", "DailyReport") %></li>
                        <li><%: Html.ActionLink("View Application Logs Report", "Search", "Log") %></li>
                    </ul>
                </div>

                <div class="clear"></div>
        <% }

        if (Roles.IsUserInRole("Reports")) { %>
            <div style="width: 250px; float: left; margin-left: 10px; padding-top: 10px;">
                <h3 style="margin: 0; padding-bottom: 10px;">Reports</h3>
                <select id="reportSelect" onchange="if (this.selectedIndex > 0) { window.location='/Reports/reports.aspx?report=' + this.options[this.selectedIndex].value; }">
                    <option value="">Select Report</option>
                    <% foreach (string report in ConfigurationManager.AppSettings["Reports"].ToString().Split(';'))
                        { %>
                        <option value="<%: report %>"><%: report.Replace('_', ' ')%></option>       
                    <% } %>
                </select>
            </div>
        <% } %>

        <div class="clear"></div>
    </div>

    <div id="po-confirm-send" class="modal" title="Purchase Order Successfully Sent" style="overflow: hidden;">
		<p>
            The purchase order was successfully sent.
		</p>
	</div>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 550px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="JavascriptSource" runat="server">

    <script type="text/javascript">
        $(document).ready(function () {
            $("#purchase-order").click(function () {
                $("#loading").show();
                animateText('loading', '.', 3, 750);
            });

            if (window.location.search.indexOf('poSent=true') > -1) {
                var confirmDialog = $("#po-confirm-send").dialog({
                    autoOpen: true,
                    height: 175,
                    width: 300,
                    modal: true,
                    buttons: {
                        Ok: function () {
                            confirmDialog.dialog("close");
                        }
                    }
                });
            }
        });
    </script>
</asp:Content>