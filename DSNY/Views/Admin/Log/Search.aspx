﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.LogViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Fuel Forms
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="JavascriptIncludes" runat="server">
	<script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.5.custom.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery.validate.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/lodash.3.9.3.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/Tabs.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
	<link href="<%=ResolveUrl("~/Content/jqueryUI.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="server">

	<h2>View Application Logs Report</h2>
	<p style="width: 1180px;" class="dottedLine"></p>

		<div style="float: left; width: 1175px;">
			Search application logs by date.
		</div>

		<% using (Html.BeginForm("Search", "Log", FormMethod.Post, new { id = "logs" })) {%>
			<div style="float: left; margin-bottom: 10px; width: 1175px;">
				<%                     
					DateTime formDate = new DateTime();

					if (Model.searchDate != null)
						formDate = DateTime.Parse(Model.searchDate.ToString());
				%>
				<div class="display-label" style="margin-top: .9em; font-weight: bold;"><label for="searchDate">Date</label>: </div>
				<div class="display-field" style="margin: 0.9em 0; width: 160px;">
					<input type="text" id="searchDate" name="searchDate" value="<%=  formDate.Date.Year != 1 ? formDate.Date.ToString("MM/dd/yyyy") : "" %>" style="width: 70px;" size="10" readonly />
					<script type="text/javascript">
						new tcal({ 'formname': 'logs', 'controlname': 'searchDate', 'id': 'searchDate' });
					</script> 
				</div>
				<div class="display-label" style="margin-top: .9em; text-align: left;"">
					<button id="submit-form" type="button" onclick="$('#searchDate').val() !== '' ? this.form.submit() : null;">Search</button>
				</div>
			</div>
		<% } %>

		<div class="clear"></div>

		<% if (Model != null && Model.logs != null && Model.logs.Count > 0) { %>
				<% Html.RenderPartial("Grids/LogGrid", Model.logs); %>
		<% } else if (Model != null) { %>
			<h2 style="padding-left: 40px;">No logs found</h2>
		<% } %>
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="FooterContent" runat="server">
	<div id="noFloat" style="width: 1200px;">&nbsp;</div>
</asp:Content>