﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.FuelFormViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Fuel Forms
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="JavascriptIncludes" runat="server">
	<script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.5.custom.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery.validate.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/lodash.3.9.3.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/Tabs.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
	<link href="<%=ResolveUrl("~/Content/jqueryUI.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Content/fuelformPrint.css")%>" rel="stylesheet" type="text/css" media="print" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="server">

	<!--<h2>
        Data Correction: Fuel Requirements and Equipment Form
        <span class="page-title-help" >&nbsp;&nbsp;&nbsp;[<a href="#" onclick="helpPopup('FuelRequirements')">?</a>]</span>
	</h2>-->
	<h2>
        Data Correction: Fuel Requirements and Equipment Form
    </h2>

	<p style="width: 1130px;" class="dottedLine"></p>

	<% if (Model != null && Model.fuelForm != null) { %>
		<div style="float: left; width: 1130px;">
			<div class="iconLeft">
				<a href="<%: Url.Action("Search", "FuelForm") %>" class="iconLink">
					<img border="0" src="~/Content/Images/Icons/cancel.png" alt="Cancel" runat="server" /><br />Cancel
				</a>                   
			</div>
			<div class="iconLeft">
				<a href="#" id="submitButton" class="iconLink">
					<img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Submit" style="vertical-align: bottom;" runat="server" /><br />Submit
				</a>
			</div>
            <div id="submitting" style="display: none; padding-top: 15px;">Submitting</div>
		</div>
	<% } else { %>
		<div style="float: left; width: 1130px;">
			Search for a fuel form by selecting a location and date.
		</div>

		<% using (Html.BeginForm("Search", "FuelForm", FormMethod.Post, new { id = "fuelForm" })) {%>
			<div style="float: left; margin-bottom: 10px; width: 1175px;">
				<div class="display-label" style="margin-top: .9em; width: 100px; font-weight: bold;"><label for="location">Location</label>: </div>
				<div class="display-field" style="margin: 0.9em 0; width: 125px;">
					<%                     
						List<DSNY.Core.Interfaces.IUser> users = (List<DSNY.Core.Interfaces.IUser>)ViewData["users"];
						SelectList selectUsers = new SelectList(users, "userId", "userName");

						if (ViewData["userId"] != null)
							selectUsers.First(u => u.Value == ViewData["userId"].ToString()).Selected = true;
						
						DateTime formDate = new DateTime();

						if (ViewData["formDate"] != null)
							formDate = DateTime.Parse(ViewData["formDate"].ToString());
					%>

					<%: Html.DropDownList("userId", selectUsers)%>
				</div>

				<div class="display-label" style="margin-top: .9em; font-weight: bold;"><label for="formDate">Date</label>: </div>
				<div class="display-field" style="margin: 0.9em 0; width: 160px;">
					<input type="text" id="formDate" name="formDate" value="<%=  formDate.Date.Year != 1 ? formDate.Date.ToString("MM/dd/yyyy") : "" %>" style="width: 70px;" size="10" readonly />
					<script type="text/javascript">
						new tcal({ 'formname': 'fuelForm', 'controlname': 'formDate', 'id': 'formDate' });
					</script> 
				</div>
				<div class="display-label" style="margin-top: .9em; text-align: left;"">
					<button id="submit-form" type="button" onclick="$('#formDate').val() !== '' ? this.form.submit() : null;">Search</button>
				</div>
			</div>
		<% }
		} %>

		<div class="clear"></div>

		<% if (Model != null && Model.fuelForm != null) { %>
			<% using (Html.BeginForm("Edit", "FuelForm", FormMethod.Post, new { id = "fuelForm" })) {%>
				<%: Html.HiddenFor(m => m.isAdmin, new { @Value = true }) %>
				<%: Html.HiddenFor(m => m.isEdit) %>
				<div style="float: left; margin-bottom: 10px; width: 1130px;">
					<div class="display-label" style="margin-top: .9em; width: 100px; font-weight: bold;"><label for="locationId">Location Id</label>: </div>
					<div class="display-field" style="margin: 0.9em 0; width: 125px;">
						<%: Model.submittedBy.userName %>
					</div>

					<div class="display-label" style="margin-top: .9em; font-weight: bold;"><label for="Supervisor_Full_Name">Supervisor</label>: </div>
					<div class="display-field" style="margin: 0.9em 0; width: 160px;">
						<%: Model.fuelForm.Supervisor_Full_Name %>
					</div>

					<div class="display-label" style="margin-top: .9em; font-weight: bold; width: 150px;"><label for="Supervisor_Badge_Number">Badge Number</label>: </div>
					<div class="display-field" style="margin: 0.9em 0; width: 185px;">
						<%: Model.fuelForm.Supervisor_Badge_Number %>
					</div>

					<div id="submission-date" style="margin-top: .9em;"><%: Model.fuelForm.Submission_Date.Value.ToString("M/d/yyyy h:mm tt")  %></div>
				</div>

				<div class="clear"></div>

				<ul id="tabnav">
					<li class="selectedTab"><a href="#" onclick="f_tcalHideAll();">Fuel Requirements</a></li>
					<li>
					<% if (Model.fuelFormEquipFailure.Exists(ef => ef.is_Equipment_Failure)) { %>
						<a href="#" class="highlight" onclick="f_tcalHideAll();">* Equipment Failure *</a>
					<% } else { %>
						<a href="#" onclick="f_tcalHideAll();">Equipment Failure</a>
					<% } %>
					</li>

					<li>
					<% if (Model.fuelForm.Remarks != null) { %>
						<a href="#" class="highlight" onclick="setTimeout(function () { $('#Remarks').focus() }, 10); f_tcalHideAll();">* General Remarks *</a>
					<% } else { %>
						<a href="#" onclick="setTimeout(function () { $('#Remarks').focus() }, 10); f_tcalHideAll();">General Remarks</a>
					<% } %>
					</li>
				</ul>

				<div id="tabsContainer" style="width: 1130px;">
                    <div class="tabHeader">Fuel Requirements</div>
					<div class="tabContent">
						<% Html.RenderPartial("Grids/FuelReqEditGrid", Model); %>
					</div>

                    <div class="tabHeader">Equipment Failure</div>
					<div class="tabContent">
						<% Html.RenderPartial("Grids/EquipFailEditGrid"); %>
					</div>

                    <div class="tabHeader">General Remarks</div>
					<div class="tabContent">
						<%: Html.TextAreaFor(model => model.fuelForm.Remarks, new { @style= "width: 1070px;", @rows = "20", @maxlength = "1000" })%>
						<%: Html.HiddenFor(model => model.fuelForm.Fuel_Form_ID) %>
					</div>
				</div>

				<div class="iconContainer">
					<div id="errorMessage" style="float: right; display: none; color: red; font-weight: bold; margin: 30px 20px 0;">
						Please fill in all required fields including any partially completed rows.
					</div>
				</div>
				<%: Html.HiddenFor(m => m.isAdmin, new { @Value = true }) %>
				<%: Html.HiddenFor(model => model.fuelForm.Submission_Date) %>
				<% Html.RenderPartial("Modals/FuelForm"); %>
			<% } %>
		<% } else if (Model != null) { %>
			<h2 style="padding-left: 40px;">No fuel form found</h2>
		<% } %>

		<%if (ViewData["saved"] != null && (bool)ViewData["saved"] == true) {%>
			<h2 style="padding-left: 40px;">Fuel form was updated</h2>
		<% } %>
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="FooterContent" runat="server">
	<div id="noFloat" style="width: 1200px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content9" ContentPlaceHolderID="JavascriptSource" runat="server">
	<script type="text/javascript">
		var previousFuelForm = <%= (ViewData["PreviousFuelFormDetails"] != null ? ViewData["PreviousFuelFormDetails"] : "null") %>;
		var previousExceptions = <%= (ViewData["PreviousExceptions"] != null ? ViewData["PreviousExceptions"] : "null") %>;

		<% Html.RenderPartial("Javascript/FuelForm"); %>
	</script>
</asp:Content>