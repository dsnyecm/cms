﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.EditUserModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edit <%: Model.userName %>
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
	<script src="<%=ResolveUrl("~/Scripts/MicrosoftAjax.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/MicrosoftMvcValidation.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery.validate.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/Tabs.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<h2>Edit <%: Model.userName %>'s Information</h2>
	<p style="width: 875px;" class="dottedLine"></p>

	<div class="iconContainer">
		<div class="iconLeft">
			<a href="<%: Url.RouteUrl(new { controller = "User", action = "List" }) %>" class="iconLink">
				<img border="0" src="~/Content/Images/Icons/Cancel.png" alt="Cancel" runat="server" /><br />Cancel
			</a>
		</div>
		<div class="iconLeft">
			<a href="#" id="submitForm" class="iconLink">
				<img border="0" src="~/Content/Images/Icons/create.png" alt="Save" runat="server" /><br />Save User
			</a>
		</div>
        <div id="submitting" style="display: none; padding-top: 15px;">Submitting...</div>
	</div>

	<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

	<% using (Html.BeginForm(null, null, FormMethod.Post, new { id = "user-form" })) {%>
		<%: Html.ValidationSummary(true) %>

		<ul id="tabnav">
			<li class="selectedTab"><a href="#">User Information</a></li>
			<li><a href="#">Command Chain</a></li>
			<li><a href="#">Products</a></li>
			<li><a href="#" id="equipmentTab">Equipment</a></li>
		</ul>

		<div id="tabsContainer" style="width: 870px;">

			<div class="tabContent">
			   <%: Html.EditorFor(m => m) %>
			</div>

			<div class="tabContent">
				<div style="padding-left: 25px;">
				<% if (ViewData["Users"] != null && ((List<DSNY.Core.Interfaces.IUser>)ViewData["Users"]).Count > 0) { %>
					<div style="float: left;">
						<h3>Available Users</h3>
						<select id="usersAvailable" multiple="multiple" class="selectList">
						<% List<DSNY.Core.Interfaces.IUser> tempUsers = (List<DSNY.Core.Interfaces.IUser>)ViewData["Users"];
								  List<DSNY.Core.Interfaces.IUser> users = tempUsers.OrderBy(u => u.description).ThenBy(u => u.userName).ToList();
								  foreach (DSNY.Core.Interfaces.IUser user in users)
								  {
									  if (!Model.userCommandChain.Exists(u => u.userId == user.userId))
									  {
										  if (!string.IsNullOrEmpty(user.description))
										  { %>
										<option value="<%: user.userId %>"><%: user.description%> (<%: user.userName%>)</option>
								<% }
								  else
								  { %>
										<option value="<%: user.userId %>"><%: user.userName%></option>
								<% }
								  }  %>
					   <% } %>
						</select>
					</div>
					<div style="width: 75px; height: 175px; padding-top: 100px; padding-left: 25px; float: left;">
						<a href="#" style="margin-bottom: 50px;">
							<img id="addUser" border="0" src="~/Content/Images/Icons/moveRight.png" alt="Add User" runat="server" />
						</a>
						<div style="height: 50px;"></div>
						<a href="#">
							<img id="removeUser" border="0" src="~/Content/Images/Icons/moveLeft.png" alt="Add User" runat="server" />
						</a>
					</div>
					<div style="float: left;">
						<h3>Users who report to <%: Model.userName %></h3>
						<%: Html.DropDownList("usersSelected", (SelectList)ViewData["SelectedUsers"], new { @class = "selectList", @multiple = "multiple" })%>
					</div>
				<% } %>
				</div>
			</div>

			<div class="clear"></div>

			<% List<DSNY.Data.Product> products = (List<DSNY.Data.Product>)ViewData["Products"]; %>

			<div class="tabContent">
				<div >
				<% if (ViewData["Products"] != null && ((List<DSNY.Data.Product>)ViewData["Products"]).Count > 0) {
                        int prodCount = 0; %>
				    <table>
					    <tr>
						    <th style="width: 450px;">Product</th>
						    <th style="width: 60px;">Capacity</th>
                            <th style="width: 150px;">Sub Type</th>
					    </tr>
				    <% foreach (DSNY.Data.Product prod in products) {
                            DSNY.Data.Product_User userProduct = null; %>
							<tr class="<%: prodCount % 2 == 0 ? "alt" : string.Empty %>">
							<%  // If we have a postback, use that value, otherwise check if the model has a value, otherwise blank
                                if (Request["product" + prod.Product_ID] != null) {
                                    int tempInt;
                                    userProduct = new DSNY.Data.Product_User() { Product_Sub_Type_Id = int.TryParse(Request["prodSubType" + prod.Product_ID], out tempInt) ? (int?)tempInt : null }; %>
								<td>
									<input type="checkbox" checked="checked" id="product<%: prod.Product_ID %>" name="product<%: prod.Product_ID %>" />
                                    <%: prod.Product_Name%>
								</td>
								<td style="text-align: center;">
									<input type="text" id="prodQty<%: prod.Product_ID %>" name="prodQty<%: prod.Product_ID %>" style="width: 50px;"
									    value="<%: Request["prodQty" + prod.Product_ID] != null ? Request["prodQty" + prod.Product_ID] : string.Empty  %>" />
								</td>
							<% } else if (Model.userProducts != null && Model.userProducts.Exists(up => up.Product_ID == prod.Product_ID)) {
                                    userProduct = Model.userProducts.SingleOrDefault(pu => pu.Product_ID == prod.Product_ID); %>
								<td>
									<input type="checkbox" checked="checked" id="product<%: prod.Product_ID %>" name="product<%: prod.Product_ID %>" />
                                    <%: prod.Product_Name%>
								</td>
								<td style="text-align: center;">
									<input type="text" id="prodQty<%: prod.Product_ID %>" name="prodQty<%: prod.Product_ID %>" style="width: 50px;" value="<%: userProduct.Capacity %>" />
								</td>
							<% } else { %>
								<td>
									<input type="checkbox" id="product<%: prod.Product_ID %>" name="product<%: prod.Product_ID %>" />&nbsp;<%: prod.Product_Name%>
								</td>
								<td style="text-align: center;">
									<input type="text" id="prodQty<%: prod.Product_ID %>" name="prodQty<%: prod.Product_ID %>" style="width: 50px;" />
								</td>
							<% } %>
                                <td>
                                    <% if (prod.is_Sub_Type && prod.Product_Sub_Type.Count() > 0) { %>
								        <select name="prodSubType<%: prod.Product_ID %>" style="width: 100%;">
                                            <option value="" disabled="disabled" selected="selected"></option>
									    <% foreach (DSNY.Data.Product_Sub_Type st in prod.Product_Sub_Type) { 
									        if (userProduct != null && userProduct.Product_Sub_Type_Id == st.Product_Sub_Type_ID) { %>
											    <option value="<%: st.Product_Sub_Type_ID%>" selected="selected"><%: st.Sub_Type%></option>
										<% } else { %>
											    <option value="<%: st.Product_Sub_Type_ID %>"><%: st.Sub_Type%></option>
										<% } %>
									<% } %>
									    </select>
                                    <% } %>
                                </td>
						</tr>
					<% prodCount++;
								  } %>
					</table>
				<% } else { %>
					<h4>There are no products.</h4>
				<% } %>
				</div>
			</div>

			<div class="clear"></div>

			<div class="tabContent" style="width: 870px;">
				<% if (ViewData["Equipment"] != null && ((List<DSNY.Data.Equipment>)ViewData["Equipment"]).Count > 0) { %>
					<table id="equipmentGrid" style="margin-right: 10px; width: 95%;">
						<tr>
							<th style="width: 160px;">Equipment</th>
							<th style="width: 160px;">Description</th>
							<th style="width: 221px;">Related Product</th>
							<th style="width: 60px;">Capacity</th>
							<th style="width: 52px;">Active</th>
							<th style="width: 43px; border: none; background: white;"></th>
						</tr>
					<% List<DSNY.Data.Equipment> equipment = (List<DSNY.Data.Equipment>)ViewData["Equipment"];
								  DSNY.Data.Equipment selectedEquipment = null;
								  List<DSNY.Data.Equipment_User> equipmentUser = (List<DSNY.Data.Equipment_User>)ViewData["EquipmentUser"];
								  int equipCount = 1; %>

						<tr id="firstInputRow" style="display: none;" >
							<td>
								<input type="hidden" name="equipUserId" value="-1" />
								<select name="equipType" class="equipType" style="width: 150px;">
									<option value="">SELECT EQUIPMENT</option>
									<% foreach (DSNY.Data.Equipment equip in equipment) { %>
											<option value="<%: equip.Equipment_ID %>"><%: equip.Equipment_Name%></option>
									<% } %>
								</select>
							</td>
							<td><input type="text" name="equipDescript" value="" style="width: 150px" /></td>
							<td>
								<select name="equipProd" style="width: 250px;">
									<option value="">SELECT PRODUCT</option>
									<% foreach (DSNY.Data.Product prod in products) { %>
											<option value="<%: prod.Product_ID %>"><%: prod.Product_Name%></option>
									<% } %>
								</select>
							</td>
							<td style="text-align: center;">
								<input type="text" name="equipCapacity" value="" style="width: 50px; display: none;" />
							</td>
							<td style="text-align: center;">
								<input type="checkbox" name="equipActive" class="activeCheckbox" checked="checked" />
								<input type="hidden" name="equipActiveHidden" value="true" />
							</td>
							<td style="text-align: center;">
								<a href="#" class="removeRow" onclick="return false;">
									<img border="0" src="~/Content/Images/Icons/delete.png" alt="Remove" runat="server" />
								</a>
							</td>
						</tr>

					   <% if (equipmentUser.Count > 0) {
							    foreach (DSNY.Data.Equipment_User equipUser in equipmentUser) {
								    selectedEquipment = null; %>
							<tr class="<%: equipCount % 2 == 0 ? "alt" : string.Empty%>">
								<td>
									<input type="hidden" name="equipUserId.<%: equipUser.Equipment_User_ID%>" value="<%: equipUser.Equipment_User_ID %>" />
									<select name="equipType.<%: equipUser.Equipment_User_ID%>" class="equipType">
										<option value="-1">SELECT EQUIPMENT</option>
										<% foreach (DSNY.Data.Equipment equip in equipment) {
									            if (equip.Equipment_ID == equipUser.Equipment_ID) {
										            selectedEquipment = equip;%>
													<option value="<%: equip.Equipment_ID %>" selected="selected"><%: equip.Equipment_Name%></option>
											<% } else { %>
													<option value="<%: equip.Equipment_ID %>"><%: equip.Equipment_Name%></option>
										<%     } %>
										<% } %>
									</select>
								</td>
								<td><input type="text" name="equipDescript.<%: equipUser.Equipment_User_ID%>" value="<%: equipUser.Equipment_Description %>" style="width: 150px" /></td>
								<td>
									<select name="equipProd.<%: equipUser.Equipment_User_ID %>" style="width: 215px;">
										<option value="-1">SELECT PRODUCT</option>
										<% foreach (DSNY.Data.Product prod in products) {
									            if (prod.Product_ID == equipUser.Product_ID) {%>
													<option value="<%: prod.Product_ID %>" selected="selected"><%: prod.Product_Name%></option>
											<% } else { %>
													<option value="<%: prod.Product_ID %>"><%: prod.Product_Name%></option>
										<%     } %>
										<% } %>
									</select>
								</td>
								<td style="text-align: center;">
									<input type="text" name="equipCapacity.<%: equipUser.Equipment_User_ID%>" value="<%: equipUser.Capacity %>" 
										style="width: 50px; <%: selectedEquipment == null || !selectedEquipment.has_Capacity ? "display: none;" : "" %>" />
								</td>
								<td style="text-align: center;">
									<input type="checkbox" name="equipActive.<%: equipUser.Equipment_User_ID%>" class="activeCheckbox" <%: equipUser.is_Active ? "checked" : string.Empty %> />
									<input type="hidden" name="equipActiveHidden.<%: equipUser.Equipment_User_ID%>" value="<%: equipUser.is_Active ? "true" : "false" %>" />
								</td>
								<td style="text-align: center;">
									<a href="#" class="removeRow" onclick="return false;">
										<img border="0" src="../../Content/Images/Icons/delete.png" alt="remove" />
									</a>
								</td>
							</tr>
					<%      equipCount++;
									  }
								  } %>
					</table>
					<a href="#" id="addEquipRow" style="float: right; margin: 10px 45px 0 0;">Add Equipment Row</a>
					<div class="clear"></div>
				<% } %>
			</div>
		</div>
	<% } %>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
	<div id="noFloat" style="width: 690px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
	<script type="text/javascript">
	    $(document).ready(function () {
	        var submitting = false;

			// Kick off the tabs function
			$("#tabsContainer").tabs({ tab: 1 });

			// inital sort
			SortListbox("usersAvailable");
			SortListbox("usersSelected");

			// takes all selected options in 'usersAvailable' and moves to 'usersSelected'
			$("#MainContent_addUser").click(function () {
				$('#usersAvailable option:selected').appendTo('#usersSelected');
				$('#usersAvailable option:selected').remove();

				SortListbox("usersAvailable");
				SortListbox("usersSelected");
			});

			// takes all selected options in 'usersSelected' and moves to 'usersAvailable'
			$("#MainContent_removeUser").click(function () {
				$('#usersSelected option:selected').appendTo('#usersAvailable');
				$('#usersSelected option:selected').remove();

				SortListbox("usersAvailable");
				SortListbox("usersSelected");
			});

			// Add new row to table
			$('#addEquipRow').live('click', function () {
			    var table = $('#equipmentGrid');
			    var copiedRow = $("#firstInputRow").clone();
			    var rowIndex = table.attr('rows').length - 1;

			    copiedRow.attr('id', '');
			    copiedRow.css('display', 'table-row');

				if (copiedRow != null) {
				    var randomRowNum = Math.floor(Math.random()*1000001);
				    var userId = copiedRow.find(":input:eq(0)");
				    var equipment = copiedRow.find(":input:eq(1)");
				    var description = copiedRow.find(":input:eq(2)");
				    var product = copiedRow.find(":input:eq(3)");
				    var capacity= copiedRow.find(":input:eq(4)");
				    var active = copiedRow.find(":input:eq(5)");
				    var activeHidden = copiedRow.find(":input:eq(6)");

				    userId.attr("name", "equipUserId." + randomRowNum);
				    description.attr("name", "equipDescript." + randomRowNum).addClass('required');
				    equipment.attr("name", "equipType." + randomRowNum).addClass('required');
				    product.attr("name", "equipProd." + randomRowNum).addClass('required');
				    capacity.attr("name", "equipCapacity." + randomRowNum);
				    active.attr("name", "equipActive." + randomRowNum);
				    activeHidden.attr("name", "equipActiveHidden." + randomRowNum);

				    description.val("");
				    equipment.val("");
				    product.val("");
				    capacity.val("");
				    active.attr('checked', true);

				    var products = $('input:checkbox[name*="product"]:checked');
				    var productIds = [];

				    products.each(function() {
				        productIds.push(this.name.replace('product', ''));
				    });

				    var options = $('select[name="equipProd"] > option').clone();
				    var optionsToAdd = [];

				    options.each(function() {
				        var val = $(this).val();

				        if (val === '' || productIds.indexOf(val) > -1) {
				            optionsToAdd.push(this);
				        }
				    });

				    product.empty().append(optionsToAdd.sort());

				    copiedRow.insertAfter(table.find("tr:eq(" + rowIndex + ")"));

				    description.rules('add', { required: true, messages: { required: '' } });
				    equipment.rules('add', { required: true, messages: { required: '' } });
				    product.rules('add', { required: true, messages: { required: '' } });

				    description.valid();
				    equipment.valid();
				    product.valid();

				    var isValidFunction = function() {
				        $(this).valid();
				        checkFormValidity();
				    };

				    description.keyup(isValidFunction);
				    equipment.change(isValidFunction);
				    product.change(isValidFunction);

				    checkFormValidity();
				}
			});

			// Remove a row
			$('.removeRow').live('click', function () {
				var rowIndex = $(this).closest("tr").prevAll("tr").length;
				var row = $(this).parents("table").find("tr:eq(" + rowIndex + ")");
				row.remove();
				checkFormValidity();
			});

			// shows or hides capacity for equipment
			$('.equipType').live('change', function () {
			    // get value and then also get the targeted capacity input to change css display
				var value = $(this).val();
				var row = $(this).parents('tr');
				var inputToChange = $(this).parents('tr').find('td:eq(3)').find(':input');

				// loop through the equipment, if we get a match, flip display based on if there is a capacity
				<% foreach (DSNY.Data.Equipment equip in (List<DSNY.Data.Equipment>)ViewData["Equipment"]) 
					{ %>
					if (value == <%: equip.Equipment_ID %>) {
						$(inputToChange).css('display', '<%: equip.has_Capacity ? "inline" : "none" %>');
						
						if (!<%: equip.has_Capacity.ToString().ToLower() %>)
							$(inputToChange).val('');

						return;
					}
				<% } %>
			});

			// validations
	        $("#user-form").validate({
			    errorClass: "input-validation-error",
			    errorPlacement: function(error, element) {
				    error.hide();
			    }
	        });

	        $('input[name*="equipDescript."]').each(function () {
	            $(this).rules('add', { required: true, messages: { required: '' } });
	        }).live('input', function() {
	            $(this).valid();
	            checkFormValidity();
	        });

	        $('select[name*="prodSubType"]').each(function (st) {
	            var subType = $(this);
	            subType.rules('add', { required: true, messages: { required: '' } });
	            subType.valid();
	            checkFormValidity();
	        }).change(function() {
	            checkFormValidity();
	        });

	        $('input:checkbox[name*="product"]').each(function() {
	            var select = $(this).parent().parent().find('select');
	            if (select.length > 0) {
	                if ($(this).is(':checked')) {
	                    select.rules('add', { required: true, messages: { required: '' } });
	                } else {
	                    select.rules('remove');
	                }
	                select.valid();
	                checkFormValidity();
	            }
	        }).change(function() {
	            var select = $(this).parent().parent().find('select');
	            if (select.length > 0) {
	                if ($(this).is(':checked')) {
	                    select.rules('add', { required: true, messages: { required: '' } });
	                } else {
	                    select.rules('remove');
	                }
	                select.valid();
	                checkFormValidity();
	            }
	        });

	        function checkFormValidity() {
	            if ($("#user-form").valid()) {
	                $('#submitForm').parent().css('opacity', '1');
	            } else {
	                $('#submitForm').parent().css('opacity', '.5');
	            }
	        };

			// before submit, we select all options in the select list, this allows us to grab all the values
	        $('#submitForm').click(function () {
	            if ($('#user-form').valid() && !submitting) {
	                $('#usersSelected option').attr('selected', 'selected');
	                $('#submitForm').parent().css('opacity', '.5');
	                $('#submitting').show();
	                submitting = true;

	                document.forms[0].submit();
	            }
			});

	        $(".activeCheckbox").live('click', function () {
	            var name = $(this).attr('name') || $(this).parent().attr('name');
	            var id = name.substring(name.indexOf('.'), name.length);
	            $(':input[name="equipActiveHidden' + id + '"]').val($(this).is(':checked') ? 'true' : 'false');
	        });

	        $("#equipmentTab").click(function() {
                // sets equipment product options to selected products in other tab
	            var products = $('input:checkbox[name*="product"]:checked');
	            var productIds = [];

	            products.each(function() {
	                productIds.push(this.name.replace('product', ''));
	            });

	            $('select[name*="equipProd."]').each(function() {
	                var el = $(this);
	                var options = $('select[name="equipProd"] > option').clone();
	                var optionsToAdd = [];
	                var val = el.val();

	                options.each(function() {
	                    var val = $(this).val();

	                    if (val === '' || productIds.indexOf(val) > -1) {
	                        optionsToAdd.push(this);
	                    }
	                });

	                el.empty().append(optionsToAdd.sort());
	                el.val(val);
	                el.rules('add', { required: true, messages: { required: '' } });
	                el.valid();
	            });

	            checkFormValidity();
	        });
		});
	</script>
</asp:Content>