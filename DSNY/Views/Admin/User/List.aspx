﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<DSNY.Core.Interfaces.IUser>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Users
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery.tablesorter.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!--<h2>Users&nbsp;&nbsp;&nbsp;[<a href="#" onclick="helpPopup('ManageUser')">?</a>]</h2>-->
    <h2>Users</h2>
    <p style="width: 1050px;" class="dottedLine"></p>

    <div id="userGrid" style="width: 1050px;">
        <div class="iconLeft">
            <a href="<%: Url.RouteUrl(new { controller = "Admin", action = "Index"}) %>" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/back.png" alt="Admin List" runat="server" /><br />Admin<br />Menu
            </a>
        </div>
        <div class="iconLeft">
            <a href="<%: Url.RouteUrl(new { controller = "Users", action = "Create"}) %>" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/create.png" alt="Create User" runat="server" /><br />Create User
            </a>
        </div>
        
        <div style="padding-bottom: 50px;">&nbsp;</div>

        <div style="margin: 0 auto 5px; font-size: 1.5em; text-align: center;">
                <%
                int testVar = 0;
                foreach (var user in Model.GroupBy(u => u.userName.Substring(0, 1).ToUpper()).Select(g => g.First()).ToList()) {
                    if (!int.TryParse(user.userName.Substring(0, 1), out testVar))
                    {%>
                        <a href="#<%: user.userName.Substring(0, 1).ToUpper() %>"><%: user.userName.Substring(0, 1).ToUpper()%></a>&nbsp;
                <% }
                } %>
        </div>

        <% Html.RenderPartial("Grids/UserGrid"); %>
        <div class="iconRight">
            <a href="<%: Url.RouteUrl(new { controller = "Users", action = "Create"}) %>" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/create.png" alt="Create User" runat="server" /><br />Create User
            </a>
        </div>
        <div class="iconRight">
            <a href="<%: Url.RouteUrl(new { controller = "Admin", action = "Index"}) %>" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/back.png" alt="Admin List" runat="server" /><br />Admin<br />Menu
            </a>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 1075px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            // set up our table sorter
            $("#userTable").tablesorter({ widgets: ["zebra"],
                widgetZebra: { css: ["alt", ""] }
            });

            // AJAX event which deletes the selected item
            $(".deleteButton").live('click', function () {
                var data = { name: $(this).attr("name") };

                if (confirm('Press \'OK\' to delete ' + data.name + '.')) {

                    $.get("/Users/Delete/", data, function (data) {
                        if (data.status == "success") {
                            location.reload();
                            //$('#row-' + data.name).fadeOut('slow', function () { reStripe("userTable"); });
                        }
                        else {
                            $("#errorMessage").html("There was an error deleting " + data.name + ".<br /><br />");
                        }
                    }, 'json');
                }
            });

            //            $(".sortable").live('click', function () {
            //                if ($("#sortColumn").val() == $(this).attr("id")) {
            //                    if ($("#direction").val() == "DESC") {
            //                        var sortData = { field: $(this).attr("id"), direction: "ASC" };
            //                    }
            //                    else {
            //                        var sortData = { field: $(this).attr("id"), direction: "DESC" };
            //                    }
            //                }
            //                else {
            //                    var sortData = { field: $(this).attr("id"), direction: "DESC" };
            //                }

            //                $.get("/Users/SortedGrid", sortData, function (data) {
            //                    $("#userTable").html(data);
            //                    $("#sortColumn").val(sortData.field);
            //                    $("#direction").val(sortData.direction);
            //                }, 'html');
            //            });
        });
    </script>
</asp:Content>