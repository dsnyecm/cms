﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	E-mail Distribution List
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!--<h2>E-mail Distribution List&nbsp;&nbsp;&nbsp;[<a href="#" onclick="helpPopup('ManageEmail')">?</a>]</h2>-->
    <h2>E-mail Distribution List</h2>
    <p style="width: 680px;" class="dottedLine"></p>

    <div class="adminIconContainer">
        <div class="iconLeft">
            <a href="<%: Url.RouteUrl(new { controller = "Admin", action = "Index"}) %>" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/back.png" alt="Admin List" runat="server" /><br />Admin<br />Menu
            </a>
        </div>
        <div class="iconLeft">
            <a href="#" id="submitForm" class="iconLink">
                <img border="0" src="~/Content/Images/Icons/create.png" alt="Save" runat="server" /><br />Save
            </a>
        </div>
    </div>

    <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

    <% using (Html.BeginForm()) {%>
        <div style="width: 665px; padding-left: 25px;">
            <div style="float: left;">
                <h3>Available Users</h3>
                <%: Html.DropDownList("usersAvailable", (SelectList)ViewData["usersAvailable"], new { @class = "selectList", @multiple = "multiple" })%>
                <div style="margin-top: 10px; font-weight: bold;">*Only users with valid email addresses<br />&nbsp;&nbsp;are listed here</div>
            </div>
            <div style="width: 75px; height: 175px; padding-top: 100px; padding-left: 25px; float: left;">
                <a href="#" style="margin-bottom: 50px;">
                    <img id="addUser" border="0" src="~/Content/Images/Icons/moveRight.png" alt="Add User" runat="server" />
                </a>
                <div style="height: 50px;"></div>
                <a href="#">
                    <img id="removeUser" border="0" src="~/Content/Images/Icons/moveLeft.png" alt="Add User" runat="server" />
                </a>
            </div>
            <div style="float: left;">
                <h3>Message Distribution Users</h3>
                <%: Html.DropDownList("usersSelected", (SelectList)ViewData["usersSelected"], new { @class = "selectList", @multiple = "multiple" })%>
            </div>
    <% } %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 690px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            // inital sort
            SortListbox("usersAvailable");
            SortListbox("usersSelected");

            // takes all selected options in 'usersAvailable' and moves to 'usersSelected'
            $("#MainContent_addUser").click(function () {
                $('#usersAvailable option:selected').appendTo('#usersSelected');
                $('#usersAvailable option:selected').remove();

                SortListbox("usersAvailable");
                SortListbox("usersSelected");
            });

            // takes all selected options in 'usersSelected' and moves to 'usersAvailable'
            $("#MainContent_removeUser").click(function () {
                $('#usersSelected option:selected').appendTo('#usersAvailable');
                $('#usersSelected option:selected').remove();

                SortListbox("usersAvailable");
                SortListbox("usersSelected");
            });

            // before submit, we select all options in the select list, this allows us to grab all the values
            $('#submitForm').click(function (e) {
                e.preventDefault();

                $('#usersSelected option').attr('selected', 'selected');
                $('#usersAvailable option').attr('selected', 'selected');
                document.forms[0].submit();
            });
        });
    </script>
</asp:Content>
