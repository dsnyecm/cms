﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.DailyReportViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Daily Weather Report
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="JavascriptIncludes" runat="server">
	<script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.5.custom.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery.validate.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/lodash.3.9.3.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/Tabs.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
	<link href="<%=ResolveUrl("~/Content/jqueryUI.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="MainContent" runat="server">

	<h2>Data Correction: Daily Weather Reports</h2>
	<p style="width: 1180px;" class="dottedLine"></p>

	<% if (Model.dailyReport != null) { %>
		<div style="float: left; width: 1175px;">
			<div class="iconLeft">
				<a href="<%: Url.Action("Search", "DailyReport") %>" class="iconLink">
					<img border="0" src="~/Content/Images/Icons/cancel.png" alt="Cancel" runat="server" /><br />Cancel
				</a>                   
			</div>
			<div class="iconLeft">
				<a href="#" id="submitButton" class="iconLink">
					<img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Submit" style="vertical-align: bottom;" runat="server" /><br />Submit
				</a>
			</div>
		</div>
	<% } else { %>
		<div style="float: left; width: 1175px;">
			Search for a daily weather report by selecting a date.
		</div>

		<% using (Html.BeginForm("Search", "DailyReport", FormMethod.Post, new { id = "dailyReport" })) {%>
			<div style="float: left; margin-bottom: 10px; width: 1175px;">
				<div class="display-label" style="margin-top: .9em; font-weight: bold;"><label for="formDate">Date</label>: </div>
				<div class="display-field" style="margin: 0.9em 0; width: 160px;">
					<%: Html.TextBoxFor(m => m.searchDate, new { @class = "tcalReadOnly", style = "width: 70px;", size = "10", @readonly = "true",
							@Value = Model.searchDate.HasValue ? Model.searchDate.Value.ToString("MM/dd/yyyy") : "" })%>
					<script type="text/javascript">
						new tcal({ 'formname': 'dailyReport', 'controlname': 'searchDate', 'id': 'searchDate' });
					</script> 
				</div>
				<div class="display-label" style="margin-top: .9em; text-align: left;"">
					<button id="submit-form" type="button" onclick="$('#searchDate').val() !== '' ? this.form.submit() : null;">Search</button>
				</div>
			</div>
		<% }
		} %>

		<div class="clear"></div>

		<% if (Model != null && Model.dailyReport  != null) {
				SelectList temps = new SelectList(new string[] { "", "-20 - 0", "1 - 20", "21 - 40", "41 - 60", "61 - 80", "81 - 100", "101+" });
 
			using (Html.BeginForm("Edit", "DailyReport", FormMethod.Post, new { id = "dailyReport" })) {%>
				<div style="float: left; margin-bottom: 10px; width: 1175px;">
					<%: Html.HiddenFor(m => m.dailyReport.DailyReportId) %>
					<%: Html.HiddenFor(m => m.dailyReport.DailyReportDate) %>

					<% if (Model.dailyReport.DailyReportId == 0) { %>
						<h2 style="padding-left: 40px;">Adding a new daily weather report</h2>
					<% } %>

					<fieldset>
						<legend>Daily Weather Report - <%: Model.dailyReport.DailyReportDate.ToShortDateString() %></legend>
						<div class="editor-label">Low Temp:</div>
						<div class="editor-field">
							<%:Html.DropDownListFor(m => m.dailyReport.DailyReportTempLow, temps) %>
						</div>

						<div class="clear"></div>

						<div class="editor-label">High Temp:</div>
						<div class="editor-field">
							<%:Html.DropDownListFor(m => m.dailyReport.DailyReportTempHigh, temps) %>
						</div>

						<div class="clear"></div>

						<div class="editor-label">Weather:</div>
						<div class="editor-field">
							<%:Html.DropDownListFor(m => m.dailyReport.DailyReportWeather, new SelectList(new string[] { "", "Clear", "Rain", "Snow" })) %>
						</div>

						<div class="clear"></div>

						<div class="editor-label">Function:</div>
						<div class="editor-field">
							<%:Html.DropDownListFor(m => m.dailyReport.DailyReportFunction, new SelectList(new string[] { "", "Regular Operations", "Exception Operations", "No Operations" })) %>
						</div>

						<div class="clear"></div>

					</fieldset>
				</div>
			<% } %>
		<% } %>

		<%if (ViewData["saved"] != null && (bool)ViewData["saved"] == true) {%>
			<h2 style="padding-left: 40px;">Daily Weather Report was updated</h2>
		<% } %>
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="FooterContent" runat="server">
	<div id="noFloat" style="width: 1200px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content9" ContentPlaceHolderID="JavascriptSource" runat="server">
	<script type="text/javascript">
		var isSunday = "<%: TempData["isSunday"] %>" === "True";
		var validate = function () {
			$("#dailyReport").validate();
			var isValid = $("#dailyReport").valid();

			if (isValid) {
				$("#submitButton").removeAttr("disabled").removeClass("ui-state-disabled");
			} else {
				$("#submitButton").attr("disabled", true).addClass("ui-state-disabled");
			}

			return isValid;
		};

		if (isSunday) {
			$("#submitButton").attr("disabled", true).addClass("ui-state-disabled");
			$("#dailyReport_DailyReportFunction").val("No Operations");
			$("#dailyReport_DailyReportTempLow").prepend("<option></option>").val("");
			$("#dailyReport_DailyReportTempHigh").prepend("<option></option>").val("");
			$("#dailyReport_DailyReportWeather").prepend("<option></option>").val("");
		}

		$("#dailyReport").validate({
		    errorClass: "not-valid",
			errorPlacement: function (error, element) {
				error.hide();
			},
			rules: {
				"dailyReport.DailyReportTempLow": { required: true, messages: { required: '' } },
				"dailyReport.DailyReportTempHigh": { required: true, messages: { required: '' } },
				"dailyReport.DailyReportWeather": { required: true, messages: { required: '' } },
				"dailyReport.DailyReportFunction": { required: true, messages: { required: '' } }
			}
		});

		$('#dailyReport_DailyReportTempLow, #dailyReport_DailyReportTempHigh, #dailyReport_DailyReportWeather, #dailyReport_DailyReportFunction').live('change', function () {
			validate();
		});

		$("#submitButton").click(function () {
			if (validate) {
				$('#dailyReport').submit();
			}
		});

		validate();
	</script>
</asp:Content>

