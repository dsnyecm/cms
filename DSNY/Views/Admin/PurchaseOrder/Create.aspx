﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.PurchaseOrdersViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Create Purchase Order
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
	<script src="<%=ResolveUrl("~/Scripts/MicrosoftAjax.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/MicrosoftMvcAjax.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/MicrosoftMvcValidation.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery.validate.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.5.custom.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/lodash.3.9.3.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/jqueryUI.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<h2>Create Purchase Order</h2>
	<p style="width: 1005px;" class="dottedLine"></p>

	<div class="adminIconContainer" style="width: 1005px;">
		<div class="iconLeft">
			<a href="/Admin" class="iconLink">
				<img style="border: 0;" src="~/Content/Images/Icons/cancel.png" alt="Cancel" runat="server" /><br />Cancel
			</a>
		</div>

		<div class="iconLeft">
			<a id="addPurchaseOrderLine" href="#" class="iconLink">
				<img border="0" src="~/Content/Images/Icons/add.png" alt="Add Process Purchase Line" runat="server" /><br />
				Add Line
			</a>
		</div>

		<div class="iconLeft">
			<a id="validatePurchaseOrder" href="#" class="iconLink">
				<img border="0" src="~/Content/Images/Icons/create.png" alt="Process Purchase Order" runat="server" /><br />
				Process Purchase Order
			</a>
		</div>

		<div id="submitting" style="display: none; padding-top: 15px;">Submitting</div>
        <div id="date-time" style="float: right; padding-top: 15px;"></div>
	</div>

	<div class="clear"></div>
	<br />

	<% Html.EnableClientValidation(); %>
	<% using (Html.BeginForm("Create", "PurchaseOrder", FormMethod.Post, new { id = "purchaseOrder" })) { %>

	<% if (Model == null || Model.pos.Count == 0) { %>
		<b>Fuel quantities citywide are sufficient, no purchase orders are necessary.</b><br /><br />
    <% } %>

	<%
		bool hasError = ViewData.Keys.Contains("hasError") ? (Boolean)ViewData["hasError"] : false;

		if (hasError) {
			string errorMessage = (string)ViewData["errorMessage"];
	%>
		<div class="error-text">
			<b>There was an error processing the purchase order:</b>
			<div><%= errorMessage %></div>
		</div>
	<% } %>
	

	<table id="productOrderTable" class="tablesorter">
		<thead>
			<tr>
				<th style="width: 50px;">Add to Order</th>
				<th style="width: 80px;">Location</th>
                <th style="width: 100px;">Vendor</th>
				<th style="width: 170px;">Fuel Type</th>
                <th style="width: 100px;">Sub Type</th>
				<th style="width: 100px;">Order Amount</th>
				<th style="width: 100px;">Modification Reason</th>
				<th style="width: 50px;">On Hand</th>
				<th style="width: 50px;">Daily Usage</th>
				<th style="width: 50px;">Total Capacity</th>
			</tr>
		</thead>
		<tbody>
		    <tr class="title-row -1" style="display: none;">
			    <td colspan="10" style="font-weight: bold;"></td>
		    </tr>
            <tr class="total-row -1" style="display: none; border-bottom: none;">
				<td colspan="4"></td>
				<td style="font-weight: bold; text-align: right;">Total</td>
				<td colspan="5" style="padding-left: 50px;"><span id="total[-1]">&nbsp;</span></td>
			</tr>
			<tr class="borough-row -1" style="display: none;">
				<th colspan="10" style="font-weight: bold; text-align: center;"></th>
			</tr>
			<tr class="product-row -1" id="pos[-1]_row" style="display: none;">
				<td style="text-align: center;">
					<input checked="checked" id="pos_-1__isOrdered" name="pos[-1].isOrdered" type="checkbox" value="true"><input name="pos[-1].isOrdered" type="hidden" value="false">
				</td>
				<td>
					<input id="pos_-1__userId" name="pos[-1].userId" type="hidden" value=""><input id="pos_-1__pos_aspnet_Users_UserName" name="pos[-1].pos.aspnet_Users.UserName" type="hidden" value=""><input id="pos_-1__address" name="pos[-1].address" type="hidden" value=""><input id="pos_-1__borough" name="pos[-1].borough" type="hidden" value=""><input id="pos_-1__district" name="pos[-1].district" type="hidden" value=""><input id="pos_-1__poDisplayName" name="pos[-1].poDisplayName" type="hidden" value="">
				</td>
                <td>
				    <input type="hidden" id="pos_-1__vendorId" name="pos[-1].vendorId" value="">
                </td>
				<td>
					<input id="pos_-1__productId" name="pos[-1].productId" type="hidden" value=""><input id="pos_-1__pos_Product_Product_Name" name="pos[-1].pos.Product.Product_Name" type="hidden" value=""><input id="pos_-1__pos_Product_Measurement_Type" name="pos[-1].pos.Product.Measurement_Type" type="hidden" value=""><input id="pos_-1__pos_Product_is_Sub_Type" name="pos[-1].pos.Product.is_Sub_Type" type="hidden" value="">
				</td>
                <td></td>
				<td style="text-align: center;">
					<input data_avgd="" data_capacity="" data_eoh="" data_productid="" data_value="" id="pos_-1__orderAmount" maxlength="5" name="pos[-1].orderAmount" style="width: 65px; text-align: right;" type="text" value="" class="valid">
					<span class="field-validation-valid" id="pos_-1__orderAmount_validationMessage"></span>
				</td>
				<td style="text-align: center;">
					<input class="is-ordered" id="pos_-1__exceptionReason" name="pos[-1].exceptionReason" style="width: 110px; background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAfBJREFUWAntVk1OwkAUZkoDKza4Utm61iP0AqyIDXahN2BjwiHYGU+gizap4QDuegWN7lyCbMSlCQjU7yO0TOlAi6GwgJc0fT/fzPfmzet0crmD7HsFBAvQbrcrw+Gw5fu+AfOYvgylJ4TwCoVCs1ardYTruqfj8fgV5OUMSVVT93VdP9dAzpVvm5wJHZFbg2LQ2pEYOlZ/oiDvwNcsFoseY4PBwMCrhaeCJyKWZU37KOJcYdi27QdhcuuBIb073BvTNL8ln4NeeR6NRi/wxZKQcGurQs5oNhqLshzVTMBewW/LMU3TTNlO0ieTiStjYhUIyi6DAp0xbEdgTt+LE0aCKQw24U4llsCs4ZRJrYopB6RwqnpA1YQ5NGFZ1YQ41Z5S8IQQdP5laEBRJcD4Vj5DEsW2gE6s6g3d/YP/g+BDnT7GNi2qCjTwGd6riBzHaaCEd3Js01vwCPIbmWBRx1nwAN/1ov+/drgFWIlfKpVukyYihtgkXNp4mABK+1GtVr+SBhJDbBIubVw+Cd/TDgKO2DPiN3YUo6y/nDCNEIsqTKH1en2tcwA9FKEItyDi3aIh8Gl1sRrVnSDzNFDJT1bAy5xpOYGn5fP5JuL95ZjMIn1ya7j5dPGfv0A5eAnpZUY3n5jXcoec5J67D9q+VuAPM47D3XaSeL4AAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%;" type="text" value="">
					<span class="field-validation-valid" id="pos_0__exceptionReason_validationMessage"></span>
				</td>
				<td style="text-align: center;"></td>
				<td style="text-align: center;"></td>
				<td style="text-align: center;"></td>
			</tr>
			<% 
                int rowCount = 0;
                int rowColorCount = 0;
                int productId = 0;
                int runningTotal = 0;
                string bourough = string.Empty;
                List<DSNY.Data.Product_User> productsUsers = (List<DSNY.Data.Product_User>)ViewData["productUsers"];
                List<DSNY.Data.Vendor> vendors = (List<DSNY.Data.Vendor>)ViewData["vendors"];
                String reportUrl = 
                    String.Format("/Reports/reports.aspx?report={0}&Start_Date={1}&End_Date={2}", 
                        ConfigurationManager.AppSettings["PurchaseOrderReport"].ToString(), DateTime.Now.AddDays(-14).ToShortDateString(), DateTime.Now.ToShortDateString());

                if (Model != null && Model.pos != null && Model.pos.Count > 0) {
                    foreach (DSNY.ViewModels.PurchaseOrderViewModel p in Model.pos) {
                        DSNY.Data.Product_User productUser = productsUsers.SingleOrDefault(pu => pu.Product_ID == p.productId && pu.UserId == p.userId);

                        if (rowCount == 0) { %>
						<tr class="title-row <%= p.productId %>">
							<td colspan="10" style="font-weight: bold;"><%= p.pos.Product.Product_Name %> (<%= p.pos.Product.Measurement_Type %>)</td>
						</tr>
					<% }

					if (productId != p.productId && productId != 0) { %>
						<tr class="total-row <%= productId %>" style="border-bottom: none;">
							<td colspan="4"></td>
							<td style="font-weight: bold; text-align: right;">Total</td>
							<td colspan="5" style="padding-left: 50px;"><span id="total[<%=productId %>]"><%= runningTotal %></span></td>
						</tr>
						<tr class="title-row <%= p.productId %>">
							<td colspan="10" style="font-weight: bold;"><br /><br /><%= p.pos.Product.Product_Name %> (<%= p.pos.Product.Measurement_Type %>)</td>
						</tr>
					<%
							runningTotal = 0;
							rowColorCount = 0;
							productId = p.productId;
							bourough = null;
						}

						if (p.borough!= null && p.borough != bourough) {
							bourough = p.borough; %>
						<tr class="borough-row <%= p.borough %> <%= p.productId %>">
							<th colspan="10" style="font-weight: bold; text-align: center;"><%= p.borough %></th>
						</tr>
					<%} %>

					<tr class="product-row <%= p.productId %> <%: rowColorCount % 2 == 0 ? "alt" : string.Empty %>" id="pos[<%= rowCount %>]_row">
						<td style="text-align: center;">
							<%: Html.CheckBoxFor(po => po.pos[rowCount].isOrdered) %>
						</td>
						<td>
							<a class="po" href="<%=  reportUrl + "&Product_Id=" + p.productId + "&User_Id=" + p.pos.UserId %>" target="_blank" >
                                <%= p.pos.aspnet_Users.UserName %>
                            </a>
							<%: Html.HiddenFor(po => po.pos[rowCount].userId) %><%: Html.HiddenFor(po => po.pos[rowCount].pos.aspnet_Users.UserName) %><%: Html.HiddenFor(po => po.pos[rowCount].address) %><%: Html.HiddenFor(po => po.pos[rowCount].borough) %><%: Html.HiddenFor(po => po.pos[rowCount].district) %><%: Html.HiddenFor(po => po.pos[rowCount].poDisplayName) %>
						</td>
                        <td>
                            <% if (p.pos.Product.Product_Vendor.Count() > 0) { 
                                    DSNY.Data.Product_Vendor productVendor = p.pos.Product.Product_Vendor.First();

                                    if (p.pos.Product.Product_Vendor.Count() == 1) { %>
							        <input  type="hidden" id="pos_<%= rowCount %>__vendorId" name="pos[<%= rowCount %>].vendorId" value="<%= productVendor.Vendor_ID %>">
                                    <%= vendors.First(v => v.Vendor_ID == productVendor.Vendor_ID).Vendor_Name%>
                                <% } else {  %>
							        <select id="pos_<%= rowCount %>__vendorId" name="pos[<%= rowCount %>].vendorId" style="width: 100px;">
                                        <option value="" disabled="disabled" selected="selected"></option>
							            <% foreach (DSNY.Data.Product_Vendor pv in p.pos.Product.Product_Vendor) {
                                                DSNY.Data.Vendor vendor = vendors.FirstOrDefault(v => v.Vendor_ID == pv.Vendor_ID);
                                                if (productUser.Vendor_ID.HasValue && vendor.Vendor_ID ==  productUser.Vendor_ID) { %>
									            <option value="<%: vendor.Vendor_ID %>" selected="selected"><%: vendor.Vendor_Name %></option>
							                <% } else { %>
									            <option value="<%: vendor.Vendor_ID %>"><%: vendor.Vendor_Name %></option>
							                <% } %>
                                    <% } %>
                                <% } %>
                            <% } %>
                        </td>
						<td>
							<%= p.pos.Product.Product_Name%> (<%= p.pos.Product.Measurement_Type %>)
							<%: Html.HiddenFor(po => po.pos[rowCount].productId) %><%: Html.HiddenFor(po => po.pos[rowCount].pos.Product.Product_Name) %><%: Html.HiddenFor(po => po.pos[rowCount].pos.Product.Measurement_Type) %><%: Html.HiddenFor(po => po.pos[rowCount].pos.Product.is_Sub_Type) %>
						</td>
                        <td>
                            <% if (productUser.Product.is_Sub_Type && productUser.Product.Product_Sub_Type.Count() > 0) {%>
							    <select id="pos_<%= rowCount %>__productSubTypeId" name="pos[<%= rowCount %>].productSubTypeId" style="width: 100px;">
                                    <option value="" disabled="disabled" selected="selected"></option>
							    <% foreach (DSNY.Data.Product_Sub_Type st in productUser.Product.Product_Sub_Type.OrderBy(pu => pu.Sub_Type).ToList()) { 
                                        if (productUser.Product_Sub_Type_Id == st.Product_Sub_Type_ID) { %>
									        <option value="<%: st.Product_Sub_Type_ID%>" selected="selected"><%: st.Sub_Type %></option>
							        <% } else { %>
									        <option value="<%: st.Product_Sub_Type_ID %>"><%: st.Sub_Type %></option>
							        <% } %>
                                <% } %>
                            <% } %>
                        </td>
						<td style="text-align: center;">
							<%: Html.TextBoxFor(po => po.pos[rowCount].orderAmount, new { style="width: 65px; text-align: right;", maxlength="5",
								data_value =p.orderAmount, data_capacity=p.capacity, data_productid=p.productId, data_avgd=p.avgDispensed, data_eoh=p.endOnHand }) %>
							<%: Html.ValidationMessageFor(po => po.pos[rowCount].orderAmount) %>
						</td>
						<td style="text-align: center;">
							<%: Html.TextBoxFor(po => po.pos[rowCount].exceptionReason, new { @class="is-ordered", style="width: 110px;" })%>
							<%: Html.ValidationMessageFor(po => po.pos[rowCount].exceptionReason) %>
						</td>
						<td style="text-align: center;">
							<%= p.endOnHand %>
						</td>
						<td style="text-align: center;">
							<%= p.avgDispensed %>
						</td>
						<td style="text-align: center;">
							<%= p.capacity %>
						</td>
					</tr>
				<% 
                    runningTotal += Model.pos[rowCount].isOrdered ? p.orderAmount : 0;

                    if (rowCount == Model.pos.Count() - 1) { %>
					<tr class="total-row <%= p.productId %>">
						<td colspan="4"></td>
						<td style="font-weight: bold; text-align: right;">Total</td>
						<td style="text-align: center;"><span id="total[<%=p.productId %>]"><%= runningTotal %></span></td>
						<td  colspan="5"></td>
					</tr>
				<% }
				   
                   if (productId == 0) {
					   productId = p.productId;
				   }

				   rowCount++;
				   rowColorCount++;
			   }
            } %>
			</tbody>
		</table>
	<% } %>

    <div id="confirm-process" class="modal" title="Process Purchase Order" style="overflow: hidden;">
	    <p>
		    <span class="ui-icon ui-icon-alert"></span>
		    <span class="modal-text">
			    Are you sure you want to process this purchase order?
		    </span>
	    </p>
    </div>

    <% if (Model != null)
        { %>
	<div id="purchaseOrderAddView" class="modal" title="Add Purchase Order Line">
		<form id="poForm" action="/" method="get">
			<fieldset style="width: 300px;">
				<div class="editor-label" style="width: 90px;"><label for="loction">Location:</label></div>
				<div class="editor-field" style="width: 205px;">
					<%: Html.DropDownList("poLocation", new SelectList(ViewData["users"] as List<DSNY.Data.aspnet_Users>, "userId", "userName"))%>
				</div>

				<div class="editor-label" style="width: 90px;"><label for="fuelType">Fuel Type:</label></div>
				<div class="editor-field" style="width: 205px;">
					<select id="po" name="po"></select>
				</div>

                <div class="sub-type" style="display: none;">
				    <div class="editor-label" style="width: 90px;"><label for="subType">Sub Type:</label></div>
				    <div class="editor-field" style="width: 205px;">
					    <select id="subType" name="subType"></select>
				    </div>
                </div>

                <div class="vendor" style="display: none;">
				    <div class="editor-label" style="width: 90px;"><label for="vendor">Vendor:</label></div>
				    <div class="editor-field" style="width: 205px;">
					    <select id="vendor" name="vendor"></select>
				    </div>
                </div>

				<div class="editor-label" style="width: 90px;"><label for="orderAmount">Order Amount:</label></div>
				<div class="editor-field" style="width: 205px;">
					<input type="text" id="poAmount" name="poAmount" style="width: 50px" maxlength="5" />
				</div>

				<div class="editor-label" style="width: 90px;"><label for="exception">Reason:</label></div>
				<div class="editor-field" style="width: 205px;">
					<input type="text" id="poException" name="poException" style="width: 193px;" />
				</div>

				<!-- Allow form submission with keyboard without duplicating the dialog button -->
				<input type="submit" tabindex="-1" style="position: absolute; top: -1000px" />
			</fieldset>
			<div class="clearfix"></div>
		</form>
	</div>
    <% } %>

	<div id="purchaseOrderView" class="modal" title="Purchase Order Preview">
		<div style="width: 250px; float: left;">
			<span style="font-weight: bold;">Available Orders</span>
			<ul id="poCategories"></ul>
		</div>

		<div id="poContainer" style="float: left; display: none; padding-left: 25px;">
			<h2 style="text-align: center;">New York City<br />Department of Sanitation</h2>
			<div  style="padding-top: 5px;">
				<div id="poVendorInfo" style="float: left"></div>
				<div id="poDate" style="float: right"></div>
				<div class="clear"></div>
			</div>

			<h3 id="poProductName"  style="text-align: center;"></h3>

			<table id="productOrderViewTable">
				<thead>
				<tr>
					<th style="width: 75px; text-align: center;">Sub Type</th>
					<th style="width: 90px; text-align: center;">District</th>
					<th style="width: 200px; text-align: center;">Address</th>
					<th style="width: 100px; text-align: center;">Amount</th>
				</tr>
				</thead>
				<tbody id="productOrderViewRows">

				</tbody>
			</table>
		</div>

		<div class="clear"></div>
	</div>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
	<div id="noFloat" style="width: 714px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="JavascriptSource" runat="server">
    <% var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        List<DSNY.Data.Vendor> vendors = (List<DSNY.Data.Vendor>)ViewData["vendors"];
        String vendorsSerialized = null;
        String reportUrl = ConfigurationManager.AppSettings["ReportServer"].ToString() + ConfigurationManager.AppSettings["ReportPath"].ToString()
            + "/" + ConfigurationManager.AppSettings["PurchaseOrderReport"].ToString() 
            + "?&Start_Date=" + DateTime.Now.AddDays(-14).ToShortDateString() + "&End_Date=" + DateTime.Now.ToShortDateString();

        if (vendors != null && vendors.Count > 0)
        {
            vendorsSerialized = serializer.Serialize(vendors.Select(v => new DSNY.Data.Vendor() { Vendor_ID = v.Vendor_ID, Vendor_Name = v.Vendor_Name, Account_Number = v.Account_Number, Phone_Number = v.Phone_Number }).ToList());
        } %>
	<script type="text/javascript">
	   
		$(document).ready(function () {
			var rowCount = <%= Model != null ? Model.pos.Count() - 1 : 0 %>,
				poItems = [],
                vendors = <%: MvcHtmlString.Create(!String.IsNullOrEmpty(vendorsSerialized) ? vendorsSerialized : "[]") %>;
				selectedUser = null,
				selectedUserProducts = [],
				submitting = false;

		    animateDateTime("date-time");
		    animateDateTime("poDate");

			var setProductTotal = function(id) {
				var products = $('input[name*=".orderAmount"][data_productId="' + id + '"]');
				var total = _.sum(products, function(p) {
				    var input = $(p);
				    var isOrdered = input.parent().parent().find('input[name*=".isOrdered"]');
				    return isOrdered.is(':checked') ? parseInt($(p).val()) : 0;
				});

				$('[id="total[' + id + ']"]').text(total);
			};

			var addOrderAmountValidation = function(oa) {
			    var orderAmount = $(oa),
					  capacity = parseInt(orderAmount.attr('data_capacity')),
					  avgDispensed = parseInt(orderAmount.attr('data_avgd')),
					  eoh = parseInt(orderAmount.attr('data_eoh')),
					  maxCapacity = capacity - eoh + avgDispensed;
				
				if(maxCapacity > 0) {
				    orderAmount.rules('add', { required: true, digits: true, max: maxCapacity, messages: { required: '', digits: '', max: '' } });
				    orderAmount.validate();
				    checkSubmitEnable();
				}
			};

			$('select[id="poLocation"]').prepend($('<option />').val('0').html('-- Select User --'));

			$("#purchaseOrder").validate({
				errorClass: "input-validation-error",
				errorPlacement: function(error, element) {
					error.hide();
					return false;
				}
			});

			$('input[name*=".isOrdered"]').live('click', function () {
				var namePrefix = this.name.substr(0, this.name.indexOf('.isOrdered'));
				var orderAmount = $('input[name="' + namePrefix + '.orderAmount"]');
				var reason = $('input[name="' + namePrefix + '.exceptionReason"]');
				var vendor = $('select[name="' + namePrefix + '.vendorId"]');
				var subtype = $('select[name="' + namePrefix + '.productSubTypeId"]');
				var productId = parseInt(orderAmount.attr('data_productid'));

				if (!$(this).is(':checked')) {
				    reason.rules('add', { required: true, messages: { required: '' } });
				    if (vendor.length > 0) {
				        vendor.rules('remove');
				    }

				    if (subtype.length > 0) {
				        subtype.rules('remove');
				    }

				    orderAmount.rules('remove');
				    orderAmount.attr('disabled', 'true');
					$('tr[id="' + namePrefix + '_row"').css('background-color', '#ccc');
				} else {
					if (orderAmount.val() == orderAmount.attr('data_value')) {
					    reason.rules('remove');
					}

					addOrderAmountValidation(orderAmount);

					if (vendor.length > 0) {
					    vendor.rules('add', { required: true, messages: { required: '' } });
					}
					
					if (subtype.length > 0) {
					    subtype.rules('add', { required: true, messages: { required: '' } });
					}

					$('tr[id="' + namePrefix + '_row"').removeAttr('style');
					$('input[name="' + namePrefix + '.orderAmount"]').removeAttr('disabled');
				}

				var productId = parseInt($(this).parents('.product-row').attr('class').replace('product-row ', '').replace(' alt', ''));
				setProductTotal(productId)

				orderAmount.valid();    // validate input
				reason.valid();
				if (vendor.length > 0) {
				    vendor.valid();
				}

				if (subtype.length > 0) {
				    subtype.valid();
				}

				checkSubmitEnable();
				setProductTotal(productId);
			});

		    $('select[name*="productSubTypeId"], select[name*="vendorId"]').each(function (st) {
			    var subType = $(this);
			    subType.rules('add', { required: true, messages: { required: '' } });
			    subType.valid();
			    checkSubmitEnable();
			}).live('change', function() {
			    checkSubmitEnable();
			});

			$('input[name*="exceptionReason"]').live('input', function () {
				checkSubmitEnable();
			});

		    $('input[name*="orderAmount"]').each(function(oa) {
		        var productId = parseInt($(this).attr('data_productid'));
		        if (productId > 0) {
		            addOrderAmountValidation(this);
		        }
			}).live('input', function () {
				var value = parseInt($(this).val()),
					  originalValue = parseInt($(this).attr('data_value')),
					  capacity = parseInt($(this).attr('data_capacity')),
					  productId = parseInt($(this).attr('data_productid')),
					  namePrefix = this.name.substr(0, this.name.indexOf('.orderAmount')),
					  isOrdered = $('input[name="' + namePrefix + '.isOrdered"]').is(':checked'),
					  reason = $('input[name="' + namePrefix + '.exceptionReason"]');

				if (value !== originalValue) {
					reason.rules('add', { required: true, messages: { required: '' } });
				} else if (isOrdered) {
					reason.rules('remove');
				}

				setProductTotal(productId);

				$(this).valid();    // validate input
				reason.valid();    
				checkSubmitEnable();
			});

			// enables/disables form submit button
			function checkSubmitEnable() {
				if ($("#purchaseOrder").valid()) {
					$("#validatePurchaseOrder").removeAttr("disabled").removeClass("ui-state-disabled");
				} else {
					$("#validatePurchaseOrder").attr("disabled", true).addClass("ui-state-disabled");
				}
			}

			// returns the borough from the user name
			function getBorough(userName) {
				var abbr = userName.slice(0, 2);

				switch (abbr) {
					case 'MN':
						return "Manhattan";
						break;

					case 'BK':
						return "Brooklyn";
						break;

					case 'BX':
						return "Bronx";
						break;

					case 'QN':
						return "Queens";
						break;

					case 'SI':
						return "Staten Island";
						break;        
				};
			}

			//$('#poCategories li').live('click', function() {
            //    // select first vendor of category
			//    $(this).find('ul li:first a').click();
			//});

			$('#poCategories ul li a').live('click', function() {
			    var el = $(this),
                    productId = parseInt(el.attr('data-product-id')),
                    vendorId = parseInt(el.attr('data-vendor-id')),
					items = _.filter(poItems, function(p) {
					    return p.isOrdered && p.productId === productId && p.vendorId === vendorId;
					});

				$('#poCategories a').removeAttr("style");
				$(this).attr("style", "font-weight: bold; font-style: italic;");
				// empty po rows
				$('#productOrderViewRows').empty();

				// fill in po with data
				if (items.length > 0) {
				    var vendor = _.find(vendors, function(v) { return v.Vendor_ID == items[0].vendorId; });

				    if (vendor) {
				        var vendorInfo = '<span style="font-weight: bold; text-decoration: underline;">' + vendor.Vendor_Name + '</span>';

				        if (vendor.Account_Number) {
				            vendorInfo += '<br />' + vendor.Account_Number;
				        }

				        if (vendor.Phone_Number) {
				            vendorInfo += '<br />' + vendor.Phone_Number;
				        }

				        $("#poVendorInfo").html(vendorInfo);
				    }

				    $("#poProductName").text(items[0].productName);

					var currentBorough = null;

					var hasSubitems = false;

					for (var i = 0; i < items.length; i++) {
					    if (items[i].isSubType) {
					        hasSubitems = true;
					        break;
					    }
					}

					_.forEach(items, function(i, count) {
						if (i.borough != '' && (count === 0 || currentBorough !== i.borough)) {
							currentBorough = i.borough;
							$('#productOrderViewRows').append('<tr style="color: white; text-weight: bold; background-color: green; text-align: center;"><td colspan="' + (hasSubitems ? 4 : 3) +'">' + currentBorough + '</td></tr>');
						}

						if (hasSubitems) {
						    $('#productOrderViewTable thead tr th:first').show();
						    $('#productOrderViewTable thead tr th:eq(2)').attr('style', 'width: 200px; text-align: center;');
						    $('#productOrderViewTable').append('<tr><td style="text-align: center;">' + (i.productSubType ? i.productSubType : '') + '</td><td style="text-align: center;">' + i.district + '</td><td style="text-align: center;">' + i.address + '</td><td style="text-align: center;">' + i.orderAmount + '</td></tr>');
						} else {
						    $('#productOrderViewTable thead tr th:first').hide();
						    $('#productOrderViewTable thead tr th:eq(2)').attr('style', 'width: 286px; text-align: center;');
						    $('#productOrderViewTable').append('<tr><td style="text-align: center;">' + i.district + '</td><td style="text-align: center;">' + i.address + '</td><td style="text-align: center;">' + i.orderAmount + '</td></tr>');
						}
					});

					reStripe("productOrderViewTable");
					$('#poContainer').show();
				}
			});

			$('#validatePurchaseOrder').click(function () {
				if ($("#purchaseOrder").valid() && !submitting) {
					var po = $('#purchaseOrder').serializeArray(),
						position = 0,
						tempObj = null;

					poItems = [];

					// parse the array for po form values
					for (var i = 0, max = po.length; i < max; i++) {
						tempObj = {};
						tempObj.isOrdered = (po[i++].value === 'true');
						i++;
						tempObj.userId = po[i++].value;
						tempObj.userName = po[i++].value;
						tempObj.address = po[i++].value;
						tempObj.borough = po[i++].value;
						tempObj.district = po[i++].value;
						tempObj.poDisplayName = po[i++].value;

						if (po[i].name.indexOf('vendorId') > -1) {
						    var vendorInput = $('input[name="' + po[i].name + '"]');
						    var st = $('select[name="' + po[i].name +'"] option:selected:enabled');

						    if (vendorInput.length > 0) {
						        tempObj.vendorId = parseInt(vendorInput.val());
						        tempObj.vendorName = vendorInput.parent('td').text().trim();
						    } else if (st.length > 0) {
						        tempObj.vendorId = parseInt(st.val());
						        tempObj.vendorName = st.text().trim();
						    }

						    i++;
						} else {
							tempObj.vendorId = null;
							tempObj.vendorName = null;
						}

						tempObj.productId = parseInt(po[i++].value);
						tempObj.productName = po[i++].value + ' (' + po[i++].value + ')';
						tempObj.isSubType = po[i++].value === 'True';
							
						if (po[i].name.indexOf('productSubTypeId') > -1) {
						    var st = $('select[name="' + po[i++].name +'"] option:selected:enabled');
							tempObj.productSubTypeId = st.val();
							tempObj.productSubType = st.text();
						} else {
							tempObj.productSubTypeId = null;
						}
							
						tempObj.orderAmount = po[i].name.indexOf('orderAmount') > -1 ? po[i++].value : -1;

						tempObj.exceptionReason = po[i].value;

						poItems.push(tempObj);

						position++;
					}

					// sort by borough and name
					poItems = _.sortByAll(poItems, ['borough', 'userName'])
						.filter(function(item) {
							return item.isOrdered && item.orderAmount > 0;
						});

					// get categories
					var categories = _(poItems)
						.uniq(function(item) {
							return item.productName;
						})
						.sortBy('productName')
						.value();

					$('#poCategories').empty();
					$('#poContainer').hide();

					_.forEach(categories, function(c) {
					    var catHtml = '<li>' + c.productName;
					    var productVendors = _(poItems)
                            .filter(function(item) {
                                return item.productId === c.productId && !isNaN(item.vendorId);
                            })
                            .uniq(function(item) {
                                return item.vendorId
                            })
                            .value();

					    if (productVendors.length > 0) {
					        catHtml += '<ul style="margin-bottom: 5px;">';

					        _.forEach(productVendors, function(v) {
					            catHtml += '<li><a href="#" data-vendor-id="'+ v.vendorId + '" data-product-id="' + c.productId + '">' + v.vendorName + '</li>';
					        })
					        catHtml += '</ul>';
					    }

					    catHtml += '</li>';

					    $('#poCategories').append(catHtml);
					});

					$('#poCategories li:first ul li:first a').click();

					var dialog = $("#purchaseOrderView").dialog({
						autoOpen: true,
						height: $(window).height() - 100,
						width: 1000,
						modal: true,
						buttons: {
							"Process": function () {
							    showConfirmProcessModal(dialog);
							},
							Cancel: function () {
								dialog.dialog("close");
							}
						}
					});
				}
			});

			function showConfirmProcessModal(opener) {
			    var confirmDialog = $("#confirm-process").dialog({
			        autoOpen: true,
			        height: 175,
			        width: 300,
			        modal: true,
			        buttons: {
			            No: function () {
			                confirmDialog.dialog("close");
			            },
			            Yes: function () {
			                confirmDialog.dialog("close");

			                if (opener) {
			                    opener.dialog("close");
			                }

			                $("#validatePurchaseOrder").attr("disabled", true).addClass("ui-state-disabled");
			                $('#submitting').show();
			                submitting = true;
			                animateText('submitting', '.', 3, 750);

			                $('#purchaseOrder').submit();
			            }
			        }
			    });
			}

			$('#addPurchaseOrderLine').click(function () {
				var dialog = $("#purchaseOrderAddView").dialog({
					autoOpen: true,
					height: 375,
					width: 357,
					modal: true,
					open: function () {
						var form = $("#poForm"),
							location = $('select[id="poLocation"]'),
							fuelType = $('select[id="po"]'),
                            subType = $('select[id="subType"]'),
                            vendor = $('select[id="vendor"]'),
							orderAmount = $('input[id="poAmount"]'),
							exception = $('input[id="poException"]');

						form.validate({
							errorClass: "input-validation-error",
							errorPlacement: function(error, element) {
								error.hide();
							}
						});

						fuelType.get(0).options.length = 0; // delete all options
						subType.get(0).options.length = 0;
						vendor.get(0).options,length = 0;
						$('.sub-type').hide();
						$('.vendor').hide();

						location.bind('change', function() {
						    $('.sub-type').hide();
						    $('.vendor').hide();

							// get products for user
							$.get("/PurchaseOrder/GetProductsForUser", { userId: this.value }, function (data) {
							    var fuelTypeOptions = fuelType.get(0).options;
							    var vendorOptions = vendor.get(0).options;
							    var parsedData = JSON.parse(data);
							    var product = parsedData[0];
								
								fuelTypeOptions.length = 0;
								_.forEach(parsedData, function(p) {
								    fuelTypeOptions[fuelTypeOptions.length] = new Option(p.Product_Name, p.Product_ID);
								});

								vendorOptions.length = 0;
								if (product.Vendors && product.Vendors.length > 0) {
								    _.forEach(product.Vendors, function(v) {
								        vendorOptions[vendorOptions.length] = new Option(v.Vendor_Name, v.Vendor_ID);
								    });

								    $('.vendor').show();
								} else {
								    $('.vendor').hide();
								}

								selectedUserProducts = parsedData;
							}, 'json');

							// get user data to populate new row item with
							$.get('/PurchaseOrder/GetUserData', { userId: this.value }, function (data) {
								selectedUser = {};
								_.forEach(JSON.parse(data), function(d) { selectedUser[d.Key] = d.Value; });
							}, 'json');
						});

						fuelType.bind('change', function() {
                            var value = this.value;
                            var product = _.find(selectedUserProducts, function(p) {  return p.Product_ID == value; });
                            var vendorOptions = vendor.get(0).options;

                            if (product.is_Sub_Type && product.Sub_Types.length > 0) {
                                var subTypeOptions = subType.get(0).options;
                                subTypeOptions.length = 0;

                                _.forEach(product.Sub_Types, function(st) {
                                    subTypeOptions[subTypeOptions.length] = new Option(st.Sub_Type, st.Product_Sub_Type_ID);
                                });

                                $('.sub-type').show();
                            } else {
                                $('.sub-type').hide();
                            }

                            vendorOptions.length = 0;
                            if (product.Vendors && product.Vendors.length > 0) {
                                _.forEach(product.Vendors, function(v) {
                                    vendorOptions[vendorOptions.length] = new Option(v.Vendor_Name, v.Vendor_ID);
                                });

                                $('.vendor').show();
                            } else {
                                $('.vendor').hide();
                            }
						});

						// disable button
						$(".ui-dialog-buttonpane button:contains('Add')").attr("disabled", true).addClass("ui-state-disabled");

						// reset inputs
						location.get(0).selectedIndex = 0;
						fuelType.get(0).selectedIndex = 0;
						vendor.get(0).selectedIndex = 0;
						orderAmount.removeAttr('value');
						exception.removeAttr('value');

						// input validations
						orderAmount.rules('add', { required: true, digits: true, messages: { required: '', digits: '' } });
						exception.rules('add', { required: true, messages: { required: '' } });

						form.find(':input').bind('input', function () {
							var valid = form.valid();

							// enables/disables button based on form validity
							if (valid) {
								$(".ui-dialog-buttonpane button:contains('Add')").removeAttr("disabled").removeClass("ui-state-disabled");
							} else {
								$(".ui-dialog-buttonpane button:contains('Add')").attr("disabled", true).addClass("ui-state-disabled");
							}
						});

						// run validations
						orderAmount.valid();
						exception.valid();
					},
					buttons: {
						"Add": function () {
							// gather form values
							var location = $('select[id="poLocation"] option:selected'),
								fuelType = $('select[id="po"] option:selected'),
                                subType = $('select[id="subType"] option:selected'),
                                subTypeSelect = $('select[id="subType"]'),
                                vendor = $('select[id="vendor"] option:selected'),
                                vendorSelect = $('select[id="vendor"]'),
								orderAmount = $('input[id="poAmount"]'),
								exception = $('input[id="poException"]');

							// find row to copy, and an input to gather name/id info from
							var titleRow = $('.title-row:first'),
								boroughRow = $('.borough-row:first'),
								productRow = $('.product-row:first'),
								totalRow = $('.total-row:first'),
								input = productRow.find('input[name*="orderAmount"]');

							if (input.length > 0) {
								// get name, id and prefixes
								var name = input.attr('name'),
									id = input.attr('id'),
									namePrefix = name.substr(0, name.indexOf('.orderAmount')),
									idPrefix = id.substr(0, id.indexOf('__orderAmount'));

								rowCount++;

								// clone row and change text/inputs for new item
								var newProductRow = productRow.clone(),
									  selectedProduct = _.find(selectedUserProducts, { Product_Name: fuelType.text() }),
                                      locationHtml = newProductRow.find('td:eq(1)').html(),
									  titleHtml = newProductRow.find('td:eq(3)').html();

								newProductRow.attr('id', 'pos[' + rowCount + ']_row');

								newProductRow.find('td:eq(1)').html('<a href="<%=  reportUrl  %>'+ '&Product_Id=' + fuelType.val() + '&User_Id=' + selectedUser.userId + '">' + location.text() + '</a>' + locationHtml.substr(locationHtml.indexOf('<'), locationHtml.length));
								newProductRow.find('td:eq(3)').html(fuelType.text() + ' (' + selectedProduct.Measurement_Type + ')' + titleHtml.substr(titleHtml.indexOf('<'), titleHtml.length));
								newProductRow.find('#' + idPrefix + '__isOrdered').attr('name', 'pos[' + rowCount + '].isOrdered').attr('id', 'pos_' + rowCount + '__isOrdered').attr('checked', true);
								newProductRow.find('input[name="' + namePrefix + '.isOrdered"]').attr('name', 'pos[' + rowCount + '].isOrdered').val(false);
								newProductRow.find('#' + idPrefix + '__productId').attr('name', 'pos[' + rowCount + '].productId').attr('id', 'pos_' + rowCount + '__productId').val(fuelType.val());
								newProductRow.find('#' + idPrefix + '__userId').attr('name', 'pos[' + rowCount + '].userId').attr('id', 'pos_' + rowCount + '__userId').val(selectedUser.userId);
								newProductRow.find('#' + idPrefix + '__pos_aspnet_Users_UserName').attr('name', 'pos[' + rowCount + '].pos_aspnet_Users_UserName').attr('id', 'pos_' + rowCount + '__pos_aspnet_Users_UserName').val(selectedUser.userName);
								newProductRow.find('#' + idPrefix + '__address').attr('name', 'pos[' + rowCount + '].address').attr('id', 'pos_' + rowCount + '__address').val(selectedUser.address);
								newProductRow.find('#' + idPrefix + '__borough').attr('name', 'pos[' + rowCount + '].borough').attr('id', 'pos_' + rowCount + '__borough').val(selectedUser.borough);
								newProductRow.find('#' + idPrefix + '__district').attr('name', 'pos[' + rowCount + '].district').attr('id', 'pos_' + rowCount + '__district').val(selectedUser.district);
								newProductRow.find('#' + idPrefix + '__poDisplayName').attr('name', 'pos[' + rowCount + '].poDisplayName').attr('id', 'pos_' + rowCount + '__poDisplayName').val(selectedUser.poDisplayName);
								newProductRow.find('#' + idPrefix + '__orderAmount').attr('name', 'pos[' + rowCount + '].orderAmount').attr('id', 'pos_' + rowCount + '__orderAmount').attr('data_productId', fuelType.val()).val(orderAmount.val());
								newProductRow.find('#' + idPrefix + '__exceptionReason').attr('name', 'pos[' + rowCount + '].exceptionReason').attr('id', 'pos_' + rowCount + '__exceptionReason').val(exception.val());
								newProductRow.find('#' + idPrefix + '__pos_Product_Product_Name').attr('name', 'pos[' + rowCount + '].pos.Product.Product_Name').attr('id', 'pos_' + rowCount + '__pos_Product_Product_Name').val(selectedProduct.Product_Name);
								newProductRow.find('#' + idPrefix + '__pos_Product_Measurement_Type').attr('name', 'pos[' + rowCount + '].pos.Product.Measurement_Type').attr('id', 'pos_' + rowCount + '__pos_Product_Measurement_Type').val(selectedProduct.Measurement_Type);
								newProductRow.find('#' + idPrefix + '__pos_Product_is_Sub_Type').attr('name', 'pos[' + rowCount + '].pos.Product.is_Sub_Type').attr('id', 'pos_' + rowCount + '__pos_Product_is_Sub_Type').val(selectedProduct.is_Sub_Type);
								
								if (subType.val()) {
								    var select = subTypeSelect.clone();
								    select.attr('name', 'pos[' + rowCount + '].productSubTypeId').attr('id', 'pos_' + rowCount + '__productSubTypeId').val(subType.val());
								    select.attr('style', 'width: 100%;');
								    newProductRow.find('td:eq(4)').empty().append(select);
								}

								if (vendorSelect.get(0).options.length > 0 && vendor.val()) {
								    var select = vendorSelect.clone();
								    select.attr('name', 'pos[' + rowCount + '].vendorId').attr('id', 'pos_' + rowCount + '__vendorId').val(vendor.val()).attr('style', 'width: 100%;');
								    select.attr('style', 'width: 100%;');
								    newProductRow.find('td:eq(2)').empty().append(select);
								} else {
								    newProductRow.find('td:eq(2)').empty();
								}

								newProductRow.removeAttr('class').addClass('product-row').addClass(fuelType.val()).attr('id', 'pos[' + rowCount + ']_row');

								var borough = getBorough(location.text()),
									  fuelTypeExists = $('.title-row.' + fuelType.val()).length > 0,
									  fuelTypeBoroughExists = $('.' + borough + '.' + fuelType.val()).length > 0;

								// check for row with the same fuel type and borough
								if (fuelTypeExists && fuelTypeBoroughExists) {
									var row = $('.borough-row.' + borough + '.' + fuelType.val()).nextAll('.borough-row.' + fuelType.val() + ':first');
									if (row.length > 0) {
										var extraClass = row.prev('tr').hasClass('alt') ? '' : 'alt';
										row.before(newProductRow.removeClass('alt').addClass(extraClass));
									} else {
										var productTotalRow = $('.total-row.' + fuelType.val());
										var extraClass = productTotalRow.prev('tr').hasClass('alt') ? '' : 'alt';
										productTotalRow.before(newProductRow.removeClass('alt').addClass(extraClass));
										productTotalRow.show();
									}

									newProductRow.show();
								} else if (fuelTypeExists && !fuelTypeBoroughExists) {
									var productTotalRow = $('.total-row.' + fuelType.val()),
										  newBoroughRow = boroughRow.clone().removeAttr('class').addClass('borough-row').addClass(fuelType.val());

									newBoroughRow.find('th:first').text(borough);

									productTotalRow.before(newBoroughRow)
										  .before(newProductRow.removeClass('alt').addClass(extraClass));

									newBoroughRow.show();
									productTotalRow.show();
									newProductRow.show();
								} else if (!fuelTypeExists) {
									// need to create all rows for new fuel type
									var table = $('#productOrderTable'),
										newTitleRow = titleRow.clone().removeAttr('class').addClass('title-row').addClass(fuelType.val()),
										newBoroughRow = boroughRow.clone().removeAttr('class').addClass('borough-row').addClass(borough).addClass(fuelType.val()),
										newTotalRow = totalRow.clone().removeAttr('class').addClass('total-row').addClass(fuelType.val());

									// update title/borough/total values
									newTitleRow.find('td:first').html('<br><br>' + fuelType.text());
									newBoroughRow.find('th:first').text(borough);
									newTotalRow.find('span[id*="total"]').attr('id', 'total[' + fuelType.val() + ']');

									newBoroughRow.show();
									newTitleRow.show();
									newTotalRow.show();
									newProductRow.show();

									table.append(newTitleRow)
										.append(newBoroughRow)
										.append(newProductRow.addClass('alt'))
										.append(newTotalRow);
								}

								setProductTotal(fuelType.val());

								dialog.dialog("close");
							}
						},
						Cancel: function () {
							dialog.dialog("close");
						}
					}
				});
			});           
		});
	</script>
</asp:Content>