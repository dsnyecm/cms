﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.FuelFormViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Fuel Requirements and Equipment Form
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
	<script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.5.custom.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery.validate.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/lodash.3.9.3.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/Tabs.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
	<link href="<%=ResolveUrl("~/Content/jqueryUI.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Content/fuelformPrint.css")%>" rel="stylesheet" type="text/css" media="print" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<!--<h2>
        Fuel Requirements and Equipment Form
        <span class="page-title-help">&nbsp;&nbsp;&nbsp;[<a href="#" onclick="helpPopup('FuelRequirements')">?</a>]</span>
	</h2>-->
	<h2>
        Fuel Requirements and Equipment Form
   </h2>

	<p style="width: 1130px;" class="dottedLine"></p>

	<% Html.EnableClientValidation(); %>
	<% using (Html.BeginForm("Edit", "FuelForm", FormMethod.Post, new { id = "fuelForm" })) {%>
		<%: Html.HiddenFor(m => m.isAdmin, new { @Value = false }) %>
		<%: Html.HiddenFor(m => m.isEdit) %>

		<div style="float: left; width: 1130px;">
			<div class="iconLeft">
				<a href="" onclick="history.go(-1); return false;" class="iconLink">
					<img border="0" src="~/Content/Images/Icons/cancel.png" alt="Cancel" runat="server" /><br />Cancel
				</a>
			</div>
			<div class="iconLeft">
				<a href="#" id="submitButton" class="iconLink">
					<img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Submit" style="vertical-align: bottom;" runat="server" /><br />Submit
				</a>
			</div>
			<div id="submitting" style="display: none; padding-top: 15px;">Submitting</div>
		</div>

		<div class="clear"></div>

		<div style="float: left; margin-bottom: 10px; width: 1130px;">
			<div class="display-label" style="margin-top: .9em; width: 100px; font-weight: bold;"><label for="locationId">Location Id</label>: </div>
			<div class="display-field" style="margin: 0.9em 0; width: 125px;">
				<%: Model.submittedBy.userName %>
			</div>

			<div class="display-label" style="margin-top: .9em; font-weight: bold;"><label for="Supervisor_Full_Name">Supervisor</label>: </div>
			<div class="display-field" style="margin: 0.9em 0; width: 160px;">
				<%: Model.fuelForm.Supervisor_Full_Name %>
			</div>

			<div class="display-label" style="margin-top: .9em; font-weight: bold; width: 150px;"><label for="Supervisor_Badge_Number">Badge Number</label>: </div>
			<div class="display-field" style="margin: 0.9em 0; width: 185px;">
				<%: Model.fuelForm.Supervisor_Badge_Number %>
			</div>

			<div id="submission-date" style="margin-top: .9em;"><%: DateTime.Now.ToString("M/d/yyyy h:mm tt")  %></div>
		</div>

		<div class="clear"></div>

		<% var equipFailure = ViewData["EquipFailure"] != null ? (List<DSNY.Data.Fuel_Form_Equipment_Failure>)ViewData["EquipFailure"] : null; %>

		<ul id="tabnav">
			<li class="selectedTab"><a href="#" onclick="f_tcalHideAll();">Fuel Requirements</a></li>
			<li>
			<% if (Model.fuelFormEquipFailure.Exists(ef => ef.is_Equipment_Failure)) { %>
				<a href="#" class="highlight" onclick="f_tcalHideAll();">* Equipment Failure *</a>
			<% } else { %>
				<a href="#" onclick="f_tcalHideAll();">Equipment Failure</a>
			<% } %>
			</li>

			<li>
			<% if (Model.fuelForm.Remarks != null) { %>
				<a href="#" class="highlight" onclick="setTimeout(function () { $('#Remarks').focus() }, 10); f_tcalHideAll();">* General Remarks *</a>
			<% } else { %>
				<a href="#" onclick="setTimeout(function () { $('#Remarks').focus() }, 10); f_tcalHideAll();">General Remarks</a>
			<% } %>
			</li>
		</ul>

		<div id="tabsContainer" style="width: 1130px;">
            <div class="tabHeader">Fuel Requirements</div>
			<div class="tabContent">
				<% Html.RenderPartial("Grids/FuelReqEditGrid", Model); %>
			</div>

            <div class="tabHeader">Equipment Failure</div>
			<div class="tabContent">
				<% Html.RenderPartial("Grids/EquipFailEditGrid", Model); %>
			</div>

            <div class="tabHeader">General Remarks</div>
			<div class="tabContent">
				<%: Html.TextAreaFor(model => model.fuelForm.Remarks, new { @style= "width: 1070px;", @rows = "20", @maxlength = "1000" })%>
				<%: Html.HiddenFor(model => model.fuelForm.Fuel_Form_ID) %>
			</div>
		</div>

		<div class="iconContainer"></div>

		<% Html.RenderPartial("Modals/FuelForm"); %>
	<% } %>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
	<div id="noFloat" style="width: 1200px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
	<script type="text/javascript">
		var previousFuelForm = <%= (ViewData["PreviousFuelFormDetails"] != null ? ViewData["PreviousFuelFormDetails"] : "null") %>;
		var previousEquipFailures = <%= (ViewData["PreviousEquipmentFailuresJson"] != null ? ViewData["PreviousEquipmentFailuresJson"] : "null") %>;
		var previousExceptions = <%= (ViewData["PreviousExceptions"] != null ? ViewData["PreviousExceptions"] : "null") %>;

		<% Html.RenderPartial("Javascript/FuelForm"); %>
	</script>
</asp:Content>