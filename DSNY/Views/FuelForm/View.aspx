﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.FuelFormViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	View Fuel Form
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/Tabs.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssIncludes" runat="server">
    <link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Content/fuelformPrint.css")%>" rel="stylesheet" type="text/css" media="print" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Fuel Requirements and Equipment Form</h2>
    <p style="width: 1130px;" class="dottedLine"></p>

    <% using (Html.BeginForm("Create", "FuelForm", FormMethod.Post, new { id = "fuelForm" })) {%>

        <div style="float: left; width: 1130px;">
            <div class="iconLeft">
                <a href="" onclick="history.go(-1); return false;" class="iconLink">
                    <img border="0" src="~/Content/Images/Icons/cancel.png" alt="Close" runat="server" /><br />Close
                </a>
            </div>
        </div>

        <div class="clear"></div>

        <div style="float: left; margin-bottom: 10px; width: 1130px;">
            <div class="display-label" style="margin-top: .9em; width: 100px; font-weight: bold;"><label for="locationId">Location Id</label>: </div>
            <div class="display-field" style="margin: 0.9em 0; width: 125px;">
                <%: Model.submittedBy.userName %>
            </div>

            <div class="display-label" style="margin-top: .9em; font-weight: bold;"><label for="Supervisor_Full_Name">Supervisor</label>: </div>
            <div class="display-field" style="margin: 0.9em 0; width: 160px;">
                <%: Model.fuelForm.Supervisor_Full_Name %>
            </div>

            <div class="display-label" style="margin-top: .9em; font-weight: bold; width: 150px;"><label for="Supervisor_Badge_Number">Badge Number</label>: </div>
            <div class="display-field" style="margin: 0.9em 0; width: 185px;">
                <%: Model.fuelForm.Supervisor_Badge_Number %>
            </div>

            <div style="margin-top: .9em;"><%: Model.fuelForm.Submission_Date.Value.ToString("M/d/yyyy h:mm tt")  %></div>
        </div>

        <div class="clear"></div>

        <ul id="tabnav">
            <li class="selectedTab"><a href="#">Fuel Requirements</a></li>
            <% if (Model.fuelFormEquipFailure.Count() > 0) { %>
                <li><a href="#" class="highlight" onclick="f_tcalHideAll();">* Equipment Failure *</a></li>
            <% } else { %>
                <li><a href="#" onclick="f_tcalHideAll();">Equipment Failure</a></li>
            <% } %>

			<li>
			<% if (Model.fuelForm.Remarks != null) { %>
				<a href="#" class="highlight" onclick="setTimeout(function () { $('#Remarks').focus() }, 10); f_tcalHideAll();">* General Remarks *</a>
			<% } else { %>
				<a href="#" onclick="setTimeout(function () { $('#Remarks').focus() }, 10); f_tcalHideAll();">General Remarks</a>
			<% } %>
			</li>
        </ul>

        <div id="tabsContainer" style="width: 1130px;">
            <div class="tabHeader">Fuel Requirements</div>
            <div class="tabContent">
                <% Html.RenderPartial("Grids/FuelReqViewGrid", Model.fuelFormDetails); %>
            </div>

            <div class="tabHeader">Equipment Failure</div>
            <div class="tabContent">
                <% Html.RenderPartial("Grids/EquipFailViewGrid", Model.fuelFormEquipFailure); %>
            </div>

            <div class="tabHeader">General Remarks</div>
            <div class="tabContent">
                <% if (!string.IsNullOrEmpty(Model.fuelForm.Remarks)) { %>
                    <pre style="width: 1080px; font-family: Verdana,Helvetica,Sans-Serif; font-size: 1.2em; color: #000000; word-wrap: break-word; white-space: pre-wrap;"><%: Model.fuelForm.Remarks %></pre>
                <% } %>
            </div>
        </div>

    <% } %>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 1200px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#tabsContainer").tabs({ tab: 1 });
            preload(["<%=ResolveUrl("~/Content/Images/Icons/delete.png")%>"]);
        });
    </script>
</asp:Content>