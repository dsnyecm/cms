<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.StcTransferViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Street Treatment Transfers
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
	<script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery-migrate.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.12.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/knockout-3.4.2.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/knockout.mapping-latest.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/knockout.wrap.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery.validate.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jQuery.Validator.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jQuery-Validator.Knockout.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/bindinghandlers.js?v=0.06")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/Tabs.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/images/jqueryUI/jquery-ui-1.12.1.min.css")%>" rel="stylesheet" type="text/css" />
	<link href="<%=ResolveUrl("~/Content/images/jqueryUI/jquery-ui-1.12.icon-font.min.css")%>" rel="stylesheet" type="text/css" />
	<link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<h2>Street Treatment Transfers</h2>
	<p  style="width: 915px;" class="dottedLine"></p>

	<div id="contentContainer" style="padding:0;display:none;" data-bind="visible: show">
		<div style="float: left;">
			<div class="iconLeft">
				<a href="" class="iconLink" data-bind="click: cancel">
					<img border="0" src="~/Content/Images/Icons/cancel.png" alt="Close" runat="server" /><br />Close
				</a>
			</div>
            <div class="iconLeft" data-bind="if: Products().length > 0 && Sites[0]().length > 0 && TruckTypes().length > 0">
                <a href="" class="iconLink" data-bind="click: add">
                    <div style="margin-top: 2px"><img border="0" src="~/Content/Images/Icons/add.png" alt="Add Line" runat="server" /></div>
                    <div style="margin-top: 5px">
                        <span>Add Line</span>
                    </div>
                </a>
            </div>
			<div class="iconLeft">
				<a href="#" id="submitButton" class="iconLink" data-bind="click: save">
					<img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Save" style="vertical-align: bottom;" runat="server" /><br />Save
				</a>
			</div>
		</div>
		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

		<div id="saved" style="display: none; padding-top: 15px;">Your transfers have been saved.</div>
		<div id="finalized" style="display: none; padding-top: 15px;">Your transfer has been finalized.</div>

		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <div style="float: left; width: 915px">
            <div class="editor-field" style="width: 915px">
            	<div style="display: inline-block;">Transfer Start Date: <input type="input" class="RequestDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { name: 'StartDate' }" /></div>
            	<div style="display: inline-block;">End Date: <input type="input" class="RequestDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { name: 'EndDate' }" /></div>
                Site:
                <select data-bind="options: FromSites(null, null, 0),
                   optionsValue: 'id',
                   optionsText: 'description',
                   value: selectedSite,
                   optionsCaption: 'All'"></select>
                <input type="checkbox" data-bind="checked: Finalized"/> Show Finalized
                <input type="button" value="Retrieve" data-bind="click: function(){ changeFilter($root.selectedProduct); }" />
            </div>
        </div>

		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <div data-bind="if: Products().length == 0">There are no active STC Products set up.</div>
        <div data-bind="if: Sites[0]().length == 0">There are no STC Sites set up.</div>
        <div data-bind="if: TruckTypes().length == 0">There are no Truck Types set up.</div>

		<div style="width: 99%; display: table;" data-bind="jqValidation: validationContext">
			<table data-bind="foreach: StcTransfers" style="width: 100%;">
                <tr data-bind="css: { 'alt': $index() % 2 == 1 }">
					<td>
						<div class="row" style="padding-left: 20px;">
							<div style="width: 5%; display: table-cell; " style="padding-right: 0">
								<div>
									Commodity
								</div>
								<div>
									<select data-bind="options: $root.Products,
													   optionsValue: 'Id',
													   optionsText: 'Name',
													   value: ProductId, enable: Edit() && !Finalized()"></select>
								</div>
							</div>
							<div style="width: 5%; display: table-cell; " style="padding-right: 0">
								<div>
									From Site
								</div>
								<div>
									<select data-bind="options: $root.FromSites(FromUserId(), FromUserName(), ProductId()),
													   optionsValue: 'id',
													   optionsText: 'description',
													   value: FromUserId, enable: Edit() && !Finalized()"></select>
								</div>
							</div>
							<div style="width: 5%; display: table-cell; " style="padding-right: 0">
								<div>
									To Site
								</div>
								<div>
									<select data-bind="options: $root.ToSites(FromUserId(), ToUserId(), ToUserName(), ProductId()),
													   optionsValue: 'id',
													   optionsText: 'description',
													   value: ToUserId, enable: Edit() && !Finalized()"></select>
								</div>
							</div>
							<div style="width: 5%; display: table-cell; " style="padding-right: 0">
								<div>
									Truck Type
								</div>
								<div>
									<select data-bind="options: $root.TruckTypes,
													   optionsValue: 'Id',
													   optionsText: 'Name',
													   value: TruckTypeId, enable: Edit() && !Finalized(), event: { change: $root.LoadChange($data) }"></select>
								</div>
							</div>
							<div style="width: 7%; display: table-cell; ">
								<div>
									Transfer Date
								</div>
								<div>
									<input type="input" class="StartDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { onlyIf: !Finalized(), name: 'TransferDate', required: true }, uniqueName: true" />
								</div>
							</div>
							<div style="width: 5%; display: table-cell; ">
								<div>
									Loads
								</div>
								<div>
									<input type="text" class="Small" data-bind="numeric, value: Loads, event: { change: $root.LoadChange($data) }, enable: !Finalized()" data-required="true" maxlength="15" />
								</div>
							</div>
							<div style="width: 5%; display: table-cell; ">
								<div>
									Qty
								</div>
								<div>
									<input type="text" class="Small" data-bind="money, value: Qty, enable: !Finalized()" data-required="true" />
								</div>
							</div>
							<div style="width: 10%; display: table-cell; white-space:nowrap;">
								<div>
									Boro Snow Supervisor
								</div>
								<div style="white-space:nowrap">
									<input type="text" class="Small" data-bind="value: BoroSuperName, enable: !Finalized()" maxlength="50"/>
									<input type="text" class="Small " data-bind="value: BoroSuperBadge, enable: !Finalized()" maxlength="20"/>
								</div>
								<div style="display: inline-block; width: 102px;">
									Name
								</div>
								<div style="display: inline-block; width: 102px;">
									Badge
								</div>
							</div>
							<div style="width: 10%; display: table-cell; white-space:nowrap;">
								<div>
									San Supervisor
								</div>
								<div style="white-space:nowrap">
									<input type="text" class="Small" data-bind="value: SanSuperName, enable: !Finalized()" maxlength="50"/>
									<input type="text" class="Small" data-bind="value: SanSuperBadge, enable: !Finalized()" maxlength="20"/>
								</div>
								<div style="display: inline-block; width: 102px;">
									Name
								</div>
								<div style="display: inline-block; width: 102px;">
									Badge
								</div>
							</div>

    <!-- ko if: $root.Finalizable() && !Finalized() -->
							<div style="width: 5%; display: table-cell;">
								<div>&nbsp;</div>
								<div>
									<input type="button" class="Small" data-bind="click: $root.finalize" value="Finalize" />
								</div>
							</div>
    <!-- /ko -->
    <!-- ko if: Id() < 0 -->
                            <div style="width: 5%; display: table-cell;">
								<div>&nbsp;</div>
								<div><img src="/content/images/icons/delete.png" data-bind="click: $root.delete" class="tcalIcon" alt="Delete Item" title="Delete Item">
								</div>
							</div>
    <!-- /ko -->
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PrintArea" runat="server">
	<div id="printArea"></div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
	<script type="text/javascript">
		var obj = JSON.parse('<%= this.Model.Json %>');
		obj.loading = true;
		function findFirstDiffPos(a, b)
		{
		   var shorterLength = Math.min(a.length, b.length);

		   for (var i = 0; i < shorterLength; i++)
		   {
		       if (a[i] !== b[i]) return i;
		   }

		   if (a.length !== b.length) return shorterLength;

		   return -1;
		}
		obj.cancel = function() {
			var currentViewModelString = JSON.stringify(ko.wrap.toJS(vm.StcTransfers));
			if (viewModelString != currentViewModelString){
				if (!confirm("There are unsaved changes.  Are you sure you want to continue?")) return false;
			}
			history.go(-1); 
			return false;
		}
        obj.add = function () { this.BlankStcTransfer.Id(this.BlankStcTransfer.Id()-1); this.StcTransfers.push(ko.wrap.fromJS(ko.wrap.toJS(this.BlankStcTransfer))); }
        obj.delete = function (item) { 
		    vm.StcTransfers.remove(function(t) {
		        return t.Id() == item.Id();
		    });
		}
        obj.edit = function (item) { item.Edit(!item.Edit()); }
		obj.changeFilter= function(){
			var currentViewModelString = JSON.stringify(ko.wrap.toJS(vm.StcTransfers));
			if (viewModelString != currentViewModelString){
				if (!confirm("There are unsaved changes.  Are you sure you want to continue?")) return false;
			}
			vm.refresh();
		}
		obj.GetTruckType = function(id){
			return ko.utils.arrayFirst(vm.TruckTypes(), function(item){
				return item.Id() == id;
			});
		}
		obj.LoadChange = function(data){
			if (vm.loading()) return;
			if (data.Loads()){
				data.Qty(vm.GetTruckType(data.TruckTypeId()).Capacity() * data.Loads())
			}
		}
		obj.refresh = function(){
			var url = window.location.toString() + '/ChangeFilter/';
			if (typeof product == undefined) { product = vm.selectedProduct; }
			var viewModel = { External: ko.toJS(vm.External), StartDate: ko.toJS(vm.StartDate), EndDate: ko.toJS(vm.EndDate), selectedSite: ko.toJS(vm.selectedSite), Finalized: ko.toJS(vm.Finalized), CurrentUserId: ko.toJS(vm.CurrentUserId) };
			vm.loading(true);
			$.post({ url: url, 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				data: JSON.stringify(viewModel),
				success: function(data){
					ko.wrap.updateFromJS(vm.StcTransfers, data.StcTransfers);
					vm.loading(false);
					viewModelString = JSON.stringify(ko.wrap.toJS(vm.StcTransfers));
				},
				error: function(err){
					console.log(JSON.stringify(err));                    
				}
			});
		}
		obj.save = function () {
            if ($('.input-validation-error').length > 0){
                alert('There are invalid fields.  Please address them before saving.');
                return false;
            }
            var validationResult = this.validationContext.Validate();
            if (!validationResult.valid) {
                this.errors(validationResult.messages);
                alert('There are required fields missing.');
                return false;
            }

			var self = this;
			var viewModel = { values: ko.toJS(self.StcTransfers) };

			$.post({ url: window.location.toString() + '/Save', 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				//traditional: true,
				data: JSON.stringify(viewModel), 
				success: function(data){
					if (data != "Success"){
						alert(data);
						return false;
					}
					viewModelString = JSON.stringify(ko.wrap.toJS(vm.StcTransfers));
					$('#saved').show();
					setTimeout(function(){
						$('#saved').hide();
					}, 5000);
					self.refresh();
					console.log(JSON.stringify(data));
				}
			}).fail(function(err){
				console.log(JSON.stringify(err));       
			});
		}
		obj.finalize = function (d, e) {
            var validationResult = vm.validationContext.Validate();
            if (!validationResult.valid) {
                vm.errors(validationResult.messages);
                alert('There are required fields missing.  Please address issues with required fields before finalizing any transfers.');
                return false;
            }
			// var val = true;
			// $(e.target).closest(".row").find(":input").each(function(i, item){
			// 	if($(item).data('required') && !$(item).val()) {
			// 		val = false;
			// 	}
			// })
			// if (!val) {
   //          	alert('There are required fields missing.');
			// 	return false;
			// }
			var self = vm;
			d.Finalized(true);
			return;

			var viewModel = { value: ko.toJS(d) };

			$.post({ url: window.location.toString() + '/Finalize', 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				//traditional: true,
				data: JSON.stringify(viewModel), 
				success: function(data){
					if (data != "Success"){
						alert(data);
						return false;
					}
					viewModelString = JSON.stringify(ko.wrap.toJS(vm.StcTransfers));
					$('#finalized').show();
					setTimeout(function(){
						$('#finalized').hide();
					}, 5000);
					console.log(JSON.stringify(data));
				}
			}).fail(function(err){
				console.log(JSON.stringify(err));       
			});
		}
		obj.show = true;
		var vm = ko.wrap.fromJS(obj);
		vm.FromSites = function(fromId, fromName, productId) {
			return ko.computed(function () {
				var self = vm;
				var exists = false;  
				var sites = ko.utils.arrayFilter(self.Sites[productId](), function (item) {
					if (item.id() == fromId){ exists == true; }
					return true;
				});
				if (!exists && fromName != null){
					sites.push({id: ko.observable(fromId), description: ko.observable(fromName), visible: ko.observable(true)});
				}
				return sites;
			})();
		};
		vm.ToSites = function(fromId, toId, toName, productId) {
			return ko.computed(function () {
				var self = vm;
				var exists = false;  
				var sites = ko.utils.arrayFilter(self.Sites[productId](), function (item) {
					if (item.id() == toId){ exists == true; }
					return item.id() != fromId;
				});
				if (!exists && toName != null){
					sites.push({id: ko.observable(toId), description: ko.observable(toName), visible: ko.observable(true)});
				}
				return sites;
			})();
		};

		vm.validationContext = ko.jqValidation({
			returnBool:false, // We want more details of our validation result.
			useInlineErrors: true, // Use inline errors
            errorClass: 'input-validation-error', // Apply error class
            msg_empty: '', // Global empty message.
			noInlineErrors: "*[type='password']" // Password fields should not show inline errors.
		});        
	
		vm.errors = ko.observableArray([]);
		var viewModelString;
		$(function () {
			ko.applyBindings(vm, $('#contentContainer')[0]);
			vm.loading(false);
			viewModelString = JSON.stringify(ko.wrap.toJS(vm.StcTransfers));
		});
	</script>
</asp:Content>
