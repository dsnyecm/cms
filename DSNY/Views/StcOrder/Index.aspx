﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.StcOrderViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Street Treatment Product Orders
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
	<script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.12.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/knockout-3.4.2.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/knockout.wrap.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jQuery.Validator.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jQuery-Validator.Knockout.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/bindinghandlers.js?v=0.01")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/Tabs.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/images/jqueryUI/jquery-ui-1.12.1.min.css")%>" rel="stylesheet" type="text/css" />
	<link href="<%=ResolveUrl("~/Content/images/jqueryUI/jquery-ui-1.12.icon-font.min.css")%>" rel="stylesheet" type="text/css" />
	<link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<h2>Street Treatment Product Orders</h2>
	<p  style="width: 950px;" class="dottedLine"></p>

	<div id="contentContainer" style="padding:0;display:none;" data-bind="visible: show">
		<div style="float: left;">
			<div class="iconLeft">
				<a href="" class="iconLink" data-bind="click: cancel">
					<img border="0" src="~/Content/Images/Icons/cancel.png" alt="Close" runat="server" /><br />Close
				</a>
			</div>
			<div class="iconLeft">
				<a href="#" id="submitButton" class="iconLink" data-bind="click: preview">
					<img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Save" style="vertical-align: bottom;" runat="server" /><br />Save
				</a>
			</div>
		</div>
		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

		<div id="saved" style="display: none; padding-top: 15px;">Your orders have been saved.</div>

		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>
		<div style="width: 915px;">
			<div class="col-xs-4 pull-right" data-bind="if: PreviousOrderDates().length > 0">	
				<span data-bind="text: 'Previous ' + $root.selectedProduct.Name() + ' Orders:'"></span>		
				<select data-bind="options: PreviousOrderDates,
								   optionsValue: 'id',
								   optionsText: 'description',
								   value: selectedOrderDate,
								   optionsCaption: '',
								   event: { change: changeDate }"></select>
			</div>
		</div>

		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

		<ul id="tabnav" data-bind="foreach: Products">
			<li data-bind="css: { selectedTab: $root.selectedProduct.Id() == Id() }"><a href="#" data-bind="text: Name, click: $root.changeProduct"></a></li>
		</ul>

		<div id="tabsContainer" style="width: 950px;">
			<div class="tabHeader" data-bind="text: selectedProduct.Name()"></div>
			<div class="tabContent">
				<table style="width: 900px;">
					<tbody data-bind="foreach: Orders">
						<tr data-bind="if: $index() < 1 || Borough() != (($parent.Orders()[$index()-1]) ? $parent.Orders()[$index()-1].Borough() : '#')">
							<th data-bind="text: Borough"></th>
						</tr>
						<tr data-bind="css: { 'alt': $index() % 2 == 1 }">
							<td>
								<div class="row">
									<div class="col-xs-1" data-bind="if: $index() < 1 || District() != (($parent.Orders()[$index()-1]) ? $parent.Orders()[$index()-1].District() : '#')">
										<b><span data-bind="text: District"></span></b>
									</div>

									<div class="col-xs-2">
										<div class="col-xs-12" style="padding-left: 0">
											<b>Purchase Order #</b>
										</div>
										<div class="col-xs-12" style="padding-left: 0">
											<select data-bind="options: PurchaseOrders,
															   optionsValue: 'Id',
															   optionsText: 'PurchaseOrderNumber',
															   value: selectedPurchaseOrder"></select>
										</div>
									</div>
									<div class="col-xs-3">
										<div class="col-xs-12" style="padding-left: 0">
											<b>Vendor</b>
										</div>
										<div class="col-xs-12" style="padding-left: 0">
											<span data-bind="text: $root.GetPurchaseOrder(PurchaseOrders, selectedPurchaseOrder).Vendor()"></span>
										</div>
									</div>
									<div class="col-xs-2">
										<div>
											<b>Available Quantity (Ordered)</b></b>
										</div>
										<div>
											<span data-bind="text: $root.GetPurchaseOrder(PurchaseOrders, selectedPurchaseOrder).AmountRemaining()"></span>
										</div>
									</div>
									<div class="col-xs-2">
										<div>
											<b>Available Quantity (Delivered)</b></b>
										</div>
										<div>
											<span data-bind="text: $root.GetPurchaseOrder(PurchaseOrders, selectedPurchaseOrder).AmountRemainingForDelivery()"></span>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-1">
									</div>
									<div class="col-xs-2">
										<div><b>Address</b></div>
										<div><span data-bind="text: Address"></span></div>
									</div>
									<div class="col-xs-2" style="padding-right: 0">
										<div>
											<b><span data-bind="text: 'Quantity (' + $root.selectedProduct.Measurement() + ')'"></span></b>
										</div>
										<div>
											<input type="text" class="Qty" data-bind="money, value: Qty"/>
										</div>
									</div>
									<div class="col-xs-1" data-bind="if: $root.selectedProduct.LoadSize()" style="padding-left: 0">
										<div>
											<b><span>Loads</span></b>
										</div>
										<div>
											<span data-bind="text: (!Qty()) ? '' : parseFloat(Qty() / $root.selectedProduct.LoadSize()).toFixed(2)"></span>
										</div>
									</div>
									<div class="col-xs-2">
										<div>
											<b>Total Capacity</b>
										</div>
										<div>
											<span data-bind="text: Capacity"></span>
										</div>
									</div>
									<div class="col-xs-1">
										<div class="col-xs-12" style="padding-left: 0">
											<b>On Hand Amount</b>
										</div>
										<div class="col-xs-12" style="padding-left: 0">
											<span data-bind="text: OnHand"></span>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>	
		<div id="orderPreview" class="modal" style="overflow: hidden;">
			<div class="col-xs-12" style="text-align: center;">
				<h1>New York City<br/>
				Department of Sanitation</h1>
			</div>
			<table id="orderPreviewTable" style="width: 100%">
				<thead>
				<tr>
					<th>Borough</th>
					<th>District</th>
					<th>Location</th>
					<th>Phone Number</th>
					<th>Purchase Order #</th>
		<!-- ko if: $root.selectedProduct.Name() == 'Sand' -->
					<th>Yards</th>
					<th>Loads</th>
		<!-- /ko -->
		<!-- ko if: $root.selectedProduct.Name().substring(0,7) == 'Calcium' -->
		<!-- ko if: 1==0 -->
					<th>Number of Tanks</th>
		<!-- /ko -->
					<th>Tank Capacity</th>
					<th>Gallons Ordered</th>
		<!-- /ko -->
		<!-- ko if: $root.selectedProduct.Name() == 'Salt' -->
					<th>Tons</th>
		<!-- /ko -->
				</tr>
				</thead>
				<tbody data-bind="foreach: filterOrders">
					<tr data-bind="css: { 'alt': $index() % 2 == 1 }">
						<td><span data-bind="text: Borough"></span></td>
						<td><span data-bind="text: District"></span></td>
						<td><span data-bind="text: Address"></span></td>
						<td><span data-bind="text: Telephone"></span></td>
						<td><span data-bind="text: $root.GetPurchaseOrder(PurchaseOrders, selectedPurchaseOrder).PurchaseOrderNumber"></span></td>
		<!-- ko if: $root.selectedProduct.Name() == 'Sand' -->
						<td style="text-align: right"><span data-bind="text: Qty"></span></td>
						<td style="text-align: right"><span data-bind="text: parseFloat(Qty() / $root.selectedProduct.LoadSize()).toFixed(2)"></span></td>
		<!-- /ko -->
		<!-- ko if: $root.selectedProduct.Name().substring(0,7) == 'Calcium' -->
		<!-- ko if: 1==0 -->
						<td><span data-bind="text: Qty"></span></td>
		<!-- /ko -->
						<td><span data-bind="text: Capacity"></span></td>
						<td><span data-bind="text: Qty"></span></td>
		<!-- /ko -->
		<!-- ko if: $root.selectedProduct.Name() == 'Salt' -->
						<td><span data-bind="text: Qty"></span></td>
		<!-- /ko -->
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PrintArea" runat="server">
	<div id="printArea"></div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
	<script type="text/javascript">
		var obj = JSON.parse('<%= this.Model.Json %>');
		function findFirstDiffPos(a, b)
		{
		   var shorterLength = Math.min(a.length, b.length);

		   for (var i = 0; i < shorterLength; i++)
		   {
			   if (a[i] !== b[i]) return i;
		   }

		   if (a.length !== b.length) return shorterLength;

		   return -1;
		}
		obj.cancel = function() {
			var currentViewModelString = JSON.stringify(ko.wrap.toJS(vm.Orders));
			if (viewModelString != currentViewModelString){
				if (!confirm("There are unsaved changes.  Are you sure you want to continue?")) return false;
			}
			history.go(-1); 
			return false;
		}
		obj.changeProduct = function(){
			var currentViewModelString = JSON.stringify(ko.wrap.toJS(vm.Orders));
			if (viewModelString != currentViewModelString){
				if (!confirm("There are unsaved changes.  Are you sure you want to continue?")) return false;
			}
			vm.refresh(this.Id());
		}
		obj.preview = function(){		
			var self = this;
			var dict = {};
			var bcontinue = true;
			ko.utils.arrayFirst(this.Orders(), function(o) {
				if (parseInt(o.Qty())){
					var obj;
					if (!dict[o.selectedPurchaseOrder()]){ obj = { Qty: parseInt(o.Qty()), PurchaseOrderNumber: vm.GetPurchaseOrder(o.PurchaseOrders, o.selectedPurchaseOrder).PurchaseOrderNumber(), OrderedAmountRemaining: vm.GetPurchaseOrder(o.PurchaseOrders, o.selectedPurchaseOrder).AmountRemaining(), DeliveredAmountRemaining: vm.GetPurchaseOrder(o.PurchaseOrders, o.selectedPurchaseOrder).AmountRemainingForDelivery() }; 
						dict[o.selectedPurchaseOrder()] = obj;
					}
					else { 
						obj = dict[o.selectedPurchaseOrder()];
						obj.Qty += parseInt(o.Qty()); 
					}
					if ( obj.Qty > obj.DeliveredAmountRemaining ){
						obj.OverDeliveredAmount = true;
					}
					if ( obj.Qty > obj.OrderedAmountRemaining ){
						obj.OverOrderedAmount = true;
					}
					if ( o.Qty() > o.Capacity() ){
						if (!confirm("The Quantity amount for " + o.District() + " " + o.Qty() + " exceeds the total capacity of " + o.Capacity() + ".  Do you want to continue?")) { bcontinue = false; }
					}
				}
			});
			if (!bcontinue) { return false; }

			if(JSON.stringify(dict) == '{}') { alert('There is nothing to save.'); return false; }

			bcontinue = true;
			$(dict).each(function(i, item){
				var keys = Object.keys(item);
				if (item[keys[i]].OverDeliveredAmount){
					alert("The orders cannot be placed.  The combined Quantity " + item[keys[i]].Qty + " exceeds the Available Quantity (Delivered) " + item[keys[i]].DeliveredAmountRemaining + " for Purchase Order Number: " + item[keys[i]].PurchaseOrderNumber + ".");
					bcontinue = false;
				}
				if (bcontinue){
					if (item[keys[i]].OverOrderedAmount){
						if (!confirm("The combined Quantity " + item[keys[i]].Qty + " exceeds the Available Quantity (Ordered) " + item[keys[i]].OrderedAmountRemaining + " for Purchase Order Number: " + item[keys[i]].PurchaseOrderNumber + ".  Would you like to override?")) { bcontinue = false; }
					}
				}
			});

			if (!bcontinue){ return false; }

			var orderPreviewDialog = $("#orderPreview").dialog({
				autoOpen: true,
				height: 400,
				width: 600,
				modal: true,
				title: "Preview " + vm.selectedProduct.Name() + " Order",
				buttons: {
					Cancel: function () {
						$(this).dialog("close");
        				$("div.ui-dialog").remove();
					},
					Process: function () {
						$(this).dialog("close");
        				$("div.ui-dialog").remove();
						vm.save();
					}
				}
			});
		}
		obj.refresh = function(id){
			var url = window.location.toString() + '/ChangeProduct/';
			if (id) { url += id; }
			$.post({ url: url, 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				success: function(data){
					vm.pauseChangeDate = true;
					ko.wrap.updateFromJS(vm.selectedProduct.Id, data.selectedProduct.Id);
					ko.wrap.updateFromJS(vm.selectedProduct.Name, data.selectedProduct.Name);
					ko.wrap.updateFromJS(vm.selectedProduct.Measurement, data.selectedProduct.Measurement);
					ko.wrap.updateFromJS(vm.selectedProduct.LoadSize, data.selectedProduct.LoadSize);
					ko.wrap.updateFromJS(vm.Orders, data.Orders);
					ko.wrap.updateFromJS(vm.PreviousOrderDates, data.PreviousOrderDates);
					vm.selectedOrderDate(undefined);
					vm.pauseChangeDate = false;
					viewModelString = JSON.stringify(ko.wrap.toJS(vm.Orders));
				},
				error: function(err){
					console.log(JSON.stringify(err));                    
				}
			});
		}
		obj.changeDate = function(value){
			if (vm.pauseChangeDate) return;
			var currentViewModelString = JSON.stringify(ko.wrap.toJS(vm.Orders));
			if (viewModelString != currentViewModelString){
				if (!confirm("There are unsaved changes.  Are you sure you want to continue?")) return false;
			}
			if (!value.selectedOrderDate()) return vm.refresh(value.selectedProduct.Id());
			var viewModel = { selectedProduct: ko.toJS(value.selectedProduct), selectedOrderDate: ko.toJS(value.selectedOrderDate) };

			var url = window.location.toString() + '/ChangeDate/';
			$.post({ url: url, 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				data: JSON.stringify(viewModel), 
				success: function(data){
					ko.wrap.updateFromJS(vm.Orders, data.Orders);
					viewModelString = JSON.stringify(ko.wrap.toJS(vm.Orders));
				},
				error: function(err){
					console.log(JSON.stringify(err));                    
				}
			});
		}
		obj.GetPurchaseOrder = function(orders, id){
			return ko.utils.arrayFirst(orders(), function(item){
				return item.Id() == id();
			});
		}
		obj.toggle = function (item) { if (!item.Active()){ item.Qty(null); item.Capacity(null); this.deletes().push(item); } }
		obj.deletes = [];
		obj.save = function () {
			var self = this;
			var viewModel = { selectedProduct: ko.toJS(self.selectedProduct), Orders: ko.toJS(self.filterOrders) };

			$.post({ url: window.location.toString() + '/Save', 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				//traditional: true,
				data: JSON.stringify(viewModel), 
				success: function(data){
					$('#saved').show();
					setTimeout(function(){
						$('#saved').hide();
					}, 5000);
					self.refresh(self.selectedProduct.Id());
					console.log(JSON.stringify(data));
				}
			}).fail(function(err){
				console.log(JSON.stringify(err));       
			});
		}
		obj.show = true;
		var vm = ko.wrap.fromJS(obj);
		vm.filterOrders = ko.computed(function () {
			var self = vm;
			return ko.utils.arrayFilter(self.Orders(), function (item) {
				return item.Qty() && item.Qty() > 0;
			});
		});

		vm.validationContext = ko.jqValidation({
			returnBool:false, // We want more details of our validation result.
			useInlineErrors: true, // Use inline errors
			errorClass: 'input-validation-error', // Apply error class
			msg_empty: '', // Global empty message.
			noInlineErrors: "*[type='password']" // Password fields should not show inline errors.
		});
	
		vm.errors = ko.observableArray([]);
		var viewModelString;
		$(function () {
			console.log(ko.toJSON(vm.PreviousOrderDates));
			ko.applyBindings(vm, $('#contentContainer')[0]);
			viewModelString = JSON.stringify(ko.wrap.toJS(vm.Orders));
		});
	</script>
</asp:Content>