﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.SiteOnHandViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Set Site On Hand/Capacity
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout-3.4.2.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout.wrap.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jQuery.Validator.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jQuery-Validator.Knockout.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/bindinghandlers.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssIncludes" runat="server">
    <link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Set Site On Hand/Capacity</h2>
    <p  style="width: 915px;" class="dottedLine"></p>

    <div id="contentContainer" style="width: 840px;display:none;" data-bind="visible: show, jqValidation: validationContext">
        <div style="float: left;">
            <div class="iconLeft">
                <a href="" onclick="history.go(-1); return false;" class="iconLink">
                    <img border="0" src="~/Content/Images/Icons/cancel.png" alt="Close" runat="server" /><br />Close
                </a>
            </div>
            <div class="iconLeft">
                <a href="#" id="submitButton" class="iconLink" data-bind="click: save">
                    <img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Save" style="vertical-align: bottom;" runat="server" /><br />Save
                </a>
            </div>
            <div id="submitting" style="display: none; padding-top: 15px;">Submitting</div>
        </div>

        <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <div id="saved" style="display: none; padding-top: 15px;">Your changes have been saved.</div>

        <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <div style="float: left;">
            <div class="editor-field">Site:
                <select data-bind="options: Sites,
                   optionsValue: 'id',
                   optionsText: 'description',
                   value: selectedSite,
                   optionsCaption: 'All'"></select>
                <input type="button" value="Retrieve" data-bind="click: changeSite" />
            </div>
        </div>

        <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <div id="SiteOnHandDetails" style="display: inline-block; overflow-y: auto;">
        <% Html.RenderPartial("Grids/SiteProductOnHand", Model.SiteOnHands); %>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PrintArea" runat="server">
    <div id="printArea"></div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        var obj = JSON.parse('<%= this.Model.Json %>');
        obj.changeSite = function () { this.filteredSite(this.selectedSite()); }
        obj.toggle = function (item) { console.log(ko.toJSON(item)); if (!item.Active()){ item.Qty(null); item.Capacity(null); item.Deleted(true); } else { item.Deleted(false); } console.log(ko.toJSON(item)); }
        obj.deletes = [];
        obj.save = function () {
            var validationResult = this.validationContext.Validate();
            if (!validationResult.valid) {
                this.errors(validationResult.messages);
                alert('There are required fields missing.')
                return false;
            }

            var values = ko.toJS(this.filterOnHand()).filter(function(x){
                return x.Active === true || x.Deleted === true;
            });

            $.post({ url: window.location.toString() + '/Save', 
                type: 'post', 
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                //traditional: true,
                data: JSON.stringify(values), 
                success: function(data){
                    $('#saved').show();
                    setTimeout(function(){
                        $('#saved').hide();
                    }, 5000);
                    console.log(JSON.stringify(data));
                },
                error: function(err){
                    console.log(JSON.stringify(err));                    
                }
            })
        }
        obj.show = true;
        obj.filteredSite = null;
        var vm = ko.wrap.fromJS(obj);
        vm.filterOnHand = ko.computed(function() {
            var self = vm;
            if(!self.filteredSite()) {
                return self.SiteOnHands(); //initial load when no filter is specified
            } else {
                return ko.utils.arrayFilter(self.SiteOnHands(), function(item) {
                    return item.UserId() === self.filteredSite();
                });
            }
        });

        vm.validationContext = ko.jqValidation({
            returnBool:false, // We want more details of our validation result.
            useInlineErrors: true, // Use inline errors
            errorClass: 'input-validation-error', // Apply error class
            msg_empty: '', // Global empty message.
            noInlineErrors: "*[type='password']" // Password fields should not show inline errors.
        });
        
        vm.errors = ko.observableArray([]);
        $(function () {
            ko.applyBindings(vm, $('#contentContainer')[0]);
        });
    </script>
</asp:Content>