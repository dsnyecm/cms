<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.StcAgencyViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    STC Agency Maintenance
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout-3.4.2.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout.mapping-latest.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout.wrap.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jQuery.Validator.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jQuery-Validator.Knockout.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/bindinghandlers.js?v=0.03")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssIncludes" runat="server">
    <link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>STC Agency Maintenance</h2>
    <p  style="width: 915px;" class="dottedLine"></p>

    <div id="contentContainer" style="width: 1000px;display:none;" data-bind="visible: show, jqValidation: validationContext">
        <div style="float: left;">
            <div class="iconLeft">
                <a href="" class="iconLink" data-bind="click: cancel">
                    <img border="0" src="~/Content/Images/Icons/cancel.png" alt="Close" runat="server" /><br />Close
                </a>
            </div>
            <div class="iconLeft">
                <a href="" class="iconLink" data-bind="click: add">
                    <div style="margin-top: 2px"><img border="0" src="~/Content/Images/Icons/add.png" alt="Add Line" runat="server" /></div>
                    <div style="margin-top: 5px">
                        <span>Add Line</span>
                    </div>
                </a>
            </div>
            <div class="iconLeft">
                <a href="#" id="submitButton" class="iconLink" data-bind="click: save">
                    <img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Save" style="vertical-align: bottom;" runat="server" /><br />Save
                </a>
            </div>
            <div id="submitting" style="display: none; padding-top: 15px;">Submitting</div>
        </div>

        <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <div id="saved" style="display: none; padding-top: 15px;">Your changes have been saved.</div>

        <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <div id="StcAgencyDetails" style="display: inline-block; overflow-y: auto;">            
            <div data-bind="visible: filterDisplay().length > 0">
                <table style="width: 100%">
                    <thead>
                    <tr>
                        <th>Active</th>
                        <th>Name</th>
                        <th>Code</th>
                        <th>Address</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Billable</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: filterDisplay">
                        <tr data-bind="css: { 'alt': $index() % 2 == 1 }">
                            <td><input type="checkbox" data-bind="checked: Active"/></td>
                            <td><input type="text" class="Name" data-bind="value: Name" data-required="true" maxlength="50" /></td>
                            <td><input type="text" class="Small" data-bind="value: Code" maxlength="10" /></td>
                            <td><input type="text" class="Address" data-bind="value: Address" maxlength="200" /></td>
                            <td><input type="text" class="Email" data-bind="value: Email" maxlength="200" /></td>
                            <td><input type="text" class="Small" data-bind="value: Phone" maxlength="50" /></td>
                            <td><input type="checkbox" data-bind="checked: Billable"/></td>
                            <td><img src="/content/images/icons/delete.png" data-bind="click: $root.delete" class="tcalIcon" alt="Delete Item" title="Delete Item"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div data-bind="visible: filterDisplay().length === 0">
                There are no STC Agencies.
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PrintArea" runat="server">
    <div id="printArea"></div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        var obj = JSON.parse('<%= this.Model.Json %>');
        obj.filterValue = -1;
        obj.changeFilter = function () { if (this.OpenOnly()) { this.filterValue(0); } else { this.filterValue(-1); } }
        obj.delete = function (item) { console.log(item); item.Deleted(true); }
        obj.add = function () { this.StcAgencies.push(ko.wrap.fromJS(ko.wrap.toJS(this.BlankStcAgency))); }
        obj.toggle = function (id) { A_TCALS[id].f_toggle(); }
        obj.cancel = function() {
            var currentViewModelString = JSON.stringify(ko.wrap.toJS(vm.StcAgencies));
            if (viewModelString != currentViewModelString){
                if (!confirm("There are unsaved changes.  Are you sure you want to continue?")) return false;
            }
            history.go(-1); 
            return false;
        }
        obj.clear = function (id) { A_TCALS[id].f_clear(); }
        obj.save = function () {
            var validationResult = this.validationContext.Validate();
            console.log(validationResult);
            if (!validationResult.valid) {
                this.errors(validationResult.messages);
                alert('There are required fields missing.')
                return false;
            }

            var values = ko.toJS(this.filterSave());
            var loc = window.location.toString();
            $.post({ url: loc + '/Save', 
                type: 'post', 
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                //traditional: true,
                data: JSON.stringify(values), 
                success: function(data){
                    $('#saved').show();
                    setTimeout(function(){
                        $('#saved').hide();
                    }, 5000);
                    vm.refresh();
                },
                error: function(err){
                    console.log(JSON.stringify(err));                    
                }
            })
        }
        obj.refresh = function () {
            var loc = window.location.toString();
            $.post({ url: loc + '/Refresh', 
                type: 'post', 
                contentType: 'application/json; charset=utf-8',
                success: function(data){
                    ko.wrap.updateFromJS(vm.StcAgencies, data.StcAgencies);
                    viewModelString = JSON.stringify(ko.wrap.toJS(vm.StcAgencies));
                },
                error: function(err){
                    console.log(JSON.stringify(err));                    
                }
            })
        }
        obj.show = true;

        var vm = ko.wrap.fromJS(obj);
        vm.filterDisplay = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.StcAgencies(), function (item) {
                return !item.Deleted();
            });
        });
        vm.filterSave = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.StcAgencies(), function (item) {
                return !(item.Deleted() && !item.Id());
            });
        });

        vm.validationContext = ko.jqValidation({
            returnBool:false, // We want more details of our validation result.
            useInlineErrors: true, // Use inline errors
            errorClass: 'input-validation-error', // Apply error class
            msg_empty: '', // Global empty message.
            noInlineErrors: "*[type='password']" // Password fields should not show inline errors.
        });
    
        vm.errors = ko.observableArray([]);
        var viewModelString;
        $(function () {
            ko.applyBindings(vm, $('#contentContainer')[0]);
            viewModelString = JSON.stringify(ko.wrap.toJS(vm.StcAgencies));
        });
    </script>
</asp:Content>
