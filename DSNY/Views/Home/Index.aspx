﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master"  Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.LogOnModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Login
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/MicrosoftAjax.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/MicrosoftMvcValidation.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery.cookie.js")%>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            //pass cookie name 
            if (checkCookie('CMS_User_Signed_In')) {
                $('#timeout').show();
                $.removeCookie('CMS_User_Signed_In', { path: '/' });
            } else {
                $('#timeout').hide();
            }
        });

        function checkCookie($name)
        {
            if ('<%=Request.Cookies["CMS_User_Signed_In"] != null ? Request.Cookies["CMS_User_Signed_In"].Value : "" %>' === 'true'){
                return true;
            } else {
                return false;
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm()) { %>
        <div id="loginContainer">
            <span id="timeout" class="session-timeout" style="display:none"><%=System.Configuration.ConfigurationManager.AppSettings["TimeoutMessage"]%></span>
            <%: Html.ValidationMessage("LoginError", new { @id = "loginError" }) %>
			<%: Html.ValidationMessage("LoginInfo", new { @id = "loginInfo" }) %>
            <!--<div id="loginHeader">
                Please sign in with your user name and password&nbsp;
                [<a style="color: #FFFFFF;" href="#" onclick="helpPopup('Login')">?</a>]</div>-->
            <div id="loginHeader">
                Please sign in with your user name and password</div>
            <div id="loginBox"">
                <div class="display-label">
                    <%: Html.ValidationMessageFor(m => m.UserName) %>
                    <%: Html.LabelFor(m => m.UserName) %>:
                </div>
                <div class="display-field">
                    <%: Html.TextBoxFor(m => m.UserName) %>
                </div>
                
                <div class="clear"></div>

                <div class="display-label">
                    <%: Html.ValidationMessageFor(m => m.Password) %>
                    <%: Html.LabelFor(m => m.Password) %>:
                </div>
                <div class="display-field">
                    <%: Html.PasswordFor(m => m.Password)%>
                </div>
                
                <div class="clear"></div>

                <p class="loginButton">
                    <input type="submit" value="Log On" />
                </p>
            </div>
        </div>
    <% } %>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 690px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            // focus on the user name field on load
            $("#UserName").focus();
        });
    </script>
</asp:Content>