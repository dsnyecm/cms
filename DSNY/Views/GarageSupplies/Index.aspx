﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Site Administration
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.5.custom.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/jqueryUI.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="contentContainer" class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <h2>Garage Tool Administration</h2>
                <p class="fullDottedLine"></p>
            </div>
        </div>
        <div class="row">
            <% if (Roles.IsUserInRole("GS Warehouse") || Roles.IsUserInRole("GS HQ")) { %>
            <div class="col-md-2" style="padding-left: 25px;">
                <h3 style="margin: 0;">Tools Maintenance</h3>
                <ul>
                    <li><%: Html.ActionLink("Tool Type Maintenance", "ToolTypes", "ToolsMaintenance") %></li>
                    <li><%: Html.ActionLink("Tools, Power Tools and Quota", "Maintain", "ToolsMaintenance")%></li>
                </ul>
            </div>
            <% } %>

            <% if (Roles.IsUserInRole("GS Borough") || Roles.IsUserInRole("GS District") || Roles.IsUserInRole("GS Warehouse") || Roles.IsUserInRole("GS HQ")) { %>
            <div class="col-md-2">
                <h3 style="margin: 0;">Manage Inventory</h3>
                <ul>
                    <% if (Roles.IsUserInRole("GS Borough") || Roles.IsUserInRole("GS HQ")) { %>
                    <li><%: Html.ActionLink("Reassign Power Tools", "ReassignPowerTools", "ManageInventory") %></li>
                    <% } %>
                    <% if (Roles.IsUserInRole("GS HQ")) { %>
                    <li><%: Html.ActionLink("Inventory Request", "CreateInventoryRequest", "ManageInventory") %></li>
                    <% } %>
                    <li><%: Html.ActionLink("Inventory Submission", "InventorySubmission", "ManageInventory") %></li>
                </ul>
            </div>
            <% } %>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 550px;">&nbsp;</div>
</asp:Content>
