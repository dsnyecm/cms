﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.ToolTypesViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Tool Type Maintenance
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery.validate-1.17.0.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.12.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout-3.4.2.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout.mapping-latest.js")%>" type="text/javascript"></script>    
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/bindinghandlers.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jqValidate-bindinghandlers.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/lodash.3.9.3.min.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/jquery-ui.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="contentContainer" class="container-fluid">
        <!-- HEADER -->
        <div class="row">
            <div class="col-md-7">
                <h2>Tool Type Maintenance</h2>
                <p class="fullDottedLine"></p>
            </div>
        </div>

        <!-- ICONS -->
        <div class="row padded-bottom">
            <div class="col-md-12">
                <a href="" class="iconLink iconLeft" onclick="history.go(-1); return false;">
                    <img border="0" src="~/Content/Images/Icons/Cancel.png" alt="Cancel" runat="server" /><br />Cancel
                </a>

                <a href="" class="iconLink iconLeft" data-bind="click: $root.addToolType">
                    <img border="0" src="~/Content/Images/Icons/add.png" alt="Add Tool Type" runat="server" /><br />Add Tool Type
                </a>

                <a href="" id="submitButton" class="iconLink iconLeft" data-bind="click: save">
                    <img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Save" runat="server" /><br />Save
                </a>
            </div>
        </div>

        <div class="row padded-top">
            <div class="col-md-12">
                <label class="pointer"><input type="checkbox" data-bind="checked: showInactive" />Show Inactive</label>
            </div>
        </div>

        <!-- TABLE -->
        <div class="row spaced">
            <form action="#" id="toolTypeForm" class="col-md-7" data-bind="jqValidationForm: validationContext">
		        <table>
			        <tr>
				        <th>Active</th>
				        <th>Product Name</th>
				        <th>Power Tool</th>
				        <th>Measurement</th>
				        <th>Units Per Measurement</th>
				        <th>&nbsp;</th>
			        </tr>

                    <tbody data-bind="foreach: toolTypes, striped: true">
                        <tr>
                            <td style="text-align: center;">
                                <input type="checkbox" data-bind="checked: is_Active, uniqueName: true" />
                            </td>
                            <td>
                                <input  class="tool-type-name" data-bind="value: Tool_Type_Name, uniqueName: true, jqValidationElement: true" />
                            </td>
                            <td style="text-align: center;">
                                <input type="checkbox" data-bind="checked: is_PowerTool, uniqueName: true" />
                            </td>
                            <td>
                                <select data-bind="options: $root.measurements,
                                    optionsValue: 'Measurement_id',
                                    optionsText: 'Measurement_Name',
                                    value: Measurement_id"></select>
                            </td>
                            <td style="text-align: center;">
                                <input data-bind="value: Quantity_Per_Measurement, numeric: true, uniqueName: true, jqValidationElement: true" style="width: 25%;" maxlength="9"  />
                            </td>
                            <td>
                                <a href style="vertical-align: super;" data-bind="click: $root.showMakeModelModal">Add Make/Model</a>

							    <a href class="removeRow" data-bind="visible: !('Tool_Type_id' in $data), click: $root.removeToolType">
								    <img border="0" src="~/Content/Images/Icons/delete.png" alt="Remove" runat="server" />
							    </a>
                            </td>
                        </tr>
                    </tbody>
		        </table>
            </form>
        </div>

        <!-- Make/Model Modal -->
        <div id="makeModelModal" class="modal" data-bind="if: show.modal">
            <form action="#" id="makeModelForm" data-bind="jqValidationForm: modalValidationContext">
                <div class="container-fluid">
                    <!-- ko foreach: makes -->
                    <div class="row padded-bottom">
                        <div class="col-md-1">Active</div>
                        <div class="col-md-2">Make</div>
                        <div class="col-md-4">Model</div>
                        <div class="col-md-2">&nbsp;</div>
                    
                        <div class="col-md-1">
                            <input type="checkbox" data-bind="checked: is_Active, uniqueName: true" />
                        </div>
                        <div class="col-md-2">
                            <input data-bind="value: Make_Name, uniqueName: true, jqValidationElement: true" style="width: 100%;" />
                        </div>
                        <div class="col-md-4" style="border: 1px solid black; padding-top: 7px;">
                            <!-- ko foreach: Models -->
                            <div class="row">
                                <div class="col-md-5">
                                    <input data-bind="value: Model_Name, uniqueName: true, jqValidationElement: true" style="width: 100%;" />
                                </div>
                                <div class="col-md-3">
                                    <a href class="iconLink" data-bind="click: $root.addMakeModel.bind($parent)">
                                        <img border="0" src="~/Content/Images/Icons/add.png" alt="Add Model" runat="server" />
                                    </a>

					                <a href class="removeRow" data-bind="visible: $parent.Models().length > 1, click: $root.removeMakeModel.bind($parent)">
						                <img border="0" src="~/Content/Images/Icons/delete.png" alt="Remove Model" runat="server" />
					                </a>
                                </div>
                            </div>
                            <!-- /ko -->
                        </div>

                        <div class="col-md-2">
                            <a href class="iconLink" data-bind="click: $root.addMake">
                                <img border="0" src="~/Content/Images/Icons/add.png" alt="Add Make" runat="server" />
                            </a>

					        <a href class="removeRow" data-bind="visible: $root.makes().length > 1, click: $root.removeMake">
						        <img border="0" src="~/Content/Images/Icons/delete.png" alt="Remove Make" runat="server" />
					        </a>
                        </div>
                    </div>
                    <!-- /ko -->
                </div>
            </form>
	    </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        function ViewModel() {
            var self = this;
            var model = JSON.parse('<%= Model.Json %>');

            // copy model into context
            $.extend(this, ko.mapping.fromJS(model));

            var formValidationChecks = {
                checkToolTypeSubmitEnable: function () {
                    if ($("#toolTypeForm").valid()) {
                        $("#submitButton").removeAttr("disabled").removeClass("ui-state-disabled");
                    } else {
                        $("#submitButton").attr("disabled", true).addClass("ui-state-disabled");
                    }
                },
                checkModalOkEnable: function () {
                    if ($("#makeModelForm").valid()) {
                        $(".ui-button:contains('Ok')").button("enable");
                    } else {
                        $(".ui-button:contains('Ok')").button("disable");
                    }
                }
            };

            var interfaces = {
                model: {
                    Model_Name: null,
                },
                make: {
                    is_Active: true,
                    Make_Name: null,
                    Models: [],
                },
                toolType: {
                    is_Active: true,
                    Tool_Type_Name: null,
                    is_PowerTool: false,
                    Measurement_id: null,
                    Quantity_Per_Measurement: null,
                    Makes: [],
                }
            };

            this.makes = ko.observableArray();
            this.show = {
                modal: ko.observable(false)
            };
            this.showInactive = ko.observable(false);

            this.showInactive.subscribe(function (showInactive) {
                $.get({
                    url: '/ToolsMaintenance/GetToolTypes',
                    data: {
                        showInactive: showInactive,
                        date: new Date().getTime()
                    },
                    success: function (data) {
                        model.toolTypes = data;

                        self.toolTypes(_.map(data, function (d) {
                            _.forEach(d.Makes, function (m) {
                                m.Models = ko.observableArray(m.Models);
                            });

                            d.Makes = ko.observableArray(d.Makes);

                            return _.assign({}, d);
                        }));
                    },
                    error: function (err) {
                        console.log(JSON.stringify(err));
                    }
                });
            });

            this.save = function () {
                if ($("#toolTypeForm").valid()) {
                    $("#submitButton").attr("disabled", true).addClass("ui-state-disabled");

                    var data = ko.toJS(self.toolTypes);
                    var toolTypeId = -1;

                    // filter data for objects that have changed or are new
                    data = data.filter(function (d, index) {
                        if (index > model.toolTypes.length) {
                            return true;
                        }

                        return ko.toJSON(model.toolTypes[index]) !== ko.toJSON(d);
                    });

                    $.post({
                        url: '/ToolsMaintenance/SaveToolTypes',
                        type: 'post',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        data: JSON.stringify(data),
                        success: function (data) {
                            window.location.href = "/GarageSupplies";
                        },
                        error: function (err) {
                            $("#submitButton").removeAttr("disabled").removeClass("ui-state-disabled");
                            console.log(JSON.stringify(err));
                        }
                    });
                }
            };

            this.validationContext = {
                submitEnable: formValidationChecks.checkToolTypeSubmitEnable
            };
            this.modalValidationContext = {
                submitEnable: formValidationChecks.checkModalOkEnable
            };

            this.addToolType = function (checkForm) {
                var toolType = $.extend({}, interfaces.toolType);

                toolType.Makes = ko.observableArray([]);

                self.toolTypes.push(toolType);

                if (checkForm === undefined || checkForm) {
                    formValidationChecks.checkToolTypeSubmitEnable();
                }

                window.scrollTo(0, document.body.scrollHeight);
                $('.tool-type-name:last').focus();
            }

            this.removeToolType = function () {
                if (self.toolTypes().length > 1) {
                    self.toolTypes.remove(this);
                }
                formValidationChecks.checkToolTypeSubmitEnable();
            }

            this.showMakeModelModal = function () {
                var that = this;
                self.makes.removeAll();
                self.show.modal(true);

                if (this.Makes().length === 0) {
                    var make = $.extend({}, interfaces.make);
                    make.Models = ko.observableArray([$.extend({}, interfaces.model)]);
                    self.makes.push(make);
                } else {
                    ko.utils.arrayPushAll(self.makes, this.Makes());
                }

                var title = 'Add Make/Model for ';

                if (typeof(this.Tool_Type_Name) === 'function' && this.Tool_Type_Name() !== null) {
                    title += this.Tool_Type_Name();
                } else if (typeof (this.Tool_Type_Name) === 'string' && this.Tool_Type_Name !== '') {
                    title += this.Tool_Type_Name;
                } else {
                    title += 'New Product';
                }

                var modal = $("#makeModelModal").dialog({
                    autoOpen: true,
                    height: 400,
                    width: 585,
                    modal: true,
                    title: title,
                    open: function (ev, ui) {
                        formValidationChecks.checkModalOkEnable();
                    },
                    close: function (ev, ui) {
                        self.show.modal(false);
                    },
                    buttons: {
                        Ok: function () {
                            if ($("#makeModelForm").valid()) {
                                that.Makes.removeAll();
                                ko.utils.arrayPushAll(that.Makes, self.makes());
                                modal.dialog("close");
                            }
                        }
                    }
                });
            }
            this.addMake = function () {
                var make = $.extend({}, interfaces.make);
                make.Models = ko.observableArray([$.extend({}, interfaces.model)]);

                self.makes.push(make);
                formValidationChecks.checkModalOkEnable();
            }
            this.removeMake = function () {
                if (self.makes().length > 1) {
                    self.makes.remove(this);
                }
                formValidationChecks.checkModalOkEnable();
            }
            this.addMakeModel = function () {
                this.Models.push($.extend({}, interfaces.model));
                formValidationChecks.checkModalOkEnable();
            }
            this.removeMakeModel = function (item) {
                if (this.Models().length > 1) {
                    this.Models.remove(item);
                }
                formValidationChecks.checkModalOkEnable();
            }

            // have at least one tool type in array
            if (this.toolTypes().length === 0) {
                this.addToolType(false);
                setTimeout(function () {
                    formValidationChecks.checkToolTypeSubmitEnable();
                }, 0);
            }
        }

        $(function () {
            ko.applyBindings(new ViewModel(), $('#contentContainer')[0]);
        });
    </script>
</asp:Content>