﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.ToolMaintenanceViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Tool Type Maintenance
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery.validate-1.17.0.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.12.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout-3.4.2.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout.mapping-latest.js")%>" type="text/javascript"></script>    
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/bindinghandlers.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jqValidate-bindinghandlers.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/lodash.3.9.3.min.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/jquery-ui.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="contentContainer" class="container-fluid">
        <form action="#" id="toolMaintenanceForm" data-bind="jqValidationForm: validationContext">
            <!-- HEADER -->
            <div class="row">
                <div class="col-md-7">
                    <h2>Tools, Power Tools and Quota</h2>
                    <p class="fullDottedLine"></p>
                </div>
            </div>

            <!-- ICONS -->
            <div class="row">
                <div class="col-md-12">
                    <a href="" class="iconLink iconLeft" onclick="history.go(-1); return false;">
                        <img border="0" src="~/Content/Images/Icons/Cancel.png" alt="Cancel" runat="server" /><br />Cancel
                    </a>

                    <a href="" id="submitButton" class="iconLink iconLeft" data-bind="click: save">
                        <img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Save" runat="server" /><br />Save
                    </a>
                </div>
            </div>

            <!-- SEARCH -->
            <div class="row spaced padded-top">
                <div class="col-md-12">
                    <div class="pull-left">
                        Location: 
                        <select data-bind="options: districtBouroughUsers,
                            optionsValue: 'userId',
                            optionsText: 'userName',
                            optionsCaption: 'Select Location',
                            value: userId,
                            enable: show.filters.standardTools() || show.filters.powerTools()"></select>
                    </div>

                    <div class="pull-left spaced">
                        Standard Tools: 
                        <select data-bind="options: standardTools,
                            optionsValue: 'Tool_Type_id',
                            optionsText: 'Tool_Type_Name',
                            optionsCaption: 'Select Tool Type',
                            value: standardToolTypeId,
                            enable: show.filters.standardTools"></select>
                    </div>

                    <div class="pull-left spaced">
                        Power Tools: 
                        <select data-bind="options: powerTools,
                            optionsValue: 'Tool_Type_id',
                            optionsText: 'Tool_Type_Name',
                            optionsCaption: 'Select Tool Type',
                            value: powerToolTypeId,
                            enable: show.filters.powerTools"></select>
                    </div>

                    <button type="button" class="pull-left spaced" data-bind="click: search, enable: show.filters.standardTools() || show.filters.powerTools()">Retrieve</button>
                    
                    <div class="pull-left spaced">
                        <label class="pointer"><input type="checkbox" data-bind="checked: showInactive, enable: show.filters.standardTools() || show.filters.powerTools()" />Show Inactive</label>
                    </div>
                </div>
            </div>

            <!-- TABS -->
            <div class="row padded-top">
                <div class="col-md-7">
                    <div id="tabs">
                        <ul>
                            <li><a href="#inventory">Warehouse Inventory Citywide</a></li>
                            <li><a href="#site-quota" data-bind="click: formValidationChecks.checkToolMaintenanceSubmitEnable">Site Quota</a></li>
                            <li><a href="#power-tools" data-bind="click: formValidationChecks.checkToolMaintenanceSubmitEnable">Power Tools</a></li>
                        </ul>

                        <!-- WAREHOUSE INVENTORY -->
                        <div id="inventory">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Product Name</th>
                                        <th>Unit</th>
                                        <th>Unit Qty</th>
                                        <th>Warehouse Inventory</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: standardInventory, striped: standardInventory">
                                    <tr>
                                        <!-- ko with: Tool_User.Tool_Type -->
                                        <td>
                                            <span data-bind="text: Tool_Type_Name"></span>
                                        </td>
                                        <td>
                                            <span data-bind="text: Measurement.Measurement_Name"></span>
                                        </td>
                                        <td style="text-align: center;">
                                            <span data-bind="text: Quantity_Per_Measurement"></span>
                                        </td>
                                        <!-- /ko -->
                                        <td style="text-align: center;">
                                            <span data-bind="text: Current_On_Hand"></span>
                                        </td>
                                        <td>
                                            <a href data-bind="click: $root.showRecieveModal">Recieve</a>
                                        </td>
                                        <td>
                                            <a href data-bind="click: $root.showEmergencySendModal">Emergency Send</a>
                                        </td>
                                        <td>
                                            <a href="" class="iconLink" data-bind="click: $root.getHistory">
                                                <img border="0" src="~/Content/Images/Icons/calendar.png" alt="Get History" runat="server" /><br />
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!-- SITE QUOTA -->
                        <div id="site-quota">
                            <div data-bind="visible: siteQuotas().length === 0">
                                Please select either a location and/or tool type and click "Retrieve" to view data.
                            </div>

                            <table data-bind="visible: siteQuotas().length > 0">
                                <thead>
                                    <tr>
                                        <th>Active</th>
                                        <th>Location</th>
                                        <th>Tool</th>
                                        <th>Unit</th>
                                        <th>Unit Qty</th>
                                        <th>Site Quota</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: siteQuotas, striped: siteQuotas">
                                    <tr>
                                        <td>
                                            <input type="checkbox" data-bind="checked: is_Active, uniqueName: true" />
                                        </td>
                                        <td>
                                            <span data-bind="text: aspnet_Users.UserName"></span>
                                        </td>
                                        <td>
                                            <span data-bind="text: Tool_Type.Tool_Type_Name"></span>
                                        </td>
                                        <td>
                                            <span data-bind="text: Tool_Type.Measurement.Measurement_Name"></span>
                                        </td>
                                        <td style="text-align: center;">
                                            <span data-bind="text: Tool_Type.Quantity_Per_Measurement"></span>
                                        </td>
                                        <td>
                                            <input data-bind="textInput: Site_Quota, numeric: true, uniqueName: true, jqValidationElement: true" style="width: 50px;" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!-- POWER TOOLS -->
                        <div id="power-tools">
                            <div data-bind="visible: !searched()">
                                Please select either a location and/or tool type and click "Retrieve" to view data.
                            </div>

                            <div class="clearfix" data-bind="visible: searched()">
                                <a href class="iconLink pull-left" data-bind="click: addPowerToolInventory">
                                    <img border="0" src="~/Content/Images/Icons/add.png" alt="Associate a power tool" runat="server" />
                                </a>
                                <a href class="pull-left" style="padding-top: 7px;"  data-bind="click: addPowerToolInventory">Associate a power tool</a>
                            </div>

                            <table data-bind="if: powerToolInventory().length > 0">
                                <thead>
                                    <tr>
                                        <th>Active</th>
                                        <th>Product Type</th>
                                        <th>Make</th>
                                        <th>Model #</th>
                                        <th>Serial #</th>
                                        <th>DSNY #</th>
                                        <th>Assigned Location</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: powerToolInventory, striped: powerToolInventory">
                                    <tr>
                                        <td>
                                            <input type="checkbox" data-bind="checked: is_Active, uniqueName: true" />
                                        </td>
                                        <td>
                                            <select class-="power-tool-product-type" data-bind="attr: { name: 'powerTool_product_type' + $index() },
                                                options: $root.powerTools,
                                                optionsValue: 'Tool_Type_id',
                                                optionsText: 'Tool_Type_Name',
                                                value: Tool_Type_id,
                                                optionsCaption: 'Select Product',
                                                jqValidationElement: true"></select>
                                        </td>
                                        <td>
                                            <select data-bind="visible: Makes().length > 0,
                                                options: Makes,
                                                optionsValue: 'Make_id',
                                                optionsText: 'Make_Name',
                                                value: Make_id"></select>
                                        </td>
                                        <td>
                                            <select data-bind="visible: Models().length > 0,
                                                options: Models,
                                                optionsValue: 'Model_id',
                                                optionsText: 'Model_Name',
                                                value: Model_id"></select>
                                        </td>
                                        <td>
                                            <input data-bind="textInput: Serial_Number, uniqueName: true, jqValidationElement: true" style="width: 100px;" />
                                        </td>
                                        <td>
                                            <input data-bind="textInput: DSNY_Number, uniqueName: true, jqValidationElement: true" style="width: 100px;" />
                                        </td>
                                        <td>
                                            <select data-bind="options: $root.districtBouroughUsers,
                                                optionsValue: 'userId',
                                                optionsText: 'userName',
                                                value: Userid"></select>
                                        </td>
                                        <td>
                                            <a href class="removeRow" data-bind="visible: !('Power_Tool_Inventory_Id' in $data) , click: $root.removePowerToolInventory">
                                                <img border="0" src="~/Content/Images/Icons/delete.png" alt="Remove Power Tool" runat="server" />
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!-- Recieve Modal -->
        <div id="recieveModal" class="modal" data-bind="if: show.modals.recieve">
            <form action="#" id="recieveForm" data-bind="jqValidationForm: recieveModalValidationContext">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            Recieved Qty: <input data-bind="textInput: recieve.qty, numeric: true, uniqueName: true, jqValidationElement: true" style="width: 75px;" />
                        </div>
                    </div>
                    <div class="row padded-top">
                        <div class="col-md-12">
                            <div>Comment</div>
                            <textarea data-bind="value: recieve.comment, uniqueName: true, jqValidationElement: true" style="width: 100%; height: 55px;"></textarea>
                        </div>
                    </div>
                </div>
            </form>
	    </div>

        <!-- Emergency Send Modal -->
        <div id="emergencySendModal" class="modal" title="Emergency Send" data-bind="if: show.modals.emergencySend">
            <form action="#" id="emergencySendForm" data-bind="jqValidationForm: emergencySendModalValidationContext">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            Send Location:
                            <select data-bind="attr: { name: 'sendLocation' },
                                options: $root.districtBouroughUsers,
                                optionsValue: 'userId',
                                optionsText: 'userName',
                                value: emergencySend.userId"></select>
                        </div>
                    </div>
                    <div class="row padded">
                        <div class="col-md-12">
                            Send Qty: <input id="emergency-send-qty" data-bind="textInput: emergencySend.qty, numeric: true, uniqueName: true, jqValidationElement: true" style="width: 75px;" />
                            <div>must be less than or equal to warehouse quantity</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div>Comment</div>
                            <textarea data-bind="value: emergencySend.comment, uniqueName: true, jqValidationElement: true" style="width: 100%; height: 55px;"></textarea>
                        </div>
                    </div>
                </div>
            </form>
	    </div>

        <!-- Inventory History Modal -->
        <div id="inventoryHistoryModal" class="modal" data-bind="if: show.modals.history">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <table class="light" data-bind="visible: inventoryHistory().length > 0">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Location</th>
                                    <th>Sent To</th>
                                    <th>Action</th>
                                    <th>Qty</th>
                                    <th>Comment</th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: inventoryHistory">
                                <tr>
                                    <td>
                                        <span data-bind="text: InsertDateTime.toLocaleString()"></span>
                                    </td>
                                    <td>
                                        <span data-bind="text: userName"></span>
                                    </td>
                                    <td>
                                        <span data-bind="text: sentTo"></span>
                                    </td>
                                    <td>
                                        <span data-bind="text: Workflow_Action.Workflow_Action_Name"></span>
                                    </td>
                                    <td>
                                        <span data-bind="text: Qty"></span>
                                    </td>
                                    <td>
                                        <span data-bind="text: Comment"></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <h3 data-bind="visible: inventoryHistory().length == 0">
                            No inventory history
                        </h3>
                    </div>
                </div>
            </div>
	    </div>
    </div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 550px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        var ViewModel = function () {
            var self = this;
            var model = JSON.parse('<%= Model.Json %>');
            console.log(model);
            // copy model into context
            $.extend(this, ko.mapping.fromJS(model));

            var interfaces = {
                powerToolInventory: {
                    is_Active: null,
                    Make_id: -1,
                    Makes: [],
                    Model_id: -1,
                    Models: [],
                    Userid: -1,
                    Serial_Number: null,
                    DSNY_Number: null,
                }
            };

            this.formValidationChecks = {
                checkToolMaintenanceSubmitEnable: function () {
                    if ($("#toolMaintenanceForm").valid()) {
                        $("#submitButton").removeAttr("disabled").removeClass("ui-state-disabled");
                    } else {
                        $("#submitButton").attr("disabled", true).addClass("ui-state-disabled");
                    }
                },
                checkRecieveModalOkEnable: function () {
                    if ($("#recieveForm").valid()) {
                        $(".ui-button:contains('Ok')").button("enable");
                    } else {
                        $(".ui-button:contains('Ok')").button("disable");
                    }
                },
                checkEmergencySendModalOkEnable: function () {
                    if ($("#emergencySendForm").valid()) {
                        $(".ui-button:contains('Ok')").button("enable");
                    } else {
                        $(".ui-button:contains('Ok')").button("disable");
                    }
                }
            };

            this.validationContext = {
                submitEnable: self.formValidationChecks.checkToolMaintenanceSubmitEnable
            };
            this.recieveModalValidationContext = {
                submitEnable: self.formValidationChecks.checkRecieveModalOkEnable
            };
            this.emergencySendModalValidationContext = {
                submitEnable: self.formValidationChecks.checkEmergencySendModalOkEnable
            };

            // search variables
            this.userId = ko.observable();
            this.standardToolTypeId = ko.observable();
            this.powerToolTypeId = ko.observable();
            this.showInactive = ko.observable(false);
            this.selected = {
                userId: ko.observable(),
                standardToolTypeId: ko.observable(),
                powerToolTypeId: ko.observable()
            };
            this.searched = ko.observable(false);

            this.powerTools = ko.observableArray(_.filter(model.toolTypes, function (t) {
                return t.is_PowerTool;
            }));

            this.standardTools = ko.observableArray(_.filter(model.toolTypes, function (t) {
                return !t.is_PowerTool;
            }));

            // data holders
            this.siteQuotas = ko.observableArray();
            this.powerToolInventory = ko.observableArray();
            this.warehouseRecieved = ko.observableArray([]);
            this.warehouseSent = ko.observableArray([]);
            this.inventoryHistory = ko.observableArray();

            this.districtBouroughUsers = ko.computed(function () {
                return _.filter(this.users(), function (u) {
                    return u.roles().indexOf('GS District') || u.roles().indexOf('GS Bourough');
                })
            }, this);

            this.recieve = {
                qty: ko.observable(),
                comment: ko.observable()
            };
            this.emergencySend = {
                userId: ko.observable(),
                qty: ko.observable(),
                comment: ko.observable()
            };
         
            this.show = {
                modals: {
                    recieve: ko.observable(false),
                    emergencySend: ko.observable(false),
                    history: ko.observable(false)
                },
                filters: {
                    standardTools: ko.observable(false),
                    powerTools: ko.observable(false)
                }
            };

            this.search = function () {
                // copy userid and toolTypeId
                if (self.userId() || self.standardToolTypeId() || self.powerToolTypeId()) {
                    self.selected.userId(self.userId());
                    self.selected.standardToolTypeId(self.standardToolTypeId());
                    self.selected.powerToolTypeId(self.powerToolTypeId());

                    $.post({
                        url: '/ToolsMaintenance/GetLocationTool',
                        data: {
                            userId: self.selected.userId(),
                            powerToolTypeId: self.selected.powerToolTypeId(),
                            standardToolTypeId: self.selected.standardToolTypeId(),
                            showInactive: self.showInactive(),
                            date: new Date().getTime()
                        },
                        dataType: 'json',
                        traditional: true,
                        success: function (data) {
                            self.siteQuotas.removeAll();
                            self.powerToolInventory.removeAll();

                            if (data) {
                                if (data.powerToolInventory && data.powerToolInventory.length > 0) {
                                    _.forEach(data.powerToolInventory, function (pt) {
                                        pt.Makes = ko.observableArray(self.getMakes(pt.Tool_Type_id));

                                        if (pt.Make_id) {
                                            pt.Models = ko.observableArray(self.getModels(pt.Tool_Type_id, pt.Make_id));
                                        } else {
                                            pt.Models = ko.observableArray();
                                        }
                                    });

                                    self.powerToolInventory(data.powerToolInventory);
                                }

                                if (data.siteQuotas && data.siteQuotas.length > 0) {
                                    self.siteQuotas(data.siteQuotas);
                                }
                            }

                            self.formValidationChecks.checkToolMaintenanceSubmitEnable();
                            self.searched(true);
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                }
            }

            this.getHistory = function () {
                $.get({
                    url: '/ToolsMaintenance/GetInventoryHistory',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: {
                        standardToolsInventoryId: this.Standard_Tools_Inventory_id,
                    },
                    success: function (data) {
                        self.show.modals.history(true);

                        self.inventoryHistory(_.forEach(data, function (d) {
                            d.userName = _.find(self.users(), function (u) {
                                return u.userId() == d.InsertUserid;
                            }).userName();

                            if (d.InsertDateTime.indexOf('Date') > -1) {
                                d.InsertDateTime = new Date(parseInt(d.InsertDateTime.replace("/Date(", "").replace(")/", ""), 10));
                            } else {
                                d.InsertDateTime = new Date(d.InsertDateTime);
                            }

                            if (d.Action_Id) {
                                d.Workflow_Action = _.find(self.workflowActions(), function (w) {
                                    return w.Workflow_Action_id() === d.Action_Id;
                                });

                                if (d.Workflow_Action && d.Workflow_Action.Workflow_cd() === 'EMSND') {
                                    d.sentTo = _.find(self.users(), function (u) {
                                        return u.userId() == d.Send_Userid;
                                    }).userName();
                                } else {
                                    d.sentTo = 'Warehouse';
                                }
                            }
                        }));

                        var modal = $("#inventoryHistoryModal").dialog({
                            autoOpen: true,
                            height: 350,
                            width: 600,
                            title: 'Inventory History',
                            modal: true,
                            close: function (ev, ui) {
                                self.show.modals.history(false);
                            },
                            buttons: {
                                Ok: function () {
                                    modal.dialog("close");
                                }
                            }
                        });
                    },
                    error: function (err) {
                        console.log(JSON.stringify(err));
                    }
                });
            }

            this.save = function () {
                if ($("#toolMaintenanceForm").valid()) {
                    $("#submitButton").attr("disabled", true).addClass("ui-state-disabled");

                    var data = {
                        siteQuotas: _.map(ko.toJS(self.siteQuotas), function(sq) {
                            delete sq.Tool_Type;
                            delete sq.aspnet_Users;
                            return sq;
                        }),
                        powerToolInventory: _.map(_.filter(ko.toJS(self.powerToolInventory), function (p) { return p.Tool_Type_id }), function(p) {
                            delete p.aspnet_Users;
                            return p;
                        }),
                        warehouseRecieved: ko.toJS(self.warehouseRecieved),
                        warehouseSent: ko.toJS(self.warehouseSent)
                    };

                    $.post({
                        url: '/ToolsMaintenance/SaveMaintain',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        data: JSON.stringify(data),
                        success: function (data) {
                            self.search();
                        },
                        error: function (err) {
                            $("#submitButton").removeAttr("disabled").removeClass("ui-state-disabled");
                            console.log(JSON.stringify(err));
                        }
                    });
                }
            }

            this.showRecieveModal = function () {
                var that = this;

                self.show.modals.recieve(true);
                self.recieve.qty(null);
                self.recieve.comment(null);

                var modal = $("#recieveModal").dialog({
                    autoOpen: true,
                    height: 225,
                    width: 350,
                    modal: true,
                    title: 'Recieve ' + this.Tool_User.Tool_Type.Tool_Type_Name(),
                    open: function (ev, ui) {
                        self.formValidationChecks.checkRecieveModalOkEnable();
                    },
                    close: function (ev, ui) {
                        self.show.modals.recieve(false);
                    },
                    buttons: {
                        Ok: function () {
                            if ($("#recieveForm").valid()) {
                                self.warehouseRecieved.push({
                                    Standard_Tools_Inventory_id: that.Standard_Tools_Inventory_id(),
                                    qty: parseInt(self.recieve.qty()),
                                    comment: self.recieve.comment()
                                });

                                that.Current_On_Hand(that.Current_On_Hand() + parseInt(self.recieve.qty()));

                                modal.dialog("close");
                            }
                        },
                        Cancel: function() {
                            modal.dialog("close");
                        }
                    }
                });
            };

            this.showEmergencySendModal = function () {
                var that = this;

                self.show.modals.emergencySend(true);
                self.emergencySend.userId(null);
                self.emergencySend.qty(null);
                self.emergencySend.comment(null);

                var modal = $("#emergencySendModal").dialog({
                    autoOpen: true,
                    height: 265,
                    width: 320,
                    modal: true,
                    open: function (ev, ui) {
                        $("#emergency-send-qty").rules('add', { digits: true, min: 1, max: that.Current_On_Hand(), messages: { digits: '', min: '', max: '' } });
                        self.formValidationChecks.checkEmergencySendModalOkEnable();
                    },
                    close: function (ev, ui) {
                        self.show.modals.emergencySend(false);
                    },
                    buttons: {
                        Ok: function () {
                            if ($("#emergencySendForm").valid()) {
                                self.warehouseSent.push({
                                    Standard_Tools_Inventory_id: that.Standard_Tools_Inventory_id(),
                                    qty: parseInt(self.emergencySend.qty()),
                                    comment: self.emergencySend.comment(),
                                    sentTo: self.emergencySend.userId()
                                });

                                that.Current_On_Hand(that.Current_On_Hand() - parseInt(self.emergencySend.qty()));

                                modal.dialog("close");
                            }
                        },
                        Cancel: function () {
                            modal.dialog("close");
                        }
                    }
                });
            };

            this.addPowerToolInventory = function () {
                var powerToolInventory = $.extend({}, interfaces.powerToolInventory);

                powerToolInventory.is_Active = ko.observable(true);
                powerToolInventory.Userid = ko.observable(self.selected.userId());
                powerToolInventory.Tool_Type_id = ko.observable();
                powerToolInventory.Make_id = ko.observable();
                powerToolInventory.Makes = ko.observableArray();
                powerToolInventory.Models = ko.observableArray();

                powerToolInventory.Tool_Type_id.subscribe(function (toolTypeId) {
                    powerToolInventory.Makes(self.getMakes(toolTypeId));
                });

                powerToolInventory.Make_id.subscribe(function (makeId) {
                    powerToolInventory.Models(self.getModels(powerToolInventory.Tool_Type_id(), makeId));
                });

                self.powerToolInventory.push(powerToolInventory);
                self.formValidationChecks.checkToolMaintenanceSubmitEnable();

                window.scrollTo(0, document.body.scrollHeight);
                $('.power-tool-product-type:last').focus();
                
            };

            this.removePowerToolInventory = function () {
                if (!('Power_Tool_Inventory_Id' in this)) {
                    self.powerToolInventory.remove(this);
                }

                self.formValidationChecks.checkToolMaintenanceSubmitEnable();
            }

            this.getMakes = function (toolTypeId) {
                var result = _.find(model.toolTypes, function (tt) {
                    return tt.Tool_Type_id === toolTypeId;
                });

                return result ? result.Makes : [];
            };

            this.getModels = function (toolTypeId, makeId) {
                var result = _.find(model.toolTypes, function (tt) {
                    return tt.Tool_Type_id === toolTypeId;
                });

                if (result && result.Makes && result.Makes.length > 0) {
                    var make = _.find(result.Makes, function (m) {
                        return m.Make_id === makeId;
                    });

                    if (make && make.Models.length > 0) {
                        return make.Models;
                    }
                }

                return [];
            }
        }

        $(function () {
            var koModel = new ViewModel();

            ko.applyBindings(koModel, $('#contentContainer')[0]);

            $('#tabs').tabs({
                activate: function (event, ui) {
                    var index = ui.newTab.index();
                    if (index === 1) {
                        koModel.show.filters.standardTools(true);
                        koModel.show.filters.powerTools(false);
                    } else if (index === 2) {
                        koModel.show.filters.standardTools(false);
                        koModel.show.filters.powerTools(true);
                    }
                }
            });
        });
    </script>
</asp:Content>