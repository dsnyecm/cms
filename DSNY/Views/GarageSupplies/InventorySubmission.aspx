﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.InventorySubmissionViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Tools Inventory and Needs Request
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery.validate-1.17.0.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.12.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout-3.4.2.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout.mapping-latest.js")%>" type="text/javascript"></script>    
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/bindinghandlers.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jqValidate-bindinghandlers.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/lodash.3.9.3.min.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/jquery-ui.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="contentContainer" class="container-fluid">
        <form action="#" id="inventorySubmission" data-bind="jqValidationForm: validationContext">
            <!-- HEADER -->
            <div class="row">
                <div class="col-md-7" style="min-width: 1095px;">
                    <h2>Tools Inventory and Needs Request</h2>
                    <p class="fullDottedLine"></p>
                </div>
            </div>

            <!-- ICONS -->
            <div class="row">
                <div class="col-md-12">
                    <a href="" class="iconLink iconLeft" onclick="history.go(-1); return false;">
                        <img border="0" src="~/Content/Images/Icons/Cancel.png" alt="Cancel" runat="server" /><br />Cancel
                    </a>

                    <a href="" id="submitButton" class="iconLink iconLeft ui-state-disabled" data-bind="click: save" disabled="disabled">
                        <img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Submit" runat="server" /><br />Submit
                    </a>

                    <div id="submitting" style="display: none; padding-top: 15px;">Submitting</div>
                </div>
            </div>

            <!-- SEARCH -->
            <div class="row spaced padded-top" data-bind="visible: isBorough() || isHQ()">
                <div class="col-md-12">
                    Location: 
                    <select class="ignore"
                        data-bind="options: boroughUsers,
                        optionsValue: 'userId',
                        optionsText: 'userName',
                        value: userId"></select>

                    <button type="button" class="spaced ignore" data-bind="click: search">Retrieve</button>
                </div>
            </div>

            <!-- TABS -->
            <div class="row padded-top">
                <div class="col-md-7" style="min-width: 1095px;">
                     <div data-bind="visible: isDistrict() && standardWorkflows().length === 0 && powertoolWorkflows().length === 0" style="padding-top: 20px;">
                         <strong>There are no open inventory requests</strong>
                     </div>

                    <div id="tabs" data-bind="visible: (isDistrict() && (standardWorkflows().length > 0 || powertoolWorkflows().length > 0)) || isWarehouse() || isBorough() || isHQ()">
                        <ul>
                            <li data-bind="visible: isWarehouse()"><a href="#inventory">Warehouse Inventory Approval</a></li>
                            <li data-bind="visible: !isWarehouse()"><a href="#standard">Standard</a></li>
                            <li data-bind="visible: !isWarehouse()"><a href="#power-tools">Power Tools</a></li>
                        </ul>

                        <!-- WAREHOUSE INVENTORY APPROVAL-->
                        <div id="inventory">
                            <div data-bind="visible: warehouseWorkflows().length === 0">There are no warehouse inventory approval workflow items.</div>
                            <table data-bind="visible: warehouseWorkflows().length > 0">
                                <thead>
                                    <tr>
                                        <th>Location</th>
                                        <th>Product Type</th>
                                        <th>Requested</th>
                                        <th>Approved Qty</th>
                                        <th>Comment</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: warehouseWorkflows, striped: warehouseWorkflows">
                                    <tr>
                                        <td>
                                            <span data-bind="text: record.user.userName"></span>
                                        </td>
                                        <td>
                                            <span data-bind="text: record.Tool_Type.Tool_Type_Name"></span>
                                        </td>

                                        <!-- ko with: record -->
                                        <td style="text-align: center;">
                                            <span data-bind="text: Requested_Qty"></span>
                                        </td>
                                        <td style="text-align: center;">
                                            <input data-bind="textInput: Approved_Qty, numeric: true" style="width: 40px;" />
                                        </td>
                                        <td>
                                            <input data-bind="textInput: comment" style="width: 200px;" />
                                        </td>
                                        <!-- /ko -->
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!-- STANDARD-->
                        <div id="standard">
                            <div data-bind="visible: standardWorkflows().length === 0">There are currently no inventory tasks in your work queue</div>
                            <table data-bind="visible: standardWorkflows().length > 0">
                                <thead>
                                    <tr>
                                        <th>Action</th>
                                        <th>Product</th>
                                        <th>Worn/<br />Damaged</th>
                                        <th>Lost/<br />Stolen</th>
                                        <th>On Hand</th>
                                        <th style="text-align: center;">Current<br />On Hand</th>
                                        <th>Measurement</th>
                                        <th>Quota</th>
                                        <th>Issue</th>
                                        <th style="text-align: center;">Boro<br />Approved</th>
                                        <th style="text-align: center;">Boro<br />Shipped</th>
                                        <th>Location</th>
                                        <th style="width: 105px;">Comment</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: standardWorkflows, striped: standardWorkflows">
                                    <tr>
                                        <td>
                                            <select data-bind="options: nextActions,
                                                optionsValue: 'Workflow_Action_id',
                                                optionsText: 'Workflow_Action_Name',
                                                optionsCaption: 'Select Action',
                                                value: nextActionId,
                                                uniqueName: true,
                                                jqValidationElement: true,
                                                jqValidationTrigger: nextActionId, jqValidationTriggerDisableValues: [$parent.cancelActionId, $parent.rejectActionId],
                                                attr: { disabled: nextActions.length == 0 || (nextActions.length == 1 && nextActions[0].Workflow_cd() == 'CNCL') }"></select>
                                        </td>
                                        <td>
                                            <span data-bind="text: tool.Tool_User.Tool_Type.Tool_Type_Name"></span>
                                        </td>

                                        <!-- ko with: record -->
                                        <td>
                                            <input data-bind="textInput: Damaged_Qty,
                                                attr: { disabled: $parent.Workflow_Action.Workflow_cd() !== 'IR' },
                                                numeric: true, uniqueName: true, jqValidationElement: false" style="width: 40px;" />
                                        </td>
                                        <td>
                                            <input data-bind="textInput: Lost_Qty,
                                                attr: { disabled: $parent.Workflow_Action.Workflow_cd() !== 'IR' },
                                                numeric: true, uniqueName: true, jqValidationElement: false" style="width: 40px;" />
                                        </td>
                                        <td>
                                            <input data-bind="textInput: On_Hand_Qty,
                                                attr: { disabled: $parent.Workflow_Action.Workflow_cd() !== 'IR' },
                                                numeric: true, uniqueName: true, jqValidationElement: false" style="width: 40px;" />
                                        </td>
                                        <!-- /ko -->

                                        <td style="text-align: center;">
                                            <span data-bind="text: tool.Current_On_Hand"></span>
                                        </td>
                                        <td>
                                            <span data-bind="text: tool.Tool_User.Tool_Type.Measurement.Measurement_Name"></span>
                                            (<span data-bind="text: tool.Tool_User.Tool_Type.Quantity_Per_Measurement"></span>)
                                        </td>
                                        <td style="text-align: center;">
                                            <span data-bind="text: tool.Tool_User.Site_Quota"></span>
                                        </td>

                                        <!-- ko with: record -->
                                        <td>
                                            <input data-bind="textInput: Issue_Qty,
                                                attr: { disabled: $parent.Workflow_Action.Workflow_cd() !== 'IR' },
                                                numeric: true, uniqueName: true, jqValidationElement: false" style="width: 40px;" />
                                        </td>
                                        <td style="text-align: center;">
                                            <input data-bind="textInput: Boro_Approved_Qty,
                                                attr: { disabled: $parent.Workflow_Action.Workflow_cd() !== 'STB' },
                                                numeric: true, uniqueName: true, jqValidationElement: false" style="width: 40px;" />
                                        </td>
                                        <td style="text-align: center;">
                                            <input data-bind="textInput: Boro_Shipped_Qty,
                                                attr: { disabled: $parent.Workflow_Action.Workflow_cd() != 'ABO' },
                                                numeric: true, uniqueName: true" style="width: 40px;" />
                                        </td>
                                        <!-- /ko -->

                                        <td>
                                            <span data-bind="text: tool.Tool_User.aspnet_Users.UserName"></span>
                                        </td>

                                        <td>
                                            <input data-bind="textInput: record.comment, uniqueName: true" style="width: 100px;" />
                                        </td>

                                        <td>
                                            <a href="" class="iconLink" data-bind="click: $root.getHistory">
                                                <img border="0" src="~/Content/Images/Icons/calendar.png" alt="Get History" runat="server" /><br />
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!-- POWER TOOLS-->
                        <div id="power-tools">
                            <div data-bind="visible: powertoolWorkflows().length === 0">There are no power tool workflow items.</div>
                            <table data-bind="visible: powertoolWorkflows().length > 0">
                                <thead>
                                    <tr>
                                        <th>Active?</th>
                                        <th>Location</th>
                                        <th>Product Type</th>
                                        <th>Serial #</th>
                                        <th>Make</th>
                                        <th>Make #</th>
                                        <th>DSNY #</th>
                                        <th>Down?</th>
                                        <th>Stolen?</th>
                                        <th>Dispose?</th>
                                        <th style="width: 205px;">Comment</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody data-bind="foreach: powertoolWorkflows, striped: powertoolWorkflows">
                                    <tr>
                                        <td style="text-align: center;">
                                            <input type="checkbox" data-bind="checked: record.is_Active, uniqueName: true,
                                                jqValidationTrigger: record.is_Active, jqValidationTriggerDisableValues: [true]" />
                                        </td>
                                        <td>
                                            <span data-bind="text: tool.aspnet_Users.UserName"></span>
                                        </td>

                                        <!-- ko with: tool -->
                                        <td>
                                            <span data-bind="text: Tool_Type.Tool_Type_Name"></span>
                                        </td>
                                        <td>
                                            <span data-bind="text: Serial_Number"></span>
                                        </td>
                                        <td>
                                            <span data-bind="text: Make.Make_Name"></span>
                                        </td>
                                        <td>
                                            <span data-bind="text: Model.Model_Name"></span>
                                        </td>
                                        <td>
                                            <span data-bind="text: DSNY_Number"></span>
                                        </td>
                                        <!-- /ko -->

                                        <!-- ko with: record -->
                                        <td style="text-align: center;">
                                            <input type="checkbox" data-bind="checked: is_Down, uniqueName: true,
                                                jqValidationTrigger: is_Down, jqValidationTriggerDisableValues: [false]" />
                                        </td>
                                        <td style="text-align: center;">
                                            <input type="checkbox" data-bind="checked: is_Stolen, uniqueName: true,
                                                jqValidationTrigger: is_Stolen, jqValidationTriggerDisableValues: [false]" />
                                        </td>
                                        <td style="text-align: center;">
                                            <input type="checkbox" data-bind="checked: is_Dispose, uniqueName: true,
                                                jqValidationTrigger: is_Stolen, jqValidationTriggerDisableValues: [false]" />                                            
                                        </td>
                                        <td>
                                            <input data-bind="textInput: comment, uniqueName: true, jqValidationElement: !is_Active()" style="width: 200px;" />
                                        </td>
                                        <!-- /ko -->

                                        <td>
                                            <a href="" class="iconLink" data-bind="click: $root.getHistory">
                                                <img border="0" src="~/Content/Images/Icons/calendar.png" alt="Get History" runat="server" /><br />
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!-- Workflow History Modal -->
        <div id="workflowHistoryModal" class="modal" data-bind="if: show.history">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <table class="light">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Location</th>
                                    <th>Action</th>
                                    <th>Comment</th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: workflowHistory">
                                <tr>
                                    <td>
                                        <span data-bind="text: Workflow_Datetime.toLocaleString()"></span>
                                    </td>
                                    <td>
                                        <span data-bind="text: userName"></span>
                                    </td>
                                    <td>
                                        <span data-bind="text: Workflow_Action.Workflow_Action_Name"></span>
                                    </td>
                                    <td>
                                        <span data-bind="text: Comment"></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
	    </div>
    </div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 550px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        var ViewModel = function () {
            var self = this;
            var model = JSON.parse('<%= Model.Json %>');

            // copy model into context
            $.extend(this, ko.mapping.fromJS(model));
            console.log(model);

            this.formValidationChecks = {
                checkInventorySubmissionSubmitEnable: function () {
                    if ($("#inventorySubmission").valid()) {
                        $("#submitButton").removeAttr("disabled").removeClass("ui-state-disabled");
                    } else {
                        $("#submitButton").attr("disabled", true).addClass("ui-state-disabled");
                    }
                },
            };

            this.validationContext = {
                submitEnable: self.formValidationChecks.checkInventorySubmissionSubmitEnable
            };

            // ui variables
            this.userId = ko.observable(this.users && this.users.length > 0 ? this.users[0].userId : null);
            this.selected = {
                userId: ko.observable()
            };
            this.show = {
                filter: ko.observable(true),
                history: ko.observable(false)
            };

            this.cancelActionId = _.find(model.workflowActionRoles, function (w) {
                return w.Workflow_Action.Workflow_Action_Name == 'Cancel';
            }).Workflow_Action_id;

            this.rejectActionId = _.find(model.workflowActionRoles, function (w) {
                return w.Workflow_Action.Workflow_Action_Name == 'Rejected';
            }).Workflow_Action_id;

            // data holders
            this.workflowHistory = ko.observableArray();
            this.standardWorkflows = ko.observableArray();
            this.powertoolWorkflows = ko.observableArray();
            this.warehouseWorkflows = ko.observableArray();

            function updateWorkflows() {
                self.standardWorkflows(_.sortByAll(
                    _.map(
                        _.filter(self.workflows(), function (w) {
                            return w.Table_Association() == 'Tool_Inventory_Std';
                    }), function (w) {
                        w.record = _.find(self.standardInventory(), function (i) {
                            return i.Tool_Inventory_Std_Id() == w.Table_Id();
                        });

                        w.record.comment = ko.observable();

                        // hard coded for faster testing
                        //w.record.Damaged_Qty = 12;
                        //w.record.Lost_Qty = 12;
                        //w.record.On_Hand_Qty = 12;
                        //w.record.Issue_Qty = 12;
                        //w.record.Boro_Approved_Qty = 12;
                        //w.record.Boro_Shipped_Qty = 12;

                        w.tool = _.find(ko.toJS(self.tools), function (t) {
                            return t.Tool_User.Tool_User_Id == w.record.Tool_User_Id()
                        });
                        
                        w.nextActions = _.map(
                            _.filter(self.workflowActionFlows(), function (wa) {
                                var actionRole = _.find(model.workflowActionRoles, function (ar) {
                                    return ar.Workflow_Action_id == wa.Next_Workflow_id();
                                });

                                var hasRole = _.find(model.roles, function (r) {
                                    return r.RoleId == actionRole.Role_id;
                                }) !== undefined;;

                                return wa.Workflow_id() == w.Workflow_Action_id() && hasRole;
                            }),
                            function (wa) {
                                return wa.Workflow_Action1;
                            });
                        
                        w.nextActionId = ko.observable();
                        //w.nextActionId = ko.observable(w.nextActions[0].Workflow_Action_id());

                        return w;
                    }), ['tool.Tool_User.aspnet_Users.UserName', 'tool.Tool_User.Tool_Type.Tool_Type_Name']));

                self.powertoolWorkflows(_.sortByAll(
                    _.map(
                        _.filter(self.workflows(), function (w) {
                            return w.Table_Association() == 'Tool_Inventory_PT';
                    }), function (w) {
                        w.record = _.find(self.powertoolInventory(), function (i) {
                            return i.Tool_Inventory_PT_id() == w.Table_Id();
                        });
                        w.record.comment = ko.observable();

                        w.tool = _.find(ko.toJS(self.powertools), function (t) {
                            return t.Power_Tool_Inventory_Id == w.record.Power_Tool_Inventory_id()
                        });

                        w.nextActions = _.map(
                            _.filter(self.workflowActionFlows(), function (wa) {
                                var actionRole = _.find(model.workflowActionRoles, function (ar) {
                                    return ar.Workflow_Action_id == wa.Next_Workflow_id();
                                });

                                var hasRole = _.find(model.roles, function (r) {
                                    return r.RoleId == actionRole.Role_id;
                                }) !== undefined;;

                                return wa.Workflow_id() == w.Workflow_Action_id() && hasRole;
                            }),
                            function (wa) {
                                return wa.Workflow_Action1;
                            });

                        w.user = _.find(self.users(), function (u) {
                            return u.userId() == w.Userid();
                        });

                        w.nextActionId = ko.observable();

                        return w;
                    }), ['tool.Tool_User.aspnet_Users.UserName', 'tool.Tool_User.Tool_Type.Tool_Type_Name']));

                self.warehouseWorkflows(_.map(
                    _.filter(self.workflows(), function (w) {
                        return w.Table_Association() == 'Tool_Inventory_WH';
                    }), function (w) {
                        w.record = _.find(self.warehouseInventory(), function (i) {
                            return i.Tool_Inventory_WH_Id() == w.Table_Id();
                        });
                        w.record.comment = ko.observable();

                        w.record.user = _.find(self.users(), function (u) {
                            return u.userId() == w.record.Userid();
                        });

                        return w;
                    }));
            };

            updateWorkflows();           

            // user variables
            this.boroughUsers = ko.computed(function () {
                return _.filter(this.users(), function (u) {
                    return _.find(self.commandUsers(), function (cuId) {
                        return cuId == u.userId();
                    }) !== undefined;
                })
            }, this);

            this.isDistrict = ko.computed(function () {
                return _.find(this.roles(), function (r) {
                    return r.RoleName() == 'GS District';
                }) !== undefined;
            }, this);

            this.isBorough = ko.computed(function () {
                return _.find(this.roles(), function (r) {
                    return r.RoleName() == 'GS Borough';
                }) !== undefined;
            }, this);

            this.isWarehouse = ko.computed(function () {
                return _.find(this.roles(), function (r) {
                    return r.RoleName() == 'GS Warehouse';
                }) !== undefined;
            }, this);

            this.isHQ = ko.computed(function () {
                return _.find(this.roles(), function (r) {
                    return r.RoleName() == 'GS HQ';
                }) !== undefined;
            }, this);

            this.search = function () {
                // copy userid                
                self.selected.userId(self.userId());

                $.get({
                    url: '/ManageInventory/GetBoroughDistrictWorkflows',
                    data: {
                        userId: self.selected.userId(),
                        all: self.selected.userId() === undefined,
                        date: new Date().getTime()
                    },
                    success: function (data) {
                        if (data.workflows && data.workflows.length > 0) {
                            self.workflows = ko.mapping.fromJS(_.map(data.workflows, function (w) {
                                w.Workflow_Datetime = new Date(parseInt(w.Workflow_Datetime.replace("/Date(", "").replace(")/", ""), 10));
                                return w;
                            }));
                       }

                        if (data.tools && data.tools.length > 0) {
                            self.tools = ko.mapping.fromJS(_.map(data.tools, function (w) {
                                w.Last_Update_Date = new Date(parseInt(w.Last_Update_Date.replace("/Date(", "").replace(")/", ""), 10));
                                return w;
                            }));
                        }

                        if (data.standardInventory && data.standardInventory.length > 0) {
                            self.standardInventory = ko.mapping.fromJS(_.map(data.standardInventory, function (s) { return s; }));
                        }

                        if (data.powertools && data.powertools.length > 0) {
                            self.powertools = ko.mapping.fromJS(_.map(data.powertools, function (p) { return p; }));
                        }

                        if (data.powertoolInventory && data.powertoolInventory.length > 0) {
                            self.powertoolInventory = ko.mapping.fromJS(_.map(data.powertoolInventory, function (p) { return p; }));
                        }

                        updateWorkflows();

                        self.formValidationChecks.checkInventorySubmissionSubmitEnable();
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            }

            this.getHistory = function () {
                $.get({
                    url: '/ManageInventory/GetWorkflowHistory',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: {
                        tableName: this.Table_Association,
                        tableId: this.Table_Id,
                        date: new Date().getTime()
                    },
                    success: function (data) {
                        self.show.history(true);

                        self.workflowHistory(_.forEach(data, function (d) {
                            d.userName = _.find(self.users(), function (u) {
                                return u.userId() == d.Userid;
                            }).userName;

                            if (d.Workflow_Datetime.indexOf('Date') > -1) {
                                d.Workflow_Datetime = new Date(parseInt(d.Workflow_Datetime.replace("/Date(", "").replace(")/", ""), 10));
                            } else {
                                d.Workflow_Datetime = new Date(d.Workflow_Datetime);
                            }
                        }));

                        var modal = $("#workflowHistoryModal").dialog({
                            autoOpen: true,
                            height: 350,
                            width: 600,
                            title: 'Workflow History',
                            modal: true,
                            close: function (ev, ui) {
                                self.show.history(false);
                            },
                            buttons: {
                                Ok: function () {
                                    modal.dialog("close");
                                }
                            }
                        });
                    },
                    error: function (err) {
                        console.log(JSON.stringify(err));
                    }
                });
            }

            this.save = function () {
                if ($("#inventorySubmission").valid()) {
                    $("#submitButton").attr("disabled", true).addClass("ui-state-disabled");

                    $('#submitting').show();
                    animateText('submitting', '.', 3, 750);

                    var workflowActions = [];

                    var standard = _.filter(ko.toJS(self.standardWorkflows()), function (w) {
                        if (w.nextActionId > 0) {
                            workflowActions.push({
                                UserId: w.tool.Tool_User.Userid,
                                Comment: w.record.comment,
                                Workflow_Tracking_id: w.Workflow_Tracking_id,
                                Workflow_Next_Action_id: w.nextActionId,
                            });
                        }

                        return w.nextActionId > 0;
                    });

                    var powertoolComments = [];
                    var powertool = _.forEach(ko.toJS(self.powertoolWorkflows()), function (w) {
                        powertoolComments.push({
                            Power_Tool_Inventory_Id: w.record.Power_Tool_Inventory_id,
                            Userid: self.currentUserId(),
                            Comment: w.record.comment
                        })
                    });

                    var warehouse = _.filter(ko.toJS(self.warehouseWorkflows()), function (w) {
                        return w.record.Approved_Qty != null;
                    });

                    var data = {
                        standardInventory: _.map(standard, function (w) {
                            return w.record;
                        }),
                        powertoolInventory: _.map(powertool, function (w) {
                            return w.record;
                        }),
                        warehouseInventory:  _.map(warehouse, function (w) {
                            return w.record;
                        }),
                        workflowActions: workflowActions,
                        powertoolComments: powertoolComments
                    };

                    console.log(data);

                    $.post({
                        url: '/ManageInventory/SaveInventorySubmission',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        data: JSON.stringify(data),
                        success: function (data) {
                            //window.location.href = "/GarageSupplies";
                            window.location.reload();
                        },
                        error: function (err) {
                            $("#submitButton").removeAttr("disabled").removeClass("ui-state-disabled");
                            console.log(JSON.stringify(err));
                        }
                    });
                }
            }
        }

        $(function () {
            var koModel = new ViewModel();

            ko.applyBindings(koModel, $('#contentContainer')[0]);

            $('#tabs').tabs({
                active: koModel.isWarehouse() ? 0 : 1,
            });
        });
    </script>
</asp:Content>