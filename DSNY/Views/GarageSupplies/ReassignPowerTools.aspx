﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.ToolMaintenanceViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Reassign Power Tool
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery.validate-1.17.0.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout-3.4.2.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout.mapping-latest.js")%>" type="text/javascript"></script>    
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/bindinghandlers.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jqValidate-bindinghandlers.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/lodash.3.9.3.min.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/jquery-ui.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="contentContainer" class="container-fluid">
        <!-- HEADER -->
        <div class="row">
            <div class="col-md-6">
                <h2>Reassign Power Tools</h2>
                <p class="fullDottedLine"></p>
            </div>
        </div>

        <!-- ICONS -->
        <div class="row">
            <div class="col-md-12">
                <a href="" class="iconLink iconLeft" onclick="history.go(-1); return false;">
                    <img border="0" src="~/Content/Images/Icons/Cancel.png" alt="Cancel" runat="server" /><br />Cancel
                </a>

                <a href="" id="submitButton" class="iconLink iconLeft ui-state-disabled" data-bind="click: save" disabled="disabled">
                    <img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Save" runat="server" /><br />Save
                </a>
            </div>
        </div>

        <!-- SEARCH -->
        <div class="row spaced padded-top">
            <div class="col-md-12">
                Location: 
                <select data-bind="options: users,
                    optionsValue: 'userId',
                    optionsText: 'userName',
                    value: userId"></select>

                <button type="button" class="spaced" data-bind="click: search">Retrieve</button>
            </div>
        </div>

        <!-- POWER TOOLS -->
        <div class="row padded-top" style="display: none;" data-bind="visible: show.powerTools">
            <form action="#" id="powerToolsForm" data-bind="jqValidationForm: validationContext">
                <div class="col-md-6" data-bind="visible: powerTools().length === 0">
                    No power tools have been associated with this location
                </div>

                <div class="col-md-6">
                    <table data-bind="visible: powerTools().length > 0">
                        <thead>
                            <tr>
                                <th>Product Type</th>
                                <th>Serial #</th>
                                <th>Make</th>
                                <th>Model #</th>
                                <th>DSNY #</th>
                                <th>Current Location</th>
                                <th>Reassigned Location</th>
                                <th>Comment</th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: powerTools, striped: true">
                            <tr>
                                <td>
                                    <span data-bind="text: Tool_Type.Tool_Type_Name"></span>
                                </td>
                                <td>
                                    <span data-bind="text: Serial_Number"></span>
                                </td>
                                <td>
                                    <span data-bind="text: Make.Make_Name"></span>
                                </td>
                                <td>
                                    <span data-bind="text: Model.Model_Name"></span>
                                </td>
                                <td>
                                    <span data-bind="text: DSNY_Number"></span>
                                </td>
                                <td>
                                    <span data-bind="text: aspnet_Users.UserName"></span>
                                </td>
                                <td>                                    
                                    <select data-bind="options: $root.users,
                                        optionsValue: 'userId',
                                        optionsText: 'userName',
                                        optionsCaption: '--',
                                        value: Userid,
                                        uniqueName: true,
                                        jqValidationTrigger: Userid"></select>
                                </td>
                                <td>
                                    <div>
                                        <input data-bind="value: Comment, uniqueName: true, jqValidationElement: false" style="width: 200px;" />
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 550px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        var ViewModel = function () {
            var self = this;
            var model = JSON.parse('<%= Model.Json %>');

            // copy model into context
            $.extend(this, ko.mapping.fromJS(model));

            this.formValidationChecks = {
                checkPowerToolSubmitEnable: function () {
                    if ($("#powerToolsForm").valid()) {
                        $("#submitButton").removeAttr("disabled").removeClass("ui-state-disabled");
                    } else {
                        $("#submitButton").attr("disabled", true).addClass("ui-state-disabled");
                    }
                },
            };

            this.validationContext = {
                submitEnable: self.formValidationChecks.checkPowerToolSubmitEnable
            };

            // search variables
            this.userId = ko.observable();
            this.selected = {
                userId: ko.observable(),
            };

            // data holders
            this.powerTools = ko.observableArray();
         
            this.show = {
                powerTools: ko.observable(false),
            };

            this.search = function () {
                // copy userid
                self.selected.userId(self.userId());

                $.get({
                    url: '/ManageInventory/GetLocationPowerTools',
                    data: {
                        userId: self.selected.userId(),
                        date: new Date().getTime()
                    },
                    success: function (data) {
                        self.powerTools.removeAll();

                        if (data && data.length > 0) {
                            self.powerTools(_.map(data, function (d) {
                                d.Userid = ko.observable();
                                d.Comment = null;
                                return d;
                            }));
                        }

                        self.show.powerTools(true);
                        self.formValidationChecks.checkPowerToolSubmitEnable();
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            }

            this.save = function () {
                if ($("#powerToolsForm").valid()) {
                    $("#submitButton").attr("disabled", true).addClass("ui-state-disabled");

                    var data = _.map(
                        _.filter(ko.toJS(self.powerTools), function (pt) { return pt.Userid !== null && pt.Comment !== null }),
                        function(d) {
                            return {
                                Comment: d.Comment,
                                Power_Tool_Inventory_Id: d.Power_Tool_Inventory_Id,
                                UserId: d.Userid
                            }
                        });

                    $.post({
                        url: '/ManageInventory/ReassignPowerTools',
                        type: 'post',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        data: JSON.stringify(data),
                        success: function (data) {
                            self.search();
                        },
                        error: function (err) {
                            $("#submitButton").removeAttr("disabled").removeClass("ui-state-disabled");
                            console.log(JSON.stringify(err));
                        }
                    });
                }
            }
        }

        $(function () {
            ko.applyBindings(new ViewModel(), $('#contentContainer')[0]);
        });
    </script>
</asp:Content>