﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.DeliveryViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Street Treatment Deliveries
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
	<script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.12.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/knockout-3.4.2.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/knockout.wrap.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jQuery.Validator.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jQuery-Validator.Knockout.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/bindinghandlers.js?v=0.04")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/Tabs.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/images/jqueryUI/jquery-ui-1.12.1.min.css")%>" rel="stylesheet" type="text/css" />
	<link href="<%=ResolveUrl("~/Content/images/jqueryUI/jquery-ui-1.12.icon-font.min.css")%>" rel="stylesheet" type="text/css" />
	<link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<h2>Street Treatment Deliveries</h2>
	<p  style="width: 915px;" class="dottedLine"></p>

	<div id="contentContainer" style="padding:0;display:none;" data-bind="visible: show">
		<div style="float: left;">
			<div class="iconLeft">
				<a href="" class="iconLink" data-bind="click: cancel">
					<img border="0" src="~/Content/Images/Icons/cancel.png" alt="Close" runat="server" /><br />Close
				</a>
			</div>
			<div class="iconLeft" data-bind="if: Addable">
				<a href="" class="iconLink" data-bind="click: add">
					<div style="margin-top: 2px"><img border="0" src="~/Content/Images/Icons/add.png" alt="Add Line" runat="server" /></div>
					<div style="margin-top: 5px">
						<span>Add Line</span>
					</div>
				</a>
			</div>
			<div class="iconLeft">
				<a href="#" id="submitButton" class="iconLink" data-bind="click: save">
					<img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Save" style="vertical-align: bottom;" runat="server" /><br />Save
				</a>
			</div>
		</div>
		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

		<div id="saved" style="display: none; padding-top: 15px;">Your deliveries have been saved.</div>
		<div id="finalized" style="display: none; padding-top: 15px;">Your delivery has been finalized.</div>

		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

		<div style="float: left; width: 915px">
			<div class="editor-field" style="width: 915px">
				<div style="display: inline-block;">Delivery Start Date: <input type="input" class="RequestDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { name: 'StartDate' }" /></div>
				<div style="display: inline-block;">End Date: <input type="input" class="RequestDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { name: 'EndDate' }" /></div>
				Site:
				<select data-bind="options: zoneSites(null, null, null),
				   optionsValue: 'id',
				   optionsText: 'description',
				   value: selectedSite,
				   optionsCaption: 'All'"></select>
				<input type="checkbox" data-bind="checked: Finalized"/> Show Finalized
				<input type="button" value="Retrieve" data-bind="click: function(){ changeFilter($root.selectedProduct); }" />
			</div>
		</div>

		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

		<div data-bind="if: PurchaseOrders().length == 0">There are no active STC Purchase Orders set up for this product.</div>
		<div data-bind="if: Products().length == 0">There are no active STC Products set up.</div>
		<div data-bind="if: Boroughs().length == 0">There are no Boroughs set up.</div>
		<div data-bind="if: Sites[0]().length == 0">There are no STC Sites set up.</div>

		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

		<ul id="tabnav" data-bind="foreach: Products">
			<li data-bind="css: { selectedTab: $root.selectedProduct.Id() == Id() }"><a href="#" data-bind="text: Name, click: function() { $root.changeFilter($data); }"></a></li>
		</ul>

		<div id="tabsContainer" style="width: 1400px;" data-bind="if: Products().length > 0">
			<div class="tabHeader" data-bind="text: selectedProduct.Name()"></div>
			<div class="tabContent" data-bind="jqValidation: validationContext">
				<div style="margin-top: -30px">
				<div class="col-xs-4"></div>
	<!-- ko if: $root.selectedProduct.Name() == 'Salt' -->					
					<div class="col-xs-2">
						<h3 style="display: inline-block;">Total Weight Master:&nbsp;</h3>
						<span style="font-weight: 700" data-bind="text: $root.totalLeaveWeight()"></span>
					</div>
	<!-- /ko -->
					<div class="col-xs-2">
	<!-- ko if: $root.selectedProduct.Name() == 'Salt' -->	
						<h3 style="display: inline-block;">Total Borough Weight:&nbsp;</h3>
	<!-- /ko -->
	<!-- ko if: $root.selectedProduct.Name() != 'Salt' -->	
						<h3 style="display: inline-block;">Total Qty:&nbsp;</h3>
	<!-- /ko -->
						<span style="font-weight: 700" data-bind="text: $root.totalReceiveWeight()"></span>
					</div>
				</div>
				<table data-bind="foreach: Deliveries" style="width: 1350px;">
					<tbody data-bind="if: $root.selectedProduct.Name() == 'Salt'">
					<tr data-bind="css: { 'alt': $index() % 2 == 1 }">
						<td>
							<div class="nopadding">
								<div class="row">
									<div class="col-xs-7"></div>
									<div class="col-xs-2"><h3>Weight Master</h3></div>
									<div class="col-xs-2"><h3>Borough Supervisor</h3></div>
								</div>
								<div class="row" style="padding-left: 20px;">
									<div class="col-xs-2" style="padding-right: 0">
										<div class="col-xs-12" style="padding-left: 0">
											Purchase Order #
										</div>
										<div class="col-xs-12" style="padding-left: 0">
											<select data-bind="options: PurchaseOrders,
															   optionsValue: 'Id',
															   optionsText: 'PurchaseOrderNumber',
															   value: selectedPurchaseOrder, enable: Edit() && !Finalized() && !$root.Restricted()"></select>
										</div>
									</div>
									<div class="col-xs-1" style="padding-left: 0">
										<div class="col-xs-12" style="padding-left: 0">
											Vendor
										</div>
										<div class="col-xs-12" style="padding-left: 0">
											<span data-bind="text: $root.GetPurchaseOrder(PurchaseOrders, selectedPurchaseOrder).Vendor()"></span>
										</div>
									</div>
									<div class="col-xs-1">
										<div>
											Truck No.
										</div>
										<div>
											<input type="text" class="Small" data-bind="value: TruckNumber, enable: Edit() && !Finalized()" data-required="true"/>
										</div>
									</div>
									<div class="col-xs-1">
										<div>
											Plate No.
										</div>
										<div>
											<input type="text" class="Small" data-bind="value: PlateNumber, enable: Edit() && !Finalized()" data-required="true"/>
										</div>
									</div>
									<div class="col-xs-1">
										<div>
											Destination
										</div>
										<div>
											<select data-bind="options: $root.zoneSites(selectedSite(), selectedSiteName(), $root.GetPurchaseOrder(PurchaseOrders, selectedPurchaseOrder)),
															   optionsValue: 'id',
															   optionsText: 'description',
															   value: selectedSite, enable: Edit() && !Finalized()"></select>
										</div>
									</div>
									<div class="col-xs-1">
										<div>
											Delivery Date
										</div>
										<div>
											<input type="input" class="StartDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { name: 'DeliveryDate', required: true, validate: $root.validateDate }" />
										</div>
									</div>
									<div class="col-xs-1">
										<div>
											Weight
										</div>
										<div>
											<input type="text" class="Small" data-bind="money, value: LeaveWeight, enable: Edit() && !Finalized() && !$root.Restricted()" data-required="true"/>
										</div>
									</div>
									<div class="col-xs-1">
										<div>
											Leave Time
										</div>
										<div>
											<input type="text" class="Small" placeholder="00:00" data-bind="timeFormat: LeaveTime, enable: Edit() && !Finalized() && !$root.Restricted()"/>
										</div>
									</div>
									<div class="col-xs-1">
										<div>
											Weight
										</div>
										<div>
											<input type="text" class="Small" data-bind="money, value: ReceiveWeight, enable: Edit() && !Finalized()" />
										</div>
									</div>
									<div class="col-xs-1">
										<div>
											Received Time
										</div>
										<div>
											<input type="text" class="Small" placeholder="00:00" data-bind="timeFormat: ReceiveTime, value: ReceiveTime, enable: Edit() && !Finalized()"/>
										</div>
									</div>
	<!-- ko if: 1 == 0 -->
									<div class="col-xs-1">
										<a href="#" class="iconLink" data-bind="click: $root.edit">
											<span class="ui-icon ui-icon-pencil" style="font-size: 1.5em; cursor: pointer;"></span>
										</a>
									</div>
	<!-- /ko -->
								</div>
							</div>
							<fieldset style="width: 1200px" class="padding10px">
								<legend>Sign Off</legend>
								<div class="row" style="border: 1px">
									<div class="col-xs-1">
										Weight Master
									</div>
									<div class="col-xs-1">
										<div>
											<input type="text" class="Small" data-bind="value: WeightMasterName, enable: Edit() && !Finalized() && !$root.Restricted()"/>
										</div>
										<div>
											Name
										</div>
									</div>
									<div class="col-xs-1">
										<div>
											<input type="text" class="Small" data-bind="value: WeightMasterBadge, enable: Edit() && !Finalized() && !$root.Restricted()"/>
										</div>
										<div>
											Badge
										</div>
									</div>
									<div class="col-xs-1">
										Receiving Super
									</div>
									<div class="col-xs-1">
										<div>
											<input type="text" class="Small" data-bind="value: ReceiveSuperName, enable: Edit() && !Finalized()"/>
										</div>
										<div>
											Name
										</div>
									</div>
									<div class="col-xs-1">
										<div>
											<input type="text" class="Small" data-bind="value: ReceiveSuperBadge, enable: Edit() && !Finalized()"/>
										</div>
										<div>
											Badge
										</div>
									</div>
									<div class="col-xs-1">
										Boro Super
									</div>
									<div class="col-xs-1">
										<div>
											<input type="text" class="Small" data-bind="value: BoroSuperName, enable: Edit() && !Finalized()"/>
										</div>
										<div>
											Name
										</div>
									</div>
									<div class="col-xs-1">
										<div>
											<input type="text" class="Small" data-bind="value: BoroSuperBadge, enable: Edit() && !Finalized()"/>
										</div>
										<div>
											Badge
										</div>
									</div>
	<!-- ko if: $root.Finalizable() && !Finalized() -->
									<div class="col-xs-1">
										<input type="button" class="Small" data-bind="click: $root.finalize, enable: Edit() && !Finalized()" value="Finalize" />
									</div>
	<!-- /ko -->
								</div>
							</fieldset>

						</td>
					</tr>
					</tbody>
					<tbody data-bind="if: $root.selectedProduct.Name() == 'Sand'">
						<tr data-bind="css: { 'alt': $index() % 2 == 1 }">
							<td>
						
								<div class="row" style="padding-left: 20px;">
									<div style="width: 5%; display: table-cell; " style="padding-right: 0">
										<div>
											Purchase Order #
										</div>
										<div>
											<select data-bind="options: PurchaseOrders,
															   optionsValue: 'Id',
															   optionsText: 'PurchaseOrderNumber',
															   value: selectedPurchaseOrder, enable: Edit() && !Finalized()"></select>
										</div>
									</div>
									<div style="width: 5%; display: table-cell; " style="padding-right: 0">
										<div>
											Vendor
										</div>
										<div>
											<span data-bind="text: $root.GetPurchaseOrder(PurchaseOrders, selectedPurchaseOrder).Vendor()"></span>
										</div>
									</div>
									<div style="width: 5%; display: table-cell; ">
										<div>
											Truck No.
										</div>
										<div>
											<input type="text" class="Small validate-finalize" data-bind="value: TruckNumber, enable: Edit() && !Finalized()" data-required="true" maxlength="50" />
										</div>
									</div>
									<div style="width: 5%; display: table-cell; ">
										<div>
											Plate No.
										</div>
										<div>
											<input type="text" class="Small validate-finalize" data-bind="value: PlateNumber, enable: Edit() && !Finalized()" data-required="true" maxlength="15" />
										</div>
									</div>
									<div style="width: 5%; display: table-cell; ">
										<div>
											Destination
										</div>
										<div>
											<select data-bind="options: $root.zoneSites(selectedSite(), selectedSiteName(), $root.GetPurchaseOrder(PurchaseOrders, selectedPurchaseOrder)),
															   optionsValue: 'id',
															   optionsText: 'description',
															   value: selectedSite, enable: Edit() && !Finalized()"></select>
										</div>
									</div>
									<div style="width: 8%; display: table-cell; ">
										<div>
											Delivery Date
										</div>
										<div>
											<input type="input" class="StartDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { name: 'DeliveryDate', required: true, validate: $root.validateDate }" />
										</div>
									</div>
									<div style="width: 5%; display: table-cell; ">
										<div>
											Time
										</div>
										<div>
											<input type="text" class="Small" placeholder="00:00" data-bind="timeFormat: LeaveTime, enable: Edit() && !Finalized()"/>
										</div>
									</div>
									<div style="width: 5%; display: table-cell; ">
										<div>
											Qty
										</div>
										<div>
											<input type="text" class="Small validate-finalize" data-bind="money, value: ReceiveWeight, uniqueName: true, enable: Edit() && !Finalized()" data-required="true" />
										</div>
									</div>
									<div style="width: 14%; display: table-cell;white-space:nowrap">
										<div>
											Dump Super
										</div>
										<div style="white-space:nowrap">
											<input type="text" class="Small validate-finalize" data-bind="value: ReceiveSuperName, uniqueName: true, enable: Edit() && !Finalized()" maxlength="50"/>
											<input type="text" class="Small validate-finalize " data-bind="value: ReceiveSuperBadge, uniqueName: true, enable: Edit() && !Finalized()" maxlength="20"/>
										</div>
										<div style="display: inline-block; width: 102px;">
											Name
										</div>
										<div style="display: inline-block; width: 102px;">
											Badge
										</div>
									</div>
									<div style="width: 14%; display: table-cell;white-space:nowrap">
										<div>
											Boro Super
										</div>
										<div style="white-space:nowrap">
											<input type="text" class="Small validate-finalize" data-bind="value: BoroSuperName, uniqueName: true, enable: Edit() && !Finalized()" maxlength="50"/>
											<input type="text" class="Small validate-finalize" data-bind="value: BoroSuperBadge, uniqueName: true, enable: Edit() && !Finalized()" maxlength="20"/>
										</div>
										<div style="display: inline-block; width: 102px;">
											Name
										</div>
										<div style="display: inline-block; width: 102px;">
											Badge
										</div>
									</div>
	<!-- ko if: 1 == 0 -->
									<div style="width: 2%; display: table-cell;">
										<div>&nbsp;</div>
										<a href="#" class="iconLink" data-bind="click: $root.edit">
											<span class="ui-icon ui-icon-pencil" style="font-size: 1.5em; cursor: pointer;"></span>
										</a>
									</div>
	<!-- /ko -->
	<!-- ko if: $root.Finalizable() && !Finalized() -->
									<div style="width: 5%; display: table-cell;">
										<div>&nbsp;</div>
										<div>
											<input type="button" class="Small validate-finalize" data-bind="click: $root.finalize, enable: Edit() && !Finalized()" value="Finalize" />
										</div>
									</div>
									<div style="width: 3%; display: table-cell;">
										<div>&nbsp;</div>
									</div>
	<!-- /ko -->
								</div>
							</div>

						</td>
					</tr>
					</tbody>
					<tbody data-bind="if: $root.selectedProduct.Name() == 'Calcium' || $root.selectedProduct.Name() == 'Calcium Chloride'">
						<tr data-bind="css: { 'alt': $index() % 2 == 1 }">
							<td>

								<div class="row" style="padding-left: 20px;">
									<div style="width: 5%; display: table-cell; " style="padding-right: 0">
										<div>
											Ticket #
										</div>
										<div>
											<input type="text" class="Small" data-bind="value: TicketNumber, enable: Edit() && !Finalized()" data-required="true" maxlength="50"/>
										</div>
									</div>
									<div style="width: 5%; display: table-cell; " style="padding-right: 0">
										<div>
											Purchase Order #
										</div>
										<div>
											<select data-bind="options: PurchaseOrders,
															   optionsValue: 'Id',
															   optionsText: 'PurchaseOrderNumber',
															   value: selectedPurchaseOrder, enable: Edit() && !Finalized()"></select>
										</div>
									</div>
									<div style="width: 5%; display: table-cell; " style="padding-right: 0">
										<div>
											Vendor
										</div>
										<div>
											<span data-bind="text: $root.GetPurchaseOrder(PurchaseOrders, selectedPurchaseOrder).Vendor()"></span>
										</div>
									</div>
									<div style="width: 5%; display: table-cell; ">
										<div>
											Destination
										</div>
										<div>
											<select data-bind="options: $root.zoneSites(selectedSite(), selectedSiteName(), $root.GetPurchaseOrder(PurchaseOrders, selectedPurchaseOrder)),
															   optionsValue: 'id',
															   optionsText: 'description',
															   value: selectedSite, enable: Edit() && !Finalized()"></select>
										</div>
									</div>
									<div style="width: 7%; display: table-cell; ">
										<div>
											Delivery Date
										</div>
										<div>
											<input type="input" class="StartDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { name: 'DeliveryDate', required: true, validate: $root.validateDate }" />
										</div>
									</div>
									<div style="width: 5%; display: table-cell; ">
										<div>
											Qty
										</div>
										<div>
											<input type="text" class="Small" data-bind="money, value: ReceiveWeight, enable: Edit() && !Finalized()" data-required="true" />
										</div>
									</div>
									<div style="width: 10%; display: table-cell;">
										<div>
											Dump Super
										</div>
										<div>
											<input type="text" class="Small" data-bind="value: ReceiveSuperName, enable: Edit() && !Finalized()" maxlength="50"/>
											<input type="text" class="Small" data-bind="value: ReceiveSuperBadge, enable: Edit() && !Finalized()" maxlength="20"/>
										</div>
										<div style="display: inline-block; width: 102px;">
											Name
										</div>
										<div style="display: inline-block; width: 102px;">
											Badge
										</div>
									</div>
									<div style="width: 10%; display: table-cell;">
										<div>
											Boro Super
										</div>
										<div>
											<input type="text" class="Small" data-bind="value: BoroSuperName, enable: Edit() && !Finalized()" maxlength="50"/>
											<input type="text" class="Small" data-bind="value: BoroSuperBadge, enable: Edit() && !Finalized()" maxlength="20"/>
										</div>
										<div style="display: inline-block; width: 102px;">
											Name
										</div>
										<div style="display: inline-block; width: 102px;">
											Badge
										</div>
									</div>
	<!-- ko if: 1 == 0 -->
									<div style="width: 2%; display: table-cell;">
										<div>&nbsp;</div>
										<a href="#" class="iconLink" data-bind="click: $root.edit">
											<span class="ui-icon ui-icon-pencil" style="font-size: 1.5em; cursor: pointer;"></span>
										</a>
									</div>
	<!-- /ko -->
	<!-- ko if: $root.Finalizable() && !Finalized() -->
									<div style="width: 5%; display: table-cell;">
										<div>&nbsp;</div>
										<div>
											<input type="button" class="Small validate-finalize" data-bind="click: $root.finalize, enable: Edit() && !Finalized()" value="Finalize" />
										</div>
									</div>
	<!-- /ko -->
								</div>
							</div>

						</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PrintArea" runat="server">
	<div id="printArea"></div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
	<script type="text/javascript">
		var obj = JSON.parse('<%= this.Model.Json %>');
		function getParameterByName(name, url) {
			if (!url) url = window.location.href;
			name = name.replace(/[\[\]]/g, "\\$&");
			var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
				results = regex.exec(url);
			if (!results) return null;
			if (!results[2]) return '';
			return decodeURIComponent(results[2].replace(/\+/g, " "));
		}
		function findFirstDiffPos(a, b)
		{
		   var shorterLength = Math.min(a.length, b.length);

		   for (var i = 0; i < shorterLength; i++)
		   {
			   if (a[i] !== b[i]) return i;
		   }

		   if (a.length !== b.length) return shorterLength;

		   return -1;
		}
		obj.cancel = function() {
			var currentViewModelString = JSON.stringify(ko.wrap.toJS(vm.Deliveries));
			if (viewModelString != currentViewModelString){
				if (!confirm("There are unsaved changes.  Are you sure you want to continue?")) return false;
			}
			var num = getParameterByName('p')
			if (!num){ num = 1; }
			history.go(-num); 
			return false;
		}
		obj.add = function () { vm.Deliveries.push(ko.wrap.fromJS(ko.wrap.toJS(vm.BlankDelivery))); }
		obj.edit = function (item) { item.Edit(true); }
		obj.changeProduct = function(product){
			var currentViewModelString = JSON.stringify(ko.wrap.toJS(vm.Deliveries));
			if (viewModelString != currentViewModelString){
				if (!confirm("There are unsaved changes.  Are you sure you want to continue?")) return false;
			}
			var num = getParameterByName('p')
			if (!num){ num = 2; } else { num++; }
			vm.redirect('/Product/' + product.Name() + '?p=' + num);
		}
		obj.getRelativeUrl = function(relativeLocation){
			var location = window.location.toString();
			location = location.substring(0, location.indexOf('Delivery') + 8) + relativeLocation;
			return location;
		}
		obj.redirect = function(relativeLocation){
			window.location = vm.getRelativeUrl(relativeLocation);
		}
		obj.refresh = function(product){
			window.location.reload(false); 
		}
		obj.changeFilter = function(product){
			if (product.Id() != vm.selectedProduct.Id()){				
				var currentViewModelString = JSON.stringify(ko.wrap.toJS(vm.Deliveries));
				if (viewModelString != currentViewModelString){
					if (!confirm("There are unsaved changes.  Are you sure you want to continue?")) return false;
				}
			}
			var url = vm.getRelativeUrl('/Product/' + product.Name());
			//if (typeof product == undefined) { product = vm.selectedProduct; }
			var viewModel = { selectedProduct: ko.toJS(product), StartDate: ko.toJS(vm.StartDate), EndDate: ko.toJS(vm.EndDate), selectedSite: ko.toJS(vm.selectedSite), Finalized: ko.toJS(vm.Finalized), CurrentUserId: ko.toJS(vm.CurrentUserId) };
			$.post({ url: url, 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				data: JSON.stringify(viewModel),
				success: function(data){
					ko.wrap.updateFromJS(vm.Addable, data.Addable);
					var keys = Object.keys(vm.Sites);
					keys.map(function(a){
						ko.wrap.updateFromJS(vm.Sites[a], data.Sites[a]);
					});
					ko.wrap.updateFromJS(vm.selectedProduct.Id, data.selectedProduct.Id);
					ko.wrap.updateFromJS(vm.selectedProduct.Name, data.selectedProduct.Name);
					ko.wrap.updateFromJS(vm.selectedProduct.Measurement, data.selectedProduct.Measurement);
					ko.wrap.updateFromJS(vm.selectedProduct.LoadSize, data.selectedProduct.LoadSize);
					ko.wrap.updateFromJS(vm.BlankDelivery.ProductId, data.BlankDelivery.ProductId);
					ko.wrap.updateFromJS(vm.BlankDelivery.PurchaseOrders, data.BlankDelivery.PurchaseOrders);
					ko.wrap.updateFromJS(vm.PurchaseOrders, data.PurchaseOrders);
					ko.wrap.updateFromJS(vm.Deliveries, data.Deliveries);
					viewModelString = JSON.stringify(ko.wrap.toJS(vm.Deliveries));
				},
				error: function(err){
					console.log(JSON.stringify(err));
				}
			});
		}
		obj.GetPurchaseOrder = function(orders, id){
			return ko.utils.arrayFirst(orders(), function(item) {
				return item.Id() == id();
			});
		}
		obj.toggle = function (item) { if (!item.Active()){ item.Qty(null); item.Capacity(null); this.deletes().push(item); } }
		obj.deletes = [];
		obj.delete = function (item) {
			var self = this;
			if (confirm('Are you sure that you want to delete this delivery?')){
				$.post({ url: vm.getRelativeUrl('/Delete/' + item.Id()), 
					type: 'post', 
					contentType: 'application/json; charset=utf-8',
					success: function(data){
						if (data != "Success"){
							alert(data);
						}
						self.refresh();
					},
					error: function(err){
						console.log(JSON.stringify(err));                    
					}
				})
			}
		}
		obj.save = function () {
			var errorMsg = "";
			var self = this;
			if ($('.invalid-validation-error').length > 0){
				alert('There are invalid fields.  Please address them before saving.');
				return false;
			}
			var validationResult = this.validationContext.Validate();
			if (!validationResult.valid) {
				this.errors(validationResult.messages);
				errorMsg += 'There are required fields missing.\n';
			}
			if (errorMsg != ""){
				alert(errorMsg);
				return false;
			}

			if (ko.utils.arrayFirst(self.Deliveries(), function(o) {
				var po = vm.GetPurchaseOrder(o.PurchaseOrders, o.selectedPurchaseOrder);
				if ( (o.LeaveWeight() > po.Amount()) || (o.ReceiveWeight() > po.Amount()) ){
					if (!confirm("The weight amount entered will exceed the PO total. Do you want to continue?")) { return true; }
				}
			})) { return false; };

			var viewModel = { selectedProduct: ko.toJS(self.selectedProduct), Deliveries: ko.toJS(self.saveDeliveries) };

			$.post({ url: vm.getRelativeUrl('/Save'), 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				//traditional: true,
				data: JSON.stringify(viewModel), 
				success: function(data){
					ko.wrap.updateFromJS(vm.Deliveries, data.Deliveries);
					viewModelString = JSON.stringify(ko.wrap.toJS(vm.Deliveries));
					if (data.ErrorCode == 0){
						$('#saved').show();
						setTimeout(function(){
							$('#saved').hide();
						}, 5000);
					}
					if (data.Message){
						alert(data.Message);
					} else {
						self.changeFilter(vm.selectedProduct);
					}
				}
			}).fail(function(err){
				console.log(JSON.stringify(err));       
			});
		}
		obj.validateDate = function(dt){
			var current = new Date();
			return dt <= current.valueOf();
		}
		obj.finalize = function (d, e) {
			var errorMsg = "";
			var dt = new Date(d.DeliveryDate() + ' ' + d.LeaveTime());
			var dt2 = new Date(d.DeliveryDate() + ' ' + d.ReceiveTime());
			var current = new Date();
			if (dt.valueOf() > current.valueOf()){
				alert('The date and time cannot be in the future.');
				return false;
			}
			if (dt2.valueOf() > current.valueOf()){
				alert('The date and time cannot be in the future.');
				return false;
			}

			if ($('.invalid-validation-error').length > 0){
				alert('There are invalid fields.  Please address them before saving.');
				return false;
			}

			var validationResult = vm.validationContext.Validate();
			if (!validationResult.valid) {
				vm.errors(validationResult.messages);
				errorMsg += 'There are required fields missing.  Please address issues with required fields before finalizing any deliveries.\n';
			}
			if (errorMsg != ""){
				alert(errorMsg);
				return false;
			}
			// var val = true;
			// $(e.target).closest(".row").find(":input").each(function(i, item){
			// 	if($(item).data('required') && !$(item).val()) {
			// 		val = false;
			// 	}
			// })
			// if (!val) {
   //          	alert('There are required fields missing.');
			// 	return false;
			// }
			var self = vm;
			if (!d.ReceiveWeight()){ alert("The weight received by the borough is required before finalizing."); return false; }
			d.Finalized(true);
			$('#finalized').show();
			setTimeout(function(){
				$('#finalized').hide();
			}, 5000);
			return;

			var viewModel = { value: ko.toJS(d) };

			$.post({ url: vm.getRelativeUrl('/Finalize'), 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				//traditional: true,
				data: JSON.stringify(viewModel), 
				success: function(data){
					if (data != "Success"){
						alert(data);
						return false;
					}
					viewModelString = JSON.stringify(ko.wrap.toJS(vm.Deliveries));
					d.Edit(false);
					$('#finalized').show();
					setTimeout(function(){
						$('#finalized').hide();
					}, 5000);
				}
			}).fail(function(err){
				console.log(JSON.stringify(err));       
			});
		}
		obj.show = true;
		var vm = ko.wrap.fromJS(obj);
		vm.saveDeliveries = ko.computed(function () {
			var self = vm;
			return ko.utils.arrayFilter(self.Deliveries(), function (item) {
				return true;
			});
		});
		vm.zoneSites = function(Id, Name, po) {
			return ko.computed(function () {
				var self = vm;
				var exists = false;  
				var zoneId = (po) ? po.ZoneId() : 0;
				var sites = ko.utils.arrayFilter(self.Sites[zoneId](), function (item) {
					if (item.id() == Id){ exists = true; }
					return true;
				});
				if (po && po.Zone2Id()){
					var sites2 = ko.utils.arrayFilter(self.Sites[po.Zone2Id()](), function (item) {
						if (!ko.utils.arrayFirst(sites, function(item2) {
							return item2.id() === item.id();
						})){
							sites.push({id: ko.observable(item.id()), description: ko.observable(item.description()), visible: ko.observable(true)});
						}
						if (item.id() == Id){ exists = true; }
						return true;
					});
				}
				//TO DO: Zone2Id
				if (!exists && Name != null){
					sites.push({id: ko.observable(Id), description: ko.observable(Name), visible: ko.observable(true)});
				}
				return sites;
			})();
		};
		vm.totalLeaveWeight = ko.computed(function () {
			var total = 0;
			ko.utils.arrayFilter(vm.Deliveries(), function (item) {
				total += item.LeaveWeight() ? parseInt(item.LeaveWeight()) : 0;
			});
			return total;
		});
		vm.totalReceiveWeight = ko.computed(function () {
			var total = 0;
			ko.utils.arrayFilter(vm.Deliveries(), function (item) {
				total += item.ReceiveWeight() ? parseInt(item.ReceiveWeight()) : 0;
			});
			return total;
		});

		vm.validationContext = ko.jqValidation({
			returnBool:false, // We want more details of our validation result.
			useInlineErrors: true, // Use inline errors
			errorClass: 'input-validation-error', // Apply error class
			msg_empty: '', // Global empty message.
			noInlineErrors: "*[type='password']" // Password fields should not show inline errors.
		});
	
		vm.errors = ko.observableArray([]);
		var viewModelString;
		$(function () {
			ko.applyBindings(vm, $('#contentContainer')[0]);
			viewModelString = JSON.stringify(ko.wrap.toJS(vm.Deliveries));
		});
	</script>
</asp:Content>