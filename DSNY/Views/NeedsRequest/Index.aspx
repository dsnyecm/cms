﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.NeedsRequestViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Commodity Needs Request
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
	<script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/knockout-3.4.2.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/knockout.mapping-latest.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/knockout.wrap.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery.validate.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jQuery.Validator.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jQuery-Validator.Knockout.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/date.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/bindinghandlers.js?v=0.03")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<h2>Commodity Needs Request</h2>
	<p  style="width: 915px;" class="dottedLine"></p>

	<div id="contentContainer" style="width: 1000px;display: none;" data-bind="visible: show, jqValidation: validationContext">
		<div style="float: left;">
			<div class="iconLeft">
				<a href="" class="iconLink" data-bind="click: cancel">
					<img border="0" src="~/Content/Images/Icons/cancel.png" alt="Close" runat="server" /><br />Close
				</a>
			</div>
				<div class="iconLeft" data-bind="if: Sites().length > 0">
						<a href="" class="iconLink" data-bind="click: add">
							<div style="margin-top: 2px"><img border="0" src="~/Content/Images/Icons/add.png" alt="Add Line" runat="server" /></div>
							<div style="margin-top: 5px">
								<span>Add Line</span>
							</div>
						</a>
				</div>
			<div class="iconLeft">
				<a href="#" id="submitButton" class="iconLink" data-bind="click: save">
					<img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Save" style="vertical-align: bottom;" runat="server" /><br />Save
				</a>
			</div>
			<div id="submitting" style="display: none; padding-top: 15px;">Submitting</div>
		</div>

		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

			<div id="saved" style="display: none; padding-top: 15px;">Your changes have been saved.</div>
			<div id="confirmed" style="display: none; padding-top: 15px;">Your needs request has been confirmed.</div>

			<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

		<div style="float: left;">
			<div class="editor-field" style="width: 700px">
				<div style="display: inline-block;">Request Date: <input type="input" class="RequestDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { name: 'RequestDate' }" /></div>
				Site:
				<select data-bind="options: Sites,
											optionsValue: 'id',
											optionsText: 'description',
											value: SelectedSite,
											optionsCaption: Sites().length > 1 ? 'All' : null"></select>
				<input type="checkbox" data-bind="checked: Finalized"/> Show Confirmed
				<input type="button" value="Retrieve" data-bind="click: changeFilter" />
			</div>
		</div>

		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>
			<div data-bind="if: Sites().length == 0">There are no active STC Products associated to Users in STC Product User.</div>

		<div id="StcPurchaseOrderDetails" style="display: inline-block;  overflow-y: auto;">
			<div data-bind="visible: filterDisplay().length > 0">
				<table style="width: 100%">
					<thead>
					<tr>
						<th>Request Date</th>
						<th>Commodity</th>
						<th>Site</th>
						<th>Qty</th>
						<th>Supervisor</th>
						<th>Badge</th>
						<th></th>
						<th></th>
					</tr>
					</thead>
					<tbody data-bind="foreach: filterDisplay">
            			<tr data-bind="css: { 'alt': $index() % 2 == 1 }">
							<td><input type="input" class="RequestDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { onlyIf: !ConfirmDate(), name: 'RequestDate' }" /></td>
							<td><select data-bind="options: Products,
															optionsValue: 'id',
															optionsText: 'description',
															value: ProductId, event: { change: function(d, e){ $data.Product($root.getSelectedOption(Products, $data.ProductId()).description()); $root.changeQty($data); } }, enable: !ConfirmDate()"></select></td>
							<td><select data-bind="options: Sites,
															optionsValue: 'id',
															optionsText: 'description',
															value: UserId, event: { change: function(){ $data.User($root.getSelectedOption(Sites, $data.UserId()).description()); $root.changeQty($data); $root.changeSite(UserId(), Products); } }, enable: !ConfirmDate()"></select></td>
							<td><input type="text" class="Qty" data-bind="money, value: Qty, enable: !ConfirmDate()"  data-required="true" /></td>
							<td><input type="text" class="Supervisor" data-bind="value: Supervisor, enable: !ConfirmDate()" /></td>
							<td><input type="text" class="Badge" data-bind="value: Badge, enable: !ConfirmDate()"  /></td>
							<td data-bind="ifnot: ConfirmDate"><input type="button" value="Confirm" data-bind="click: $root.confirm" /></td>
							<td><img src="/content/images/icons/delete.png" data-bind="click: function(){ Deleted(true); }" class="tcalIcon" alt="Delete Needs Request" title="Delete Needs Request"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div data-bind="visible: filterDisplay().length === 0">
				There are no needs requests.
			</div>
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PrintArea" runat="server">
	<div id="printArea"></div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
	<script type="text/javascript">
		var obj = JSON.parse('<%= this.Model.Json %>');
		obj.filterSite = undefined;
		obj.filterDate = "";
		obj.filterFinalized = false;
		obj.cancel = function() {
			var currentViewModelString = JSON.stringify(ko.wrap.toJS(vm.NeedsRequests));
			if (viewModelString != currentViewModelString){
				if (!confirm("There are unsaved changes.  Are you sure you want to continue?")) return false;
			}
			history.go(-1); 
			return false;
		}
		obj.changeFilter= function(){
			var currentViewModelString = JSON.stringify(ko.wrap.toJS(vm.NeedsRequests));
			if (viewModelString != currentViewModelString){
				if (!confirm("There are unsaved changes.  Are you sure you want to continue?")) return false;
			}
			vm.refresh();
		}
		obj.confirm = function (d, e) {
			var self = vm;
			var viewModel = { value: ko.toJS(d) };
			var today = new Date();
			var confirmDate = (today.getMonth()+1)+'/'+today.getDate()+'/'+today.getFullYear() + ' ' + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			d.ConfirmDate(confirmDate);
			$('#confirmed').show();
			setTimeout(function(){
				$('#confirmed').hide();
			}, 5000);
			return;

			$.post({ url: window.location.toString() + '/Confirm', 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				//traditional: true,
				data: JSON.stringify(viewModel), 
				success: function(data){
					if (data != "Success"){
						alert(data);
						return false;
					}
					viewModelString = JSON.stringify(ko.wrap.toJS(vm.NeedsRequests));
					$('#confirmed').show();
					setTimeout(function(){
						$('#confirmed').hide();
					}, 5000);
					console.log(JSON.stringify(data));
				}
			}).fail(function(err){
				console.log(JSON.stringify(err));       
			});
		}
		obj.delete = function (item) { item.Deleted(true); }
		obj.add = function () { this.NeedsRequests.push(ko.wrap.fromJS(ko.wrap.toJS(this.BlankNeedsRequest))); }
		obj.changeQty = function(item) { if (item.New()) { var siteOnHand = vm.getSiteOnHand(item.ProductId(), item.UserId()); item.Qty(siteOnHand.Capacity() - siteOnHand.Qty()); } }
		obj.refresh = function(){
			var url = window.location.toString() + '/ChangeFilter/';
			if (typeof product == undefined) { product = vm.selectedProduct; }
			var viewModel = { CurrentUser: ko.toJS(vm.CurrentUser), RequestDate: ko.toJS(vm.RequestDate), SelectedSite: ko.toJS(vm.SelectedSite), Finalized: ko.toJS(vm.Finalized) };
			$.post({ url: url, 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				data: JSON.stringify(viewModel),
				success: function(data){
					ko.wrap.updateFromJS(vm.NeedsRequests, data.NeedsRequests);
					viewModelString = JSON.stringify(ko.wrap.toJS(vm.NeedsRequests));
				},
				error: function(err){
					console.log(JSON.stringify(err));                    
				}
			});
		}
		obj.save = function () {
			var validationResult = this.validationContext.Validate();
			if (!validationResult.valid) {
				this.errors(validationResult.messages);
					return false;
			}

			var self = this;
			var values = ko.toJS(this.filterSave());
			var loc = window.location.toString();
			$.post({ url: loc + '/Save', 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				//traditional: true,
				data: JSON.stringify(values), 
				success: function(data){
					viewModelString = JSON.stringify(ko.wrap.toJS(vm.NeedsRequests));
					$('#saved').show();
					setTimeout(function(){
						$('#saved').hide();
					}, 5000);
					self.refresh();
				},
				error: function(err){
					console.log(JSON.stringify(err));                    
				}
			})
		}
		obj.changeSite = function(id, products){
			$.post({ url: window.location.toString() + '/GetProducts/' + id, 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				success: function(data){
					ko.wrap.updateFromJS(products, data);
				},
				error: function(err){
					console.log(JSON.stringify(err));                    
				}
			})
		}
		obj.show = true;

		var vm = ko.wrap.fromJS(obj);

		vm.rowFilter = function(item){
			var self = vm;
			return (typeof self.filterSite() == 'undefined' || self.filterSite() == item.UserId()) //get  site
			&& (self.filterDate() == "" || (Date.parse(self.filterDate()).getTime() == Date.parse(item.RequestDate()).getTime())) //get  date
			//&& (self.filterFinalized() == item.) //get  finalized
			|| !item.Id();
		}
		vm.filterDisplay = ko.computed(function () {
			var self = vm;
			return ko.utils.arrayFilter(self.NeedsRequests(), function (item) {
				return item.Deleted() == false && self.rowFilter(item);
			});
		});
		vm.filterSave = ko.computed(function () {
			var self = vm;
			return ko.utils.arrayFilter(self.NeedsRequests(), self.rowFilter);
		});
		vm.getSiteOnHand = function (productId, userId) {
			var self = vm;
			return ko.utils.arrayFirst(self.SiteOnHands(), function(item){
				return item.ProductId() == productId && item.UserId() == userId;
			});
		};
		vm.getSelectedOption = function (options, id) {
			return ko.utils.arrayFirst(options(), function(item){
				return item.id() == id;
			});
		};

		vm.validationContext = ko.jqValidation({
			returnBool:false, // We want more details of our validation result.
			useInlineErrors: true, // Use inline errors
            errorClass: 'input-validation-error', // Apply error class
            msg_empty: '', // Global empty message.
			noInlineErrors: "*[type='password']" // Password fields should not show inline errors.
		});
		
		vm.errors = ko.observableArray([]);

		var viewModelString;
		$(function () {
			ko.applyBindings(vm, $('#contentContainer')[0]);
			viewModelString = JSON.stringify(ko.wrap.toJS(vm.NeedsRequests));
		});
	</script>
</asp:Content>