﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<DSNY.Data.Sent_Message>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Administration Dashboard
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery.validate.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.5.custom.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/lodash.3.9.3.min.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssIncludes" runat="server">
    <link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Content/jqueryUI.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Content/print.css")%>" rel="stylesheet" type="text/css" media="print" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="FuelFormAlert" runat="server">
    <% if (ViewData["MissingFuelForms"] != null)
       { %>
    <div id="alertIcon" style="float: left;" alt="Click to view missing fuel forms">
        <img src="~/Content/images/icons/Alert.png" title="Click to view missing fuel forms" alt="Click to view missing fuel forms" runat="server" />
    </div>
    <div id="alert"><%: ViewData["MissingFuelForms"].ToString() %></div>
    <% } %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!--<h2>Administration Dashboard&nbsp;&nbsp;&nbsp;[<a href="#" onclick="helpPopup('AdminDashboard')">?</a>]</h2>-->
    <h2>Administration Dashboard</h2>
    <p style="width: 915px;" class="dottedLine"></p>

    <% using (Html.BeginForm("Search", "Dashboard", FormMethod.Post, new { id = "messageForm" })) {%>

    <div style="width: 900px;">
        <div class="iconLeft">
            <a href="#" id="printButton" style="text-decoration: none; color: #696969;">
                <img border="0" src="~/Content/Images/Icons/printer.png" alt="Print Selected" runat="server" /><br />
                Print Selected
            </a>
        </div>

        <div class="iconLeft" style="margin-left: 10px;">
            <a href="<%: Url.RouteUrl(new { controller = "Message", action = "Create"}) %>" style="text-decoration: none; color: #696969;">
                <img border="0" src="~/Content/Images/Icons/writeEmail.png" alt="Write Email" runat="server" /><br />
                Create Message
            </a>
        </div>

        <div class="iconLeft" style="margin-left: 10px; margin-top: 3px;">
            <a href="<%: Url.RouteUrl(new { controller = "Dashboard", action = "Admin"}) %>" style="text-decoration: none; color: #696969;">
                <img border="0" src="~/Content/Images/Icons/refresh.png" alt="Refresh" runat="server" /><br />
                Refresh
            </a>
        </div>

        <div id="messageCount">
        <%
           int pageSize = DSNY.MvcApplication.pageSize;
           int totalRecords = 0;

           if (TempData["totalRecords"] != null)
           {
               int.TryParse(TempData["totalRecords"].ToString(), out totalRecords);

               if (totalRecords > 0 && totalRecords > pageSize)
               {
                   // attempt to get the current page number
                   int page = 1;

                   if (Request["page"] != null && !string.IsNullOrEmpty(Request["page"])) {
                       int.TryParse(Request["page"], out page);
                   } %>
                        Showing <%: ((page - 1) * pageSize) + 1%>  - <%: (page * pageSize) > totalRecords ? totalRecords : page * pageSize%> out of <%: totalRecords%> messages
        <% } else  { %>
            <%: Model.Count()%> messages returned
        <% }
           } else { %>
            <%: Model.Count() %> messages returned
    <% } %>
        </div>

        <% Html.RenderPartial("Controls/SearchUserControl"); %>
    </div>

    <div class="clear" style="padding-bottom: 15px;"></div>

    <div id="messageGrid">
        <% Html.RenderPartial("Grids/MessageGrid"); %>
    </div>

    <input type="hidden" id="view" name="view" value="admin" />
    <% } %>

    <% if (TempData["TodayDailyReport"] == null) { %>
    <div id="daily-report" class="modal" title="Daily Report">
        <form id="dailyReport">
            <fieldset style="width: 300px;">
                <label for="weather">Weather</label><br />
                <input type="radio" name="weather" value="Clear" />Clear<br />
                <input type="radio" name="weather" value="Rain" />Rain<br />
                <input type="radio" name="weather" value="Snow" />Snow<br />
                <br />

                <label for="weatherFunction">Function</label><br />
                <input type="radio" name="weatherFunction" value="Regular Operations" />Regular Operations<br />
                <input type="radio" name="weatherFunction" value="Exception Operations" />Exception Operations<br />
                <input type="radio" name="weatherFunction" value="No Operations" />No Operations<br />
                <br />

                <label for="tempLow">Temperature (&#8457;)</label><br />
                &nbsp;&nbsp;Low:
                <select name="tempLow">
                    <option></option>
                    <option value="-20 - 0">-20 - 0</option>
                    <option value="1 - 20">1 - 20</option>
                    <option value="21 - 40">21 - 40</option>
                    <option value="41 - 60">41 - 60</option>
                    <option value="61 - 80">61 - 80</option>
                    <option value="81 - 100">81 - 100</option>
                    <option value="101+">101+</option>
                </select>
                <br />

                &nbsp;High:
                <select name="tempHigh">
                    <option></option>
                    <option value="-20 - 0">-20 - 0</option>
                    <option value="1 - 20">1 - 20</option>
                    <option value="21 - 40">21 - 40</option>
                    <option value="41 - 60">41 - 60</option>
                    <option value="61 - 80">61 - 80</option>
                    <option value="81 - 100">81 - 100</option>
                    <option value="101+">101+</option>
                </select>
            </fieldset>
        </form>
    </div>
    <% } %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="PrintArea" runat="server">
    <div id="printArea"></div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 945px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            // stripes the message table
            reStripe("messageTable");

            // if coming from a message view, the window name is changed to denote which message was read
            // then we change the status from Received to Read
            if (window.name.length > 0) {
                var newWindowName = '';
                var msgIds = window.name.split(',');

                for (var i = 0; i < msgIds.length; i++) {
                    if (msgIds[i] != '' && $('td[messageID = "' + msgIds[i] + '"]').length) {
                        $('td[messageID = "' + msgIds[i] + '"]').text("Read");
                        newWindowName += ',' + msgIds[i];
                    }
                }

                window.name = newWindowName;
            }

            <% if (TempData.Keys.Count(k => k == "TodayDailyReport") > 0 && TempData["TodayDailyReport"] == null) { %>
            // validations
            $("#dailyReport").validate({
                errorClass: "hidden-validation-error",
                errorPlacement: function (error, element) {
                    error.hide();
                },
                rules: {
                    weather: { required: true, messages: { required: '' } },
                    weatherFunction: { required: true, messages: { required: '' } },
                    tempLow: { required: true, messages: { required: '' } },
                    tempHigh: { required: true, messages: { required: '' } }
                }
            });

            $('input[name="weather"], input[name="weatherFunction"], select[name="tempLow"], select[name="tempHigh"]').live('change', function () {
                $("#dailyReport").validate();

                if ($("#dailyReport").valid()) {
                    $(".ui-button:contains('Submit')").button("enable");
                } else {
                    $(".ui-button:contains('Submit')").button("disable");
                }
            });

            var dialog = $("#daily-report").dialog({
                autoOpen: true,
                height: 382,
                width: 365,
                modal: true,
                buttons: {
                    "Submit": function () {
                        var reportData = {
                            "weather": $("input[name=weather]:checked").val(),
                            "function": $("input[name=weatherFunction]:checked").val(),
                            "tempLow": $("select[name=tempLow] option:selected").val(),
                            "tempHigh": $("select[name=tempHigh] option:selected").val()
                        };

                        $.post("/DailyReport/AddDailyReport", reportData, function (data) {
                            dialog.dialog("close");
                        }, 'json');
                    },
                    Cancel: function () {
                        dialog.dialog("close");
                    }
                },
                close: function () {
                    document.forms[0].reset();
                }
            });

                    $(".ui-button:contains('Submit')").button("disable");
            <% } %>

            // alert popup
            $("#alertIcon").click(function () {
                if ($(".messagepop").html() == null) {
                    var popUpHTML = '<div class="messagepop"><div style="font-weight: bold;">Missing Fuel Forms from:</div><div><ul>';
                    <% if (ViewData["MissingFuelFormUsers"] != null)
                       {
                           List<DSNY.Core.Interfaces.IUser> users = (List<DSNY.Core.Interfaces.IUser>)ViewData["MissingFuelFormUsers"];
                           foreach (DSNY.Core.Interfaces.IUser user in users) {
                               if (!string.IsNullOrEmpty(user.description)) {%>
                                popUpHTML += '<li><%= Page.User.Identity.Name.ToLower() == user.userName.ToLower() ? "<span style=\"font-weight: bold;\">*</span>" : string.Empty %>' +
                                    '<%: user.description %></li>';
                 <% } else { %>
                                popUpHTML += '<li><%= Page.User.Identity.Name.ToLower() == user.userName.ToLower() ? "<span style=\"font-weight: bold;\">*</span>" : string.Empty %>' +
                                    '<%: user.userName %></li>';
                <% }
                    }
                } %>
                    popUpHTML += '</ul></div></div>';

                    $(this).addClass("selected").parent().append(popUpHTML);
                }

                $(".messagepop").animate({ opacity: 'toggle', height: 'toggle' }, "fast");

                return false;
            });

            // sends an ajax request on a sortable table field title
            $(".sortable").live('click', function () {
                fuelFormValue = $("#isFuelForm").val();
                pageValue = $("#page").val();
                fromDateValue = $("#fromDate").val();
                toDateValue = $("#toDate").val();
                searchTermsValue = $("#searchTerms").val();
                viewValue = '<%: TempData["view"] %>';

                if ($("#sortColumn").val() == $(this).attr("id")) {
                    if ($("#direction").val() == "ASC") {
                        var sortData = {
                            field: $(this).attr("id"), direction: "DESC", fuelForm: fuelFormValue, page: pageValue, fromDate: fromDateValue,
                            toDate: toDateValue, searchTerms: searchTermsValue, view: viewValue
                        };
                    }
                    else {
                        var sortData = {
                            field: $(this).attr("id"), direction: "ASC", fuelForm: fuelFormValue, page: pageValue, fromDate: fromDateValue,
                            toDate: toDateValue, searchTerms: searchTermsValue, view: viewValue
                        };
                    }
                }
                else {
                    var sortData = {
                        field: $(this).attr("id"), direction: "ASC", fuelForm: fuelFormValue, page: pageValue, fromDate: fromDateValue,
                        toDate: toDateValue, searchTerms: searchTermsValue, view: viewValue
                    };
                }

                // ajax call to get the sorted grid based on json sort data
                $.get("/Dashboard/SortedGrid", sortData, function (data) {
                    $("#messageGrid").html(data);
                    reStripe("messageTable");
                }, 'html');
            });
        });
    </script>
</asp:Content>