﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    STC Dashboard
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssIncludes" runat="server">
    <link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="FuelFormAlert" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%="" %>
    <% if (Roles.IsUserInRole("STC Site") || Roles.IsUserInRole("STC Borough") || Roles.IsUserInRole("STC HQ")) { %>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <h2>STC Administration</h2>
                <p style="width: 915px;" class="dottedLine"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-1"></div>
        <% if (Roles.IsUserInRole("STC HQ") || Roles.IsUserInRole("STC Borough")) { %>
            <div class="col-xs-3">
            <% if (Roles.IsUserInRole("STC HQ")) { %>
            	<h3>Maintenance</h3>
            	<ul>
                  <li><%: Html.ActionLink("Measurement Maintenance", "", "Measurement") %></li>
                  <li><%: Html.ActionLink("Truck Maintenance", "", "TruckType") %></li>
                  <li><%: Html.ActionLink("STC Agency Maintenance", "", "StcAgency") %></li>
            	  <li><%: Html.ActionLink("Zone Maintenance", "", "Zone") %></li>
            	  <li><%: Html.ActionLink("Set Site On Hand/Capacity", "", "SiteOnHand") %></li>
            	</ul>
            <% } %>
            	<h3>Orders</h3>
            	<ul>
            <% if (Roles.IsUserInRole("STC HQ")) { %>
            	  <li><%: Html.ActionLink("Set Purchase Order", "", "StcPurchaseOrder") %></li>
            <% } %>
            <% if (Roles.IsUserInRole("STC HQ") || Roles.IsUserInRole("STC Borough")) { %>
            	  <li><%: Html.ActionLink("Process STC Order", "", "StcOrder") %></li>
            <% } %>
            	</ul>
            </div>
        <% } %>
            <div class="col-xs-3">
            	<h3>STC Movement</h3>
            	<ul>
            	  <li><%: Html.ActionLink("Delivery", "", "Delivery") %></li>
                  <li><%: Html.ActionLink("Pickup", "", "StcPickup/DSNY") %></li>
                  <li><%: Html.ActionLink("External Pickup", "", "StcPickup") %></li>
            <% if (Roles.IsUserInRole("STC HQ") || Roles.IsUserInRole("STC Borough")) { %>
                  <li><%: Html.ActionLink("Transfer", "", "StcTransfer") %></li>
            <% } %>
                  <li><%: Html.ActionLink("Commodity Needs Request", "", "NeedsRequest") %></li>
            	</ul>
            </div>
        </div>
    </div>
    <% } %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PrintArea" runat="server">
    <div id="printArea"></div>
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 945px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
    </script>
</asp:Content>