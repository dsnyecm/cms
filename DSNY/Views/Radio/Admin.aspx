﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Radio Administration
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.5.custom.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssIncludes" runat="server">
    <link href="<%=ResolveUrl("~/Content/jqueryUI.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="server">
    <div id="contentContainer" class="container-fluid">
         <div class="row">
            <div class="col-md-4">
                <h2>Radio Administration</h2>
                <p class="fullDottedLine"></p>
            </div>
        </div>
         <div class="row">
            <% if (Roles.IsUserInRole("Radio Admin")) { %>
            <div class="col-md-2" style="padding-left: 25px;">
                <h3 style="margin: 0;">Radio Maintenance</h3>
                <ul>
                    <li><%: Html.ActionLink("Manage Radio Codes", "Codes","Radio") %></li>
                    <li><%: Html.ActionLink("Manage Radios", "Index", "Radio")%></li>
                </ul>
            </div>
            <% } %>

            <% if (Roles.IsUserInRole("Radio District")) { %>
            <div class="col-md-2">
                <h3 style="margin: 0;">Manage Inventory</h3>
                <ul>
                  <li>
                      <%: Html.ActionLink("Manage Inventory","Inventory","Radio") %>
                  </li>
                </ul>
            </div>
            <% } %>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="FooterContent" runat="server">
     <div id="noFloat" style="width: 550px;">&nbsp;</div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="PrintArea" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="JavascriptSource" runat="server">
</asp:Content>
