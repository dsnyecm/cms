﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Manage Radios
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery.validate-1.17.0.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.12.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout-3.4.2.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout.mapping-latest.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout.wrap.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jQuery.Validator.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jQuery-Validator.Knockout.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/bindinghandlers.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jqValidate-bindinghandlers.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/lodash.3.9.3.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssIncludes" runat="server">
    <link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Content/jquery-ui.css")%>" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #radio-equipment tbody tr {
            background: white;
        }

            #radio-equipment tbody tr:nth-child(4n+1), #radio-equipment tbody tr:nth-child(4n+2) {
                background: #A4D49A;
            }
    </style>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="server">
    <div id="contentContainer" class="container-fluid" data-bind="jqValidation: validationContext">
        <div class="row">
            <div class="col-md-4">
                <h2>Manage Radios</h2>
                <p class="fullDottedLine"></p>
            </div>
        </div>
        <div class="iconContainer">
            <div class="iconLeft">
                <a href="<%: Url.RouteUrl(new { controller = "Radio", action = "Admin" }) %>" class="iconLink">
                    <img border="0" src="~/Content/Images/Icons/Cancel.png" alt="Cancel" runat="server" /><br />
                    Cancel
                </a>
            </div>
            <div class="iconLeft">
                <a href="" class="iconLink" data-bind="click: add">
                    <div style="margin-top: 2px">
                        <img border="0" src="~/Content/Images/Icons/add.png" alt="Add Line" runat="server" />
                    </div>
                    <div style="margin-top: 5px">
                        <span>Add Line</span>
                    </div>
                </a>
            </div>
            <div class="iconLeft">
                <a href="#" id="submitForm" class="iconLink" data-bind="click: save">
                    <img border="0" src="~/Content/Images/Icons/create.png" alt="Save" runat="server" /><br />
                    Save
                </a>
            </div>
            <div class="iconLeft">
                <a href="#" class="iconLink" data-bind="click: createTrunkSheet">
                    <img border="0" src="~/Content/Images/Icons/Excel.png" alt="Trunk Sheet" runat="server" /><br />
                    Create Trunk Sheet
                </a>
            </div>
            <div class="iconLeft">
                <a href="#" data-bind="click: createRMAReport" class="iconLink">
                    <img border="0" src="~/Content/Images/Icons/RMA.png" alt="RMA" runat="server" /><br />
                    RMA Report
                </a>
            </div>
            <div class="iconLeft">
                <a href="#" class="iconLink" data-bind="click: requestInventory">
                    <img border="0" src="~/Content/Images/Icons/inventory-request.png" alt="Inventory Request" runat="server" /><br />
                    Inventory Request
                </a>
            </div>
            <div class="iconLeft">
                <a href="#" class="iconLink" data-bind="click: createPickupSheet">
                    <img border="0" src="~/Content/Images/Icons/pickup.png" alt="Pickup" runat="server" /><br />
                    Pickup Sheet
                </a>
            </div>
            <div id="submitting" style="display: none; padding-top: 15px;">Submitting...</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="saved" style="display: none; padding-top: 15px; color: forestgreen">Your changes have been saved.</div>
            </div>
        </div>
        <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <!-- SEARCH -->
        <div class="row spaced padded-top">
            <div class="col-md-12">
                Borough: 
                <select data-bind="options: BorughsWithLocations,
    optionsValue: 'description',
    optionsText: 'description',
    value: boroughCd,
    optionsCaption: 'Choose...'">
                </select>

                Location: 
                <select data-bind="enable: boroughCd(), options: BoroughLocatons,
    optionsValue: 'userId',
    optionsText: 'fullName',
    value: locationId,
    optionsCaption: 'Choose...'">
                </select>
                <input type="checkbox" data-bind="checked: ShowInActive" />
                Show InActive

                <input type="button" value="Retrieve" data-bind="click: search " />
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <span id="error" style="color: red;"></span>
            </div>
        </div>
        <div id="tabs">
            <ul>
                <li><a href="#radio-equipment">Radio Equipment</a></li>
                <li><a href="#accessories">Accessories</a></li>
            </ul>

            <div id="radio-equipment" class="tab-div" data-index="0">
                <div data-bind="visible: AllRadios().length > 0">
                    <table id="radio-grid" style="width: 100%">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Status</th>
                                <th>Serial #</th>
                                <th style="width: 50px;">Radio ID</th>
                                <th>Trunk ID</th>
                                <th>Location</th>
                                <th>Equipment Type</th>
                                <th>Make</th>
                                <th>Model</th>
                                <th>Vehicle No</th>
                                <th>Inventory Date</th>
                                <th>Service Date</th>
                                <th>Spare</th>
                                <th>Active</th>
                                <th align="center" style="text-align: center;"><span style="border-bottom: solid 1px white; text-align: center;">District Inventory</span><br />
                                    <span style="float: left;">Status</span>&nbsp;&nbsp;&nbsp;&nbsp;<span style="float: right;">Date</span>
                                </th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: AllRadios">
                            <tr>
                                <td>
                                    <input type="checkbox" data-bind="visible: Id() > 0, checked: Selected" />
                                    <a href="#" data-bind="visible: Id() == 0, click: $root.removeRadio">
                                        <img src="../../Content/images/icons/remove.png" alt="Remove" title="Remove this radio" /></a>
                                </td>
                                <td>
                                    <select data-required="true" data-bind="options: $parent.Statuses,
    optionsValue: 'id',
    optionsText: 'description',
    value: StatusId,
    optionsCaption: 'Choose...',
    event: { change: $root.statusChanged }">
                                    </select>

                                </td>
                                <td style="width: 100px;">
                                    <input type="text" style="width: 100px;" class="Small" data-bind="value: SerialNumber" data-required="true" /></td>
                                <td>
                                    <input type="text" style="width: 50px;" class="Small" data-bind="value: DSNYRadioId" /></td>
                                <td>
                                    <input type="text" style="width: 50px;" class="Small" data-bind="value: TrunkId" /></td>
                                <td>
                                    <select data-required="true" data-bind="options: $parent.Locations,
    optionsValue: 'userId',
    optionsText: 'fullName',
    value: IssueToUserId,
    optionsCaption: 'Choose...'">
                                    </select>
                                </td>
                                <td>
                                    <select data-required="true" data-bind="options: $parent.EquipmentTypes,
    optionsValue: 'id',
    optionsText: 'description',
    value: RadioEquipmentTypeId,
    optionsCaption: 'Choose...'">
                                    </select>
                                </td>
                                <td>
                                    <select data-required="true" data-bind="options: $root.Makes,
    optionsValue: 'id',
    optionsText: 'description',
    value: RadioMakeId,
    event: { change: $root.makeChanged },
    optionsCaption: 'Choose...'">
                                    </select>
                                </td>
                                <td>
                                    <select data-required="true" data-bind="options: $data.Models,
    optionsValue: 'id',
    optionsText: 'description',
    value: RadioModelId,
    optionsCaption: 'Choose...'">
                                    </select>
                                </td>
                                <td>
                                    <input type="text" style="width: 60px;" class="Small" data-bind="value: AssignedToVehicle" /></td>
                                <td>
                                    <input type="text" class="Small tcalReadOnly" data-bind="value: InventoryDate, tcal: { name: 'InventoryDate' }" readonly="true" size="10" style="width: 70px;" /></td>
                                <td>
                                    <input type="text" class="Small tcalReadOnly" data-bind="value: ServiceDate, tcal: { name: 'ServiceDate' }" readonly="true" size="10" style="width: 70px;" /></td>
                                <td align="center">
                                    <input type="checkbox" data-bind="checked: IsSpare" /></td>
                                <td align="center">
                                    <input type="checkbox" data-bind="checked: IsActive" />
                                </td>
                                <td>
                                    <span data-bind="text: DistrictInventoryStatus, style: { color: LastDistInvStatusId() != StatusId() ? 'red' : 'black', fontWeight: LastDistInvStatusId() != StatusId() ? 'bold' : '', float: left }"></span>
                                    &nbsp;&nbsp;
                                    <span style="float: right;" data-bind="text: LastDistInventoryDate, style: { color: IsDistrictInventoryDateOlderThanRequestDate() ? 'red' : 'black', fontWeight: IsDistrictInventoryDateOlderThanRequestDate() ? 'bold' : 'normal' } "></span>
                                </td>
                                <td>
                                    <a href="#" data-bind="if: Id() > 0, click: $root.showHistory ">History</a>
                                </td>
                            </tr>
                            <tr data-bind="visible: Id() > 0">
                                <td colspan="16" style="text-align: center">Comments:
                                    <input type="text" data-bind="value: Comments, attr: {'data-required': Id()>0 && StatusId() != OriginalStatusId() }" style="width: 50%;" class="radio-comments"  />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div data-bind="visible: AllRadios().length === 0">
                    There are no Radios.
                </div>
            </div>
            <div id="accessories" class="tab-div" data-index="1">
                <div>
                    <table id="radio-accessory-grid" style="width: 100%">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Part Number</th>
                                <th>Location</th>
                                <th>Equipment Type</th>
                                <th>Make</th>
                                <th>Description</th>
                                <th>Qty</th>
                                <th>Spare</th>
                                <th>Action</th>
                                <th>Last District Inventory(Date/Qty)</th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: AllRadioAccessories">
                            <tr>
                                <td style="width: 40px; text-align: center;">
                                    <a href="#" data-bind="visible: Id() == 0, click: $root.removeRadioAccessory">
                                        <img src="../../Content/images/icons/remove.png" alt="Remove" title="Remove this radio" /></a>
                                </td>
                                <td>
                                    <input type="text" class="Small" data-bind="value: PartNumber" data-required="true" /></td>
                                <td>
                                    <select data-required="true" data-bind="options: $parent.Locations,
    optionsValue: 'userId',
    optionsText: 'fullName',
    value: UserId,
    optionsCaption: 'Choose...'">
                                    </select>
                                </td>
                                <td>
                                    <select data-required="true" data-bind="options: $parent.EquipmentTypes,
    optionsValue: 'id',
    optionsText: 'description',
    value: EquipmentTypeId,
    optionsCaption: 'Choose...'">
                                    </select>
                                </td>
                                <td>
                                    <select data-required="true" data-bind="options: $root.Makes,
    optionsValue: 'id',
    optionsText: 'description',
    value: MakeId,
    optionsCaption: 'Choose...'">
                                    </select>
                                </td>
                                <td>
                                    <input type="text" data-bind="value: Description" />
                                </td>
                                <td>
                                    <input data-required="true" type="number" data-bind="value: CurrentQty" style="width: 50px;" />
                                </td>
                                <td>
                                    <input type="checkbox" data-bind="checked: IsSpare" />
                                </td>
                                <td>
                                    <a href="#" data-bind="if: Id() > 0, click: $root.receive">Receive</a>&nbsp;
                                    <a href="#" data-bind="if: Id() > 0, click: $root.tranferIt ">Transfer</a>
                                    <!-- Render Spans if its a new accessory -->
                                    <span data-bind="if: Id() == 0">Receive</span>&nbsp;
                                     <span data-bind="if: Id() == 0">Transfer</span>
                                </td>
                                <td>
                                    <span data-bind="text: LastDistInvDate, style: { color: IsDistrictInventoryDateOlderThanRequestDate() ? 'red' : 'black', fontWeight: IsDistrictInventoryDateOlderThanRequestDate() ? 'bold': 'normal' }"></span>
                                    &nbsp;&nbsp;
                                    <span data-bind="text: LastDistInvQty, style: { color: LastDistInvQty() != CurrentQty() ? 'red' : 'black', fontWeight: LastDistInvQty() != CurrentQty() ? 'bold' : '', float: left }""></span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div data-bind="visible: AllRadioAccessories().length === 0">
                    There are no accessories.
                </div>
            </div>
        </div>
    </div>
    <div id="radio-history" class="modal" title="Radio History">
        <script type="text/html" id="history-template">
            <td><span data-bind="text: ChangeDate"></span></td>
            <td><span data-bind="text: Status"></span></td>
            <td><span data-bind="text: SerialNumber"></span></td>
            <td><span data-bind="text: DSNYRadioId"></span></td>
            <td><span data-bind="text: TrunkId"></span></td>
            <td><span data-bind="text: Location"></span></td>
            <td><span data-bind="text: EquipmentType"></span></td>
            <td><span data-bind="text: Make"></span></td>
            <td><span data-bind="text: Model"></span></td>
            <td><span data-bind="text: VehicleNumber"></span></td>
            <td><span data-bind="text: InventoryDate"></span></td>
            <td><span data-bind="text: ServiceDate"></span></td>
            <td>
                <input type="checkbox" data-bind="checked: IsSpare" disabled />
            </td>
            <td>
                <input type="checkbox" data-bind="checked: IsActive" disabled />
            </td>
            <td><span data-bind="text: Comments"></span></td>
        </script>
        <table style="width: 100%">
            <thead>
                <tr>
                    <th>Change Date</th>
                    <th>Status</th>
                    <th>Serial #</th>
                    <th>Radio ID</th>
                    <th>Trunk ID</th>
                    <th>Location</th>
                    <th>Equipment Type</th>
                    <th>Make</th>
                    <th>Model</th>
                    <th>Vehicle No</th>
                    <th>Inventory Date</th>
                    <th>Service Date</th>
                    <th>Spare</th>
                    <th>Active</th>
                    <th>Comments</th>
                </tr>
            </thead>
            <tbody data-bind="foreach: AllHistories()">
                <tr data-bind="template: { name: 'history-template', data: $data }">
                </tr>
            </tbody>
        </table>
    </div>


    <div id="radio-receive" class="modal" title="Recieve" data-bind="jqValidation: validationContext" style="overflow: hidden;">
        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                <span style="color: red;" data-bind="text: error" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span><b>Qty:</b></span>
                <input id="receive-qty" type="number" data-bind="qty" class="Small" style="width: 50px" data-required="true" />
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12">
                <span><b>Comments:</b></span>
                <br />
                <textarea id="receive-comments" data-bind="comments" class="Small" data-required="true" style="width: 300px; height: 120px;"></textarea>
            </div>
        </div>
    </div>
    <div id="radio-transfer" class="modal" title="Send" data-bind="jqValidation: validationContext" style="overflow: hidden;">
        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                <span style="color: red;" data-bind="text: error" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <span><b>Qty:</b></span>
                <input id="transfer-qty" type="number" data-bind="qty" class="Small" style="width: 50px" data-required="true" />
            </div>
            <div class="col-md-7" style="text-align: right;">
                <span><b>Send To:</b></span>
                <select id="transfer-to" data-required="true" data-bind="options: users,
    optionsValue: 'userId',
    optionsText: 'fullName',
    value: transferToId,
    optionsCaption: 'Select Transfer Location...'"
                    style="width: 100px;">
                </select>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12">
                <span><b>Comments:</b></span>
                <br />
                <textarea id="transfer-comments" data-bind="comments" class="Small" data-required="true" style="width: 375px; height: 120px;"></textarea>
            </div>
        </div>
    </div>
    <div id="inventory-request" class="modal" title="Inventory Request" style="overflow: hidden;">
        <div class="row padded-top">
            <div class="col-md-12">
                <span>Your are about to notify all districts that have radios to provide a new inventory,<br />
                    <br />
                    Do you want to continue?</span>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 550px;">&nbsp;</div>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        var obj = JSON.parse('<%= this.Model.Json %>');
        obj.add = function () {
            if ($('#tabs').tabs('option', 'active') == "0") {
                this.Radios.push(ko.wrap.fromJS(ko.wrap.toJS(this.BlankRadio)));
            }
            else {
                this.RadioAccessories.push(ko.wrap.fromJS(ko.wrap.toJS(this.BlankRadioAccessory)));
            }
        };
        obj.boroughCd = ko.observable('');
        obj.locationId = ko.observable('');

        obj.boroughCd_Search = ko.observable(''); // used for searching
        obj.locationId_Search = ko.observable(''); // used for searching

        obj.Models = ko.observableArray();

        obj.searchActive = ko.observable(false);
        obj.ShowInActive_Search = ko.observable(false); // For searching
        obj.search = function () {
            //vm.searchActive(true);
            obj.boroughCd_Search(vm.boroughCd());
            obj.locationId_Search(vm.locationId());
            obj.ShowInActive_Search(vm.ShowInActive());
        }

        //obj.boroughCd.subscribe(function (newval) {
        //    if (newval == undefined || newval == '') {
        //        vm.searchActive(false);
        //    }
        //});


        obj.createRMAReport = function () {
            var radios = ko.toJS(vm.RadiosToSave());
            var anySelected = false;
            for (var i = 0; i < radios.length; i++) {
                if (radios[i].Selected == true) {
                    anySelected = true;
                    break;
                }
            }

            if (anySelected == false) {
                $('#error').html("Please selected at least one radio to create RMA report");
                setTimeout(function () { $('#error').html(""); }, 5000);
                return;
            }

            $.post({
                url: '/Radio/CreateRMAReport',
                type: 'post',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify({ radios: radios }),
                success: function (data) {
                    // $("#submitForm").removeAttr("disabled").removeClass("ui-state-disabled");
                    if (data.success) {
                        // window.open(data.file, '_blank', 'location=yes,height=700,width=1000,scrollbars=yes,status=yes');                       
                        window.location.href = data.file;
                    }
                    else {
                        $('#error').html(data.error);
                    }
                },
                error: function (err) {
                    $("#submitForm").removeAttr("disabled").removeClass("ui-state-disabled");
                    console.log(JSON.stringify(err));
                }
            });
        }

        obj.createPickupSheet = function () {
            var radios = ko.toJS(vm.RadiosToSave());
            var anySelected = false;
            for (var i = 0; i < radios.length; i++) {
                if (radios[i].Selected == true) {
                    anySelected = true;
                    break;
                }
            }

            if (anySelected == false) {
                $('#error').html("Please selected at least one radio to create pickup sheet");
                setTimeout(function () { $('#error').html(""); }, 5000);
                return;
            }

            $.post({
                url: '/Radio/CreatePickupSheet',
                type: 'post',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify({ radios: radios }),
                success: function (data) {
                    // $("#submitForm").removeAttr("disabled").removeClass("ui-state-disabled");
                    if (data.success) {
                        // window.open(data.file, '_blank', 'location=yes,height=700,width=1000,scrollbars=yes,status=yes');                       
                        window.location.href = data.file;
                    }
                    else {
                        $('#error').html(data.error);
                    }
                },
                error: function (err) {
                    $("#submitForm").removeAttr("disabled").removeClass("ui-state-disabled");
                    console.log(JSON.stringify(err));
                }
            });
        }

        obj.statusChanged = function (radio, event) {
            if (radio.Id() > 0) {
                $(event.target).closest("tr").next().find(".radio-comments").prop("data-required", true);
                //$(formId).removeData('validator');
                //$(formId).removeData('unobtrusiveValidation');
                //$.validator.unobtrusive.parse(formId);
                debugger;
            }
        };
        obj.makeChanged = function (event) {
            var make = event.RadioMakeId();
            $.get({
                url: '/radio/GetModelsByMake',
                data: {
                    makeId: make,
                    date: new Date().getTime()
                },
                success: function (data) {
                    if (data && data.length > 0) {
                        console.log("models loaded for make id " + make);
                        event.Models(data);
                    }
                    else {
                        console.log("No models found for make " + make);
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            });
        };

        obj.save = function () {
            var validationResult = this.validationContext.Validate();
            console.log(validationResult);
            if (!validationResult.valid) {
                this.errors(validationResult.messages);
                var invalidInput = validationResult.invalidInputs[0].elem[0];
                var tabDiv = $(invalidInput).parents(".tab-div")[0];
                var index = $(tabDiv).data("index");
                $('#tabs').tabs({ active: index });
                return false;
            }

            $("#submitForm").attr("disabled", "disabled").addClass("ui-state-disabled");

            var radios = ko.toJS(vm.RadiosToSave());
            var radioAccessories = ko.toJS(vm.AccessoriesToSave());
            $.post({
                url: '/Radio/SaveRadios',
                type: 'post',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify({ radios: radios, radioAccessories: radioAccessories }),
                success: function (data) {
                    $("#submitForm").removeAttr("disabled").removeClass("ui-state-disabled");
                    if (data.success) {
                        $('#saved').show();
                        setTimeout(function () { $('#saved').hide(); }, 5000);
                        $('#error').html('');
                        window.location.href = window.location.href; // reload the page so the newly added radios Ids are set
                    }
                    else {
                        var errorMsg = JSON.parse(data.errors).join("<br/>");
                        $('#error').html(errorMsg);
                    }
                },
                error: function (err) {
                    $("#submitForm").removeAttr("disabled").removeClass("ui-state-disabled");
                    console.log(JSON.stringify(err));
                }
            });
        };
        var objHistoryData = [];
        var vmHistory = ko.wrap.fromJS(objHistoryData);
        obj.showHistory = function (event) {
            $.get({
                url: '/radio/GetRadioHistory',
                data: { radioId: event.Id() },
                success: function (data) {
                    debugger;
                    if (!objHistoryData.histories) {
                        objHistoryData = { histories: ko.observableArray(data) };
                        vmHistory = ko.wrap.fromJS(objHistoryData);
                        vmHistory.AllHistories = ko.computed(function () {
                            var self = vmHistory;
                            return ko.utils.arrayFilter(self.histories(), function (item) {
                                return item;
                            });
                        });
                        ko.applyBindings(vmHistory, $('#radio-history')[0])
                    }
                    else {
                        ko.wrap.updateFromJS(vmHistory.histories, data);
                    }

                    // $('#radio-history table tbody tr').remove();
                    var dialog = $("#radio-history").dialog({
                        autoOpen: true,
                        height: 382,
                        width: 1000,
                        modal: true,
                        buttons: {
                            Cancel: function () {
                                dialog.dialog("close");
                                //ko.cleanNode($('#radio-history')[0]);
                            }
                        },
                        close: function () {
                            //ko.cleanNode($('#radio-history')[0]);
                        }
                    });
                }
            });

        };

        obj.receive = function (accessory) {
            var model = {
                Id: accessory.Id(),
                qty: ko.observable(),
                comments: ko.observable(''),
                error: ko.observable('')
            };
            var vmReceive = ko.wrap.fromJS(model);
            vmReceive.validationContext = ko.jqValidation({
                returnBool: false, // We want more details of our validation result.
                useInlineErrors: true, // Use inline errors
                errorClass: 'input-validation-error', // Apply error class
                msg_empty: '', // Global empty message.
                noInlineErrors: "*[type='password']" // Password fields should not show inline errors.
            });

            ko.applyBindings(vmReceive, $('#radio-receive')[0]);
            var dialog = $("#radio-receive").dialog({
                autoOpen: true,
                height: 300,
                width: 400,
                modal: true,
                buttons: {
                    "Save": function () {
                        var validationResult = vmReceive.validationContext.Validate();
                        if (!validationResult.valid) {
                            return false;
                        }
                        //  var data = ko.toJS(vmReceive);
                        $.ajax({
                            url: '/radio/Receive',
                            type: 'post',
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'json',
                            data: JSON.stringify({ id: vmReceive.Id(), qty: $('#receive-qty').val(), comments: $('#receive-comments').val() }),
                            success: function (data) {
                                if (data.success) {
                                    $('#error').html('');
                                    dialog.dialog("close");
                                    ko.cleanNode($('#radio-receive')[0]);
                                    accessory.CurrentQty(accessory.CurrentQty() + parseInt($('#receive-qty').val()));
                                    $('#saved').show();
                                    $(function () { $('#saved').hide(); }, 5000);
                                }
                                else {
                                    vmReceive.error(data.errors);
                                }
                            },
                            error: function (err) {
                                console.log(JSON.stringify(err));
                            }
                        })

                    },
                    Cancel: function () {
                        ko.cleanNode($('#radio-receive')[0]);
                    }
                },
                close: function () {
                    ko.cleanNode($('#radio-receive')[0]);
                }
            });
        };

        obj.tranferIt = function (accessory) {
            debugger;
            // Exclude the currrent accessory's location becuase it can't be transferred to current accessory itself.
            var locationsForTransfer = $.map(obj.Locations, function (location) {
                if (location.userId != accessory.UserId())
                    return location;
            });
            var model = {
                Id: accessory.Id(),
                qty: ko.observable(),
                comments: ko.observable(''),
                users: ko.observableArray(locationsForTransfer), // use the main/root VM to get the locations list
                transferToId: ko.observable(''),
                error: ko.observable('')
            };

            var vmTransfer = ko.wrap.fromJS(model);
            vmTransfer.validationContext = ko.jqValidation({
                returnBool: false, // We want more details of our validation result.
                useInlineErrors: true, // Use inline errors
                errorClass: 'input-validation-error', // Apply error class
                msg_empty: '', // Global empty message.
                noInlineErrors: "*[type='password']" // Password fields should not show inline errors.
            });

            ko.applyBindings(vmTransfer, $('#radio-transfer')[0]);
            var dialog = $("#radio-transfer").dialog({
                autoOpen: true,
                height: 300,
                width: 400,
                modal: true,
                buttons: {
                    "Save": function () {
                        var validationResult = vmTransfer.validationContext.Validate();
                        if (!validationResult.valid) {
                            return false;
                        }

                        $.ajax({
                            url: '/radio/Transfer',
                            type: 'post',
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'json',
                            data: JSON.stringify({ id: vmTransfer.Id(), qty: $('#transfer-qty').val(), comments: $('#transfer-comments').val(), transferToId: $('#transfer-to').val() }),
                            success: function (data) {
                                if (data.success) {
                                    window.location.href = window.location.href;
                                }
                                else {
                                    debugger;
                                    vmTransfer.error(data.errors);
                                }
                            },
                            error: function (err) {
                                console.log(JSON.stringify(err));
                            }
                        })
                    },
                    Cancel: function () {
                        ko.cleanNode($('#radio-transfer')[0]);
                    }
                },
                close: function () {
                    ko.cleanNode($('#radio-transfer')[0]);
                }
            });
        };

        obj.requestInventory = function () {
            var dialog = $("#inventory-request").dialog({
                autoOpen: true,
                height: 200,
                width: 400,
                modal: true,
                buttons: {
                    "Yes": function () {
                        $.ajax({
                            url: '/radio/SendInventoryRequest',
                            type: 'post',
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'json',
                            success: function (data) {
                                $(".ui-button:contains('Yes')").button("enable");
                                $(".ui-button:contains('No')").button("enable");
                                if (data.success) {
                                    alert("Inventory request submitted.");
                                    dialog.dialog("close");
                                }
                                else {
                                    dialog.dialog("close");
                                    $('#error').html(data.errors);
                                }
                            },
                            error: function (err) {
                                $(".ui-button:contains('Yes')").button("enable");
                                $(".ui-button:contains('No')").button("enable");
                                console.log(JSON.stringify(err));
                            }
                        })

                        $(".ui-button:contains('Yes')").button("disable");
                        $(".ui-button:contains('No')").button("disable");
                    },
                    No: function () {
                        dialog.dialog("close");
                    }
                }
            });
        }

        obj.createTrunkSheet = function () {
            var anySelected = false;
            var selectedRadioIds = [];
            for (var i = 0; i < vm.AllRadios().length; i++) {
                if (vm.AllRadios()[i].Selected()) {
                    selectedRadioIds.push(vm.AllRadios()[i].Id());
                }
            }
            if (selectedRadioIds.length == 0) {
                alert("Please select one or more radios to create the trunk sheet.");
                return;
            }

            var Ids = selectedRadioIds.join("|");
            window.location.href = '/radio/CreateTrunkSheet?radioIds=' + Ids;
        }

        obj.removeRadio = function (radio, event) {
            radio.IsDeleted(true);
        }

        obj.removeRadioAccessory = function (radioAccessory, event) {
            radioAccessory.IsDeleted(true);
        }
        var vm = ko.wrap.fromJS(obj);
        vm.RadiosToSave = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.Radios(), function (item) {
                return item;
            });
        });

        vm.AccessoriesToSave = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.RadioAccessories(), function (item) {
                return item;
            });
        });

        self.locationId = ko.observable('');
        self.ShowInActive = ko.observable(false);
        self.items = ko.observableArray(obj.Locations);
        vm.AllRadios = ko.computed(function () {
            var self = vm;
            var borough = self.boroughCd_Search();
            var location = self.locationId_Search();
            var showInActive = self.ShowInActive_Search();
            return ko.utils.arrayFilter(self.Radios(), function (item) {
                if (item.Id() < 1 && item.IsDeleted() == false)
                    return item;

                if (borough) {
                    if (item.Borough() == borough) {
                        if (location) {
                            if (item.IssueToUserId() == location) {
                                if (showInActive) {
                                    if (item.IsDeleted() == false)
                                        return item;
                                }
                                else {
                                    if (item.IsDeleted() == false && item.IsActive() == true)
                                        return item;
                                }
                            }
                        }
                        else {
                            if (showInActive) {
                                if (item.IsDeleted() == false)
                                    return item;
                            }
                            else {
                                if (item.IsDeleted() == false && item.IsActive() == true)
                                    return item;
                            }
                        }
                    }
                }
                else {
                    if (showInActive) {
                        if (item.IsDeleted() == false)
                            return item;
                    }
                    else {
                        if (item.IsDeleted() == false && item.IsActive() == true)
                            return item;
                    }
                }
            });
        });

        vm.AllRadioAccessories = ko.computed(function () {
            var self = vm;

            var borough = self.boroughCd_Search();
            var location = self.locationId_Search();
            var showInActive = self.ShowInActive_Search();

            return ko.utils.arrayFilter(self.RadioAccessories(), function (item) {
                if (item.Id() < 1 && item.IsDeleted() == false)
                    return item;

                if (borough) {
                    if (item.Borough() == borough) {
                        if (location) {
                            if (item.UserId() == location) {
                                if (item.IsDeleted() == false)
                                    return item;
                            }
                        }
                        else {
                            if (item.IsDeleted() == false)
                                return item;
                        }
                    }
                }
                else {
                    if (item.IsDeleted() == false)
                        return item;
                }
            });
        });

        vm.validationContext = ko.jqValidation({
            returnBool: false, // We want more details of our validation result.
            useInlineErrors: true, // Use inline errors
            errorClass: 'input-validation-error', // Apply error class
            msg_empty: '', // Global empty message.
            noInlineErrors: "*[type='password']" // Password fields should not show inline errors.
        });

        vm.errors = ko.observableArray([]);
        self.boroughCd = ko.observable('');
        self.boroughItems = ko.observableArray(obj.Boroughs);

        vm.BorughsWithLocations = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.Boroughs(), function (item) {

                var locations = ko.utils.arrayFilter(self.Locations(), function (loc) {
                    if (loc.borough() == item.description())
                        return loc;
                });
                if (locations.length > 0)
                    return item;
            });
        });

        vm.BoroughLocatons = ko.computed(function () {
            var self = vm;
            var borghValue = self.boroughCd();
            return ko.utils.arrayFilter(self.Locations(), function (item) {
                if (item.borough() == borghValue)
                    return item;
            });
        });

        $(function () {
            $('#tabs').tabs({ active: 0 });
            ko.applyBindings(vm, $('#contentContainer')[0]);

            reStripe("radio-accessory-grid");
        });

    </script>
</asp:Content>
