﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.RadioCodesViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Radio Codes Maintenance
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery.validate-1.17.0.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.12.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout-3.4.2.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout.mapping-latest.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout.wrap.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jQuery.Validator.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jQuery-Validator.Knockout.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/bindinghandlers.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jqValidate-bindinghandlers.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/lodash.3.9.3.min.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssIncludes" runat="server">
    <link href="<%=ResolveUrl("~/Content/jquery-ui.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="server">
    <div id="contentContainer" style="width: 1000px;" data-bind="jqValidation: validationContext">
        <div class="row">
            <div class="col-md-4">
                <h2>Manage Radio Codes</h2>
                <p class="fullDottedLine"></p>
            </div>
        </div>
        <div class="iconContainer">
            <div class="iconLeft">
                <a href="<%: Url.RouteUrl(new { controller = "Radio", action = "Admin" }) %>" class="iconLink">
                    <img border="0" src="~/Content/Images/Icons/Cancel.png" alt="Cancel" runat="server" /><br />
                    Cancel
                </a>
            </div>
            <div class="iconLeft">
                <a href="" id="add-link" class="iconLink" data-bind="click: addEquipmentType">
                    <div style="margin-top: 2px">
                        <img border="0" src="~/Content/Images/Icons/add.png" alt="Add Line" runat="server" />
                    </div>
                    <div style="margin-top: 5px">
                        <span>Add Line</span>
                    </div>
                </a>
            </div>
            <div class="iconLeft">
                <a href="#" id="submitButton" class="iconLink" data-bind="click: save">
                    <img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Save" style="vertical-align: bottom;" runat="server" /><br />
                    Save
                </a>
            </div>
            <br />
        </div>
        <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>
         <div id="submitting" style="display: none; padding-top: 15px;">Submitting...</div>
            <div id="saved" style="display: none; padding-top: 15px; color: forestgreen">Your changes have been saved.</div>
        <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <div id="tabs">
            <ul>
                <li><a href="#equipment-type" onclick="changeAddAction('addEquipmentType')">Equipment Type</a></li>
                <li><a href="#equipment-status" onclick="changeAddAction('addEquipmentStatus')">Equipment Status</a></li>
                <li><a href="#radio-make-model" onclick="changeAddAction('addMake')">Radio Make/Model</a></li>
                <li><a href="#repair-problem-type" onclick="changeAddAction('addRepairProblemType')">Repair Problem Type</a></li>
            </ul>

            <div id="equipment-type" class="tab-div" data-index="0">
                <div data-bind="visible: filterEquipmentTypessDisplay().length > 0">
                    <table style="width: 100%">
                        <thead>
                            <tr>
                                <th>Active</th>
                                <th>Equipment Type</th>
                                <th>District Inventory</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: filterEquipmentTypessDisplay">
                            <tr data-bind="css: { 'alt': $index() % 2 == 1 }">
                                  <td>
                                    <input type="checkbox" class="Name" data-bind="checked: IsActive" data-required="true" />
                                </td>
                                <td>
                                    <input type="text" class="Name" data-bind="value: Type,, uniqueName: true" data-required="true" maxlength="50" style="width: 150px" />
                                </td>
                                <td>
                                    <input type="checkbox" class="Name" data-bind="checked: DistrictInventory" data-required="true" />
                                </td>
                                <td>
                                    <img src="/content/images/icons/delete.png" data-bind="click: $root.deleteEquopmentStatus" class="tcalIcon" alt="Delete Item" title="Delete Item">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div data-bind="visible: filterEquipmentTypessDisplay().length === 0">
                    There are no Equipment Types. Click on "Add Line" button above to enter an equipment type.
                </div>
            </div>
            <div id="equipment-status" class="tab-div" data-index="1">
                <div data-bind="visible: filterEquipmentStatusesDisplay().length > 0">
                    <table style="width: 100%">
                        <thead>
                            <tr>
                                <th>Equipment Status</th>
                                <th>Workflow</th>
                                <th>Inventory</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: filterEquipmentStatusesDisplay">
                            <tr data-bind="css: { 'alt': $index() % 2 == 1 }">
                           
                                <td>
                                    <input type="text" class="Name" data-bind="value: Name" data-required="true" maxlength="50" style="width: 150px" />
                                </td>
                                <td>
                                    <input type="checkbox" class="Name" data-bind="checked: WorkFlow" data-required="true" />
                                </td>
                                <td>
                                    <input type="checkbox" class="Name" data-bind="checked: Inventory" data-required="true" />
                                </td>

                                <td>
                                    <img src="/content/images/icons/delete.png" data-bind="click: $root.deleteEquopmentStatus" class="tcalIcon" alt="Delete Item" title="Delete Item"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div data-bind="visible: filterEquipmentStatusesDisplay().length === 0">
                 There are no Equipment Status. Click on "Add Line" button above to enter an equipment status.
                </div>
            </div>

            <div id="radio-make-model"  class="tab-div" data-index="2">
                <div id="makeModelModal">
                    <div class="row padded-bottom" data-bind="foreach: RadioMakesToDisplay">
                        <div class="row">
                              <div class="col-md-1">Active</div>
                            <div class="col-md-2">Make</div>
                            <div class="col-md-4">Model</div>
                            <div class="col-md-2">&nbsp;</div>
                        </div>
                        <div class="row">
                              <div class="col-md-1">
                                    <input type="checkbox" class="Name" data-bind="checked: IsActive" data-required="true" />
                                </div>
                            <div class="col-md-2">
                                <input data-bind="value: Name, uniqueName: true"  data-required="true" style="width: 100%;" />
                            </div>
                            <div class="col-md-4" style="border: 1px solid black; padding-top: 7px;">
                                <div class="row" data-bind="foreach: Models()">
                                    <div class="col-md-12" data-bind="visible: IsDeleted() === false">
                                        <div class="row">
                                            <div class="col-md-1">
                                                <input type="checkbox" class="Name" data-bind="checked: IsActive" data-required="true" />
                                            </div>
                                            <div class="col-md-5" <%-- data-bind="visible:  IsDeleted == false"--%>>
                                                <input data-bind="value: Name, uniqueName: true"  data-required="true" style="width: 100%;" />
                                            </div>
                                            <div class="col-md-3">
                                                <a href class="iconLink" data-bind="click: $root.addMakeModel.bind($parent)">
                                                    <img border="0" src="~/Content/Images/Icons/add.png" alt="Add Model" runat="server" />
                                                </a>

                                                <a href class="removeRow" data-bind="visible: $parent.Models().length > 1, click: $root.removeMakeModel.bind($parent)">
                                                    <img border="0" src="~/Content/Images/Icons/delete.png" alt="Remove Model" runat="server" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-2">
                                <a href class="removeRow" data-bind="click: $root.removeMake">
                                    <img border="0" src="~/Content/Images/Icons/delete.png" alt="Remove Make" runat="server" />
                                </a>
                            </div>
                        </div>
                    </div>

                    <div  data-bind="visible: RadioMakesToDisplay().length === 0">
                 There is no Make. Click on "Add Line" button above to enter new make.
                </div>
                </div>
            </div>

            <div id="repair-problem-type"  class="tab-div"data-index="3">
                <div data-bind="visible: filterRpairProblemTypeDisplay().length > 0">
                    <table style="width: 100%">
                        <thead>
                            <tr>
                                <th>Active</th>
                                <th>Repair Problem Type</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: filterRpairProblemTypeDisplay">
                            <tr data-bind="css: { 'alt': $index() % 2 == 1 }">
                                <td>
                                    <input type="checkbox" class="Name" data-bind="checked: IsActive" data-required="true" />
                                </td>
                                <td>
                                    <input type="text" class="Name" data-bind="value: Type" data-required="true" maxlength="50" style="width: 150px" /></td>

                                <td>
                                    <img src="/content/images/icons/delete.png" data-bind="click: $root.deleteRepairProblemType" class="tcalIcon" alt="Delete Item" title="Delete Item"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div data-bind="visible: filterRpairProblemTypeDisplay().length === 0">
                   There is no repair problem type. Click on "Add Line" button above to enter a repair problem type.
                </div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 550px;">&nbsp;</div>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        var obj = JSON.parse('<%= this.Model.Json %>');
        obj.addRepairProblemType = function () { this.RepairProblemType.push(ko.wrap.fromJS(ko.wrap.toJS(this.BlankRepairProblemType))); }
        obj.deleteRepairProblemType = function (item) { console.log(item); item.IsDeleted(true); }

        obj.addEquipmentStatus = function () { this.EquipmentStatuses.push(ko.wrap.fromJS(ko.wrap.toJS(this.BlankEquipmentStatus))); }
        obj.deleteEquopmentStatus = function (item) { console.log(item); item.IsDeleted(true); }

        obj.addEquipmentType = function () { this.EquipmentTypes.push(ko.wrap.fromJS(ko.wrap.toJS(this.BlankEquipmentType))); }
        obj.deleteEquopmentType = function (item) { console.log(item); item.IsDeleted(true); }


        //obj.showMakeModelModal = function () {
        //    var that = this;
        //    self.RadioMakes.removeAll();
        //    self.show.modal(true);

        //    if (this.RadioMakes().length === 0) {
        //        var make = $.extend({}, interfaces.make);
        //        make.Models = ko.observableArray([$.extend({}, interfaces.model)]);
        //        self.RadioMakes.push(make);
        //    } else {
        //        ko.utils.arrayPushAll(self.RadioMakes, this.RadioMakes());
        //    }
        //}

        obj.addMake = function () {
            var make = ko.wrap.toJS(this.BlankMake);
            make.Models.push(ko.wrap.fromJS(ko.wrap.toJS(this.BlankModel)))
            this.RadioMakes.push(ko.wrap.fromJS(make));
        }
        obj.removeMake = function (item) { console.log(item); item.IsDeleted(true); }
        obj.addMakeModel = function () {
            debugger;

            //this.Models.push(ko.wrap.toJS(this.BlankModel));
            // this.BlankModel.IsDeleted(false);
            this.Models.push(ko.wrap.fromJS(ko.wrap.toJS(obj.BlankModel)));
        }

        obj.removeMakeModel = function (item) {
            console.log(item);
            item.IsDeleted(true);
        }

        obj.save = function () {
            var validationResult = this.validationContext.Validate();
            console.log(validationResult);
            if (!validationResult.valid) {
                this.errors(validationResult.messages);
                alert('There are required fields missing.')
                var invalidInput = validationResult.invalidInputs[0].elem[0];
                var tabDiv = $(invalidInput).parents(".tab-div")[0];
                var index = $(tabDiv).data("index");
                $('#tabs').tabs({ active: index });
                var actionName = "addEquipmentType";
                if (index = 1) {
                    actionName = "addEquipmentStatus";
                }
                if (index = 2) {
                    actionName = "addMake";
                }
                if (index = 3) {
                    actionName = "addRepairProblemType";
                }
                changeAddAction(actionName);
                return false;
            }
            var model = {};
            model.RepairProblemType = ko.toJS(this.filterSaveRepairProblemCodes());
            model.EquipmentStatuses = ko.toJS(this.filterSaveEquipmentStatuses());
            model.EquipmentTypes = ko.toJS(this.filterSaveEquipmentTypes());
            model.RadioMakes = ko.toJS(this.filterSaveMakeTypes());
            $.post({
                url: '/Radio/Save',
                type: 'post',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(model),
                success: function (result) {
                    if (result.success) {
                        $('#saved').show();
                        setTimeout(function () {
                            $('#saved').hide();
                        }, 5000);
                        ko.wrap.updateFromJS(vm.RepairProblemType, result.data.RepairProblemType);
                        ko.wrap.updateFromJS(vm.EquipmentStatuses, result.data.EquipmentStatuses);
                        ko.wrap.updateFromJS(vm.EquipmentTypes, result.data.EquipmentTypes);
                        ko.wrap.updateFromJS(vm.RadioMakes, result.data.RadioMakes);
                    }
                    else {
                        alert(result.message);
                        window.location.reload();
                    }

                },
                error: function (err) {
                    console.log(JSON.stringify(err));
                }
            })
        }



        var vm = ko.wrap.fromJS(obj);

        // vm.RadioModelsToDisplay = ko.computed(function () {
        //    var self = this;
        //     return ko.utils.arrayFilter(self.Models(), function (item) {
        //        return !item.IsDeleted;
        //    });
        //});

        vm.RadioMakesToDisplay = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.RadioMakes(), function (item) {
                return !item.IsDeleted();
            });
        });

        vm.filterSaveMakeTypes = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.RadioMakes(), function (item) {
                return item;
            });
        });

        vm.filterEquipmentTypessDisplay = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.EquipmentTypes(), function (item) {
                return !item.IsDeleted();
            });
        });
        vm.filterSaveEquipmentTypes = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.EquipmentTypes(), function (item) {
                return !(item.IsDeleted() && !item.ID());
            });
        });

        vm.filterRpairProblemTypeDisplay = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.RepairProblemType(), function (item) {
                return !item.IsDeleted();
            });
        });
        vm.filterSaveRepairProblemCodes = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.RepairProblemType(), function (item) {
                return !(item.IsDeleted() && !item.ID());
            });
        });

        vm.filterEquipmentStatusesDisplay = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.EquipmentStatuses(), function (item) {
                return !item.IsDeleted();
            });
        });


        vm.filterSaveEquipmentStatuses = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.EquipmentStatuses(), function (item) {
                return !(item.IsDeleted() && !item.ID());
            });
        });

        //vm.getVM = ko.computed(function () {
        //    var self = vm;
        //    ko.utils.mode
        //    return self;
        //});



        vm.validationContext = ko.jqValidation({
            returnBool: false, // We want more details of our validation result.
            useInlineErrors: true, // Use inline errors
            errorClass: 'input-validation-error', // Apply error class
            msg_empty: '', // Global empty message.
            noInlineErrors: "*[type='password']" // Password fields should not show inline errors.
        });
        vm.errors = ko.observableArray([]);
        $(function () {
            $('#tabs').tabs({ active: 0 });
            ko.applyBindings(vm, $('#contentContainer')[0]);
        });

        function changeAddAction(bindingMehtodName) {
            $("#add-link").attr("data-bind", "click: " + bindingMehtodName + "");
            ko.cleanNode($("#add-link")[0]);
            ko.applyBindings(vm, $("#add-link")[0]);
        }
    </script>
</asp:Content>
