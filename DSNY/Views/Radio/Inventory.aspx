﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.InventroyViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Radio Codes Maintenance
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery.validate-1.17.0.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.12.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout-3.4.2.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout.mapping-latest.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout.wrap.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jQuery.Validator.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jQuery-Validator.Knockout.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/bindinghandlers.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jqValidate-bindinghandlers.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/lodash.3.9.3.min.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssIncludes" runat="server">
    <link href="<%=ResolveUrl("~/Content/jquery-ui.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="server">
    <div id="contentContainer" style="width: 1000px;" data-bind="jqValidation: validationContext">
        <div class="row">
            <div class="col-md-4">
                <h2>Manage Inventory</h2>
                <p class="fullDottedLine"></p>
            </div>
        </div>
        <div class="iconContainer">
            <div class="iconLeft">
                <a href="<%: Url.RouteUrl(new { controller = "Radio", action = "Admin" }) %>" class="iconLink">
                    <img border="0" src="~/Content/Images/Icons/Cancel.png" alt="Cancel" runat="server" /><br />
                    Cancel
                </a>
            </div>

            <div class="iconLeft">
                <a href="#" id="submitButton" class="iconLink" data-bind="click: save">
                    <img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Save" style="vertical-align: bottom;" runat="server" /><br />
                    Save
                </a>
            </div>
            <br />
            <div id="submitting" style="display: none; padding-top: 15px;">Submitting...</div>
            <div id="saved" style="display: none; padding-top: 15px; color: forestgreen">Your changes have been saved.</div>
        </div>
        <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <div id="tabs">
            <ul>
                <li><a href="#inventory-div">Inventory</a></li>
                <li><a href="#accessory-div">Accessories</a></li>
            </ul>

            <div id="inventory-div">
                <div data-bind="visible: AllRadios().length > 0">
                    <table style="width: 100%">
                        <thead>
                            <tr>
                                <th>Status</th>
                                <th>Serial #</th>
                                <th>Equipment Type</th>
                                <th>DSNY Id</th>
                                <th>Make</th>
                                <th>Model</th>
                                <th>Vehicle No</th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: AllRadios">
                            <tr>
                                <td>
                                    <select data-bind="options: $parent.Statuses,
    optionsValue: 'id',
    optionsText: 'description',
    value: DistInvStatusId">
                                    </select>

                                </td>
                                <td>
                                    <input disabled type="text" class="Small" data-bind="value: SerialNumber" /></td>

                                <td>
                                    <select disabled data-bind="options: $parent.EquipmentTypes,
    optionsValue: 'id',
    optionsText: 'description',
    value: RadioEquipmentTypeId">
                                    </select>
                                </td>
                                <td>
                                    <input disabled type="text" class="Small" data-bind="value: DSNYRadioId" /></td>
                                <td>
                                    <select disabled data-bind="options: $root.Makes,
    optionsValue: 'id',
    optionsText: 'description',
    value: RadioMakeId,
    event: { change: $root.makeChanged }">
                                    </select>
                                </td>
                                <td>
                                    <select disabled data-bind="options: $data.Models,
    optionsValue: 'id',
    optionsText: 'description',
    value: RadioModelId">
                                    </select>
                                </td>
                                <td>
                                    <input disabled type="text" class="Small" data-bind="value: AssignedToVehicle" /></td>
                                <tr data-bind="visible: DistInvStatusId() != StatusId()">
                                    <td colspan="14" style="text-align: center">Comments:
                                    <input type="text" class="status-comments" data-bind="value: Comments" style="width: 50%;" />
                                    </td>
                                </tr>
                        </tbody>
                    </table>
                </div>
                <div data-bind="visible: AllRadios().length === 0">
                    There are no Radios.
                </div>
            </div>
            <div id="accessory-div">
                <div data-bind="visible: AllAccessories().length > 0">
                    <table style="width: 100%">
                        <thead>
                            <tr>
                                <th>Quantity</th>
                                <th>Part Number</th>
                                <th>Make</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: AllAccessories">
                            <tr>
                                <td>
                                    <input type="number" class="Small" data-bind="value: LastDistInvQty" data-required="true" style="width: 50px;" />
                                </td>
                                <td>
                                    <input disabled type="text" class="Small" data-bind="value: PartNumber" />
                                </td>
                                <td>
                                    <select disabled data-bind="options: $root.Makes,
    optionsValue: 'id',
    optionsText: 'description',
    value: MakeId,">
                                    </select>
                                </td>
                                <td>
                                    <input disabled type="text" class="Small" data-bind="value: Description" style="width:300px;" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div data-bind="visible: AllAccessories().length === 0">
                    All items quantity is updated .
                </div>
            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="FooterContent" runat="server">
    <div id="noFloat" style="width: 550px;">&nbsp;</div>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">

        var obj = JSON.parse('<%= this.Model.Json %>');

        obj.boroughCd = ko.observable('');
        obj.locationId = ko.observable('');

        obj.Models = ko.observableArray();
        obj.search = function () {
            alert("search");
        }

        obj.save = function () {

            var flag = false;
            debugger;
            $(".status-comments").each(function (ind, ele) {
                if (flag == false && $(ele).is(":visible") && $(ele).val() == "") {
                    alert('Comments field is required.');
                    flag = true;
                    return;
                }
            });

            if (flag) {
                return false;
            }

            var validationResult = this.validationContext.Validate();
            console.log(validationResult);
            if (!validationResult.valid) {
                this.errors(validationResult.messages);
                alert('There are required fields missing.')
                return false;
            }
            var mod = {};
            mod.radios = ko.toJS(this.AllRadios());
            mod.accessories = ko.toJS(this.AllAccessories());
            $.post({
                url: '/Radio/SaveInventory',
                type: 'post',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(mod),
                success: function (model) {
                    if (model.success) {
                        alert("Inventory submitted.");
                        window.location.href = window.location.href;
                    }
                    else
                    {                        
                        alert(data.errors);
                    }
                },
                error: function (err) {
                    console.log(JSON.stringify(err));
                }
            });
        };
        var vm = ko.wrap.fromJS(obj);
        vm.AllRadios = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.Radios(), function (item) {
                return item;
            });
        });


        vm.AllAccessories = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.RadioAccessories(), function (item) {
                return item;
            });
        });
        vm.validationContext = ko.jqValidation({
            returnBool: false, // We want more details of our validation result.
            useInlineErrors: true, // Use inline errors
            errorClass: 'input-validation-error', // Apply error class
            msg_empty: '', // Global empty message.
            noInlineErrors: "*[type='password']" // Password fields should not show inline errors.
        });
        vm.errors = ko.observableArray([]);
        $(function () {
            $('#tabs').tabs({ active: 0 });
            ko.applyBindings(vm, $('#contentContainer')[0]);
        })

    </script>
</asp:Content>
