﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.Data.Message>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Send Message / Create Task
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="JavascriptIncludes" runat="server">
	<script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/messagePrint.css")%>" rel="stylesheet" type="text/css" media="print" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<!---<h2>
		Send Message / Create Task
		<span class="page-title-help">&nbsp;&nbsp;&nbsp;[<a href="#" onclick="helpPopup('CreateMessage')">?</a>]</span>
	</h2>-->
	<h2>
		Send Message / Create Task
	</h2>
	<p class="dottedLine" style="width: 980px"></p>

	<div style="width: 980px;">
		<div class="iconRight" style="float: right; width: 50px; text-align: center; font-size: .85em;">
			<a href="#" id="sendEmail" style="text-decoration: none; color: #696969;">
				<img style="border: 0;" src="~/Content/Images/Icons/sendEmail.png" alt="Send" runat="server" /><br />Send
			</a>
		</div>
		<div class="iconRight" style="float: right; width: 50px; text-align: center; font-size: .85em;">
			<a href="" onclick="window.name=window.name+',<%: Model.Message_Id %>'; history.go(-1); return false;" style="text-decoration: none; color: #696969;">
				<img style="border: 0;" src="~/Content/Images/Icons/cancel.png" alt="Cancel" runat="server" /><br />Cancel
			</a>
		</div>

	   <div style="float: left;">
			<% Html.EnableClientValidation(); %>
			<% using (Html.BeginForm("Create", "Message", FormMethod.Post)) {%>
				<%: Html.ValidationSummary(true)%>
					
					<div class="message-display-label"><%: Html.LabelFor(model => model.Sent_By_UserId)%>/<%: Html.LabelFor(model => model.Message_Date)%>:</div>
					<div class="editor-field">
						<input id="Sent_By" name="Sent_By" class="text-box-readonly" readonly="true" type="text" value="<%: User.Identity.Name.ToString() %>" /> 
						<%: Html.TextBoxFor(model => model.Message_Date, new { @readonly = "true", @class = "text-box-readonly",
								@Value = Model.Message_Date.Value.TimeOfDay == new TimeSpan(0, 0, 0) ?
								Model.Message_Date.Value.ToString("M/d/yyyy") : Model.Message_Date.Value.ToString("M/d/yyyy h:mm tt")})%>
					</div>

					<div class="clear"></div>

					<div class="message-display-label"><%: Html.ValidationMessageFor(model => model.Dept_Msg_No)%><%: Html.LabelFor(model => model.Dept_Msg_No)%>:</div>
					<div class="editor-field" style="width: 600px;"><%: Html.TextBoxFor(model => model.Dept_Msg_No, new { @class = "text-box" })%></div>

					<div class="clear"></div>

					<div class="message-display-label"><%: Html.ValidationMessageFor(model => model.Message_Code)%><%: Html.LabelFor(model => model.Message_Code)%>:</div>
					<div class="editor-field" style="width: 600px;"><%: Html.TextBoxFor(model => model.Message_Code, new { @class = "text-box" })%></div>
			
					<div class="clear"></div>

					<div class="message-display-label"><%: Html.ValidationMessageFor(model => model.Message_Subject)%><%: Html.LabelFor(model => model.Message_Subject)%>:</div>
					<div class="editor-field" style="width: 600px;"><%: Html.TextBoxFor(model => model.Message_Subject, new { @class = "text-box" })%></div>
			
					<div class="clear"></div>

					<div class="message-display-label"><%: Html.ValidationMessageFor(model => model.Message_Text)%><%: Html.LabelFor(model => model.Message_Text)%>:</div>
					<div class="editor-field">
						<textarea id="Message_Text" name="Message_Text" cols="88" rows="20" tabindex="4" wrap="hard"><%: Model.Message_Text%></textarea>
                        <pre id="Message_Text_Hidden"></pre>
					</div>

					<div class="clear"></div>
			<% } %>
		</div>
	</div>

	<div class="clear"></div>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
	<div id="noFloat" style="width: 880px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="JavascriptSource" runat="server">

<script type="text/javascript">
	$(document).ready(function () {

		// init the text editor
		//$("#Message_Text").markItUp(mySettings);

		// focus on dept code #
	    $("#Dept_Msg_No").focus();
	    $('#Message_Text_Hidden').text($('#Message_Text').val());

		// handles tab character insertion in textarea
		$("#Message_Text").keydown(function (e) {
			if (e.keyCode == 9) {
				if (e.shiftKey) {
					// need to implement negative tab
				} else {
					pasteIntoInput(this, '\t');
				}

				e.preventDefault();
			}

			$('#Message_Text_Hidden').text($(this).val());
		});

		$("#Message_Text").bind('input propertychange', function (e) {
		    $('#Message_Text_Hidden').text($(this).val());
		})

		/* Validation for Page */
		$('#sendEmail').click(function () {
			var error = false;

			if ($('#Dept_Msg_No').val() == '') {
				$('#Dept_Msg_No_validationMessage').text('*').addClass('error').show();
				error = true;
			}
			else {
				$('#Dept_Msg_No_validationMessage').hide();
			}

			if ($('#Message_Code').val() == '') {
				$('#Message_Code_validationMessage').text('*').addClass('error').show();
				error = true;
			}
			else {
				$('#Message_Code_validationMessage').hide();
			}

			if ($('#Message_Subject').val() == '') {
				$('#Message_Subject_validationMessage').text('*').addClass('error').show();
				error = true;
			}
			else {
				$('#Message_Subject_validationMessage').hide();
			}

			if ($('#Message_Subject').val() == '') {
				$('#message_text_validationmessage').text('*').addclass('error').show();
				error = true;
			}
			else {
				$('#message_text_validationmessage').hide();
			}

			if (error == false) {
				if (confirm('Are you sure you want to send this message?')) {
					document.forms[0].submit();
				}
			}
		});
	});
</script>
</asp:Content>