﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.Data.Message>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Send Message / Create Task
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="JavascriptIncludes" runat="server">
	<script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.5.custom.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/jqueryUI.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Content/messagePrint.css")%>" rel="stylesheet" type="text/css" media="print" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<!--<h2>
        Send Message / Create Task
        <span class="page-title-help">&nbsp;&nbsp;&nbsp;[<a href="#" onclick="helpPopup('CreateMessage')">?</a>]</span>
	</h2>-->
	<h2>
        Send Message / Create Task
	</h2>

	<p class="dottedLine" style="width: 980px"></p>

	<div style="width: 980px;">
		<div class="iconRight" style="float: right; width: 50px; text-align: center; font-size: .85em;">
			<a href="#" id="sendEmail" style="text-decoration: none; color: #696969;">
				<img border="0" src="~/Content/Images/Icons/sendEmail.png" alt="Send" runat="server" /><br />Send
			</a>
		</div>
		<div class="iconRight" style="float: right; width: 50px; text-align: center; font-size: .85em;">
			<a href="#" id="cancel" style="text-decoration: none; color: #696969;">
				<img border="0" src="~/Content/Images/Icons/cancel.png" alt="Cancel" runat="server" /><br />Cancel
			</a>
		</div>

	   <div style="float: left;">
			<% Html.EnableClientValidation(); %>
			<% using (Html.BeginForm()) {%>
				<%: Html.ValidationSummary(true) %>
			
					<div class="message-display-label"><%: Html.LabelFor(model => model.Sent_By_UserId)%>/<%: Html.LabelFor(model => model.Message_Date)%>:</div>
					<div class="editor-field">
						<input id="Sent_By" name="Sent_By" class="text-box-readonly" readonly="readonly" type="text" value="<%: User.Identity.Name.ToString() %>" /> 
						<%: Html.TextBoxFor(model => model.Message_Date, new { @readonly = "true", @class = "text-box-readonly",
								@Value = Model.Message_Date.Value.TimeOfDay == new TimeSpan(0, 0, 0) ?
								Model.Message_Date.Value.ToString("M/d/yyyy") : Model.Message_Date.Value.ToString("M/d/yyyy h:mm tt")})%>
					</div>

					<div class="clear"></div>

					<div class="message-display-label"><%: Html.ValidationMessageFor(model => model.Dept_Msg_No)%><%: Html.LabelFor(model => model.Dept_Msg_No)%>:</div>
					<div class="editor-field"><%: Html.TextBoxFor(model => model.Dept_Msg_No, new { @class = "text-box", tabIndex = 1 })%></div>

					<div class="clear"></div>

					<div class="message-display-label"><%: Html.ValidationMessageFor(model => model.Message_Code)%><%: Html.LabelFor(model => model.Message_Code)%>:</div>
					<div class="editor-field"><%: Html.TextBoxFor(model => model.Message_Code, new { @class = "text-box", tabIndex = 2 })%></div>

					<div class="clear"></div>

					<div class="message-display-label"><%: Html.ValidationMessageFor(model => model.Message_Subject) %><%: Html.LabelFor(model => model.Message_Subject) %>:</div>
					<div class="editor-field"><%: Html.TextBoxFor(model => model.Message_Subject, new { @class = "text-box", tabIndex = 3 })%></div>
			
					<div class="clear"></div>

					<div class="message-display-label"><%: Html.ValidationMessageFor(model => model.Message_Text) %><%: Html.LabelFor(model => model.Message_Text) %>:</div>
					<div class="editor-field">
						<textarea id="Message_Text" name="Message_Text" cols="88" rows="20" tabindex="4" wrap="hard"></textarea>
                        <pre id="Message_Text_Hidden"></pre>
					</div>

					<div class="clear"></div>
			<% } %>
		</div>
	</div>

	<div class="clear"></div>

	<div id="confirm-send" class="modal" title="Send Message" style="overflow: hidden;">
		<p>
			<span class="ui-icon ui-icon-alert"></span>
            Are you sure you want to send this message?
		</p>
	</div>

	<div id="changes-lost" class="modal" title="Lost Changes" style="overflow: hidden;">
		<p>
			<span class="ui-icon ui-icon-alert"></span>
			Your changes will be lost. Are you sure you want to leave?
		</p>
	</div>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
	<div id="noFloat" style="width: 880px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="JavascriptSource" runat="server">

<script type="text/javascript">
	$(document).ready(function () {
		var deptMsgNo = $('#Dept_Msg_No');
		var messageCode = $('#Message_Code');
		var messageSubject = $('#Message_Subject');
		var messageText = $('#Message_Text');
		var checkCancel = true;
		
		// init the text editor
		//$("#Message_Text").markItUp();

		// focus on dept code #
		deptMsgNo.focus();
		$('#Message_Text_Hidden').text($('#Message_Text').val());

		// handles tab character insertion in textarea
		messageText.keydown(function (e) {
			if (e.keyCode == 9) {
				pasteIntoInput(this, '\t');

				e.preventDefault();
			}
		});

		messageText.bind('input propertychange', function (e) {
		    $('#Message_Text_Hidden').text($(this).val());
		})

		/* Validation for Page */
		$('#sendEmail').click(function () {
			var error = false;

			if (deptMsgNo.val() == '') {
				$('#Dept_Msg_No_validationMessage').text('*').addClass('error').show();
				error = true;
			}
			else {
				$('#Dept_Msg_No_validationMessage').hide();
			}

			if (messageCode.val() == '') {
				$('#Message_Code_validationMessage').text('*').addClass('error').show();
				error = true;
			}
			else {
				$('#Message_Code_validationMessage').hide();
			}

			if (messageSubject.val() == '') {
				$('#Message_Subject_validationMessage').text('*').addClass('error').show();
				error = true;
			}
			else {
				$('#Message_Subject_validationMessage').hide();
			}

			if (messageText.val() == '') {
				$('#Message_Text_validationMessage').text('*').addClass('error').show();
				error = true;
			}
			else {
				$('#Message_Text_validationMessage').hide();
			}

			if (error == false) {
				var confirmDialog = $("#confirm-send").dialog({
					autoOpen: true,
					height: 175,
					width: 300,
					modal: true,
					buttons: {
					    Yes: function () {
					        checkCancel = false;
							document.forms[0].submit();
						},
						No: function () {
							confirmDialog.dialog("close");
						}
					}
				});
			}
		});

		var isTouched = function () {
			return deptMsgNo.val() !== '' || messageCode.val() !== '' || messageSubject.val() !== '' || messageText.val() !== '';
		}

		var goBack = function () {
			checkCancel = false;
			window.name = window.name+',<%: Model.Message_Id %>';
			history.go(-1);
		}

		$(window).bind("beforeunload", function (evt) {
			if (checkCancel && isTouched())  {
				return 'Your changes will be lost. Are you sure you want to leave?';
			}
		});

		$('#cancel').click(function () {
			if (isTouched()) {
				var cancelDialog = $("#changes-lost").dialog({
					autoOpen: true,
					height: 175,
					width: 300,
					modal: true,
					buttons: {
						Leave: function () {
							goBack();   // not sure why, have to call twice
							goBack();
						},
						Stay: function () {
							cancelDialog.dialog("close");
						}
					}
				});
			} else {
				goBack();
			}
		});
	});

</script>
</asp:Content>