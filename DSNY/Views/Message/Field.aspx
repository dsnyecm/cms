﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.Data.Message>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	View Message
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="JavascriptIncludes" runat="server">
	<script src="<%=ResolveUrl("~/Scripts/jquery-1.4.1.min.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/messagePrint.css")%>" rel="stylesheet" type="text/css" media="print" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<h2>View Message</h2>
	<p class="dottedLine" style="width: 840px"></p>

	<div style="width: 840px;">
		<div class="iconRight" style="float: right; width: 50px; text-align: center; font-size: .85em;">
			<a href="" onclick="window.name=window.name+',<%: Model.Message_Id %>'; history.go(-1); return false;" style="text-decoration: none; color: #696969;">
				<img border="0" src="~/Content/Images/Icons/cancel.png" runat="server" /><br />Close
			</a>
		</div>

		<div style="float: left;">
			<div class="message-display-label"><%: Html.LabelFor(model => model.Sent_By_UserId)%>/<%: Html.LabelFor(model => model.Message_Date)%>:</div>
			<div class="message-display-field">
				<span ><%: Model.aspnet_Users.UserName.ToString() %></span>
				<span style="padding-left: 50px;"><%: Model.Message_Date.Value.TimeOfDay == new TimeSpan(0, 0, 0) ? Model.Message_Date.Value.ToString("M/d/yyyy") : Model.Message_Date.Value.ToString("M/d/yyyy h:mm tt") %></span>
			</div>

			<div class="clear"></div>

			<div class="message-display-label"><%: Html.LabelFor(model => model.Dept_Msg_No)%>:</div>
			<div class="message-display-field"><%: Model.Dept_Msg_No %></div>

			<div class="clear"></div>

			<div class="message-display-label"><%: Html.LabelFor(model => model.Message_Code)%>:</div>
			<div class="message-display-field"><%: Model.Message_Code %></div>

			<div class="clear"></div>

			<div class="message-display-label"><%: Html.LabelFor(model => model.Message_Subject) %>:</div>
			<div class="message-display-field" style="width: 650px;"><%: Model.Message_Subject %></div>
			
			<div class="clear"></div>

			<div class="message-display-label"><%: Html.LabelFor(model => model.Message_Text) %>:</div>
			<div class="message-display-field">
                <pre id="Message_Text" style="margin-top: 3px;"><%: Model.Message_Text %></pre>
                <pre id="Message_Text_Hidden"><%: Model.Message_Text %></pre>
			</div>
            
		</div>
	</div>

	<div class="clear"></div>

</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="FooterContent" runat="server">
	<div id="noFloat" style="width: 880px;">&nbsp;</div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="JavascriptSource" runat="server">

<script type="text/javascript">
	$(document).ready(function () {
		if ($.browser.msie && parseInt($.browser.version, 10) == 7) {
			$("#Message_Text").text($("#Message_Text").text().replace(new RegExp('\t', 'g'), '\t\t'));
		}
	});
</script>

</asp:Content>