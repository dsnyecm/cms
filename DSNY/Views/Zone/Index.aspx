﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.ZoneViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Zone Maintenance
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.12.1.min.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout-3.4.2.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/knockout.wrap.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jQuery.Validator.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jQuery-Validator.Knockout.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/bindinghandlers.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssIncludes" runat="server">
    <link href="<%=ResolveUrl("~/Content/images/jqueryUI/jquery-ui-1.12.1.min.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Content/images/jqueryUI/jquery-ui-1.12.icon-font.min.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Zone Maintenance</h2>
    <p  style="width: 915px;" class="dottedLine"></p>

    <div id="contentContainer" style="display:none;" class="container" data-bind="visible: show, jqValidation: validationContext">
        <div style="float: left;" class="row">
            <div class="iconLeft">
                <a href="" onclick="history.go(-1); return false;" class="iconLink">
                    <img border="0" src="~/Content/Images/Icons/cancel.png" alt="Close" runat="server" /><br />Close
                </a>
            </div>
            <div class="iconLeft">
                <a href="" class="iconLink" data-bind="click: add">
                    <div style="margin-top: 2px"><img border="0" src="~/Content/Images/Icons/add.png" alt="Add Zone" runat="server" /></div>
                    <div style="margin-top: 5px">
                        <span>Add Zone</span>
                    </div>
                </a>
            </div>
            <div class="iconLeft">
                <a href="#" id="submitButton" class="iconLink" data-bind="click: save">
                    <img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Save" style="vertical-align: bottom;" runat="server" /><br />Save
                </a>
            </div>
            <div id="submitting" style="display: none; padding-top: 15px;">Submitting</div>
        </div>

        <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <div id="saved" style="display: none; padding-top: 15px;">Your changes have been saved.</div>

        <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-2"></div>
            <div class="col-xs-3">Zone
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-2"></div>
            <div class="col-xs-3">
                <div class="form-group" data-bind="if: Zones().length > 0">
                    <select data-bind="options: Zones,
                       optionsValue: 'Id',
                       optionsText: 'Name',
                       value: selectedZone,
                       event: { change: changeZone }"></select>
                    <img src="/content/images/icons/delete.png" data-bind="click: $root.delete" class="tcalIcon" alt="Delete Zone" title="Delete Zone"/>
                </div>
                <div class="form-group" data-bind="if: Zones().length == 0">
                    Click the + sign to add a zone.
                </div>
            </div>
        </div>

        <div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <div class="row">
            <div class="col-xs-3">Available
            </div>
            <div class="col-xs-2">
            </div>
            <div class="col-xs-3">Selected
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">
                <select style="width: 100%; height: 200px !important" data-bind="options: filterSites,
                   optionsValue: 'id',
                   optionsText: 'description',
                   selectedOptions: selectedSites" size="10" multiple="true"></select>
            </div>
            <div class="col-xs-2">
                <div class="col-xs-12" style="margin-top: 50px; text-align: center; padding: 0"><span class="ui-icon ui-icon-arrow-1-e" style="font-size: 4em; cursor: pointer;" data-bind="click: select"></span></div>
                <div class="col-xs-12" style="text-align: center; padding: 0"><span class="ui-icon ui-icon-arrow-1-w" style="font-size: 4em; cursor: pointer;" data-bind="click: unselect"></span></div>
            </div>
            <div class="col-xs-3">
                <select style="width: 100%; height: 200px !important" data-bind="options: filterCurrentZoneSites,
                   optionsValue: 'id',
                   optionsText: 'description',
                   selectedOptions: currentZoneSelectedSites" size="10" multiple="true"></select>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">Use control and mouseclick at the same time to select multiple entries.
            </div>
        </div>
    </div>

    <div id="zoneAdd" class="modal" title="Create Zone" style="overflow: hidden;">
        <p>
            <span class="ui-icon ui-icon-alert"></span>
            <span class="modal-text">
                Zone Name
            </span>
            <input id="ZoneName" type="text" />
        </p>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PrintArea" runat="server">
    <div id="printArea"></div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
    <script type="text/javascript">
        var obj = JSON.parse('<%= this.Model.Json %>');
        obj.add = function () { 
            var zoneAddDialog = $("#zoneAdd").dialog({
                autoOpen: true,
                height: 175,
                width: 300,
                modal: true,
                buttons: {
                    Cancel: function () {
                        $(this).dialog("close");
                        $("#ZoneName").val("");
                    },
                    Add: function () {
                        $(this).dialog("close");
                        var zone = { Name: $("#ZoneName").val() };
                        $("#ZoneName").val("");
                        $.post({ url: window.location.toString() + '/Add', 
                            type: 'post', 
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'json',
                            data: JSON.stringify(zone),
                            success: function(data){
                                if (data != "Success"){
                                    alert(data);
                                    return false;
                                }
                                vm.showNewZone();
                            },
                            error: function(err){
                                console.log(JSON.stringify(err));                    
                            }
                        })
                    }
                }
            });
        }
        obj.select = function() {
            var self = this;
            var sel = self.selectedSites();
            for (var i = 0; i < sel.length; i++) {
                var selCat = sel[i];
                var match = ko.utils.arrayFirst(self.Sites(), function(item) {
                    return item.id() == selCat;
                });
                if (match) {
                    self.CurrentZoneSites.push(ko.wrap.fromJS(ko.toJS(match)));
                }
                match.visible(false);
            }
            self.selectedSites([]);
        }
        obj.unselect = function() {
            var self = this;
            var sel = self.currentZoneSelectedSites();
            for (var i = 0; i < sel.length; i++) {
                var selCat = sel[i];
                var result = self.CurrentZoneSites.remove(function(item) {
                    return item.id() == selCat;
                });
                var match = ko.utils.arrayFirst(self.Sites(), function(item) {
                    return item.id() == selCat;
                });
                match.visible(true);
            }
            self.currentZoneSelectedSites([]);
        }
        obj.changeZone = function(){
            var currentViewModelString = JSON.stringify(ko.wrap.toJS(vm.CurrentZoneSites));
            if (viewModelString != currentViewModelString){
                if (!confirm("There are unsaved changes.  Are you sure you want to continue?")) {
                    vm.selectedZone(vm.previousZone());
                    return false;
                }
            }
            vm.previousZone(vm.selectedZone());
            var self = this;
            self.refresh(self.selectedZone());
        }
        obj.showNewZone = function(zone){
            var self = this;
            self.refresh(-1);
        }
        obj.refresh = function(id){
            var self = this;
            var url = window.location.toString() + '/ChangeZone/';
            if (id) { url += id; }
            $.post({ url: url, 
                type: 'post', 
                contentType: 'application/json; charset=utf-8',
                success: function(data){
                    ko.wrap.updateFromJS(self.Zones, data.Zones);
                    ko.wrap.updateFromJS(self.Sites, data.Sites);
                    ko.wrap.updateFromJS(self.CurrentZoneSites, data.CurrentZoneSites);
                    ko.wrap.updateFromJS(self.selectedSites, data.selectedSites);
                    ko.wrap.updateFromJS(self.currentZoneSelectedSites, data.currentZoneSelectedSites);
                    self.selectedZone(data.selectedZone);
                    viewModelString = JSON.stringify(ko.wrap.toJS(vm.CurrentZoneSites));
                },
                error: function(err){
                    console.log(JSON.stringify(err));                    
                }
            });
        }
        obj.toggle = function (item) { if (!item.Active()){ item.Qty(null); item.Capacity(null); this.deletes().push(item); } }
        obj.deletes = [];
        obj.delete = function () {
            var self = this;
            if (confirm('Are you sure that you want to delete this zone and all the selected sites in the zone?')){
                $.post({ url: window.location.toString() + '/Delete/' + self.selectedZone(), 
                    type: 'post', 
                    contentType: 'application/json; charset=utf-8',
                    success: function(data){
                        if (data != "Success"){
                            if (confirm(data)){
                                $.post({ url: window.location.toString() + '/Delete/' + self.selectedZone() + '?confirm=true' , success: function(){ self.refresh(); } });
                            }
                            return;
                        }
                        self.refresh();
                    },
                    error: function(err){
                        console.log(JSON.stringify(err));                    
                    }
                })
            }
        }
        obj.save = function () {
            var self = this;
            var viewModel = { selectedZone: ko.toJS(self.selectedZone), CurrentZoneSites: ko.toJS(self.CurrentZoneSites) };

            $.post({ url: window.location.toString() + '/Save', 
                type: 'post', 
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                //traditional: true,
                data: JSON.stringify(viewModel), 
                success: function(data){
                    $('#saved').show();
                    setTimeout(function(){
                        $('#saved').hide();
                    }, 5000);
                },
                error: function(err){
                    console.log(JSON.stringify(err));                    
                }
            })
        }
        obj.show = true;
        var vm = ko.wrap.fromJS(obj);
        vm.filterSites = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.Sites(), function (item) {
                return item.visible();
            });
        });
        vm.filterCurrentZoneSites = ko.computed(function () {
            var self = vm;
            return ko.utils.arrayFilter(self.CurrentZoneSites(), function (item) {
                return item.visible();
            });
        });

        vm.validationContext = ko.jqValidation({
            returnBool:false, // We want more details of our validation result.
            useInlineErrors: true, // Use inline errors
            errorClass: 'input-validation-error', // Apply error class
            msg_empty: '', // Global empty message.
            noInlineErrors: "*[type='password']" // Password fields should not show inline errors.
        });
    
        vm.errors = ko.observableArray([]);
        var viewModelString;
        $(function () {
            ko.applyBindings(vm, $('#contentContainer')[0]);
            viewModelString = JSON.stringify(ko.wrap.toJS(vm.CurrentZoneSites));
        });
    </script>
</asp:Content>