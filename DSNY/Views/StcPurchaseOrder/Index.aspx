﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.StcPurchaseOrderViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Set Purchase Order
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
	<script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/knockout-3.4.2.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/knockout.mapping-latest.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/knockout.wrap.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jQuery.Validator.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jQuery-Validator.Knockout.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/bindinghandlers.js?v=0.07")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<h2>Set Purchase Order</h2>
	<p  style="width: 915px;" class="dottedLine"></p>

	<div id="contentContainer" style="width: 1100px;display:none;" data-bind="visible: show, jqValidation: validationContext">
		<div style="float: left;">
			<div class="iconLeft">
				<a href="" class="iconLink" data-bind="click: cancel">
					<img border="0" src="~/Content/Images/Icons/cancel.png" alt="Close" runat="server" /><br />Close
				</a>
			</div>
			<div class="iconLeft">
				<a href="" class="iconLink" data-bind="click: add">
					<div style="margin-top: 2px"><img border="0" src="~/Content/Images/Icons/add.png" alt="Add Line" runat="server" /></div>
					<div style="margin-top: 5px">
						<span>Add Line</span>
					</div>
				</a>
			</div>
			<div class="iconLeft">
				<a href="#" id="submitButton" class="iconLink" data-bind="click: save">
					<img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Save" style="vertical-align: bottom;" runat="server" /><br />Save
				</a>
			</div>
			<div id="submitting" style="display: none; padding-top: 15px;">Submitting</div>
		</div>

		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

		<div id="saved" style="display: none; padding-top: 15px;">Your changes have been saved.</div>

		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

		<div style="float: left;">
			<div class="editor-field"><input type="checkbox" data-bind="checked: OpenOnly"/> Open PO's Only
				<input type="button" value="Retrieve" data-bind="click: changeFilter" />
			</div>
		</div>

		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

		<div data-bind="if: Products().length == 0">There are no active STC Products set up.</div>
		<div data-bind="if: Vendors().length == 0">There are no active STC Venors set up.</div>
		<div data-bind="if: Zones().length == 0">There are no active Zones set up.</div>

		<div id="StcPurchaseOrderDetails" style="display: inline-block; overflow-y: auto;">
		<% if (Model.PurchaseOrders != null) Html.RenderPartial("Grids/StcPurchaseOrders", Model.PurchaseOrders); %>
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PrintArea" runat="server">
	<div id="printArea"></div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
	<script type="text/javascript">
		var obj = JSON.parse('<%= this.Model.Json %>');
		obj.showOpenOnly = true;
		obj.changeFilter = function () { if (this.OpenOnly()) { this.showOpenOnly(true); } else { this.showOpenOnly(false); } }
		obj.delete = function (item) { console.log(item); item.Deleted(true); }
		obj.add = function () { this.PurchaseOrders.push(ko.mapping.fromJS(ko.mapping.toJS(this.BlankPurchaseOrder))); }
		obj.cancel = function() {
			var currentViewModelString = JSON.stringify(ko.wrap.toJS(vm.PurchaseOrders));
			if (viewModelString != currentViewModelString){
				if (!confirm("There are unsaved changes.  Are you sure you want to continue?")) return false;
			}
			history.go(-1); 
			return false;
		}
		obj.save = function () {
            if ($('.input-validation-error').length > 0){
                alert('There are invalid fields.  Please address them before saving.');
                return false;
            }
			var validationResult = this.validationContext.Validate();
			console.log(validationResult);
			if (!validationResult.valid) {
				this.errors(validationResult.messages);
				alert('There are required fields missing.')
				return false;
			}

			var values = ko.toJS(this.filterSave());
			var loc = window.location.toString();
			$.post({ url: loc + '/Save', 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				//traditional: true,
				data: JSON.stringify(values), 
				success: function(data){
					if (data != "Success"){
						if (data.indexOf("deleted") > 0){
							ko.utils.arrayFirst(vm.PurchaseOrders(), function(item) {
								item.Deleted(false);
							});
						}
						alert(data);						
						return false;
					}
					$('#saved').show();
					setTimeout(function(){
						$('#saved').hide();
					}, 5000);
					vm.refresh();
				},
				error: function(err){
					console.log(JSON.stringify(err));                    
				}
			})
		}
		obj.refresh = function(){
			var url = window.location.toString() + '/Refresh';
			$.post({ url: url, 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				success: function(data){
					ko.wrap.updateFromJS(vm.PurchaseOrders, data.PurchaseOrders);
					viewModelString = JSON.stringify(ko.wrap.toJS(vm.PurchaseOrders));
				},
				error: function(err){
					console.log(JSON.stringify(err));                    
				}
			});
		}
		obj.show = true;

		var vm = ko.mapping.fromJS(obj);
		vm.filterDisplay = ko.computed(function () {
			var self = vm;
			return ko.utils.arrayFilter(self.PurchaseOrders(), function (item) {
				return (self.showOpenOnly() ? !item.CompleteDate() : true) && !item.Deleted();
			});
		});
		vm.filterSave = ko.computed(function () {
			var self = vm;
			return ko.utils.arrayFilter(self.PurchaseOrders(), function (item) {
				return (self.showOpenOnly() ? !item.CompleteDate() : true) && !(item.Deleted() && !item.Id());
			});
		});

		vm.validationContext = ko.jqValidation({
			returnBool:false, // We want more details of our validation result.
			useInlineErrors: true, // Use inline errors
			errorClass: 'input-validation-error', // Apply error class
			msg_empty: '', // Global empty message.
			noInlineErrors: "*[type='password']" // Password fields should not show inline errors.
		});
	
		vm.errors = ko.observableArray([]);
		var viewModelString;
		$(function () {
			ko.applyBindings(vm, $('#contentContainer')[0]);
			viewModelString = JSON.stringify(ko.wrap.toJS(vm.PurchaseOrders));
		});
	</script>
</asp:Content>