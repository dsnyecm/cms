<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DSNY.ViewModels.StcPickupViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Street Treatment Pickups
</asp:Content>

<asp:Content ID="JavascriptIncludes" ContentPlaceHolderID="JavascriptIncludes" runat="server">
	<script src="<%=ResolveUrl("~/Scripts/jquery-3.3.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery-migrate.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery-ui-1.12.1.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/knockout-3.4.2.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/knockout.mapping-latest.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/knockout.wrap.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jquery.validate.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jQuery.Validator.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/jQuery-Validator.Knockout.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/DSNYFunctions.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/calendar_us.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Scripts/bindinghandlers.js?v=0.04")%>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/Tabs.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CssIncludes" runat="server">
	<link href="<%=ResolveUrl("~/Content/images/jqueryUI/jquery-ui-1.12.1.min.css")%>" rel="stylesheet" type="text/css" />
	<link href="<%=ResolveUrl("~/Content/images/jqueryUI/jquery-ui-1.12.icon-font.min.css")%>" rel="stylesheet" type="text/css" />
	<link href="<%=ResolveUrl("~/Content/calendar.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div id="contentContainer" style="padding:0;display:none;" data-bind="visible: show">
	<h2 data-bind="text: External() ? 'Street Treatment External Pickups' : 'Street Treatment Pickups'"></h2>
	<p  style="width: 915px;" class="dottedLine"></p>

		<div style="float: left;">
			<div class="iconLeft">
				<a href="" class="iconLink" data-bind="click: cancel">
					<img border="0" src="~/Content/Images/Icons/cancel.png" alt="Close" runat="server" /><br />Close
				</a>
			</div>
            <div class="iconLeft" data-bind="if: Products().length > 0 && Sites().length > 0 && filterAgencies().length > 0">
                <a href="" class="iconLink" data-bind="click: add">
                    <div style="margin-top: 2px"><img border="0" src="~/Content/Images/Icons/add.png" alt="Add Line" runat="server" /></div>
                    <div style="margin-top: 5px">
                        <span>Add Line</span>
                    </div>
                </a>
            </div>
			<div class="iconLeft">
				<a href="#" id="submitButton" class="iconLink" data-bind="click: save">
					<img border="0" src="~/Content/Images/Icons/clipboard_check.png" alt="Save" style="vertical-align: bottom;" runat="server" /><br />Save
				</a>
			</div>
		</div>
		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

		<div id="saved" style="display: none; padding-top: 15px;">Your pickups have been saved.</div>
		<div id="finalized" style="display: none; padding-top: 15px;">Your pickup has been finalized.</div>

		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <div style="float: left; width: 915px">
            <div class="editor-field" style="width: 915px">
            	<div style="display: inline-block;">Pickup Start Date: <input type="input" class="RequestDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { name: 'StartDate' }" /></div>
            	<div style="display: inline-block;">End Date: <input type="input" class="RequestDate" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { name: 'EndDate' }" /></div>
                Site:
                <select data-bind="options: productSites(null, null, 0),
                   optionsValue: 'id',
                   optionsText: 'description',
                   value: selectedSite,
                   optionsCaption: 'All'"></select>
                <input type="checkbox" data-bind="checked: Finalized"/> Show Finalized
                <input type="button" value="Retrieve" data-bind="click: function(){ changeFilter($root.selectedProduct); }" />
            </div>
        </div>

		<div class="clear" style="margin-bottom: 10px;">&nbsp;</div>

        <div data-bind="if: Products().length == 0">There are no active STC Products in site on hand in your chain of command.</div>
        <div data-bind="if: Sites().length == 0">There are no STC Sites with site on hand set up.</div>
        <div data-bind="if: filterAgencies().length == 0 && External()">There are no active Agencies set up that are not designated as internal.</div>
        <div data-bind="if: filterAgencies().length == 0 && !External()">There are no active Agencies set up that are designated as internal.</div>

		<div style="width: 1400px; display: table;" data-bind="jqValidation: validationContext">
			<table data-bind="foreach: StcPickups" style="width: 1500px;">
                <tr data-bind="css: { 'alt': $index() % 2 == 1 }">
					<td>
						<div class="row" style="padding-left: 20px;">
							<div style="width: 5%; display: table-cell; " style="padding-right: 0;border: 1px solid red;">
								<div>
									Site
								</div>
								<div>
									<select data-bind="options: $root.productSites(UserId(), UserName(), ProductId()),
													   optionsValue: 'id',
													   optionsText: 'description',
													   value: UserId, enable: Edit() && !Finalized()"></select>
								</div>
							</div>
							<div style="width: 5%; display: table-cell; " style="padding-right: 0">
								<div>
									Commodity
								</div>
								<div>
									<select data-bind="options: $root.Products,
													   optionsValue: 'Id',
													   optionsText: 'Name',
													   value: ProductId, enable: Edit() && !Finalized()"></select>
								</div>
							</div>
							<div style="width: 5%; display: table-cell; " style="padding-right: 0">
								<div>
									Pickup For
								</div>
								<div data-bind="if: $root.External">
									<select data-bind="foreach: $root.filterAgencies(AgencyId()), value: AgencyId, enable: Edit() && !Finalized()">  
									    <option data-bind="value: Id, text: (Name().length > 15 ? Name().substring(0, 14) + '...' : Name()), attr: { title: Name().length > 15 ? Name() : '' }"></option>
									</select>
								</div>
								<div data-bind="ifnot: $root.External">
									<span data-bind="text: $root.Agencies()[0].Name()"></span>
								</div>
							</div>
							<div style="width: 5%; display: table-cell; ">
								<div>
									Driver Name
								</div>
								<div>
									<input type="text" class="Small validate-finalize" data-bind="value: DriverName, uniqueName: true, enable: Edit() && !Finalized()" data-required="true" maxlength="50" />
								</div>
							</div>
							<div style="width: 5%; display: table-cell; ">
								<div>
									Truck No.
								</div>
								<div>
									<input type="text" class="Small validate-finalize" data-bind="value: TruckNumber, uniqueName: true, enable: Edit() && !Finalized()" data-required="true" maxlength="50" />
								</div>
							</div>
							<div style="width: 5%; display: table-cell; ">
								<div>
									Plate No.
								</div>
								<div>
									<input type="text" class="Small validate-finalize" data-bind="value: PlateNumber, uniqueName: true, enable: Edit() && !Finalized()" data-required="true" maxlength="15" />
								</div>
							</div>
							<div style="width: 7%; display: table-cell; ">
								<div>
									Pickup Date
								</div>
								<div>
									<input type="input" class="StartDate validate-finalize" class="tcalReadOnly" readonly="true" size="10" style="width: 70px;" data-bind="tcal: { onlyIf: !Finalized(), name: 'PickupDate', validate: $root.validateDate }, uniqueName: true" data-required="true"  />
								</div>
							</div>
							<div style="width: 5%; display: table-cell; ">
								<div>
									Time
								</div>
								<div>
									<input type="text" class="Small validate-finalize" data-bind="timeFormat: PickupTime, enable: Edit() && !Finalized()" data-required="true" maxlength="15" />
								</div>
							</div>
							<div style="width: 5%; display: table-cell; ">
								<div>
									Qty
								</div>
								<div>
									<input type="text" class="Small validate-finalize" data-bind="money, value: Qty, uniqueName: true, enable: Edit() && !Finalized()" data-required="true" />
								</div>
							</div>
							<div style="width: 10%; display: table-cell; white-space:nowrap">
								<div>
									Dump Super
								</div>
								<div style="white-space:nowrap">
									<input type="text" class="Small validate-finalize" data-bind="value: DistrictSuperName, uniqueName: true, enable: Edit() && !Finalized()" maxlength="50"/>
									<input type="text" class="Small validate-finalize " data-bind="value: DistrictSuperBadge, uniqueName: true, enable: Edit() && !Finalized()" maxlength="20"/>
								</div>
								<div style="display: inline-block; width: 102px;">
									Name
								</div>
								<div style="display: inline-block; width: 102px;">
									Badge
								</div>
							</div>
							<div style="width: 10%; display: table-cell; white-space:nowrap">
								<div>
									Boro Super
								</div>
								<div style="white-space:nowrap">
									<input type="text" class="Small validate-finalize" data-bind="value: BoroSuperName, uniqueName: true, enable: Edit() && !Finalized() && $root.BoroSignable()" maxlength="50"/>
									<input type="text" class="Small validate-finalize" data-bind="value: BoroSuperBadge, uniqueName: true, enable: Edit() && !Finalized() && $root.BoroSignable()" maxlength="20"/>
								</div>
								<div style="display: inline-block; width: 102px;">
									Name
								</div>
								<div style="display: inline-block; width: 102px;">
									Badge
								</div>
							</div>
    <!-- ko if: $root.Finalizable() && !Finalized() -->
							<div style="width: 5%; display: table-cell;">
								<div>&nbsp;</div>
								<div>
									<input type="button" class="Small validate-finalize" data-bind="click: $root.finalize, enable: Edit() && !Finalized()" value="Finalize" />
								</div>
							</div>
    <!-- /ko -->
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PrintArea" runat="server">
	<div id="printArea"></div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="JavascriptSource" runat="server">
	<script type="text/javascript">
		var obj = JSON.parse('<%= this.Model.Json %>');
		function findFirstDiffPos(a, b)
		{
		   var shorterLength = Math.min(a.length, b.length);

		   for (var i = 0; i < shorterLength; i++)
		   {
		       if (a[i] !== b[i]) return i;
		   }

		   if (a.length !== b.length) return shorterLength;

		   return -1;
		}
		obj.cancel = function() {
			var currentViewModelString = JSON.stringify(ko.wrap.toJS(vm.StcPickups));
			if (viewModelString != currentViewModelString){
				if (!confirm("There are unsaved changes.  Are you sure you want to continue?")) return false;
			}
			history.go(-1); 
			return false;
		}
        obj.add = function () { this.StcPickups.push(ko.wrap.fromJS(ko.wrap.toJS(this.BlankStcPickup))); }
        obj.edit = function (item) { item.Edit(!item.Edit()); }
		obj.changeFilter= function(){
			var currentViewModelString = JSON.stringify(ko.wrap.toJS(vm.StcPickups));
			if (viewModelString != currentViewModelString){
				if (!confirm("There are unsaved changes.  Are you sure you want to continue?")) return false;
			}
			vm.refresh();
		}
		obj.getRelativeUrl = function(relativeLocation){
			var location = window.location.toString();
			location = location.substring(0, location.indexOf('StcPickup') + 9) + relativeLocation;
			return location;
		}
		obj.refresh = function(){
			var url = vm.getRelativeUrl('/ChangeFilter/');
			if (typeof product == undefined) { product = vm.selectedProduct; }
			var viewModel = { External: ko.toJS(vm.External), StartDate: ko.toJS(vm.StartDate), EndDate: ko.toJS(vm.EndDate), selectedSite: ko.toJS(vm.selectedSite), Finalized: ko.toJS(vm.Finalized), Finalizable: ko.toJS(vm.Finalizable) };
			$.post({ url: url, 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				data: JSON.stringify(viewModel),
				success: function(data){
					ko.wrap.updateFromJS(vm.StcPickups, data.StcPickups);
					viewModelString = JSON.stringify(ko.wrap.toJS(vm.StcPickups));
				},
				error: function(err){
					console.log(JSON.stringify(err));                    
				}
			});
		}
		obj.toggle = function (item) { if (!item.Active()){ item.Qty(null); item.Capacity(null); this.deletes().push(item); } }
		obj.deletes = [];
		obj.delete = function () {
			var self = this;
			if (confirm('Are you sure that you want to delete this zone and all the selected sites in the zone?')){
				$.post({ url: window.location.toString() + '/Delete/' + self.selectedZone(), 
					type: 'post', 
					contentType: 'application/json; charset=utf-8',
					success: function(data){
						if (data != "Success"){
							alert(data);
						}
						self.refresh();
						console.log(JSON.stringify(data));
					},
					error: function(err){
						console.log(JSON.stringify(err));                    
					}
				})
			}
		}
		obj.save = function () {
            if ($('.invalid-validation-error').length > 0){
                alert('There are invalid fields.  Please address them before saving.');
                return false;
            }
            var validationResult = vm.validationContext.Validate();
            if (!validationResult.valid) {
                vm.errors(validationResult.messages);
                alert('There are required fields missing.');
                return false;
            }

			var self = this;
			var viewModel = { values: ko.toJS(self.StcPickups) };

			$.post({ url: vm.getRelativeUrl('/Save'), 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				//traditional: true,
				data: JSON.stringify(viewModel), 
				success: function(data){
					if (data != "Success"){
						alert(data);
						return false;
					}
					viewModelString = JSON.stringify(ko.wrap.toJS(vm.StcPickups));
					$('#saved').show();
					setTimeout(function(){
						$('#saved').hide();
					}, 5000);
					self.refresh();
					console.log(JSON.stringify(data));
				}
			}).fail(function(err){
				console.log(JSON.stringify(err));       
			});
		}
		obj.validateDate = function(dt){
			var current = new Date();
			return dt <= current.valueOf();
		}
		obj.finalize = function (d, e) {
			var errorMsg = "";
			var dt = new Date(d.PickupDate() + ' ' + d.PickupTime());
			var current = new Date();
			if (dt.valueOf() > current.valueOf()){
				alert('The date and time cannot be in the future.');
				return false;
			}

			if ($('.invalid-validation-error').length > 0){
				alert('There are invalid fields.  Please address them before saving.');
				return false;
			}
			var validationResult = vm.validationContext.Validate();
			if (!validationResult.valid) {
				vm.errors(validationResult.messages);
				errorMsg += 'There are required fields missing.  Please address issues with required fields before finalizing any pickups.\n';
			}
			if (errorMsg != ""){
				alert(errorMsg);
				return false;
			}
			// var val = true;
			// $(e.target).closest(".row").find(":input").each(function(i, item){
			// 	if($(item).data('required') && !$(item).val()) {
			// 		val = false;
			// 	}
			// })
			// if (!val) {
   //          	alert('There are required fields missing.');
			// 	return false;
			// }
			var self = vm;
			d.Finalized(true);
			$('#finalized').show();
			setTimeout(function(){
				$('#finalized').hide();
			}, 5000);
			return;

			var viewModel = { value: ko.toJS(d) };

			$.post({ url: window.location.toString() + '/Finalize', 
				type: 'post', 
				contentType: 'application/json; charset=utf-8',
				dataType: 'json',
				//traditional: true,
				data: JSON.stringify(viewModel), 
				success: function(data){
					if (data != "Success"){
						alert(data);
						return false;
					}
					viewModelString = JSON.stringify(ko.wrap.toJS(vm.StcPickups));
					$('#finalized').show();
					setTimeout(function(){
						$('#finalized').hide();
					}, 5000);
					console.log(JSON.stringify(data));
				}
			}).fail(function(err){
				console.log(JSON.stringify(err));       
			});
		}
		obj.show = true;
		var vm = ko.wrap.fromJS(obj);
		vm.filterAgencies = function(agencyId) {
			return ko.computed(function () {
				var self = vm;
				return ko.utils.arrayFilter(self.Agencies(), function (item) {
					return item.Active() || item.Id() == agencyId;
				});
			})();
		}
		vm.productSites = function(Id, Name, productId) {
			return ko.computed(function () {
				var self = vm;
				var exists = false;  
				var sites = ko.utils.arrayFilter(self.Sites()[productId](), function (item) {
					if (item.id() == Id){ exists = true; }
					return true;
				});
				if (!exists && Name != null){
					sites.push({id: ko.observable(Id), description: ko.observable(Name), visible: ko.observable(true)});
				}
				return sites;
			})();
		};

		vm.validationContext = ko.jqValidation({
			returnBool:false, // We want more details of our validation result.
			useInlineErrors: true, // Use inline errors
            errorClass: 'input-validation-error', // Apply error class
            msg_empty: '', // Global empty message.
			noInlineErrors: "*[type='password']" // Password fields should not show inline errors.
		});        
	
		vm.errors = ko.observableArray([]);
		var viewModelString;
		$(function () {
	        //$.validator.addMethod("DistrictSuperName", $.validator.methods.required, "The name of the Game must be specified");
			$.validator.addClassRules("validate-finalize", { DistrictSuperName: true, DistrictSuperBadge: true });

			ko.applyBindings(vm, $('#contentContainer')[0]);
			viewModelString = JSON.stringify(ko.wrap.toJS(vm.StcPickups));
		});
	</script>
</asp:Content>
