﻿var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

$(document).ready(function () {
    // on print button click, do a bunch of stuff including ajax
    $("#printButton").click(function () {
        $("#printArea").html("");

        var checkedCount = $(".printMessage:checked").length;
        var checkedCompleted = 0;
        var checkedIds = "";

        if (checkedCount > 0) {
            $(".printMessage:checked").each(function (index) {
                if (checkedIds == "") {
                    checkedIds = $(this).val();
                }
                else {
                    checkedIds += "," + $(this).val();
                }
            });

            var data = { ids: checkedIds };

            $.ajax({
                type: "GET",
                async: false,
                url: "/Message/GetMessages/",
                data: data,
                dataType: "json",
                success: function (data) {
                    $.each(data, function (index, messageData) {
                        if (checkedCompleted > 0) {
                            $("#printArea").append("<hr class='print-divider' />");
                        } else {
                            $("#printArea").append("<br />");
                        }

                        if (messageData.deptMsgNo != '') {
                            $("#printArea").append("<div class='print-label'>Dept Msg #: </div><div class='print-field'>" + messageData.deptMsgNo +
                                            "</div>");
                        }

                        if (messageData.sentBy != '') {
                            $("#printArea").append("<div class='print-label'>Sent By: </div><div class='print-field'>" + messageData.sentBy +
                                            "</div><div class='clear'></div>");
                        }

                        $("#printArea").append("<div class='print-label'>Date: </div><div class='print-field'>" + messageData.messageDate +
                                        "</div>");
                        $("#printArea").append("<div class='print-label'>Code: </div><div class='print-field'>" + messageData.code +
                                        "</div><div class='clear'></div>");
                        $("#printArea").append("<div class='print-label'>Subject: </div><div class='print-field' style='width: 460px;'>"
                                        + messageData.subject + "</div><div class='clear'></div>");
                        $("#printArea").append("<div class='print-text'><pre style='white-space: pre-wrap;'>"
                                        + messageData.messageText + "</pre></div>");
                        $("#printArea").append("<div class='clear'></div>");

                        checkedCompleted++;
                    });
                }
            });

            if (checkedCompleted == checkedCount) {
                window.print();
            }
        }
    });
});

// function to sort the listbox when things are add/removed from it
function SortListbox(listBox) {
    var $r = $("#" + listBox + " option");
    $r.sort(function (a, b) {
        if (a.text.toLowerCase() < b.text.toLowerCase()) return -1;
        if (a.text.toLowerCase() == b.text.toLowerCase()) return 0;
        return 1;
    });

    $($r).remove();
    $("#" + listBox).append($($r));
};

// Restripes a table to correct class
function reStripe(tableName) {
    $("#" + tableName + " tr:visible").each(function (index) {
        if (index > 0) {
            if (index % 2 == 0) {
                $(this).attr("class", "alt");
            }
            else {
                $(this).attr("class", "");
            }
        }
    });
}

// Preloads images based on an array
function preload(arrayOfImages) {
    $(arrayOfImages).each(function () {
        (new Image()).src = this
        // Alternatively you could use:
        //$('<img/>')[0].src = this;
    });
}

// Checks if the passed in string is a number
function IsNumeric(sText) {
    var ValidChars = "0123456789.";
    var IsNumber = true;
    var Char;


    for (i = 0; i < sText.length && IsNumber == true; i++) {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1) {
            IsNumber = false;
        }
    }
    return IsNumber;
}

/*function helpPopup(helpFile) {
    window.open("/Content/help/help.htm?id=" + helpFile, "helpWindow", "statusbar=0,resizable=1,location=0,toolbar=0,menubar=0,height=820,width=1360")
}*/

function pasteIntoInput(el, text) {
    el.focus();

    if (typeof el.selectionStart == "number") {
        var val = el.value;
        var selStart = el.selectionStart;
        el.value = val.slice(0, selStart) + text + val.slice(el.selectionEnd);
        el.selectionEnd = el.selectionStart = selStart + text.length;
    } else if (typeof document.selection != "undefined") {
        var textRange = document.selection.createRange();
        textRange.text = text;
        textRange.collapse(false);
        textRange.select();
    }
}

function getUrlPath() {
    return window.location.pathname;
}

function animateText(id, animator, numOfAnimators, speed) {
    var text = $("#" + id).text();

    setInterval(function () {
        var numOfItems = ($("#" + id).text().match(new RegExp("\\" + animator, "gi")) || []).length;
        var itemsToAdd = "";

        for (var i = 0; i < (numOfItems < (numOfAnimators || 3) ? numOfItems + 1 : 0) ; i++) {
            itemsToAdd += animator;
        }

        $("#" + id).text(text + itemsToAdd );
    }, speed || 750);
}

function animateDateTime(id) {
    $("#" + id).text(getCurrentDateTimeString());

    setInterval(function () {
        $("#" + id).text(getCurrentDateTimeString());
    }, 20000);
}

function getCurrentDateTimeString() {
    var date = new Date(Date.now());

    return days[date.getDay()] + ', ' + months[date.getMonth()] + ' ' + date.getDate() 
        + ', ' + date.getFullYear() + ' ' + date.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
}