﻿//Tab Function
$.fn.tabs = function () {
    return this.each(function () {
        var container = this;

        //Setup
        $(".tabContent:gt(0)", this).each(function () {
            $(this).hide();
        });

        //Tab Switching
        $("#tabnav > li > a").click(function (event) {
            event.preventDefault();
            $(container).find(".tabContent:visible").hide();
            $(container).find(".tabContent").eq($(this).parent().prevAll().length).show();

            $("#tabnav").find("li:visible").removeClass("selectedTab");
            $(this).parent().addClass("selectedTab");
        });
    });
};