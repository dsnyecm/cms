ko.bindingHandlers.tcal = {
	init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
		var options = allBindingsAccessor().attr || {},
			tcalOptions = allBindingsAccessor().tcal || {},
			$el = $(element);

		var id = tcalOptions.data.name;
		var required = tcalOptions.data.required;
		var onlyIf = tcalOptions.data.onlyIf;
		var validate = tcalOptions.data.validate;
		if (typeof bindingContext.$index == 'function'){
			id += '_' + bindingContext.$index();
		}
		$el.attr("id", id);
		$el.val(viewModel[tcalOptions.data.name]());
		var cal = '<img src="/content/images/calendar/cal.gif" id="tcalico_' + id +'" class="tcalIcon" alt="Open Calendar" title="Open Calendar">';
		var clear = '<a class="pointer tcalIcon-delete" title="Clear Date">X</a>';

		$el.on('change', function(value){
			viewModel[tcalOptions.data.name](value.target.value);
		})

		if (typeof onlyIf == 'undefined' || onlyIf){
			var iconCal = $(cal).appendTo($el.parent());
			$(iconCal).on('click', function(){
				if ($el.val() == "Required") { 
							bindingContext.$root.errors = ko.observableArray([]);
					$el.removeClass("error");
					A_TCALS[id].f_clear();
				}
				A_TCALS[id].f_toggle();
			});

			if (!required){
				var iconClear = $(clear).appendTo($el.parent());
				$(iconClear).on('click', function(){
					viewModel[tcalOptions.data.name](undefined);
					A_TCALS[id].f_clear();
				});
			}

			//initialize datepicker with some optional options
			new tcal({ 'controlname': id, 'id': id, 'icon': iconCal, 'validate': validate });
		} else {
			$el.prop( "disabled", true );
		}
	},
	preprocess: function(value, bindingName) {
		return '{data:' + value + ', name:\'' + bindingName + '\'}';
	}
};

ko.bindingHandlers.money = {
	init: function (element, valueAccessor) {
		$(element).on("keydown", function (event) {
			return numeric(event, function(){ keydown2decimal(element); });
		});
	}
};

ko.bindingHandlers.numeric = {
	init: function (element, valueAccessor) {
		$(element).on("keydown", function (event) {
			return numeric(event);
		});
	}
};

ko.bindingHandlers.striped = {
    update: function (element, valueAccessor) {
        var value = ko.unwrap(valueAccessor());
        var el = $(element);

        el.children('tr').each(function (index) {
            if (index % 2) {
                $(this).addClass('alt');
            }
        });
    }
};

function numeric(event, callback){
	// Allow: backspace, delete, tab, escape, and enter
	if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
		// Allow: Ctrl+A
		(event.keyCode == 65 && event.ctrlKey === true) ||
		// Allow: . ,
		(event.keyCode == 190 || event.keyCode == 110) ||
		// Allow: home, end, left, right
		(event.keyCode >= 35 && event.keyCode <= 39)) {
		// let it happen, don't do anything
		return true;
	}
	else {
		// Ensure that it is a number and stop the keypress
		if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
			event.preventDefault();
		}
	}
	if (typeof callback === 'function'){
		callback();
	}
}

String.prototype.splice = function(idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};

function keydown2decimal(el){	
	var value = event.target.value;
	var newvalue = value.splice(el.selectionStart, 0, event.key);
	var ex = /^([0-9]+)?[\.]?[0-9]?[0-9]?$/;
	if(ex.test(newvalue)==false){
		event.preventDefault();
	}
}

var allowedInput = function(length, regex) {
	return {
		init: function(element, valueAccessor, allBindingsAccessor, bindingContext) {
			ko.bindingHandlers.textInput.init(element, valueAccessor, allBindingsAccessor, bindingContext);
		},

		update: function(element, valueAccessor) {
			var value = ko.unwrap(valueAccessor());
			if(value){
				if (value.length > length || !regex.test(value)) {
					$(element).addClass('input-validation-error');
					$(element).addClass('invalid-validation-error');
				}
				if (value.length <= length && regex.test(value)) {
					$(element).removeClass('input-validation-error');
					$(element).removeClass('invalid-validation-error');
				}
			} else { $(element).removeClass('input-validation-error');
					$(element).removeClass('invalid-validation-error'); }
		}
	}
};

ko.bindingHandlers.timeFormat = allowedInput(8, new RegExp(/^((0?[0-9])|(1[0-9])|(2[0-3])):([0-5])([0-9])$/));


