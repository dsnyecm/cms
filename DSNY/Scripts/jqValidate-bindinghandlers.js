(function ($, ko) {
    // Make sure JQV is loaded.
    if (!$.fn.validate) throw new Error("I can't seem to locate jQuery-validator-plugin. Has it been loaded?");

    ko.bindingHandlers.jqValidationForm = {
        init: function (element, valueAccessor) {
            var $elem = $(element),
                  options = ko.mapping.toJS(valueAccessor());

            options = $.extend({
                ignore: ".ignore",
                errorClass: 'input-validation-error',
                errorPlacement: function (error, element) {
                    error.hide();
                    return false;
                },
                onkeyup: function (el, ev) {
                    if ('submitEnable' in options) {
                        options.submitEnable();
                    }
                    return true;
                },
                onclick: function (el, ev) {
                    if ('submitEnable' in options) {
                        options.submitEnable();
                    }
                    return true;
                }
            }, options);

            $elem.validate(options);
        }
    };

    ko.bindingHandlers.jqValidationElement = {
        init: function (element, valueAccessor) {
            var el = $(element);
            var value = valueAccessor();
            var valueUnwrapped = ko.unwrap(value);

            if (valueUnwrapped === true) {
                el.rules('add', { required: true, messages: { required: '' } });
            }
            
            el.valid();
        }
    };

    ko.bindingHandlers.jqValidationTrigger = {
        update: function (element, valueAccessor, allBindings) {
            var value = valueAccessor();
            var valueUnwrapped = ko.unwrap(value);
            var el = $(element);
            var parentElType = allBindings.get('jqValidationTriggerParent') || 'tr';
            var parentEl = el.closest(parentElType);
            var disableValues = allBindings.get('jqValidationTriggerDisableValues') || [];

            // find all inputs in row and then enable/disable validation
            parentEl.find('input, select, textarea').each(function () {
                var input = $(this);

                if (input.attr('data-bind').indexOf('jqValidationElement') > -1 && !input.is(':disabled') && this != el.get(0)) {
                    if (valueUnwrapped == undefined || disableValues.indexOf(valueUnwrapped) > -1) {
                        input.rules('remove');
                    } else {
                        input.rules('add', { required: true, messages: { required: '' } });
                    }
                    
                    input.valid();
                }
            });
        }
    };
})(jQuery, ko);
