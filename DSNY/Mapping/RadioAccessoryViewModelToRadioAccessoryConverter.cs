﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DSNY.Data;
using DSNY.ViewModels;

namespace DSNY.Mapping
{
    public class RadioAccessoryViewModelToRadioAccessoryConverter:ITypeConverter<RadioAccessoryViewModel,Radio_Accessory>
    {
        public Radio_Accessory Convert(RadioAccessoryViewModel source,Radio_Accessory destination,  ResolutionContext context)
        {
            if (destination == null)
                destination = new Radio_Accessory();
            
            destination.Part_Number = source.PartNumber;
            destination.Radio_Make_id = source.MakeId;         
            destination.Radio_Equipment_Type_id = source.EquipmentTypeId;
            destination.Description = source.Description;
            destination.Userid = source.UserId;
            destination.Current_Qty = source.CurrentQty;
            destination.Is_Spare = source.IsSpare;
            return destination;
        }
    }
}