using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Mapping
{
    public class MeasurementConverter : ITypeConverter<DSNY.Data.Measurement, Measurement>
    {
        public Measurement Convert(DSNY.Data.Measurement source, Measurement destination, ResolutionContext context)
        {
            return new Measurement
            {
                Id = source.Measurement_id,
                Name = source.Measurement_Name,
                Active = source.is_active.HasValue ? source.is_active.Value : true,
                Stc = source.is_STC.HasValue ? source.is_STC.Value : false,
                Tool = source.is_Tool.HasValue ? source.is_Tool.Value : false,
                Deleted = false
            };
        }
    }

}
