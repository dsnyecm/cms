using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Mapping
{
    public class StcAgencyConverter : ITypeConverter<STC_Agency, StcAgency>
    {
        public StcAgency Convert(STC_Agency source, StcAgency destination, ResolutionContext context)
        {
            return new StcAgency
            {
                Id = source.STC_Agency_id,
                Name = source.Agency_Name,
                Code = source.Agency_cd,
                Address = source.Agency_Address,
                Email = source.Agency_Email,
                Phone = source.Agency_Phone,
                Active = source.Is_Active.HasValue ? source.Is_Active.Value : false,
                Billable = source.Is_Billable,
                Deleted = false
            };
        }
    }

}
