﻿using AutoMapper;
using DSNY.Data;
using DSNY.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.Mapping
{
    public class RadioAccessoryToRadioAccessoryViewModelConverter : ITypeConverter<Radio_Accessory, RadioAccessoryViewModel>
    {
        public RadioAccessoryViewModel Convert(Radio_Accessory source, RadioAccessoryViewModel destination, ResolutionContext context)
        {
            if (destination == null)
                destination = new RadioAccessoryViewModel();

            destination.Id = source.Radio_Accessory_id;
            destination.PartNumber = source.Part_Number;
            destination.MakeId = source.Radio_Make_id;
            destination.LastDistInvQty = source.Last_Dist_Inv_Qty;
            if(source.Last_Dist_Inv_Date!=null)
                destination.LastDistInvDate = source.Last_Dist_Inv_Date.Value.ToShortDateString();

            destination.EquipmentTypeId = source.Radio_Equipment_Type_id;
            destination.Description = source.Description;
            destination.UserId = source.Userid;
            destination.CurrentQty = source.Current_Qty;
            destination.IsSpare = source.Is_Spare.GetValueOrDefault();
            if (source.Inventory_Request_Date != null)
                destination.InventoryRequestDate = source.Inventory_Request_Date.Value;

            return destination;
        }
    }
}