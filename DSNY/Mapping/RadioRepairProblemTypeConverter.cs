﻿using AutoMapper;
using DSNY.Data;
using DSNY.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.Mapping
{
    public class RadioRepairProblemTypeConverter : ITypeConverter<Radio_Repair_Problem_Type, RepairProblemTypeModel>
    {
        public RepairProblemTypeModel Convert(Radio_Repair_Problem_Type source, RepairProblemTypeModel destination, ResolutionContext context)
        {
            return new RepairProblemTypeModel
            {
                Type = source.Repair_Problem_Name,
                ID = source.Radio_Repair_Problem_id,
                IsActive = source.is_Active.GetValueOrDefault()

            };
        }
    }
}