﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Mapping
{
    public class StcPurchaseOrderConverter : ITypeConverter<STC_Purchase_Order, StcPurchaseOrder>
    {
        public StcPurchaseOrder Convert(STC_Purchase_Order source, StcPurchaseOrder destination, ResolutionContext context)
        {
            var zones = source.Zones.ToList();
            return new StcPurchaseOrder
            {
                Id = source.STC_Purchase_Orders_id,
                ProductId = source.STC_Product_id,
                PurchaseOrderNumber = source.Purchase_Order_Num,
                Amount = source.Order_Qty_Amount.ToString(),
                AmountRemaining = source.Current_Order_Qty_Amount.ToString(),
                AmountRemainingForDelivery = source.Current_Delivery_Qty_Amount.ToString(),
                VendorId = source.Vendor_Id,
                Vendor = source.Vendor.Vendor_Name,
                ZoneId = zones.Any() ? zones[0].Zone_id : (int?)null,
                Zone2Id = zones.Count > 1 ? zones[1].Zone_id : (int?)null,
                Zone = zones.Any() ? zones[0].Zone_Name : null,
                StartDate = source.PO_Start_Date.HasValue ? source.PO_Start_Date.Value.ToShortDateString() : "",
                EndDate = source.PO_Complete_Date.HasValue ? source.PO_Complete_Date.Value.ToShortDateString(): "",
                CompleteDate = source.PO_Complete_Date.HasValue ? source.PO_Complete_Date.Value.ToShortDateString() : "",
                New = false,
                Deleted = false
            };
        }
    }

}