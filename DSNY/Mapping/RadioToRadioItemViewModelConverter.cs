﻿using AutoMapper;
using DSNY.Data;
using DSNY.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.Mapping
{
    public class RadioToRadioItemViewModelConverter : ITypeConverter<DSNY.Data.Radio, DSNY.ViewModels.RadioItemViewModel>
    {
        public RadioItemViewModel Convert(Radio source, RadioItemViewModel destination, ResolutionContext context)
        {
            if(destination==null)
                destination = new RadioItemViewModel();
            destination.Id = source.Radio_id;
            destination.DSNYRadioId = source.DSNY_Radio_id;
            destination.AssignedToVehicle = source.Assigned_To_Vehicle;
            destination.InventoryDate = source.Inventory_Date?.ToString("MM/dd/yyyy");
            destination.IsActive = source.is_Active.GetValueOrDefault(true);
            destination.IsSpare = source.is_Spare.GetValueOrDefault();
            destination.IssueToUserId = source.Issued_To_Userid;           
            destination.SerialNumber = source.Radio_Serial_Number;
            destination.ServiceDate = source.Service_Date?.ToString("MM/dd/yyyy");
            destination.StatusId = source.Radio_Status_id;
            destination.RadioEquipmentTypeId = source.Radio_Equipment_Type_id;
            destination.RadioMakeId = source.Radio_Make_id;
            destination.RadioModelId = source.Radio_Model_id;
            destination.TrunkId = source.Trunk_id;
            destination.LastDistInvStatusId = source.Last_Dist_Inv_Status_id;
            if (source.Last_Dist_Inv_Date != null)
                destination.LastDistInventoryDate = source.Last_Dist_Inv_Date.Value.ToShortDateString();

            destination.IsInventoryRequested = source.Is_Inventory_Requested.GetValueOrDefault();
            if(source.Inventory_Request_Date!=null)
                destination.InventoryRequestDate = source.Inventory_Request_Date.Value;

            destination.Comments = source.Last_Comments;
            return destination;
        }
    }
}