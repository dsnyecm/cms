﻿using AutoMapper;
using DSNY.Data;
using DSNY.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.Mapping
{
    public class RadioModelConverter : ITypeConverter<Radio_Model, RadioModelVM>
    {
        public RadioModelVM Convert(Radio_Model source, RadioModelVM destination, ResolutionContext context)
        {
            return new RadioModelVM
            {
                ID = source.Radio_Model_id,
                Name = source.Model_Name,
                IsActive = source.is_Active.GetValueOrDefault()

            };
        }
    }
}