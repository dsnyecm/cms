﻿using AutoMapper;
using DSNY.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.Mapping
{
    public class RadioToRadioHistoryConverter : ITypeConverter<Radio, Radio_History>
    {
        public Radio_History Convert(Radio source, Radio_History destination, ResolutionContext context)
        {
            if (source == null)
                return null;

            if (destination == null)
                destination = new Radio_History();

            destination.Radio_id = source.Radio_id;
            destination.DSNY_Radio_id = source.DSNY_Radio_id;
            destination.Assigned_To_Vehicle = source.Assigned_To_Vehicle;
            destination.Inventory_Date = source.Inventory_Date;
            destination.is_Active = source.is_Active;
            destination.is_Spare = source.is_Spare;
            destination.Issued_To_Userid = source.Issued_To_Userid;
            destination.Last_Dist_Inv_Date = source.Last_Dist_Inv_Date;
            destination.Last_Dist_Inv_Status_id = source.Last_Dist_Inv_Status_id;
            destination.Radio_Serial_Number = source.Radio_Serial_Number;
            destination.Service_Date = source.Service_Date;
            destination.Radio_Status_id = source.Radio_Status_id;
            destination.Radio_Equipment_Type_id = source.Radio_Equipment_Type_id;
            destination.Radio_Make_id = source.Radio_Make_id;
            destination.Radio_Model_id = source.Radio_Model_id;
            destination.Trunk_id = source.Trunk_id;
            destination.ChangeDate = DateTime.Now;

            return destination;
        }
    }
}