﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Mapping
{
    public class ZoneConverter : ITypeConverter<DSNY.Data.Zone, Zone>
    {
        public Zone Convert(DSNY.Data.Zone source, Zone destination, ResolutionContext context)
        {
            return new Zone
            {
                Id = source.Zone_id,
                Name = source.Zone_Name,
                Deleted = false
            };
        }
    }

}