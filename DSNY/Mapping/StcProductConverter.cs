﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Mapping
{
    public class StcProductConverter : ITypeConverter<STC_Product, StcProduct>
    {
        public StcProduct Convert(STC_Product source, StcProduct destination, ResolutionContext context)
        {
            return new StcProduct
            {
                Id = source.STC_Product_id,
                Name = source.STC_Product_Name,
                Measurement = source.Measurement.Measurement_Name,
                LoadSize = source.LoadSize
            };
        }
    }

}