using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Mapping
{
    public class TruckTypeConverter : ITypeConverter<Truck_Type, TruckType>
    {
        public TruckType Convert(Truck_Type source, TruckType destination, ResolutionContext context)
        {
            return new TruckType
            {
                Id = source.STC_Truck_Type_id,
                Name = source.Truck_Type_Name,
                Capacity = source.Truck_Capacity,
                Active = source.is_active.HasValue ? source.is_active.Value : true,
                Deleted = false
            };
        }
    }

}
