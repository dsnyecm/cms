﻿using AutoMapper;
using DSNY.Data;
using DSNY.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.Mapping
{
    public class RadioItemToRadioConverter : ITypeConverter<DSNY.ViewModels.RadioItemViewModel,DSNY.Data.Radio>
    {
        public Radio Convert(RadioItemViewModel source, Radio destination,  ResolutionContext context)
        {
            if(destination==null)
                destination = new Radio();

            destination.Radio_id = source.Id;
            destination.DSNY_Radio_id  = source.DSNYRadioId;
            destination.Assigned_To_Vehicle= source.AssignedToVehicle ;
            if(source.InventoryDate!=null)
            {
                destination.Inventory_Date = DateTime.Parse(source.InventoryDate+" 12:00 AM");
            }
            
            destination.is_Active = source.IsActive;
            destination.is_Spare = source.IsSpare;
            destination.Issued_To_Userid = source.IssueToUserId;           
            destination.Radio_Serial_Number= source.SerialNumber;
            if (source.ServiceDate != null)
            {
                destination.Service_Date = DateTime.Parse(source.ServiceDate+" 12:00 AM");
            }
            destination.Radio_Status_id = source.StatusId;           
            destination.Radio_Equipment_Type_id = source.RadioEquipmentTypeId;
            destination.Radio_Make_id = source.RadioMakeId;
            destination.Radio_Model_id = source.RadioModelId;
            destination.Trunk_id = source.TrunkId;
            destination.is_Down = false;
            destination.is_Disposed = false;
            destination.Last_Comments = source.Comments;
            return destination;
        }
    }
}