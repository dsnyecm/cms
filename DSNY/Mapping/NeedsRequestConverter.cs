﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Mapping
{
    public class NeedsRequestConverter : ITypeConverter<STC_Needs_Request, NeedsRequest>
    {
        private DSNYContext _context;
        public NeedsRequestConverter(DSNYContext IoCContext)
        {
            _context = IoCContext;
        }
        public NeedsRequest Convert(STC_Needs_Request source, NeedsRequest destination, ResolutionContext context)
        {
            var ret = new NeedsRequest
            {
                Id = source.STC_Needs_Request_id,
                UserId = source.Userid.ToString(),
                User = source.aspnet_Users.UserName,
                RequestDate = source.Request_Date.HasValue ? source.Request_Date.Value.ToShortDateString() : "",
                ConfirmDate = source.Confirm_Date.HasValue ? source.Confirm_Date.Value.ToShortDateString() : "",
                ProductId = source.STC_Product_id,
                Product = source.STC_Product.STC_Product_Name,
                Supervisor = source.Boro_Super_Name,
                Badge = source.Boro_Super_Badge,
                Qty = source.Request_Qty.ToString(),
                New = false,
                Deleted = false,
                Products = Mapper.Map<List<Dropdown>>(_context.DataContext.STC_Product_User.Where(x => x.STC_User_ID == source.Userid)
                    .Select(x => new { id = x.STC_Product_ID, description = x.STC_Product.STC_Product_Name, visible = true }).ToList())
            };
            if (!ret.Products.Select(x => int.Parse(x.id)).Contains(ret.ProductId.Value))
            {
                ret.Products.Add(new Dropdown { id = ret.ProductId.Value.ToString(), description = ret.Product, visible = true });
            }
            return ret;
        }
    }

}