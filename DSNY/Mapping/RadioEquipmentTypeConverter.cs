﻿using AutoMapper;
using DSNY.Data;
using DSNY.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.Mapping
{
    public class RadioEquipmentTypeConverter : ITypeConverter<Radio_Equipment_Type, EquipmentTypeModel>
    {
        public EquipmentTypeModel Convert(Radio_Equipment_Type source, EquipmentTypeModel destination, ResolutionContext context)
        {
            return new EquipmentTypeModel
            {
                DistrictInventory = source.is_District_Inventory.GetValueOrDefault(),
                IsActive = source.is_Active.GetValueOrDefault(),
                Type = source.Equipment_Type_Name,
                ID = source.Radio_Equipment_Type_id,
            };
        }
    }
}