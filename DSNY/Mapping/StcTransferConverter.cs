using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Mapping
{
    public class StcTransferConverter : ITypeConverter<STC_Transfer, StcTransfer>
    {
        public StcTransfer Convert(STC_Transfer source, StcTransfer destination, ResolutionContext context)
        {
            return new StcTransfer
            {
                Id = source.STC_Transfer_Id,
                ProductId = source.STC_Product_id,
                FromUserId = source.From_Userid.ToString(),
                FromUserName = source.aspnet_Users1.UserName,
                ToUserId = source.To_Userid.ToString(),
                ToUserName = source.aspnet_Users.UserName,
                TruckTypeId = source.STC_Truck_Type_id,
                Loads = source.Loads,
                Qty = source.Qty.ToString(),
                TransferDate = source.STC_Transfer_Date.HasValue ? source.STC_Transfer_Date.Value.ToShortDateString() : "",
                TransferTime = source.STC_Transfer_Date.HasValue ? source.STC_Transfer_Date.Value.ToString("HH:mm") : "",
                BoroSuperName = source.Snow_Super_Name,
                BoroSuperBadge = source.Snow_Super_Badge,
                SanSuperName = source.San_Super_Name,
                SanSuperBadge = source.San_Super_Badge,
                Finalized = source.is_finalize.HasValue ? source.is_finalize.Value : false,
                Edit = true,
                Deleted = false
            };
        }
    }

}
