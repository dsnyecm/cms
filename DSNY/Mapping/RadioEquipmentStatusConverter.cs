﻿using AutoMapper;
using DSNY.Data;
using DSNY.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.Mapping
{
    public class RadioEquipmentStatusConverter : ITypeConverter<Radio_Equipment_Status, EquipmentStatusModel>
    {
        public EquipmentStatusModel Convert(Radio_Equipment_Status source, EquipmentStatusModel destination, ResolutionContext context)
        {
            return new EquipmentStatusModel
            {
                Name = source.Equipment_Status_Name,
                ID = source.Radio_Equipment_Status_id,
                Inventory = source.is_Inventory.GetValueOrDefault(),
                WorkFlow = source.is_Workflow.GetValueOrDefault()
            };
        }
    }
}