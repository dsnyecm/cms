using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Mapping
{
    public class StcPickupConverter : ITypeConverter<STC_Pickup, StcPickup>
    {
        public StcPickup Convert(STC_Pickup source, StcPickup destination, ResolutionContext context)
        {
            return new StcPickup
            {
                Id = source.STC_Pickup_id,
                ProductId = source.STC_Product_id.Value,
                AgencyId = source.Agency_Id,
                DriverName = source.Driver_Name,
                TruckNumber = source.Truck_Number,
                PlateNumber = source.Plate_Number,
                UserId = source.aspnet_Users.UserId.ToString(),
                UserName = source.aspnet_Users.UserName,
                PickupDate = source.Pickup_Date.HasValue ? source.Pickup_Date.Value.ToShortDateString() : "",
                PickupTime = source.Pickup_Date.HasValue ? source.Pickup_Date.Value.ToString("HH:mm") : "",
                Qty = source.Qty.ToString(),
                DistrictSuperName = source.District_Super_Name,
                DistrictSuperBadge = source.District_Super_Badge,
                BoroSuperName = source.Boro_Super_Name,
                BoroSuperBadge = source.Boro_Super_Badge,
                Finalized = source.is_finalized.HasValue ? source.is_finalized.Value : false,
                Edit = source.is_finalized.HasValue ? !source.is_finalized.Value : true,
                Deleted = false
            };
        }
    }

}
