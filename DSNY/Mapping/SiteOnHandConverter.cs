﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Mapping
{
    public class SiteOnHandConverter : ITypeConverter<STC_Site_On_Hand, SiteOnHand>
    {
        public SiteOnHand Convert(STC_Site_On_Hand source, SiteOnHand destination, ResolutionContext context)
        {
            return new SiteOnHand
            {
                Id = source.STC_Product_id + "|" + source.UserId.ToString(),
                UserId = source.UserId.ToString(),
                UserName = source.UserName,
                ProductId = source.STC_Product_id,
                ProductName = source.STC_Product_Name,
                Measurement = source.Measurement_Name,
                Active = source.is_Active.HasValue ? source.is_Active.Value : false,
                Qty = source.On_Hand_Qty.ToString(),
                Capacity = source.Capacity,
                Deleted = false
            };
        }
    }

}