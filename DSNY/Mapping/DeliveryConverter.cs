﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Mapping
{
    public class DeliveryConverter : ITypeConverter<STC_Delivery, Delivery>
    {
        public Delivery Convert(STC_Delivery source, Delivery destination, ResolutionContext context)
        {
            var ret = new Delivery
            {
                Id = source.STC_Delivery_id,
                ProductId = source.STC_Product_id.Value,
                PurchaseOrders = Mapper.Map<List<StcPurchaseOrder>>(source.STC_Product.STC_Purchase_Order),
                selectedPurchaseOrder = source.STC_Purchase_Order.STC_Purchase_Orders_id,
                VendorId = source.STC_Purchase_Order.Vendor_Id,
                TicketNumber = source.Ticket_Number,
                TruckNumber = source.Truck_Number,
                PlateNumber = source.Plate_Number,
                selectedSite = source.aspnet_Users.UserId.ToString(),
                selectedSiteName = source.aspnet_Users.UserName,
                DeliveryDate = source.WM_Leave_Date.HasValue ? source.WM_Leave_Date.Value.ToShortDateString() : "",
                LeaveTime = source.WM_Leave_Date.HasValue ? source.WM_Leave_Date.Value.ToString("HH:mm") : "",
                LeaveWeight = source.WM_Qty.ToString(),
                ReceiveTime = source.Boro_Recieved_Date.HasValue ? source.Boro_Recieved_Date.Value.ToString("HH:mm") : "",
                ReceiveWeight = source.Boro_Qty.ToString(),
                WeightMasterName = source.WM_Name,
                WeightMasterBadge = source.WM_Badge_Number,
                BoroSuperName = source.Boro_Super_Name,
                BoroSuperBadge = source.Boro_Super_Badge,
                ReceiveSuperName = source.Recieving_Super_Name,
                ReceiveSuperBadge = source.Recieving_Super_Badge,
                Finalized = source.is_Finalize.HasValue ? source.is_Finalize.Value : false,
                Edit = true,
                Deleted = false
            };
            ret.Sites = new List<Dropdown>();
            foreach(var zone in source.STC_Purchase_Order.Zones)
            {
                ret.Sites.AddRange(Mapper.Map<List<Dropdown>>(zone.aspnet_Users
                    .Select(x => new { id = x.UserId, description = x.UserName, visible = true })));
            }
            return ret;
        }
    }

}