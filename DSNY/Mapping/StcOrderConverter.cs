﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Mapping
{
    public class StcOrderConverter : ITypeConverter<STC_Orders, StcOrder>
    {
        public StcOrder Convert(STC_Orders source, StcOrder destination, ResolutionContext context)
        {
            return new StcOrder
            {
                Id = source.STC_Orders_id,
                Borough = source.Borough.Borough_Name,
                BoroughId = source.Borough_Id,
                UserId = source.Userid.ToString(),
                selectedPurchaseOrder = source.STC_Purchase_Order_Id,
                Qty = source.Order_Qty.ToString(),
                Deleted = false
            };
        }
    }

}