﻿using AutoMapper;
using DSNY.Data;
using DSNY.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DSNY.Mapping
{
    public class RadioMakeConverter : ITypeConverter<Radio_Make, RadioMakeModel>
    {
        public RadioMakeModel Convert(Radio_Make source, RadioMakeModel destination, ResolutionContext context)
        {
            return new RadioMakeModel
            {
                ID = source.Radio_Make_id,
                Name = source.Make_Name,
                IsActive = source.is_Active.GetValueOrDefault()
            };
        }
    }
}