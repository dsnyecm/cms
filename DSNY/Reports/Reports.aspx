﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="DSNY.Mvc.Reports" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>DSNY - Reports</title>
    <link href="../Content/Site.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <div class="page">
        <div id="header">
             <div id="logindisplay" class="whiteLink">
                Welcome <b><%: Page.User.Identity.Name %></b>
                [ 
                <% if (Roles.IsUserInRole("System Administrator")) { %>
                    <a class="menu" href="/Admin">Site Administration</a>  | 
                <% }

                   if (Roles.IsUserInRole("Administrator")) { %>
                    <a class="menu" href="/Dashboard/Admin">Administrator Dashboard</a>  | 
                <% }
       
                   if (Roles.IsUserInRole("Field") || Roles.IsUserInRole("Garage")) { %>
                     <a class="menu" href="/Dashboard/Field">Field Dashboard</a>  | 
                <% } %>
                <a class="menu" href="/Home/LogOff">Log Off</a>  ]
            </div> 
            <div style="float: right; width: 350px;">

            </div>
            <div class="clear"></div>
        </div>
        <div id="left"></div>
        <div id="main">
            <h2>Reports</h2>
            <p style="width: 1115px;" class="dottedLine"></p>
            <form id="ReportForm" runat="server">
                <asp:ScriptManager ID="ScriptManager1" runat="server" />

                <asp:Label ID="lblReportNotFound" Text="Report was not found.  Please select a report from the administration menu." ForeColor="Red" Font-Bold="true" Visible="false" runat="server" />

                <div id="report">
                    <rsweb:ReportViewer ID="ReportViewer" AsyncRendering="true" SizeToReportContent="true" ShowZoomControl="False" ShowParameterPrompts="true" ShowCredentialPrompts="false" 
                        ShowFindControls="true" Width="1100px" Height="600px" runat="server" />
                </div>
            </form>
        </div>
        <div class="clear"></div>
        
        <div id="footer">
        </div>
    </div>
</body>
</html>