﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

using Microsoft.Reporting.WebForms;
using System.Configuration;

namespace DSNY.Mvc
{
    [Serializable]
    public sealed class ReportServerNetworkCredentials : IReportServerCredentials
    {
        private string _userName;
        private string _password;
        private string _domain;
        
        #region IReportServerCredentials Members

        public ReportServerNetworkCredentials(string userName, string password, string domain)
        {
            _userName = userName;
            _password = password;
            _domain = domain;
        }

        public bool GetFormsCredentials(out System.Net.Cookie authCookie, out string userName, out string password, out string authority)
        {
            authCookie = null;
            userName = null;
            password = null;
            authority = null;

            return false;
        }

        public WindowsIdentity ImpersonationUser
        {
            get
            {
                return null;
            }
        }

        public System.Net.ICredentials NetworkCredentials
        {
            get
            {
                return new System.Net.NetworkCredential(_userName, _password, _domain);
            }
        }

        #endregion
    }
}