﻿using System;
using System.Web.Mvc;
using System.Linq;

using DSNY.Core.Interfaces;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Repository.Interfaces.Repositories;
using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;
using System.Collections.Generic;
using System.Web.Security;
using System.Net.Mail;
using Microsoft.Office.Interop.Excel;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using DSNY.Common.Utilities;
using System.Text;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the Radio area
    /// </summary>    
    public class RadioController : BaseController
    {
        IRadioRepository _radioRepository;
        IUserRepository _userRepository;
        DSNYContext _context;
        #region Constructor
        public RadioController(IRadioRepository radioRepository, IUserRepository userRepository, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler, DSNYContext IoCContext) : base(IoCLogger, IoCExceptionHandler)
        {
            _context = IoCContext;
            _radioRepository = radioRepository;
            _userRepository = userRepository;
        }
        #endregion

        #region Actions
        [Authorize(Roles = "Radio Admin, Radio District")]
        public ActionResult Admin()
        {
            return View();
        }

        [Authorize(Roles = "Radio Admin")]
        public ActionResult Codes()
        {
            var model = GetCodesViewModel();
            return View(model);
        }

        [Authorize(Roles = "Radio Admin")]
        public ActionResult GetModelsByMake(int makeId)
        {
            var models = _radioRepository.GetRadioModelsByMakeId(makeId, false);
            var objModels = models.Select(x => new Dropdown { id = x.Radio_Model_id.ToString(), description = x.Model_Name }).ToList();
            return Json(objModels, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Radio Admin")]
        [HttpPost]
        public ActionResult SaveRadios(List<RadioItemViewModel> radios, List<RadioAccessoryViewModel> radioAccessories)
        {
            var ret = ValidateRadiosForSaving(radios, radioAccessories);
            if (ret.Count > 0) // if > 0, it means one or or more validation errors exist
            {
                return Json(new { success = false, errors = Newtonsoft.Json.JsonConvert.SerializeObject(ret) });
            }

            var table = _context.DataContext.Radios;
            foreach (var o in radios)
            {
                if (o.IsDeleted)
                    continue;

                if (o.Id > 0)
                {
                    var row = table.Where(x => x.Radio_id == o.Id).FirstOrDefault();
                    if (row == null)
                    {
                        var updatedRadio = Mapper.Map<Radio>(o);
                        table.AddObject(updatedRadio);
                    }
                    else
                    {
                        // check if radio changed in any way
                        var objOldRadio = Mapper.Map<RadioItemViewModel>(row);
                        if (o.Equals(objOldRadio) == false || (!string.IsNullOrEmpty(o.Comments) && row.Last_Comments!=o.Comments)) // even if user saves after adding some comments, add it to history
                        {
                            var radioHistory = Mapper.Map<Radio_History>(row);
                            radioHistory.Radio_id = o.Id;
                            radioHistory.Comment = o.Comments;
                            _context.DataContext.Radio_History.AddObject(radioHistory);
                        }

                        row = Mapper.Map<RadioItemViewModel, DSNY.Data.Radio>(o, row);
                    }
                }
                else
                {
                    var newRadio = Mapper.Map<Radio>(o);
                    table.AddObject(newRadio);
                }
            }

            if (radioAccessories != null)
            {
                foreach (var accessory in radioAccessories)
                {
                    if (accessory.IsDeleted)
                        continue;

                    if (accessory.Id > 0)
                    {
                        var row = _context.DataContext.Radio_Accessory.Where(x => x.Radio_Accessory_id == accessory.Id).FirstOrDefault();
                        if (row == null)
                        {
                            var newAccessory = Mapper.Map<Radio_Accessory>(accessory);
                            _context.DataContext.Radio_Accessory.AddObject(newAccessory);
                        }
                        else
                        {
                            // check if radio changed in any way
                            var radioAccessory = Mapper.Map<RadioAccessoryViewModel, Radio_Accessory>(accessory, row);
                        }
                    }
                    else
                    {
                        var newRadioAccessory = Mapper.Map<Radio_Accessory>(accessory);
                        _context.DataContext.Radio_Accessory.AddObject(newRadioAccessory);
                    }
                }
            }

            _context.DataContext.SaveChanges();

            return Json(new { success = true });
        }


        [Authorize(Roles = "Radio District")]
        [HttpPost]
        public ActionResult SaveInventory(List<RadioItemViewModel> radios, List<RadioAccessoryViewModel> accessories)
        {
            if (radios != null)
            {
                var table = _context.DataContext.Radios;
                foreach (var o in radios)
                {
                    var row = table.Where(x => x.Radio_id == o.Id).FirstOrDefault();
                    // check if radio changed in any way
                    row.Last_Dist_Inv_Status_id = o.DistInvStatusId;
                    row.Last_Dist_Inv_Date = DateTime.Now;
                    row.Is_Inventory_Requested = false;
                    if(!string.IsNullOrEmpty(o.Comments) && o.Comments!=row.Last_Comments)
                    {
                        row.Last_Comments = o.Comments;
                    }
                }
                _context.DataContext.SaveChanges();
            }
            if (accessories != null)
            {
                var assTable = _context.DataContext.Radio_Accessory;
                foreach (var o in accessories)
                {
                    var row = assTable.Where(x => x.Radio_Accessory_id == o.Id).FirstOrDefault();
                    // check if radio changed in any way
                    row.Last_Dist_Inv_Qty = o.LastDistInvQty;
                    row.Last_Dist_Inv_Date = DateTime.Now;
                    row.Is_Inventory_Requested = false;
                }
                _context.DataContext.SaveChanges();
            }
            return Json(new { success = true });
        }

        private List<string> ValidateRadiosForSaving(List<RadioItemViewModel> radios, List<RadioAccessoryViewModel> radioAccessories)
        {
            var errors = new List<string>();
            if (radios.Count > 0)
            {
                if (radios.Where(x => x.IsDeleted == false).Select(x => x.SerialNumber.ToLower()).Distinct().Count() != radios.Where(x => x.IsDeleted == false).Count())
                {
                    var duplicateSerialKey = radios.Where(x => x.IsDeleted == false).GroupBy(x => x.SerialNumber.ToLower()).Where(x => x.Count() > 1).Select(x => x.Key).First();
                    errors.Add($"Duplicate serial number not allowed. Duplicate value: {duplicateSerialKey}");
                }

                if (radios.Where(x => x.IsDeleted == false && x.DSNYRadioId != null).Select(x => x.DSNYRadioId.ToLower()).Distinct().Count() != radios.Where(x => x.IsDeleted == false && x.DSNYRadioId != null).Count())
                {
                    var duplicateDSNYRadioId = radios.Where(x => x.IsDeleted == false && x.DSNYRadioId != null).GroupBy(x => x.DSNYRadioId.ToLower()).Where(x => x.Count() > 1).Select(x => x.Key).First();
                    errors.Add($"Duplicate radio id not allowed. Duplicate value: {duplicateDSNYRadioId}");
                }

                if (radios.Where(x => x.IsDeleted == false && x.TrunkId != null).Select(x => x.TrunkId.ToLower()).Distinct().Count() != radios.Where(x => x.IsDeleted == false && x.TrunkId != null).Count())
                {
                    var duplicateTrunkId = radios.Where(x => x.IsDeleted == false && x.TrunkId != null).GroupBy(x => x.TrunkId.ToLower()).Where(x => x.Count() > 1).Select(x => x.Key).First();
                    errors.Add($"Duplicate trunk id not allowed. Duplicate value: {duplicateTrunkId}");
                }
            }

            if (radioAccessories != null && radioAccessories.Count > 0)
            {
                var totalGroups = radioAccessories.Where(x => x.IsDeleted == false).GroupBy(x => new { x.PartNumber, x.UserId, x.EquipmentTypeId, x.MakeId }).Where(x => x.Count() > 1).ToList();
                if(totalGroups.Count>0)
                {
                    errors.Add($"Part number, location, equipment type and make must be unique in each radio accessory.");
                }

                //if (radioAccessories.Where(x => x.IsDeleted == false).Select(x => x.PartNumber.ToLower()).Distinct().Count() != radioAccessories.Count)
                //{
                //    var duplicatePartNumber = radioAccessories.GroupBy(x => x.PartNumber.ToLower()).Where(x => x.Count() > 1).Select(x => x.Key).First();
                //    errors.Add($"Duplicate part number not allowed. Duplicate value: {duplicatePartNumber}");
                //}
            }
            return errors;
        }

        public RadioCodesViewModel GetCodesViewModel()
        {
            var model = new RadioCodesViewModel();
            model.EquipmentTypes = Mapper.Map<List<EquipmentTypeModel>>(_radioRepository.GetEquipmentTypes());
            model.RepairProblemType = Mapper.Map<List<RepairProblemTypeModel>>(_radioRepository.GetRadioRepairCodes());
            model.EquipmentStatuses = Mapper.Map<List<EquipmentStatusModel>>(_radioRepository.GetRadioEquipmentStatus());
            model.RadioMakes = Mapper.Map<List<RadioMakeModel>>(_radioRepository.GetRadioMakes());
            foreach (var make in model.RadioMakes)
            {
                make.Models = Mapper.Map<List<RadioModelVM>>(_radioRepository.GetRadioModelsByMakeId(make.ID.GetValueOrDefault(), true));
            }
            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");
            return model;
        }

        [HttpPost]
        [Authorize(Roles = "Radio Admin")]
        public ActionResult Save(RadioCodesViewModel model)
        {
            try
            {
                var message = "";
                if (ValidateCodesModel(model, out message))
                {
                    SaveMakes(model.RadioMakes);
                    SaveEquipmentTypes(model.EquipmentTypes);
                    SaveRepairProblemTypes(model.RepairProblemType);
                    SaveEquipmentStatuses(model.EquipmentStatuses);
                    var data = GetCodesViewModel();
                    return Json(new { success = true, data = data });
                }
                return Json(new { success = false, message = message });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });

            }
        }

        private bool ValidateCodesModel(RadioCodesViewModel model, out string message)
        {
            if (ValidateEquipmentTypeCodes(model.EquipmentTypes, out message) && ValidateEquipmentStatusCodes(model.EquipmentStatuses, out message) && ValidateMakeModelCodes(model.RadioMakes, out message))
            {
                message = "";
                return true;
            }
            return false;
        }

        private bool ValidateEquipmentStatusCodes(List<EquipmentStatusModel> values, out string message)
        {
            var table = _context.DataContext.Radios;
            foreach (var o in values)
            {
                if (o.IsDeleted)
                {
                    var repairProblemType = table.Where(x => x.Radio_Status_id == o.ID && x.is_Active == true).FirstOrDefault();
                    if (repairProblemType != null) { message = "Cannot delete Equipment Type " + o.Name + " has active radios"; return false; }
                }
                else if (o.ID.HasValue && (o.ID.Value > 0))
                {
                    var row = values.Where(x => x.ID != o.ID && !x.IsDeleted && x.Name == o.Name).FirstOrDefault();
                    if (row != null)
                    {
                        message = "Duplicate Equipment Status " + o.Name + " "; return false;
                    }

                }
                else
                {
                    var row = values.Where(x => x.ID != o.ID && !x.IsDeleted && x.Name == o.Name).FirstOrDefault();
                    if (row != null)
                    {
                        message = "Duplicate Equipment Status " + o.Name + " "; return false;
                    }
                }
            }
            message = "";
            return true;
        }
        private bool ValidateEquipmentTypeCodes(List<EquipmentTypeModel> values, out string message)
        {
            var table = _context.DataContext.Radios;
            var asstable = _context.DataContext.Radio_Accessory;
            foreach (var o in values)
            {
                if (o.IsDeleted)
                {
                    var repairProblemType = table.Where(x => x.Radio_Equipment_Type_id == o.ID && x.is_Active == true).FirstOrDefault();
                    if (repairProblemType != null) { message = "Cannot delete Equipment Type " + o.Type + " has active radios"; return false; }
                    var ass = asstable.Where(x => x.Radio_Equipment_Type_id == o.ID).FirstOrDefault();
                    if (ass != null) { message = "Cannot delete Equipment Type " + o.Type + " has Acccessories"; return false; }
                }
                else if (o.ID.HasValue && (o.ID.Value > 0))
                {
                    var row = values.Where(x => x.ID != o.ID && !x.IsDeleted && x.Type == o.Type).FirstOrDefault();
                    if (row != null)
                    {
                        message = "Duplicate Equipment Type " + o.Type + " "; return false;
                    }

                }
                else
                {
                    var row = values.Where(x => x.ID != o.ID && !x.IsDeleted && x.Type == o.Type).FirstOrDefault();
                    if (row != null)
                    {
                        message = "Duplicate Equipment Type " + o.Type + " "; return false;
                    }
                }
            }
            message = "";
            return true;
        }
        private bool ValidateMakeModelCodes(List<RadioMakeModel> radioMakes, out string message)
        {
            var table = _context.DataContext.Radio_Make;
            var modeltable = _context.DataContext.Radio_Model;
            var radioTable = _context.DataContext.Radios;
            var asstable = _context.DataContext.Radio_Accessory;
            foreach (var o in radioMakes)
            {
                if (o.IsDeleted)
                {
                    var makeRow = radioTable.Where(x => x.Radio_Make_id == o.ID && x.is_Active == true).FirstOrDefault();
                    if (makeRow != null) { message = "Cannot delete make " + o.Name + " has active radios"; return false; }

                    var ass = asstable.Where(x => x.Radio_Make_id == o.ID).FirstOrDefault();
                    if (ass != null) { message = "Cannot delete Equipment Type " + o.Name + " has Acccessories"; return false; }
                }
                else if (o.ID.HasValue && (o.ID.Value > 0))
                {
                    var row = radioMakes.Where(x => x.ID != o.ID && x.Name == o.Name && !x.IsDeleted).FirstOrDefault();
                    if (row != null)
                    {
                        message = "Duplicate Makes found " + o.Name; return false;
                    }
                    else
                    {
                        foreach (var model in o.Models)
                        {
                            if (model.IsDeleted)
                            {
                                var makeRow = radioTable.Where(x => x.Radio_Model_id == model.ID && x.is_Active == true).FirstOrDefault();
                                if (makeRow != null) { message = "Cannot delete model " + model.Name + " has active radios"; return false; }
                            }
                            else if (model.ID.HasValue && (model.ID.Value > 0))
                            {
                                var mrow = o.Models.Where(x => x.ID != model.ID && x.Name == model.Name && !x.IsDeleted).FirstOrDefault();
                                if (mrow != null)
                                {
                                    message = "Duplicate Models found " + model.Name; return false;
                                }
                            }
                            else
                            {
                                var mrow = o.Models.Where(x => x.ID != model.ID && x.Name == model.Name && !x.IsDeleted).FirstOrDefault();
                                if (mrow != null)
                                {
                                    message = "Duplicate Models found " + model.Name; return false;
                                }
                            }

                        }
                    }
                }
                else
                {

                    var row = radioMakes.Where(x => x.ID != o.ID && x.Name == o.Name && !x.IsDeleted).FirstOrDefault();
                    if (row != null)
                    {
                        message = "Duplicate Makes found " + o.Name; return false;
                    }
                    foreach (var model in o.Models)
                    {
                        var mrow = o.Models.Where(x => x.ID != model.ID && x.Name == model.Name && !x.IsDeleted).FirstOrDefault();
                        if (mrow != null)
                        {
                            message = "Duplicate Models found " + model.Name; return false;
                        }
                    }
                }
            }
            message = "";
            return true;
        }
        private void SaveMakes(List<RadioMakeModel> radioMakes)
        {
            var table = _context.DataContext.Radio_Make;
            var modeltable = _context.DataContext.Radio_Model;
            foreach (var o in radioMakes)
            {
                if (o.IsDeleted)
                {
                    foreach (var model in o.Models)
                    {
                        var mrow = modeltable.Where(x => x.Radio_Model_id == model.ID).FirstOrDefault();
                        if (mrow != null) { _context.DataContext.DeleteObject(mrow); }
                    }
                    var makeRow = table.Where(x => x.Radio_Make_id == o.ID).FirstOrDefault();
                    if (makeRow != null) { _context.DataContext.DeleteObject(makeRow); }
                }
                else if (o.ID.HasValue && (o.ID.Value > 0))
                {
                    var row = table.Where(x => x.Radio_Make_id == o.ID).FirstOrDefault();
                    if (row == null)
                    {
                        var newRow = new Radio_Make();
                        newRow.Make_Name = o.Name;
                        newRow.is_Active = o.IsActive;
                        table.AddObject(newRow);

                        foreach (var model in o.Models)
                        {
                            var mRow = new Radio_Model();
                            mRow.Model_Name = model.Name;
                            mRow.is_Active = model.IsActive;
                            mRow.Radio_Make_id = newRow.Radio_Make_id;
                            modeltable.AddObject(mRow);
                        }
                    }
                    else
                    {
                        row.Make_Name = o.Name;
                        row.is_Active = o.IsActive;
                        foreach (var model in o.Models)
                        {
                            if (model.IsDeleted)
                            {
                                var mrow = modeltable.Where(x => x.Radio_Model_id == model.ID).FirstOrDefault();
                                if (mrow != null) { _context.DataContext.DeleteObject(mrow); }
                            }
                            else if (model.ID.HasValue && (model.ID.Value > 0))
                            {
                                var mrow = modeltable.Where(x => x.Radio_Model_id == model.ID).FirstOrDefault();
                                if (mrow == null)
                                {
                                    var mRow = new Radio_Model();
                                    mRow.Model_Name = model.Name;
                                    mRow.is_Active = model.IsActive;
                                    mRow.Radio_Make_id = o.ID;
                                    modeltable.AddObject(mRow);
                                }
                                else
                                {
                                    mrow.Model_Name = model.Name;
                                    mrow.is_Active = model.IsActive;

                                }
                            }
                            else
                            {
                                var mRow = new Radio_Model();
                                mRow.Model_Name = model.Name;
                                mRow.is_Active = model.IsActive;
                                mRow.Radio_Make_id = o.ID;
                                modeltable.AddObject(mRow);
                            }

                        }
                    }
                }
                else
                {
                    var newRow = new Radio_Make();
                    newRow.Make_Name = o.Name;
                    newRow.is_Active = o.IsActive;
                    table.AddObject(newRow);
                    _context.DataContext.SaveChanges();
                    foreach (var model in o.Models)
                    {
                        var mRow = new Radio_Model();
                        mRow.Model_Name = model.Name;
                        mRow.is_Active = model.IsActive;
                        mRow.Radio_Make_id = newRow.Radio_Make_id;
                        modeltable.AddObject(mRow);
                    }
                }
            }
            _context.DataContext.SaveChanges();
        }
        public void SaveEquipmentTypes(List<EquipmentTypeModel> values)
        {
            var table = _context.DataContext.Radio_Equipment_Type;
            foreach (var o in values)
            {
                if (o.IsDeleted)
                {
                    var repairProblemType = table.Where(x => x.Radio_Equipment_Type_id == o.ID).FirstOrDefault();
                    if (repairProblemType != null) { _context.DataContext.DeleteObject(repairProblemType); }
                }
                else if (o.ID.HasValue && (o.ID.Value > 0))
                {
                    var row = table.Where(x => x.Radio_Equipment_Type_id == o.ID).FirstOrDefault();
                    if (row == null)
                    {
                        var newRow = new Radio_Equipment_Type();
                        newRow.Equipment_Type_Name = o.Type;
                        newRow.is_Active = o.IsActive;
                        newRow.is_District_Inventory = o.DistrictInventory;
                        table.AddObject(newRow);
                    }
                    else
                    {
                        row.Equipment_Type_Name = o.Type;
                        row.is_Active = o.IsActive;
                        row.is_District_Inventory = o.DistrictInventory;
                    }
                }
                else
                {
                    var newRow = new Radio_Equipment_Type();
                    newRow.Equipment_Type_Name = o.Type;
                    newRow.is_Active = o.IsActive;
                    newRow.is_District_Inventory = o.DistrictInventory;
                    table.AddObject(newRow);
                }
            }
            _context.DataContext.SaveChanges();
        }
        public void SaveRepairProblemTypes(List<RepairProblemTypeModel> values)
        {
            var table = _context.DataContext.Radio_Repair_Problem_Type;
            foreach (var o in values)
            {
                if (o.IsDeleted)
                {
                    var repairProblemType = table.Where(x => x.Radio_Repair_Problem_id == o.ID).FirstOrDefault();
                    if (repairProblemType != null) { _context.DataContext.DeleteObject(repairProblemType); }
                }
                else if (o.ID.HasValue && (o.ID.Value > 0))
                {
                    var row = table.Where(x => x.Radio_Repair_Problem_id == o.ID).FirstOrDefault();
                    if (row == null)
                    {
                        var newRow = new Radio_Repair_Problem_Type();
                        newRow.Repair_Problem_Name = o.Type;
                        newRow.is_Active = o.IsActive;
                        table.AddObject(newRow);
                    }
                    else
                    {
                        row.Repair_Problem_Name = o.Type;
                        row.is_Active = o.IsActive;
                    }
                }
                else
                {
                    var newRow = new Radio_Repair_Problem_Type();
                    newRow.Repair_Problem_Name = o.Type;
                    newRow.is_Active = o.IsActive;
                    table.AddObject(newRow);
                }
            }
            _context.DataContext.SaveChanges();
        }
        public void SaveEquipmentStatuses(List<EquipmentStatusModel> values)
        {
            var table = _context.DataContext.Radio_Equipment_Status;
            foreach (var o in values)
            {
                if (o.IsDeleted)
                {
                    var repairProblemType = table.Where(x => x.Radio_Equipment_Status_id == o.ID).FirstOrDefault();
                    if (repairProblemType != null) { _context.DataContext.DeleteObject(repairProblemType); }
                }
                else if (o.ID.HasValue && (o.ID.Value > 0))
                {
                    var row = table.Where(x => x.Radio_Equipment_Status_id == o.ID).FirstOrDefault();
                    if (row == null)
                    {
                        var newRow = new Radio_Equipment_Status();
                        newRow.Equipment_Status_Name = o.Name;
                        newRow.is_Workflow = o.WorkFlow;
                        newRow.is_Inventory = o.Inventory;
                        table.AddObject(newRow);
                    }
                    else
                    {
                        row.Equipment_Status_Name = o.Name;
                        row.is_Workflow = o.WorkFlow;
                        row.is_Inventory = o.Inventory;
                    }
                }
                else
                {
                    var newRow = new Radio_Equipment_Status();
                    newRow.Equipment_Status_Name = o.Name;
                    newRow.is_Workflow = o.WorkFlow;
                    newRow.is_Inventory = o.Inventory;
                    table.AddObject(newRow);
                }
            }
            _context.DataContext.SaveChanges();
        }

        [Authorize(Roles = "Radio Admin")]
        public ActionResult Index()
        {
            RadioViewModel model = new RadioViewModel();
            try
            {
                model = GetViewModel();
            }
            catch (Exception e)
            {
                _exceptionHandler.HandleException(e);
            }

            return View(model);
        }

        [Authorize(Roles = "Radio District")]
        public ActionResult Inventory()
        {
            var model = new InventroyViewModel();
            try
            {
                var currentUser = _userRepository.getUser(User.Identity.Name);
                model.Radios = Mapper.Map<List<RadioItemViewModel>>(_radioRepository.getAllRadio(false).Where(x => x.Is_Inventory_Requested.GetValueOrDefault() && x.Issued_To_Userid == currentUser.userId));
                model.RadioAccessories = Mapper.Map<List<RadioAccessoryViewModel>>(_radioRepository.GetAllRadioAccessories().Where(x => x.Is_Inventory_Requested.GetValueOrDefault() && x.Userid == currentUser.userId));
                foreach (var radio in model.Radios)
                {
                    radio.Models = _radioRepository.GetRadioModelsByMakeId(radio.RadioMakeId.GetValueOrDefault(), false).Select(x => new Dropdown() { id = x.Radio_Model_id.ToString(), description = x.Model_Name }).ToList();
                    radio.DistInvStatusId = radio.StatusId;
                }
                model.RadioAccessories.ForEach(x => x.LastDistInvQty = x.CurrentQty);
                model.EquipmentTypes = _radioRepository.GetEquipmentTypes().Where(x => x.is_District_Inventory.GetValueOrDefault() == true).Select(x => new Dropdown() { id = x.Radio_Equipment_Type_id.ToString(), description = x.Equipment_Type_Name }).ToList();
                model.Statuses = _radioRepository.GetRadioEquipmentStatus().Where(x => x.is_Inventory.GetValueOrDefault() == true).Select(x => new Dropdown() { id = x.Radio_Equipment_Status_id.ToString(), description = x.Equipment_Status_Name }).ToList();
                model.Makes = _radioRepository.GetRadioMakes().Select(x => new Dropdown { id = x.Radio_Make_id.ToString(), description = x.Make_Name }).ToList();
                model.Models = new List<Dropdown>();
                model.Radios = model.Radios.Where(x => model.EquipmentTypes.Any(y => y.id == x.RadioEquipmentTypeId.ToString())).ToList(); // show only district inventory radios
                model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");
            }
            catch (Exception e)
            {
                _exceptionHandler.HandleException(e);
            }
            return View(model);
        }

        [Authorize(Roles = "Radio Admin")]
        [HttpGet]
        public JsonResult GetRadioHistory(int radioId)
        {
            var radioHistories = _radioRepository.GetRadioHistory(radioId).OrderByDescending(x => x.ChangeDate);
            var objRadioHistories = new List<RadioHistoryViewModel>();
            foreach (var radioHistory in radioHistories)
            {
                var radioHistoryItem = new RadioHistoryViewModel();
                radioHistoryItem.Status = _context.DataContext.Radio_Equipment_Status.First(x => x.Radio_Equipment_Status_id == radioHistory.Radio_Status_id)?.Equipment_Status_Name;
                radioHistoryItem.Comments = radioHistory.Comment;
                radioHistoryItem.DSNYRadioId = radioHistory.DSNY_Radio_id;
                radioHistoryItem.SerialNumber = radioHistory.Radio_Serial_Number;
                radioHistoryItem.TrunkId = radioHistory.Trunk_id;
                radioHistoryItem.VehicleNumber = radioHistory.Assigned_To_Vehicle;
                if (radioHistory.Service_Date != null)
                    radioHistoryItem.ServiceDate = radioHistory.Service_Date.Value.ToShortDateString();
                if (radioHistory.Inventory_Date != null)
                    radioHistoryItem.InventoryDate = radioHistory.Inventory_Date.Value.ToShortDateString();

                radioHistoryItem.IsActive = radioHistory.is_Active.GetValueOrDefault(true);
                radioHistoryItem.IsSpare = radioHistory.is_Spare.GetValueOrDefault();
                if (radioHistory.Radio_Make_id != null)
                {
                    radioHistoryItem.Make = _context.DataContext.Radio_Make.First(x => x.Radio_Make_id == radioHistory.Radio_Make_id.Value).Make_Name;
                }

                if (radioHistory.Radio_Model_id != null)
                {
                    radioHistoryItem.Model = _context.DataContext.Radio_Model.First(x => x.Radio_Model_id == radioHistory.Radio_Model_id.Value).Model_Name;
                }

                if (radioHistory.Issued_To_Userid != null)
                {
                    var issuedUser = _context.DataContext.aspnet_Users.FirstOrDefault(x => x.UserId == radioHistory.Issued_To_Userid.Value);
                    if (issuedUser != null)
                        radioHistoryItem.Location = issuedUser.UserName;
                }

                if (radioHistory.Radio_Equipment_Type_id != null)
                {
                    var equipment = _context.DataContext.Radio_Equipment_Type.FirstOrDefault(x => x.Radio_Equipment_Type_id == radioHistory.Radio_Equipment_Type_id.Value);
                    if (equipment != null)
                        radioHistoryItem.EquipmentType = equipment.Equipment_Type_Name;
                }

                radioHistoryItem.ChangeDate = radioHistory.ChangeDate.ToShortDateString();

                objRadioHistories.Add(radioHistoryItem);
            }

            return Json(objRadioHistories, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Radio Admin")]
        [HttpPost]
        public JsonResult Receive(int id, int qty, string comments)
        {
            try
            {
                var accessory = _context.DataContext.Radio_Accessory.FirstOrDefault(x => x.Radio_Accessory_id == id);
                if (accessory == null)
                {
                    return Json(new { success = false, errors = "Accessory not found." });
                }

                if (qty < 0)
                    return Json(new { success = false, errors = "Qty must be greater than 0" });

                var radioAccessoryLedger = new Radio_Accessory_Ledger();
                radioAccessoryLedger.Radio_Accessory_id = id;
                radioAccessoryLedger.Qty = qty;
                radioAccessoryLedger.Comment = comments;
                radioAccessoryLedger.is_recieve = true;
                _context.DataContext.Radio_Accessory_Ledger.AddObject(radioAccessoryLedger);

                accessory.Current_Qty += qty;
                _context.DataContext.SaveChanges();
                return Json(new { success = true });
            }
            catch (Exception e)
            {
                _exceptionHandler.HandleException(e);
                return Json(new { success = false, errors = e.Message });
            }
        }

        [Authorize(Roles = "Radio Admin")]
        [HttpPost]
        public JsonResult Transfer(int id, int qty, string comments, string transferToId)
        {
            try
            {
                var accessory = _context.DataContext.Radio_Accessory.FirstOrDefault(x => x.Radio_Accessory_id == id);
                if (accessory != null)
                {
                    if (accessory.Current_Qty < qty)
                        return Json(new { success = false, errors = "You can't transfer more than available quantity" });
                }
                else
                    return Json(new { success = false, errors = "Accessory not found." });

                if (qty < 0)
                    return Json(new { success = false, errors = "Qty must be greater than 0" });

                var radioAccessoryLedger = new Radio_Accessory_Ledger();
                radioAccessoryLedger.Radio_Accessory_id = id;
                radioAccessoryLedger.Qty = qty;
                radioAccessoryLedger.Comment = comments;
                radioAccessoryLedger.is_recieve = false;
                radioAccessoryLedger.Transfer_Userid = Guid.Parse(transferToId);

                _context.DataContext.Radio_Accessory_Ledger.AddObject(radioAccessoryLedger);

                accessory.Current_Qty -= qty; // reduce the quantity

                _context.DataContext.SaveChanges();

                // Check if this accessory exists in that location. If not add that to them.
                var added= AddAccessoryToLocationIfDoesNotExist(id,Guid.Parse(transferToId),qty);
                return Json(new { success = true, accessoryAddedToTragetLocation=added });
            }
            catch (Exception e)
            {
                _exceptionHandler.HandleException(e);
                return Json(new { success = false, errors = e.Message });
            }
        }
        /// <summary>
        /// Adds an accessory to a location if that location does not contain that accessory already.Accessory is matched based on part number, equipment type and make. 
        /// </summary>
        /// <param name="accessoryId">Accessory Id that needs to be checked</param>
        /// <param name="locationId">Location Id for which the accessory needs to be matched</param>
        /// <param name="qty">Quantity that needs to be added to target location if the accessory is not found.</param>
        /// <returns>True if target location does not have that accessory and one is added else false.</returns>
        private bool AddAccessoryToLocationIfDoesNotExist(int accessoryId,Guid locationId,int qty)
        {
            var oldAccessory = _context.DataContext.Radio_Accessory.FirstOrDefault(x => x.Radio_Accessory_id == accessoryId);

            var accessory = _context.DataContext.Radio_Accessory.FirstOrDefault(x => x.Part_Number == oldAccessory.Part_Number && x.Radio_Equipment_Type_id == oldAccessory.Radio_Equipment_Type_id
             && x.Radio_Make_id == oldAccessory.Radio_Make_id && x.Userid == locationId);

            if(accessory==null)
            {
                accessory = new Radio_Accessory();
                accessory.Part_Number = oldAccessory.Part_Number;
                accessory.Userid = locationId;
                accessory.Radio_Equipment_Type_id = oldAccessory.Radio_Equipment_Type_id;
                accessory.Radio_Make_id = oldAccessory.Radio_Make_id;
                accessory.Current_Qty = qty;
                accessory.Description = oldAccessory.Description;
                accessory.Is_Spare = oldAccessory.Is_Spare;

                _context.DataContext.Radio_Accessory.AddObject(accessory);
                _context.DataContext.SaveChanges();

                return true;
            }
            else
            {
                accessory.Current_Qty += qty;
                _context.DataContext.SaveChanges();
                return false;
            }            
        }

        [Authorize(Roles = "Radio Admin")]
        [HttpPost]
        public JsonResult SendInventoryRequest()
        {
            try
            {
                var sentUsers = new List<Guid>();// list of users whom we have alrady sent email
                var sentEmails = new List<string>();// list of users whom we have alrady sent email
                var radios = _context.DataContext.Radios.Where(x => (x.is_Active == null || x.is_Active == true));
                var dt = DateTime.Now;
                foreach (var radio in radios)
                {
                    if (sentUsers.Contains(radio.Issued_To_Userid.Value) == false)
                    {
                        var location = _userRepository.getUser(radio.Issued_To_Userid.Value);
                        if (location != null && !string.IsNullOrWhiteSpace(location.email))
                        {
                            sentEmails.Add(location.email);
                        }
                        sentUsers.Add(radio.Issued_To_Userid.Value);
                    }
                    radio.Is_Inventory_Requested = true;
                    radio.Inventory_Request_Date = dt;
                }

                var radioAccessories = _context.DataContext.Radio_Accessory;
                foreach (var radioAccessory in radioAccessories)
                {
                    if (sentUsers.Contains(radioAccessory.Userid.Value) == false)
                    {
                        var location = _userRepository.getUser(radioAccessory.Userid.Value);
                        if (location != null&& !string.IsNullOrWhiteSpace(location.email) && !sentEmails.Contains(location.email))
                        {
                            sentEmails.Add(location.email);
                        }
                        sentUsers.Add(radioAccessory.Userid.Value);
                    }

                    radioAccessory.Is_Inventory_Requested = true;
                    radioAccessory.Inventory_Request_Date = dt;
                }
                SendInventoryEmailToRadioDistriction(sentEmails);

                _context.DataContext.SaveChanges();
                return Json(new { success = true });
            }
            catch (Exception e)
            {
                _exceptionHandler.HandleException(e);
                return Json(new { success = false, errors = e.Message });
            }
        }

        [Authorize(Roles = "Radio Admin")]
        [HttpGet]
        public FileResult CreateTrunkSheet(string radioIds)
        {
            try
            {
                List<int> radio_Ids = new List<int>();
                radioIds.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(x => radio_Ids.Add(int.Parse(x)));
                var excelFile = CreateTrunkSheetExcel(radio_Ids);
                return File(excelFile, "application/vnd.ms-excel", "Trunk Sheet.xlsx");
            }
            catch (Exception e)
            {
                _exceptionHandler.HandleException(e);
                throw;
            }
        }
        #endregion

        #region ViewModel Building
        RadioViewModel GetViewModel()
        {
            var model = new RadioViewModel();
            model.Radios = Mapper.Map<List<RadioItemViewModel>>(_radioRepository.getAllRadio(true));
            foreach (var radio in model.Radios)
            {
                radio.Models = _radioRepository.GetRadioModelsByMakeId(radio.RadioMakeId.GetValueOrDefault(), false).Select(x => new Dropdown() { id = x.Radio_Model_id.ToString(), description = x.Model_Name }).ToList();
                if (radio.LastDistInvStatusId != null)
                {
                    var status = _radioRepository.GetRadioEquipmentStatus().FirstOrDefault(x => x.Radio_Equipment_Status_id == radio.LastDistInvStatusId.Value);
                    if (status != null)
                    {
                        radio.DistrictInventoryStatus = status.Equipment_Status_Name;
                    }
                }

                if (radio.IssueToUserId != null)
                {
                    var user = _userRepository.getUser(radio.IssueToUserId.Value);
                    radio.Borough = user.borough;
                }
            }

            model.RadioAccessories = Mapper.Map<List<RadioAccessoryViewModel>>(_radioRepository.GetAllRadioAccessories());
            foreach (var radioAccessory in model.RadioAccessories)
            {
                if (radioAccessory.UserId != null)
                {
                    var user = _userRepository.getUser(radioAccessory.UserId.Value);
                    radioAccessory.Borough = user.borough;
                }
            }

            model.Boroughs = _context.DataContext.Boroughs.OrderBy(x => x.Borough_Name).Select(x => new Dropdown { id = x.Borough_Name, description = x.Borough_Name }).ToList();//_context.DataContext.Boroughs.OrderBy(x => x.Borough_Name).ToList();
            model.Locations = _userRepository.getRoleUsers("Radio District").OrderBy(x => x.fullName).ToList();
            model.EquipmentTypes = _radioRepository.GetEquipmentTypes().Select(x => new Dropdown { id = x.Radio_Equipment_Type_id.ToString(), description = x.Equipment_Type_Name }).OrderBy(x => x.description).ToList();
            model.Statuses = _radioRepository.GetRadioEquipmentStatus().Where(x => x.is_Workflow.GetValueOrDefault() == true).Select(x => new Dropdown { id = x.Radio_Equipment_Status_id.ToString(), description = x.Equipment_Status_Name }).ToList();
            model.Makes = _radioRepository.GetRadioMakes().Select(x => new Dropdown { id = x.Radio_Make_id.ToString(), description = x.Make_Name }).ToList();
            model.Models = new List<Dropdown>();
            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");

            return model;
        }
        #endregion

        private void SendInventoryEmailToRadioDistriction(List<string> emails)
        {
            var emailMessage = new MailMessage();
            foreach (var email in emails)
            {
                emailMessage.To.Add(email);
            }
            emailMessage.Subject = "Inventory Request";
            emailMessage.Body = $"HQ is requesting a new round of radio and radio accessory inventory on {DateTime.Now.ToShortDateString()}. Please conduct your inventory check and enter the status and quantities in CMS through the radio inventory submission screen. Please complete this inventory review within one week of this request.";
            emailMessage.IsBodyHtml = true;

            Common.Utilities.EmailUtilities.SendEmail(emailMessage);
        }

        private string CreateTrunkSheetExcel(List<RadioItemViewModel> radios)
        {
            var application = new Microsoft.Office.Interop.Excel.Application();
            var missingValue = System.Reflection.Missing.Value;
            var workBook = application.Workbooks.Add(missingValue);
            var workSheet = workBook.Worksheets.get_Item(1);

            var startRange = workSheet.Cells[1, 1];
            var endRange = workSheet.Cells[1, 3];
            try
            {
                var headerRange = (Range)workSheet.get_Range((Object)startRange, (Object)endRange);
                headerRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                headerRange.VerticalAlignment = XlVAlign.xlVAlignCenter;
            }
            catch (Exception e)
            {

            }


            workSheet.Cells[1, 1] = "SERIAL NUMBER";
            workSheet.Cells[1, 2] = "ID NUMBER";
            workSheet.Cells[1, 3] = "TRUNK ID";
            var rowNumber = 2;
            foreach (var radio in radios)
            {
                workSheet.Cells[rowNumber, 1] = radio.SerialNumber;
                workSheet.Cells[rowNumber, 2] = radio.DSNYRadioId;
                workSheet.Cells[rowNumber, 3] = null;
            }

            try
            {
                workSheet.UsedRange.EntireColumn.AutoFit();
            }
            catch (Exception e)
            {
            }

            var fullPath = Path.GetTempFileName();
            System.IO.File.Delete(fullPath);
            workBook.Saved = true;
            workBook.SaveCopyAs(fullPath);
            Marshal.ReleaseComObject(workSheet);
            workBook.Close(true, missingValue, missingValue);
            Marshal.ReleaseComObject(workBook);
            application.Quit();
            //Marshal.ReleaseComObject(missingValue);

            //var fileInfo = new FileInfo(fullPath);
            return fullPath;
        }

        private string CreateTrunkSheetExcel(List<int> radioIds)
        {
            var application = new Microsoft.Office.Interop.Excel.Application();
            var missingValue = System.Reflection.Missing.Value;
            var workBook = application.Workbooks.Add(missingValue);
            var workSheet = workBook.Worksheets.get_Item(1);

            var startRange = workSheet.Cells[1, 1];
            var endRange = workSheet.Cells[1, 3];
            try
            {
                var headerRange = (Range)workSheet.Range[(Range)startRange, (Range)endRange];
                headerRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                headerRange.VerticalAlignment = XlVAlign.xlVAlignCenter;
                headerRange.Font.Bold = true;
            }
            catch (Exception e)
            {
            }

            workSheet.Cells[1, 1] = "SERIAL NUMBER";
            workSheet.Cells[1, 2] = "ID NUMBER";
            workSheet.Cells[1, 3] = "TRUNK ID";
            var rowNumber = 2;
            foreach (var radioId in radioIds)
            {
                var radio = _context.DataContext.Radios.FirstOrDefault(x => x.Radio_id == radioId);
                if (radio != null)
                {
                    workSheet.Cells[rowNumber, 1] = radio.Radio_Serial_Number;
                    workSheet.Cells[rowNumber, 2] = radio.DSNY_Radio_id;
                    workSheet.Cells[rowNumber, 3] = null;
                    rowNumber++;
                }
            }

            try
            {
                workSheet.UsedRange.EntireColumn.AutoFit();
            }
            catch (Exception e)
            {
            }

            var fullPath = Path.GetTempFileName();
            System.IO.File.Delete(fullPath);
            workBook.Saved = true;
            workBook.SaveCopyAs(fullPath);
            Marshal.ReleaseComObject(workSheet);
            workBook.Close(true, missingValue, missingValue);
            Marshal.ReleaseComObject(workBook);
            application.Quit();
            //Marshal.ReleaseComObject(missingValue);

            //var fileInfo = new FileInfo(fullPath);
            return fullPath;
        }

        public FileResult DownloadPickupSheet(string fileName)
        {
            var path = Path.Combine(Server.MapPath("~/PDfs"), fileName);
            if (System.IO.File.Exists(path))
            {
                return File(path, "application/pdf", fileName);
            }
            else
                return null;
        }

        public JsonResult CreatePickupSheet(List<RadioItemViewModel> radios)
        {
            try
            {
                var pdfFile = CreatePickupSheetInternal(radios);
                if (pdfFile != null)
                {
                    var url = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                    return Json(new { success = true, file = url + "radio/DownloadPickupSheet?fileName=" + Path.GetFileName(pdfFile) });
                }
                else
                {
                    return Json(new { success = false, error = "Unexpected error in Pickup sheet generation" });
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, error = e.Message });
            }
        }


        public JsonResult CreateRMAReport(List<RadioItemViewModel> radios)
        {
            try
            {
                var pdfFile = GenrateRMAReport(radios);
                if (pdfFile != null)
                {
                    var url = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                    return Json(new { success = true, file = url + "radio/DownloadPickupSheet?fileName=" + Path.GetFileName(pdfFile) });
                }
                else
                {
                    return Json(new { success = false, error = "Unexpected error in Pickup sheet generation" });
                }
            }
            catch (Exception e)
            {
                return Json(new { success = false, error = e.Message });
            }
        }


        private string CreatePickupSheetInternal(List<RadioItemViewModel> items)
        {
            try
            {
                var selectedItems = items.Where(x => x.Selected).ToList();
                if (selectedItems.Count < 1)
                    throw new Exception("No radio's selected to create the pickup sheet");

                var body = System.IO.File.ReadAllText(Server.MapPath("~/Views/Shared/PDFTemplates/_PickUpSheet.html"));
                var sb = new StringBuilder();
                var i = 1;
                foreach (var item in selectedItems)
                {
                    sb.Append("<div style=\"width:100%;dispaly:block;padding-bottom:5px;\">");
                    sb.Append("<div style=\"width:5%;display:inline;float:left\">");
                    sb.Append(i++.ToString());
                    sb.Append("</div>");
                    sb.Append("<div style=\"width:10%;display:inline;float:left\">");
                    sb.Append(item.DSNYRadioId + "<br />" + item.SerialNumber);
                    sb.Append("</div>");
                    sb.Append("<div style=\"width:85%;display:inline;float:left\">");
                    var user = _userRepository.getUser(item.IssueToUserId.Value);
                    sb.Append((user != null ? user.fullName : "") + "   <br />");
                    sb.Append("<b>Problem Reported :</b> " + item.Comments + "   <br />");
                    sb.Append("<b>Problem Found :</b>    <br />");
                    sb.Append("<b>Repair Action :</b>    <br />");
                    sb.Append("</div>");
                    sb.Append("</div><br/>");
                }
                body = body.Replace("{{foreachText}}", sb.ToString());
                body = body.Replace("{{date}}", DateTime.Now.ToString("dddd, MMMM dd, yyyy"));


                var fileData = PDFUtility.ConvertHTMLToPdf(body);
                var fileName = Path.Combine(Server.MapPath("~/PDFs"), $"PickupSheet_{DateTime.Now.Ticks}.pdf");
                System.IO.File.WriteAllBytes(fileName, fileData);
                return fileName;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }

            return null;
        }

        private string GenrateRMAReport(List<RadioItemViewModel> items)
        {
            try
            {
                var selectedItems = items.Where(x => x.Selected).ToList();
                if (selectedItems.Count < 1)
                    throw new Exception("No radio's selected to create the pickup sheet");

                var body = System.IO.File.ReadAllText(Server.MapPath("~/Views/Shared/PDFTemplates/_RMAReport.html"));
                var sb = new StringBuilder();
                var i = 1;
                foreach (var item in selectedItems)
                {
                    sb.Append("<tr style=\"border-bottom:1px solid gray\">");
                    sb.Append("<td style=\"width:5%;border-bottom:1px solid gray\" >");
                    sb.Append("<span>"+ i++.ToString() +"</span><br/> <span style=\"margin-left:30px;\">  </span>");
                    sb.Append("</td>");
                    sb.Append("<td style=\"width:15%;border-bottom:1px solid gray\" > ");
                    sb.Append(item.DSNYRadioId + "/" + item.SerialNumber + "&nbsp;&nbsp;&nbsp;<br/>");
                    sb.Append("<span style=\"float:left;\">Problem:</span>");
                    sb.Append("</td>");
                    sb.Append("<td style=\"width:80%;border-bottom:1px solid gray\">");
                    var user = _userRepository.getUser(item.IssueToUserId.Value);
                    sb.Append("<span style=\"float:left\">");
                    sb.Append((user != null ? user.fullName : ""));
                    sb.Append("</span><br/> <span>" + (item.Comments ?? "&nbsp;&nbsp;&nbsp;") + "</span>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                }
                body = body.Replace("{{foreachText}}", sb.ToString());
                body = body.Replace("{{date}}", DateTime.Now.ToString("dddd, MMMM dd, yyyy"));


                var fileData = PDFUtility.ConvertHTMLToPdf(body);
                var fileName = Path.Combine(Server.MapPath("~/PDFs"), $"RMA_{DateTime.Now.Ticks}.pdf");
                System.IO.File.WriteAllBytes(fileName, fileData);
                return fileName;
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }

            return null;
        }


    }
}