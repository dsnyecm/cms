﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;

using DSNY.Core.Models;
using DSNY.Core.Interfaces;
using DSNY.Core.Repository;
using DSNY.Common;
using DSNY.Common.Logger;
using DSNY.Common.Exception;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for role pages
    /// </summary>
    
    public class RoleController : BaseController
    {
        #region Variables

        private IRoleRepository _roleRepository { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RoleController"/> class.
        /// </summary>
        /// <param name="IoCRoleRepository">The inversion of control role repository.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public RoleController(IRoleRepository IoCRoleRepository, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _roleRepository = IoCRoleRepository;
        }

        #endregion

        #region List

        /// <summary>
        /// Lists the roles.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult List()
        {
            Dictionary<string, string[]> rolesDictionary = _roleRepository.getAllRolesWithUsers();
            List<IRole> roles = new List<IRole>();

            foreach (string role in rolesDictionary.Keys)
            {
                roles.Add(new Role() { roleName = role, usersInRole = rolesDictionary[role].ToList<string>() });
            }
            
            if (TempData["Error"] != null)
                ModelState.AddModelError("Error", TempData["Error"].ToString());

            return View("~/Views/Admin/Role/List.aspx", roles);
        }

        #endregion

        #region Create

        /// <summary>
        /// Creates a role.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Create()
        {
            return View("~/Views/Admin/Role/Create.aspx");
        }

        /// <summary>
        /// HTTP Post create action. Creates role.
        /// </summary>
        /// <param name="collection">The form collection.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            bool createdRole = _roleRepository.addRole(collection["roleName"]);           

            if (createdRole)
                return RedirectToAction("List");
            else
            {
                ModelState.AddModelError("Error", "This role already exists");
                return View("~/Views/Admin/Role/Create.aspx");
            }
        }

        #endregion

        #region Delete

        // commented out, wasn't needed.

        /// <summary>
        /// Deletes role
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        //[Authorize(Roles = "System Administrator")]
        //[HttpGet]
        //public ActionResult Delete(string name)
        //{
        //    var returnValue = new { status = "failed", name = name };

        //    try
        //    {
        //        if (_roleRepository.deleteRole(name))
        //            returnValue = new { status = "success", name = name };
        //    }
        //    catch (Exception ex)
        //    {
        //        _exceptionHandler.HandleException(ex);
        //    }

        //    return Json(returnValue, JsonRequestBehavior.AllowGet);
        //}

        #endregion

        #region Sort

        /// <summary>
        /// AJAX method to sort role.
        /// </summary>
        /// <param name="field">The sort field.</param>
        /// <param name="direction">The direction.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpGet]
        public ActionResult SortedGrid(string field, string direction)
        {
            return View("RoleGrid", _roleRepository.getSortedRoles(field, direction == "ASC" ? Enums.SortDirection.Ascending : Enums.SortDirection.Descending));
        }

        #endregion
    }
}