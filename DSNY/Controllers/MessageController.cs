﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Profile;

//using Microsoft.Practices.Unity;
//using Microsoft.Practices.Unity.Configuration;

using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Common.Printing;
using DSNY.Common.Utilities;
using DSNY.Core.Interfaces;
using DSNY.Core.Repository;
using DSNY.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using DSNY.Controllers;

namespace DSNY.Mvc.Controllers
{
    /// <summary>
    /// Controller for Messages
    /// </summary>
    
    public class MessageController : BaseController
    {
        #region Variables

        private IMessageRepository _messageRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageController"/> class.
        /// </summary>
        /// <param name="IMessageRepository">The inversion of control message repository.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public MessageController(IMessageRepository IoCMessageRepo, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _messageRepository = IoCMessageRepo;
        }

        #endregion

        /// <summary>
        /// Creates a new message model and sets the message date to now
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrator")]
        [ValidateInput(false)]
        public ActionResult Create()
        {
            Message model = new Message() { Message_Date = DateTime.Now };

            return View("Create", model);
        }

        /// <summary>
        /// HTTP Post create action. Takes returned model and creates new message, then sends emails to the distribution list,
        /// finall ending with a custom print document class to send message to the default printer
        /// </summary>
        /// <param name="model"></param>
        /// <param name="collection"></param>
        /// <returns></returns>
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(Message model, FormCollection collection)
        {
            Message newMessage = new Message();

            // First write the new message to the database
            try
            {
                // HTML Encode everything for safety reasons
                newMessage.Sent_By_UserId = (Guid)Membership.GetUser(true).ProviderUserKey;
                newMessage.Message_Date = DateTime.Now;
                newMessage.Message_Code = model.Message_Code;
                newMessage.Dept_Msg_No = model.Dept_Msg_No;
                newMessage.Message_Subject = model.Message_Subject;
                newMessage.Message_Text = model.Message_Text.Trim();
                newMessage.is_Fuel_Form = false;

                _messageRepository.addMessage(newMessage, true);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }

            // now get all users which are on the email distribution list
            var emailUsers = Membership.GetAllUsers().Cast<MembershipUser>()
                                            .Select(user => ProfileBase.Create(user.UserName, true))
                                            .Where(profile => ((bool)profile["IsEmail"]) == true)
                                            .Select(profile => Membership.GetUser(profile.UserName)).ToList();

            try
            {
                // send out the email with one large bcc instead of sending it to specific users
                // set up the smtp mail info

                string mailServer = ConfigurationManager.AppSettings["Mail Server"];
                string mailUser = ConfigurationManager.AppSettings["Mail User"];
                string mailPassword = ConfigurationManager.AppSettings["Mail Password"];
                string mailFrom = ConfigurationManager.AppSettings["Mail From Address"];
                bool useSSL = false;
                int mailPort = 25;

                bool.TryParse(ConfigurationManager.AppSettings["Mail SSL"], out useSSL);
                int.TryParse(ConfigurationManager.AppSettings["Mail Port"], out mailPort);

                if (!string.IsNullOrEmpty(mailServer))
                {
                    SmtpClient sc = new SmtpClient(mailServer);

                    if (!string.IsNullOrEmpty(mailUser) && !string.IsNullOrEmpty(mailPassword))
                    {
                        NetworkCredential nc = new NetworkCredential(mailUser, mailPassword);
                        sc.UseDefaultCredentials = false;
                        sc.Credentials = nc;
                    }
                    else
                        sc.UseDefaultCredentials = true;

                    sc.EnableSsl = useSSL;
                    sc.Port = mailPort;

                    // set up the mail message going out
                    MailMessage mm = new MailMessage();

                    int iBatch = 0;

                    // setup message
                    mm.From = new MailAddress(mailFrom, newMessage.aspnet_Users.UserName);
                    mm.Subject = "DSNY CMS: " + newMessage.Message_Subject;
                    mm.IsBodyHtml = true;
                    mm.BodyEncoding = Encoding.UTF8;
                    mm.Body += "<table style='border-width: 0px;'>";
                    mm.Body += "<tr><td style='text-align: right; width: 125px; font-size: 11pt; font-family: monospace, arial, sans-serif;'>Dept Msg #: </td>";
                    mm.Body += "<td style='font-size: 11pt; font-family: monospace, arial, sans-serif;'>" + newMessage.Dept_Msg_No + "</td>";
                    mm.Body += "<td style='text-align: right; width: 125px; font-size: 11pt; font-family: monospace, arial, sans-serif;'>Sent By: </td>";
                    mm.Body += "<td style='font-size: 11pt; font-family: monospace, arial, sans-serif;'>" + newMessage.aspnet_Users.UserName + "</td></tr>";
                    mm.Body += "<tr><td style='text-align: right; width: 125px; font-size: 11pt; font-family: monospace, arial, sans-serif;'>Date: </td>";
                    mm.Body += "<td style='font-size: 11pt; font-family: monospace, arial, sans-serif;'>" + newMessage.Message_Date.Value.ToString("M/d/yyyy h:mm tt") + "</td>";
                    mm.Body += "<td style='text-align: right; width: 125px; font-size: 11pt; font-family: monospace, arial, sans-serif;'>Code: </td>";
                    mm.Body += "<td style='font-size: 11pt; font-family: monospace, arial, sans-serif;'>" + newMessage.Message_Code + "</td></tr>";
                    mm.Body += "<tr><td style='text-align: right; width: 125px; font-size: 11pt; font-family: monospace, arial, sans-serif;'>Subject: </td>";
                    mm.Body += "<td style='font-size: 11pt; font-family: monospace, arial, sans-serif;'>" + newMessage.Message_Subject + "</td><td colspan='2'></td></tr>";
                    mm.Body += "<tr><td colspan='4'><pre style='font-size: 11pt; font-family: monospace, arial, sans-serif; line-height: 1.1em;'>";
                    mm.Body += Server.HtmlDecode(newMessage.Message_Text);
                    mm.Body += "<pre></td></tr>";
                    mm.Body += "</table></div>";

                    // loop through users in batches of 25
                    foreach (MembershipUser user in emailUsers)
                    {
                        if (!string.IsNullOrEmpty(user.Email))
                        {
                            // add email address to BCC
                            mm.Bcc.Add(new MailAddress(user.Email, user.UserName));

                            // increase counter
                            iBatch++;

                            // make sure we are actually sending to someone
                            if (mm.Bcc.Count > 0 && iBatch % 25 == 0)
                            {
                                sendEmailMessage(sc, mm);
                                mm.Bcc.Clear();
                            }
                        }
                    }

                    // send email any left over mail users that don't fit neatly into the 25 user batches
                    if (mm.Bcc.Count > 0)
                        sendEmailMessage(sc, mm);

                    // dispose of our objects
                    mm.Dispose();
                    sc.Dispose();
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }

            // Now we print to the virtual printer with the custom print document implementation
            try
            {
                string printerName = ConfigurationManager.AppSettings["Printer Name"];

                // check the app config setting, if we find a printer name, then print
                if (!string.IsNullOrEmpty(printerName))
                {
                    // calculates the appropriate number of spaces to add before 'Sent By:' to have it line up with 'Code:'
                    //int offsetLength = newMessage.Message_Date.Value.ToString("M/d/yyyy h:mm tt").Length - newMessage.Message_Code.Length;

                    string printText = "Dept Msg #: " + Server.HtmlDecode(newMessage.Dept_Msg_No);

                    printText += "\t\t\tSent By: " + Server.HtmlDecode(newMessage.aspnet_Users.UserName) + Environment.NewLine;
                    printText += "\tDate: " + Server.HtmlDecode(newMessage.Message_Date.Value.ToString("M/d/yyyy h:mm tt"));
                    printText += "\t\t\tCode: " + Server.HtmlDecode(newMessage.Message_Code) + Environment.NewLine;
                    printText += "\tSubject: " + Server.HtmlDecode(newMessage.Message_Subject) + Environment.NewLine + Environment.NewLine;
                    printText += newMessage.Message_Text;

                    StringBuilder tabbedPrintText = new StringBuilder();
                    int newLinePos = 0;

                    // loop through entire message text, replacing \t (tab) with appropriate number of spaces
                    for (int i = 0; i < printText.Length; i++)
                    {
                        // if newline, reset out line position to 0
                        if (printText[i] == '\n')
                            newLinePos = 0;

                        // if tab
                        if (printText[i] == '\t')
                        {
                            // calculate number of spaces to replace tabs with
                            // 8 (default spaces per a tab) - (position of cursor mod 8)
                            int N = 8 - (newLinePos % 8);

                            // replace tab calculation with spaces if we have 
                            if (N > 0)
                            {
                                if (N == 8)
                                {
                                    newLinePos++;
                                    tabbedPrintText.Append(" ");
                                }
                                else
                                {
                                    for (int j = 0; j <= N; j++)
                                    {
                                        newLinePos++;
                                        tabbedPrintText.Append(" ");
                                    }
                                }
                            }
                            else
                                newLinePos++;
                        }
                        else
                        {
                            // increase our line position and concat together string
                            newLinePos++;
                            tabbedPrintText.Append(printText[i]);
                        }
                    }
                  
                    // send print message text to printer class
                    PrintCustomDocument pd = new PrintCustomDocument(tabbedPrintText.ToString(), _exceptionHandler);

                    // the name of the printer to send job to
                    pd.PrinterSettings.PrinterName = printerName;
                    pd.Print();

                    // dispose of the object
                    pd.Dispose();
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }

            return RedirectToAction("Admin", "Dashboard");
        }

        /// <summary>
        /// Sends an email message and recovers from any sending errors
        /// </summary>
        /// <param name="sc"></param>
        /// <param name="mm"></param>
        private void sendEmailMessage(SmtpClient sc, MailMessage mm)
        {
            try
            {
                // send mail message
                sc.Send(mm);
            }
            catch (SmtpFailedRecipientException ex)
            {
                // if there is an exception, find the exact recipient
                MailAddress failedRecp = mm.Bcc.SingleOrDefault(recp => recp.Address == ex.FailedRecipient);

                // if we found receipient, remove from BCC
                if (failedRecp != null)
                    mm.Bcc.Remove(failedRecp);

                // retry to send message with all other recepients
                sendEmailMessage(sc, mm);
            }
        }

        /// <summary>
        /// Shows a message for a field/garage user
        /// </summary>
        /// <param name="id">id of sent message</param>
        /// <returns></returns>
        [Authorize(Roles = "Field, Garage")]
        public ActionResult Field(int? id)
        {
            if (id != null)
            {
                int validId = (int)id;
                Message model = _messageRepository.getMessage(validId, (Guid)Membership.GetUser().ProviderUserKey);

                if (model != null)
                {
                    _messageRepository.updateMessageTimestamp(validId, (Guid)Membership.GetUser().ProviderUserKey);

                    // If this is an old message, remove the double spaces
                    if (model.original_message_id > 0)
                    {
                        string tempMsg = model.Message_Text.Trim();
                        tempMsg = TextUtilities.cleanMessage(tempMsg);
                        model.Message_Text = tempMsg;
                    }

                    return View(model);
                }
                else
                    return RedirectToAction("Field", "Dashboard");
            }
            else
                return RedirectToAction("Field", "Dashboard");
        }

        /// <summary>
        /// Shows a message for an admin, which also serves as a template
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Administrator")]
        public ActionResult Admin(int? id)
        {
            if (id != null)
            {
                int validId = (int) id;
                Message model = _messageRepository.getMessage(validId, (Guid)Membership.GetUser().ProviderUserKey);

                if (model != null)
                {
                    _messageRepository.updateMessageTimestamp(validId, (Guid)Membership.GetUser().ProviderUserKey);

                    // If this is an old message, remove the double spaces
                    if (model.original_message_id > 0)
                    {
                        string tempMsg = model.Message_Text.Trim();
                        tempMsg = TextUtilities.cleanMessage(tempMsg);
                        model.Message_Text = tempMsg;
                    }

                    return View(model);
                }
                else
                    return RedirectToAction("Admin", "Dashboard");
            }
            else
                return RedirectToAction("Admin", "Dashboard");
        }

        /// <summary>
        /// AJAX method for getting html for printing multiple messages
        /// </summary>
        /// <param name="ids">comma separated ids of messages</param>
        /// <returns></returns>
        [Authorize(Roles = "Field, Garage, Administrator")]
        [HttpGet]
        public ActionResult GetMessages(string ids)
        {
            List<object> returnValue = new List<object>();
            string[] idArray = ids.Split(',');
            int id;
            string sentByUserName = string.Empty;

            if (idArray.Length > 0)
            {
                foreach (string item in idArray)
                {
                    int.TryParse(item, out id);

                    if (id > 0)
                    {
                        Message requestedMessage = _messageRepository.getMessage(id, (Guid)Membership.GetUser().ProviderUserKey);

                        if (requestedMessage != null)
                        {
                            sentByUserName = requestedMessage.aspnet_Users != null ? requestedMessage.aspnet_Users.UserName : string.Empty;

                            returnValue.Add(new
                            {
                                messageDate = requestedMessage.Message_Date.Value.TimeOfDay == new TimeSpan(0, 0, 0) ?
                                    requestedMessage.Message_Date.Value.ToString("M/d/yyyy") : requestedMessage.Message_Date.Value.ToString("M/d/yyyy h:mm tt"),
                                deptMsgNo = requestedMessage.Dept_Msg_No,
                                code = requestedMessage.Message_Code,
                                subject = requestedMessage.Message_Subject,
                                sentBy = sentByUserName,
                                messageText = Server.HtmlDecode(requestedMessage.Message_Text)
                            });
                        }
                    }

                    // reset our parse value
                    id = 0;
                }
            }

            return Json(returnValue, JsonRequestBehavior.AllowGet);
        }
    }
}
