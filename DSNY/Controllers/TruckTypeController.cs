using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DSNY.Core.Interfaces;
using DSNY.Common;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Core.Repository;
using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the dashboard area
    /// </summary>
    [HandleError]
    public class TruckTypeController : Controller
    {
        private ILogger _logger;
        private IExceptionHandler _exceptionHandler;
        private DSNYContext _context;
        private TruckTypeRepository _truckType;
        private IVendorRepository _vendorRepository;

        public TruckTypeController(ILogger IoCLogger, IExceptionHandler IoCExceptionHandler, DSNYContext IoCContext, IVendorRepository IoCVendor)
        {
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
            _context = IoCContext;
            _truckType = new TruckTypeRepository(_context);
            _vendorRepository = IoCVendor;
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Index()
        {
            var model = GetViewModel();
            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");
            return View(model);
        }

        private TruckTypeViewModel GetViewModel()
        {
            var model = new TruckTypeViewModel();
            model.TruckTypes = Mapper.Map<List<TruckType>>(_truckType.GetAll());
            model.BlankTruckType = new TruckType
            {
                Active = true,
                Deleted = false
            };
            if (!model.TruckTypes.Any()) { model.TruckTypes.Add(model.BlankTruckType); }
            return model;
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Refresh()
        {
            return Json(GetViewModel());
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Save(List<TruckType> values)
        {
            if (values == null || !values.Any()) { return Json("Nothing to save"); }
            var table = _context.DataContext.Truck_Type;
            foreach (var o in values)
            {
                if (o.Deleted)
                {
                    var order = table.Where(x => x.STC_Truck_Type_id == o.Id).FirstOrDefault();
                    if (order != null) { _context.DataContext.DeleteObject(order); }
                }
                else if (o.Id.HasValue && (o.Id.Value > 0))
                {
                    var row = table.Where(x => x.STC_Truck_Type_id == o.Id).FirstOrDefault();
                    if (row == null)
                    {
                        table.AddObject(NewRow(o));
                    }
                    else
                    {
                        Update(row, o);
                    }
                }
                else
                {
                    table.AddObject(NewRow(o));
                }
            }
            _context.DataContext.SaveChanges();

            return Json("Success");
        }

        private void Update(Truck_Type row, TruckType o)
        {
            row.Truck_Type_Name = o.Name;
            row.Truck_Capacity = o.Capacity;
            row.is_active = o.Active;
        }

        private Truck_Type NewRow(TruckType o)
        {
            return new Truck_Type
            {
                Truck_Type_Name = o.Name,
                Truck_Capacity = o.Capacity,
                is_active = o.Active
            };
        }
    }
}
