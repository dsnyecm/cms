﻿using System;
using System.Configuration;
using System.Web.Mvc;
using System.Web.Security;

using DSNY.Common.Exception;
using DSNY.Common.Logger;
using DSNY.Models;
using DSNY.ViewModels;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for index pages
    /// </summary>
    public class HomeController : BaseController
    {
        #region Variables

        public IFormsAuthenticationService _formsService { get; set; }
        public IMembershipService _membershipService { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="IoCFormsService">The inversion of control forms service.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public HomeController(IFormsAuthenticationService IoCFormsService, IMembershipService IoCMembershipService, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler) 
            : base(IoCLogger, IoCExceptionHandler)
        {
            _formsService = IoCFormsService;
            _membershipService = IoCMembershipService;
        }

        #endregion

        #region Actions

        /// <summary>
        /// When hitting the home, redirect to appropriate homepage for user group
        /// If we have a user in multiple
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (Roles.IsUserInRole("Administrator"))
                return RedirectToAction("Admin", "Dashboard");
            else if (Roles.IsUserInRole("Field") || Roles.IsUserInRole("Garage"))
                return RedirectToAction("Field", "Dashboard");
            else if (Roles.IsUserInRole("System Administrator"))
                return RedirectToAction("Index", "Admin");
            else if (Roles.IsUserInRole("GS District") || Roles.IsUserInRole("GS Borough") || Roles.IsUserInRole("GS Warehouse") || Roles.IsUserInRole("GS HQ"))
                return RedirectToAction("Index", "GarageSupplies");
			else if (Roles.IsUserInRole("STC Site") || Roles.IsUserInRole("STC HQ") || Roles.IsUserInRole("STC Borough"))
				return RedirectToAction("Index", "Stc");
			else if (Roles.IsUserInRole("Radio Admin") || Roles.IsUserInRole("Radio District"))
				return RedirectToAction("Radio", "Admin");
			else
                return View();
        }

        /// <summary>
        /// If the index is posted to, try to log user on
        /// </summary>
        /// <param name="model">The log on model</param>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try { 
                    // validate user
                    if (_membershipService.ValidateUser(model.UserName, model.Password))
                    {
                        // have form service create login cookie
                        _formsService.SignIn(model.UserName, false);

						// if user is administrator, check if ip is in config list of allowed ips
						if (Roles.IsUserInRole(model.UserName, "Administrator"))
						{
							string allowedIpAddresses = ConfigurationManager.AppSettings["AllowedIpAddresses"];
							string allowedComputerNames = ConfigurationManager.AppSettings["AllowedComputerNames"];

							string ipAddress = Request.UserHostAddress;
							string computerName = System.Net.Dns.GetHostName();

							if (string.IsNullOrEmpty(ipAddress) && string.IsNullOrEmpty(computerName))
							{
								ModelState.AddModelError("LoginError", "Your ip address and computer name were not able to be resolved and are needed for authentication.");
								_formsService.SignOut();
							}
							else if (allowedIpAddresses.IndexOf(ipAddress) == -1 && allowedComputerNames.IndexOf(computerName) == -1)
							{
								string errorMessage = "You are not authorized to login. Please contact the help desk for assistance.";

								if (ConfigurationManager.AppSettings["AllowedIpErrorMessage"] != null) {
									errorMessage = ConfigurationManager.AppSettings["AllowedIpErrorMessage"];
								}

								ModelState.AddModelError("LoginError", errorMessage);
								ModelState.AddModelError("LoginInfo", String.Format("Your IP address is: {0}, your computer name is: {1}", ipAddress, computerName));

								_formsService.SignOut();
							}
							else
							{
								// check for return url and redirect
								if (!String.IsNullOrEmpty(returnUrl))
								{
									return Redirect(returnUrl);
								}
								else
								{
									return RedirectToAction("Index", "Home");
								}
							}
						}
						else
						{
							// check for return url and redirect
							if (!String.IsNullOrEmpty(returnUrl))
							{
								return Redirect(returnUrl);
							}
							else
							{
								return RedirectToAction("Index", "Home");
							}
						}
					}
					else
                    {
                        ModelState.AddModelError("LoginError", "The user name or password provided is incorrect.");
                    }
                } 
                catch (Exception ex)
                {
                    ModelState.AddModelError("LoginError", ex.Message);
                }
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("LoginError", "There was a problem logging in.  Please contact a system administrator.");
            return View(model);
        }

        /// <summary>
        /// Logs user the off and redirects to the index.
        /// </summary>
        /// <returns></returns>
        public ActionResult LogOff()
        {
            _formsService.SignOut();

            return RedirectToAction("Index", "Home");
        }

        #endregion
    }
}