﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;

using DSNY.Common.Logger;
using DSNY.Common.Exception;

using DSNY.Core.Interfaces;
using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;
using System.Web.Security;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for index pages
    /// </summary>
    
    public class ToolsMaintenanceController : BaseController
    {
        #region Variables

        private DSNYContext _dataProvider = null;
        private IUserRepository _userRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolsMaintenanceController"/> class.
        /// </summary>
        /// <param name="IoCDataProvider">The inversion of control data prodiver.</param>
        /// <param name="IoCUserRepo">The inversion of control user repo.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public ToolsMaintenanceController(DSNYContext IoCDataProvider, IUserRepository IoCUserRepo, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _dataProvider = IoCDataProvider;
            _userRepository = IoCUserRepo;
        }

        #endregion

        #region Actions

        [Authorize(Roles = "GS Warehouse, GS HQ")]
        public ActionResult Index()
        {
            return ToolTypes();
        }

        /// <summary>
        /// When hitting the home, redirect to appropriate homepage for user group
        /// If we have a user in multiple
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "GS Warehouse, GS HQ")]
        public ActionResult ToolTypes()
        {
            ToolTypesViewModel model = new ToolTypesViewModel()
            {
                measurements = Mapper.Map<List<MeasurementViewModel>>(_dataProvider.DataContext.Measurements.Where(m => m.is_active == true && m.is_Tool == true).ToList()),
                toolTypes = Mapper.Map<List<ToolTypeViewModel>>(_dataProvider.DataContext.Tool_Type.Where(t => t.is_Active == true).ToList())
            };

            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");

            return View("~/Views/GarageSupplies/ToolTypes.aspx", model);
        }

        [HttpGet]
        [Authorize(Roles = "GS Warehouse, GS HQ")]
        public ActionResult GetToolTypes(bool? showInactive)
        {
            List<Tool_Type> toolTypes = _dataProvider.DataContext.Tool_Type
                    .Where(t => showInactive.HasValue && showInactive == true ? true : t.is_Active == true)
                    .ToList();

            return Json(Mapper.Map<List<ToolTypeViewModel>>(toolTypes), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize(Roles = "GS Warehouse, GS HQ")]
        public ActionResult SaveToolTypes(List<ToolTypeViewModel> toolTypes)
        {
            if (toolTypes == null) { return Json("Nothing to save"); }

            try
            {
                var table = _dataProvider.DataContext.Tool_Type;
                List<IUser> users = _userRepository.getRolesUsers(new List<string>() { "gs district", "gs warehouse" });

                // loop over recieved Tool_Types and either update or add
                toolTypes.ForEach(tt => {
                    var item = Mapper.Map<Tool_Type>(tt);

                    // try to find tool type
                    if (tt.Tool_Type_id > 0)
                    {
                        var row = table.Where(x => x.Tool_Type_id == tt.Tool_Type_id).FirstOrDefault();   

                        if (row == null)
                        {
                            // add users to Tool_Type for Tool_User table if not a powertool
                            if (item.is_PowerTool == false)
                            {
                                users.ForEach(u =>
                                    item.Tool_User.Add(new Tool_User()
                                    {
                                        is_Active = item.is_Active,
                                        Site_Quota = 0,
                                        Tool_Type_id = item.Tool_Type_id,
                                        Userid = u.userId
                                    }));
                            }

                            table.AddObject(item);
                        }
                        else
                        {
                            row.Tool_Type_Name = item.Tool_Type_Name;
                            row.Measurement_id = item.Measurement_id;
                            row.is_Active = item.is_Active;
                            row.is_PowerTool = item.is_PowerTool;
                            row.Quantity_Per_Measurement = item.Quantity_Per_Measurement;

                            // gather all make ids
                            List<int> makeIds = item.Makes
                                .Where(x => x.Make_id > 0)
                                .GroupBy(x => x.Make_id)
                                .Select(make => make.First().Make_id)
                                .ToList();
                            
                            // delete missing makes
                            row.Makes
                                .Where(x => !makeIds.Contains(x.Make_id))
                                .ToList()
                                .ForEach(x =>
                                {
                                    x.Models.ToList().ForEach(y => _dataProvider.DataContext.DeleteObject(y));
                                    _dataProvider.DataContext.DeleteObject(x);
                                });

                            // add/update makes
                            item.Makes.ToList()
                                .ForEach(m =>
                                {
                                    if (m.Make_id == 0)
                                    {
                                        row.Makes.Add(m);
                                    }
                                    else if (row.Makes.Count(x => x.Make_id == m.Make_id) == 1)
                                    {
                                        var make = row.Makes.Single(x => x.Make_id == m.Make_id);
                                        make.is_Active = m.is_Active;
                                        make.Make_Name = m.Make_Name;

                                        List<int> modelIds = m.Models
                                            .Where(x => x.Model_id > 0)
                                            .GroupBy(x => x.Model_id)
                                            .Select(model => model.First().Model_id)
                                            .ToList();

                                        // delete models
                                        make.Models
                                            .Where(x => !modelIds.Contains(x.Model_id))
                                            .ToList()
                                            .ForEach(x => make.Models.Remove(x));

                                        // add/update models
                                        m.Models.ToList()
                                            .ForEach(mo =>
                                            {
                                                if (mo.Model_id == 0)
                                                {
                                                    make.Models.Add(mo);
                                                }
                                                else if (make.Models.Count(x => x.Model_id == mo.Model_id) == 1)
                                                {
                                                    var model = make.Models.Single(x => x.Model_id == mo.Model_id);
                                                    model.Model_Name = mo.Model_Name;
                                                }
                                            });
                                    }
                                });
                        }
                    }
                    else
                    {
                        table.AddObject(item);

                        _dataProvider.DataContext.SaveChanges();

                        // add users to Tool_Type for Tool_User table if not a powertool
                        if (item.is_PowerTool == false)
                        {
                            users.ForEach(u =>
                                item.Tool_User.Add(new Tool_User()
                                {
                                    is_Active = item.is_Active,
                                    Site_Quota = 0,
                                    Tool_Type_id = item.Tool_Type_id,
                                    Userid = u.userId
                                }));
                        }
                    }
                });

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }

            return Json("Success");
        }

        /// <summary>
        /// When hitting the home, redirect to appropriate homepage for user group
        /// If we have a user in multiple
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "GS Warehouse, GS HQ")]
        public ActionResult Maintain()
        {
            Guid warehouseUserGuid = _userRepository.getRolesUsers(new List<string>() { "gs warehouse" }).First().userId;

            ToolMaintenanceViewModel model = new ToolMaintenanceViewModel()
            {
                measurements = Mapper.Map<List<MeasurementViewModel>>(_dataProvider.DataContext.Measurements.Where(x => x.is_active == true && x.is_Tool == true).ToList()),
                toolTypes = Mapper.Map<List<ToolTypeViewModel>>(_dataProvider.DataContext.Tool_Type.Where(x => x.is_Active == true).ToList()),
                standardInventory = Mapper.Map<List<StandardToolsInventoryViewModel>>(_dataProvider.DataContext.Standard_Tools_Inventory
                    .Where(x => x.Tool_User.Userid == warehouseUserGuid).ToList())
                    .OrderBy(x => x.Tool_User.Tool_Type.Tool_Type_Name)
                    .ToList(),
                users = _userRepository.getRolesUsers(new List<string>() { "gs borough", "gs district", "gs warehouse", "gs hq" }).OrderBy(u => u.userName).ToList(),
                workflowActions = Mapper.Map<List<WorkflowActionViewModel>>(_dataProvider.DataContext.Workflow_Action.ToList()),
            };

            // only get district and borough

            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");

            return View("~/Views/GarageSupplies/ToolMaintenance.aspx", model);
        }

        [HttpPost]
        [Authorize(Roles = "GS Warehouse, GS HQ")]
        public ActionResult GetLocationTool(Guid? userId, int? standardToolTypeId, int? powerToolTypeId, bool? showInactive)
        {
            List<Tool_User> toolUsers = new List<Tool_User>();
            List<Power_Tool_Inventory> powerToolInventory = new List<Power_Tool_Inventory>();

            if (userId.HasValue || powerToolTypeId.HasValue)
            {
                powerToolInventory = _dataProvider.DataContext.Power_Tool_Inventory
                        .Where(t =>
                            (userId.HasValue ? t.UserId == userId : true == true) &&
                            (powerToolTypeId.HasValue ? t.Tool_Type_id == powerToolTypeId : true) &&
                            (showInactive.HasValue && showInactive == true ? true : t.is_Active == true))
                        .OrderBy(t => t.aspnet_Users.UserName)
                        .ThenBy(t => t.Tool_Type.Tool_Type_Name)
                        .ToList();
            }

            if (userId.HasValue || standardToolTypeId.HasValue)
            {
                toolUsers = _dataProvider.DataContext.Tool_User
                        .Where(t =>
                            (userId.HasValue ? t.Userid == userId : true == true) &&
                            (standardToolTypeId.HasValue ? t.Tool_Type_id == standardToolTypeId : true) &&
                            (showInactive.HasValue && showInactive == true ? true : t.is_Active == true))
                        .OrderBy(t => t.aspnet_Users.UserName)
                        .ThenBy(t => t.Tool_Type.Tool_Type_Name)
                        .ToList();
            }

            return Json(new LocationToolsViewModel()
            {
                powerToolInventory = Mapper.Map<List<PowerToolInventoryViewModel>>(powerToolInventory),
                siteQuotas = Mapper.Map<List<ToolUserViewModel>>(toolUsers)
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize(Roles = "GS Warehouse, GS HQ")]
        public ActionResult GetInventoryHistory(int standardToolsInventoryId)
        {
            var workflowHistory = Mapper.Map<List<StandardToolsLedgerViewModel>>
                (_dataProvider.DataContext.Standard_Tools_Ledger.Where(w => w.Standard_Tools_Inventory_id == standardToolsInventoryId).ToList());

            return Content(Newtonsoft.Json.JsonConvert.SerializeObject(workflowHistory).Replace("'", "\'"), "application/json");
        }

        /// <summary>
        /// When hitting the home, redirect to appropriate homepage for user group
        /// If we have a user in multiple
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "GS Warehouse, GS HQ")]
        public ActionResult SaveMaintain(ToolMaintenanceSaveModel toolMaintenance)
        {
            if (toolMaintenance == null) { return Json("Nothing to save"); }

            Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;

            try
            {
                if (toolMaintenance.siteQuotas != null && toolMaintenance.siteQuotas.Count > 0)
                {
                    // Tool_User Updates
                    var toolUserTable = _dataProvider.DataContext.Tool_User;

                    // loop over recieved Tool_Users and either update or add
                    toolMaintenance.siteQuotas.ForEach(tu => {
                        var item = Mapper.Map<Tool_User>(tu);

                        // try to find tool type
                        var row = toolUserTable.SingleOrDefault(x => x.Tool_User_id == tu.Tool_User_Id);

                        if (row != null)
                        {
                            row.Site_Quota = item.Site_Quota;
                            row.is_Active = item.is_Active;
                        }
                    });
                }

                _dataProvider.DataContext.SaveChanges();

                // Standard_Tools_Inventory Updates
                var standardToolsTable = _dataProvider.DataContext.Standard_Tools_Inventory;
                var standardToolsLedgerTable = _dataProvider.DataContext.Standard_Tools_Ledger;

                if (toolMaintenance.warehouseRecieved != null && toolMaintenance.warehouseRecieved.Count > 0)
                {
                    toolMaintenance.warehouseRecieved.ForEach(wr =>
                    {
                        standardToolsLedgerTable.AddObject(new Standard_Tools_Ledger()
                        {
                            Standard_Tools_Inventory_id = wr.Standard_Tools_Inventory_id,
                            Qty = wr.qty,
                            Comment = wr.comment,
                            InsertUserid = currentUserGuid,
                            InsertDateTime = DateTime.Now,
                            Action_Id = _dataProvider.DataContext.Workflow_Action.SingleOrDefault(x => x.Workflow_cd == "WHRCV").Workflow_Action_id
                        });

                        var row = standardToolsTable.SingleOrDefault(x => x.Standard_Tools_Inventory_id == wr.Standard_Tools_Inventory_id);

                        if (row != null)
                        {
                            row.Current_On_Hand += wr.qty;
                            row.Last_Update_Date = DateTime.Now;
                        }
                    });
                }

                if (toolMaintenance.warehouseSent != null && toolMaintenance.warehouseSent.Count > 0)
                {
                    toolMaintenance.warehouseSent.ForEach(ws =>
                    {
                        standardToolsLedgerTable.AddObject(new Standard_Tools_Ledger()
                        {
                            Standard_Tools_Inventory_id = ws.Standard_Tools_Inventory_id,
                            Qty = ws.qty,
                            Comment = ws.comment,
                            Send_Userid = ws.sentTo,
                            InsertUserid = currentUserGuid,
                            InsertDateTime = DateTime.Now,
                            Action_Id = _dataProvider.DataContext.Workflow_Action.SingleOrDefault(x => x.Workflow_cd == "EMSND").Workflow_Action_id
                        });

                        var row = standardToolsTable.SingleOrDefault(x => x.Standard_Tools_Inventory_id == ws.Standard_Tools_Inventory_id);

                        if (row != null)
                        {
                            row.Current_On_Hand -= ws.qty;
                            row.Last_Update_Date = DateTime.Now;
                        }

                        // find standard tool inventory tool_user, add qty
                    });
                }

                _dataProvider.DataContext.SaveChanges();

                if (toolMaintenance.powerToolInventory != null && toolMaintenance.powerToolInventory.Count > 0)
                {
                    // Power_Tool_Inventory Updates
                    var powerToolTable = _dataProvider.DataContext.Power_Tool_Inventory;

                    // loop over recieved Power_Tool_Inventory and either update or add
                    toolMaintenance.powerToolInventory.ForEach(pt =>
                    {
                        var item = Mapper.Map<Power_Tool_Inventory>(pt);
                        bool added = false;

                        // try to find tool type
                        if (pt.Power_Tool_Inventory_Id > 0)
                        {
                            var row = powerToolTable.Where(x => x.Power_Tool_Inventory_Id == pt.Power_Tool_Inventory_Id).FirstOrDefault();

                            if (row == null)
                            {
                                powerToolTable.AddObject(item);
                                added = true;
                            }
                            else
                            {
                                row.is_Active = item.is_Active;
                                row.Tool_Type_id = item.Tool_Type_id;
                                row.Make_id = item.Make_id;
                                row.Model_id = item.Model_id;
                                row.Serial_Number = item.Serial_Number;
                                row.DSNY_Number = item.DSNY_Number;
                                row.UserId = item.UserId;
                            }
                        }
                        else
                        {
                            powerToolTable.AddObject(item);
                            added = true;
                        }

                        _dataProvider.DataContext.SaveChanges();

                        if (added)
                        {
                            // add to workflow tracking table
                            _dataProvider.DataContext.Workflow_Tracking.AddObject(new Workflow_Tracking()
                            {
                                Workflow_Action_id = _dataProvider.DataContext.Workflow_Action.SingleOrDefault(x => x.Workflow_cd == "ADDCMS").Workflow_Action_id,
                                Userid = currentUserGuid,
                                Comment = string.Empty,
                                Workflow_Datetime = DateTime.Now,
                                Table_Association = "Power_Tool_Inventory",
                                Table_Id = item.Power_Tool_Inventory_Id
                            });
                        }
                    });
                }

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }

            return Json("Success");
        }
        #endregion
    }
}