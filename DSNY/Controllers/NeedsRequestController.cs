﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using DSNY.Core.Interfaces;
using DSNY.Common;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Core.Repository;
using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;
using System.Net.Mail;
using DSNY.Common.Utilities;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the dashboard area
    /// </summary>
    
    public class NeedsRequestController : BaseController
    {
        private IUserRepository _userRepository;
        private IRoleRepository _roleRepository;
        private DSNYContext _context;
        private SiteOnHandRepository _siteOnHandRepository;

        public NeedsRequestController(IUserRepository IoCUserRepo, IRoleRepository IoCRoleRepository, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler, DSNYContext IoCContext)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _userRepository = IoCUserRepo;
            _roleRepository = IoCRoleRepository;
            _context = IoCContext;
            _siteOnHandRepository = new SiteOnHandRepository(_context);
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC Site, STC HQ")]
        public ActionResult Index()
        {
            var currentUser = _userRepository.getUser((Guid)Membership.GetUser().ProviderUserKey);
            
            var inputModel = new NeedsRequestViewModel();
            inputModel.CurrentUser = new Dropdown { id = currentUser.userId.ToString(), description = currentUser.userName, visible = true };
            inputModel.Finalized = false;
            var model = GetViewModel(inputModel);

            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");
            return View(model);
        }

        private NeedsRequestViewModel GetViewModel(NeedsRequestViewModel viewModel)
        {
            var model = new NeedsRequestViewModel();
            List<IUser> users = null;
            users = _userRepository.getStcUsersWithActiveSiteOnHand(new Guid(viewModel.CurrentUser.id));
            var siteOnHands = _siteOnHandRepository.GetByUsers(users);
            model.CurrentUser = viewModel.CurrentUser;
            model.Sites = Mapper.Map<List<Dropdown>>(users
                .Select(x => new { id = x.userId.ToString(), description = x.userName, visible = true }).Distinct().ToList());
            model.SiteOnHands = Mapper.Map<List<SiteOnHand>>(siteOnHands);
            if (model.Sites.Any())
            {
                var site = new Guid(model.Sites[0].id);
                var products = _context.DataContext.STC_Product_User.Where(x => x.STC_User_ID == site)
                    .Select(x => new { id = x.STC_Product_ID, description = x.STC_Product.STC_Product_Name, visible = true }).ToList();
                model.Products = Mapper.Map<List<Dropdown>>(products);
                if (model.Products.Any())
                {
                    var needsRequests = _context.DataContext.STC_Needs_Request.AsQueryable();
                    if (viewModel.CurrentUser != null)
                    {
                        var userIds = model.Sites.Select(x => new Guid(x.id)).ToList();
                        needsRequests = needsRequests.Where(x => userIds.Contains( x.Userid.Value ));
                    }
                    if (viewModel.RequestDate != null)
                    {
                        model.RequestDate = viewModel.RequestDate;
                        DateTime requestDate = DateTime.MinValue;
                        if (DateTime.TryParse(viewModel.RequestDate, out requestDate))
                        {
                            needsRequests = needsRequests.Where(x => x.Request_Date == requestDate);
                        }
                    }
                    if (viewModel.SelectedSite != null)
                    {
                        model.SelectedSite = viewModel.SelectedSite;
                        needsRequests = needsRequests.Where(x => x.Userid == new Guid(viewModel.SelectedSite));
                    }
                    model.Finalized = viewModel.Finalized;
                    if (!viewModel.Finalized)
                    {
                        needsRequests = needsRequests.Where(x => !x.Confirm_Date.HasValue);
                    }
                    model.NeedsRequests = Mapper.Map<List<NeedsRequest>>(needsRequests.OrderBy(x => x.Request_Date)
                        .ThenBy(x => x.STC_Product_id).ToList());
                    foreach (var request in model.NeedsRequests)
                    {
                        request.Sites = model.Sites;
                    }
                    var currentSiteOnHand = model.SiteOnHands.Where(x => x.ProductId == int.Parse(model.Products[0].id) && x.UserId == model.Sites[0].id).FirstOrDefault();
                    model.BlankNeedsRequest = new NeedsRequest
                    {
                        New = true,
                        Products = model.Products,
                        ProductId = products[0].id,
                        Product = products[0].description,
                        Sites = model.Sites,
                        UserId = users[0].userId.ToString(),
                        User = users[0].userName,
                        RequestDate = DateTime.Now.ToShortDateString(),
                        Qty = (((decimal)currentSiteOnHand.Capacity) - decimal.Parse(currentSiteOnHand.Qty)).ToString()
                    };
                }
            }
            return model;
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC Site, STC HQ")]
        public ActionResult ChangeFilter(NeedsRequestViewModel viewModel)
        {
            return Json(GetViewModel(viewModel));
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC Site, STC HQ")]
        public ActionResult GetProducts(string id)
        {
            var site = new Guid(id);
            return Json(Mapper.Map<List<Dropdown>>(_context.DataContext.STC_Product_User.Where(x => x.STC_User_ID == site)
                .Select(x => new { id = x.STC_Product_ID, description = x.STC_Product.STC_Product_Name, visible = true }).ToList()));
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC Site, STC HQ")]
        public ActionResult Confirm(NeedsRequest value)
        {
            if (value == null) { return Json("Nothing to save"); }
            value.ConfirmDate = DateTime.Now.ToShortDateString();
            var table = _context.DataContext.STC_Needs_Request;
            if (value.Deleted)
            {
                var request = table.Where(x => x.STC_Needs_Request_id == value.Id).FirstOrDefault();
                if (request != null) { _context.DataContext.DeleteObject(request); }
            }
            else
            {
                if (value.Id.HasValue && (value.Id.Value > 0))
                {
                    var row = table.Where(x => x.STC_Needs_Request_id == value.Id).FirstOrDefault();
                    if (row == null)
                    {
                        table.AddObject(NewRow(value));
                    }
                    else
                    {
                        Update(row, value);
                    }
                }
                else
                {
                    table.AddObject(NewRow(value));
                }
            }
            _context.DataContext.SaveChanges();

            try
            {
                var measurement = _context.DataContext.STC_Product.Where(x => x.STC_Product_id == value.ProductId.Value).Select(x => x.Measurement.Measurement_Name).FirstOrDefault();
                // set up the mail message going out
                MailMessage mm = new MailMessage();

                // setup message
                mm.Subject = string.Format("{0}/{1} Needs Request Reviewed By HQ", value.User, value.Product);
                mm.IsBodyHtml = false;
                //mm.BodyEncoding = Encoding.UTF8;
                mm.Body = string.Format(@"HQ has reviewed your request created on {0} for {1} {2} of {3}.",
                value.RequestDate, value.Qty, measurement, value.Product);

                IUser user = _userRepository.getUser(new Guid(value.UserId));
                mm.To.Add(new MailAddress(user.email, string.IsNullOrEmpty(user.fullName) ? string.Empty : user.fullName));

                EmailUtilities.SendEmail(mm);
            }
            catch (Exception ex)
            {
                return Json(string.Format("An email could not be sent due to the following error: {0}", ex.Message));
            }

            return Json("Success");
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC Site, STC HQ")]
        public ActionResult Save(List<NeedsRequest> values)
        {
            //var values = new List<OnHandViewModel>();
            if (values == null || !values.Any()) { return Json("Nothing to save");  throw new Exception("There were no items to save."); }
            var data = _context.DataContext.STC_Needs_Request;
            foreach (var o in values)
            {
                if (o.Deleted)
                {
                    _context.DataContext.ExecuteStoreCommand(string.Format("DELETE FROM [STC_Needs_Request] WHERE STC_Needs_Request_id = {0}", o.Id));
                }
                else
                {
                    var row = data.Where(x => x.STC_Needs_Request_id == o.Id).FirstOrDefault();
                    if (row == null)
                    {
                        data.AddObject(NewRow(o));

                        try
                        {
                            var measurement = _context.DataContext.STC_Product.Where(x => x.STC_Product_id == o.ProductId.Value).Select(x => x.Measurement.Measurement_Name).FirstOrDefault();
                            // set up the mail message going out
                            MailMessage mm = new MailMessage();

                            // setup message
                            mm.Subject = string.Format("{0}/{1} Needs Request", o.User, o.Product);
                            mm.IsBodyHtml = false;
                            //mm.BodyEncoding = Encoding.UTF8;
                            mm.Body = string.Format(@"A {0} needs request has been created on {1} for {2} {3} of {4}.
Please login to CMS.
Got to the STC Menu option and select Commodity Needs Request.
After reviewing, click on the confirm button to notify {0} and the borough that the request has been reviewed by HQ.", 
                            o.User, o.RequestDate, o.Qty, measurement, o.Product);

                            List<IUser> users = _userRepository.getRolesUsers(new List<string>(new string[] { "STC HQ" }));

                            users.ForEach(user =>
                            {
                                mm.To.Add(new MailAddress(user.email, string.IsNullOrEmpty(user.fullName) ? string.Empty : user.fullName));
                            });

                            EmailUtilities.SendEmail(mm);
                        } catch (Exception ex)
                        {
                            return Json(string.Format("An email could not be sent due to the following error: {0}", ex.Message));
                        }
                    }
                    else
                    {
                        Update(row, o);
                    }
                }
            }
            _context.DataContext.SaveChanges();
            
            return Json("Success");
        }

        private void Update(STC_Needs_Request row, NeedsRequest o)
        {
            DateTime requestDate;
            DateTime confirmDate;
            row.Userid = new Guid(o.UserId);
            row.STC_Product_id = o.ProductId;
            row.Request_Qty = decimal.Parse(o.Qty);
            row.Request_Date = DateTime.TryParse(o.RequestDate, out requestDate) ? requestDate : (DateTime?)null;
            row.Boro_Super_Name = o.Supervisor;
            row.Boro_Super_Badge = o.Badge;
            row.Confirm_Date = DateTime.TryParse(o.ConfirmDate, out confirmDate) ? confirmDate : (DateTime?)null;
        }

        private STC_Needs_Request NewRow(NeedsRequest o)
        {
            DateTime requestDate;
            DateTime confirmDate;
            return new STC_Needs_Request
            {
                Userid = new Guid(o.UserId),
                STC_Product_id = o.ProductId,
                Request_Qty = decimal.Parse(o.Qty),
                Request_Date = DateTime.TryParse(o.RequestDate, out requestDate) ? requestDate : (DateTime?)null,
                Boro_Super_Name = o.Supervisor,
                Boro_Super_Badge = o.Badge,
                Confirm_Date = DateTime.TryParse(o.ConfirmDate, out confirmDate) ? confirmDate : (DateTime?)null
            };
        }
    }
}