﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.Mvc;

using DSNY.Common;
using DSNY.Common.Exception;
using DSNY.Common.Logger;
using DSNY.Common.Utilities;
using DSNY.Core.Interfaces;
using DSNY.Core.Models;
using DSNY.Core.Repository;
using DSNY.Models;
using DSNY.ViewModels;
using DSNY.Data;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for user pages
    /// </summary>
    public class UserController : BaseController
    {
        #region Variables

        private DSNYContext _dataProvider = null;
        private IUserRepository _userRepository { get; set; }
        private IRoleRepository _roleRepository { get; set; }
        private IProductRepository _productRepository { get; set; }
        private IMessageRepository _messageRepository { get; set; }
        private IEquipmentRepository _equipmentRepository { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UserController"/> class.
        /// </summary>
        /// <param name="IoCUserRepository">The inversion of control user repository.</param>
        /// <param name="IoCRoleRepository">The inversion of control role repository.</param>
        /// <param name="IoCProductRepository">The inversion of control product repository.</param>
        /// <param name="IoCEquipmentRepository">The inversion of control equipment repository.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public UserController(DSNYContext IoCDataProvider, IUserRepository IoCUserRepository, IRoleRepository IoCRoleRepository, 
            IProductRepository IoCProductRepository, IMessageRepository IoCMessageRepository,
            IEquipmentRepository IoCEquipmentRepository, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _dataProvider = IoCDataProvider;
            _userRepository = IoCUserRepository;
            _roleRepository = IoCRoleRepository;
            _productRepository = IoCProductRepository;
            _messageRepository = IoCMessageRepository;
            _equipmentRepository = IoCEquipmentRepository;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
        }

        #endregion

        #region List

        /// <summary>
        /// Lists the users.  Pushes off to a specific page in View structure.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult List()
        {
            List<IUser> users = _userRepository.getAllUsers(false);

            return View("~/Views/Admin/User/List.aspx", users);
        }

        #endregion

        #region Create

        /// <summary>
        /// Create form for user.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Create()
        {
            // Set our view data variable to hold all the possible roles
            ViewData["Roles"] = _roleRepository.getAllRoles();
            ViewData["Products"] = _productRepository.getAllProducts(false, true);
            ViewData["Equipment"] = _equipmentRepository.getAllEquipment(false);
            ViewData["Users"] = _userRepository.getAllUsers(false);
            ViewData["SelectedUsers"] = new SelectList(new List<IUser>(), "userId", "fullName");

            return View("~/Views/Admin/User/Create.aspx");
        }

        /// <summary>
        /// HTTP Post create action.  Creates a user.
        /// </summary>
        /// <param name="model">The user model.</param>
        /// <param name="collection">The form collection.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult Create(NewUserModel model, FormCollection collection)
        {
            // Dummy value that we'll never use but can use to check against
            MembershipCreateStatus createStatus = MembershipCreateStatus.InvalidAnswer;
			bool hasError = false;

            if (string.IsNullOrEmpty(model.email))
            {
                model.email = string.Empty;
            }

			// Do a double check on passwords matching, since the MVC validation doesn't work in some cases
			if (!string.IsNullOrEmpty(model.password))
			{
				var countAlpha = model.password.Count(char.IsLetter);
				var countNonAlpha = model.password.Length - countAlpha;
				// Do validation checks for user information
				if (model.password != model.confirmPassword)
				{
					ModelState.AddModelError("Password", "The password and confirmation password do not match.");
					hasError = true;
				}
				else if (model.password.Length < Membership.Provider.MinRequiredPasswordLength)
				{
					ModelState.AddModelError("Password", "The password must be at least " + Membership.Provider.MinRequiredPasswordLength.ToString() + " characters.");
					hasError = true;
				}
				else if (countNonAlpha < 1 || countAlpha < 1)
				{
					ModelState.AddModelError("Password", "The password must have at least 1 alphabetic character and at least 1 numeric or special character.");
					hasError = true;
				}
			}

			if (!hasError)
			{
				// Initalize the roles
				model.role = new List<IRole>();

				// Fill the model's role from the form's collection object
				if (collection["role"] != null)
				{
					string[] selectedRoles = collection["role"].Split(',');

					foreach (string role in selectedRoles)
					{
						model.role.Add(new Role() { roleName = role });
					}
				}

				createStatus = _userRepository.addUser(model.userName, model.password, model.description, model.address, model.phoneNumber, model.borough, model.poDisplayName, model.district, model.email, model.role, model.isDistribution, model.isEmail, model.isActive);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    // Get our new user
                    IUser newUser = _userRepository.getUser(model.userName);

                    // Add tool_user associations if user in appropriate role
                    if (model.role.Where(r => r.roleName == "GS District" || r.roleName == "GS Warehouse").Count() > 0)
                    {
                        var toolTypes = _dataProvider.DataContext.Tool_Type.Where(t => t.is_Active == true && t.is_PowerTool == false).ToList();
                        List<Tool_User> toolUsers = new List<Tool_User>();

                        toolTypes.ForEach(t =>
                        {
                            Tool_User tempToolUser = new Tool_User()
                            {
                                is_Active = true,
                                Site_Quota = 0,
                                Tool_Type_id = t.Tool_Type_id,
                                Userid = newUser.userId
                            };

                            _dataProvider.DataContext.Tool_User.AddObject(tempToolUser);
                            toolUsers.Add(tempToolUser);
                        });

                        _dataProvider.DataContext.SaveChanges();

                        toolUsers.ForEach(t =>
                        {
                            _dataProvider.DataContext.Standard_Tools_Inventory.AddObject(new Standard_Tools_Inventory()
                            {
                                Tool_User = t,
                                Current_On_Hand = 0,
                                Last_Update_Date = DateTime.Now
                            });
                        });
                    }

                    // declare the user product variables
                    int prodId = -1, equipId = -1, prodQty = -1, tempProdId = -1, equipCapacity = -1;
                    int? prodSubTypeId = null;
                    int tempInt;
                    Guid userId = Guid.Empty;

                    // declare the user equipment variables
                    int equipCount = 0;
                    bool activeProduct = false;
                    List<string> equipDescripts = new List<string>(), equipCapacities = new List<string>(), equipActive = new List<string>();
                    Dictionary<int, int> productEquipCapacity = new Dictionary<int, int>();

                    // Take our return collection object holding the GUID of the users, split it and loop over to add
                    if (collection["usersSelected"] != null)
                    {
                        string[] userGuids = collection["usersSelected"].Split(',');

                        foreach (string item in userGuids)
                        {
                            Guid.TryParse(item, out userId);

                            if (userId != Guid.Empty)
                                _userRepository.addUserToCommandChain(newUser.userId, userId);

                            userId = Guid.Empty;
                        }
                    }

                    foreach (string key in collection.Keys)
                    {
                        if (key.Contains("product"))
                        {
                            // get product ID from the key
                            int.TryParse(key.Replace("product", string.Empty), out prodId);

                            // if we have a good product ID, get quantity and then updated product
                            if (prodId > -1)
                            {
                                int.TryParse(collection["prodQty" + prodId.ToString()], out prodQty);
                                prodSubTypeId = int.TryParse(collection["prodSubType" + prodId.ToString()], out tempInt) ? (int?)tempInt : null;

                                if (prodQty > 0)
                                    _productRepository.addUserToProduct(prodId, newUser.userId, prodQty, prodSubTypeId);
                                else
                                    _productRepository.addUserToProduct(prodId, newUser.userId, prodQty, prodSubTypeId);
                            }

                            prodId = -1;
                        }
                        else if (key.Contains("equipDescript."))
                        {
                            // grab equip ID from key
                            int.TryParse(key.Replace("equipDescript.", string.Empty), out equipId);

                            // if we have a good equip ID and a related product exists for this equip
                            if (equipId > 0 && collection["equipProd." + equipId] != null)
                            {
                                // get descriptions, capacities, active for one equip type
                                equipDescripts.AddRange(collection["equipDescript." + equipId].Split(','));
                                equipCapacities.AddRange(collection["equipCapacity." + equipId].Split(','));
                                equipActive.AddRange(collection["equipActive." + equipId].Split(','));

                                // for each product related to this equipment
                                foreach (string productId in collection["equipProd." + equipId].Split(','))
                                {
                                    // get product ID and capacity
                                    int.TryParse(productId, out tempProdId);
                                    int.TryParse(equipCapacities[equipCount], out equipCapacity);

                                    // if active
                                    if (equipActive[equipCount] != null)
                                        activeProduct = equipActive[equipCount] == "on";

                                    // if we have a prod ID and a description, create user - product - equip record
                                    if (tempProdId > -1 && !string.IsNullOrEmpty(equipDescripts[equipCount].ToString()))
                                    {
                                        if (equipCapacity > 0)
                                        {
                                            if (productEquipCapacity.Keys.Contains(tempProdId))
                                                productEquipCapacity[tempProdId] = productEquipCapacity[tempProdId] + equipCapacity;
                                            else
                                                productEquipCapacity.Add(tempProdId, equipCapacity);
                                        }

                                        _equipmentRepository.addUserToEquipment(equipId, tempProdId, equipDescripts[equipCount], equipCapacity, activeProduct, newUser.userId);
                                    }

                                    // reset the local variables
                                    tempProdId = -1;
                                    equipCapacity = -1;
                                    equipCount++;
                                    activeProduct = false;
                                }

                                // clear the collections
                                equipDescripts.Clear();
                                equipCapacities.Clear();
                                equipActive.Clear();
                            }

                            // clear equip id and equip count
                            equipId = 0;
                            equipCount = 0;
                        }
                    }

                    // loop through capacities for equipment products and then update the product - user capacities
                    foreach (int key in productEquipCapacity.Keys)
                    {
                        _productRepository.updateProductUserCapacity(key, newUser.userId, productEquipCapacity[key]);
                    }

                    // if user is on the distribution list associate user with all past messages
                    if (model.isDistribution && newUser != null)
                        _messageRepository.updateNewUserWithCurrentMessages(newUser.userId);

                    return RedirectToAction("List");
                }
            }

            if (createStatus != MembershipCreateStatus.InvalidAnswer)
                ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));

            // Set our view data variables to hold all needed information
            ViewData["Roles"] = _roleRepository.getAllRoles();
            ViewData["Products"] = _productRepository.getAllProducts(false, true);
            ViewData["Equipment"] = _equipmentRepository.getAllEquipment(false);
            ViewData["Users"] = _userRepository.getAllUsers(false);
            ViewData["SelectedUsers"] = new SelectList(new List<IUser>(), "userId", "fullName");

            return View("~/Views/Admin/User/Create.aspx", model);
        }

        #endregion

        #region Edit

        /// <summary>
        /// Edits a user.
        /// </summary>
        /// <param name="name">The user name.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Edit(string name)
        {
            // Get our user and fill ViewModel
            EditUserModel editUser = new EditUserModel();
            IUser userToEdit = _userRepository.getUser(name);

            if (userToEdit != null)
            {
                editUser.userName = userToEdit.userName;
                editUser.description = userToEdit.description;
                editUser.email = userToEdit.email;
                editUser.address = userToEdit.address;
                editUser.phoneNumber = userToEdit.phoneNumber;
                editUser.borough = userToEdit.borough;
                editUser.poDisplayName = userToEdit.poDisplayName;
                editUser.district = userToEdit.district;
                editUser.role = userToEdit.roles.Count > 0 ? userToEdit.roles : new List<IRole>();
                editUser.isDistribution = userToEdit.isDistribution;
                editUser.isEmail = userToEdit.isEmail;
                editUser.isActive = userToEdit.isActive;
                editUser.isLocked = userToEdit.isLocked;
                editUser.userEquipment = _equipmentRepository.getEquipmentForUser(userToEdit.userId, true, true);
                editUser.userProducts = _productRepository.getProductsForUser(userToEdit.userId, true);
                editUser.userCommandChain = _userRepository.getUserCommandChain(userToEdit.userId);

                ViewData["EquipmentUser"] = editUser.userEquipment;
            }

            // Set our view data variable to hold all the possible roles
            ViewData["Roles"] = _roleRepository.getAllRoles();
            ViewData["Products"] = _productRepository.getAllProducts(false, true);
            ViewData["Users"] = _userRepository.getAllUsers(false);
            ViewData["Equipment"] = _equipmentRepository.getAllEquipment(false);
            ViewData["SelectedUsers"] = new SelectList(editUser.userCommandChain, "userId", "fullName");

            return View("~/Views/Admin/User/Edit.aspx", editUser);
        }

        /// <summary>
        /// HTTP Post edit action. Edit a user.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult Edit(EditUserModel model, FormCollection collection)
        {
            IUser updateUser = null;

            try
            {
                string[] keys = { "password", "email", "" };
                if (!string.IsNullOrEmpty(model.password))
                {
                    var countAlpha = model.password.Count(char.IsLetter);
                    var countNonAlpha = model.password.Length - countAlpha;
                    // Do validation checks for user information
                    if (model.password != model.confirmPassword)
                    {
                        ModelState.AddModelError("Password", "The password and confirmation password do not match.");
                    }
                    else if (model.password.Length < Membership.Provider.MinRequiredPasswordLength)
                    {
                        ModelState.AddModelError("Password", "The password must be at least " + Membership.Provider.MinRequiredPasswordLength.ToString() + " characters.");
                    }
                    else if (countNonAlpha < 1 || countAlpha < 1)
                    {
                        ModelState.AddModelError("Password", "The password must have at least 1 alphabetic character and at least 1 numeric or special character.");
                    }
                }
                else if (!string.IsNullOrEmpty(model.email) && !EmailCheck.IsEmail(model.email))
                {
                    ModelState.AddModelError("email", "Email address is invalid.");
                }
                if (!ModelState.Where(y => keys.Contains(y.Key)).Select(x => x.Value.Errors).Where(x => x.Count>0).Any())
                {
                    return ExecuteUpdate(model, collection, updateUser);
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                ModelState.AddModelError("", "Error: " + ex.Message);
            }

            // gets updated command chain for model
            model.userCommandChain = _userRepository.getUserCommandChain(_userRepository.getUser(model.userName).userId);

            // fill the view data with extra info
            ViewData["Roles"] = _roleRepository.getAllRoles();
            ViewData["Products"] = _productRepository.getAllProducts(false, true);
            ViewData["Equipment"] = _equipmentRepository.getAllEquipment(false);
            ViewData["Users"] = _userRepository.getAllUsers(false);
            ViewData["SelectedUsers"] = new SelectList(model.userCommandChain, "userId", "fullName");

            // if we have equipment for user, get them, otherwise create empty list
            if (updateUser != null)
                ViewData["EquipmentUser"] = _equipmentRepository.getEquipmentForUser(updateUser.userId, true, true);
            else
                ViewData["EquipmentUser"] = new List<Equipment_User>();

            return View("~/Views/Admin/User/Edit.aspx", model);
        }

        private ActionResult ExecuteUpdate(EditUserModel model, FormCollection collection, IUser updateUser)
        {

            // Make our EditUserModel into a IUser object to pass to our user repository
            updateUser = new User();
            updateUser.userName = model.userName;
            updateUser.description = model.description;
            updateUser.address = model.address;
            updateUser.phoneNumber = model.phoneNumber;
            updateUser.borough = model.borough;
            updateUser.poDisplayName = model.poDisplayName;
            updateUser.district = model.district;
            updateUser.email = model.email;
            updateUser.roles = new List<IRole>();
            updateUser.isDistribution = model.isDistribution;
            updateUser.isEmail = model.isEmail;
            updateUser.isActive = model.isActive;
            updateUser.isLocked = model.isLocked;

            // fill in the roles
            if (collection["role"] != null)
            {
                string[] selectedRoles = collection["role"].Split(',');

                foreach (string role in selectedRoles)
                {
                    updateUser.roles.Add(new Role() { roleName = role });
                }
            }

            // update
            _userRepository.updateUser(updateUser);

            // Change password if we need to
            if (!string.IsNullOrEmpty(model.password) && !string.IsNullOrEmpty(model.confirmPassword) &&
                    model.password == model.confirmPassword)
            {
                _userRepository.updatePassword(updateUser, model.password);
            }

            // Re-get our user with all their updated info
            updateUser = _userRepository.getUser(model.userName);

            // Add/activate or deactivate tool_user
            var rows = _dataProvider.DataContext.Tool_User.Where(x => x.Userid == updateUser.userId);

            // if user is district or warehouse, then add to tool_user table
            if (updateUser.roles.Where(r => r.roleName == "GS District" || r.roleName == "GS Warehouse").Count() > 0)
            {
                if (rows.Count() == 0)
                {
                    var toolTypes = _dataProvider.DataContext.Tool_Type.Where(t => t.is_Active == true && t.is_PowerTool == false).ToList();
                    List<Tool_User> toolUsers = new List<Tool_User>();

                    toolTypes.ForEach(t =>
                    {
                        var toolUser = _dataProvider.DataContext.Tool_User.SingleOrDefault(x => x.Userid == updateUser.userId && x.Tool_Type_id == t.Tool_Type_id);

                        if (toolUser != null)
                        {
                            toolUser.is_Active = true;
                        }
                        else
                        {
                            Tool_User tempToolUser = new Tool_User()
                            {
                                is_Active = true,
                                Site_Quota = 0,
                                Tool_Type_id = t.Tool_Type_id,
                                Userid = updateUser.userId
                            };

                            _dataProvider.DataContext.Tool_User.AddObject(tempToolUser);
                            toolUsers.Add(tempToolUser);
                        }
                    });

                    _dataProvider.DataContext.SaveChanges();

                    toolUsers.ForEach(t =>
                    {
                        _dataProvider.DataContext.Standard_Tools_Inventory.AddObject(new Standard_Tools_Inventory()
                        {
                            Tool_User = t,
                            Current_On_Hand = 0,
                            Last_Update_Date = DateTime.Now
                        });
                    });

                    _dataProvider.DataContext.SaveChanges();
                }
                else
                {
                    // if Tool_User rows, set rows to active
                    rows.Where(x => x.Tool_Type.is_Active == true).ToList().ForEach(r => r.is_Active = true);
                    _dataProvider.DataContext.SaveChanges();
                }
            }
            else if (rows.Count() > 0)
            {
                // if user isn't in those roles but has Tool_User rows, set rows to inactive
                rows.ToList().ForEach(r => r.is_Active = false);
                _dataProvider.DataContext.SaveChanges();
            }

            // Update the message/email distribution list for our user
            _userRepository.updateMessageDistForUser(updateUser.userId, updateUser.isDistribution);
            _userRepository.updateEmailDistForUser(updateUser.userId, updateUser.isEmail);

            List<Equipment_User> currentEquip = _equipmentRepository.getEquipmentForUser(updateUser.userId, true, true);

            // Delete all user / product relations and command chain relations, we'll add them all back
            _productRepository.deleteAllProductsForUser(updateUser.userId);
            _userRepository.deleteUserFromCommandChain(updateUser.userId);

            // Take our return collection object holding the GUID of the users, split it and loop over to add
            // repopulates command chain relationships
            if (collection["usersSelected"] != null)
            {
                string[] userGuids = collection["usersSelected"].Split(',');
                Guid tempGuid = Guid.Empty;

                foreach (string item in userGuids)
                {
                    Guid.TryParse(item, out tempGuid);

                    if (tempGuid != Guid.Empty)
                        _userRepository.addUserToCommandChain(updateUser.userId, tempGuid);

                    tempGuid = Guid.Empty;
                }
            }

            // Fill the users product / equipment relations from form data
            int prodId = -1, equipUserId = -1, prodQty = -1, loopCounter = 0;
            int? prodSubTypeId = null;
            int tempProdId = -1, equipTypeId = -1, equipCapacity = -1;
            int tempInt;

            bool activeProduct;
            List<int> updatedEquipUsers = new List<int>();
            Dictionary<int, int> productEquipCapacity = new Dictionary<int, int>();
            Dictionary<string, string> equipUserIds = collection.Cast<string>().Where(k => k.StartsWith("equipUserId.")).ToDictionary(k => k, k => collection[k]);
            Dictionary<string, string> equipTypes = collection.Cast<string>().Where(k => k.StartsWith("equipType.")).ToDictionary(k => k, k => collection[k]);
            Dictionary<string, string> equipDescripts = collection.Cast<string>().Where(k => k.StartsWith("equipDescript.")).ToDictionary(k => k, k => collection[k]);
            Dictionary<string, string> equipProds = collection.Cast<string>().Where(k => k.StartsWith("equipProd.")).ToDictionary(k => k, k => collection[k]);
            Dictionary<string, string> equipCapacities = collection.Cast<string>().Where(k => k.StartsWith("equipCapacity.")).ToDictionary(k => k, k => collection[k]);
            Dictionary<string, string> equipActives = collection.Cast<string>().Where(k => k.StartsWith("equipActiveHidden.")).ToDictionary(k => k, k => collection[k]);

            foreach (var item in equipDescripts)
            {
                if (loopCounter + 1 <= equipDescripts.Count)
                {
                    int.TryParse(equipUserIds.ElementAt(loopCounter).Value, out equipUserId);
                    int.TryParse(equipProds.ElementAt(loopCounter).Value, out tempProdId);
                    int.TryParse(equipTypes.ElementAt(loopCounter).Value, out equipTypeId);
                    int.TryParse(equipCapacities.ElementAt(loopCounter).Value, out equipCapacity);
                    bool.TryParse(equipActives.ElementAt(loopCounter).Value, out activeProduct);

                    if (tempProdId > 0 && !string.IsNullOrEmpty(equipDescripts.ElementAt(loopCounter).Value))
                    {
                        if (equipCapacity > 0)
                        {
                            if (productEquipCapacity.Keys.Contains(tempProdId))
                                productEquipCapacity[tempProdId] = productEquipCapacity[tempProdId] + equipCapacity;
                            else
                                productEquipCapacity.Add(tempProdId, equipCapacity);
                        }

                        if (equipUserId > 0)
                        {
                            updatedEquipUsers.Add(equipUserId);
                            _equipmentRepository.updateEquipmentUser(equipUserId, equipTypeId, tempProdId, equipDescripts.ElementAt(loopCounter).Value, equipCapacity, activeProduct);
                        }
                        else
                            _equipmentRepository.addUserToEquipment(equipTypeId, tempProdId, equipDescripts.ElementAt(loopCounter).Value, equipCapacity, activeProduct, updateUser.userId);
                    }

                    tempProdId = -1;
                    equipUserId = -1;
                    equipTypeId = -1;
                    equipCapacity = -1;
                    activeProduct = false;
                }

                loopCounter++;
            }

            // loop through all form keys
            foreach (string key in collection.Keys)
            {
                // look for the product key in the form
                if (key.Contains("product"))
                {
                    // parse our the product Id
                    int.TryParse(key.Replace("product", string.Empty), out prodId);

                    // if we got something, get rest of info for those specific product Id key fields
                    if (prodId > -1)
                    {
                        int.TryParse(collection["prodQty" + prodId.ToString()], out prodQty);
                        prodSubTypeId = int.TryParse(collection["prodSubType" + prodId.ToString()], out tempInt) ? (int?)tempInt : null;

                        if (prodQty > 0)
                            _productRepository.addUserToProduct(prodId, updateUser.userId, prodQty, prodSubTypeId);
                        else
                            _productRepository.addUserToProduct(prodId, updateUser.userId, prodQty, prodSubTypeId);
                    }

                    prodId = -1;
                }
            }

            // loop through equipment user updated, look at previous equiupment user relationships and then delete any not present
            foreach (Equipment_User equipUser in currentEquip.Where(e => !updatedEquipUsers.Contains(e.Equipment_User_ID)).ToList())
            {
                _equipmentRepository.deleteEquipmentUser(equipUser.Equipment_User_ID);
            }

            // loop through capacities for equipment products and then update the product - user capacities
            foreach (int key in productEquipCapacity.Keys)
            {
                _productRepository.updateProductUserCapacity(key, updateUser.userId, productEquipCapacity[key]);
            }

            return RedirectToAction("List");
        }

        #endregion

        #region Delete

        /// <summary>
        /// AJAX method to delete a user.
        /// </summary>
        /// <param name="name">The user name.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpGet]
        public ActionResult Delete(string name)
        {
            var returnValue = new { status = "failed", name = name };

            if (_userRepository.deleteUser(name))
                returnValue = new { status = "success", name = name };

            return Json(returnValue, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Sort

        /// <summary>
        /// AJAX method to sort the user list.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="direction">The direction.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpGet]
        public ActionResult SortedGrid(string field, string direction)
        {
            return View("UserGrid", _userRepository.getSortedUsers(field, direction == "ASC" ? Enums.SortDirection.Ascending :
                Enums.SortDirection.Descending));
        }

        #endregion

        #region Message Distribution

        /// <summary>
        /// Preapres lists of users available and users in the message distribution
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult MessageDist()
        {
            List<IUser> msgDistUsers = _userRepository.getAllMessageDistribUsers();
            List<IUser> allUsers = _userRepository.getAllUsers(false);
            List<IUser> nonMsgDistUsers = new List<IUser>();

            foreach (IUser user in allUsers)
            {
                if (!msgDistUsers.Exists(eu => eu.userId == user.userId))
                    nonMsgDistUsers.Add(user);
            }

            ViewData["usersAvailable"] = new SelectList(nonMsgDistUsers, "userId", "fullName");
            ViewData["usersSelected"] = new SelectList(msgDistUsers, "userId", "fullName");

            return View("~/Views/Admin/User/MessageDist.aspx");
        }

        /// <summary>
        /// HTTP Post edit action.  Sets up who recieves system messages.
        /// </summary>
        /// <param name="usersAvailable">The users available.</param>
        /// <param name="usersSelected">The users selected.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult MessageDist(Guid[] usersAvailable, Guid[] usersSelected)
        {
            // Take our return collection object holding the GUID of the users, split it and loop over to add
            if (usersAvailable != null)
            {
                foreach (Guid item in usersAvailable)
                {
                    _userRepository.updateMessageDistForUser(item, false);
                }
            }

            if (usersSelected != null)
            {
                foreach (Guid item in usersSelected)
                {
                    _userRepository.updateMessageDistForUser(item, true);
                }
            }

            return RedirectToAction("Index", "Admin");
        }

        #endregion

        #region Email Distribution

        /// <summary>
        /// Preapres lists of users available and users in the email distribution
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult EmailDist()
        {
            List<IUser> emailDistUsers = _userRepository.getAllEmailDistribUsers();
            List<IUser> usersWithEmailAddresses = _userRepository.getAllEmailAddressUsers();
            List<IUser> nonEmailDistUsers = new List<IUser>();

            foreach (IUser user in usersWithEmailAddresses)
            {
                if (!emailDistUsers.Exists(eu => eu.userId == user.userId))
                    nonEmailDistUsers.Add(user);
            }

            ViewData["usersAvailable"] = new SelectList(nonEmailDistUsers, "userId", "fullName");
            ViewData["usersSelected"] = new SelectList(emailDistUsers, "userId", "fullName");

            return View("~/Views/Admin/User/EmailDist.aspx");
        }

        /// <summary>
        /// HTTP Post edit action.  Sets up who recieves e-mailed system messages.
        /// </summary>
        /// <param name="usersSelected">The users selected.</param>
        /// <param name="usersAvailable">The users available.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult EmailDist(Guid[] usersSelected, Guid[] usersAvailable)
        {
            // Take our return collection object holding the GUID of the users, split it and loop over to add
            if (usersAvailable != null)
            {
                foreach (Guid item in usersAvailable)
                {
                    _userRepository.updateEmailDistForUser(item, false);
                }
            }

            if (usersSelected != null)
            {
                foreach (Guid item in usersSelected)
                {
                    _userRepository.updateEmailDistForUser(item, true);
                }
            }

            return RedirectToAction("Index", "Admin");
        }

        #endregion
    }
}