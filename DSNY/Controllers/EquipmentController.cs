﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Core.Interfaces;
using DSNY.Data;
using DSNY.Common;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for <see cref="Equipment"/> pages
    /// </summary>
    
    public class EquipmentController : BaseController
    {
        #region Variables

        private IEquipmentRepository _equipmentRepository;
        private IUserRepository _userRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EquipmentController"/> class.
        /// </summary>
        /// <param name="IoCProductRepository">The inversion of control product repository.</param>
        /// <param name="IoCUserRepository">The inversion of control user repository.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public EquipmentController(IEquipmentRepository IoCProductRepository, IUserRepository IoCUserRepository, 
            ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _equipmentRepository = IoCProductRepository;
            _userRepository = IoCUserRepository;
        }

        #endregion

        #region Actions

        /// <summary>
        /// Shows the <see cref="Equipment"/> grid.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult List()
        {
            List<Equipment> model = _equipmentRepository.getAllEquipment(true);

            return View("~/Views/Admin/Equipment/List.aspx", model);
        }

        /// <summary>
        /// Create form for an <see cref="Equipment"/> item.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Create()
        {
            return View("~/Views/Admin/Equipment/Create.aspx");
        } 

        /// <summary>
        /// Accepts the post for the create form, adds an <see cref="Equipment"/> item.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="collection"></param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult Create(Equipment model, FormCollection collection)
        {
            try
            {
                model.has_Capacity = true;
                _equipmentRepository.addEquipment(model);

                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex);

                return RedirectToAction("List");
            }
        }
        
        /// <summary>
        /// Edit the <see cref="Equipment"/> item.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Edit(int id)
        {
            Equipment model = _equipmentRepository.getEquipment(id);

            return View("~/Views/Admin/Equipment/Edit.aspx", model);
        }

        /// <summary>
        /// Accepts the post for the edit form, adds an <see cref="Equipment"/> item.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="model">The model.</param>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult Edit(int id, Equipment model, FormCollection collection)
        {
            try
            {
                _equipmentRepository.updateEquipment(id, model);

                return RedirectToAction("List");
            }
            catch
            {
                return View();
            }
        }

        /// <summary>
        /// Accepts the delete request to delete an <see cref="Equipment"/> item.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="name">The equipment name.</param>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpGet]
        public ActionResult Delete(int id, string name)
        {
            var returnValue = new { status = "failure", id = id, name = name };

            try
            {
                _equipmentRepository.deleteEquipment(id);
                returnValue = new { status = "success", id = id, name = name };
            }
            catch (Exception)
            {
                // do nothing
            }

            return Json(returnValue, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Sorts the <see cref="Equipment"/> grid.
        /// </summary>
        /// <param name="field">The field to sort on.</param>
        /// <param name="direction">Sort direction.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpGet]
        public ActionResult SortedGrid(string field, string direction)
        {
            return View("EquipmentGrid", _equipmentRepository.getSortedEquipment(field, direction == "ASC" ? 
                Enums.SortDirection.Ascending : Enums.SortDirection.Descending, true));
        }

        #endregion
    }
}