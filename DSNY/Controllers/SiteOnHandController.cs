﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using DSNY.Core.Interfaces;
using DSNY.Common;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Core.Repository;
using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the dashboard area
    /// </summary>
    public class SiteOnHandController : BaseController
    {
        private IUserRepository _userRepository;
        private DSNYContext _context;

        public SiteOnHandController(IUserRepository IoCUserRepo, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler, DSNYContext IoCContext)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _userRepository = IoCUserRepo;
            _context = IoCContext;
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Index()
        {
            var model = new SiteOnHandViewModel();
            model.Sites = _userRepository.getStcUsers((Guid)Membership.GetUser().ProviderUserKey).Select(x => new Dropdown { id = x.userId.ToString(), description = x.userName }).ToList();
            model.SiteOnHands = Mapper.Map<List<SiteOnHand>>(_context.DataContext.STC_Site_On_Hand.OrderBy(x => x.UserName).ThenBy(x => x.SortOrder).ToList());
            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");
            return View(model);
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Save(List<SiteOnHand> values)
        {
            //var values = new List<OnHandViewModel>();
            if (values == null || !values.Any()) { return Json("Nothing to save"); throw new Exception("There were no items to save."); }
            var productUser = _context.DataContext.STC_Product_User;
            foreach (var o in values)
            {
                var userGuid = new Guid(o.UserId);
                var row = productUser.Where(x => x.STC_User_ID == userGuid && x.STC_Product_ID == o.ProductId).FirstOrDefault();
                if (o.Deleted)
                {
                    if (row != null)
                    {
                        _context.DataContext.DeleteObject(row);
                    }
                    //_context.DataContext.ExecuteStoreCommand(string.Format("DELETE FROM [STC_Product_User] WHERE STC_User_ID = '{0}' AND STC_Product_ID = '{1}'", o.UserId, o.ProductId));
                }
                else
                {
                    if (row == null)
                    {
                        productUser.AddObject(new STC_Product_User
                        {
                            STC_User_ID = Guid.Parse(o.UserId),
                            STC_Product_ID = o.ProductId,
                            Capacity = o.Capacity,
                            On_Hand_Qty = decimal.Parse(o.Qty),
                            is_Active = o.Active
                        });
                    }
                    else
                    {
                        row.Capacity = o.Capacity;
                        row.On_Hand_Qty = decimal.Parse(o.Qty);
                        row.is_Active = o.Active;
                    }
                }
            }
            _context.DataContext.SaveChanges();

            return Json("Success");
        }
    }
}