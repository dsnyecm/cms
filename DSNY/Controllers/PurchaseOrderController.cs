﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Web.Security;

using DSNY.Core.Interfaces;
using DSNY.Common;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Core.Repository;
using DSNY.Data;
using DSNY.ViewModels;

using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing;
using iTextSharp.text.pdf.draw;
using System.Net.Mail;
using System.Net;
using System.Text;
using DSNY.Core.Models;
using System.Threading;
using System.Data.Objects.DataClasses;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the administration area
    /// </summary>    
    public class PurchaseOrderController : BaseController
    {
        #region Variables

        private IPurchaseOrderRepository _purchaseOrderRepository;
        private IUserRepository _userRepository;
        private IProductRepository _productRepository;
        private IVendorRepository _vendorRepository;
        private IDSNYExceptionRepository _dsnyExceptionRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PurchaseOrderController"/> class.
        /// </summary>
        /// <param name="IoCMessageRepo">The inversion of control  message repo.</param>
        /// <param name="IoCUserRepo">The inversion of control  user repo.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public PurchaseOrderController(IPurchaseOrderRepository IoCPurchaseOrderRepo, IUserRepository IoCUserRepository, IProductRepository IoCProductRepository,
            IVendorRepository IoCVendorRepository, IDSNYExceptionRepository IoCDSNYExceptionRepo, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _purchaseOrderRepository = IoCPurchaseOrderRepo;
            _userRepository = IoCUserRepository;
            _productRepository = IoCProductRepository;
            _vendorRepository = IoCVendorRepository;
            _dsnyExceptionRepository = IoCDSNYExceptionRepo;
        }

        #endregion

        #region Actions

        /// <summary>
        /// Action for the admin index, only system administrators may access
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Create()
        {
            PurchaseOrdersViewModel pos = null;

            try
                {
                List<GetPurchaseOrderItems_Result> gpos = _purchaseOrderRepository.getPurchaseOrder().ToList();
                List<Product> products = _productRepository.getAllProducts(false, false);

                // fill in the view model, holding two arrays: purchase order line and data for that purchase order line
                pos = new PurchaseOrdersViewModel()
                {
                    pos = gpos.Select(p => {
                        var productVendors = new EntityCollection<Product_Vendor>();
                        _vendorRepository.getAllProductVendors(p.Product_ID, false).ForEach(pv =>
                            {
                                productVendors.Add(pv);
                            });

                        return new PurchaseOrderViewModel()
                        {
                            pos = new Purchase_Orders()
                            {
                                UserId = p.UserId,
                                aspnet_Users = new aspnet_Users()
                                {
                                    UserId = p.UserId,
                                    UserName = p.UserName
                                },
                                Product_ID = p.Product_ID,
                                Product = new Product()
                                {
                                    Product_ID = p.Product_ID,
                                    Product_Name = p.Product_Name,
                                    Measurement_Type = p.Measurement_Type,
                                    Product_Vendor = productVendors,
                                    is_Sub_Type = products.First(pr => pr.Product_ID == p.Product_ID).is_Sub_Type
                                },
                                Order_Date = DateTime.Now,
                                Is_Ordered = false
                            },
                            userId = p.UserId,
                            address = p.Address,
                            borough = p.Borough,
                            district = p.District,
                            poDisplayName = p.PODisplayName,
                            productId = p.Product_ID,
                            capacity = p.Capacity != null ? (int)p.Capacity : -1,
                            avgDispensed = p.Avg_Dispensed != null ? (int)p.Avg_Dispensed : -1,
                            endOnHand = p.End_On_Hand != null ? (int)p.End_On_Hand : -1,
                            isOrdered = true,
                            orderAmount = p.Order_Amount != null ? (int)p.Order_Amount : -1
                        };
                    })
                    .OrderBy(p => p.pos.Product.Product_Name).ThenBy(p => p.borough).ThenBy(p => p.pos.aspnet_Users.UserName)
                    .ToList()
                };

                ViewData["productUsers"] = _productRepository.getProductsForUsers(false);
                ViewData["users"] = _productRepository.getUsersOfProducts().OrderBy(u => u.UserName).ToList();
                ViewData["vendors"] = _vendorRepository.getAllVendors(false).ToList();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }

            return View("~/Views/Admin/PurchaseOrder/Create.aspx", pos);
        }

        [Authorize(Roles = "System Administrator")]
        [HttpGet]
        public ActionResult GetProductsForUser(Guid userId)
        {
            string products = String.Empty; ;

            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                var a = _productRepository.getProductsVendorsForUser(userId, false);

                var p = a.Select(pv => new ProductUserModel()
                {
                    Product_ID = pv.Product_ID,
                    Product_Name = pv.Product_Name,
                    Default_Capacity = pv.Default_Capacity,
                    is_Drums = pv.is_Drums,
                    is_Water = pv.is_Water,
                    Measurement_Type = pv.Measurement_Type,
                    Order_Delivery_Distribution = pv.Order_Delivery_Distribution,
                    Order_Delivery_Type = pv.Order_Delivery_Type,
                    is_Sub_Type = pv.is_Sub_Type,
                    Sub_Type_Id = pv.Product_User.Count(pu => pu.UserId == userId) > 0 ? pv.Product_User.FirstOrDefault(pu => pu.UserId == userId).Product_Sub_Type_Id : null,
                    Sub_Types = pv.Product_Sub_Type.Select(pst => new Product_Sub_Type() {  Product_Sub_Type_ID = pst.Product_Sub_Type_ID, Sub_Type = pst.Sub_Type }).ToList(),
                    Vendors = pv.Product_Vendor.Select(v => new Vendor() { Account_Number = v.Vendor.Account_Number, Phone_Number = v.Vendor.Phone_Number, Vendor_ID = v.Vendor_ID, Vendor_Name = v.Vendor.Vendor_Name  }).ToList()
                })
                .OrderBy(pv => pv.Product_Name);

                products = serializer.Serialize(p);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }

            return Json(products, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "System Administrator")]
        [HttpGet]
        public ActionResult GetUserData(Guid userId)
        {
            string returnUser = String.Empty;

            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                IUser user = _userRepository.getUser(userId);
                dynamic dynamicUser = new System.Dynamic.ExpandoObject();

                dynamicUser.userId = user.userId;
                dynamicUser.userName = user.userName;
                dynamicUser.address = user.address;
                dynamicUser.borough = user.borough;
                dynamicUser.district = user.district;
                dynamicUser.poDisplayName = user.poDisplayName;

                returnUser = serializer.Serialize(dynamicUser);
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }

            return Json(returnUser, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Action for the admin index, only system administrators may access
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult Create(PurchaseOrdersViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;
                    int exceptionCategoryId = _dsnyExceptionRepository.getDSNYExceptionCategory("POO").ExceptionCategoryId;

                    List<Purchase_Orders> orders = model.pos
                        .Where(p => p.isOrdered && p.orderAmount > 0)
                        .Select(p => new Purchase_Orders()
                        {
                            UserId = p.userId,
                            Product_ID = p.productId,
                            Order_Amount = p.orderAmount,
                            Order_Date = DateTime.Now,
                            Vendor_ID = p.vendorId,
                            Product_Sub_Type_ID = p.productSubTypeId,
                            Not_Ordered_Reason = p.exceptionReason,
                            Is_Ordered = true
                        }).ToList();

                    _purchaseOrderRepository.createPurchaseOrder(orders);

                    // add exceptions
                    List<DSNY.Data.DSNYException> exceptions = model.pos
                        .Where(p => !String.IsNullOrEmpty(p.exceptionReason))
                        .Select(p => {
                            bool hasPo = orders.FirstOrDefault(o => o.UserId == p.userId && o.Product_ID == p.productId) != null;

                            return new DSNY.Data.DSNYException()
                            {
                                Exception = hasPo
                                    ? p.exceptionReason
                                    : String.Format("Location {0}/Product {1}/Quantity {2}/Reason {3}", p.pos.aspnet_Users.UserName, p.pos.Product.Product_Name, p.orderAmount, p.exceptionReason), 
                                ExceptionCategoryId = exceptionCategoryId,
                                ExceptionUser = currentUserGuid,
                                ExceptionTableName = "Purchase_Orders",
                                ExceptionTableKey = hasPo
                                    ? orders.First(o => o.UserId == p.userId && o.Product_ID == p.productId).Purchase_Order_ID
                                    : -1,
                                ExceptionDateTime = DateTime.Now
                            }; 
                        }).ToList();

                    _dsnyExceptionRepository.addExceptions(exceptions);

                    // update product user product sub type ids
                    model.pos
                        .Where(p => p.productSubTypeId.HasValue)
                        .All (p => _productRepository.updateProductUserSubType(p.productId, p.userId, p.productSubTypeId));

                    // update product default vendor id
                    model.pos
						.Where(p => p.vendorId > 0)
                        .All(p => _productRepository.updateProductUserVendor(p.productId, p.userId, p.vendorId));

                    List<String> pdfs = createPDF(model);

                    sendEmails(orders, pdfs);

                    deletePdfs(pdfs);
                    pdfs = null;

                    return RedirectToAction("Index", "Admin", new { poSent = "true" } );
                }
            }
            catch (Exception ex)
            {
				ViewData["hasError"] = true;
				ViewData["errorMessage"] = ex.InnerException.Message;
                _exceptionHandler.HandleException(ex);
			}

			// if we got here, there was a problem, fill viewdata and return same model
			ViewData["productUsers"] = _productRepository.getProductsForUsers(false);
			ViewData["users"] = _productRepository.getUsersOfProducts().OrderBy(u => u.UserName).ToList();
			ViewData["vendors"] = _vendorRepository.getAllVendors(false).ToList();

			return View("~/Views/Admin/PurchaseOrder/Create.aspx", model);
		}

        #endregion

        #region Email Send

        private void sendEmails(List<Purchase_Orders> orders, List<String> pdfs)
        {
			try
			{
				// send out the email with one large bcc instead of sending it to specific users
				// set up the smtp mail info

				string mailServer = ConfigurationManager.AppSettings["Mail Server"];
				string mailUser = ConfigurationManager.AppSettings["Mail User"];
				string mailPassword = ConfigurationManager.AppSettings["Mail Password"];
				string mailFrom = ConfigurationManager.AppSettings["Mail From Address"];
				bool useSSL = false;
				int mailPort = 25;

				bool.TryParse(ConfigurationManager.AppSettings["Mail SSL"], out useSSL);
				int.TryParse(ConfigurationManager.AppSettings["Mail Port"], out mailPort);

				if (!string.IsNullOrEmpty(mailServer))
				{
					SmtpClient sc = new SmtpClient(mailServer);

					if (!string.IsNullOrEmpty(mailUser) && !string.IsNullOrEmpty(mailPassword))
					{
						NetworkCredential nc = new NetworkCredential(mailUser, mailPassword);
						sc.UseDefaultCredentials = false;
						sc.Credentials = nc;
					}
					else
						sc.UseDefaultCredentials = true;

					sc.EnableSsl = useSSL;
					sc.Port = mailPort;

					var productVendors = orders
						.GroupBy(
							p => p.Product,
							p => p.Vendor,
							(product, vendors) => new { product, vendors })
						.Select(v => new { product = v.product, vendors = v.vendors.Distinct() });

					try
					{

						foreach (var prod in productVendors)
						{
							Product product = prod.product;

							foreach (var vendor in prod.vendors)
							{
								if (!String.IsNullOrEmpty(product.Order_Delivery_Distribution) || !String.IsNullOrEmpty(vendor.Email_Address))
								{
									string productName = wordCase(product.Product_Name);

									// set up the mail message going out
									MailMessage mm = new MailMessage();

									// setup message
									mm.From = new MailAddress(mailFrom, "DSNY CMS Purchase Order System");

									mm.Subject = DateTime.Now.ToString("g") + " Purchase Order for " + productName;
									mm.IsBodyHtml = false;
									//mm.BodyEncoding = Encoding.UTF8;
									mm.Body += "Purchase order for " + productName + " on " + DateTime.Now.ToString("f");

									mm.Attachments.Add(new Attachment(pdfs.First(p => p.Contains(productName + " - " + vendor.Vendor_Name)), "application/pdf"));

									List<String> emailTos = new List<String>();

									if (!String.IsNullOrEmpty(product.Order_Delivery_Distribution))
									{
										product.Order_Delivery_Distribution.Split(',').ToList().ForEach(oddEmail =>
											emailTos.Add(oddEmail.Trim())
										);
									}

									if (!String.IsNullOrEmpty(vendor.Email_Address))
									{
										vendor.Email_Address.Split(',').ToList().ForEach(vendorEmail =>
											emailTos.Add(vendorEmail.Trim())
										);
									}

									// add distinct emails to message
									emailTos.Distinct().ToList().ForEach(e => mm.To.Add(e));

									try
									{
										sc.Send(mm);
									}
									catch (SmtpFailedRecipientException ex)
									{
										_exceptionHandler.HandleException(ex);
									}
									finally
									{
										mm.Dispose();
									}
								}
							}
						}
					}
					catch (Exception ex)
					{
						_exceptionHandler.HandleException(ex);
					}
					finally
					{
						sc.Dispose();
					}
				}
			}
            catch (Exception ex)
            {
				_exceptionHandler.HandleException(ex);
			}
		}


        #endregion

        #region PDF

        private List<String> createPDF(PurchaseOrdersViewModel items)
        {
            List<String> returnFiles = new List<String>();

            try
            {
                string path = Server.MapPath("..\\PDFs");

                // prepare fonts
                iTextSharp.text.Font titleFont = FontFactory.GetFont("Arial", 28, new BaseColor(30, 149, 33));
                iTextSharp.text.Font subFont = FontFactory.GetFont("Arial", 14, BaseColor.BLACK);
                iTextSharp.text.Font productFont = FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
                iTextSharp.text.Font tableHeaderFont = FontFactory.GetFont("Arial", 12, BaseColor.WHITE);
                iTextSharp.text.Font tableFont = FontFactory.GetFont("Arial", 12, BaseColor.BLACK);

                // title
                var title = new Paragraph("New York City\nDepartment of Sanitation\n\n", titleFont);
                title.Alignment = Element.ALIGN_CENTER;

                // get each product Id to create a PO for and get all Products for into
                List<int> productOrders = items.pos.Where(i => i.isOrdered &&  i.orderAmount > 0).Select(i => i.productId).Distinct().ToList();
                List<Product> products = _productRepository.getAllProducts(false, false);
                List<Vendor> vendors = _vendorRepository.getAllVendors(false).ToList();
                List<Product_Sub_Type> subTypes = _productRepository.getProductSubTypes();

                var productsVendors = items.pos
                    .Where(i => i.isOrdered && i.orderAmount > 0)
                    .GroupBy(
                        po => po.productId,
                        po => po.vendorId,
                        (productId, vendorIds) => new { productId, vendorIds });

                foreach (var productIdKey in productsVendors)
                {
                    int productId = productIdKey.productId;

                    foreach (var vendorId in productIdKey.vendorIds.Distinct())
                    {
                        // grab specific orders and specific product
                        var orders = items.pos.Where(i => i.isOrdered && i.orderAmount > 0 && i.productId == productId && i.vendorId == vendorId).ToList();
                        bool hasSubItems = orders.Where(o => o.productSubTypeId.HasValue).Count() > 0;
                        Product prod = products.FirstOrDefault(i => i.Product_ID == productId);
                        Vendor vendor = vendors.FirstOrDefault(v => v.Vendor_ID == vendorId);

                        // prepare sub-header and product name
                        Chunk glue = new Chunk(new VerticalPositionMark());
                        Paragraph sub = new Paragraph(vendor.Vendor_Name, subFont);
                        Paragraph subVendor = new Paragraph("", subFont);
                        Paragraph product = new Paragraph(prod.Product_Name, productFont);
                        product.Alignment = Element.ALIGN_CENTER;

                        sub.Add(new Chunk(glue));
                        sub.Add(DateTime.Now.ToString("f"));

                        // add vendor data if we have it
                        if (vendor.Account_Number != null)
                        {
                            subVendor.Add(vendor.Account_Number);
                        }

                        if (vendor.Phone_Number != null)
                        {
                            subVendor.Add("\n" + vendor.Phone_Number);
                        }

                        // create order table
                        PdfPTable table = new PdfPTable(4);
                        table.TotalWidth = 525f;    //fix the absolute width of the table
                        table.LockedWidth = true;
                        float[] widths = hasSubItems ? new float[] { 2f, 1f, 3f, 1f } : new float[] { 1f, 3f, 1f, 0f };    //relative col widths in proportions - 1/3, 2/3, 3/5
                        table.SetWidths(widths);
                        table.HorizontalAlignment = 0;
                        table.SpacingBefore = 20f;
                        table.SpacingAfter = 30f;

                        // table header cell
                        if (hasSubItems)
                        {
                            PdfPCell header1 = new PdfPCell(new Phrase("Sub Type", tableHeaderFont));
                            header1.BackgroundColor = new BaseColor(30, 149, 33);
                            header1.HorizontalAlignment = 1;
                            table.AddCell(header1);
                        }

                        PdfPCell header2 = new PdfPCell(new Phrase("District", tableHeaderFont));
                        PdfPCell header3 = new PdfPCell(new Phrase("Address", tableHeaderFont));
                        PdfPCell header4 = new PdfPCell(new Phrase("Amount", tableHeaderFont));

                        header2.BackgroundColor = new BaseColor(30, 149, 33);
                        header3.BackgroundColor = new BaseColor(30, 149, 33);
                        header4.BackgroundColor = new BaseColor(30, 149, 33);

                        header2.HorizontalAlignment = 1;    //0=Left, 1=Centre, 2=Right
                        header3.HorizontalAlignment = 1;
                        header4.HorizontalAlignment = 1;

                        table.AddCell(header2);
                        table.AddCell(header3);
                        table.AddCell(header4);

                        string bourough = null;

                        // loop over orders to build order table
                        foreach (var item in orders.OrderBy(o => o.borough).ThenBy(o => o.poDisplayName).ToList())
                        {
                            // add table row per each bourough
                            if (item.borough != null && item.borough != bourough)
                            {
                                bourough = item.borough;
                                PdfPCell bouroughCell = new PdfPCell(new Phrase(bourough, tableHeaderFont));
                                bouroughCell.BackgroundColor = new BaseColor(0, 128, 0);
                                bouroughCell.Colspan = 4;
                                bouroughCell.HorizontalAlignment = 1;

                                table.AddCell(bouroughCell);
                            }

                            PdfPCell cell1 = new PdfPCell(new Phrase(string.Empty, tableFont));

                            // add order item
                            if (hasSubItems)
                            {
                                if (item.productSubTypeId.HasValue)
                                {
                                    Product_Sub_Type subType = subTypes.FirstOrDefault(st => st.Product_Sub_Type_ID == item.productSubTypeId);
                                    cell1 = new PdfPCell(new Phrase(subType.Sub_Type, tableFont));
                                }

                                cell1.HorizontalAlignment = 1;
                                table.AddCell(cell1);
                            }

                            PdfPCell cell2 = new PdfPCell(new Phrase(item.district, tableFont));
                            PdfPCell cell3 = new PdfPCell(new Phrase(item.address, tableFont));
                            PdfPCell cell4 = new PdfPCell(new Phrase(item.orderAmount.ToString(), tableFont));
                           
                            cell2.HorizontalAlignment = 1;
                            cell3.HorizontalAlignment = 1;
                            cell4.HorizontalAlignment = 1;

                            table.AddCell(cell2);
                            table.AddCell(cell3);
                            table.AddCell(cell4);

                            if (!hasSubItems)
                            {
                                table.AddCell(cell1);
                            }
                        }

                        var doc = new Document();

                        string productName = wordCase(prod.Product_Name);

                        string fileName = path + "\\" + productName + " - " + vendor.Vendor_Name +
                            (prod.Order_Delivery_Type == "Fax" ? " - Fax.pdf" : ".pdf");
                        PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Create));
                        // Create a new PdfWriter object, specifying the output stream
                        //var output = new MemoryStream();
                        //var writer = PdfWriter.GetInstance(doc, output);

                        // build PDF from parts defined above
                        doc.Open();
                        doc.Add(title);
                        doc.Add(sub);
                        doc.Add(subVendor);
                        doc.Add(new Paragraph("\n"));
                        doc.Add(product);
                        doc.Add(table);
                        doc.Close();

                        returnFiles.Add(fileName);
                    }
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
            }

            return returnFiles;
        }

        private bool deletePdfs(List<String> pdfs) {
            try
            {
                foreach (String file in pdfs)
                {
                    System.IO.File.Delete(file);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        private string wordCase(string words)
        {
            return Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(words.Replace(@"/", @"-").ToLower());
        }
    }
}