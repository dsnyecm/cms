﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

using DSNY.Core.Interfaces;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Data;
using DSNY.ViewModels;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the administration area
    /// </summary>
    
    public class LogController : BaseController
    {
        #region Variables

        private ILogRepository _logRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LogController"/> class.
        /// </summary>
        /// <param name="IoCMessageRepo">The inversion of control  message repo.</param>
        /// <param name="IoCUserRepo">The inversion of control  user repo.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public LogController(ILogRepository IoCLogRepo, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _logRepository = IoCLogRepo;
        }

        #endregion

        #region Actions

        /// <summary>
        /// Action for the admin index, only system administrators may access
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Search()
        {
            LogViewModel model = new LogViewModel()
            {
                logs = _logRepository.getLastestLogs(),
                searchDate = new DateTime()
            };

            return View("~/Views/Admin/Log/Search.aspx", model);
        }

        /// <summary>
        /// Action for the admin index, only system administrators may access
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "System Administrator")]
        public ActionResult Search(LogViewModel model)
        {
            List<Log> logs = new List<Log>();

            if (ModelState.IsValid && model.searchDate.HasValue)
            {
                model.logs = _logRepository.findLogs(model.searchDate.Value);
            }

            return View("~/Views/Admin/Log/Search.aspx", model);
        }

        #endregion
    }
}