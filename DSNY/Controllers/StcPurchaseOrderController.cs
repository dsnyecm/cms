using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DSNY.Core.Interfaces;
using DSNY.Common;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Core.Repository;
using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the dashboard area
    /// </summary>
    public class StcPurchaseOrderController : BaseController
    {
        private DSNYContext _context;
        private StcPurchaseOrderRepository _stcPORepository;
        private StcProductRepository _stcProductRepository;
        private IVendorRepository _vendorRepository;
        private ZoneRepository _zoneRepository;

        public StcPurchaseOrderController(ILogger IoCLogger, IExceptionHandler IoCExceptionHandler, DSNYContext IoCContext, IVendorRepository IoCVendor)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _context = IoCContext;
            _stcPORepository = new StcPurchaseOrderRepository(_context);
            _stcProductRepository = new StcProductRepository(_context);
            _vendorRepository = IoCVendor;
            _zoneRepository = new ZoneRepository(_context);
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Index()
        {
            var model = GetViewModel();
            model.OpenOnly = true;
            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");
            return View(model);
        }

        private StcPurchaseOrderViewModel GetViewModel()
        {
            var model = new StcPurchaseOrderViewModel();
            var products = _stcProductRepository.GetAll();
            if (products.Any())
            {
                model.Products = Mapper.Map<List<Dropdown>>(products.Select(x => new { id = x.STC_Product_id, description = x.STC_Product_Name, visible = true }).ToList());
                model.Vendors = Mapper.Map<List<Dropdown>>(_vendorRepository.getStcVendors().Select(x => new { id = x.Vendor_ID, description = x.Vendor_Name, visible = true }).ToList());
                model.Zones = Mapper.Map<List<Dropdown>>(_zoneRepository.GetActive().Select(x => new { id = x.Zone_id, description = x.Zone_Name, visible = true }).ToList());
                model.PurchaseOrders = Mapper.Map<List<StcPurchaseOrder>>(_stcPORepository.GetAll());
                foreach (var po in model.PurchaseOrders)
                {
                    po.Zones = new List<Dropdown>(model.Zones);
                    if (!po.Zones.Select(x => x.id).Contains(po.ZoneId.Value.ToString()))
                    {
                        po.Zones.Add(new Dropdown
                        {
                            id = po.ZoneId.Value.ToString(),
                            description = po.Zone,
                            visible = true
                        });
                    }
                }
                model.BlankPurchaseOrder = new StcPurchaseOrder
                {
                    New = true,
                    AmountRemaining = "1",
                    StartDate = DateTime.Now.ToShortDateString(),
                    Zones = model.Zones
                };
                if (!model.PurchaseOrders.Any()) { model.PurchaseOrders.Add(model.BlankPurchaseOrder); }
            }
            return model;
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Refresh()
        {
            return Json(GetViewModel());
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Save(List<StcPurchaseOrder> values)
        {
            //var values = new List<OnHandViewModel>();
            if (values == null || !values.Any()) { return Json("Nothing to save"); }
            var table = _context.DataContext.STC_Purchase_Order;
            foreach (var o in values.OrderByDescending(x => x.Deleted))
            {
                var row = table.Where(x => x.STC_Purchase_Orders_id == o.Id).FirstOrDefault();
                if (o.Deleted)
                {
                    if (row != null) { _context.DataContext.DeleteObject(row); }
                }
                else
                {
                    var duplicateQuery = table.Where(x => x.Purchase_Order_Num == o.PurchaseOrderNumber);
                    if (o.Id.HasValue)
                    {
                        duplicateQuery = duplicateQuery.Where(x => x.STC_Purchase_Orders_id != o.Id.Value);
                    }
                    var duplicateOrderNumber = duplicateQuery.FirstOrDefault();
                    if (duplicateOrderNumber != null && duplicateOrderNumber.EntityState != System.Data.EntityState.Deleted)
                    {
                        return Json(string.Format(@"The purchase order number: {0} has been used on another purchase order.  
Purchase order numbers need to be unique.", duplicateOrderNumber.Purchase_Order_Num));
                    }
                    if (o.Id.HasValue && (o.Id.Value > 0))
                    {
                        if (row == null)
                        {
                            table.AddObject(NewRow(o));
                        }
                        else
                        {
                            Update(row, o);
                        }
                    }
                    else
                    {
                        table.AddObject(NewRow(o));
                    }
                }
            }
            try
            {
                _context.DataContext.SaveChanges();
                return Json("Success");
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                if (ex.InnerException != null) {
                    if (ex.InnerException.Message.Contains("DELETE"))
                    {
                        return Json(@"The item that you marked to delete could not be deleted due to it being used in another part of the application.
The deleted items that you have marked for deletion have been unmarked.
If you made other changes that you would like to save, please click save again.");
                    }
                    else
                    {
                        return Json(ex.InnerException.Message);
                    }
                } else
                {
                    return Json(ex.Message);
                }
            }

        }

        private void Update(STC_Purchase_Order row, StcPurchaseOrder o)
        {
            var orderTotal = _context.DataContext.STC_Orders.Where(x => x.STC_Purchase_Order_Id == o.Id).Sum(x => x.Order_Qty);
            var deliveryTotal = _context.DataContext.STC_Delivery.Where(x => x.STC_Purchase_Order_id == o.Id).Sum(x => x.Boro_Qty);
            DateTime? startDate = null;
            DateTime tempDate;
            if (DateTime.TryParse(o.StartDate, out tempDate)) { startDate = tempDate; }
            DateTime? endDate = null;
            if (DateTime.TryParse(o.EndDate, out tempDate)) { endDate = tempDate; }
            row.STC_Product_id = o.ProductId;
            row.Purchase_Order_Num = o.PurchaseOrderNumber;
            decimal amt = 0.0M;
            if (decimal.TryParse(o.Amount, out amt))
            {
                row.Order_Qty_Amount = amt;
                row.Current_Order_Qty_Amount = orderTotal.HasValue ? (amt - orderTotal.Value) < 0 ? 0 : (amt - orderTotal.Value) : amt;
                row.Current_Delivery_Qty_Amount = deliveryTotal.HasValue ? (amt - deliveryTotal.Value) < 0 ? 0 : (amt - deliveryTotal.Value) : amt;
            }
            row.Vendor_Id = o.VendorId;
            row.PO_Start_Date = startDate;
            row.PO_Complete_Date = endDate;
            var zoneIds = new List<int>();
            if (o.ZoneId.HasValue) { zoneIds.Add(o.ZoneId.Value); }
            if (o.Zone2Id.HasValue) { zoneIds.Add(o.Zone2Id.Value); }
            var zonesToDelete = row.Zones.Where(x => !zoneIds.Contains(x.Zone_id)).ToList();
            foreach(var zone in zonesToDelete)
            {
                row.Zones.Remove(zone);
            }
            if (o.ZoneId.HasValue)
            {
                if (row.Zones.Where(x => x.Zone_id == o.ZoneId.Value).FirstOrDefault() == null)
                {
                    var zone = _context.DataContext.Zones.Where(x => x.Zone_id == o.ZoneId.Value).FirstOrDefault();
                    row.Zones.Add(zone);
                }
            }
            if (o.Zone2Id.HasValue)
            {
                if (row.Zones.Where(x => x.Zone_id == o.Zone2Id.Value).FirstOrDefault() == null)
                {
                    var zone = _context.DataContext.Zones.Where(x => x.Zone_id == o.Zone2Id.Value).FirstOrDefault();
                    row.Zones.Add(zone);
                }
            }
        }

        private STC_Purchase_Order NewRow(StcPurchaseOrder o)
        {
            DateTime? startDate = null;
            DateTime tempDate;
            if (DateTime.TryParse(o.StartDate, out tempDate)) { startDate = tempDate; }
            DateTime? endDate = null;
            if (DateTime.TryParse(o.EndDate, out tempDate)) { endDate = tempDate; }
            var ret = new STC_Purchase_Order
            {
                STC_Product_id = o.ProductId,
                Purchase_Order_Num = o.PurchaseOrderNumber,
                Order_Qty_Amount = decimal.Parse(o.Amount),
                Current_Order_Qty_Amount = decimal.Parse(o.Amount),
                Current_Delivery_Qty_Amount = decimal.Parse(o.Amount),
                Vendor_Id = o.VendorId,
                PO_Start_Date = startDate,
                PO_Complete_Date = endDate
            };
            if (o.ZoneId.HasValue)
            {
                var zone = _context.DataContext.Zones.Where(x => x.Zone_id == o.ZoneId.Value).FirstOrDefault();
                ret.Zones.Add(zone);
            }
            if (o.Zone2Id.HasValue)
            {
                var zone = _context.DataContext.Zones.Where(x => x.Zone_id == o.Zone2Id.Value).FirstOrDefault();
                ret.Zones.Add(zone);
            }
            return ret;
        }
    }
}