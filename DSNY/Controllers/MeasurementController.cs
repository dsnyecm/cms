using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DSNY.Core.Interfaces;
using DSNY.Common;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Core.Repository;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the dashboard area
    /// </summary>
    [HandleError]
    public class MeasurementController : Controller
    {
        private ILogger _logger;
        private IExceptionHandler _exceptionHandler;
        private DSNY.Data.DSNYContext _context;
        private MeasurementRepository _measurement;
        private StcProductRepository _stcProductRepository;
        private IVendorRepository _vendorRepository;

        public MeasurementController(ILogger IoCLogger, IExceptionHandler IoCExceptionHandler, DSNY.Data.DSNYContext IoCContext, IVendorRepository IoCVendor)
        {
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
            _context = IoCContext;
            _measurement = new MeasurementRepository(_context);
            _stcProductRepository = new StcProductRepository(_context);
            _vendorRepository = IoCVendor;
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Index()
        {
            var model = GetViewModel();
            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");
            return View(model);
        }

        private MeasurementMaintenanceViewModel GetViewModel()
        {
            var model = new MeasurementMaintenanceViewModel();
            model.Measurements = Mapper.Map<List<Measurement>>(_measurement.GetAll());
            model.BlankMeasurement = new Measurement
            {
                Active = true,
                Deleted = false
            };
            if (!model.Measurements.Any()) { model.Measurements.Add(model.BlankMeasurement); }
            return model;
        }

        /// <summary>
        /// Returns the ViewModel
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Refresh()
        {
            return Json(GetViewModel());
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Save(List<Measurement> values)
        {
            if (values == null || !values.Any()) { return Json("Nothing to save"); }

            try
            {
                var table = _context.DataContext.Measurements;
                foreach (var o in values)
                {
                    if (o.Deleted)
                    {
                        var order = table.Where(x => x.Measurement_id == o.Id).FirstOrDefault();
                        if (order != null) { _context.DataContext.DeleteObject(order); }
                    }
                    else if (o.Id.HasValue && (o.Id.Value > 0))
                    {
                        var row = table.Where(x => x.Measurement_id == o.Id).FirstOrDefault();
                        if (row == null)
                        {
                            table.AddObject(NewRow(o));
                        }
                        else
                        {
                            Update(row, o);
                        }
                    }
                    else
                    {
                        table.AddObject(NewRow(o));
                    }
                }
                _context.DataContext.SaveChanges();

                return Json("Success");
            }
            catch (Exception ex)
            {
                return Json("The measurement could not be deleted because it is in use.");
            }
        }

        private void Update(DSNY.Data.Measurement row, Measurement o)
        {
            row.Measurement_Name = o.Name;
            row.is_active = o.Active;
            row.is_STC = o.Stc;
            row.is_Tool = o.Tool;
        }

        private DSNY.Data.Measurement NewRow(Measurement o)
        {
            return new DSNY.Data.Measurement
            {
                Measurement_Name = o.Name,
                is_active = o.Active,
                is_STC = o.Stc,
                is_Tool = o.Tool
            };
        }
    }
}
