﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

using iTextSharp.text;
using iTextSharp.text.pdf;

using DSNY.Core.Interfaces;
using DSNY.Common;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Core.Repository;
using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;
using DSNY.Common.Utilities;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the dashboard area
    /// </summary>    
    public class StcOrderController : BaseController
    {
        private IUserRepository _userRepository;
        private DSNYContext _context;
        private StcPurchaseOrderRepository _stcPORepository;
        private StcProductRepository _stcProductRepository;
        private SiteOnHandRepository _siteOnHandRepository;

        public StcOrderController(IUserRepository IoCUserRepo, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler, DSNYContext IoCContext)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _userRepository = IoCUserRepo;
            _context = IoCContext;
            _stcPORepository = new StcPurchaseOrderRepository(_context);
            _stcProductRepository = new StcProductRepository(_context);
            _siteOnHandRepository = new SiteOnHandRepository(_context);
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Index()
        {
            var model = GetViewModel();
            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");
            return View(model);
        }

        private StcOrderViewModel GetViewModel()
        {
            return GetViewModel(null);
        }

        private StcOrderViewModel GetViewModel(int? id)
        {
            var model = new StcOrderViewModel();
            model.Products = Mapper.Map<List<StcProduct>>(_stcProductRepository.GetAll().OrderBy(x => x.SortOrder));
            if (id.HasValue)
            {
                model.selectedProduct = model.Products.Where(x => x.Id == id.Value).FirstOrDefault();
            }
            if (model.selectedProduct == null)
            {
                model.selectedProduct = model.Products[0];
            }
            var oneYearAgo = DateTime.Now.AddYears(-1);
            var previousDates = _context.DataContext.STC_Orders
                .Where(x => x.STC_Product_id == model.selectedProduct.Id && x.Order_Date > oneYearAgo).Select(x => x.Order_Date).ToList();
            model.PreviousOrderDates = Mapper.Map<List<Dropdown>>(previousDates
                .Select(x => new { id = x.Value.ToShortDateString(), description = x.Value.ToShortDateString(), visible = true })
                .Distinct());
            model.Orders = GetOrders(model.selectedProduct.Id);
            return model;
        }

        private List<StcOrder> GetOrders(int productId)
        {
            var ret = new List<StcOrder>();
            foreach (var onHand in _siteOnHandRepository.GetActiveByProduct(productId)) { 

                var iuser = _userRepository.getUser(onHand.UserId);
                var recentOrderDate = DateTime.Now.AddMinutes(-1 * int.Parse(ConfigurationManager.AppSettings["DelayInMinutesSinceLastOrder"]));
                if (_context.DataContext.STC_Orders.Where(x => x.Userid == iuser.userId && x.STC_Product_id == productId &&
                    x.Order_Date > recentOrderDate).ToList().Any()) continue;
                var purchaseOrders = _stcPORepository.GetByProductAndUser(productId, iuser.userId).Where(x => x.Current_Delivery_Qty_Amount > 0 && !x.PO_Complete_Date.HasValue);
                if (!purchaseOrders.Any()) continue;
                var boroughName = iuser.borough == null || iuser.borough == "" ? "Borough Undefined" : iuser.borough;
                var qty = onHand != null && onHand.Capacity.HasValue && onHand.On_Hand_Qty.HasValue && (onHand.On_Hand_Qty.Value <= (0.8M * ((decimal)onHand.Capacity.Value))) ? ((decimal)onHand.Capacity.Value) - onHand.On_Hand_Qty.Value : (decimal?)null;
                //if (qty.HasValue && qty.Value > purchaseOrders[0].Current_Order_Qty_Amount) { qty = purchaseOrders[0].Current_Order_Qty_Amount; }
                var order = new StcOrder
                {
                    PurchaseOrders = Mapper.Map<List<StcPurchaseOrder>>(purchaseOrders),
                    User = iuser.description,
                    UserId = iuser.userId.ToString(),
                    BoroughId = GetBoroughId(boroughName),
                    Borough = boroughName,
                    District = iuser.userName,
                    Address = iuser.address == null || iuser.address == "" ? "Address Undefined" : iuser.address,
                    Capacity = onHand == null ? (int?)null : onHand.Capacity,
                    OnHand = onHand == null ? (int?)null : onHand.On_Hand_Qty,
                    Telephone = iuser.phoneNumber,
                    Qty = qty.ToString()
                };
                order.selectedPurchaseOrder = order.PurchaseOrders[0].Id.Value;
                ret.Add(order);
            }
            return ret.OrderBy(x => x.Borough).ThenBy(x => x.District).ThenBy(x => x.Address).ToList();
        }
        
        private List<string> createPDF(StcOrderViewModel viewModel)
        {
            List<String> returnFiles = new List<String>();

            // prepare fonts
            iTextSharp.text.Font titleFont = FontFactory.GetFont("Arial", 28, new BaseColor(30, 149, 33));
            iTextSharp.text.Font subFont = FontFactory.GetFont("Arial", 14, BaseColor.BLACK);
            iTextSharp.text.Font productFont = FontFactory.GetFont("Arial", 18, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            iTextSharp.text.Font tableHeaderFont = FontFactory.GetFont("Arial", 10, BaseColor.WHITE);
            iTextSharp.text.Font tableFont = FontFactory.GetFont("Arial", 10, BaseColor.BLACK);

            // title
            var title = new Paragraph("New York City\nDepartment of Sanitation\n\n", titleFont);
            title.Alignment = Element.ALIGN_CENTER;


            string path = Server.MapPath("..\\PDFs");

            foreach (var purchaseOrderId in viewModel.Orders.Select(x => x.selectedPurchaseOrder).Distinct())
            {
                // grab specific orders and specific product
                var purchaseOrder = _stcPORepository.GetById(purchaseOrderId);
                var vendor = purchaseOrder.Vendor;
                var measurement = purchaseOrder.STC_Product.Measurement;

                string fileName = path + "\\" + viewModel.selectedProduct.Name + " - PO" + purchaseOrder.Purchase_Order_Num + "_" + DateTime.Now.ToShortDateString().Replace("/", "-") + "_"
                    + DateTime.Now.ToFileTime().ToString() + ".pdf";
                var doc = new Document();
                var writer = PdfWriter.GetInstance(doc, new FileStream(fileName, FileMode.Create));
                //writer.CloseStream = false;

                // build PDF from parts defined above
                doc.Open();
                //doc.NewPage();
                doc.Add(title);

                Paragraph product = new Paragraph(viewModel.selectedProduct.Name + " Order Sheet", productFont);
                product.Alignment = Element.ALIGN_CENTER;

                // prepare sub-header and product name
                Chunk glue = new Chunk(new iTextSharp.text.pdf.draw.VerticalPositionMark());
                string vendorText = string.Format("{0} (PO # {1})", vendor.Vendor_Name, purchaseOrder.Purchase_Order_Num).ToUpper();
                Paragraph sub = new Paragraph(vendorText, subFont);
                sub.Alignment = Element.ALIGN_CENTER;
                Paragraph subVendor = new Paragraph(string.Format("Orders for {0}", DateTime.Now.ToString("f")), subFont);
                subVendor.Alignment = Element.ALIGN_CENTER;
                                
                //if (vendor.Phone_Number != null)
                //{
                //    subVendor.Add("\n" + vendor.Phone_Number);
                //}

                bool hasCapacity = viewModel.selectedProduct.Name.StartsWith("Calcium");

                // create order table
                PdfPTable table = new PdfPTable(4);
                table.TotalWidth = 525f;    //fix the absolute width of the table
                table.LockedWidth = true;
                float[] widths = hasCapacity ? new float[] { 1f, 3f, 2f, 2f } : new float[] { 1f, 3f, 0f, 2f };    //relative col widths in proportions - 1/3, 2/3, 3/5
                //float[] widths = new float[] { 1f, 3f, 2f, 2f };
                table.SetWidths(widths);
                table.HorizontalAlignment = 0;
                table.SpacingBefore = 20f;
                table.SpacingAfter = 30f;

                // table header cell
                //if (hasSubItems)
                //{
                //    PdfPCell header1 = new PdfPCell(new Phrase("Sub Type", tableHeaderFont));
                //    header1.BackgroundColor = new BaseColor(30, 149, 33);
                //    header1.HorizontalAlignment = 1;
                //    table.AddCell(header1);
                //}
                var locationText = viewModel.selectedProduct.Name.StartsWith("Calcium") ? "Actual Tank Location" : "Location";

                //PdfPCell header1 = new PdfPCell(new Phrase("Borough".ToUpper(), tableHeaderFont));
                PdfPCell header2 = new PdfPCell(new Phrase("District".ToUpper(), tableHeaderFont));
                PdfPCell header3 = new PdfPCell(new Phrase(locationText.ToUpper(), tableHeaderFont));
                PdfPCell header4 = new PdfPCell(new Phrase("Tank Capacity".ToUpper(), tableHeaderFont));
                PdfPCell header5 = new PdfPCell(new Phrase(string.Format("{0} Ordered", measurement.Measurement_Name).ToUpper(), tableHeaderFont));

                //header1.BackgroundColor = new BaseColor(30, 149, 33);
                header2.BackgroundColor = new BaseColor(30, 149, 33);
                header3.BackgroundColor = new BaseColor(30, 149, 33);
                header4.BackgroundColor = new BaseColor(30, 149, 33);
                header5.BackgroundColor = new BaseColor(30, 149, 33);

                //header1.HorizontalAlignment = 1;
                header2.HorizontalAlignment = 1;    //0=Left, 1=Centre, 2=Right
                header3.HorizontalAlignment = 1;
                header4.HorizontalAlignment = 1;
                header5.HorizontalAlignment = 1;

                //table.AddCell(header1);
                table.AddCell(header2);
                table.AddCell(header3);
                table.AddCell(header4);
                table.AddCell(header5);

                var totalQty = 0.0M;

                foreach (var order in viewModel.Orders.Where(x => x.selectedPurchaseOrder == purchaseOrderId))
                {
                    string bourough = null;

                    totalQty += decimal.Parse(order.Qty);

                    //begin previous loop
                    var item = _userRepository.getUser(new Guid(order.UserId));
                    // add table row per each bourough
                    if (item.borough != null && item.borough != bourough)
                    {
                        bourough = item.borough;
                        PdfPCell bouroughCell = new PdfPCell(new Phrase(bourough, tableHeaderFont));
                        bouroughCell.BackgroundColor = new BaseColor(0, 128, 0);
                        bouroughCell.Colspan = 4;
                        bouroughCell.HorizontalAlignment = 1;

                        table.AddCell(bouroughCell);
                    }

                    PdfPCell cell1 = new PdfPCell(new Phrase(string.Empty, tableFont));

                    // add order item
                    //if (hasSubItems)
                    //{
                    //    if (item.productSubTypeId.HasValue)
                    //    {
                    //        Product_Sub_Type subType = subTypes.FirstOrDefault(st => st.Product_Sub_Type_ID == item.productSubTypeId);
                    //        cell1 = new PdfPCell(new Phrase(subType.Sub_Type, tableFont));
                    //    }

                    //    cell1.HorizontalAlignment = 1;
                    //    table.AddCell(cell1);
                    //}

                    PdfPCell cell2 = new PdfPCell(new Phrase(item.userName, tableFont));
                    PdfPCell cell3 = new PdfPCell(new Phrase(item.address, tableFont));
                    PdfPCell cell4 = new PdfPCell(new Phrase(order.Capacity.ToString(), tableFont));
                    PdfPCell cell5 = new PdfPCell(new Phrase(order.Qty.ToString(), tableFont));

                    cell2.HorizontalAlignment = 1;
                    cell3.HorizontalAlignment = 1;
                    cell4.HorizontalAlignment = 1;
                    cell5.HorizontalAlignment = 1;

                    table.AddCell(cell2);
                    table.AddCell(cell3);
                    table.AddCell(cell4);
                    table.AddCell(cell5);

                    //if (!hasSubItems)
                    //{
                    //    table.AddCell(cell1);
                    //}
                }

                doc.Add(product);
                doc.Add(sub);
                doc.Add(subVendor);
                doc.Add(table);

                doc.Close();

                SendEmail(purchaseOrder.STC_Product, totalQty, DateTime.Now.Date, vendor, fileName);
            }
            return returnFiles;
        }

        private bool deletePdfs(List<String> pdfs)
        {
            try
            {
                foreach (String file in pdfs)
                {
                    System.IO.File.Delete(file);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult ChangeProduct(int? id)
        {
            return Json(GetViewModel(id));
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult ChangeDate(StcOrderViewModel viewModel)
        {
            var model = new StcOrderViewModel();
            DateTime previousDate;
            if(!DateTime.TryParse(viewModel.selectedOrderDate, out previousDate)) { return Json("Could not parse the date in ChangeDate."); }

            DateTime previousEndDate = previousDate.AddDays(1);
            model.Orders = Mapper.Map<List<StcOrder>>(_context.DataContext.STC_Orders
                .Where(x => x.STC_Product_id == viewModel.selectedProduct.Id && x.Order_Date >= previousDate && x.Order_Date < previousEndDate).ToList());
            foreach (var order in model.Orders)
            {
                var iuser = _userRepository.getUser(new Guid(order.UserId));
                var purchaseOrders = _context.DataContext.STC_Purchase_Order.Where(x => x.STC_Purchase_Orders_id == order.selectedPurchaseOrder).ToList();
                order.PurchaseOrders = Mapper.Map<List<StcPurchaseOrder>>(purchaseOrders);
                var boroughName = iuser.borough == null || iuser.borough == "" ? "Borough Undefined" : iuser.borough;
                var onHand = _context.DataContext.STC_Site_On_Hand
                    .Where(x => x.UserId == iuser.userId && x.STC_Product_id == viewModel.selectedProduct.Id).FirstOrDefault();
                order.User = iuser.description;
                order.UserId = iuser.userId.ToString();
                order.District = iuser.userName;
                order.Address = iuser.address == null || iuser.address == "" ? "Address Undefined" : iuser.address;
                order.Telephone = iuser.phoneNumber;
                order.Capacity = onHand == null ? (int?)null : onHand.Capacity;
                order.OnHand = onHand == null ? (int?)null : onHand.On_Hand_Qty;                
            }
            return Json(model);
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Pdf(StcOrderViewModel viewModel)
        {
            createPDF(viewModel);
            //HttpContext.Response.AppendHeader("Content-Disposition", "Attachment; Filename=file.pdf");
            //return File(createPDF(viewModel).ToArray(), "application/pdf", "Report.pdf");
            return Json("Success");
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Save(StcOrderViewModel viewModel)
        {
            var emails = new List<STC_Orders>();
            //var values = new List<OnHandViewModel>();
            if (viewModel.Orders == null || !viewModel.Orders.Any()) { return Json("Nothing to save"); }
            var table = _context.DataContext.STC_Orders;
            foreach (var o in viewModel.Orders)
            {
                if (o.Deleted)
                {
                    var order = table.Where(x => x.STC_Orders_id == o.Id).FirstOrDefault();
                    if (order != null) { _context.DataContext.DeleteObject(order); }
                }
                else if (!string.IsNullOrEmpty(o.Qty))
                {
                    if (o.Id.HasValue && (o.Id.Value > 0))
                    {
                        var row = table.Where(x => x.STC_Orders_id == o.Id).FirstOrDefault();
                        if (row == null)
                        {
                            var order = NewRow(o, viewModel.selectedProduct.Id);
                            table.AddObject(order);
                            emails.Add(order);
                        }
                        else
                        {
                            Update(row, o);
                        }
                    }
                    else
                    {
                        var order = NewRow(o, viewModel.selectedProduct.Id);
                        table.AddObject(order);
                        emails.Add(order);
                    }
                }
            }
            _context.DataContext.SaveChanges();

            var pdfs = createPDF(viewModel);
            deletePdfs(pdfs);
            
            return Json("Success");
        }

        private void Update(STC_Orders row, StcOrder o)
        {
            var diff = decimal.Parse(o.Qty) - row.Order_Qty.Value;
            _stcPORepository.SubtractOrderedAmountRemaining(o.selectedPurchaseOrder, diff);
            row.Order_Qty = decimal.Parse(o.Qty);
        }

        private STC_Orders NewRow(StcOrder o, int ProductId)
        {
            _stcPORepository.SubtractOrderedAmountRemaining(o.selectedPurchaseOrder, decimal.Parse(o.Qty));

            var po = _stcPORepository.GetById(o.selectedPurchaseOrder);

            int zoneid = 0;
            foreach (var zone in po.Zones)
            {
                if (zone.aspnet_Users.Select(x => x.UserId.ToString()).Contains(o.UserId))
                {
                    zoneid = zone.Zone_id;
                }
            }

            return new STC_Orders
            {
                STC_Product_id = ProductId,
                STC_Purchase_Order_Id = o.selectedPurchaseOrder,
                Zone_Id = zoneid,
                Borough_Id = o.BoroughId,
                Userid = new Guid(o.UserId), 
                Order_Qty = decimal.Parse(o.Qty),
                Order_Date = DateTime.Now
            };
        }

        //call this function when loading the orders
        private int GetBoroughId(string boroughName)
        {
            var borough = _context.DataContext.Boroughs.Where(x => x.Borough_Name == boroughName).FirstOrDefault();
            if (borough == null)
            {
                borough = new Borough
                {
                    Borough_Name = boroughName
                };
                _context.DataContext.Boroughs.AddObject(borough);
            }
            _context.DataContext.SaveChanges();
            return borough.Borough_id;
        }

        private string SendEmail(STC_Product product, decimal qty, DateTime orderdate, Vendor vendor, string attachment)
        {
            try
            {
                // set up the mail message going out
                MailMessage mm = new MailMessage();

                // setup message
                mm.Subject = string.Format("DSNY Order for {0}", product.STC_Product_Name);
                mm.IsBodyHtml = false;
                //mm.BodyEncoding = Encoding.UTF8;
                mm.Body = string.Format(@"DSNY is requesting an order for {0} {1} of {2} issued on {3}. Attached are the order details in pdf format.",
                string.Format("{0:0.##}", qty), product.Measurement.Measurement_Name, product.STC_Product_Name, orderdate);

                mm.Attachments.Add(new Attachment(attachment, "application/pdf"));

                var users = _userRepository.getRoleUsers("STC HQ");
                foreach (var user in users)
                {
                    if (!string.IsNullOrEmpty(user.email))
                    {
                        mm.To.Add(new MailAddress(user.email, string.IsNullOrEmpty(user.fullName) ? string.Empty : user.fullName));
                    }
                }

                if (!string.IsNullOrEmpty(vendor.Email_Address))
                {
                    mm.To.Add(new MailAddress(vendor.Email_Address, vendor.Vendor_Name));
                }

                EmailUtilities.SendEmail(mm);

                return null;
            }
            catch (Exception ex)
            {
                return string.Format("An email could not be sent due to the following error: {0}\n", ex.Message);
            }
        }
    }
}