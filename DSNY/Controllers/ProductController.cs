﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DSNY.Core.Interfaces;
using DSNY.Core.Repository;
using DSNY.Common.Exception;
using DSNY.Common.Logger;
using DSNY.Data;
using DSNY.ViewModels;
using DSNY.Common;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for product pages
    /// </summary>
    
    public class ProductController : BaseController
    {
        #region Variables

        private IProductRepository _productRepository;
        private IVendorRepository _vendorRepository;
        private IUserRepository _userRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductController"/> class.
        /// </summary>
        /// <param name="IoCProductRepository">The inversion of control product repository.</param>
        /// /// <param name="IoCProductRepository">The inversion of control vendor repository.</param>
        /// <param name="IoCUserRepository">The inversion of control user repository.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public ProductController(IProductRepository IoCProductRepository, IVendorRepository IoCVendorRepository, IUserRepository IoCUserRepository, 
            ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _productRepository = IoCProductRepository;
            _vendorRepository = IoCVendorRepository;
            _userRepository = IoCUserRepository;
        }

        #endregion

        #region List

        /// <summary>
        /// Lists this instance.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult List()
        {
            List<Product> model = _productRepository.getAllProducts(true, true);

            return View("~/Views/Admin/Product/List.aspx", model);
        }

        #endregion

        #region Create

        /// <summary>
        /// Create form for product
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Create()
        {
             List<SelectListItem> OrderDeliveryTypes = new List<SelectListItem>() {
                new SelectListItem { Value = "Email", Text = "Email" },
                new SelectListItem { Value = "Fax", Text = "Fax" }
             };

            ViewData["OrderDeliveryTypes"] = OrderDeliveryTypes;
            ViewData["Order_Delivery_Type"] = null;
            ViewData["Vendors"] = _vendorRepository.getAllVendors(false);

            ProductViewModel viewModel = new ProductViewModel();

            return View("~/Views/Admin/Product/Create.aspx", viewModel);
        }

        /// <summary>
        /// HTTP Post create action. Creates a product.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult Create(ProductViewModel model, FormCollection collection)
        {
            try
            {
                model.Product.Order_Delivery_Type = "Email";    // always set to email for now

                model.Product.Product_Sub_Type.Clear();

                if (model.Product.is_Sub_Type && model.SubTypes != null)
                {
                    model.SubTypes.ForEach(st => {
                        if (st.Sub_Type != "")
                        {
                            model.Product.Product_Sub_Type.Add(st);
                        }
                    });
                }

                if (collection["vendorsSelected"] != null)
                {
                    collection["vendorsSelected"].Split(',').ToList().ForEach(v =>
                    {
                        int Vendor_ID;
                        Int32.TryParse(v, out Vendor_ID);

                        model.Product.Product_Vendor.Add(new Product_Vendor()
                        {
                            Vendor_ID = Vendor_ID
                        });
                    });
                }

                _productRepository.addProduct(model.Product);

                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex);

                return View("~/Views/Admin/Product/Create.aspx");
            }
        }

        #endregion

        #region Edit

        /// <summary>
        /// Edit form for a product.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Edit(int id)
        {
            // Populate our model
            Product model = _productRepository.getProduct(id);
            ProductViewModel viewModel = new ProductViewModel() { Product = model, SubTypes = model.Product_Sub_Type.ToList() };

            ViewData["Vendors"] = _vendorRepository.getAllVendors(false);

            // order delivery type select list
            ViewData["OrderDeliveryTypes"] = new SelectList(new List<SelectListItem>() {
               new SelectListItem { Value = "Email", Text = "Email" },
               new SelectListItem { Value = "Fax", Text = "Fax", Selected = true }
            }, "Value", "Text");
            ViewData["Order_Delivery_Type"] = model.Order_Delivery_Type;

            return View("~/Views/Admin/Product/Edit.aspx", viewModel);
        }

        /// <summary>
        /// HTTP Post create action.  Edit a product.
        /// </summary>
        /// <param name="id">The product id.</param>
        /// <param name="model">The product model.</param>
        /// <param name="collection">The form collection.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult Edit(int id, ProductViewModel model, FormCollection collection)
        {
            try
            {
                model.Product.Order_Delivery_Type = "Email";    // always set to email for now

                model.Product.Product_Sub_Type.Clear();

                if (model.Product.is_Sub_Type && model.SubTypes != null)
                {
                    model.SubTypes.ForEach(st => {
                        if (st.Sub_Type != "")
                        {
                            model.Product.Product_Sub_Type.Add(st);
                        }
                    });
                }

                if (collection["vendorsSelected"] != null)
                {
                    collection["vendorsSelected"].Split(',').ToList().ForEach(v =>
                    {
                        int Vendor_ID;
                        Int32.TryParse(v, out Vendor_ID);

                        model.Product.Product_Vendor.Add(new Product_Vendor()
                        {
                            Product_ID = id,
                            Vendor_ID = Vendor_ID
                        });
                    });
                }

                _productRepository.updateProduct(id, model.Product, false);

                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", "There was an error updating the product. Please try again.");

                return RedirectToAction("Edit");
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// Deletes the specified product.
        /// </summary>
        /// <param name="id">The product id.</param>
        /// <param name="name">The product name.</param>
        /// <param name="collection">The form collection.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpGet]
        public ActionResult Delete(int id, string name, FormCollection collection)
        {
            var returnValue = new { status = "failure", id = id, name = name };

            try
            {
                _productRepository.deleteProduct(id);
                returnValue = new { status = "success", id = id, name = name };
            }
            catch (Exception)
            {
                // do nothing, it was caught and then return a 
            }

            return Json(returnValue, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Sort

        /// <summary>
        /// Sortes the product grid.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="direction">The direction.</param>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpGet]
        public ActionResult SortedGrid(string field, string direction)
        {
            return View("ProductGrid", _productRepository.getSortedProducts(field, direction == "ASC" ? 
                Enums.SortDirection.Ascending : Enums.SortDirection.Descending, true));
        }

        #endregion
    }
}