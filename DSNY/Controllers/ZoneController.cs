﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using DSNY.Core.Interfaces;
using DSNY.Common;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Core.Repository;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the dashboard area
    /// </summary>
    
    public class ZoneController : BaseController
    {
        private IUserRepository _userRepository;
        private DSNY.Data.DSNYContext _context;
        private ZoneRepository _zones;

        public ZoneController(IUserRepository IoCUserRepo, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler, DSNY.Data.DSNYContext IoCContext)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _userRepository = IoCUserRepo;
            _context = IoCContext;
            _zones = new ZoneRepository(_context);
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Index()
        {
            var model = GetViewModel();
            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");
            return View(model);
        }

        private ZoneViewModel GetViewModel()
        {
            return GetViewModel(null);
        }

        private ZoneViewModel GetViewModel(int? id)
        {
            var model = new ZoneViewModel();
            model.Sites = _userRepository.getStcUsers((Guid)Membership.GetUser().ProviderUserKey).Select(x => new Dropdown { id = x.userId.ToString(), description = x.userName, visible = true }).OrderBy(x => x.description).ToList();
            model.Zones = Mapper.Map<List<Zone>>(_zones.GetActive().OrderBy(x => x.Zone_Name).ToList());
            //if -1 is passed in as the id, then set the zone to the last zone added
            int? zone = id.HasValue ? (id.Value == -1 ? model.Zones.Any() ? model.Zones[model.Zones.Count - 1].Id.Value : (int?)null : id.Value) 
                : model.Zones.Any() ? model.Zones[0].Id.Value : (int?)null;
            if (zone.HasValue)
            {
                model.selectedZone = zone.Value;
                model.previousZone = zone.Value;
                model.CurrentZoneSites = Mapper.Map<List<Dropdown>>(_context.DataContext.Zones.Where(x => x.Zone_id == zone).First().aspnet_Users
                    .Select(x => new { id = x.UserId, description = x.UserName, visible = true }).OrderBy(x => x.description).ToList());
            } else { model.CurrentZoneSites = new List<Dropdown>(); }

            //set all the visible properties of the main list of sites of those in the currentzonesites to false
            foreach (var site in model.CurrentZoneSites)
            {
                model.Sites.Find(x => x.id == site.id).visible = false;
            }
            return model;
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult ChangeZone(int? id)
        {
            return Json(GetViewModel(id));
        }

        /// <summary>
        /// Adds a zone
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Add(Zone zone)
        {
            try
            {
                var data = _context.DataContext.Zones.Where(x => x.Zone_Name == zone.Name).FirstOrDefault();
                if (data != null)
                {
                    if (data.is_Active.HasValue && !data.is_Active.Value)
                    {
                        data.is_Active = true;
                    }
                    else
                    {
                        return Json("The Zone with that name already exists.");
                    }
                }
                else
                {
                    _context.DataContext.Zones.AddObject(new Data.Zone { Zone_Name = zone.Name, is_Active = true });
                }
                _context.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
            return Json("Success");
        }

        /// <summary>
        /// Deletes the zone
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Delete(int id, string confirm)
        {
            bool bConfirm = false;
            if (!bool.TryParse(confirm, out bConfirm)) { bConfirm = false; }
            try
            {
                var data = _context.DataContext.Zones.Where(x => x.Zone_id == id).First();
                foreach(var user in data.aspnet_Users)
                {
                    data.aspnet_Users.Remove(user);
                }
                _context.DataContext.DeleteObject(data);
                _context.DataContext.SaveChanges();
            }
            catch
            {
                if (!bConfirm)
                {
                    return Json(@"Zone has been used for historical data. It will be made innactive instead. 
If you wish to make it active again, then click on the + button and type in the zone name(not case sensitive). 
Are you sure you want to innactivate the zone?");
                }
                _context = new Data.DSNYContext();
                var data = _context.DataContext.Zones.Where(x => x.Zone_id == id).First();
                data.is_Active = false;
                _context.DataContext.SaveChanges();
            }
            return Json("Success");
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Save(ZoneViewModel viewModel)
        {
            var data = _context.DataContext.Zones.Where(x => x.Zone_id == viewModel.selectedZone).First();
            if (data.aspnet_Users.Any())
            {
                foreach (var site in data.aspnet_Users.ToList())
                {
                    if (viewModel.CurrentZoneSites == null) { data.aspnet_Users.Remove(site); }
                    else
                    {
                        var siteIds = viewModel.CurrentZoneSites.Select(x => x.id).ToList();
                        if (!siteIds.Contains(site.UserId.ToString())) { data.aspnet_Users.Remove(site); }
                    }
                }
            }
            if (viewModel.CurrentZoneSites != null)
            {
                foreach (var o in viewModel.CurrentZoneSites)
                {
                    var user = _context.DataContext.aspnet_Users.Where(x => x.UserId == new Guid(o.id)).First();
                    data.aspnet_Users.Add(user);
                }
            }
            _context.DataContext.SaveChanges();

            return Json("Success");
        }
    }
}