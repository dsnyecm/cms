﻿using System;
using System.Web.Mvc;
using System.Web.Security;

using DSNY.Core.Interfaces;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Data;
using DSNY.ViewModels;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the administration area
    /// </summary>
    
    public class DailyReportController : BaseController
    {
        #region Variables

        private IDailyReportRepository _dailyReportRepository;
        private IUserRepository _userRepository;
        private IDSNYExceptionRepository _dsnyExceptionRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyReportController"/> class.
        /// </summary>
        /// <param name="IoCMessageRepo">The inversion of control  message repo.</param>
        /// <param name="IoCUserRepo">The inversion of control  user repo.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public DailyReportController(IDailyReportRepository IoCDailyReportRepo, IUserRepository IoCUserRepository, IDSNYExceptionRepository IoCDSNYExceptionRepo, 
            ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _dailyReportRepository = IoCDailyReportRepo;
            _userRepository = IoCUserRepository;
            _dsnyExceptionRepository = IoCDSNYExceptionRepo;
        }

        #endregion

        #region Actions

        /// <summary>
        /// Action for the admin index, only system administrators may access
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Search()
        {
            ViewData["saved"] = TempData.ContainsKey("saved") ? TempData["saved"] : false;

            DailyReportViewModel drm = new DailyReportViewModel();

            return View("~/Views/Admin/DailyReport/Search.aspx", drm);
        }

        /// <summary>
        /// AJAX method which takes a field and direction, then returns an HTML table of the sorted items
        /// </summary>
        /// <param name="field">field to sort on</param>
        /// <param name="direction">direction of sort (ASC or DESC)</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult AddDailyReport(FormCollection collection)
        {
            DailyReport newReport = new DailyReport();

            newReport.DailyReportFunction = collection["function"];
            newReport.DailyReportWeather = collection["weather"];
            newReport.DailyReportTempLow = collection["tempLow"];
            newReport.DailyReportTempHigh = collection["tempHigh"];

            newReport.DailyReportDate = DateTime.Now;

            _dailyReportRepository.addDailyReport(newReport);

            return Json(new { message = "success" }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Action for the admin index, only system administrators may access
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "System Administrator")]
        public ActionResult Search(DailyReportViewModel model)
        {
            if (ModelState.IsValid && model.searchDate.HasValue)
            {
                model.dailyReport = _dailyReportRepository.findReport(model.searchDate.Value);
            }

            if (model.dailyReport == null)
            {
                model.dailyReport = new DailyReport()
                {
                    DailyReportDate = model.searchDate.Value
                };
            }

            TempData["isSunday"] = model.searchDate.Value.DayOfWeek == DayOfWeek.Sunday;

            return View("~/Views/Admin/DailyReport/Search.aspx", model);
        }

        /// <summary>
        /// Action for the admin index, only system administrators may access
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult Edit(DailyReportViewModel model)
        {
            if (ModelState.IsValid)
            {
                Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;

                if (model.dailyReport.DailyReportId == 0)
                {
                    _dailyReportRepository.addDailyReport(model.dailyReport);
                } else {
                    _dailyReportRepository.updateDailyReport(model.dailyReport);
                }

                _dsnyExceptionRepository.addException(new DSNYException()
                {
                    ExceptionUser = currentUserGuid,
                    ExceptionDateTime = DateTime.Now,
                    Exception = "Update daily weather report",
                    ExceptionTableName = "DailyReports",
                    ExceptionTableKey = model.dailyReport.DailyReportId,
                    ExceptionCategoryId = _dsnyExceptionRepository.getDSNYExceptionCategory("DRO").ExceptionCategoryId
                });
                TempData["saved"] = true;

                return RedirectToAction("Search", "DailyReport");
            }
            else
            {
                return View("~/Views/Admin/DailyReport/Search.aspx", model);
            }
        }

        #endregion
    }
}