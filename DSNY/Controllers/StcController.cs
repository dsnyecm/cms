﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DSNY.Controllers
{
    public class StcController : Controller
    {
        /// <summary>
        /// Displays the navigation for STC.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
    }
}