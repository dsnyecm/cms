﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

using DSNY.Core.Interfaces;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Core.Repository;
using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;
using System.Net.Mail;
using DSNY.Common.Utilities;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the dashboard area
    /// </summary>
    
    public class DeliveryController : BaseController
    {
        private IUserRepository _userRepository;
        private DSNYContext _context;
        private StcPurchaseOrderRepository _stcPORepository;
        private StcProductRepository _stcProductRepository;
        private SiteOnHandRepository _siteOnHandRepository;

        public DeliveryController(IUserRepository IoCUserRepo, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler, DSNYContext IoCContext)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _userRepository = IoCUserRepo;
            _context = IoCContext;
            _stcPORepository = new StcPurchaseOrderRepository(_context);
            _stcProductRepository = new StcProductRepository(_context);
            _siteOnHandRepository = new SiteOnHandRepository(_context);
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC Site, STC HQ")]
        public ActionResult Index()
        {
            var inputModel = new DeliveryViewModel
            {
                StartDate = DateTime.Today.ToShortDateString()
            };
            var model = GetViewModel(inputModel);
            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");
            return View(model);
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC Site, STC HQ")]
        public ActionResult Product(string id, DeliveryViewModel viewModel)
        {
            if (id != null)
            {
                viewModel.selectedProduct = Mapper.Map<StcProduct>(_stcProductRepository.GetByProductName(id));
            }
            var model = GetViewModel(viewModel);
            return Json(model);
        }

        private DeliveryViewModel GetViewModel()
        {
            return GetViewModel(null);
        }

        private DeliveryViewModel GetViewModel(DeliveryViewModel viewModel)
        {
            var model = new DeliveryViewModel();
            model.Products = Mapper.Map<List<StcProduct>>(_stcProductRepository.GetAll().OrderBy(x => x.SortOrder).ToList());
            var isHQ = _userRepository.IsUserInRole((Guid)Membership.GetUser().ProviderUserKey, "STC HQ");
            if (model.Products.Any())
            {
                if (viewModel != null && viewModel.selectedProduct != null)
                {
                    model.selectedProduct = viewModel.selectedProduct;
                }
                if (model.selectedProduct == null)
                {
                    model.selectedProduct = model.Products[0];
                }
                if (model.selectedProduct.Name == "Salt")
                {
                    model.Addable = isHQ;
                    model.Restricted = !isHQ;
                }
                else
                {
                    model.Addable = true;
                }
                model.Finalizable = isHQ;
                model.Boroughs = Mapper.Map<List<Dropdown>>(_context.DataContext.Boroughs.Select(x => new { id = x.Borough_id, description = x.Borough_Name, visible = true }).ToList());
                model.Sites = new Dictionary<string, List<Dropdown>>();
                var zones = new ZoneRepository(_context).GetActive();
                var sitesUserHasAccessTo = _userRepository.getStcUsersWithActiveSiteOnHand((Guid)Membership.GetUser().ProviderUserKey, model.selectedProduct.Id);
                model.Sites.Add("0", Mapper.Map<List<Dropdown>>(sitesUserHasAccessTo.Select(x => new { id = x.userId, description = x.userName, visible = true }).ToList()));
                foreach (var zone in zones)
                {
                    model.Sites.Add(zone.Zone_id.ToString(), Mapper.Map<List<Dropdown>>(_userRepository.getStcUsersWithActiveSiteOnHand((Guid)Membership.GetUser().ProviderUserKey, model.selectedProduct.Id, zone.Zone_id)
                        .Select(x => new { id = x.userId, description = x.userName, visible = true }).ToList()));
                }
                var purchaseOrders = _stcPORepository.GetActiveByProductId(model.selectedProduct.Id);
                model.PurchaseOrders = purchaseOrders.Any() ? Mapper.Map<List<StcPurchaseOrder>>(purchaseOrders) : new List<StcPurchaseOrder>();
                if (model.Addable) { model.Addable = purchaseOrders.Any(); }
                model.BlankDelivery = new Delivery
                {
                    ProductId = model.selectedProduct.Id,
                    PurchaseOrders = model.PurchaseOrders,
                    selectedPurchaseOrder = purchaseOrders.Any() ? purchaseOrders[0].STC_Purchase_Orders_id : 0,
                    Sites = purchaseOrders.Any() ? Mapper.Map<List<Dropdown>>(model.Sites[purchaseOrders[0].Zones.First().Zone_id.ToString()]) :
                        Mapper.Map<List<Dropdown>>(model.Sites["0"]),
                    DeliveryDate = DateTime.Today.ToShortDateString(),
                    Edit = true,
                    Deleted = false
                };
                var deliveries = _context.DataContext.STC_Delivery.Where(x => x.STC_Product_id == model.selectedProduct.Id);
                if (!isHQ)
                {
                    var siteIds = sitesUserHasAccessTo.Select(x => x.userId).ToList();
                    deliveries = deliveries.Where(x => siteIds.Contains(x.Userid));
                }
                if (viewModel != null)
                {
                    var startDate = DateTime.MinValue;
                    var endDate = DateTime.MaxValue;
                    if (!string.IsNullOrEmpty(viewModel.StartDate))
                    {
                        model.StartDate = viewModel.StartDate;
                        if (DateTime.TryParse(viewModel.StartDate, out startDate))
                        {
                            deliveries = deliveries.Where(x => x.WM_Leave_Date >= startDate);
                        }
                    }
                    if (!string.IsNullOrEmpty(viewModel.EndDate))
                    {
                        model.EndDate = viewModel.EndDate;
                        if (DateTime.TryParse(viewModel.EndDate, out endDate))
                        {
                            endDate = endDate.AddDays(1);
                            deliveries = deliveries.Where(x => x.WM_Leave_Date <= endDate);
                        }
                    }
                    if (!viewModel.Finalized)
                    {
                        deliveries = deliveries.Where(x => !x.is_Finalize.HasValue || x.is_Finalize.Value == false);
                        model.Finalized = false;
                    }
                    if (!string.IsNullOrEmpty(viewModel.selectedSite))
                    {
                        model.selectedSite = viewModel.selectedSite;
                        Guid site = new Guid(viewModel.selectedSite);
                        deliveries = deliveries.Where(x => x.Userid == site);
                    }
                }
                else
                {

                    var startDate = DateTime.Today;
                    model.StartDate = DateTime.Today.ToShortDateString();
                    deliveries = deliveries.Where(x => x.WM_Leave_Date >= startDate);
                    deliveries = deliveries.Where(x => !x.is_Finalize.HasValue || x.is_Finalize.Value == false);
                }
                model.Deliveries = Mapper.Map<List<Delivery>>(deliveries.OrderBy(x => x.Truck_Number).ThenBy(x => x.WM_Leave_Date).ToList());
                foreach(var delivery in model.Deliveries)
                {
                    if (!delivery.Sites.Select(x => x.id).Contains(delivery.selectedSite))
                    {
                        var user = _userRepository.getUser(new Guid(delivery.selectedSite));
                        delivery.Sites.Add(new Dropdown { id = user.userId.ToString(), description = user.userName, visible = true });
                    }
                }
            }
            return model;
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC Site, STC HQ")]
        public ActionResult ChangeFilter(DeliveryViewModel viewModel)
        {
            return Json(GetViewModel(viewModel));
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC Site, STC HQ")]
        public ActionResult Save(DeliveryViewModel viewModel)
        {
            string errors = null;
            var values = viewModel.Deliveries;
            //var values = new List<OnHandViewModel>();
            if (values == null || !values.Any()) { return Json("Nothing to save"); }
            try
            {
                var table = _context.DataContext.STC_Delivery;
                foreach (var o in values)
                {
                    if (o.Deleted)
                    {
                        var order = table.Where(x => x.STC_Delivery_id == o.Id).FirstOrDefault();
                        if (order != null) { _context.DataContext.DeleteObject(order); }
                    }
                    else
                    {
                        var po = _stcPORepository.GetById(o.selectedPurchaseOrder);
                        o.VendorId = po.Vendor_Id;
                        if (o.Id.HasValue && (o.Id.Value > 0))
                        {
                            var row = table.Where(x => x.STC_Delivery_id == o.Id).FirstOrDefault();
                            if (row == null)
                            {
                                var newRow = NewRow(o);
                                table.AddObject(newRow);
                                _context.DataContext.SaveChanges();
                                o.Id = newRow.STC_Delivery_id;
                                o.NewEmail = true;
                            }
                            else
                            {
                                Update(row, o);
                            }
                        }
                        else
                        {
                            var newRow = NewRow(o);
                            table.AddObject(newRow);
                            _context.DataContext.SaveChanges();
                            o.Id = newRow.STC_Delivery_id;
                            o.NewEmail = true;
                        }
                    }
                }
                _context.DataContext.SaveChanges();
                foreach (var o in values)
                {
                    if (o.NewEmail && viewModel.selectedProduct.Name == "Salt")
                    {
                        viewModel.Message += SendEmailToBorough(o);
                    }
                    if (o.UpdateEmail && viewModel.selectedProduct.Name == "Salt")
                    {
                        viewModel.Message += SendEmailToHQ(o);
                    }
                }
                if (!string.IsNullOrEmpty(errors))
                {
                    return Json(viewModel);
                }
            } catch(Exception ex)
            {
                viewModel.Message = ex.Message;
                viewModel.ErrorCode = -1;
                return Json(viewModel);
            }
            return Json(viewModel);
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC Site, STC HQ")]
        public ActionResult Finalize(Delivery value)
        {
            if (value == null) { return Json("Nothing to save"); }
            var table = _context.DataContext.STC_Delivery;
            if (value.Deleted)
            {
                var delivery = table.Where(x => x.STC_Delivery_id == value.Id).FirstOrDefault();
                if (delivery != null) { _context.DataContext.DeleteObject(delivery); }
            }
            else
            {
                var po = _stcPORepository.GetById(value.selectedPurchaseOrder);
                value.VendorId = po.Vendor_Id;
                if (value.Id.HasValue && (value.Id.Value > 0))
                {
                    var row = table.Where(x => x.STC_Delivery_id == value.Id).FirstOrDefault();
                    if (row == null)
                    {
                        table.AddObject(NewRow(value));
                    }
                    else
                    {
                        Update(row, value);
                    }
                }
                else
                {
                    table.AddObject(NewRow(value));
                }
            }
            _context.DataContext.SaveChanges();

            return Json("Success");
        }

        private void Update(STC_Delivery row, Delivery o)
        {
            bool changes = false;
            if (o.Finalized)
            {
                _stcPORepository.SubtractDeliveryAmountRemaining(o.selectedPurchaseOrder, decimal.Parse(o.ReceiveWeight));
                _siteOnHandRepository.AddOnHand(new Guid(o.selectedSite), o.ProductId, decimal.Parse(o.ReceiveWeight));
            }

            DateTime? startDate = null;
            DateTime tempDate;
            if (DateTime.TryParse(o.DeliveryDate + " " + o.LeaveTime, out tempDate)) { startDate = tempDate; }
            DateTime? endDate = null;
            if (o.ReceiveTime != null && o.ReceiveTime != "") {
                if (DateTime.TryParse(o.DeliveryDate + " " + o.ReceiveTime, out tempDate)) { endDate = tempDate; }
            };
            if (startDate > DateTime.Now) { throw new Exception("The date and time must not be in the future."); }
            if (endDate > DateTime.Now) { throw new Exception("The date and time must not be in the future."); }
            if (row.STC_Product_id != o.ProductId)
            {
                row.STC_Product_id = o.ProductId;
                changes = true;
            }
            if (row.STC_Purchase_Order_id != o.selectedPurchaseOrder)
            {
                row.STC_Purchase_Order_id = o.selectedPurchaseOrder;
                changes = true;
            }
            if (row.Vendor_id != o.VendorId.Value)
            {
                row.Vendor_id = o.VendorId.Value;
                changes = true;
            }
            if (row.Userid != new Guid(o.selectedSite))
            {
                row.Userid = new Guid(o.selectedSite);
                changes = true;
            }
            if (row.WM_Leave_Date.Value != startDate)
            {
                row.WM_Leave_Date = startDate;
                changes = true;
            }
            var weight = 0.0M;
            if (decimal.TryParse(o.LeaveWeight, out weight))
            {
                if (row.WM_Qty != weight)
                {
                    row.WM_Qty = weight;
                    changes = true;
                }
            }
            if (row.Boro_Recieved_Date.Value != endDate)
            {
                row.Boro_Recieved_Date = endDate;
                changes = true;
            }
            weight = 0.0M;
            if (decimal.TryParse(o.ReceiveWeight, out weight))
            {
                if (row.Boro_Qty != weight)
                {
                    row.Boro_Qty = weight;
                    changes = true;
                }
            }
            if (row.Ticket_Number != o.TicketNumber)
            {
                row.Ticket_Number = o.TicketNumber;
                changes = true;
            }
            if (row.Truck_Number != o.TruckNumber)
            {
                row.Truck_Number = o.TruckNumber;
                changes = true;
            }
            if (row.Plate_Number != o.PlateNumber)
            {
                row.Plate_Number = o.PlateNumber;
                changes = true;
            }
            if (row.WM_Name != o.WeightMasterName)
            {
                row.WM_Name = o.WeightMasterName;
                changes = true;
            }
            if (row.WM_Badge_Number != o.WeightMasterBadge){
                row.WM_Badge_Number = o.WeightMasterBadge;
                changes = true;
            }
            if (row.Recieving_Super_Name != o.ReceiveSuperName){
                row.Recieving_Super_Name = o.ReceiveSuperName;
                changes = true;
            }
            if (row.Recieving_Super_Badge != o.ReceiveSuperBadge){
                row.Recieving_Super_Badge = o.ReceiveSuperBadge;
                changes = true;
            }
            if (row.Boro_Super_Name != o.BoroSuperName){
                row.Boro_Super_Name = o.BoroSuperName;
                changes = true;
            }
            if (row.Boro_Super_Badge != o.BoroSuperBadge){
                row.Boro_Super_Badge = o.BoroSuperBadge;
                changes = true;
            }
            if (row.is_Finalize.Value != o.Finalized){
                row.is_Finalize = o.Finalized;
                changes = true;
            }

            if (_userRepository.IsUserInRole((Guid)Membership.GetUser().ProviderUserKey, "STC Borough") && changes)
            {
                o.UpdateEmail = true;
            }
        }

        private STC_Delivery NewRow(Delivery o)
        {
            if (o.Finalized)
            {
                _stcPORepository.SubtractDeliveryAmountRemaining(o.selectedPurchaseOrder, decimal.Parse(o.ReceiveWeight));
                _siteOnHandRepository.AddOnHand(new Guid(o.selectedSite), o.ProductId, decimal.Parse(o.ReceiveWeight));
            }

            DateTime? startDate = null;
            DateTime tempDate;
            if (DateTime.TryParse(o.DeliveryDate + " " + o.LeaveTime, out tempDate)) { startDate = tempDate; }
            DateTime? endDate = null;
            if (DateTime.TryParse(o.DeliveryDate + " " + o.ReceiveTime, out tempDate)) { endDate = tempDate; }
            if (startDate > DateTime.Now) { throw new Exception("The date and time must not be in the future."); }
            if (endDate > DateTime.Now) { throw new Exception("The date and time must not be in the future."); }
            var delivery = new STC_Delivery
            {
                STC_Product_id = o.ProductId,
                STC_Purchase_Order_id = o.selectedPurchaseOrder,
                Vendor_id = o.VendorId.Value,
                Ticket_Number = o.TicketNumber,
                Truck_Number = o.TruckNumber,
                Plate_Number = o.PlateNumber,
                Userid = new Guid(o.selectedSite),
                WM_Leave_Date = startDate,
                Boro_Recieved_Date = endDate,
                WM_Name = o.WeightMasterName,
                WM_Badge_Number = o.WeightMasterBadge,
                Recieving_Super_Name = o.ReceiveSuperName,
                Recieving_Super_Badge = o.ReceiveSuperBadge,
                Boro_Super_Name = o.BoroSuperName,
                Boro_Super_Badge = o.BoroSuperBadge,
                is_Finalize = o.Finalized
            };
            var weight = 0.0M;
            if (decimal.TryParse(o.LeaveWeight, out weight))
            {
                delivery.WM_Qty = weight;
            }
            if (decimal.TryParse(o.ReceiveWeight, out weight))
            {
                delivery.Boro_Qty = weight;
            }
            return delivery;
        }

        private string SendEmailToBorough(Delivery value)
        {
            try
            {
                var measurement = _context.DataContext.STC_Product.Where(x => x.STC_Product_id == value.ProductId).Select(x => x.Measurement.Measurement_Name).FirstOrDefault();
                // set up the mail message going out
                MailMessage mm = new MailMessage();

                // setup message
                mm.Subject = string.Format("New Delivery Created By HQ");
                mm.IsBodyHtml = false;
                //mm.BodyEncoding = Encoding.UTF8;
                mm.Body = string.Format(@"HQ has created new delivery line items for date {0}. Please go to CMS and update the line items with the borough delivery information.",
                value.DeliveryDate);

                var users = _userRepository.getStcBoroughUserBySiteUserId(new Guid(value.selectedSite));
                if (users.Count == 0) {
                    var site = _userRepository.getUser(new Guid(value.selectedSite));
                    throw new Exception(string.Format("The site {0} does not have a STC Borough user configured in the STC Site's chain of command.", site.userName));
                }
                foreach (var user in users)
                {
                    mm.To.Add(new MailAddress(user.email, string.IsNullOrEmpty(user.fullName) ? string.Empty : user.fullName));
                }

                EmailUtilities.SendEmail(mm);

                return null;
            }
            catch (Exception ex)
            {
                return string.Format("An email could not be sent due to the following error: {0}\n", ex.Message);
            }
        }

        private string SendEmailToHQ(Delivery value)
        {
            try
            {
                var site = _userRepository.getUser(new Guid(value.selectedSite));
                var currentUser = _userRepository.getUser((Guid)Membership.GetUser().ProviderUserKey);
                var measurement = _context.DataContext.STC_Product.Where(x => x.STC_Product_id == value.ProductId).Select(x => x.Measurement.Measurement_Name).FirstOrDefault();
                // set up the mail message going out
                MailMessage mm = new MailMessage();

                // setup message
                mm.Subject = string.Format("Delivery Updated By {0}", currentUser.userName);
                mm.IsBodyHtml = false;
                //mm.BodyEncoding = Encoding.UTF8;
                mm.Body = string.Format(@"Records in delivery for {0} are awaiting finalization.", site.borough);

                var users = _userRepository.getStcBoroughUserBySiteUserId(new Guid(value.selectedSite));
                if (users.Count == 0)
                {
                    throw new Exception(string.Format("The site {0} does not have a STC Borough user configured in the STC Site's chain of command.", site.userName));
                }
                foreach (var user in users)
                {
                    mm.To.Add(new MailAddress(user.email, string.IsNullOrEmpty(user.fullName) ? string.Empty : user.fullName));
                }

                EmailUtilities.SendEmail(mm);

                return null;
            }
            catch (Exception ex)
            {
                return string.Format("An email could not be sent due to the following error: {0}\n", ex.Message);
            }
        }
    }
}