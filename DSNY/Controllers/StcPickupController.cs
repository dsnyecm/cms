using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using DSNY.Core.Interfaces;
using DSNY.Common;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Core.Repository;
using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the dashboard area
    /// </summary>
    [HandleError]
    public class StcPickupController : Controller
    {
        private IUserRepository _userRepository;
        private ILogger _logger;
        private IExceptionHandler _exceptionHandler;
        private DSNYContext _context;
        private StcPickupRepository _pickups;
        private StcProductRepository _products;
        private StcAgencyRepository _agencies;
        private SiteOnHandRepository _siteOnHandRepository;

        public StcPickupController(IUserRepository IoCUserRepo, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler, DSNYContext IoCContext)
        {
            _userRepository = IoCUserRepo;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
            _context = IoCContext;
            _pickups = new StcPickupRepository(_context);
            _products = new StcProductRepository(_context);
            _agencies = new StcAgencyRepository(_context);
            _siteOnHandRepository = new SiteOnHandRepository(_context);
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC Site, STC HQ")]
        public ActionResult Index()
        {
            var isHQ = _userRepository.IsUserInRole((Guid)Membership.GetUser().ProviderUserKey, "STC HQ");
            var vm = new StcPickupViewModel
            {
                External = true
            };
            var model = GetViewModel(vm);
            model.Finalizable = isHQ;
            model.BoroSignable = true; //Bug 216, make everyone able to add the borough super
            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");
            return View(model);
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC Site, STC HQ")]
        public ActionResult DSNY()
        {
            var vm = new StcPickupViewModel
            {
                External = false
            };
            var isHQ = _userRepository.IsUserInRole((Guid)Membership.GetUser().ProviderUserKey, "STC HQ");
            var isBorough = _userRepository.IsUserInRole((Guid)Membership.GetUser().ProviderUserKey, "STC Borough");
            var model = GetViewModel(vm);
            model.BoroSignable = isHQ || isBorough;
            model.Finalizable = isHQ;
            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");
            return View("Index", model);
        }

        private StcPickupViewModel GetViewModel()
        {
            return GetViewModel(null);
        }

        private StcPickupViewModel GetViewModel(StcPickupViewModel viewModel)
        {
            var model = new StcPickupViewModel();
            model.Sites = new List<Dropdown>[4];
            var sites = _userRepository.getStcUsersWithActiveSiteOnHand((Guid)Membership.GetUser().ProviderUserKey);
            var siteIds = sites.Select(x => x.userId).ToList();
            model.Products = Mapper.Map<List<StcProduct>>(_products.GetByUserIds(siteIds));
            model.Sites[0] = Mapper.Map<List<Dropdown>>(_userRepository.getStcUsersWithActiveSiteOnHand((Guid)Membership.GetUser().ProviderUserKey).Select(x => new { id = x.userId, description = x.userName, visible = true }).ToList());
            model.Sites[1] = Mapper.Map<List<Dropdown>>(_userRepository.getStcUsersWithActiveSiteOnHand((Guid)Membership.GetUser().ProviderUserKey, 1).Select(x => new { id = x.userId, description = x.userName, visible = true }).ToList());
            model.Sites[2] = Mapper.Map<List<Dropdown>>(_userRepository.getStcUsersWithActiveSiteOnHand((Guid)Membership.GetUser().ProviderUserKey, 2).Select(x => new { id = x.userId, description = x.userName, visible = true }).ToList());
            model.Sites[3] = Mapper.Map<List<Dropdown>>(_userRepository.getStcUsersWithActiveSiteOnHand((Guid)Membership.GetUser().ProviderUserKey, 3).Select(x => new { id = x.userId, description = x.userName, visible = true }).ToList());
            var internalAgency = _agencies.GetByName(ConfigurationManager.AppSettings["DSNY_Agency_Name"]);
            if (viewModel != null && viewModel.External == false)
            {
                model.External = false;
                model.Agencies = new List<StcAgency>();
                if (internalAgency != null)
                {
                    model.Agencies.Add(Mapper.Map<StcAgency>(internalAgency));
                }
            }
            else
            {
                model.External = true;
                var agencies = _agencies.GetAll().AsQueryable();
                if (internalAgency != null)
                {
                    agencies = agencies.Where(x => x.STC_Agency_id != internalAgency.STC_Agency_id);
                }
                model.Agencies = Mapper.Map<List<StcAgency>>(agencies.ToList());
            }
            if (model.Products.Any() && model.Sites.Any() && model.Agencies.Any())
            {
                model.BlankStcPickup = new StcPickup
                {
                    ProductId = model.Products[0].Id,
                    PickupDate = DateTime.Now.ToShortDateString(),
                    Finalized = false,
                    Edit = true,
                    Deleted = false
                };
                if (model.Agencies.Count == 1)
                {
                    model.BlankStcPickup.AgencyId = model.Agencies[0].Id.Value;
                }
                var pickup = _pickups.GetByUserIds(siteIds).AsQueryable();
                if (internalAgency != null)
                {
                    if (model.External)
                    {
                        pickup = pickup.Where(x => x.Agency_Id != internalAgency.STC_Agency_id);
                    }
                    else
                    {
                        pickup = pickup.Where(x => x.Agency_Id == internalAgency.STC_Agency_id);
                    }
                }
                if (viewModel != null)
                {
                    var startDate = DateTime.MinValue;
                    var endDate = DateTime.MaxValue;
                    if (!string.IsNullOrEmpty(viewModel.StartDate))
                    {
                        if (DateTime.TryParse(viewModel.StartDate, out startDate))
                        {
                            pickup = pickup.Where(x => x.Pickup_Date >= startDate);
                        }
                    }
                    if (!string.IsNullOrEmpty(viewModel.EndDate))
                    {
                        if (DateTime.TryParse(viewModel.EndDate, out endDate))
                        {
                            endDate = endDate.AddDays(1);
                            pickup = pickup.Where(x => x.Pickup_Date <= endDate);
                        }
                    }
                    if (!string.IsNullOrEmpty(viewModel.selectedSite))
                    {
                        Guid site = new Guid(viewModel.selectedSite);
                        pickup = pickup.Where(x => x.Userid == site);
                    }
                    if (!viewModel.Finalized)
                    {
                        pickup = pickup.Where(x => x.is_finalized == viewModel.Finalized);
                    }
                }
                else if (viewModel == null)
                {
                    var startDate = DateTime.Today;
                    model.StartDate = DateTime.Today.ToShortDateString();
                    pickup = pickup.Where(x => x.Pickup_Date >= startDate && x.is_finalized != true);
                }
                model.StcPickups = Mapper.Map<List<StcPickup>>(pickup.ToList());
            }
            return model;
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC Site, STC HQ")]
        public ActionResult ChangeFilter(StcPickupViewModel viewModel)
        {
            return Json(GetViewModel(viewModel));
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC Site, STC HQ")]
        public ActionResult Finalize(StcPickup value)
        {
            if (value == null) { return Json("Nothing to save"); }
            var table = _context.DataContext.STC_Pickup;
            if (value.Deleted)
            {
                var order = table.Where(x => x.STC_Pickup_id == value.Id).FirstOrDefault();
                if (order != null) { _context.DataContext.DeleteObject(order); }
            }
            else
            {
                if (value.Id.HasValue && (value.Id.Value > 0))
                {
                    var row = table.Where(x => x.STC_Pickup_id == value.Id).FirstOrDefault();
                    if (row == null)
                    {
                        table.AddObject(NewRow(value));
                    }
                    else
                    {
                        Update(row, value);
                    }
                }
                else
                {
                    table.AddObject(NewRow(value));
                }
            }
            _context.DataContext.SaveChanges();

            return Json("Success");
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC Site, STC HQ")]
        public ActionResult Save(List<StcPickup> values)
        {
            if (values == null || !values.Any()) { return Json("Nothing to save"); }
            try
            {
                var table = _context.DataContext.STC_Pickup;
                foreach (var o in values)
                {
                    if (o.Deleted)
                    {
                        var order = table.Where(x => x.STC_Pickup_id == o.Id).FirstOrDefault();
                        if (order != null) { _context.DataContext.DeleteObject(order); }
                    }
                    else
                    {
                        if (o.Id.HasValue && (o.Id.Value > 0))
                        {
                            var row = table.Where(x => x.STC_Pickup_id == o.Id).FirstOrDefault();
                            if (row == null)
                            {
                                table.AddObject(NewRow(o));
                            }
                            else
                            {
                                Update(row, o);
                            }
                        }
                        else
                        {
                            table.AddObject(NewRow(o));
                        }
                    }
                }
                _context.DataContext.SaveChanges();

                return Json("Success");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        private void Update(STC_Pickup row, StcPickup o)
        {
            if (o.Finalized)
            {
                _siteOnHandRepository.SubtractOnHand(new Guid(o.UserId), o.ProductId, decimal.Parse(o.Qty));
            }

            DateTime? pickupdate = null;
            DateTime tempDate;
            if (DateTime.TryParse(o.PickupDate + " " + o.PickupTime, out tempDate)) { pickupdate = tempDate; }
            if (pickupdate > DateTime.Now) { throw new Exception("The date and time must not be in the future."); }
            row.STC_Product_id = o.ProductId;
            row.Agency_Id = o.AgencyId;
            row.Driver_Name = o.DriverName;
            row.Truck_Number = o.TruckNumber;
            row.Plate_Number = o.PlateNumber;
            row.Userid = new Guid(o.UserId);
            row.Pickup_Date = pickupdate;
            row.Qty = decimal.Parse(o.Qty);
            row.District_Super_Name = o.DistrictSuperName;
            row.District_Super_Badge = o.DistrictSuperBadge;
            row.Boro_Super_Name = o.BoroSuperName;
            row.Boro_Super_Badge = o.BoroSuperBadge;
            row.is_finalized = o.Finalized;
        }

        private STC_Pickup NewRow(StcPickup o)
        {
            if (o.Finalized)
            {
                _siteOnHandRepository.SubtractOnHand(new Guid(o.UserId), o.ProductId, decimal.Parse(o.Qty));
            }

            DateTime? pickupdate = null;
            DateTime tempDate;
            if (DateTime.TryParse(o.PickupDate + " " + o.PickupTime, out tempDate)) { pickupdate = tempDate; }
            if (pickupdate > DateTime.Now) { throw new Exception("The date and time must not be in the future."); }
            return new STC_Pickup
            {
                STC_Product_id = o.ProductId,
                Agency_Id = o.AgencyId,
                Driver_Name = o.DriverName,
                Truck_Number = o.TruckNumber,
                Plate_Number = o.PlateNumber,
                Userid = new Guid(o.UserId),
                Pickup_Date = pickupdate,
                Qty = decimal.Parse(o.Qty),
                District_Super_Name = o.DistrictSuperName,
                District_Super_Badge = o.DistrictSuperBadge,
                Boro_Super_Name = o.BoroSuperName,
                Boro_Super_Badge = o.BoroSuperBadge,
                is_finalized = o.Finalized
            };
        }
    }
}
