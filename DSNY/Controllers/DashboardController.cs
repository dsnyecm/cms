﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;

using DSNY.Core.Interfaces;
using DSNY.Common;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Core.Repository;
using DSNY.Data;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the dashboard area
    /// </summary>
    
    public class DashboardController : BaseController
    {
        #region Variables

        private IMessageRepository _messageRepository;
        private IUserRepository _userRepository;
        private IDailyReportRepository _dailyReportRepository;

        private int pageSize = MvcApplication.pageSize;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DashboardController"/> class.
        /// </summary>
        /// <param name="IoCMessageRepo">The inversion of control  message repo.</param>
        /// <param name="IoCUserRepo">The inversion of control  user repo.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public DashboardController(IMessageRepository IoCMessageRepo, IUserRepository IoCUserRepo, DailyReportRepository IoCDailyReportRepo,
            ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _messageRepository = IoCMessageRepo;
            _userRepository = IoCUserRepo;
            _dailyReportRepository = IoCDailyReportRepo;
        }

        #endregion

        #region Actions

        /// <summary>
        /// Redirects to the index of the dashboard home
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Shows admin messages, can accept a list of sent messages, usually used by the search action
        /// </summary>
        /// <param name="messages">List of sent messages which can be displayed instead of standard sent messages (all sent message for user)</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrator")]
        public ActionResult Admin(List<Sent_Message> messages, string isFuelForm, FormCollection collection)
        {
            // Daily Report Check
            TempData["TodayDailyReport"] = _dailyReportRepository.findReport(DateTime.Now.Date);

            Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;
            List<IUser> fuelFormRequired = _userRepository.getMissingFuelForms(currentUserGuid);
            int totalRecords = 0;

            // check if there are fuel forms still due for this user
            if (fuelFormRequired.Count > 0)
            {
                ViewData["MissingFuelForms"] = fuelFormRequired.Count + " fuel forms missing";
                ViewData["MissingFuelFormUsers"] = fuelFormRequired;
            }

            // parse what page we're on
            int page = 1;

            if (!string.IsNullOrEmpty(collection["page"]))
                int.TryParse(collection["page"], out page);

            bool fuelForm;

            if (TempData["view"] == null && isFuelForm == null)
                fuelForm = true;
            else
                fuelForm = isFuelForm == "on" ? true : false;
            
            TempData["view"] = "admin";

            if (messages == null) 
            {
                List<Sent_Message> returnMessages =
                    _messageRepository.getMessagesForUser(currentUserGuid, DateTime.Now.AddDays(-90), DateTime.Now.AddDays(1), fuelForm, page, pageSize, out totalRecords, true);

                TempData["totalRecords"] = totalRecords;

                return View(returnMessages);
            }
            else
                return View(messages);
        }

        /// <summary>
        /// Shows field messages, can accept a list of sent messages, usually used by the search action
        /// </summary>
        /// <param name="messages">List of sent messages which can be displayed instead of standard sent messages (all sent message for user)</param>
        /// <returns></returns>
        [Authorize(Roles = "Field, Garage")]
        public ActionResult Field(List<Sent_Message> messages, string isFuelForm, FormCollection collection)
        {
            Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;
            List<IUser> fuelFormRequired = _userRepository.getMissingFuelForms(currentUserGuid);
            int totalRecords = 0;

            if (fuelFormRequired.Count > 0)
            {
                ViewData["MissingFuelForms"] = fuelFormRequired.Count + " fuel forms missing";
                ViewData["MissingFuelFormUsers"] = fuelFormRequired;
            }

            // parse what page we're on
            int page = 1;

            if (!string.IsNullOrEmpty(collection["page"]))
                int.TryParse(collection["page"], out page);

            bool fuelForm;

            if (TempData["view"] == null && isFuelForm == null)
                fuelForm = true;
            else
                fuelForm = isFuelForm == "on" ? true : false;

            TempData["view"] = "field";
            TempData["totalRecords"] = totalRecords;

            if (messages == null)
            {
                List<Sent_Message> returnMessages =
                    _messageRepository.getMessagesForUser(currentUserGuid, DateTime.Now.AddDays(-90), DateTime.Now.AddDays(1), fuelForm, page, pageSize, out totalRecords, true);

                TempData["totalRecords"] = totalRecords;

                return View(returnMessages);
            }
            else
                return View(messages);
        }

        /// <summary>
        /// Search method takes the terms, from date and to date and returns all sent messages which match criteria that match the current logged in 
        /// user's Id
        /// </summary>
        /// <param name="searchTerms">search terms to search on</param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        [Authorize(Roles = "Administrator, Field, Garage")]
        [HttpPost]
        public ActionResult Search(string searchTerms, string fromDate, string toDate, string isFuelForm, string sortColumn, string direction, FormCollection collection)
        {
            List<Sent_Message> searchResults = null;
            int totalRecords = 0;
            //string sortColumn = string.Empty, direction = string.Empty;

            // parse the from/to dates from strings
            DateTime startDate = DateTime.Now.AddDays(-90), endDate = DateTime.Now;
            DateTime.TryParse(fromDate, out startDate);
            DateTime.TryParse(toDate, out endDate);
        
            // parse what page we're on
            int page = 1;

            if (!string.IsNullOrEmpty(collection["page"]))
                int.TryParse(collection["page"], out page);

            // parse if a fuel form or not
            bool fuelForm = isFuelForm == "on" ? true : false;

            // if the search terms are empty, a simple query is faster than a full text search
            if (!string.IsNullOrEmpty(searchTerms))
            {
                searchResults = _messageRepository.searchMessages(searchTerms, startDate, endDate, (Guid)Membership.GetUser().ProviderUserKey, fuelForm, page, pageSize,
                    sortColumn, direction, out totalRecords);
            }
            else
            {
                if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(direction))
                {
                    searchResults = _messageRepository.getSortedMessagesForUser(sortColumn, direction == "ASC" ? Enums.SortDirection.Ascending : Enums.SortDirection.Descending,
                        (Guid)Membership.GetUser().ProviderUserKey, startDate, endDate, searchTerms, fuelForm, page, pageSize, out totalRecords);
                }
                else
                    searchResults = _messageRepository.getMessagesForUser((Guid)Membership.GetUser().ProviderUserKey, startDate, endDate, fuelForm, page, pageSize, out totalRecords, true);
            }

            // set the viewdata up so the values inputted to search are retained
            ViewData["searchTerms"] = searchTerms;
            ViewData["fromDate"] = fromDate;
            ViewData["toDate"] = toDate;
            ViewData["searchFuelForm"] = fuelForm;
            TempData["totalRecords"] = totalRecords;
            TempData["sortField"] = sortColumn;
            TempData["direction"] = direction;

            // the alert functions
            Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;
            List<IUser> fuelFormRequired = _userRepository.getMissingFuelForms(currentUserGuid);

            if (fuelFormRequired.Count > 0)
            {
                ViewData["MissingFuelForms"] = fuelFormRequired.Count + " fuel forms missing";
                ViewData["MissingFuelFormUsers"] = fuelFormRequired;
            }

            // depdning on where the page came from, send back to the correct page
            if (collection["view"] != null && collection["view"].ToString() == "admin")
            {
                TempData["view"] = "admin";
                return View("Admin", searchResults);
            }
            else
            {
                TempData["FuelForm"] = "true";
                TempData["view"] = "field";
                return View("Field", searchResults);
            }
        }

        /// <summary>
        /// AJAX method which takes a field and direction, then returns an HTML table of the sorted items
        /// </summary>
        /// <param name="field">field to sort on</param>
        /// <param name="direction">direction of sort (ASC or DESC)</param>
        /// <returns></returns>
        [Authorize(Roles = "Administrator, Field, Garage")]
        [HttpGet]
        public ActionResult SortedGrid(string field, string direction, string fuelForm, string page, string fromDate, string toDate, string searchTerms, string view)
        {
            int totalRecords = 0;

            // parse the from/to dates from strings
            DateTime startDate = DateTime.Now.AddDays(-90), endDate = DateTime.Now;
            DateTime.TryParse(fromDate, out startDate);
            DateTime.TryParse(toDate, out endDate);

            // parse what page we're on if it exists
            int sortPage = 1;
            
            if (page != null)
                int.TryParse(page, out sortPage);

            // parse if a fuel form or not
            bool isFuelForm = fuelForm == "on" ? true : false;

            if (view != null && view == "admin")
                TempData["view"] = "admin";
            else
                TempData["view"] = "field";

            List<Sent_Message> messages = null;

            if (!string.IsNullOrEmpty(searchTerms))
            {
                messages = _messageRepository.searchMessages(searchTerms, startDate, endDate, (Guid)Membership.GetUser().ProviderUserKey, isFuelForm, sortPage, pageSize,
                    field, direction, out totalRecords);
            }
            else
            {
                messages = _messageRepository.getSortedMessagesForUser(field, direction == "ASC" ? Enums.SortDirection.Ascending : Enums.SortDirection.Descending,
                (Guid)Membership.GetUser().ProviderUserKey, startDate, endDate, searchTerms, isFuelForm, sortPage, pageSize, out totalRecords);
            }

            TempData["totalRecords"] = totalRecords;
            TempData["sortField"] = field;
            TempData["direction"] = direction;

            return View("Grids/MessageGrid", messages);
        }

        #endregion
    }
}