﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

using DSNY.Core.Interfaces;
using DSNY.Core.Models;
using DSNY.Common.Exception;
using DSNY.Common.Logger;
using DSNY.Data;
using DSNY.ViewModels;
using System.Web.Script.Serialization;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for <see cref="FuelForm"/> pages
    /// </summary>
    public class FuelFormController : BaseController
    {
        #region Variables

        private IFuelFormRepository _fuelFormRepository;
        private IMessageRepository _messageRepository;
        private IProductRepository _productRepository;
        private IEquipmentRepository _equipmentRepository;
        private IVendorRepository _vendorRepository;
        private IUserRepository _userRepository;
        private IDSNYExceptionRepository _dsnyExceptionRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FuelFormController"/> class.
        /// </summary>
        /// <param name="IoCFuelForm">The inversion of control fuel form.</param>
        /// <param name="IoCMessageRepository">The inversion of control message repository.</param>
        /// <param name="IoCProductRepo">The inversion of control product repo.</param>
        /// <param name="IoCEquipRepo">The inversion of control equip repo.</param>
        /// <param name="IoCVendorRepo">The inversion of control vendor repo.</param>
        /// <param name="IoCUserRepo">The inversion of control user repo.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public FuelFormController(IFuelFormRepository IoCFuelForm, IMessageRepository IoCMessageRepository, IProductRepository IoCProductRepo,
            IEquipmentRepository IoCEquipRepo, IVendorRepository IoCVendorRepo, IUserRepository IoCUserRepo, IDSNYExceptionRepository IoCDSNYExceptionRepo,
            ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
            : base(IoCLogger, IoCExceptionHandler)
        {

            _fuelFormRepository = IoCFuelForm;
            _messageRepository = IoCMessageRepository;
            _productRepository = IoCProductRepo;
            _equipmentRepository = IoCEquipRepo;
            _vendorRepository = IoCVendorRepo;
            _userRepository = IoCUserRepo;
            _dsnyExceptionRepository = IoCDSNYExceptionRepo;
        }

        #endregion

        #region Actions

        /// <summary>
        /// Displays fuel form for entry
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrator, Field, Garage")]
        public ActionResult Create()
        {
            Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;

            // checking for remarks from previous day fuel form
            Fuel_Form previousFuelForm = _fuelFormRepository.getLastFuelForm(currentUserGuid, null, 1);
            List<Product_User> userProducts = _productRepository.getProductsForUser(currentUserGuid, true);
            Fuel_Form newFuelForm = new Fuel_Form();

            if (previousFuelForm != null)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                if (!string.IsNullOrEmpty(previousFuelForm.Remarks))
                {
                    newFuelForm.Remarks = previousFuelForm.Remarks;
                }

                ViewData["PreviousFuelFormDetails"] = previousFuelForm.Fuel_Form_Details;

                ViewData["PreviousFuelFormDetailsJson"] = serializer.Serialize(
                    previousFuelForm.Fuel_Form_Details.Select(f => new
                    {
                        productId = f.Product_ID,
                        productName = f.Product.Product_Name,
                        equipUserId = f.Equipment_User_ID,
                        spareDrums = f.Spare_Drums,
                        tankDelivered = f.Tank_Delivered,
                        tankDispensed = f.Tank_Dispensed,
                        qtyOnHand = f.On_Hand
                    }).ToList()
                );

                ViewData["PreviousEquipmentFailuresJson"] = serializer.Serialize(
                    previousFuelForm.Fuel_Form_Equipment_Failure.Select(f => new
                    {
                        isFailure = f.is_Equipment_Failure,
                        failureDate = f.Failure_Date,
                        fixDate = f.Fix_Date,
                        remarks = f.Remarks,
                        equipmentUserId = f.Equipment_User_ID
                    }).ToList()
                );
            }

            FuelFormViewModel createViewModel = new FuelFormViewModel()
            {
                isEdit = false,
                fuelForm = newFuelForm,
                fuelFormDetails = new List<Fuel_Form_Details>(),
                fuelFormDetailsDelivery = new List<Fuel_Form_Details_Delivery>(),
                fuelFormEquipFailure = new List<Fuel_Form_Equipment_Failure>(),
                exceptions = new List<DSNY.Core.Models.DSNYException>()
            };

            // add new equip failures, will map to prev values
            _equipmentRepository.getEquipmentForUser(currentUserGuid, false, false).ToList()
                .ForEach(m => createViewModel.fuelFormEquipFailure.Add(new Fuel_Form_Equipment_Failure()
                {
                    Equipment = m.Equipment,
                    Equipment_Failure_ID = 0,
                    Equipment_User_ID = m.Equipment_User_ID,
                    Fuel_Form = createViewModel.fuelForm,
                    Fuel_Form_ID = createViewModel.fuelForm.Fuel_Form_ID
                }));

            // update failures with previous, if they exist
            if (previousFuelForm != null && previousFuelForm.Fuel_Form_Equipment_Failure != null && previousFuelForm.Fuel_Form_Equipment_Failure.Count > 0)
            {
                foreach (Fuel_Form_Equipment_Failure failure in createViewModel.fuelFormEquipFailure.Where(e => !e.Equipment_User.Product.is_Water).ToList())
                {
                    Fuel_Form_Equipment_Failure tempFail = previousFuelForm.Fuel_Form_Equipment_Failure.SingleOrDefault(m => m.Equipment_User_ID == failure.Equipment_User_ID);

                    if (tempFail != null && tempFail.Fix_Date == null)
                    {
                        failure.Failure_Date = tempFail.Failure_Date;
                        failure.Fix_Date = tempFail.Fix_Date;
                        failure.is_Equipment_Failure = tempFail.is_Equipment_Failure;
                        failure.Remarks = tempFail.Remarks;
                    }
                }
            }

            List<Equipment_User> equipUser = _equipmentRepository.getEquipmentForUser(currentUserGuid, false, true);

            if (userProducts != null && userProducts.Count > 0)
                userProducts.ForEach(m =>
                {
                    List<Equipment_User> relatedEquip = equipUser.Where(pe => pe.Product_ID == m.Product_ID && pe.Equipment.has_Capacity).ToList();

                    if (relatedEquip.Count > 0)
                    {
                        foreach (var equip in relatedEquip)
                        {
                            createViewModel.fuelFormDetails.Add(new DSNY.Data.Fuel_Form_Details()
                            {
                                Fuel_Form_ID = createViewModel.fuelForm.Fuel_Form_ID,
                                Fuel_Form = createViewModel.fuelForm,
                                Equipment_User_ID = equip.Equipment_User_ID,
                                Product_ID = m.Product_ID,
                                Product = m.Product
                            });
                        }
                    }
                    else
                    {
                        createViewModel.fuelFormDetails.Add(new DSNY.Data.Fuel_Form_Details()
                        {
                            Fuel_Form_ID = createViewModel.fuelForm.Fuel_Form_ID,
                            Fuel_Form = createViewModel.fuelForm,
                            Product_ID = m.Product_ID,
                            Product = m.Product,
                        });
                    }
                });

            ViewData["AvailableEquipment"] = equipUser;
            ViewData["Vendors"] = _vendorRepository.getAllVendors(false);
            ViewData["UserProducts"] = userProducts;

            return View(createViewModel);
        }

        /// <summary>
        /// Creates a fuel form from the model and form collection
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrator, Field, Garage")]
        [HttpPost]
        public ActionResult Create(FuelFormViewModel model, FormCollection collection)
        {
            // setup some variables
            int fuelFormId = 0;
            Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;
            Message newMessage = null;  // message variable
            Fuel_Form newFuelForm = null;   // initialize the fuel and details forms

            try
            {
                // add a new fuel form
                newFuelForm = new Fuel_Form()
                {
                    UserId = currentUserGuid,
                    Supervisor_Full_Name = model.fuelForm.Supervisor_Full_Name,
                    Supervisor_Badge_Number = model.fuelForm.Supervisor_Badge_Number,
                    Remarks = model.fuelForm.Remarks,
                    Submission_Date = DateTime.Now
                };
                List<Fuel_Form_Details_Delivery> fuelDetailsDelivery = null;
                int deliveryCount = 0, rowCount = 1;

                // associate details to model
                foreach (var ffd in model.fuelFormDetails)
                {
                    ffd.Fuel_Form_Details_Id = 0;   // set id for new record
                    newFuelForm.Fuel_Form_Details.Add(ffd);
                }

                // update fuel form exceptions
                if (model.fuelFormEquipFailure != null && model.fuelFormEquipFailure.Count > 0)
                {
                    model.fuelFormEquipFailure
                        .ForEach(m =>
                        {
                            if (m.is_Equipment_Failure == true && m.Failure_Date != null)
                                newFuelForm.Fuel_Form_Equipment_Failure.Add(m);
                        });
                }

                // add the form to the DB and grab id for message
                _fuelFormRepository.addFuelForm(newFuelForm);
                fuelFormId = newFuelForm.Fuel_Form_ID;

                // associate details to model
                foreach (var ffd in model.fuelFormDetails)
                {
                    fuelDetailsDelivery = model.fuelFormDetailsDelivery.Where(m => m.Fuel_Form_Details_Id == rowCount).ToList();

                    // associate deliveries to fuel details
                    foreach (var item in fuelDetailsDelivery)
                    {
                        if (item.Vendor_ID > -1 && item.Quantity != null && item.Invoice_Number != null)
                        {
                            item.Fuel_Form_Details_Id = newFuelForm.Fuel_Form_Details.ToList()[rowCount - 1].Fuel_Form_Details_Id;  // set id for new record
                            item.Order_No = deliveryCount++;
                            ffd.Fuel_Form_Details_Delivery.Add(item);

                            _fuelFormRepository.addFuelFormDetailsDelivery(item);
                        }
                    }

                    rowCount++;
                }

                // add exceptions
                if (model.exceptions != null && model.exceptions.Count > 0)
                {

                    // if tableId is negative, then it's the product Id and we need to search through details
                    model.exceptions.ForEach(e =>
                    {
                        if (e.tableId < 0)
                        {
                            int tableId = e.tableId * -1;
                            Fuel_Form_Details details = model.fuelFormDetails.FirstOrDefault(f => f.Product_ID == tableId);
                            e.tableId = details.Fuel_Form_Details_Id;
                        }
                    });


                    _dsnyExceptionRepository.addExceptions(
                        _dsnyExceptionRepository.convertExceptions(model.exceptions, currentUserGuid, "Fuel_Form_Details"));
                }

                // send message
                newMessage = new Message()
                {
                    is_Fuel_Form = true,
                    Message_Date = DateTime.Now,
                    Message_Subject = "Fuel Form from " + User.Identity.Name,
                    Message_Code = "Fuel Form",
                    Sent_By_UserId = (Guid)Membership.GetUser().ProviderUserKey,
                    Fuel_Form_ID = newFuelForm.Fuel_Form_ID
                };

                _messageRepository.addCommandMessage(newMessage, true);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Field", "Dashboard");
            }

            //if (newMessage != null)
            //    _messageRepository.addCommandMessage(newMessage, true);

            return RedirectToAction("Field", "Dashboard");
        }

        /// <summary>
        /// Displays fuel form for entry
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrator, Field, Garage")]
        public ActionResult Edit()
        {
            Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;
            int fuelFormId = 0;

            // checking for remarks from previous day fuel form
            Fuel_Form currentFuelForm = _fuelFormRepository.getFuelForm(currentUserGuid, DateTime.Now);

            if (currentFuelForm != null)
                int.TryParse(currentFuelForm.Fuel_Form_ID.ToString(), out fuelFormId);

            if (fuelFormId > 0)
            {
                List<Fuel_Form_Details_Delivery> fuelFormDetailsDelivery = new List<Fuel_Form_Details_Delivery>();
                currentFuelForm.Fuel_Form_Details.ToList().ForEach(ffd => fuelFormDetailsDelivery.Concat(ffd.Fuel_Form_Details_Delivery)); // fill the delivery view model object
                List<DSNY.Data.DSNYException> exceptions = _dsnyExceptionRepository.getTableExceptions("Fuel_Form_Details", currentFuelForm.Fuel_Form_Details.Select(f => f.Fuel_Form_Details_Id).ToList());

                FuelFormViewModel editModel = new FuelFormViewModel()
                {
                    isEdit = true,
                    fuelForm = currentFuelForm,
                    fuelFormDetails = currentFuelForm.Fuel_Form_Details.ToList(),
                    fuelFormDetailsDelivery = fuelFormDetailsDelivery,
                    fuelFormEquipFailure = _fuelFormRepository.getFuelFormEquipFailure(fuelFormId),
                    submittedBy = _userRepository.getUser((Guid)currentFuelForm.UserId)
                };

                Fuel_Form lastFuelForm = _fuelFormRepository.getLastFuelForm(currentUserGuid, currentFuelForm.Fuel_Form_ID, 1);
                List<Equipment_User> equipUser = _equipmentRepository.getEquipmentForUser(currentUserGuid, false, false).ToList();
                Fuel_Form_Equipment_Failure tempNewFail = new Fuel_Form_Equipment_Failure();

                // add new equip failures, map to prev values if they exist
                foreach (Equipment_User item in equipUser)
                {
                    // link equipment to product
                    Fuel_Form_Details tempFfd = editModel.fuelFormDetails.SingleOrDefault(m => m.Product_ID == item.Product_ID && m.Equipment_User_ID == item.Equipment_User_ID);

                    if (tempFfd != null)
                    {
                        tempFfd.Equipment_User_ID = item.Equipment_User_ID;
                    }

                    Fuel_Form_Equipment_Failure tempCurrentFail = editModel.fuelFormEquipFailure.SingleOrDefault(m => m.Equipment_User_ID == item.Equipment_User_ID);

                    // failure doesn't exist
                    if (tempCurrentFail == null)
                    {
                        tempNewFail = new Fuel_Form_Equipment_Failure()
                        {
                            Fuel_Form_ID = editModel.fuelForm.Fuel_Form_ID,
                            Equipment_User_ID = item.Equipment_User_ID,
                            Equipment_User = new Equipment_User()
                            {
                                Equipment_User_ID = item.Equipment_User_ID,
                                Equipment_Description = item.Equipment_Description,
                                Equipment = item.Equipment,
                                Product = item.Product,
                                Capacity = item.Capacity,
                            },
                            Equipment = item.Equipment,
                        };

                        tempNewFail.Equipment_User_ID = item.Equipment_User_ID;

                        // check if there was a failure last fuel form and map to prev values
                        if (lastFuelForm != null && lastFuelForm.Fuel_Form_Equipment_Failure != null && lastFuelForm.Fuel_Form_Equipment_Failure.Count > 0)
                        {
                            Fuel_Form_Equipment_Failure tempPrevFail = editModel.fuelFormEquipFailure.SingleOrDefault(m => m.Equipment_User_ID == item.Equipment_User_ID);

                            if (tempPrevFail != null && tempPrevFail.Fix_Date == null)
                            {
                                tempNewFail.is_Equipment_Failure = tempPrevFail.is_Equipment_Failure;
                                tempNewFail.Failure_Date = tempPrevFail.Failure_Date;
                                tempNewFail.Fix_Date = tempPrevFail.Fix_Date;
                                tempNewFail.Remarks = tempPrevFail.Remarks;
                            }
                        }

                        editModel.fuelFormEquipFailure.Add(tempNewFail);
                    }
                    else
                    {
                        tempCurrentFail.Equipment_User = item;
                    }
                }

                editModel.fuelFormEquipFailure = editModel.fuelFormEquipFailure.ToList();

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                ViewData["AvailableEquipment"] = _equipmentRepository.getEquipmentForUser(currentUserGuid, false, true);
                ViewData["Vendors"] = _vendorRepository.getAllVendors(false);

                if (lastFuelForm != null)
                {
                    ViewData["UserProducts"] = _productRepository.getProductsForUser((Guid)lastFuelForm.UserId, true);
                    ViewData["PreviousFuelFormDetails"] = lastFuelForm != null ? serializer.Serialize(
                            lastFuelForm.Fuel_Form_Details.Select(f => new
                            {
                                id = f.Fuel_Form_ID,
                                detailsId = f.Fuel_Form_Details_Id,
                                productId = f.Product_ID,
                                equipUserId = f.Equipment_User_ID,
                                spareDrums = f.Spare_Drums,
                                tankDelivered = f.Tank_Delivered,
                                tankDispensed = f.Tank_Dispensed,
                                qtyOnHand = f.On_Hand
                            }).ToList()
                        ) : null;

                    ViewData["PreviousEquipmentFailuresJson"] = serializer.Serialize(
                        lastFuelForm.Fuel_Form_Equipment_Failure.Select(f => new
                        {
                            isFailure = f.is_Equipment_Failure,
                            failureDate = f.Failure_Date,
                            fixDate = f.Fix_Date,
                            remarks = f.Remarks,
                            equipmentUserId = f.Equipment_User_ID
                        }).ToList()
                    );

                    ViewData["PreviousExceptions"] = exceptions.Count() > 0 ? serializer.Serialize(
                        exceptions.Select(e => new DSNY.Core.Models.DSNYException()
                        {
                            exceptionId = e.ExceptionId,
                            tableId = (int)e.ExceptionTableKey,
                            tableName = e.ExceptionTableName,
                            categoryCode = e.DSNYExceptionCategory.ExceptionCode,
                            categoryId = e.ExceptionCategoryId,
                            exception = e.Exception
                        }).ToList()
                    ) : null;
                }

                return View(editModel);
            }
            else
            {
                return RedirectToAction("Create", "FuelForm");
            }
        }

        /// <summary>
        /// Creates a fuel form from the model and form collection
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrator, Field, Garage")]
        [HttpPost]
        public ActionResult Edit(FuelFormViewModel model, FormCollection collection)
        {
            Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;

            // initialize the fuel and details forms
            try
            {
                Fuel_Form updateFuelForm = null;
                List<Fuel_Form_Details_Delivery> updateFuelDetailsDelivery = null;

                // updates a new fuel form
                updateFuelForm = new Fuel_Form()
                {
                    Fuel_Form_ID = model.fuelForm.Fuel_Form_ID,
                    Remarks = model.fuelForm.Remarks,
                    Submission_Date = model.isAdmin ? model.fuelForm.Submission_Date : DateTime.Now,
                };

                // fuel delivery details
                foreach (var ffd in model.fuelFormDetails)
                {
                    updateFuelDetailsDelivery = model.fuelFormDetailsDelivery.Where(m => m.Fuel_Form_Details_Id == ffd.Fuel_Form_Details_Id).ToList();

                    foreach (var item in updateFuelDetailsDelivery)
                    {
                        if (item.Vendor_ID > -1 && item.Quantity != null && item.Invoice_Number != null)
                            ffd.Fuel_Form_Details_Delivery.Add(item);
                    }

                    updateFuelForm.Fuel_Form_Details.Add(ffd);
                }

                // equipment failures
                if (model.fuelFormEquipFailure != null && model.fuelFormEquipFailure.Count > 1)
                {
                    model.fuelFormEquipFailure
                        .ForEach(m =>
                        {
                            if (m.is_Equipment_Failure == true && m.Failure_Date != null)
                            {
                                updateFuelForm.Fuel_Form_Equipment_Failure.Add(m);
                            }
                        });
                }

                // add the form to the DB and grab its Id
                _fuelFormRepository.updateFuelForm(model.fuelForm.Fuel_Form_ID, updateFuelForm, (Guid)Membership.GetUser().ProviderUserKey);

                // add exceptions
                if (model.exceptions != null && model.exceptions.Count > 0)
                {
                    List<Fuel_Form_Details> tempDetails = updateFuelForm.Fuel_Form_Details.ToList();

                    model.exceptions.ForEach(m =>
                    {
                        if (tempDetails[m.categoryId] != null)
                        {
                            m.categoryId = tempDetails[m.categoryId].Fuel_Form_Details_Id;
                        }
                    });

                    _dsnyExceptionRepository.addExceptions(_dsnyExceptionRepository.convertExceptions(model.exceptions, currentUserGuid, "Fuel_Form_Details"));
                }

                if (model.isAdmin)
                {
                    TempData["saved"] = true;
                    return RedirectToAction("Search", "FuelForm", true);
                }
                else
                    return RedirectToAction("Field", "Dashboard");
            }
            catch (Exception ex)
            {
                if (model.isAdmin)
                    return RedirectToAction("Search", "FuelForm");
                else
                    return RedirectToAction("Field", "Dashboard");
            }

        }

        /// <summary>
        /// Displays fuel form
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrator, Field, Garage")]
        public ActionResult View(int id)
        {
            Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;
            int fuelFormId = 0;

            Message fuelFormMsg = _messageRepository.getMessage(id, currentUserGuid);

            if (fuelFormMsg != null && fuelFormMsg.Fuel_Form_ID != null)
                int.TryParse(fuelFormMsg.Fuel_Form_ID.ToString(), out fuelFormId);

            if (fuelFormId > 0)
            {
                Fuel_Form fuelForm = _fuelFormRepository.getFuelForm(fuelFormId);
                List<Fuel_Form_Details> fuelFormDetails = _fuelFormRepository.getFuelFormDetails(fuelFormId);
                List<Fuel_Form_Equipment_Failure> fuelEquipFailure = _fuelFormRepository.getFuelFormEquipFailure(fuelFormId);
                IUser submittedBy = _userRepository.getUser((Guid)fuelForm.UserId);

                FuelFormViewModel displayModel = new FuelFormViewModel()
                {
                    fuelForm = fuelForm,
                    fuelFormDetails = fuelFormDetails,
                    fuelFormEquipFailure = fuelEquipFailure,
                    submittedBy = submittedBy
                };

                ViewData["UserProducts"] = _productRepository.getProductsForUser((Guid)fuelForm.UserId, true);
                ViewData["UserEquipProducts"] = _equipmentRepository.getEquipmentForUser((Guid)fuelForm.UserId, false, true);
                _messageRepository.updateMessageTimestamp(id, (Guid)Membership.GetUser().ProviderUserKey);

                return View(displayModel);
            }
            else
            {
                return RedirectToAction("Admin", "Dashboard");
            }
        }

        /// <summary>
        /// Displays search for fuel forms.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        public ActionResult Search()
        {
            ViewData["users"] = _userRepository.getAllUsers(true);
            ViewData["saved"] = TempData["saved"];
            TempData["saved"] = false;

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            ViewData["PreviousFuelFormDetails"] = serializer.Serialize(new Fuel_Form());

            return View("~/Views/Admin/FuelForms/Search.aspx");
        }

        /// <summary>
        /// Displays search for fuel forms.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator")]
        [HttpPost]
        public ActionResult Search(FormCollection collection)
        {
            Guid userId = Guid.Empty;
            DateTime formDate = new DateTime();
            FuelFormViewModel editModel = new FuelFormViewModel();

            if (collection["userId"] != null)
                Guid.TryParse(collection["userId"], out userId);

            if (collection["formDate"] != null)
                formDate = DateTime.Parse(collection["formDate"]);

            if (userId != Guid.Empty)
            {
                int fuelFormId = 0;

                // checking for remarks from previous day fuel form
                Fuel_Form currentFuelForm = _fuelFormRepository.getFuelForm(userId, formDate);

                if (currentFuelForm != null)
                    int.TryParse(currentFuelForm.Fuel_Form_ID.ToString(), out fuelFormId);

                if (fuelFormId > 0)
                {
                    List<Fuel_Form_Details_Delivery> fuelFormDetailsDelivery = new List<Fuel_Form_Details_Delivery>();
                    currentFuelForm.Fuel_Form_Details.ToList().ForEach(ffd => fuelFormDetailsDelivery.Concat(ffd.Fuel_Form_Details_Delivery)); // fill the delivery view model object
                    List<DSNY.Data.DSNYException> exceptions = _dsnyExceptionRepository.getTableExceptions("Fuel_Form_Details", currentFuelForm.Fuel_Form_Details.Select(f => f.Fuel_Form_Details_Id).ToList());

                    editModel = new FuelFormViewModel()
                    {
                        isEdit = true,
                        fuelForm = currentFuelForm,
                        fuelFormDetails = currentFuelForm.Fuel_Form_Details.OrderBy(p => p.Product.Order_Num == null).ThenBy(p => p.Product.Order_Num).ToList(),
                        fuelFormDetailsDelivery = fuelFormDetailsDelivery,
                        fuelFormEquipFailure = currentFuelForm.Fuel_Form_Equipment_Failure.ToList(),
                        submittedBy = _userRepository.getUser((Guid)currentFuelForm.UserId)
                    };

                    Fuel_Form lastFuelForm = _fuelFormRepository.getLastFuelForm(userId, currentFuelForm.Fuel_Form_ID, 1);
                    List<Equipment_User> equipUser = _equipmentRepository.getEquipmentForUser(userId, false, false).ToList();
                    Fuel_Form_Equipment_Failure tempNewFail = new Fuel_Form_Equipment_Failure();

                    // add new equip failures, map to prev values if they exist
                    foreach (Equipment_User item in equipUser)
                    {
                        // link equipment to product
                        Fuel_Form_Details tempFfd = editModel.fuelFormDetails.SingleOrDefault(m => m.Product_ID == item.Product_ID && m.Equipment_User_ID == item.Equipment_User_ID);

                        if (tempFfd != null)
                        {
                            tempFfd.Equipment_User_ID = item.Equipment_User_ID;
                        }

                        Fuel_Form_Equipment_Failure tempCurrentFail = editModel.fuelFormEquipFailure.SingleOrDefault(m => m.Equipment_User_ID == item.Equipment_User_ID);

                        // failure doesn't exist
                        if (tempCurrentFail == null)
                        {
                            tempNewFail = new Fuel_Form_Equipment_Failure()
                            {
                                Fuel_Form_ID = editModel.fuelForm.Fuel_Form_ID,
                                Equipment_User_ID = item.Equipment_User_ID,
                                Equipment_User = new Equipment_User()
                                {
                                    Equipment_Description = item.Equipment_Description,
                                    Equipment = item.Equipment,
                                    Capacity = item.Capacity,
                                },
                                Equipment = item.Equipment,
                            };

                            tempNewFail.Equipment_User_ID = item.Equipment_User_ID;

                            // check if there was a failure last fuel form and map to prev values
                            if (lastFuelForm != null && lastFuelForm.Fuel_Form_Equipment_Failure != null && lastFuelForm.Fuel_Form_Equipment_Failure.Count > 0)
                            {
                                Fuel_Form_Equipment_Failure tempPrevFail = editModel.fuelFormEquipFailure.SingleOrDefault(m => m.Equipment_User_ID == item.Equipment_User_ID);

                                if (tempPrevFail != null)
                                {
                                    Fuel_Form_Equipment_Failure currentFail = editModel.fuelFormEquipFailure.SingleOrDefault(f => f.Equipment_Failure_ID == tempPrevFail.Equipment_Failure_ID);

                                    if (currentFail == null && tempPrevFail.Fix_Date == null)
                                    {
                                        tempNewFail.is_Equipment_Failure = tempPrevFail.is_Equipment_Failure;
                                        tempNewFail.Failure_Date = tempPrevFail.Failure_Date;
                                        tempNewFail.Fix_Date = tempPrevFail.Fix_Date;
                                        tempNewFail.Remarks = tempPrevFail.Remarks;
                                    }
                                }
                            }

                            editModel.fuelFormEquipFailure.Add(tempNewFail);
                        }
                        else
                        {
                            tempCurrentFail.Equipment_User = item;
                        }
                    }

                    // order the equipment failures
                    editModel.fuelFormEquipFailure = editModel.fuelFormEquipFailure
                        .OrderBy(m => m.Equipment_User.Product.Order_Num == null)
                        .ThenBy(m => m.Equipment_User.Product.Order_Num).ToList();

                    JavaScriptSerializer serializer = new JavaScriptSerializer();

                    ViewData["AvailableEquipment"] = _equipmentRepository.getEquipmentForUser(userId, false, true);
                    ViewData["Vendors"] = _vendorRepository.getAllVendors(false);

                    if (lastFuelForm != null)
                    {
                        ViewData["UserProducts"] = _productRepository.getProductsForUser((Guid)lastFuelForm.UserId, true);
                        ViewData["PreviousFuelFormDetails"] = lastFuelForm != null ? serializer.Serialize(
                                lastFuelForm.Fuel_Form_Details.Select(f => new
                                {
                                    id = f.Fuel_Form_ID,
                                    detailsId = f.Fuel_Form_Details_Id,
                                    productId = f.Product_ID,
                                    equipUserId = f.Equipment_User_ID,
                                    spareDrums = f.Spare_Drums,
                                    tankDelivered = f.Tank_Delivered,
                                    tankDispensed = f.Tank_Dispensed,
                                    qtyOnHand = f.On_Hand
                                }).ToList()
                            ) : null;

                        ViewData["PreviousExceptions"] = exceptions.Count() > 0 ? serializer.Serialize(
                            exceptions.Select(e => new DSNY.Core.Models.DSNYException()
                            {
                                exceptionId = e.ExceptionId,
                                tableId = (int)e.ExceptionTableKey,
                                tableName = e.ExceptionTableName,
                                categoryCode = e.DSNYExceptionCategory.ExceptionCode,
                                categoryId = e.ExceptionCategoryId,
                                exception = e.Exception
                            }).ToList()
                        ) : null;
                    }
                }
            }

            ViewData["users"] = _userRepository.getAllUsers(true);
            ViewData["userId"] = userId;
            ViewData["formDate"] = formDate;

            return View("~/Views/Admin/FuelForms/Search.aspx", editModel);
        }

        #endregion
    }
}