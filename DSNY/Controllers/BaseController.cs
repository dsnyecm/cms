﻿using System.Web.Mvc;

using DSNY.Common.Logger;
using DSNY.Common.Exception;

namespace DSNY.Controllers
{
    /// <summary>
    /// Base Controller for all controllers
    /// </summary>
    public class BaseController : Controller
    {
        #region Variables

        protected IExceptionHandler _exceptionHandler;
        protected ILogger _logger { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseController"/> class.
        /// </summary>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        public BaseController(ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
        {
            _exceptionHandler = IoCExceptionHandler;
            _logger = IoCLogger;
        }

        #endregion
        protected override void OnException(ExceptionContext context)
        {
            _exceptionHandler.HandleException(context.Exception);

            string controllerName = (string)context.RouteData.Values["controller"];
            string actionName = (string)context.RouteData.Values["action"];
            HandleErrorInfo model = new HandleErrorInfo(context.Exception, controllerName, actionName);

            context.ExceptionHandled = true;
            context.HttpContext.Response.Clear();
            context.HttpContext.Response.StatusCode = 500;

            // Certain versions of IIS will sometimes use their own error page when
            // they detect a server error. Setting this property indicates that we
            // want it to try to render ASP.NET MVC's error page instead.
            context.HttpContext.Response.TrySkipIisCustomErrors = true;

            if (context.HttpContext.IsCustomErrorEnabled)
            {
                this.View("Error", model).ExecuteResult(this.ControllerContext);
            }
        }
    }
}