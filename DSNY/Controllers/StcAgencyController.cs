using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DSNY.Core.Interfaces;
using DSNY.Common;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Core.Repository;
using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the dashboard area
    /// </summary>
    [HandleError]
    public class StcAgencyController : Controller
    {
        private ILogger _logger;
        private IExceptionHandler _exceptionHandler;
        private DSNYContext _context;
        private StcAgencyRepository _stcAgencies;
        private IVendorRepository _vendorRepository;

        public StcAgencyController(ILogger IoCLogger, IExceptionHandler IoCExceptionHandler, DSNYContext IoCContext, IVendorRepository IoCVendor)
        {
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
            _context = IoCContext;
            _stcAgencies = new StcAgencyRepository(_context);
            _vendorRepository = IoCVendor;
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Index()
        {
            var model = GetViewModel();
            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");
            return View(model);
        }

        private StcAgencyViewModel GetViewModel()
        {
            var model = new StcAgencyViewModel();
            model.StcAgencies = Mapper.Map<List<StcAgency>>(_stcAgencies.GetAll());
            model.BlankStcAgency = new StcAgency
            {
                Active = true,
                Deleted = false
            };
            if (!model.StcAgencies.Any()) { model.StcAgencies.Add(model.BlankStcAgency); }
            return model;
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Refresh()
        {
            return Json(GetViewModel());
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Save(List<StcAgency> values)
        {
            if (values == null || !values.Any()) { return Json("Nothing to save"); }
            var table = _context.DataContext.STC_Agency;
            foreach (var o in values)
            {
                if (o.Deleted)
                {
                    var order = table.Where(x => x.STC_Agency_id == o.Id).FirstOrDefault();
                    if (order != null) { _context.DataContext.DeleteObject(order); }
                }
                else if (o.Id.HasValue && (o.Id.Value > 0))
                {
                    var row = table.Where(x => x.STC_Agency_id == o.Id).FirstOrDefault();
                    if (row == null)
                    {
                        table.AddObject(NewRow(o));
                    }
                    else
                    {
                        Update(row, o);
                    }
                }
                else
                {
                    table.AddObject(NewRow(o));
                }
            }
            _context.DataContext.SaveChanges();

            return Json("Success");
        }

        private void Update(STC_Agency row, StcAgency o)
        {
            row.Agency_Name = o.Name;
            row.Agency_cd = o.Code;
            row.Agency_Address = o.Address;
            row.Agency_Email = o.Email;
            row.Agency_Phone = o.Phone;
            row.Is_Active = o.Active;
            row.Is_Billable = o.Billable;
        }

        private STC_Agency NewRow(StcAgency o)
        {
            return new STC_Agency
            {
                Agency_Name = o.Name,
                Agency_cd = o.Code,
                Agency_Address = o.Address,
                Agency_Email = o.Email,
                Agency_Phone = o.Phone,
                Is_Active = o.Active,
                Is_Billable = o.Billable
            };
        }
    }
}
