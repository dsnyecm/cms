﻿using System;
using System.Web.Mvc;

using DSNY.Core.Interfaces;
using DSNY.Common.Logger;
using DSNY.Common.Exception;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the administration area
    /// </summary>    
    public class AdminController : BaseController
    {
        #region Variables

        private IPurchaseOrderRepository _purchaseOrderRepository;

        private int pageSize = MvcApplication.pageSize;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminController"/> class.
        /// </summary>
        /// <param name="IoCMessageRepo">The inversion of control  message repo.</param>
        /// <param name="IoCUserRepo">The inversion of control  user repo.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public AdminController(IPurchaseOrderRepository IoCPurchaseOrderRepo, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _purchaseOrderRepository = IoCPurchaseOrderRepo;
        }

        #endregion

        #region Actions

        /// <summary>
        /// Action for the admin index, only system administrators may access
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "System Administrator, Reports")]
        public ActionResult Index()
        {
            ViewData["purchaseOrder"] = _purchaseOrderRepository.getLastPurchaseOrder();

            return View();
        }

        #endregion
    }
}