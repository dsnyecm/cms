using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using DSNY.Core.Interfaces;
using DSNY.Common;
using DSNY.Common.Logger;
using DSNY.Common.Exception;
using DSNY.Core.Repository;
using DSNY.Data;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for the dashboard area
    /// </summary>
    [HandleError]
    public class StcTransferController : Controller
    {
        private IUserRepository _userRepository;
        private ILogger _logger;
        private IExceptionHandler _exceptionHandler;
        private DSNYContext _context;
        private StcTransferRepository _transfers;
        private StcProductRepository _products;
        private TruckTypeRepository _truckTypes;
        private SiteOnHandRepository _siteOnHandRepository;

        public StcTransferController(IUserRepository IoCUserRepo, ILogger IoCLogger, IExceptionHandler IoCExceptionHandler, DSNYContext IoCContext)
        {
            _userRepository = IoCUserRepo;
            _logger = IoCLogger;
            _exceptionHandler = IoCExceptionHandler;
            _context = IoCContext;
            _transfers = new StcTransferRepository(_context);
            _products = new StcProductRepository(_context);
            _truckTypes = new TruckTypeRepository(_context);
            _siteOnHandRepository = new SiteOnHandRepository(_context);
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Index()
        {
            var isHQ = _userRepository.IsUserInRole((Guid)Membership.GetUser().ProviderUserKey, "STC HQ");
            var model = GetViewModel();
            model.Finalizable = isHQ;
            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");
            return View(model);
        }

        private StcTransferViewModel GetViewModel()
        {
            return GetViewModel(null);
        }

        private StcTransferViewModel GetViewModel(StcTransferViewModel viewModel)
        {
            var model = new StcTransferViewModel();
            var products = _products.GetAll();
            model.Products = Mapper.Map<List<StcProduct>>(products);
            model.Sites = new Dictionary<string, List<Dropdown>>();
            model.Sites.Add("0", Mapper.Map<List<Dropdown>>(_userRepository.getStcUsersWithActiveSiteOnHand((Guid)Membership.GetUser().ProviderUserKey).Select(x => new { id = x.userId, description = x.userName, visible = true }).ToList()));
            foreach (var product in products)
            {
                model.Sites.Add(product.STC_Product_id.ToString(), Mapper.Map<List<Dropdown>>(_userRepository.getStcUsersWithActiveSiteOnHand((Guid)Membership.GetUser().ProviderUserKey, product.STC_Product_id).Select(x => new { id = x.userId, description = x.userName, visible = true }).ToList()));
            }
            model.TruckTypes = Mapper.Map<List<TruckType>>(_truckTypes.GetAll().ToList());
            if (model.Products.Any() && model.Sites.Any() && model.TruckTypes.Any())
            {
                model.BlankStcTransfer = new StcTransfer
                {
                    Id = 0,
                    TransferDate = DateTime.Now.ToShortDateString(),
                    Finalized = false,
                    Edit = true,
                    Deleted = false
                };
                var transfer = _transfers.GetAll().AsQueryable();
                if (viewModel != null)
                {
                    var startDate = DateTime.MinValue;
                    var endDate = DateTime.MaxValue;
                    if (!string.IsNullOrEmpty(viewModel.StartDate))
                    {
                        if (DateTime.TryParse(viewModel.StartDate, out startDate))
                        {
                            transfer = transfer.Where(x => x.STC_Transfer_Date >= startDate);
                        }
                    }
                    if (!string.IsNullOrEmpty(viewModel.EndDate))
                    {
                        if (DateTime.TryParse(viewModel.EndDate, out endDate))
                        {
                            endDate = endDate.AddDays(1);
                            transfer = transfer.Where(x => x.STC_Transfer_Date <= endDate);
                        }
                    }
                    if (!string.IsNullOrEmpty(viewModel.selectedSite))
                    {
                        Guid site = new Guid(viewModel.selectedSite);
                        transfer = transfer.Where(x => x.From_Userid == site || x.To_Userid == site);
                    }
                    if (!viewModel.Finalized)
                    {
                        transfer = transfer.Where(x => x.is_finalize == false);
                    }
                }
                else if (viewModel == null)
                {
                    var startDate = DateTime.Today;
                    model.StartDate = DateTime.Today.ToShortDateString();
                    transfer = transfer.Where(x => x.STC_Transfer_Date >= startDate && x.is_finalize != true);
                }
                model.StcTransfers = Mapper.Map<List<StcTransfer>>(transfer.ToList());
            }
            return model;
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult ChangeFilter(StcTransferViewModel viewModel)
        {
            return Json(GetViewModel(viewModel));
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Finalize(StcTransfer value)
        {
            if (value == null) { return Json("Nothing to save"); }
            var table = _context.DataContext.STC_Transfer;
            if (value.Deleted)
            {
                var order = table.Where(x => x.STC_Transfer_Id == value.Id).FirstOrDefault();
                if (order != null) { _context.DataContext.DeleteObject(order); }
            }
            else
            {
                if (value.Id.HasValue && (value.Id.Value > 0))
                {
                    var row = table.Where(x => x.STC_Transfer_Id == value.Id).FirstOrDefault();
                    if (row == null)
                    {
                        table.AddObject(NewRow(value));
                    }
                    else
                    {
                        Update(row, value);
                    }
                }
                else
                {
                    table.AddObject(NewRow(value));
                }
            }
            _context.DataContext.SaveChanges();

            return Json("Success");
        }

        /// <summary>
        /// Displays the needs request.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "STC Borough, STC HQ")]
        public ActionResult Save(List<StcTransfer> values)
        {
            if (values == null || !values.Any()) { return Json("Nothing to save"); }
            var table = _context.DataContext.STC_Transfer;
            foreach (var o in values)
            {
                if (o.Deleted)
                {
                    var order = table.Where(x => x.STC_Transfer_Id == o.Id).FirstOrDefault();
                    if (order != null) { _context.DataContext.DeleteObject(order); }
                } else { 
                    if (o.Id.HasValue && (o.Id.Value > 0))
                    {
                        var row = table.Where(x => x.STC_Transfer_Id == o.Id).FirstOrDefault();
                        if (row == null)
                        {
                            table.AddObject(NewRow(o));
                        }
                        else
                        {
                            Update(row, o);
                        }
                    }
                    else
                    {
                        table.AddObject(NewRow(o));
                    }
                }
            }
            _context.DataContext.SaveChanges();

            return Json("Success");
        }

        private void Update(STC_Transfer row, StcTransfer o)
        {
            if (o.Finalized)
            {
                _siteOnHandRepository.AddOnHand(new Guid(o.ToUserId), o.ProductId, decimal.Parse(o.Qty));
                _siteOnHandRepository.SubtractOnHand(new Guid(o.FromUserId), o.ProductId, decimal.Parse(o.Qty));
            }

            DateTime? transferdate = null;
            DateTime tempDate;
            if (DateTime.TryParse(o.TransferDate + " " + o.TransferTime, out tempDate)) { transferdate = tempDate; }
            row.STC_Product_id = o.ProductId;
            row.From_Userid = new Guid(o.FromUserId);
            row.To_Userid = new Guid(o.ToUserId);
            row.STC_Truck_Type_id = o.TruckTypeId;
            row.Loads = o.Loads.Value;
            row.Qty = decimal.Parse(o.Qty);
            row.STC_Transfer_Date = transferdate;
            row.Snow_Super_Name = o.BoroSuperName;
            row.Snow_Super_Badge = o.BoroSuperBadge;
            row.San_Super_Name = o.SanSuperName;
            row.San_Super_Badge = o.SanSuperBadge;
            row.is_finalize = o.Finalized;
        }

        private STC_Transfer NewRow(StcTransfer o)
        {
            if (o.Finalized)
            {
                _siteOnHandRepository.AddOnHand(new Guid(o.ToUserId), o.ProductId, decimal.Parse(o.Qty));
                _siteOnHandRepository.SubtractOnHand(new Guid(o.FromUserId), o.ProductId, decimal.Parse(o.Qty));
            }

            DateTime? tranferdate = null;
            DateTime tempDate;
            if (DateTime.TryParse(o.TransferDate + " " + o.TransferTime, out tempDate)) { tranferdate = tempDate; }
            return new STC_Transfer
            {
                STC_Product_id = o.ProductId,
                From_Userid = new Guid(o.FromUserId),
                To_Userid = new Guid(o.ToUserId),
                STC_Truck_Type_id = o.TruckTypeId,
                Loads = o.Loads.Value,
                Qty = decimal.Parse(o.Qty),
                STC_Transfer_Date = tranferdate,
                Snow_Super_Name = o.BoroSuperName,
                Snow_Super_Badge = o.BoroSuperBadge,
                San_Super_Name = o.SanSuperName,
                San_Super_Badge = o.SanSuperBadge,
                is_finalize = o.Finalized
            };
        }
    }
}
