﻿using System.Web.Mvc;

using DSNY.Common.Logger;
using DSNY.Common.Exception;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for index pages
    /// </summary>
    
    public class GarageSuppliesController : BaseController
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GarageSuppliesController"/> class.
        /// </summary>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public GarageSuppliesController(ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
            : base(IoCLogger, IoCExceptionHandler)
        {
        }

        #endregion

        #region Actions

        /// <summary>
        /// When hitting the home, redirect to appropriate homepage for user group
        /// If we have a user in multiple
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "GS Warehouse, GS Borough, GS District, GS HQ")]
        public ActionResult Index()
        {
            return View();
        }

        #endregion
    }
}