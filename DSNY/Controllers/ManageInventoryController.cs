﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;
using System.Web.Security;

using AutoMapper;

using DSNY.Common.Exception;
using DSNY.Common.Logger;
using DSNY.Common.Utilities;
using DSNY.Core.Interfaces;
using DSNY.Data;
using DSNY.ViewModels;

namespace DSNY.Controllers
{
    /// <summary>
    /// Controller for index pages
    /// </summary>
    public class ManageInventoryController : BaseController
    {
        #region Variables

        private DSNYContext _dataProvider = null;
        private IUserRepository _userRepository;
        private IRoleRepository _roleRepository;
        private IMessageRepository _messageRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ManageInventoryController"/> class.
        /// </summary>
        /// <param name="IoCDataProvider">The inversion of control data prodiver.</param>
        /// <param name="IoCUserRepo">The inversion of control user repo.</param>
        /// <param name="IoCRoleRepo">The inversion of control role repo.</param>
        /// <param name="IoCLogger">The inversion of control logger implementation.</param>
        /// <param name="IoCExceptionHandler">The inversion of control exception handler implementation.</param>
        public ManageInventoryController(DSNYContext IoCDataProvider, IUserRepository IoCUserRepo, IRoleRepository IoCRoleRepo, IMessageRepository IoCMessageRepo,
            ILogger IoCLogger, IExceptionHandler IoCExceptionHandler)
            : base(IoCLogger, IoCExceptionHandler)
        {
            _dataProvider = IoCDataProvider;
            _userRepository = IoCUserRepo;
            _roleRepository = IoCRoleRepo;
            _messageRepository = IoCMessageRepo;
        }

        #endregion

        #region Actions

        [Authorize(Roles = "GS Warehouse, GS HQ")]
        public ActionResult Index()
        {
            return ReassignPowerTools();
        }

        #region Reassign Power Tools

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "GS Borough, GS Warehouse, GS HQ")]
        public ActionResult ReassignPowerTools()
        {
            ToolMaintenanceViewModel model = new ToolMaintenanceViewModel()
            {
                users = _userRepository.getRolesUsers(new List<string>() { "gs borough", "gs district" }).OrderBy(u => u.userName).ToList()
            };

            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");

            return View("~/Views/GarageSupplies/ReassignPowerTools.aspx", model);
        }

        [HttpPost]
        [Authorize(Roles = "GS Borough, GS Warehouse, GS HQ")]
        public ActionResult ReassignPowerTools(List<PowerToolInventoryUpdateModel> powerToolUpdates)
        {
            if (powerToolUpdates == null) { return Json("Nothing to save"); }

            Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;

            try
            {
                var table = _dataProvider.DataContext.Power_Tool_Inventory;

                // loop over recieved Tool_Types and either update or add
                powerToolUpdates.ForEach(pt => {
                    // try to find tool type
                    if (pt.Power_Tool_Inventory_Id > 0)
                    {
                        var row = table.Where(x => x.Power_Tool_Inventory_Id == pt.Power_Tool_Inventory_Id).FirstOrDefault();   

                        if (row != null)
                        {
                            row.UserId = pt.Userid;

                            // add to workflow tracking table
                            _dataProvider.DataContext.Workflow_Tracking.AddObject(new Workflow_Tracking()
                            {
                                Workflow_Action_id = _dataProvider.DataContext.Workflow_Action.SingleOrDefault(x => x.Workflow_cd == "REASGN").Workflow_Action_id,
                                Userid = currentUserGuid,
                                Comment = pt.Comment,
                                Workflow_Datetime = DateTime.Now,
                                Table_Association = "Power_Tool_Inventory",
                                Table_Id = row.Power_Tool_Inventory_Id
                            });
                        }
                    }
                });

                _dataProvider.DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }

            return Json("Success");
        }

        [HttpGet]
        [Authorize(Roles = "GS Borough, GS Warehouse, GS HQ")]
        public ActionResult GetLocationPowerTools(Guid userId)
        {
            var powerTools = Mapper.Map<List<PowerToolInventoryViewModel>>
                (_dataProvider.DataContext.Power_Tool_Inventory.Include("Tool_Type").Include("Make").Include("Model").Where(t => t.UserId == userId).ToList());

            return Json(powerTools, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Inventory Requests

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "GS Warehouse, GS HQ")]
        public ActionResult CreateInventoryRequest()
        {
            return View("~/Views/GarageSupplies/CreateInventoryRequest.aspx");
        }

        /// <summary>
        /// When hitting the home, redirect to appropriate homepage for user group
        /// If we have a user in multiple
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "GS Warehouse, GS HQ")]
        public ActionResult SaveInventoryRequest(InventoryRequestSaveModel inventoryRequest)
        {
            if (inventoryRequest == null) { return Json("Nothing to save"); }

            try
            {
                Guid currentUserGuid = (Guid)Membership.GetUser().ProviderUserKey;
                Workflow_Action action = _dataProvider.DataContext.Workflow_Action.SingleOrDefault(a => a.Workflow_cd == "IR");

                // create tool inventory
                Tool_Inventory toolInventory = new Tool_Inventory()
                {
                    Inventory_Date = inventoryRequest.inventoryDate,
                    Inventory_Instruction = inventoryRequest.additionalRequest
                };

                _dataProvider.DataContext.Tool_Inventory.AddObject(toolInventory);
                _dataProvider.DataContext.SaveChanges();

                // add all power tool inventory requests
                List<Power_Tool_Inventory> powerTools = _dataProvider.DataContext.Power_Tool_Inventory.Where(t => t.is_Active == true && t.Tool_Type.is_Active == true).ToList();
                List<Tool_Inventory_PT> addedPowerTools = new List<Tool_Inventory_PT>();

                powerTools.ForEach(t =>
                {
                    Tool_Inventory_PT tempTool = new Tool_Inventory_PT()
                    {
                        Power_Tool_Inventory_id = t.Power_Tool_Inventory_Id,
                        Tool_Inventory_id = toolInventory.Tool_Inventory_id,
                        is_Active = false,
                        is_Down = false,
                        is_Stolen = false,
                        is_Dispose = false
                    };

                    _dataProvider.DataContext.Tool_Inventory_PT.AddObject(tempTool);
                    addedPowerTools.Add(tempTool);
                });

                // add all standard tool inventory requests, excludes warehouse and borough tool_user ids
                List<Guid> excludedUsers = _userRepository.getRolesUsers(new List<string>() { "gs warehouse", "gs borough" }).Select(r => r.userId).ToList();
                List<Standard_Tools_Inventory> standardTools = _dataProvider.DataContext.Standard_Tools_Inventory
                    .Where(t => t.Tool_User.is_Active == true && t.Tool_User.Tool_Type.is_Active == true
                        && !excludedUsers.Contains(t.Tool_User.Userid.Value)).ToList();
                List<Tool_Inventory_Std> addedStandardTools = new List<Tool_Inventory_Std>();

                standardTools.ForEach(t =>
                {
                    Tool_Inventory_Std tempTool = new Tool_Inventory_Std()
                    {
                        Tool_Inventory = toolInventory,
                        Tool_User_Id = t.Tool_User_id,
                        On_Hand_Qty = t.Current_On_Hand
                    };

                    _dataProvider.DataContext.Tool_Inventory_Std.AddObject(tempTool);
                    addedStandardTools.Add(tempTool);
                });

                // create workflow tracking records
                if (action != null)
                {
                    _dataProvider.DataContext.SaveChanges();

                    addedPowerTools.ForEach(t =>
                    {
                        _dataProvider.DataContext.Workflow_Tracking.AddObject(new Workflow_Tracking()
                        {
                            Workflow_Action_id = action.Workflow_Action_id,
                            Workflow_Datetime = DateTime.Now,
                            Table_Association = "Tool_Inventory_PT",
                            Table_Id = t.Tool_Inventory_PT_id,
                            Userid = currentUserGuid,
                            Comment = inventoryRequest.additionalRequest
                        });
                    });

                    addedStandardTools.ForEach(t =>
                    {
                        _dataProvider.DataContext.Workflow_Tracking.AddObject(new Workflow_Tracking()
                        {
                            Workflow_Action_id = action.Workflow_Action_id,
                            Workflow_Datetime = DateTime.Now,
                            Table_Association = "Tool_Inventory_Std",
                            Table_Id = t.Tool_Inventory_Std_Id,
                            Userid = currentUserGuid,
                            Comment = inventoryRequest.additionalRequest
                        });
                    });
                }

                _dataProvider.DataContext.SaveChanges();

                // send out messages/emails to all GS users
                List<IUser> users = _userRepository.getRolesUsers(new List<string>(new string[] { "GS District", "GS Borough", "GS Warehouse", "GS HQ" }));
                String messageSubject = String.Format("Inventory Submission Requested on {0}", inventoryRequest.inventoryDate.Date.ToShortDateString());
                String messageBody = "All boroughs and districts must submit thier inventory in the next week.\nThis is done by going to CMS, clicking on the Garage Supplies and selecting the Inventory Submission.";

                // check if we have an additional request
                if (!String.IsNullOrEmpty(inventoryRequest.additionalRequest))
                {
                    messageBody += String.Format("\n\n\nAdditional Instructions/Comments:\n\n{0}", inventoryRequest.additionalRequest);
                }

                // send messages to users
                _messageRepository.addMessageAndSendToUsers(new Message()
                {
                    Sent_By_UserId = currentUserGuid,
                    Message_Date = DateTime.Now,
                    Message_Subject = messageSubject,
                    Message_Text = messageBody,
                    is_Fuel_Form = false
                }, users.Select(u => u.userId).ToList());

                bool sendEmails = false;
                bool.TryParse(ConfigurationManager.AppSettings["SendEmails"], out sendEmails);

                // send emails to users
                if (sendEmails)
                {
                    MailMessage mm = new MailMessage();

                    // setup message
                    mm.Subject = messageSubject;
                    mm.Body = messageBody;

                    users.ForEach(user =>
                    {
                        if (!String.IsNullOrEmpty(user.email))
                        {
                            mm.To.Add(new MailAddress(user.email, string.IsNullOrEmpty(user.fullName) ? string.Empty : user.fullName));
                        }
                    });

                    EmailUtilities.SendEmail(mm);
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }

            return Json("Success");
        }

        #endregion

        #region Inventory Submission

        [Authorize(Roles = "GS Borough, GS District, GS Warehouse, GS HQ")]
        public ActionResult InventorySubmission()
        {
            MembershipUser currentUser = Membership.GetUser();
            Guid currentUserGuid = (Guid)currentUser.ProviderUserKey;
            List<IRole> roles = _roleRepository.getRolesForUser(currentUser.UserName).ToList();
            List<string> rolesString = roles.Select(r => r.roleName).ToList();
            List<aspnet_Roles> aspnetRoles = _roleRepository.getAllAspNetRoles();
            InventorySubmissionViewModel model = new InventorySubmissionViewModel()
            {
                currentUserId = currentUserGuid,
                roles = Mapper.Map<List<RoleViewModel>>(aspnetRoles.Where(r => roles.SingleOrDefault(ir => ir.roleName == r.RoleName) != null)),
                workflowActionFlows = Mapper.Map<List<WorkflowActionFlowViewModel>>(_dataProvider.DataContext.Workflow_Action_flow.ToList()),
                workflowActionRoles = Mapper.Map<List<WorkflowActionRoleViewModel>>(_dataProvider.DataContext.Workflow_Action_Role.ToList())
            };

            List<int> maxWorkflowIds;
            List<Tool_Inventory_Std> standardToolsInventory = new List<Tool_Inventory_Std>();
            List<Tool_Inventory_PT> powerToolsInventory = new List<Tool_Inventory_PT>();
            List<Tool_Inventory_WH> warehouseInventory = new List<Tool_Inventory_WH>();
            List<Workflow_Tracking> currentWorkflows;
            List<Workflow_Tracking> nextWorkflows;

            // get latest inventory request
            Tool_Inventory inventory = _dataProvider.DataContext.Tool_Inventory.OrderByDescending(i => i.Tool_Inventory_id).FirstOrDefault();

            if (inventory != null)
            {
                // only for Warehouse
                if (roles.SingleOrDefault(r => r.roleName == "GS Warehouse") != null)
                {
                    warehouseInventory = _dataProvider.DataContext.Tool_Inventory_WH.Where(i => i.Tool_Inventory_id == inventory.Tool_Inventory_id && i.Approved_Qty == null).ToList();
                    List<int> warehouseInventoryIds = warehouseInventory.Select(i => i.Tool_Inventory_WH_id).ToList();

                    //model.tools = Mapper.Map<List<StandardToolsInventoryViewModel>>
                    //    (_dataProvider.DataContext.Standard_Tools_Inventory.Where(t => t.Tool_User.Userid == currentUserGuid && t.Tool_User.is_Active == true).ToList());

                    // get all workflows for current user that aren't complete
                    currentWorkflows = _dataProvider.DataContext.Workflow_Tracking
                        .Where(w => w.Table_Association == "Tool_Inventory_WH" && warehouseInventoryIds.Contains(w.Table_Id.Value)).ToList();
                }
                else
                {
                    // get all inventory tools related to inventory request for user
                    standardToolsInventory = _dataProvider.DataContext.Tool_Inventory_Std
                        .Where(i => i.Tool_Inventory_id == inventory.Tool_Inventory_id && i.Tool_User.Userid == currentUserGuid && i.Tool_User.is_Active == true).ToList();
                    powerToolsInventory = _dataProvider.DataContext.Tool_Inventory_PT
                        .Where(i => i.Tool_Inventory_id == inventory.Tool_Inventory_id && i.Power_Tool_Inventory.UserId == currentUserGuid && i.Power_Tool_Inventory.is_Active == true).ToList();

                    List<int> standardToolsInventoryIds = standardToolsInventory.Select(i => i.Tool_Inventory_Std_Id).ToList();
                    List<int> powertoolsInventoryIds = powerToolsInventory.Select(i => i.Tool_Inventory_PT_id).ToList();

                    // get latest workflow actions for all inventory tools related to inventory request
                    maxWorkflowIds = _dataProvider.DataContext.Workflow_Tracking
                        .Where(w => w.Table_Association == "Tool_Inventory_Std" && standardToolsInventoryIds.Contains(w.Table_Id.Value))
                        .GroupBy(w => new { table = w.Table_Association, id = w.Table_Id })
                        .Select(w => w.Max(row => row.Workflow_Tracking_id))
                        .ToList();

                    maxWorkflowIds = maxWorkflowIds.Concat(_dataProvider.DataContext.Workflow_Tracking
                        .Where(w => w.Table_Association == "Tool_Inventory_PT" && powertoolsInventoryIds.Contains(w.Table_Id.Value))
                        .GroupBy(w => new { table = w.Table_Association, id = w.Table_Id })
                        .Select(w => w.Min(row => row.Workflow_Tracking_id))
                        .ToList()).ToList();

                    model.tools = Mapper.Map<List<StandardToolsInventoryViewModel>>
                        (_dataProvider.DataContext.Standard_Tools_Inventory.Where(t => t.Tool_User.Userid == currentUserGuid && t.Tool_User.is_Active == true).ToList());

                    // get all workflows for current user that aren't complete
                    currentWorkflows = _dataProvider.DataContext.Workflow_Tracking
                        .Where(w => maxWorkflowIds.Contains(w.Workflow_Tracking_id) && w.Workflow_Action.is_End_Workflow == false).ToList();
                }

                nextWorkflows = currentWorkflows.Where(w =>
                {
                    List<Workflow_Action_flow> flows = w.Workflow_Action.Workflow_Action_flow.ToList();

                    foreach (Workflow_Action_flow flow in flows)
                    {
                        foreach (Workflow_Action_Role role in flow.Workflow_Action1.Workflow_Action_Role)
                        {
                            if (flow.Workflow_Action1.Workflow_cd != "CNCL" && rolesString.Contains(role.aspnet_Roles.RoleName))
                            {
                                return true;
                            }
                        }
                    }

                    return w.Table_Association == "Tool_Inventory_PT"; ;
                }).ToList();

                // fill in model to return to view
                model.standardInventory = Mapper.Map<List<ToolInventoryStandardViewModel>>(standardToolsInventory);
                model.powertools = Mapper.Map<List<PowerToolInventoryViewModel>>
                        (_dataProvider.DataContext.Power_Tool_Inventory.Where(w => w.UserId == currentUserGuid).ToList());
                model.powertoolInventory = Mapper.Map<List<ToolInventoryPowertoolViewModel>>(powerToolsInventory);
                model.warehouseInventory = Mapper.Map<List<ToolInventoryWarehouseViewModel>>(warehouseInventory);
                //model.workflows = Mapper.Map<List<WorkflowTrackingViewModel>>(workflows);
                model.workflows = Mapper.Map<List<WorkflowTrackingViewModel>>(nextWorkflows);
                model.users = _userRepository.getRolesUsers(new List<string>() { "gs borough", "gs district", "gs warehouse", "gs hq" }).OrderBy(u => u.userName).ToList();
                model.commandUsers = _userRepository.getUserCommandChain(currentUserGuid).Select(u => u.userId).ToList();
            }

            model.Json = Newtonsoft.Json.JsonConvert.SerializeObject(model).Replace("'", "\'");

            return View("~/Views/GarageSupplies/InventorySubmission.aspx", model);
        }

        [HttpGet]
        [Authorize(Roles = "GS Borough, GS District, GS Warehouse, GS HQ")]
        public ActionResult GetWorkflowHistory(string tableName, int tableId)
        {
            var workflowHistory = Mapper.Map<List<WorkflowTrackingViewModel>>
                (_dataProvider.DataContext.Workflow_Tracking.Where(w => w.Table_Association == tableName && w.Table_Id == tableId).ToList());

            return Content(Newtonsoft.Json.JsonConvert.SerializeObject(workflowHistory).Replace("'", "\'"), "application/json");
        }

        [HttpGet]
        [Authorize(Roles = "GS Borough, GS District, GS Warehouse, GS HQ")]
        public ActionResult GetBoroughDistrictWorkflows(Guid userId)
        {
            MembershipUser currentUser = Membership.GetUser();
            Guid currentUserGuid = (Guid)currentUser.ProviderUserKey;
            List<string> rolesString = _roleRepository.getRolesForUser(currentUser.UserName).Select(r => r.roleName).ToList();

            // get latest inventory request
            Tool_Inventory inventory = _dataProvider.DataContext.Tool_Inventory.OrderByDescending(i => i.Tool_Inventory_id).FirstOrDefault();

            List<Tool_Inventory_Std> standardToolsInventory = _dataProvider.DataContext.Tool_Inventory_Std
                .Where(i => i.Tool_Inventory_id == inventory.Tool_Inventory_id && i.Tool_User.Userid == userId && i.Tool_User.is_Active == true).ToList();
            List<Tool_Inventory_PT> powerToolsInventory = _dataProvider.DataContext.Tool_Inventory_PT
                .Where(i => i.Tool_Inventory_id == inventory.Tool_Inventory_id && i.Power_Tool_Inventory.UserId == userId && i.Power_Tool_Inventory.is_Active == true).ToList();
            List<int> standardToolsInventoryIds = standardToolsInventory.Select(i => i.Tool_Inventory_Std_Id).ToList();
            List<int> powertoolsInventoryIds = powerToolsInventory.Select(i => i.Tool_Inventory_PT_id).ToList();

            // get the last workflow action for all standard/power tools from above
            List<int> maxWorkflowIds = _dataProvider.DataContext.Workflow_Tracking
                .Where(w => w.Table_Association == "Tool_Inventory_Std" && standardToolsInventoryIds.Contains(w.Table_Id.Value))
                .GroupBy(w => new { table = w.Table_Association, id = w.Table_Id })
                .Select(w => w.Max(row => row.Workflow_Tracking_id))
                .ToList();

            maxWorkflowIds = maxWorkflowIds.Concat(_dataProvider.DataContext.Workflow_Tracking
                .Where(w => w.Table_Association == "Tool_Inventory_PT" && powertoolsInventoryIds.Contains(w.Table_Id.Value))
                .GroupBy(w => new { table = w.Table_Association, id = w.Table_Id })
                .Select(w => w.Min(row => row.Workflow_Tracking_id))
                .ToList()).ToList();

            // get all workflows in maxWorkflowIdsthat aren't complete
            List<Workflow_Tracking> currentWorkflows = _dataProvider.DataContext.Workflow_Tracking
                .Where(w => maxWorkflowIds.Contains(w.Workflow_Tracking_id) && w.Workflow_Action.is_End_Workflow == false).ToList();

            List<Workflow_Tracking> nextWorkflows = currentWorkflows.Where(w =>
            {
                List<Workflow_Action_flow> flows = w.Workflow_Action.Workflow_Action_flow.ToList();

                foreach (Workflow_Action_flow flow in flows)
                {
                    foreach (Workflow_Action_Role role in flow.Workflow_Action1.Workflow_Action_Role)
                    {
                        if (flow.Workflow_Action1.Workflow_cd != "CNCL" && rolesString.Contains(role.aspnet_Roles.RoleName))
                        {
                            return true;
                        }
                    }
                }

                return w.Table_Association == "Tool_Inventory_PT";
            }).ToList();

            InventorySubmissionViewModel model = new InventorySubmissionViewModel()
            {
                tools = Mapper.Map<List<StandardToolsInventoryViewModel>>
                    (_dataProvider.DataContext.Standard_Tools_Inventory.Where(t => t.Tool_User.Userid == userId).ToList()),
                standardInventory = Mapper.Map<List<ToolInventoryStandardViewModel>>(standardToolsInventory),
                powertools = Mapper.Map<List<PowerToolInventoryViewModel>>
                    (_dataProvider.DataContext.Power_Tool_Inventory.Where(t => t.UserId == userId).ToList()),
                powertoolInventory = Mapper.Map<List<ToolInventoryPowertoolViewModel>>(powerToolsInventory),
                workflows = Mapper.Map<List<WorkflowTrackingViewModel>>(nextWorkflows)
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize(Roles = "GS Borough, GS District, GS Warehouse, GS HQ")]
        public ActionResult SaveInventorySubmission(InventorySubmissionSaveModel inventorySubmission)
        {
            if (inventorySubmission == null) { return Json("Nothing to save"); }

            try
            {
                MembershipUser currentUser = Membership.GetUser();
                Guid currentUserGuid = (Guid)currentUser.ProviderUserKey;
                Tool_Inventory inventory = _dataProvider.DataContext.Tool_Inventory.OrderByDescending(i => i.Tool_Inventory_id).FirstOrDefault();
                List<IUser> newWorkflowUsers = new List<IUser>();

                bool sendEmails = false;
                bool.TryParse(ConfigurationManager.AppSettings["SendEmails"], out sendEmails);

                if (inventorySubmission.standardInventory != null && inventorySubmission.standardInventory.Count() > 0)
                {
                    inventorySubmission.standardInventory.ForEach(x =>
                    {
                        if (x.Tool_Inventory_Std_Id > 0)
                        {
                            var row = _dataProvider.DataContext.Tool_Inventory_Std.Where(y => y.Tool_Inventory_Std_Id == x.Tool_Inventory_Std_Id).FirstOrDefault();

                            if (row != null)
                            {
                                row.Boro_Approved_Qty = x.Boro_Approved_Qty;
                                row.Boro_Shipped_Qty = x.Boro_Shipped_Qty;
                                row.Damaged_Qty = x.Damaged_Qty;
                                row.Issue_Qty = x.Issue_Qty;
                                row.Lost_Qty = x.Lost_Qty;
                                row.On_Hand_Qty = x.On_Hand_Qty;
                            }
                        }
                    });
                }

                if (inventorySubmission.powertoolInventory != null && inventorySubmission.powertoolInventory.Count() > 0)
                {
                    List<string> rolesString = _roleRepository.getRolesForUser(currentUser.UserName).Select(r => r.roleName).ToList();

                    inventorySubmission.powertoolInventory.ForEach(x =>
                    {
                        if (x.Power_Tool_Inventory_id > 0)
                        {
                            var row = _dataProvider.DataContext.Tool_Inventory_PT.Where(y => y.Tool_Inventory_PT_id == x.Tool_Inventory_PT_id).FirstOrDefault();

                            if (row != null)
                            {
                                // check if any of the flags have been changed and record in workflow tracking
                                if ((row.is_Active.Value != x.is_Active) || (row.is_Down.Value != x.is_Down) || (row.is_Stolen.Value != x.is_Stolen) || (row.is_Dispose != x.is_Dispose))
                                {
                                    int workflowActionId = 0;

                                    if (rolesString.Contains("GS Borough"))
                                    {
                                        workflowActionId = _dataProvider.DataContext.Workflow_Action.SingleOrDefault(w => w.Workflow_cd == "AHQ").Workflow_Action_id;
                                    } else if (rolesString.Contains("GS HQ"))
                                    {
                                        workflowActionId = _dataProvider.DataContext.Workflow_Action.SingleOrDefault(w => w.Workflow_cd == "AWH").Workflow_Action_id;
                                    } else
                                    {
                                        workflowActionId = _dataProvider.DataContext.Workflow_Action.SingleOrDefault(w => w.Workflow_cd == "STB").Workflow_Action_id;
                                        
                                    }

                                    PowerToolInventoryUpdateModel updateModel = inventorySubmission.powertoolComments.Find(pt => pt.Power_Tool_Inventory_Id == x.Power_Tool_Inventory_id);

                                    _dataProvider.DataContext.Workflow_Tracking.AddObject(new Workflow_Tracking()
                                    {
                                        Workflow_Action_id = workflowActionId,
                                        Table_Id = x.Tool_Inventory_PT_id,
                                        Table_Association = "Tool_Inventory_PT",
                                        Userid = updateModel.Userid,
                                        Workflow_Datetime = DateTime.Now,
                                        Comment = updateModel.Comment
                                    });
                                }

                                row.is_Down = x.is_Down;
                                row.is_Active = x.is_Active;
                                row.is_Stolen = x.is_Stolen;
                                row.is_Dispose = x.is_Dispose;
                            }

                        }
                    });
                }

                if (inventorySubmission.warehouseInventory != null && inventorySubmission.warehouseInventory.Count() > 0)
                {
                    Workflow_Action action = _dataProvider.DataContext.Workflow_Action.FirstOrDefault(w => w.Workflow_cd == "ABO");
                    Guid warehouseGuid = _userRepository.getRolesUsers((new List<string>() { "gs warehouse" })).FirstOrDefault().userId;

                    inventorySubmission.warehouseInventory.ForEach(x =>
                    {
                        if (x.Tool_Inventory_WH_Id > 0)
                        {
                            var row = _dataProvider.DataContext.Tool_Inventory_WH.Where(y => y.Tool_Inventory_WH_id == x.Tool_Inventory_WH_Id).FirstOrDefault();

                            if (row != null)
                            {
                                row.Approved_Qty = x.Approved_Qty;
                                row.Approved_Datetime = DateTime.Now;
                            }

                            newWorkflowUsers.Add(_userRepository.getUser(row.Userid.Value));

                            row.Standard_Tools_Inventory.ToList().ForEach(s =>
                            {
                                _dataProvider.DataContext.Workflow_Tracking.AddObject(new Workflow_Tracking()
                                {
                                    Workflow_Action_id = action.Workflow_Action_id,
                                    Table_Id = _dataProvider.DataContext.Tool_Inventory_Std.FirstOrDefault(a => a.Tool_Inventory_id == inventory.Tool_Inventory_id && a.Tool_User_Id == s.Tool_User_id).Tool_Inventory_Std_Id,
                                    Table_Association = "Tool_Inventory_Std",
                                    Userid = warehouseGuid,
                                    Workflow_Datetime = DateTime.Now
                                });
                            });
                        }
                    });
                }

                _dataProvider.DataContext.SaveChanges();

                if (inventorySubmission.workflowActions != null && inventorySubmission.workflowActions.Count() > 0)
                {
                    inventorySubmission.workflowActions.ForEach(x =>
                    {
                        if (x.Workflow_Tracking_id > 0)
                        {
                            var row = _dataProvider.DataContext.Workflow_Tracking.Where(y => y.Workflow_Tracking_id == x.Workflow_Tracking_id).FirstOrDefault();
                            Workflow_Action action = _dataProvider.DataContext.Workflow_Action.SingleOrDefault(y => y.Workflow_Action_id == x.Workflow_Next_Action_id);
                            List<IUser> reportToUsers = new List<IUser>();

                            _dataProvider.DataContext.Workflow_Tracking.AddObject(new Workflow_Tracking()
                            {
                                Workflow_Action_id = action.Workflow_Action_id,
                                Table_Id = row.Table_Id,
                                Table_Association = row.Table_Association,
                                Userid = currentUserGuid,
                                Workflow_Datetime = DateTime.Now,
                                Comment = x.comment
                            });

                            if (action.is_End_Workflow == true)
                            {
                                Tool_Inventory_Std tis = _dataProvider.DataContext.Tool_Inventory_Std.FirstOrDefault(t => t.Tool_Inventory_Std_Id == row.Table_Id);
                                Standard_Tools_Inventory sti = _dataProvider.DataContext.Standard_Tools_Inventory.FirstOrDefault(t => t.Tool_User_id == tis.Tool_User_Id);
                                sti.Current_On_Hand += tis.Boro_Shipped_Qty;
                                sti.Last_Update_Date = DateTime.Now;

                                // send receipt email
                                if (sendEmails)
                                {
                                    MailMessage mm = new MailMessage();

                                    mm.Subject = String.Format("{0} received {1}", currentUser.UserName, sti.Tool_User.Tool_Type.Tool_Type_Name);
                                    mm.Body = String.Format("{0} has confirmed receipt of {1} {2}(s) of {3}.", currentUser.UserName, tis.Boro_Shipped_Qty, sti.Tool_User.Tool_Type.Measurement.Measurement_Name, sti.Tool_User.Tool_Type.Tool_Type_Name);

                                    _userRepository.getReportToUsers(currentUserGuid)
                                        .Where(u => u.roles.SingleOrDefault(r => r.roleName == "GS Borough") != null)
                                        .ToList()
                                        .ForEach(user =>
                                        {
                                            if (!String.IsNullOrEmpty(user.email))
                                            {
                                                mm.To.Add(new MailAddress(user.email, string.IsNullOrEmpty(user.fullName) ? string.Empty : user.fullName));
                                            }
                                        });

                                    if (mm.To.Count() > 0 && mm.To.SingleOrDefault(m => String.IsNullOrEmpty(m.Address)) == null)
                                    {
                                        EmailUtilities.SendEmail(mm);
                                    }
                                }
                            }
                                else if(action.Workflow_cd == "STD")
                            {
                                newWorkflowUsers.Add(_userRepository.getUser(x.userId));
                            }
                            else if(action.Workflow_cd != "AWH")
                            {
                                // don't do anything for AWH, borough/district check will handle 

                                // get all possible next action roles and assign workflows to users in command chain that has one of those roles
                                List<Workflow_Action> nextActions = action.Workflow_Action_flow.Select(f => f.Workflow_Action1).ToList();
                                List<String> actionRoleNames = nextActions.Where(a => a.Workflow_cd != "CNCL").SelectMany(a => a.Workflow_Action_Role.Select(r => r.aspnet_Roles.RoleName))
                                    .Distinct().ToList();

                                // remove HQ if there are multiple roles, HQ has access to all workflow actions, but need to allow if HQ is the only role for an action
                                if (actionRoleNames.Count > 1)
                                {
                                    actionRoleNames = actionRoleNames.Where(r => !r.Contains("GS HQ")).ToList();
                                    reportToUsers = _userRepository.getReportToUsers(currentUserGuid).Where(u => u.roles.Where(r => actionRoleNames.Contains(r.roleName)).Count() > 0).ToList();
                                }
                                else if (actionRoleNames[0] == "GS HQ")
                                {
                                    // HQ doesn't use command chain, so when only role for an action is HQ, need to explicitly assign workflow to HQ
                                    reportToUsers = _userRepository.getRolesUsers(new List<string>() { "gs hq" }).OrderBy(u => u.userName).ToList();
                                }

                                newWorkflowUsers = newWorkflowUsers.Concat(reportToUsers).ToList();
                            }
                        }
                    });

                    _dataProvider.DataContext.SaveChanges();

                    // get borough users and check if all inventory requests for districts have been submitted
                    List<IUser> boroughs = _userRepository.getRolesUsers((new List<string>() { "gs borough" }).ToList());
                    List<IUser> warehouses = _userRepository.getRolesUsers((new List<string>() { "gs warehouse" }).ToList());
                    int awhWorkflowId = _dataProvider.DataContext.Workflow_Action.SingleOrDefault(w => w.Workflow_cd == "AWH").Workflow_Action_id;
                    //List<Tool_Type> toolTypes = _dataProvider.DataContext.Tool_Type.Where(t => t.is_Active == true).ToList();
                    Workflow_Action warehouseAction = _dataProvider.DataContext.Workflow_Action.SingleOrDefault(w => w.Workflow_cd == "AWH");
                    Workflow_Action boroughAction = _dataProvider.DataContext.Workflow_Action.SingleOrDefault(w => w.Workflow_cd == "ABO");

                    // at least one workflow action should be AHQ before triggering borough check
                    if (inventorySubmission.workflowActions.FirstOrDefault(w => w.Workflow_Next_Action_id == awhWorkflowId) != null)
                    {
                        // loop over each borough and check if all districts in borough have completed inventory request
                        boroughs.ForEach(b =>
                        {
                            List<IUser> districts = _userRepository.getUserCommandChain(b.userId).ToList();
                            List<Tool_Inventory_Std> standardTools = new List<Tool_Inventory_Std>();

                            // gather all standard tool ids from borough users
                            districts.ForEach(d =>
                            {
                                standardTools = standardTools.Concat(
                                    _dataProvider.DataContext.Tool_Inventory_Std.Where(t => t.Tool_Inventory_id == inventory.Tool_Inventory_id && t.Tool_User.Userid == d.userId)).ToList();
                            });

                            List<int> standardToolIds = standardTools.Select(t => t.Tool_Inventory_Std_Id).ToList();

                            List<int> maxWorkflowIds = _dataProvider.DataContext.Workflow_Tracking
                                .Where(w => w.Table_Association == "Tool_Inventory_Std" && standardToolIds.Contains(w.Table_Id.Value))
                                .GroupBy(w => new { table = w.Table_Association, id = w.Table_Id })
                                .Select(w => w.Max(row => row.Workflow_Tracking_id))
                                .ToList();

                            List<Workflow_Tracking> workflows = _dataProvider.DataContext.Workflow_Tracking
                                .Where(w => maxWorkflowIds.Contains(w.Workflow_Tracking_id)).ToList();

                            // check that all workflows are on workflow action AHQ or CNCL
                            if (workflows.Count() > 0 && workflows.Where(w => w.Workflow_Action.Workflow_cd == "AWH" || w.Workflow_Action.Workflow_cd == "CNCL").Count() == workflows.Count())
                            {
                                List<Tool_Inventory_WH> warehouseRequests = new List<Tool_Inventory_WH>();

                                // create warehouse requests
                                standardTools.Select(a => a.Tool_User.Tool_Type).Distinct().ToList().ForEach(tt =>
                                {
                                    int sum = standardTools.Where(st => st.Tool_User.Tool_Type_id == tt.Tool_Type_id).Sum(st => st.Boro_Approved_Qty).Value;

                                    warehouses.ForEach(w =>
                                    {
                                        Tool_Inventory_WH toolRequest = new Tool_Inventory_WH()
                                        {
                                            Requested_Qty = sum,
                                            Tool_Inventory_id = inventory.Tool_Inventory_id,
                                            Tool_Type_id = tt.Tool_Type_id,
                                            Userid = b.userId
                                        };

                                        _dataProvider.DataContext.Tool_Inventory_WH.AddObject(toolRequest);
                                        warehouseRequests.Add(toolRequest);
                                    });
                                });

                                _dataProvider.DataContext.SaveChanges();

                                warehouseRequests.ForEach(w =>
                                {
                                    standardTools.Where(st => st.Tool_User.Tool_Type.Tool_Type_id == w.Tool_Type_id).SelectMany(st => st.Tool_User.Standard_Tools_Inventory).ToList().ForEach(st =>
                                    {
                                        w.Standard_Tools_Inventory.Add(st);
                                    });

                                    _dataProvider.DataContext.Workflow_Tracking.AddObject(new Workflow_Tracking()
                                    {
                                        Table_Association = "Tool_Inventory_WH",
                                        Table_Id = w.Tool_Inventory_WH_id,
                                        Workflow_Action_id = warehouseAction.Workflow_Action_id,
                                        Workflow_Datetime = DateTime.Now,
                                        Userid = w.Userid.Value
                                    });
                                });

                                _dataProvider.DataContext.SaveChanges();

                                newWorkflowUsers = newWorkflowUsers.Concat(_userRepository.getRolesUsers(new List<string>() { "gs warehouse" }).OrderBy(u => u.userName).ToList()).ToList();
                            }
                        });
                    }
                }

                // send messsage/emails to workflow users
                newWorkflowUsers = newWorkflowUsers.GroupBy(u => u.userId).Select(u => u.First()).ToList();

                if (newWorkflowUsers.Count() > 0)
                {
                    String messageSubject = "An inventory submission workflow has been updated";
                    String messageBody = String.Format("An inventory workflow has been updated by {0} and requires your attention.\nThis is done by going to CMS, clicking on the Garage Supplies and selecting the Inventory Submission.", currentUser.UserName);

                    _messageRepository.addMessageAndSendToUsers(new Message()
                    {
                        Sent_By_UserId = currentUserGuid,
                        Message_Date = DateTime.Now,
                        Message_Subject = messageSubject,
                        Message_Text = messageBody,
                        is_Fuel_Form = false
                    }, newWorkflowUsers.Select(u => u.userId).ToList());

                    // send emails to users
                    if (sendEmails)
                    {
                        MailMessage mm = new MailMessage();

                        // setup message
                        mm.Subject = messageSubject;
                        mm.Body = messageBody;

                        newWorkflowUsers.ForEach(user =>
                        {
                            if (!String.IsNullOrEmpty(user.email))
                            {
                                mm.To.Add(new MailAddress(user.email, string.IsNullOrEmpty(user.fullName) ? string.Empty : user.fullName));
                            }
                        });

                        if (mm.To.Count() > 0 && mm.To.SingleOrDefault(m => String.IsNullOrEmpty(m.Address)) == null)
                        {
                            EmailUtilities.SendEmail(mm);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _exceptionHandler.HandleException(ex);
                throw (ex);
            }

            return Json("Success");
        }

        #endregion
        #endregion
    }
}