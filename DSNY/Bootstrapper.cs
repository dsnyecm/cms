﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using DSNY.Core.Interfaces;
using DSNY.Core.Repository;
using System.Web.Mvc;
using DSNY.Models;
using DSNY.Common.Logger;
using System.Web.Security;
using DSNY.Common.Exception;
using DSNY.Data;
using System.Web.Profile;
using DSNY.Repository.Interfaces.Repositories;
using DSNY.Repository.Repositories;

namespace DSNY.Mvc
{
    public static class Bootstrapper
    {
        private static IUnityContainer container = null;

        public static void ConfigureUnityContainer()
        {
            container = new UnityContainer();
            
            // Registrations
            container
                .RegisterType<DSNYContext, DSNYContext>(new HttpContextLifetimeManager<DSNYContext>())
                .RegisterType<ILogger, EnterpriseLogger>(new HttpContextLifetimeManager<EnterpriseLogger>())
                .RegisterType<IExceptionHandler, EnterpriseExceptionHandler>(new HttpContextLifetimeManager<EnterpriseExceptionHandler>())
                .RegisterType<ILogRepository, LogRepository>()
                .RegisterType<IEquipmentRepository, EquipmentRepository>()
                .RegisterType<IDailyReportRepository, DailyReportRepository>()
                .RegisterType<IPurchaseOrderRepository, PurchaseOrderRepository>()
                .RegisterType<IFuelFormRepository, FuelFormRepository>()
                .RegisterType<IMessageRepository, MessageRepository>()
                .RegisterType<IProductRepository, ProductRepository>()
                .RegisterType<IRoleRepository, RoleRepository>()
                .RegisterType<IDSNYExceptionRepository, DSNYExceptionRepository>()
                .RegisterType<IUserRepository, UserRepository>()
                .RegisterType<IVendorRepository, VendorRepository>()
                .RegisterType<IRadioRepository, RadioRepository>()
                .RegisterType<ProfileProvider, SqlProfileProvider>()
                .RegisterType<IFormsAuthenticationService, FormsAuthenticationService>()
                .RegisterType<IMembershipService, AccountMembershipService>()
                .RegisterInstance<MembershipProvider>(Membership.Provider)
                .RegisterInstance<RoleProvider>(Roles.Provider);
            
            ControllerBuilder.Current.SetControllerFactory(
                new DSNY.Mvc.Controllers.UnityControllerFactory(container));
        }
    }
}