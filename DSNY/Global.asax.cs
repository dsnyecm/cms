﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using DSNY.Mvc;

using DSNY.Data;
using DSNY.Mapping;
using DSNY.ViewModels;
using AutoMapper;

namespace DSNY
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        // Static variable to hold the global pageSize
        static int _pageSize = 0;

        /// <summary>
        /// Get or set the global page size variable
        /// </summary>
        public static int pageSize
        {
            get
            {
                if (_pageSize == 0)
                    int.TryParse(ConfigurationManager.AppSettings["PageSize"], out _pageSize);

                return _pageSize;
            }
            set
            {
                _pageSize = value;
            }
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.([iI][cC][oO]|[gG][iI][fF])(/.*)?" });

            #region Role Routes

            routes.MapRoute("Role Edit", "Roles/{action}/{name}", // URL with parameters
                new { controller = "Role" } // Parameter defaults
            );

            routes.MapRoute("Roles List", "Roles/{action}", // URL with parameters
                new { controller = "Role", action = "List" } // Parameter defaults
            );

            #endregion

            #region User Routes

            routes.MapRoute("User Edit", "Users/{action}/{name}", // URL with parameters
                new { controller = "User" } // Parameter defaults
            );

            routes.MapRoute("User List", "Users/{action}", // URL with parameters
                new { controller = "User", action = "List" } // Parameter defaults
            );

            #endregion

            #region Product Routes

            routes.MapRoute("Product Edit", "Products/{action}/{id}", // URL with parameters
                new { controller = "Product" } // Parameter defaults
            );

            routes.MapRoute("Product List", "Products/{action}", // URL with parameters
                new { controller = "Product", action = "List" } // Parameter defaults
            );

            #endregion

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        protected void Application_Start()
        {
            RegisterRoutes(RouteTable.Routes);

            ValueProviderFactories.Factories.Add(new JsonValueProviderFactory());
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMissingTypeMaps = true;

                cfg.CreateMap<aspnet_Roles, RoleViewModel>();

                cfg.CreateMap<STC_Site_On_Hand, SiteOnHand>().ConvertUsing(new SiteOnHandConverter());
                cfg.CreateMap<STC_Purchase_Order, StcPurchaseOrder>().ConvertUsing(new StcPurchaseOrderConverter());
                cfg.CreateMap<STC_Needs_Request, NeedsRequest>().ConvertUsing(new NeedsRequestConverter(new DSNYContext()));
                cfg.CreateMap<DSNY.Data.Zone, DSNY.ViewModels.Zone>().ConvertUsing(new ZoneConverter());
                cfg.CreateMap<DSNY.Data.STC_Orders, DSNY.ViewModels.StcOrder>().ConvertUsing(new StcOrderConverter());
                cfg.CreateMap<DSNY.Data.STC_Product, DSNY.ViewModels.StcProduct>().ConvertUsing(new StcProductConverter());
                cfg.CreateMap<DSNY.Data.STC_Delivery, DSNY.ViewModels.Delivery>().ConvertUsing(new DeliveryConverter());
                cfg.CreateMap<DSNY.Data.Measurement, DSNY.ViewModels.Measurement>().ConvertUsing(new MeasurementConverter());
                cfg.CreateMap<DSNY.Data.Truck_Type, DSNY.ViewModels.TruckType>().ConvertUsing(new TruckTypeConverter());
                cfg.CreateMap<DSNY.Data.STC_Agency, DSNY.ViewModels.StcAgency>().ConvertUsing(new StcAgencyConverter());
                cfg.CreateMap<DSNY.Data.STC_Pickup, DSNY.ViewModels.StcPickup>().ConvertUsing(new StcPickupConverter());
                cfg.CreateMap<DSNY.Data.STC_Transfer, DSNY.ViewModels.StcTransfer>().ConvertUsing(new StcTransferConverter());
                cfg.CreateMap<DSNY.Data.Radio_Repair_Problem_Type, DSNY.ViewModels.RepairProblemTypeModel>().ConvertUsing(new RadioRepairProblemTypeConverter());
                cfg.CreateMap<DSNY.Data.Radio_Equipment_Status, DSNY.ViewModels.EquipmentStatusModel>().ConvertUsing(new RadioEquipmentStatusConverter());
                cfg.CreateMap<DSNY.Data.Radio_Equipment_Type, DSNY.ViewModels.EquipmentTypeModel>().ConvertUsing(new RadioEquipmentTypeConverter());
                cfg.CreateMap<DSNY.Data.Radio_Make, DSNY.ViewModels.RadioMakeModel>().ConvertUsing(new RadioMakeConverter());
                cfg.CreateMap<DSNY.Data.Radio_Model, DSNY.ViewModels.RadioModelVM>().ConvertUsing(new RadioModelConverter());

                cfg.CreateMap<ToolTypeMakeModelViewModel, Model>();
                cfg.CreateMap<ToolTypeMakeViewModel, Make>();
                cfg.CreateMap<ToolTypeViewModel, Tool_Type>();                
                cfg.CreateMap<MeasurementViewModel, DSNY.Data.Measurement>();

                cfg.CreateMap<Model, ToolTypeMakeModelViewModel>();
                cfg.CreateMap<Make, ToolTypeMakeViewModel>();
                cfg.CreateMap<Tool_Type, ToolTypeViewModel>();
                cfg.CreateMap<Tool_Type, ToolTypeMinViewModel>();
                cfg.CreateMap<DSNY.Data.Measurement, MeasurementViewModel>();

                cfg.CreateMap<Tool_User, ToolUserViewModel>();
                cfg.CreateMap<ToolUserViewModel, Tool_User>();
                cfg.CreateMap<Power_Tool_Inventory, PowerToolInventoryViewModel>();
                cfg.CreateMap<PowerToolInventoryViewModel, Power_Tool_Inventory>();

                cfg.CreateMap<Standard_Tools_Inventory, StandardToolsInventoryViewModel>();
                cfg.CreateMap<StandardToolsInventoryViewModel, Standard_Tools_Inventory>();

                cfg.CreateMap<Standard_Tools_Inventory, StandardToolsInventoryViewModel>();
                cfg.CreateMap<StandardToolsInventoryViewModel, Standard_Tools_Inventory>();

                cfg.CreateMap<Tool_Inventory, ToolInventoryViewModel>();
                cfg.CreateMap<ToolInventoryViewModel, Tool_Inventory>();

                cfg.CreateMap<Tool_Inventory_PT, ToolInventoryPowertoolViewModel>();
                cfg.CreateMap<ToolInventoryPowertoolViewModel, Tool_Inventory_PT>();

                cfg.CreateMap<Tool_Inventory_Std, ToolInventoryStandardViewModel>();
                cfg.CreateMap<ToolInventoryStandardViewModel, Tool_Inventory_Std>();

                cfg.CreateMap<Tool_Inventory_WH, ToolInventoryWarehouseViewModel>();
                cfg.CreateMap<ToolInventoryWarehouseViewModel, Tool_Inventory_WH>();

                cfg.CreateMap<Workflow_Action, WorkflowActionViewModel>();
                cfg.CreateMap<Workflow_Action_flow, Workflow_Action_flow>();
                cfg.CreateMap<Workflow_Action_Role, WorkflowActionRoleViewModel>();
                cfg.CreateMap<Workflow_Tracking, WorkflowTrackingViewModel>();

                cfg.CreateMap<WorkflowActionViewModel, Workflow_Action>();
                cfg.CreateMap<Workflow_Action_flow, Workflow_Action_flow>();
                cfg.CreateMap<WorkflowActionRoleViewModel, Workflow_Action_Role>();
                cfg.CreateMap<WorkflowTrackingViewModel, Workflow_Tracking>();
                cfg.CreateMap<DSNY.Data.Radio, DSNY.ViewModels.RadioItemViewModel>().ConvertUsing(new RadioToRadioItemViewModelConverter());
                cfg.CreateMap<DSNY.ViewModels.RadioItemViewModel,DSNY.Data.Radio>().ConvertUsing(new RadioItemToRadioConverter());
                cfg.CreateMap<DSNY.Data.Radio, DSNY.Data.Radio_History>().ConvertUsing(new RadioToRadioHistoryConverter());
                cfg.CreateMap<DSNY.Data.Radio_Accessory, RadioAccessoryViewModel>().ConvertUsing(new RadioAccessoryToRadioAccessoryViewModelConverter()); 
                cfg.CreateMap<RadioAccessoryViewModel,DSNY.Data.Radio_Accessory>().ConvertUsing(new RadioAccessoryViewModelToRadioAccessoryConverter());
            });

            //Configure for Dependecny Injection using Unity
            Bootstrapper.ConfigureUnityContainer();
        }

        // Force garbage collection at the end of a request
        // Seems like there is a bug in entities framework that does not release memory after a query is executed
        // This keeps memory usage reasonable
        protected void Application_EndRequest()
        {
            GC.Collect();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            string Exception_Policy = ConfigurationManager.AppSettings["Exception Policy Group"];

            Exception ex = Server.GetLastError();
            if (ex is HttpUnhandledException && ex.InnerException != null)
            {
                ex = ex.InnerException;
            }

            if (ex != null)
            {
                Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.ExceptionPolicy.HandleException(ex, Exception_Policy);
            }
        }
    }
}