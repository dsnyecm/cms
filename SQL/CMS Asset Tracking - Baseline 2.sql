USE [DSNY_MESSAGING_PROD]
GO

 ALTER TABLE dbo.Tool_Inventory_PT ADD is_Dispose bit null
 go

ALTER TABLE dbo.STC_Pickup ADD
Driver_Name varchar(50) NULL
GO

ALTER TABLE dbo.Tool_Inventory_PT ADD
is_Dispose bit NULL
GO

-- =============================================
-- Author:	Mohamed Ashry
-- Create date: 7/22/2018
-- Description:	Returns Users with a given role
-- =============================================
CREATE FUNCTION ufn_GetUserIdByRole (@RoleName nvarchar(256))
RETURNS table
AS
Return SELECT au.UserId 
FROM aspnet_users au, dbo.aspnet_UsersInRoles aur,dbo.aspnet_Roles ar
where au.UserId = aur.UserId and 
aur.RoleId = ar.RoleId and
ar.LoweredRoleName = LOWER(@RoleName) --STC Site,STC Borough,STC HQ

GO

CREATE TABLE [dbo].[Tool_Inventory_Association](
	[Tool_Inventory_WH_id] [int] NOT NULL,
	[Standard_Tools_Inventory_id] [int] NOT NULL
 CONSTRAINT [XPKTool_Inventory_Association] PRIMARY KEY CLUSTERED 
(
	[Tool_Inventory_WH_id] ASC,
	[Standard_Tools_Inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Tool_Inventory_Association]  WITH CHECK ADD  CONSTRAINT [FK_Tool_Inventory_Association_Standard_Tools_Inventory] FOREIGN KEY([Standard_Tools_Inventory_id])
REFERENCES [dbo].[Standard_Tools_Inventory] ([Standard_Tools_Inventory_id])
GO

ALTER TABLE [dbo].[Tool_Inventory_Association]  WITH CHECK ADD  CONSTRAINT [FK_Tool_Inventory_Association_Tool_Inventory_WH] FOREIGN KEY([Tool_Inventory_WH_id])
REFERENCES [dbo].[Tool_Inventory_WH] ([Tool_Inventory_WH_id])
GO

INSERT INTO Workflow_Action_flow VALUES(6, 7);
INSERT INTO Workflow_Action_flow VALUES(6, 11);