ALTER TABLE [dbo].[Tool_Type]  WITH CHECK ADD  CONSTRAINT [FK_Tool_Type_Measurement] FOREIGN KEY([Measurement_id])
	REFERENCES [dbo].[Measurement] ([Measurement_id])
GO

ALTER TABLE [dbo].[Make]  WITH CHECK ADD  CONSTRAINT [FK_Make_Tool_Type] FOREIGN KEY([Tool_Type_id])
	REFERENCES [dbo].[Tool_Type] ([Tool_Type_id])
GO

ALTER TABLE [dbo].[Make] ADD is_Active bit null;
GO

ALTER TABLE [dbo].[Power_Tool_Inventory] ADD is_Active bit null;
GO

ALTER TABLE [dbo].[Tool_Inventory_WH] ADD Tool_Type_id bit null;
GO

ALTER TABLE [dbo].[Tool_Inventory_WH]  WITH CHECK ADD  CONSTRAINT [FK_Tool_Inventory_WH_Tool_Type] FOREIGN KEY([Tool_Type_id])
	REFERENCES [dbo].[Tool_Type] ([Tool_Type_id])
GO

ALTER TABLE [dbo].[Measurement] ADD is_STC bit null, is_Tool  bit null;
GO

ALTER TABLE [dbo].Standard_Tools_Ledger ADD Comment varchar(200) null;
GO

ALTER TABLE [dbo].[Standard_Tools_Inventory] DROP CONSTRAINT FK_Standard_Tools_Inventory_aspnet_Users, FK_Standard_Tools_Inventory_Tool_Type

ALTER TABLE [dbo].[Standard_Tools_Inventory] DROP COLUMN Userid, Tool_Type_id

ALTER TABLE [dbo].[Standard_Tools_Inventory] ADD Tool_User_id int not null;
GO

ALTER TABLE [dbo].[Standard_Tools_Inventory]  WITH CHECK ADD  CONSTRAINT [FK_Standard_Tools_Inventory_Tool_User] FOREIGN KEY([Tool_User_id])
	REFERENCES [dbo].[Tool_User] ([Tool_User_id])
GO

ALTER TABLE [dbo].[Model] ALTER COLUMN Model_Name varchar(50) not null;
GO

ALTER TABLE [dbo].[Tool_Inventory_WH] DROP CONSTRAINT PK_Tool_Inventory_WH
-- NOT SURE ABOUT THIS, MAY NEED TO NAME NEW CONSTRAINT ON NEW COLUMN CREATION
ALTER TABLE [dbo].[Tool_Inventory_WH] DROP COLUMN Tool_Inventory_WH_id;
ALTER TABLE [dbo].[Tool_Inventory_WH] ADD Tool_Inventory_WH_id int IDENTITY(1,1) PRIMARY KEY;
GO

ALTER TABLE [dbo].[Tool_Inventory_WH] DROP CONSTRAINT FK_Tool_Inventory_WH_Tool_Inventory
ALTER TABLE [dbo].[Tool_Inventory_WH] DROP COLUMN Tool_Inventory_id
ALTER TABLE [dbo].[Tool_Inventory_WH] ADD Tool_Inventory_id int not null;
ALTER TABLE [dbo].[Tool_Inventory_WH]  WITH CHECK ADD  CONSTRAINT [FK_Tool_Inventory_WH_Tool_Inventory] FOREIGN KEY([Tool_Inventory_id])
	REFERENCES [dbo].[Tool_Inventory] ([Tool_Inventory_id])
GO
GO

-- create dsnywarehouse user and add to role gs warehouse
DECLARE	@return_value int,
		@UserId uniqueidentifier,
		@CurrentTimeUtc datetime

SET @CurrentTimeUtc = GetDate()

EXEC	@return_value = [dbo].[aspnet_Membership_CreateUser]
		@ApplicationName = N'/',
		@UserName = N'dsnywarehouse',
		@Password = N'dsny2018',
		@PasswordSalt = '',
		@Email = NULL,
		@PasswordQuestion = NULL,
		@PasswordAnswer = NULL,
		@CurrentTimeUtc = @CurrentTimeUtc,
		@IsApproved = 1,
		@UserId = @UserId OUTPUT

INSERT INTO [dbo].[aspnet_UsersInRoles] (UserId, RoleId)
	VALUES(@UserId, (SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = 'gs warehouse'))
GO

-- Populate command chain with borough users
--DECLARE	@BkReturn_value int,
--		@BkUserId uniqueidentifier,
--		@BkCurrentTimeUtc datetime

--SET @BkCurrentTimeUtc = GetDate()

--EXEC	@BkReturn_value = [dbo].[aspnet_Membership_CreateUser]
--		@ApplicationName = N'/',
--		@UserName = N'bkborough',
--		@Password = N'dsny2018',
--		@PasswordSalt = '',
--		@Email = NULL,
--		@PasswordQuestion = NULL,
--		@PasswordAnswer = NULL,
--		@CurrentTimeUtc = @BkCurrentTimeUtc,
--		@IsApproved = 1,
--		@UserId = @BkUserId OUTPUT

--INSERT INTO [dbo].[aspnet_UsersInRoles] (UserId, RoleId)
--	VALUES(@BkUserId, (SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = 'gs borough'))

--INSERT INTO [dbo].Command_Chain 
--	SELECT DISTINCT u.userid, @BkUserId
--		FROM Fuel_Form ff
--		INNER JOIN aspnet_Users u on ff.userid = u.UserId
--		WHERE LoweredUserName LIKE 'bk%'
--GO

-- Prepopulate Tool_User
INSERT INTO [dbo].[aspnet_UsersInRoles]
	SELECT DISTINCT userid, (SELECT roleid FROM dbo.aspnet_Roles WHERE LoweredRoleName = 'gs district')
		FROM Fuel_Form
GO

INSERT INTO [dbo].[Tool_User]
	SELECT DISTINCT Tool_Type_Id, Userid, 0, 1
		FROM Tool_Type tt,dbo.aspnet_UsersInRoles au
		WHERE tt.is_PowerTool = 0 
			AND tt.is_Active = 1
			AND au.RoleId IN 
				(SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName IN ('gs district','gs borough', 'gs warehouse'))
GO

INSERT INTO [dbo].[Standard_Tools_Inventory]
	SELECT 0, GETDATE(), Tool_User_id
		FROM Tool_User tu
		INNER JOIN Tool_Type tt on tu.Tool_Type_id = tt.Tool_Type_id
		INNER JOIN aspnet_Users u on tu.Userid = u.UserId 
		INNER JOIN aspnet_UsersInRoles au on u.UserId = au.UserId 
		WHERE tt.is_PowerTool = 0 
			AND tt.is_Active = 1
			AND tu.is_Active = 1
			AND au.RoleId IN
				(SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName IN ('gs district','gs borough','gs warehouse'))

-- NEED TO CREATE BOROUGH USERS

INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('IR','Inventory Request Initiated',0);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('STB','Submit To Borough',0);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('AHQ','Approved. Submit To HQ',0);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('AWH','Approved. Submit To Warehouse',0);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('ABO','Approved. Ship to Borough.',0);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('STD','Ship To District',1);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('CRE','Confirm Reciept of Equipment',1);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('AHQR','Rejected',1);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('AWHR','Rejected',1);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('ABOR','Rejected',1);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('CNCL','Cancel',1);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('WHRCV','Warehouse Recieve',1);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('EMSND','Emergency Send',1);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('ADDCMS','Added to CMS',1);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('REASGN','Reassign Equipment',1);

insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'IR'),(select workflow_action_id from workflow_action where workflow_cd = 'STB'))
insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'IR'),(select workflow_action_id from workflow_action where workflow_cd = 'CNCL'))
insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'STB'),(select workflow_action_id from workflow_action where workflow_cd = 'AHQ'))
insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'STB'),(select workflow_action_id from workflow_action where workflow_cd = 'AHQR'))
insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'STB'),(select workflow_action_id from workflow_action where workflow_cd = 'CNCL'))
insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AHQ'),(select workflow_action_id from workflow_action where workflow_cd = 'AWH'))
insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AHQ'),(select workflow_action_id from workflow_action where workflow_cd = 'AWHR'))
insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AHQ'),(select workflow_action_id from workflow_action where workflow_cd = 'CNCL'))
insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AWH'),(select workflow_action_id from workflow_action where workflow_cd = 'ABO'))
insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AWH'),(select workflow_action_id from workflow_action where workflow_cd = 'ABOR'))
insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AWH'),(select workflow_action_id from workflow_action where workflow_cd = 'CNCL'))
insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'ABO'),(select workflow_action_id from workflow_action where workflow_cd = 'STD'))
insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'ABO'),(select workflow_action_id from workflow_action where workflow_cd = 'CNCL'))

insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'STB'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs district'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AHQ'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs borough'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'ABO'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs warehouse'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'STD'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs borough'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'CRE'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs district'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AHQR'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs borough'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'ABOR'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs warehouse'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'CNCL'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs district'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'CNCL'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs borough'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'CNCL'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs warehouse'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'STB'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AHQ'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AWH'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'ABO'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'STD'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'CRE'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AHQR'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AWHR'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'ABOR'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'CNCL'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))


INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[Borough]
           ([Borough_cd]
           ,[Borough_Name])
     VALUES
           ('MN'
           ,'Manhattan');
INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[Borough]
           ([Borough_cd]
           ,[Borough_Name])
     VALUES
           ('BX'
           ,'Bronx');
INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[Borough]
           ([Borough_cd]
           ,[Borough_Name])
     VALUES
           ('BN'
           ,'Brooklyn North');
INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[Borough]
           ([Borough_cd]
           ,[Borough_Name])
     VALUES
           ('BS'
           ,'Brooklyn South');
INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[Borough]
           ([Borough_cd]
           ,[Borough_Name])
     VALUES
           ('SI'
           ,'Staten Island');
INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[Borough]
           ([Borough_cd]
           ,[Borough_Name])
     VALUES
           ('QW'
           ,'Queens West');          
INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[Borough]
           ([Borough_cd]
           ,[Borough_Name])
     VALUES
           ('QE'
           ,'Queens East');                                                             
GO

/****** Object:  Index [idx_wrkflw_trck_tabass_tabid]    Script Date: 06/17/2018 22:01:11 ******/
CREATE NONCLUSTERED INDEX [idx_wrkflw_trck_tabass_tabid] ON [dbo].[Workflow_Tracking] 
(
[Table_Association] ASC,
[Table_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO