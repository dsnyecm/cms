/****** Object:  Index [idx_ff_user_ffid]    Script Date: 06/05/2016 20:00:07 ******/
CREATE NONCLUSTERED INDEX [idx_ff_user_ffid] ON [dbo].[Fuel_Form] 
(
	[Fuel_Form_ID] ASC,
	[UserId] ASC,
	[Submission_Date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


/****** Object:  Index [idx_ffd_equipuserid]    Script Date: 06/05/2016 20:00:53 ******/
CREATE NONCLUSTERED INDEX [idx_ffd_equipuserid] ON [dbo].[Fuel_Form_Details] 
(
	[Equipment_User_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

USE [DSNY_MESSAGING_PROD]
GO

/****** Object:  Index [idx_ffd_ffid_prodid]    Script Date: 06/05/2016 20:01:18 ******/
CREATE NONCLUSTERED INDEX [idx_ffd_ffid_prodid] ON [dbo].[Fuel_Form_Details] 
(
	[Fuel_Form_ID] ASC,
	[Product_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

USE [DSNY_MESSAGING_PROD]
GO

/****** Object:  Index [idx_ffd_prodid]    Script Date: 06/05/2016 20:01:28 ******/
CREATE NONCLUSTERED INDEX [idx_ffd_prodid] ON [dbo].[Fuel_Form_Details] 
(
	[Product_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


UPDATE Fuel_Form_Details
SET Tank_Delivered = abs((case 
						when (On_Hand - (
							select top 1 On_Hand 
							from Fuel_Form ff1, Fuel_Form_Details ffd1 
							where ff1.userId = ff.UserId and ff1.Fuel_Form_ID = ffd1.Fuel_Form_ID and ffd1.Product_ID = ffd.product_id and isnull(ffd.Equipment_User_ID,ffd.product_id) = isnull(ffd1.Equipment_User_ID,ffd1.Product_ID) and
									ffd1.Fuel_Form_ID = (select MAX(ff2.Fuel_Form_ID)
															from Fuel_Form ff2, Fuel_Form_Details ffd2 
															where ff2.Fuel_Form_ID = ffd2.Fuel_Form_ID and ff2.userId = ff.UserId and ffd.Product_ID = ffd2.Product_ID 
																 and ff2.Submission_Date < ff.Submission_Date and isnull(ffd.Equipment_User_ID,ffd.product_id) = isnull(ffd2.Equipment_User_ID,ffd2.Product_ID) ) )) 
							< 0 then 0
						when
						(On_Hand - (
						select top 1 On_Hand 
						from Fuel_Form ff1, Fuel_Form_Details ffd1 
						where ff1.userId = ff.UserId and ff1.Fuel_Form_ID = ffd1.Fuel_Form_ID and ffd1.Product_ID = ffd.product_id and isnull(ffd.Equipment_User_ID,ffd.product_id) = isnull(ffd1.Equipment_User_ID,ffd1.Product_ID) and
								ffd1.Fuel_Form_ID = (select MAX(ff2.Fuel_Form_ID)
														from Fuel_Form ff2, Fuel_Form_Details ffd2 
														where ff2.Fuel_Form_ID = ffd2.Fuel_Form_ID and ff2.userId = ff.UserId and ffd.Product_ID = ffd2.Product_ID 
															 and ff2.Submission_Date < ff.Submission_Date and isnull(ffd.Equipment_User_ID,ffd.product_id) = isnull(ffd2.Equipment_User_ID,ffd2.Product_ID) ) )) 
						  > 0 then
						  (On_Hand - (
						select top 1 On_Hand 
						from Fuel_Form ff1, Fuel_Form_Details ffd1 
						where ff1.userId = ff.UserId and ff1.Fuel_Form_ID = ffd1.Fuel_Form_ID and ffd1.Product_ID = ffd.product_id and isnull(ffd.Equipment_User_ID,ffd.product_id) = isnull(ffd1.Equipment_User_ID,ffd1.Product_ID) and
								ffd1.Fuel_Form_ID = (select MAX(ff2.Fuel_Form_ID)
														from Fuel_Form ff2, Fuel_Form_Details ffd2 
														where ff2.Fuel_Form_ID = ffd2.Fuel_Form_ID and ff2.userId = ff.UserId and ffd.Product_ID = ffd2.Product_ID 
															 and ff2.Submission_Date < ff.Submission_Date and isnull(ffd.Equipment_User_ID,ffd.product_id) = isnull(ffd2.Equipment_User_ID,ffd2.Product_ID) ) ))
						else 0
						end)),
	Tank_Dispensed = abs((case 
						when (On_Hand - (
							select top 1 On_Hand 
							from Fuel_Form ff1, Fuel_Form_Details ffd1 
							where ff1.userId = ff.UserId and ff1.Fuel_Form_ID = ffd1.Fuel_Form_ID and ffd1.Product_ID = ffd.product_id and isnull(ffd.Equipment_User_ID,ffd.product_id) = isnull(ffd1.Equipment_User_ID,ffd1.Product_ID) and
									ffd1.Fuel_Form_ID = (select MAX(ff2.Fuel_Form_ID)
															from Fuel_Form ff2, Fuel_Form_Details ffd2 
															where ff2.Fuel_Form_ID = ffd2.Fuel_Form_ID and ff2.userId = ff.UserId and ffd.Product_ID = ffd2.Product_ID 
																 and ff2.Submission_Date < ff.Submission_Date and isnull(ffd.Equipment_User_ID,ffd.product_id) = isnull(ffd2.Equipment_User_ID,ffd2.Product_ID) ) )) 
							> 0 then 0
						when
						(On_Hand - (
						select top 1 On_Hand 
						from Fuel_Form ff1, Fuel_Form_Details ffd1 
						where ff1.userId = ff.UserId and ff1.Fuel_Form_ID = ffd1.Fuel_Form_ID and ffd1.Product_ID = ffd.product_id and isnull(ffd.Equipment_User_ID,ffd.product_id) = isnull(ffd1.Equipment_User_ID,ffd1.Product_ID) and
								ffd1.Fuel_Form_ID = (select MAX(ff2.Fuel_Form_ID)
														from Fuel_Form ff2, Fuel_Form_Details ffd2 
														where ff2.Fuel_Form_ID = ffd2.Fuel_Form_ID and ff2.userId = ff.UserId and ffd.Product_ID = ffd2.Product_ID 
															 and ff2.Submission_Date < ff.Submission_Date and isnull(ffd.Equipment_User_ID,ffd.product_id) = isnull(ffd2.Equipment_User_ID,ffd2.Product_ID) ) )) 
						  < 0 then
						  (On_Hand - (
						select top 1 On_Hand 
						from Fuel_Form ff1, Fuel_Form_Details ffd1 
						where ff1.userId = ff.UserId and ff1.Fuel_Form_ID = ffd1.Fuel_Form_ID and ffd1.Product_ID = ffd.product_id and isnull(ffd.Equipment_User_ID,ffd.product_id) = isnull(ffd1.Equipment_User_ID,ffd1.Product_ID) and
								ffd1.Fuel_Form_ID = (select MAX(ff2.Fuel_Form_ID)
														from Fuel_Form ff2, Fuel_Form_Details ffd2 
														where ff2.Fuel_Form_ID = ffd2.Fuel_Form_ID and ff2.userId = ff.UserId and ffd.Product_ID = ffd2.Product_ID 
															 and ff2.Submission_Date < ff.Submission_Date and isnull(ffd.Equipment_User_ID,ffd.product_id) = isnull(ffd2.Equipment_User_ID,ffd2.Product_ID) ) ))
						else 0
						end)),

Spare_Drums = ISNULL(spare_drums, 0 )
						
					
FROM Fuel_Form_Details ffd 
inner join Fuel_Form ff
ON (ff.Fuel_Form_ID = ffd.Fuel_Form_ID)
WHERE DATEPART(yyyy,ff.Submission_Date) = YEAR(getdate())

GO

UPDATE Product
	SET is_Reading_Required = 1
	WHERE Product_Name = 'DIESEL' OR Product_Name = 'WASTE OIL/USED OIL' OR Product_Name = 'MOTOR OIL' OR 
	Product_Name = 'HOIST OIL' OR Product_Name = 'UNLEADED' OR Product_Name = 'HYDRAULIC OIL' OR
	Product_Name = 'BIODIESEL-B20'
	 
GO

-- ADDING ROLES BACK
--INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', 'BF30E4F3-C33C-4839-8B02-8A32EF93F247', 'Reports', 'reports', NULL)

--INSERT INTO aspnet_UsersInRoles VALUES('74189826-208B-4CDC-AE87-0A6F77CDD3E4', 'BF30E4F3-C33C-4839-8B02-8A32EF93F247')
--INSERT INTO aspnet_UsersInRoles VALUES('CBA78B43-6A02-4C91-ABCA-1788F7047314', 'BF30E4F3-C33C-4839-8B02-8A32EF93F247')
--INSERT INTO aspnet_UsersInRoles VALUES('EBA9D7FE-D3FC-43DA-982C-2970959B3F55', 'BF30E4F3-C33C-4839-8B02-8A32EF93F247')
--INSERT INTO aspnet_UsersInRoles VALUES('D23FDE64-7612-4AFC-9037-6067720B6960', 'BF30E4F3-C33C-4839-8B02-8A32EF93F247')
--INSERT INTO aspnet_UsersInRoles VALUES('35207E78-29EC-47A7-9641-8B7237D64007', 'BF30E4F3-C33C-4839-8B02-8A32EF93F247')
--INSERT INTO aspnet_UsersInRoles VALUES('9792664F-128A-49C7-960D-8D31B743C16D', 'BF30E4F3-C33C-4839-8B02-8A32EF93F247')
--INSERT INTO aspnet_UsersInRoles VALUES('D5658BEB-E33E-46E5-B07E-CD103F7901F0', 'BF30E4F3-C33C-4839-8B02-8A32EF93F247')
--INSERT INTO aspnet_UsersInRoles VALUES('7F3A7FCB-A23E-4E79-8DB1-E3B1744E5B48', 'BF30E4F3-C33C-4839-8B02-8A32EF93F247')
--INSERT INTO aspnet_UsersInRoles VALUES('943037CB-032E-414C-A6A7-E75C582926EF', 'BF30E4F3-C33C-4839-8B02-8A32EF93F247')
--INSERT INTO aspnet_UsersInRoles VALUES('0F3DDDBA-8960-4BBE-8DB4-F6381C602FF8', 'BF30E4F3-C33C-4839-8B02-8A32EF93F247')
--INSERT INTO aspnet_UsersInRoles VALUES('453FD629-2F51-4B4C-9240-FC7EBD7284DD', 'BF30E4F3-C33C-4839-8B02-8A32EF93F247')