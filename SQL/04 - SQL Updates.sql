create view STC_Site_On_Hand
as
select a.UserId, a.UserName, a.STC_Product_id, a.STC_Product_Name, pu.is_Active, pu.On_Hand_Qty, pu.Capacity 
from (select * from STC_Product p, aspnet_Users u) a 
	left outer join STC_Product_User pu 
		on a.UserId = pu.STC_User_ID and a.STC_Product_id = pu.STC_Product_ID

GO

ALTER TABLE [dbo].[STC_Purchase_Order] ADD STC_Product_id int not null;

ALTER TABLE [dbo].[STC_Purchase_Order]  WITH CHECK ADD  CONSTRAINT [FK_STC_Purchase_Order_Product] FOREIGN KEY([STC_Product_id])
REFERENCES [dbo].[STC_Product] ([STC_Product_id])
GO

ALTER TABLE [dbo].[STC_Purchase_Order] CHECK CONSTRAINT [FK_STC_Purchase_Order_Product]
GO