USE [DSNY_Messaging_Prod]
GO

/****** Object:  View [dbo].[STC_Site_On_Hand]    Script Date: 5/25/2018 8:06:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
drop view [dbo].[STC_Site_On_Hand]
go

create view [dbo].[STC_Site_On_Hand]
as
select a.UserId, a.UserName, a.STC_Product_id, a.STC_Product_Name, pu.is_Active, pu.On_Hand_Qty, pu.Capacity 
from (select p.*, u.* from STC_Product p, aspnet_Users u 
		inner join aspnet_UsersInRoles ur on u.UserId = ur.UserId 
		inner join aspnet_Roles r on ur.RoleId = r.RoleId and r.RoleName = 'STC Site') a 
	left outer join STC_Product_User pu 
		on a.UserId = pu.STC_User_ID and a.STC_Product_id = pu.STC_Product_ID
GO

insert into Measurement (Measurement_Name) values('Tons')

update STC_Product
set Measurement_id = (select Measurement_id from Measurement where Measurement_Name = 'Tons')
where STC_Product_Name = 'Salt'

insert into Measurement (Measurement_Name) values('Yards')

update STC_Product
set Measurement_id = (select Measurement_id from Measurement where Measurement_Name = 'Yards')
where STC_Product_Name = 'Sand'


alter table [STC_Orders] drop constraint [FK_STC_Orders_Borough]
go

alter table [Vendor] add is_Stc bit
go

alter table [Vendor] add is_Fuel bit
go

ALTER TABLE [dbo].[STC_Orders]  DROP  CONSTRAINT [FK_STC_Orders_Vendor]
GO

ALTER TABLE [dbo].[STC_Orders]  DROP COLUMN [Vendor_Id]
GO


ALTER TABLE [dbo].[STC_Purchase_Order]  ADD [Vendor_Id] int
GO


ALTER TABLE [dbo].[STC_Purchase_Order]  WITH CHECK ADD  CONSTRAINT [FK_STC_Purchase_Order_Vendor] FOREIGN KEY([Vendor_Id])
REFERENCES [dbo].[Vendor] ([Vendor_ID])
GO

ALTER TABLE [dbo].[STC_Purchase_Order] CHECK CONSTRAINT [FK_STC_Purchase_Order_Vendor]
GO


insert into Measurement(Measurement_Name) values ('Gallons')
insert into STC_Product(STC_Product_Name, Measurement_id) select 'Calcium', Measurement_id from Measurement where Measurement_Name = 'Gallons'


ALTER TABLE [dbo].[STC_Orders]  ADD [Order_Date] datetime
GO

ALTER TABLE [dbo].[STC_Product]  ADD [LoadSize] int
GO

update [STC_Product]
set LoadSize = 40
where STC_Product_Name = 'Sand'


ALTER TABLE [dbo].[STC_Delivery]  ADD [STC_Product_id] int
GO


ALTER TABLE [dbo].[STC_Delivery]  WITH CHECK ADD  CONSTRAINT [FK_STC_Delivery_Product] FOREIGN KEY([STC_Product_id])
REFERENCES [dbo].[STC_Product] ([STC_Product_id])
GO

ALTER TABLE [dbo].[STC_Delivery] CHECK CONSTRAINT [FK_STC_Delivery_Product]
GO

alter table [dbo].[STC_Transfer] add [STC_Transfer_Date] DateTime
go


drop view [dbo].[STC_Site_On_Hand]
go

create view [dbo].[STC_Site_On_Hand]
as
select a.UserId, a.UserName, a.STC_Product_id, a.STC_Product_Name, a.SortOrder, pu.is_Active, pu.On_Hand_Qty, pu.Capacity 
from (select p.*, u.* from STC_Product p, aspnet_Users u 
		inner join aspnet_UsersInRoles ur on u.UserId = ur.UserId 
		inner join aspnet_Roles r on ur.RoleId = r.RoleId and r.RoleName = 'STC Site') a 
	left outer join STC_Product_User pu 
		on a.UserId = pu.STC_User_ID and a.STC_Product_id = pu.STC_Product_ID
GO



alter table [Zone] add is_Active bit
go




drop view [dbo].[STC_Site_On_Hand]
go

create view [dbo].[STC_Site_On_Hand]
as
select a.UserId, a.UserName, a.STC_Product_id, a.STC_Product_Name, a.SortOrder, a.Measurement_Name, pu.is_Active, pu.On_Hand_Qty, pu.Capacity 
from (select p.*, m.Measurement_Name, u.* from STC_Product p inner join Measurement m on p.Measurement_id = m.Measurement_id, aspnet_Users u 
		inner join aspnet_UsersInRoles ur on u.UserId = ur.UserId 
		inner join aspnet_Roles r on ur.RoleId = r.RoleId and r.RoleName = 'STC Site') a 
	left outer join STC_Product_User pu 
		on a.UserId = pu.STC_User_ID and a.STC_Product_id = pu.STC_Product_ID
GO

CREATE TABLE [dbo].[STC_Purchase_Order_Zone](
	[STC_Purchase_Orders_id] [int] NOT NULL,
	[Zone_Id] [int] NOT NULL,
 CONSTRAINT [PK_STC_Purchase_Order_Zone] PRIMARY KEY CLUSTERED 
(
	[STC_Purchase_Orders_id] ASC,
	[Zone_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[STC_Purchase_Order_Zone]  WITH CHECK ADD  CONSTRAINT [FK_STC_Purchase_Order_Zone_Zone] FOREIGN KEY([Zone_id])
REFERENCES [dbo].[Zone] ([Zone_id])
GO

ALTER TABLE [dbo].[STC_Purchase_Order_Zone] CHECK CONSTRAINT [FK_STC_Purchase_Order_Zone_Zone]
GO

ALTER TABLE [dbo].[STC_Purchase_Order_Zone]  WITH CHECK ADD  CONSTRAINT [FK_STC_Purchase_Order_Zone_STC_Purchase_Order] FOREIGN KEY([STC_Purchase_Orders_id])
REFERENCES [dbo].[STC_Purchase_Order] ([STC_Purchase_Orders_id])
GO

ALTER TABLE [dbo].[STC_Purchase_Order_Zone] CHECK CONSTRAINT [FK_STC_Purchase_Order_Zone_STC_Purchase_Order]
GO

insert into STC_Purchase_Order_Zone
select STC_Purchase_Orders_id, Zone_id from STC_Purchase_Order
GO

ALTER TABLE [dbo].[STC_Purchase_Order] DROP CONSTRAINT [FK_STC_Purchase_Order_Zone]
GO

ALTER TABLE [dbo].[STC_Purchase_Order] DROP COLUMN [Zone_id]
GO

update Zone
set is_Active = 1
where is_Active is null
go


ALTER TABLE [dbo].[STC_Purchase_Order]  ADD [Current_Delivery_Qty_Amount] int
GO