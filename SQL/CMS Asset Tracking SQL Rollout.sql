/***********STC**********************/

/****** Object:  Table [dbo].[Borough]    Script Date: 07/02/2018 07:05:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Borough](
	[Borough_id] [int] IDENTITY(1,1) NOT NULL,
	[Borough_cd] [char](2) NULL,
	[Borough_Name] [varchar](30) NULL,
 CONSTRAINT [PK_Borough] PRIMARY KEY CLUSTERED
(
	[Borough_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[Tool_Type]    Script Date: 07/02/2018 07:41:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Tool_Type](
	[Tool_Type_id] [int] IDENTITY(1,1) NOT NULL,
	[Tool_Type_Name] [varchar](50) NULL,
	[is_PowerTool] [bit] NULL,
	[is_Active] [bit] NULL,
	[Measurement_id] [int] NULL,
	[Quantity_Per_Measurement] [int] NULL,
 CONSTRAINT [PK_Tool_Type] PRIMARY KEY CLUSTERED
(
	[Tool_Type_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[Make]    Script Date: 07/02/2018 07:06:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO





/****** Object:  Table [dbo].[Measurement]    Script Date: 07/02/2018 07:07:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Measurement](
	[Measurement_id] [int] IDENTITY(1,1) NOT NULL,
	[Measurement_Name] [varchar](50) NULL,
	[is_active] [bit] NULL,
	[is_STC] [bit] NULL,
	[is_Tool] [bit] NULL,
 CONSTRAINT [PK_Measurement] PRIMARY KEY CLUSTERED
(
	[Measurement_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


GO


/****** Object:  Table [dbo].[Tool_User]    Script Date: 7/4/2018 8:19:29 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Tool_User](
	[Tool_User_id] [int] IDENTITY(1,1) NOT NULL,
	[Tool_Type_id] [int] NULL,
	[Userid] [uniqueidentifier] NULL,
	[Site_Quota] [int] NULL,
	[is_Active] [bit] NULL,
 CONSTRAINT [PK_Tool_User] PRIMARY KEY CLUSTERED
(
	[Tool_User_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Tool_User]  WITH CHECK ADD  CONSTRAINT [FK_Tool_User_aspnet_Users] FOREIGN KEY([Userid])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[Tool_User] CHECK CONSTRAINT [FK_Tool_User_aspnet_Users]
GO

ALTER TABLE [dbo].[Tool_User]  WITH CHECK ADD  CONSTRAINT [FK_Tool_User_Tool_Type] FOREIGN KEY([Tool_Type_id])
REFERENCES [dbo].[Tool_Type] ([Tool_Type_id])
GO

ALTER TABLE [dbo].[Tool_User] CHECK CONSTRAINT [FK_Tool_User_Tool_Type]
GO





/****** Object:  Table [dbo].[Model]    Script Date: 07/02/2018 07:06:29 ******/

CREATE TABLE [dbo].[Make](
	[Make_id] [int] IDENTITY(1,1) NOT NULL,
	[Tool_Type_id] [int] NOT NULL,
	[Make_Name] [varchar](50) NOT NULL,
	[is_Active] [bit] NULL,
 CONSTRAINT [PK_Make] PRIMARY KEY CLUSTERED
(
	[Make_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Make]  WITH CHECK ADD  CONSTRAINT [FK_Make_Tool_Type] FOREIGN KEY([Tool_Type_id])
REFERENCES [dbo].[Tool_Type] ([Tool_Type_id])
GO

ALTER TABLE [dbo].[Make] CHECK CONSTRAINT [FK_Make_Tool_Type]
GO


/****** Object:  Table [dbo].[Model]    Script Date: 07/02/2018 07:06:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Model](
	[Model_id] [int] IDENTITY(1,1) NOT NULL,
	[Make_Id] [int] NULL,
	[Model_Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Model] PRIMARY KEY CLUSTERED
(
	[Model_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Model]  WITH CHECK ADD  CONSTRAINT [FK_Model_Make] FOREIGN KEY([Make_id])
REFERENCES [dbo].[Make] ([Make_id])
GO

ALTER TABLE [dbo].[Model] CHECK CONSTRAINT [FK_Model_Make]
GO

SET ANSI_PADDING OFF
GO




/****** Object:  Table [dbo].[Power_Tool_Inventory]    Script Date: 07/02/2018 07:09:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Power_Tool_Inventory](
	[Power_Tool_Inventory_Id] [int] IDENTITY(1,1) NOT NULL,
	[Tool_Type_id] [int] NULL,
	[Make_id] [int] NULL,
	[Model_id] [int] NULL,
	[UserId] [uniqueidentifier] NULL,
	[Serial_Number] [varchar](50) NULL,
	[DSNY_Number] [varchar](50) NULL,
	[is_Active] [bit] NULL,
 CONSTRAINT [PK_Power_Tool_Inventory] PRIMARY KEY CLUSTERED
(
	[Power_Tool_Inventory_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Power_Tool_Inventory]  WITH CHECK ADD  CONSTRAINT [FK_Power_Tool_Inventory_aspnet_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[Power_Tool_Inventory] CHECK CONSTRAINT [FK_Power_Tool_Inventory_aspnet_Users]
GO

ALTER TABLE [dbo].[Power_Tool_Inventory]  WITH CHECK ADD  CONSTRAINT [FK_Power_Tool_Inventory_Make] FOREIGN KEY([Make_id])
REFERENCES [dbo].[Make] ([Make_id])
GO

ALTER TABLE [dbo].[Power_Tool_Inventory] CHECK CONSTRAINT [FK_Power_Tool_Inventory_Make]
GO

ALTER TABLE [dbo].[Power_Tool_Inventory]  WITH CHECK ADD  CONSTRAINT [FK_Power_Tool_Inventory_Model] FOREIGN KEY([Model_id])
REFERENCES [dbo].[Model] ([Model_id])
GO

ALTER TABLE [dbo].[Power_Tool_Inventory] CHECK CONSTRAINT [FK_Power_Tool_Inventory_Model]
GO

ALTER TABLE [dbo].[Power_Tool_Inventory]  WITH CHECK ADD  CONSTRAINT [FK_Power_Tool_Inventory_Tool_Type] FOREIGN KEY([Tool_Type_id])
REFERENCES [dbo].[Tool_Type] ([Tool_Type_id])
GO

ALTER TABLE [dbo].[Power_Tool_Inventory] CHECK CONSTRAINT [FK_Power_Tool_Inventory_Tool_Type]
GO


GO

/****** Object:  Table [dbo].[Standard_Tools_Inventory]    Script Date: 07/02/2018 07:11:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Standard_Tools_Inventory](
	[Standard_Tools_Inventory_id] [int] IDENTITY(1,1) NOT NULL,
	[Current_On_Hand] [int] NULL,
	[Last_Update_Date] [datetime] NULL,
	[Tool_User_id] [int] NOT NULL,
 CONSTRAINT [PK_Standard_Tools_Inventory] PRIMARY KEY CLUSTERED
(
	[Standard_Tools_Inventory_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Standard_Tools_Inventory]  WITH CHECK ADD  CONSTRAINT [FK_Standard_Tools_Inventory_Tool_User] FOREIGN KEY([Tool_User_id])
REFERENCES [dbo].[Tool_User] ([Tool_User_id])
GO

ALTER TABLE [dbo].[Standard_Tools_Inventory] CHECK CONSTRAINT [FK_Standard_Tools_Inventory_Tool_User]
GO

/****** Object:  Table [dbo].[STC_Agency]    Script Date: 07/02/2018 07:11:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[STC_Agency](
	[STC_Agency_id] [int] IDENTITY(1,1) NOT NULL,
	[Agency_cd] [varchar](10) NULL,
	[Agency_Name] [varchar](50) NULL,
	[Agency_Email] [varchar](200) NULL,
	[Agency_Address] [varchar](200) NULL,
	[Agency_Phone] [varchar](50) NULL,
	[Is_Billable] [bit] NOT NULL,
	[Is_Active] [bit] NULL,
 CONSTRAINT [PK_STC_Agency] PRIMARY KEY CLUSTERED
(
	[STC_Agency_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[STC_Agency] ADD  CONSTRAINT [DF_STC_Agency_Is_Active]  DEFAULT ((1)) FOR [Is_Active]
GO


/****** Object:  Table [dbo].[Zone]    Script Date: 07/02/2018 07:12:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Zone](
	[Zone_id] [int] IDENTITY(1,1) NOT NULL,
	[Zone_Name] [varchar](50) NULL,
	[is_Active] [bit] NULL,
 CONSTRAINT [PK_Zone] PRIMARY KEY CLUSTERED
(
	[Zone_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[Zone_User]    Script Date: 07/02/2018 07:13:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Zone_User](
	[Zone_Id] [int] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Zone_User] PRIMARY KEY CLUSTERED
(
	[Zone_Id] ASC,
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Zone_User]  WITH CHECK ADD  CONSTRAINT [FK_Zone_User_aspnet_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[Zone_User] CHECK CONSTRAINT [FK_Zone_User_aspnet_Users]
GO

ALTER TABLE [dbo].[Zone_User]  WITH CHECK ADD  CONSTRAINT [FK_Zone_User_Zone] FOREIGN KEY([Zone_Id])
REFERENCES [dbo].[Zone] ([Zone_id])
GO

ALTER TABLE [dbo].[Zone_User] CHECK CONSTRAINT [FK_Zone_User_Zone]
GO

/****UPDATE: Vendor****/
alter table [Vendor] add is_Stc bit
go

alter table [Vendor] add is_Fuel bit
go

/****** Object:  Table [dbo].[Truck_Type]    Script Date: 07/02/2018 07:14:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Truck_Type](
	[STC_Truck_Type_id] [int] IDENTITY(1,1) NOT NULL,
	[Truck_Type_Name] [varchar](50) NULL,
	[Truck_Capacity] [int] NULL,
	[is_active] [bit] NULL,
 CONSTRAINT [PK_Truck_Type] PRIMARY KEY CLUSTERED
(
	[STC_Truck_Type_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[STC_Product]    Script Date: 07/02/2018 07:16:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[STC_Product](
	[STC_Product_id] [int] IDENTITY(1,1) NOT NULL,
	[STC_Product_Name] [varchar](30) NULL,
	[SortOrder] [int] NULL,
	[is_Active] [bit] NULL,
	[Measurement_id] [int] NULL,
	[LoadSize] [int] NULL,
 CONSTRAINT [PK_STC_Product] PRIMARY KEY CLUSTERED
(
	[STC_Product_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[STC_Product]  WITH CHECK ADD  CONSTRAINT [FK_STC_Product_Measurement] FOREIGN KEY([Measurement_id])
REFERENCES [dbo].[Measurement] ([Measurement_id])
GO

ALTER TABLE [dbo].[STC_Product] CHECK CONSTRAINT [FK_STC_Product_Measurement]
GO

/****** Object:  Table [dbo].[STC_Product_User]    Script Date: 07/02/2018 07:16:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[STC_Product_User](
	[STC_Product_ID] [int] NOT NULL,
	[STC_User_ID] [uniqueidentifier] NOT NULL,
	[Capacity] [int] NULL,
	[On_Hand_Qty] [decimal](18, 2) NULL,
	[is_Active] [bit] NULL,
 CONSTRAINT [PK_STC_Product_User] PRIMARY KEY CLUSTERED
(
	[STC_Product_ID] ASC,
	[STC_User_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[STC_Product_User]  WITH CHECK ADD  CONSTRAINT [FK_STC_Product_User_aspnet_Users] FOREIGN KEY([STC_User_ID])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[STC_Product_User] CHECK CONSTRAINT [FK_STC_Product_User_aspnet_Users]
GO

ALTER TABLE [dbo].[STC_Product_User]  WITH CHECK ADD  CONSTRAINT [FK_STC_Product_User_STC_Product] FOREIGN KEY([STC_Product_ID])
REFERENCES [dbo].[STC_Product] ([STC_Product_id])
GO

ALTER TABLE [dbo].[STC_Product_User] CHECK CONSTRAINT [FK_STC_Product_User_STC_Product]
GO

/****** Object:  Table [dbo].[STC_Pickup]    Script Date: 07/02/2018 07:17:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[STC_Pickup](
	[STC_Pickup_id] [int] IDENTITY(1,1) NOT NULL,
	[Userid] [uniqueidentifier] NULL,
	[STC_Product_id] [int] NULL,
	[Agency_Id] [int] NOT NULL,
	[Truck_Number] [varchar](50) NULL,
	[Plate_Number] [varchar](15) NULL,
	[Pickup_Date] [datetime] NULL,
	[Qty] [decimal](18, 2) NULL,
	[District_Super_Name] [varchar](50) NULL,
	[District_Super_Badge] [varchar](20) NULL,
	[Boro_Super_Name] [varchar](50) NULL,
	[Boro_Super_Badge] [varchar](20) NULL,
	[is_finalized] [bit] NULL,
 CONSTRAINT [PK_STC_Pickup] PRIMARY KEY CLUSTERED
(
	[STC_Pickup_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[STC_Pickup]  WITH CHECK ADD  CONSTRAINT [FK_STC_Pickup_aspnet_Users] FOREIGN KEY([Userid])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[STC_Pickup] CHECK CONSTRAINT [FK_STC_Pickup_aspnet_Users]
GO

ALTER TABLE [dbo].[STC_Pickup]  WITH CHECK ADD  CONSTRAINT [FK_STC_Pickup_STC_Agency] FOREIGN KEY([Agency_Id])
REFERENCES [dbo].[STC_Agency] ([STC_Agency_id])
GO

ALTER TABLE [dbo].[STC_Pickup] CHECK CONSTRAINT [FK_STC_Pickup_STC_Agency]
GO

ALTER TABLE [dbo].[STC_Pickup]  WITH CHECK ADD  CONSTRAINT [FK_STC_Pickup_STC_Product] FOREIGN KEY([STC_Product_id])
REFERENCES [dbo].[STC_Product] ([STC_Product_id])
GO

ALTER TABLE [dbo].[STC_Pickup] CHECK CONSTRAINT [FK_STC_Pickup_STC_Product]
GO

/****** Object:  Table [dbo].[STC_Purchase_Order]    Script Date: 07/02/2018 07:19:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[STC_Purchase_Order](
	[STC_Purchase_Orders_id] [int] IDENTITY(1,1) NOT NULL,
	[Purchase_Order_Num] [varchar](50) NULL,
	[Order_Amount] [decimal](18, 2) NULL,
	[Order_Qty_Amount] [decimal](18, 2) NULL,
	[Current_Order_Qty_Amount] [decimal](18, 2) NULL,
	[PO_Start_Date] [datetime] NULL,
	[PO_Complete_Date] [datetime] NULL,
	[STC_Product_id] [int] NOT NULL,
	[Vendor_Id] [int] NULL,
	[Current_Delivery_Qty_Amount] [decimal](18, 2) NULL,
 CONSTRAINT [PK_STC_Purchase_Order] PRIMARY KEY CLUSTERED
(
	[STC_Purchase_Orders_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[STC_Purchase_Order]  WITH CHECK ADD  CONSTRAINT [FK_STC_Purchase_Order_Product] FOREIGN KEY([STC_Product_id])
REFERENCES [dbo].[STC_Product] ([STC_Product_id])
GO

ALTER TABLE [dbo].[STC_Purchase_Order] CHECK CONSTRAINT [FK_STC_Purchase_Order_Product]
GO

ALTER TABLE [dbo].[STC_Purchase_Order]  WITH CHECK ADD  CONSTRAINT [FK_STC_Purchase_Order_Vendor] FOREIGN KEY([Vendor_Id])
REFERENCES [dbo].[Vendor] ([Vendor_ID])
GO

ALTER TABLE [dbo].[STC_Purchase_Order] CHECK CONSTRAINT [FK_STC_Purchase_Order_Vendor]
GO


/****** Object:  Table [dbo].[STC_Purchase_Order_Zone]    Script Date: 07/02/2018 07:19:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[STC_Purchase_Order_Zone](
	[STC_Purchase_Orders_id] [int] NOT NULL,
	[Zone_Id] [int] NOT NULL,
 CONSTRAINT [PK_STC_Purchase_Order_Zone] PRIMARY KEY CLUSTERED
(
	[STC_Purchase_Orders_id] ASC,
	[Zone_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[STC_Purchase_Order_Zone]  WITH CHECK ADD  CONSTRAINT [FK_STC_Purchase_Order_Zone_STC_Purchase_Order] FOREIGN KEY([STC_Purchase_Orders_id])
REFERENCES [dbo].[STC_Purchase_Order] ([STC_Purchase_Orders_id])
GO

ALTER TABLE [dbo].[STC_Purchase_Order_Zone] CHECK CONSTRAINT [FK_STC_Purchase_Order_Zone_STC_Purchase_Order]
GO

ALTER TABLE [dbo].[STC_Purchase_Order_Zone]  WITH CHECK ADD  CONSTRAINT [FK_STC_Purchase_Order_Zone_Zone] FOREIGN KEY([Zone_Id])
REFERENCES [dbo].[Zone] ([Zone_id])
GO

ALTER TABLE [dbo].[STC_Purchase_Order_Zone] CHECK CONSTRAINT [FK_STC_Purchase_Order_Zone_Zone]
GO

/****** Object:  Table [dbo].[STC_Transfer]    Script Date: 07/02/2018 07:20:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[STC_Transfer](
	[STC_Transfer_Id] [int] IDENTITY(1,1) NOT NULL,
	[STC_Product_id] [int] NOT NULL,
	[From_Userid] [uniqueidentifier] NOT NULL,
	[To_Userid] [uniqueidentifier] NOT NULL,
	[STC_Truck_Type_id] [int] NOT NULL,
	[Loads] [int] NOT NULL,
	[Qty] [decimal](18, 2) NOT NULL,
	[Snow_Super_Name] [varchar](50) NULL,
	[Snow_Super_Badge] [varchar](20) NULL,
	[San_Super_Name] [varchar](50) NULL,
	[San_Super_Badge] [varchar](50) NULL,
	[is_finalize] [bit] NULL,
	[STC_Transfer_Date] [datetime] NULL,
 CONSTRAINT [PK_STC_Transfer] PRIMARY KEY CLUSTERED
(
	[STC_Transfer_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[STC_Transfer]  WITH CHECK ADD  CONSTRAINT [FK_STC_Transfer_aspnet_fromUsers] FOREIGN KEY([To_Userid])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[STC_Transfer] CHECK CONSTRAINT [FK_STC_Transfer_aspnet_fromUsers]
GO

ALTER TABLE [dbo].[STC_Transfer]  WITH CHECK ADD  CONSTRAINT [FK_STC_Transfer_aspnet_toUsers] FOREIGN KEY([From_Userid])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[STC_Transfer] CHECK CONSTRAINT [FK_STC_Transfer_aspnet_toUsers]
GO

ALTER TABLE [dbo].[STC_Transfer]  WITH CHECK ADD  CONSTRAINT [FK_STC_Transfer_STC_Product] FOREIGN KEY([STC_Product_id])
REFERENCES [dbo].[STC_Product] ([STC_Product_id])
GO

ALTER TABLE [dbo].[STC_Transfer] CHECK CONSTRAINT [FK_STC_Transfer_STC_Product]
GO

ALTER TABLE [dbo].[STC_Transfer]  WITH CHECK ADD  CONSTRAINT [FK_STC_Transfer_Truck_Type] FOREIGN KEY([STC_Truck_Type_id])
REFERENCES [dbo].[Truck_Type] ([STC_Truck_Type_id])
GO

ALTER TABLE [dbo].[STC_Transfer] CHECK CONSTRAINT [FK_STC_Transfer_Truck_Type]
GO

/****** Object:  Table [dbo].[STC_Orders]    Script Date: 07/02/2018 07:20:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[STC_Orders](
	[STC_Orders_id] [int] IDENTITY(1,1) NOT NULL,
	[Zone_Id] [int] NOT NULL,
	[Borough_Id] [int] NOT NULL,
	[Userid] [uniqueidentifier] NOT NULL,
	[STC_Product_id] [int] NOT NULL,
	[STC_Purchase_Order_Id] [int] NOT NULL,
	[Order_Qty] [decimal](18, 2) NULL,
	[Order_Date] [datetime] NULL,
 CONSTRAINT [PK_STC_Orders] PRIMARY KEY CLUSTERED
(
	[STC_Orders_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[STC_Orders]  WITH CHECK ADD  CONSTRAINT [FK_STC_Orders_aspnet_Users] FOREIGN KEY([Userid])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[STC_Orders] CHECK CONSTRAINT [FK_STC_Orders_aspnet_Users]
GO

ALTER TABLE [dbo].[STC_Orders]  WITH CHECK ADD  CONSTRAINT [FK_STC_Orders_STC_Product] FOREIGN KEY([STC_Product_id])
REFERENCES [dbo].[STC_Product] ([STC_Product_id])
GO

ALTER TABLE [dbo].[STC_Orders] CHECK CONSTRAINT [FK_STC_Orders_STC_Product]
GO

ALTER TABLE [dbo].[STC_Orders]  WITH CHECK ADD  CONSTRAINT [FK_STC_Orders_STC_Purchase_Order] FOREIGN KEY([STC_Purchase_Order_Id])
REFERENCES [dbo].[STC_Purchase_Order] ([STC_Purchase_Orders_id])
GO

ALTER TABLE [dbo].[STC_Orders] CHECK CONSTRAINT [FK_STC_Orders_STC_Purchase_Order]
GO

ALTER TABLE [dbo].[STC_Orders]  WITH CHECK ADD  CONSTRAINT [FK_STC_Orders_Zone] FOREIGN KEY([Zone_Id])
REFERENCES [dbo].[Zone] ([Zone_id])
GO

ALTER TABLE [dbo].[STC_Orders] CHECK CONSTRAINT [FK_STC_Orders_Zone]
GO

/****** Object:  Table [dbo].[STC_Delivery]    Script Date: 07/02/2018 07:21:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[STC_Delivery](
	[STC_Delivery_id] [int] IDENTITY(1,1) NOT NULL,
	[STC_Purchase_Order_id] [int] NOT NULL,
	[Vendor_id] [int] NOT NULL,
	[Userid] [uniqueidentifier] NOT NULL,
	[Truck_Number] [varchar](20) NULL,
	[Plate_Number] [varchar](15) NULL,
	[WM_Qty] [decimal](18, 2) NULL,
	[WM_Leave_Date] [datetime] NULL,
	[WM_Name] [varchar](50) NULL,
	[WM_Badge_Number] [varchar](20) NULL,
	[Boro_Qty] [decimal](18, 2) NULL,
	[Boro_Recieved_Date] [datetime] NULL,
	[Recieving_Super_Name] [varchar](50) NULL,
	[Recieving_Super_Badge] [varchar](20) NULL,
	[Boro_Super_Name] [varchar](50) NULL,
	[Boro_Super_Badge] [varchar](20) NULL,
	[is_Finalize] [bit] NULL,
	[STC_Product_id] [int] NULL,
 CONSTRAINT [PK_STC_Delivery] PRIMARY KEY CLUSTERED
(
	[STC_Delivery_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[STC_Delivery]  WITH CHECK ADD  CONSTRAINT [FK_STC_Delivery_aspnet_Users] FOREIGN KEY([Userid])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[STC_Delivery] CHECK CONSTRAINT [FK_STC_Delivery_aspnet_Users]
GO

ALTER TABLE [dbo].[STC_Delivery]  WITH CHECK ADD  CONSTRAINT [FK_STC_Delivery_Product] FOREIGN KEY([STC_Product_id])
REFERENCES [dbo].[STC_Product] ([STC_Product_id])
GO

ALTER TABLE [dbo].[STC_Delivery] CHECK CONSTRAINT [FK_STC_Delivery_Product]
GO

ALTER TABLE [dbo].[STC_Delivery]  WITH CHECK ADD  CONSTRAINT [FK_STC_Delivery_STC_Purchase_Order] FOREIGN KEY([STC_Purchase_Order_id])
REFERENCES [dbo].[STC_Purchase_Order] ([STC_Purchase_Orders_id])
GO

ALTER TABLE [dbo].[STC_Delivery] CHECK CONSTRAINT [FK_STC_Delivery_STC_Purchase_Order]
GO

ALTER TABLE [dbo].[STC_Delivery]  WITH CHECK ADD  CONSTRAINT [FK_STC_Delivery_Vendor] FOREIGN KEY([Vendor_id])
REFERENCES [dbo].[Vendor] ([Vendor_ID])
GO

ALTER TABLE [dbo].[STC_Delivery] CHECK CONSTRAINT [FK_STC_Delivery_Vendor]
GO

ALTER TABLE dbo.STC_Delivery ADD
	Ticket_Number varchar(50) NULL
GO

ALTER TABLE dbo.STC_Delivery ADD
	Driver_Name varchar(50) NULL
GO


/****** Object:  Table [dbo].[STC_Needs_Request]    Script Date: 07/02/2018 07:23:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[STC_Needs_Request](
	[STC_Needs_Request_id] [int] IDENTITY(1,1) NOT NULL,
	[Userid] [uniqueidentifier] NULL,
	[STC_Product_id] [int] NULL,
	[Request_Qty] [decimal](18, 2) NULL,
	[Boro_Super_Name] [varchar](50) NULL,
	[Boro_Super_Badge] [varchar](15) NULL,
	[Request_Date] [datetime] NULL,
	[Confirm_Date] [datetime] NULL,
 CONSTRAINT [PK_STC_Needs_Request] PRIMARY KEY CLUSTERED
(
	[STC_Needs_Request_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[STC_Needs_Request]  WITH CHECK ADD  CONSTRAINT [FK_STC_Needs_Request_aspnet_Users] FOREIGN KEY([Userid])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[STC_Needs_Request] CHECK CONSTRAINT [FK_STC_Needs_Request_aspnet_Users]
GO

ALTER TABLE [dbo].[STC_Needs_Request]  WITH CHECK ADD  CONSTRAINT [FK_STC_Needs_Request_STC_Product] FOREIGN KEY([STC_Product_id])
REFERENCES [dbo].[STC_Product] ([STC_Product_id])
GO

ALTER TABLE [dbo].[STC_Needs_Request] CHECK CONSTRAINT [FK_STC_Needs_Request_STC_Product]
GO

/**********Garage Supplies****************/

/****** Object:  Table [dbo].[Workflow_Action]    Script Date: 07/02/2018 07:42:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Workflow_Action](
	[Workflow_Action_id] [int] IDENTITY(1,1) NOT NULL,
	[Workflow_cd] [varchar](10) NULL,
	[Workflow_Action_Name] [varchar](200) NULL,
	[is_End_Workflow] [bit] NULL,
 CONSTRAINT [PK_Workflow_Action] PRIMARY KEY CLUSTERED
(
	[Workflow_Action_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Workflow_Action_flow]    Script Date: 07/02/2018 07:42:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Workflow_Action_flow](
	[Workflow_Action_Flow_id] [int] IDENTITY(1,1) NOT NULL,
	[Workflow_id] [int] NOT NULL,
	[Next_Workflow_id] [int] NOT NULL,
 CONSTRAINT [PK_Workflow_Action_flow] PRIMARY KEY CLUSTERED
(
	[Workflow_Action_Flow_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Workflow_Action_flow]  WITH CHECK ADD  CONSTRAINT [FK_Workflow_Action_flow_Workflow_Action] FOREIGN KEY([Workflow_id])
REFERENCES [dbo].[Workflow_Action] ([Workflow_Action_id])
GO

ALTER TABLE [dbo].[Workflow_Action_flow] CHECK CONSTRAINT [FK_Workflow_Action_flow_Workflow_Action]
GO

ALTER TABLE [dbo].[Workflow_Action_flow]  WITH CHECK ADD  CONSTRAINT [FK_Workflow_Action_flow_Workflow_Action1] FOREIGN KEY([Next_Workflow_id])
REFERENCES [dbo].[Workflow_Action] ([Workflow_Action_id])
GO

ALTER TABLE [dbo].[Workflow_Action_flow] CHECK CONSTRAINT [FK_Workflow_Action_flow_Workflow_Action1]
GO


/****** Object:  Table [dbo].[Workflow_Action_Role]    Script Date: 07/02/2018 07:42:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Workflow_Action_Role](
	[Workflow_Action_Role_id] [int] IDENTITY(1,1) NOT NULL,
	[Workflow_Action_id] [int] NULL,
	[Role_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Workflow_Action_Role] PRIMARY KEY CLUSTERED
(
	[Workflow_Action_Role_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Workflow_Action_Role]  WITH CHECK ADD  CONSTRAINT [FK_Workflow_Action_Role_aspnet_Roles] FOREIGN KEY([Role_id])
REFERENCES [dbo].[aspnet_Roles] ([RoleId])
GO

ALTER TABLE [dbo].[Workflow_Action_Role] CHECK CONSTRAINT [FK_Workflow_Action_Role_aspnet_Roles]
GO

ALTER TABLE [dbo].[Workflow_Action_Role]  WITH CHECK ADD  CONSTRAINT [FK_Workflow_Action_Role_Workflow_Action] FOREIGN KEY([Workflow_Action_id])
REFERENCES [dbo].[Workflow_Action] ([Workflow_Action_id])
GO

ALTER TABLE [dbo].[Workflow_Action_Role] CHECK CONSTRAINT [FK_Workflow_Action_Role_Workflow_Action]
GO


/****** Object:  Table [dbo].[Workflow_Tracking]    Script Date: 07/02/2018 07:43:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Workflow_Tracking](
	[Workflow_Tracking_id] [int] IDENTITY(1,1) NOT NULL,
	[Workflow_Action_id] [int] NOT NULL,
	[Userid] [uniqueidentifier] NOT NULL,
	[Comment] [varchar](200) NULL,
	[Workflow_Datetime] [datetime] NOT NULL,
	[Table_Association] [varchar](50) NULL,
	[Table_Id] [int] NULL,
 CONSTRAINT [PK_Workflow_Tracking] PRIMARY KEY CLUSTERED
(
	[Workflow_Tracking_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Workflow_Tracking]  WITH CHECK ADD  CONSTRAINT [FK_Workflow_Tracking_Workflow_Action] FOREIGN KEY([Workflow_Action_id])
REFERENCES [dbo].[Workflow_Action] ([Workflow_Action_id])
GO

ALTER TABLE [dbo].[Workflow_Tracking] CHECK CONSTRAINT [FK_Workflow_Tracking_Workflow_Action]
GO

/****** Object:  Table [dbo].[Standard_Tools_Ledger]    Script Date: 07/02/2018 07:44:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Standard_Tools_Ledger](
	[Standard_Tools_Ledger_id] [int] IDENTITY(1,1) NOT NULL,
	[Standard_Tools_Inventory_id] [int] NULL,
	[Action_Id] [int] NULL,
	[Qty] [int] NULL,
	[InsertDateTime] [datetime] NULL,
	[InsertUserid] [uniqueidentifier] NULL,
	[Send_Userid] [uniqueidentifier] NULL,
	[Comment] [varchar](200) NULL,
 CONSTRAINT [PK_Standard_Tools_Ledger] PRIMARY KEY CLUSTERED
(
	[Standard_Tools_Ledger_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Standard_Tools_Ledger]  WITH CHECK ADD  CONSTRAINT [FK_Standard_Tools_Ledger_aspnet_Users] FOREIGN KEY([InsertUserid])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[Standard_Tools_Ledger] CHECK CONSTRAINT [FK_Standard_Tools_Ledger_aspnet_Users]
GO

ALTER TABLE [dbo].[Standard_Tools_Ledger]  WITH NOCHECK ADD  CONSTRAINT [FK_Standard_Tools_Ledger_aspnet_Users1] FOREIGN KEY([Send_Userid])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[Standard_Tools_Ledger] NOCHECK CONSTRAINT [FK_Standard_Tools_Ledger_aspnet_Users1]
GO

ALTER TABLE [dbo].[Standard_Tools_Ledger]  WITH CHECK ADD  CONSTRAINT [FK_Standard_Tools_Ledger_Standard_Tools_Inventory] FOREIGN KEY([Standard_Tools_Inventory_id])
REFERENCES [dbo].[Standard_Tools_Inventory] ([Standard_Tools_Inventory_id])
GO

ALTER TABLE [dbo].[Standard_Tools_Ledger] CHECK CONSTRAINT [FK_Standard_Tools_Ledger_Standard_Tools_Inventory]
GO

ALTER TABLE [dbo].[Standard_Tools_Ledger]  WITH CHECK ADD  CONSTRAINT [FK_Standard_Tools_Ledger_Workflow_Action] FOREIGN KEY([Action_Id])
REFERENCES [dbo].[Workflow_Action] ([Workflow_Action_id])
GO

ALTER TABLE [dbo].[Standard_Tools_Ledger] CHECK CONSTRAINT [FK_Standard_Tools_Ledger_Workflow_Action]
GO

/****** Object:  Table [dbo].[Tool_Inventory]    Script Date: 07/02/2018 07:51:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Tool_Inventory](
	[Tool_Inventory_id] [int] IDENTITY(1,1) NOT NULL,
	[Inventory_Date] [datetime] NULL,
	[Inventory_Instruction] [varchar](2000) NULL,
 CONSTRAINT [PK_Tool_Inventory] PRIMARY KEY CLUSTERED
(
	[Tool_Inventory_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Tool_Inventory_PT]    Script Date: 07/02/2018 07:51:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Tool_Inventory_PT](
	[Tool_Inventory_PT_id] [int] IDENTITY(1,1) NOT NULL,
	[Power_Tool_Inventory_id] [int] NULL,
	[Tool_Inventory_id] [int] NULL,
	[is_Active] [bit] NULL,
	[is_Down] [bit] NULL,
	[is_Stolen] [bit] NULL,
 CONSTRAINT [PK_Tool_Inventory_PT] PRIMARY KEY CLUSTERED
(
	[Tool_Inventory_PT_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Tool_Inventory_PT]  WITH CHECK ADD  CONSTRAINT [FK_Tool_Inventory_PT_Power_Tool_Inventory] FOREIGN KEY([Power_Tool_Inventory_id])
REFERENCES [dbo].[Power_Tool_Inventory] ([Power_Tool_Inventory_Id])
GO

ALTER TABLE [dbo].[Tool_Inventory_PT] CHECK CONSTRAINT [FK_Tool_Inventory_PT_Power_Tool_Inventory]
GO

ALTER TABLE [dbo].[Tool_Inventory_PT]  WITH CHECK ADD  CONSTRAINT [FK_Tool_Inventory_PT_Tool_Inventory] FOREIGN KEY([Tool_Inventory_id])
REFERENCES [dbo].[Tool_Inventory] ([Tool_Inventory_id])
GO

ALTER TABLE [dbo].[Tool_Inventory_PT] CHECK CONSTRAINT [FK_Tool_Inventory_PT_Tool_Inventory]
GO

/****** Object:  Table [dbo].[Tool_Inventory_Std]    Script Date: 07/02/2018 07:52:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Tool_Inventory_Std](
	[Tool_Inventory_Std_Id] [int] IDENTITY(1,1) NOT NULL,
	[Tool_Inventory_id] [int] NULL,
	[Tool_User_Id] [int] NULL,
	[Damaged_Qty] [int] NULL,
	[Lost_Qty] [int] NULL,
	[On_Hand_Qty] [int] NULL,
	[Issue_Qty] [int] NULL,
	[Boro_Approved_Qty] [int] NULL,
	[Boro_Shipped_Qty] [int] NULL,
 CONSTRAINT [PK_Tool_Inventory_Std1] PRIMARY KEY CLUSTERED
(
	[Tool_Inventory_Std_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Tool_Inventory_Std]  WITH CHECK ADD  CONSTRAINT [FK_Tool_Inventory_Std_Tool_Inventory] FOREIGN KEY([Tool_Inventory_id])
REFERENCES [dbo].[Tool_Inventory] ([Tool_Inventory_id])
GO

ALTER TABLE [dbo].[Tool_Inventory_Std] CHECK CONSTRAINT [FK_Tool_Inventory_Std_Tool_Inventory]
GO

ALTER TABLE [dbo].[Tool_Inventory_Std]  WITH CHECK ADD  CONSTRAINT [FK_Tool_Inventory_Std_Tool_User] FOREIGN KEY([Tool_User_Id])
REFERENCES [dbo].[Tool_User] ([Tool_User_id])
GO

ALTER TABLE [dbo].[Tool_Inventory_Std] CHECK CONSTRAINT [FK_Tool_Inventory_Std_Tool_User]
GO

/****** Object:  Table [dbo].[Tool_Inventory_WH]    Script Date: 07/02/2018 07:52:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Tool_Inventory_WH](
	[Userid] [uniqueidentifier] NULL,
	[Requested_Qty] [int] NULL,
	[Approved_Qty] [int] NULL,
	[Approved_Datetime] [datetime] NULL,
	[Tool_Inventory_WH_id] [int] IDENTITY(1,1) NOT NULL,
	[Tool_Inventory_id] [int] NOT NULL,
	[Tool_Type_id] [int] NULL,
PRIMARY KEY CLUSTERED
(
	[Tool_Inventory_WH_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Tool_Inventory_WH]  WITH CHECK ADD  CONSTRAINT [FK_Tool_Inventory_WH_aspnet_Users] FOREIGN KEY([Userid])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[Tool_Inventory_WH] CHECK CONSTRAINT [FK_Tool_Inventory_WH_aspnet_Users]
GO

ALTER TABLE [dbo].[Tool_Inventory_WH]  WITH CHECK ADD  CONSTRAINT [FK_Tool_Inventory_WH_Tool_Inventory] FOREIGN KEY([Tool_Inventory_id])
REFERENCES [dbo].[Tool_Inventory] ([Tool_Inventory_id])
GO

ALTER TABLE [dbo].[Tool_Inventory_WH] CHECK CONSTRAINT [FK_Tool_Inventory_WH_Tool_Inventory]
GO

ALTER TABLE [dbo].[Tool_Inventory_WH]  WITH CHECK ADD  CONSTRAINT [FK_Tool_Inventory_WH_Tool_Type] FOREIGN KEY([Tool_Type_id])
REFERENCES [dbo].[Tool_Type] ([Tool_Type_id])
GO

ALTER TABLE [dbo].[Tool_Inventory_WH] CHECK CONSTRAINT [FK_Tool_Inventory_WH_Tool_Type]
GO

/****** Object:  View [dbo].[STC_Site_On_Hand]    Script Date: 07/02/2018 08:00:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create view [dbo].[STC_Site_On_Hand]
as
select a.UserId, a.UserName, a.STC_Product_id, a.STC_Product_Name, a.SortOrder, a.Measurement_Name, pu.is_Active, pu.On_Hand_Qty, pu.Capacity
from (select p.*, m.Measurement_Name, u.* from STC_Product p inner join Measurement m on p.Measurement_id = m.Measurement_id, aspnet_Users u
inner join aspnet_UsersInRoles ur on u.UserId = ur.UserId
inner join aspnet_Roles r on ur.RoleId = r.RoleId and r.RoleName = 'STC Site') a
left outer join STC_Product_User pu
on a.UserId = pu.STC_User_ID and a.STC_Product_id = pu.STC_Product_ID

GO





/***********Insert********************************/

-- STREET TREATMENT COMMODITY ROLES
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'STC Borough', 'stc borough', null)
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'STC Site', 'stc site', null)
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'STC HQ', 'stc hq', null)

-- GARAGE SUPPLIES ROLES
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'GS Warehouse', 'gs warehouse', null)
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'GS Borough', 'gs borough', null)
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'GS District', 'gs district', null)
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'GS HQ', 'gs hq', null)


insert into aspnet_UsersInRoles
 select UserId,(select roleid from aspnet_Roles where LoweredRoleName = 'stc site' )from aspnet_users where right(UserName,1) = 'G' and LEFT(UserName,1) in ('M','S','Q','B') and LEN(UserName) = 5

Insert into aspnet_UsersInRoles
select distinct userid, (select roleid from dbo.aspnet_Roles where LoweredRoleName = 'gs district')
from Fuel_Form


-- create dsnywarehouse user and add to role gs warehouse
DECLARE	@return_value int,
		@UserId uniqueidentifier,
		@CurrentTimeUtc datetime

SET @CurrentTimeUtc = GetDate()

EXEC	@return_value = [dbo].[aspnet_Membership_CreateUser]
		@ApplicationName = N'/',
		@UserName = N'dsnywarehouse',
		@Password = N'dsny2018',
		@PasswordSalt = '',
		@Email = NULL,
		@PasswordQuestion = NULL,
		@PasswordAnswer = NULL,
		@CurrentTimeUtc = @CurrentTimeUtc,
		@IsApproved = 1,
		@UserId = @UserId OUTPUT

INSERT INTO [dbo].[aspnet_UsersInRoles] (UserId, RoleId)
	VALUES(@UserId, (SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = 'gs warehouse'))
GO

-- create RadioAdmin user and add to role radio admin
DECLARE	@return_value int,
		@UserId uniqueidentifier,
		@CurrentTimeUtc datetime

SET @CurrentTimeUtc = GetDate()

EXEC	@return_value = [dbo].[aspnet_Membership_CreateUser]
		@ApplicationName = N'/',
		@UserName = N'RadioAdmin',
		@Password = N'dsny2018',
		@PasswordSalt = '',
		@Email = NULL,
		@PasswordQuestion = NULL,
		@PasswordAnswer = NULL,
		@CurrentTimeUtc = @CurrentTimeUtc,
		@IsApproved = 1,
		@UserId = @UserId OUTPUT

INSERT INTO [dbo].[aspnet_UsersInRoles] (UserId, RoleId)
	VALUES(@UserId, (SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = 'radio admin'))

INSERT INTO [dbo].[aspnet_UsersInRoles] (UserId, RoleId)
VALUES(@UserId, (SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = 'field'))
GO

-- create GSHQ user and add to role radio admin
DECLARE	@return_value int,
		@UserId uniqueidentifier,
		@CurrentTimeUtc datetime

SET @CurrentTimeUtc = GetDate()

EXEC	@return_value = [dbo].[aspnet_Membership_CreateUser]
		@ApplicationName = N'/',
		@UserName = N'GSAdmin',
		@Password = N'dsny2018',
		@PasswordSalt = '',
		@Email = NULL,
		@PasswordQuestion = NULL,
		@PasswordAnswer = NULL,
		@CurrentTimeUtc = @CurrentTimeUtc,
		@IsApproved = 1,
		@UserId = @UserId OUTPUT

INSERT INTO [dbo].[aspnet_UsersInRoles] (UserId, RoleId)
	VALUES(@UserId, (SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = 'gs hq'))

INSERT INTO [dbo].[aspnet_UsersInRoles] (UserId, RoleId)
VALUES(@UserId, (SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = 'field'))
GO

-- create GSHQ user and add to role radio admin
DECLARE	@return_value int,
		@UserId uniqueidentifier,
		@CurrentTimeUtc datetime

SET @CurrentTimeUtc = GetDate()

EXEC	@return_value = [dbo].[aspnet_Membership_CreateUser]
		@ApplicationName = N'/',
		@UserName = N'STCAdmin',
		@Password = N'dsny2018',
		@PasswordSalt = '',
		@Email = NULL,
		@PasswordQuestion = NULL,
		@PasswordAnswer = NULL,
		@CurrentTimeUtc = @CurrentTimeUtc,
		@IsApproved = 1,
		@UserId = @UserId OUTPUT

INSERT INTO [dbo].[aspnet_UsersInRoles] (UserId, RoleId)
	VALUES(@UserId, (SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = 'stc hq'))

INSERT INTO [dbo].[aspnet_UsersInRoles] (UserId, RoleId)
VALUES(@UserId, (SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = 'field'))
GO
-- Prepopulate Tool_User
--INSERT INTO [dbo].[aspnet_UsersInRoles]
--	SELECT DISTINCT userid, (SELECT roleid FROM dbo.aspnet_Roles WHERE LoweredRoleName = 'gs district')
--		FROM Fuel_Form
--GO


-- NEED TO CREATE BOROUGH USERS


INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('IR','Inventory Request Initiated',0);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('STB','Submit To Borough',0);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('AHQ','Approved. Submit To HQ',0);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('AWH','Approved. Submit To Warehouse',0);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('ABO','Approved. Ship to Borough.',0);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('STD','Ship To District',0);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('CRE','Confirm Reciept of Equipment',1);
--INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('AHQR','Rejected',1);
--INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('AWHR','Rejected',1);
--INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('ABOR','Rejected',1);
--INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('CNCL','Cancel',1);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('WHRCV','Warehouse Recieve',1);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('EMSND','Emergency Send',1);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('ADDCMS','Added to CMS',1);
INSERT INTO Workflow_Action (Workflow_cd,Workflow_Action_Name,is_End_Workflow) VALUES ('REASGN','Reassign Equipment',1);
GO

insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'IR'),(select workflow_action_id from workflow_action where workflow_cd = 'STB'))
--insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'IR'),(select workflow_action_id from workflow_action where workflow_cd = 'CNCL'))
insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'STB'),(select workflow_action_id from workflow_action where workflow_cd = 'AHQ'))
--insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'STB'),(select workflow_action_id from workflow_action where workflow_cd = 'AHQR'))
--insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'STB'),(select workflow_action_id from workflow_action where workflow_cd = 'CNCL'))
insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AHQ'),(select workflow_action_id from workflow_action where workflow_cd = 'AWH'))
--insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AHQ'),(select workflow_action_id from workflow_action where workflow_cd = 'AWHR'))
--insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AHQ'),(select workflow_action_id from workflow_action where workflow_cd = 'CNCL'))
insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AWH'),(select workflow_action_id from workflow_action where workflow_cd = 'ABO'))
--insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AWH'),(select workflow_action_id from workflow_action where workflow_cd = 'ABOR'))
--insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AWH'),(select workflow_action_id from workflow_action where workflow_cd = 'CNCL'))
insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'ABO'),(select workflow_action_id from workflow_action where workflow_cd = 'STD'))
--insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'ABO'),(select workflow_action_id from workflow_action where workflow_cd = 'CNCL'))
GO

insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'STB'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs district'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AHQ'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs borough'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'ABO'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs warehouse'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'STD'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs borough'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'CRE'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs district'))
--insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AHQR'),(
--select RoleId from aspnet_Roles where LoweredRoleName = 'gs borough'))
--insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'ABOR'),(
--select RoleId from aspnet_Roles where LoweredRoleName = 'gs warehouse'))
--insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'CNCL'),(
--select RoleId from aspnet_Roles where LoweredRoleName = 'gs district'))
--insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'CNCL'),(
--select RoleId from aspnet_Roles where LoweredRoleName = 'gs borough'))
--insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'CNCL'),(
--select RoleId from aspnet_Roles where LoweredRoleName = 'gs warehouse'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'STB'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AHQ'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AWH'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'ABO'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'STD'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'CRE'),(
select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
--insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AHQR'),(
--select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
--insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'AWHR'),(
--select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
--insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'ABOR'),(
--select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
--insert into workflow_action_role(workflow_action_id, role_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'CNCL'),(
--select RoleId from aspnet_Roles where LoweredRoleName = 'gs hq'))
GO

INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[Borough]
           ([Borough_cd]
           ,[Borough_Name])
     VALUES
           ('MN'
           ,'Manhattan');
INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[Borough]
           ([Borough_cd]
           ,[Borough_Name])
     VALUES
           ('BX'
           ,'Bronx');
INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[Borough]
           ([Borough_cd]
           ,[Borough_Name])
     VALUES
           ('BN'
           ,'Brooklyn North');
INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[Borough]
           ([Borough_cd]
           ,[Borough_Name])
     VALUES
           ('BS'
           ,'Brooklyn South');
INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[Borough]
           ([Borough_cd]
           ,[Borough_Name])
     VALUES
           ('SI'
           ,'Staten Island');
INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[Borough]
           ([Borough_cd]
           ,[Borough_Name])
     VALUES
           ('QW'
           ,'Queens West');
INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[Borough]
           ([Borough_cd]
           ,[Borough_Name])
     VALUES
           ('QE'
           ,'Queens East');
GO

--STC Measurements
INSERT INTO DSNY_MESSAGING_PROD.dbo.Measurement (Measurement_Name, is_active, is_STC, is_Tool) VALUES ('Gallons',1,1,0);
INSERT INTO DSNY_MESSAGING_PROD.dbo.Measurement (Measurement_Name, is_active, is_STC, is_Tool) VALUES ('Yards',1,1,0);
INSERT INTO DSNY_MESSAGING_PROD.dbo.Measurement (Measurement_Name, is_active, is_STC, is_Tool) VALUES ('Tons',1,1,0);

--Tools Measurements
INSERT INTO DSNY_MESSAGING_PROD.dbo.Measurement (Measurement_Name, is_active, is_STC, is_Tool) VALUES ('Case',1,0,1); 
INSERT INTO DSNY_MESSAGING_PROD.dbo.Measurement (Measurement_Name, is_active, is_STC, is_Tool) VALUES ('Palette',1,0,1);                
INSERT INTO DSNY_MESSAGING_PROD.dbo.Measurement (Measurement_Name, is_active, is_STC, is_Tool) VALUES ('Dozen',1,0,1);     
INSERT INTO DSNY_MESSAGING_PROD.dbo.Measurement (Measurement_Name, is_active, is_STC, is_Tool) VALUES ('Individual',1,0,1);                
GO

--Tool Type and Data

INSERT INTO Tool_Type (Tool_Type_Name ,is_PowerTool ,is_Active ,Measurement_id ,Quantity_Per_Measurement) VALUES ('Aluminum Shovels',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Blades',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Boots',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Bow Saw',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Disposable Cups-Sleeves',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Dollies Only',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Dust Masks',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Fibro Brooms',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Gloves - Cotton',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Gloves - Rubber',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Gloves - Work',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Goggles',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Grass Whip',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Grey Can 44 Gallon',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Handles 60',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Handles 72',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Hats',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('HEADS',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('HOSE 25',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Ice Choppers',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Jackets',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Lobby Dust Brooms',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Lobby Dust Pans',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Nozzle',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Other',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Plastic Bags - Green',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Ponchos',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Rain Suit',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Rake Asphalt',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Rake Leaf',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Reduction Coupling',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Safety Vests',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Scrapers',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('SCREW',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Shovel Scoop',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Shovels Long Handle',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Snow Pushers',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Thinsulated Gloves',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Water Jugs',0,1,null,null);
INSERT INTO Tool_Type
           (Tool_Type_Name
           ,is_PowerTool
           ,is_Active
           ,Measurement_id
           ,Quantity_Per_Measurement)
     VALUES
           ('Wrench Hydra',0,1,null,null);

GO

INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[STC_Product]
          ([STC_Product_Name]
          ,[SortOrder]
          ,[is_Active]
          ,[Measurement_id]
          ,[LoadSize])
    VALUES
          ('Sand'
          ,1
          ,1
          ,(select Measurement_id from Measurement where Measurement_Name = 'Yards')
          ,40)
GO

INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[STC_Product]
          ([STC_Product_Name]
          ,[SortOrder]
          ,[is_Active]
          ,[Measurement_id]
          ,[LoadSize])
    VALUES
          ('Salt'
          ,1
          ,1
          ,(select Measurement_id from Measurement where Measurement_Name = 'Tons')
          ,null)
GO

INSERT INTO [DSNY_MESSAGING_PROD].[dbo].[STC_Product]
          ([STC_Product_Name]
          ,[SortOrder]
          ,[is_Active]
          ,[Measurement_id]
          ,[LoadSize])
    VALUES
          ('Calcium Chloride'
          ,1
          ,1
          ,(select Measurement_id from Measurement where Measurement_Name = 'Gallons')
          ,null)
GO

update [STC_Product]
set LoadSize = 40
where STC_Product_Name = 'Sand'

update Zone
set is_Active = 1
where is_Active is null
go

--INSERT INTO [dbo].[STC_Product_User]
--          ([STC_Product_ID]
--          ,[STC_User_ID]
--          ,[Capacity]
--         ,[On_Hand_Qty]
--          ,[is_Active])
    
--select sp.stc_product_id, au1.UserId, 0, 0,1
--from stc_product sp,aspnet_Users au1
--where userid in (select userid from aspnet_UsersInRoles au, aspnet_Roles ar where au.RoleId = ar.RoleId and ar.LoweredRoleName = 'stc site')
--use this instead
INSERT INTO [dbo].[STC_Product_User]
          ([STC_Product_ID]
          ,[STC_User_ID]
          ,[Capacity]
          ,[On_Hand_Qty]
          ,[is_Active])
    
select sp.stc_product_id, au1.UserId, 0, 0,0
from stc_product sp,aspnet_Users au1
where userid in (select userid from aspnet_UsersInRoles au, aspnet_Roles ar where au.RoleId = ar.RoleId and ar.LoweredRoleName = 'stc site')

ALTER TABLE dbo.Purchase_Orders alter column
	Not_Ordered_Reason varchar(350) NULL
GO

INSERT INTO [dbo].[Tool_User]
	SELECT DISTINCT Tool_Type_Id, Userid, 0, 1
		FROM Tool_Type tt,dbo.aspnet_UsersInRoles au
		WHERE tt.is_PowerTool = 0
			AND tt.is_Active = 1
			AND au.RoleId IN
				(SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName IN ('gs district','gs borough', 'gs warehouse'))
GO

INSERT INTO [dbo].[Standard_Tools_Inventory]
	SELECT 0, GETDATE(), Tool_User_id
		FROM Tool_User tu
		INNER JOIN Tool_Type tt on tu.Tool_Type_id = tt.Tool_Type_id
		INNER JOIN aspnet_Users u on tu.Userid = u.UserId
		INNER JOIN aspnet_UsersInRoles au on u.UserId = au.UserId
		WHERE tt.is_PowerTool = 0
			AND tt.is_Active = 1
			AND tu.is_Active = 1
			AND au.RoleId IN
				(SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName IN ('gs district','gs borough','gs warehouse'))


---Base Line 2
 ALTER TABLE dbo.Tool_Inventory_PT ADD is_Dispose bit null
 go

ALTER TABLE dbo.STC_Pickup ADD
Driver_Name varchar(50) NULL
GO

-- =============================================
-- Author:	Mohamed Ashry
-- Create date: 7/22/2018
-- Description:	Returns Users with a given role
-- =============================================
CREATE FUNCTION ufn_GetUserIdByRole (@RoleName nvarchar(256))
RETURNS table
AS
Return SELECT au.UserId 
FROM aspnet_users au, dbo.aspnet_UsersInRoles aur,dbo.aspnet_Roles ar
where au.UserId = aur.UserId and 
aur.RoleId = ar.RoleId and
ar.LoweredRoleName = LOWER(@RoleName) --STC Site,STC Borough,STC HQ

GO

CREATE TABLE [dbo].[Tool_Inventory_Association](
	[Tool_Inventory_WH_id] [int] NOT NULL,
	[Standard_Tools_Inventory_id] [int] NOT NULL
 CONSTRAINT [XPKTool_Inventory_Association] PRIMARY KEY CLUSTERED 
(
	[Tool_Inventory_WH_id] ASC,
	[Standard_Tools_Inventory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Tool_Inventory_Association]  WITH CHECK ADD  CONSTRAINT [FK_Tool_Inventory_Association_Standard_Tools_Inventory] FOREIGN KEY([Standard_Tools_Inventory_id])
REFERENCES [dbo].[Standard_Tools_Inventory] ([Standard_Tools_Inventory_id])
GO

ALTER TABLE [dbo].[Tool_Inventory_Association]  WITH CHECK ADD  CONSTRAINT [FK_Tool_Inventory_Association_Tool_Inventory_WH] FOREIGN KEY([Tool_Inventory_WH_id])
REFERENCES [dbo].[Tool_Inventory_WH] ([Tool_Inventory_WH_id])
GO

insert into workflow_action_flow (workflow_id, next_workflow_id) values ((select workflow_action_id from workflow_action where workflow_cd = 'STD'),(select workflow_action_id from workflow_action where workflow_cd = 'CRE'))
go

---Radio SQL

/****** Object:  Table [dbo].[Radio]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio](
	[Radio_id] [int] IDENTITY(1,1) NOT NULL,
	[Radio_Serial_Number] [varchar](50) NULL,
	[DSNY_Radio_id] [varchar](50) NULL,
	[Trunk_id] [varchar](50) NULL,
	[Issued_To_Userid] [uniqueidentifier] NULL,
	[Radio_Equipment_Type_id] [int] NULL,
	[Radio_Make_id] [int] NULL,
	[Radio_Model_id] [int] NULL,
	[Assigned_To_Vehicle] [varchar](50) NULL,
	[Inventory_Date] [datetime] NULL,
	[Service_Date] [datetime] NULL,
	[is_Spare] [bit] NULL,
	[is_Active] [bit] NULL,
	[Radio_Status_id] [int] NULL,
	[Last_Dist_Inv_Date] [datetime] NULL,
	[Last_Dist_Inv_Status_id] [int] NULL,
	[is_Down] [bit] NULL,
	[is_Disposed] [bit] NULL,
	[Is_Inventory_Requested] [bit] NULL,
 CONSTRAINT [PK_Radio] PRIMARY KEY CLUSTERED 
(
	[Radio_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Radio_Accessory]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio_Accessory](
	[Radio_Accessory_id] [int] IDENTITY(1,1) NOT NULL,
	[Part_Number] [varchar](50) NULL,
	[Userid] [uniqueidentifier] NULL,
	[Radio_Equipment_Type_id] [int] NULL,
	[Radio_Make_id] [int] NULL,
	[Radio_Maintenance_id] [int] NULL,
	[Description] [varchar](200) NULL,
	[Current_Qty] [int] NULL,
	[Last_Dist_Inv_Date] [datetime] NULL,
	[Last_Dist_Inv_Qty] [int] NULL,
	[Is_Inventory_Requested] [bit] NULL,
	[Is_Spare] [bit] NULL,
 CONSTRAINT [PK_Radio_Accessory] PRIMARY KEY CLUSTERED 
(
	[Radio_Accessory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Radio_Accessory_Ledger]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio_Accessory_Ledger](
	[Radio_Accessory_Ledger_id] [int] IDENTITY(1,1) NOT NULL,
	[Radio_Accessory_id] [int] NULL,
	[is_recieve] [bit] NULL,
	[Radio_id] [int] NULL,
	[Qty] [int] NULL,
	[Transfer_Userid] [uniqueidentifier] NULL,
	[Comment] [varchar](500) NULL,
 CONSTRAINT [PK_Radio_Accessory_Ledger] PRIMARY KEY CLUSTERED 
(
	[Radio_Accessory_Ledger_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Radio_Equipment_Status]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio_Equipment_Status](
	[Radio_Equipment_Status_id] [int] IDENTITY(1,1) NOT NULL,
	[Equipment_Status_Name] [varchar](50) NULL,
	[is_Workflow] [bit] NULL,
	[is_Inventory] [bit] NULL,
 CONSTRAINT [PK_Radio_Equipment_Status] PRIMARY KEY CLUSTERED 
(
	[Radio_Equipment_Status_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Radio_Equipment_Type]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio_Equipment_Type](
	[Radio_Equipment_Type_id] [int] IDENTITY(1,1) NOT NULL,
	[Equipment_Type_Name] [varchar](50) NOT NULL,
	[is_District_Inventory] [bit] NULL,
	[is_Active] [bit] NULL,
 CONSTRAINT [PK_Radio_Equipment_Type] PRIMARY KEY CLUSTERED 
(
	[Radio_Equipment_Type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Radio_History]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio_History](
	[Radio_History_id] [int] IDENTITY(1,1) NOT NULL,
	[Radio_id] [int] NOT NULL,
	[Radio_Serial_Number] [varchar](50) NULL,
	[DSNY_Radio_id] [varchar](50) NULL,
	[Trunk_id] [varchar](50) NULL,
	[Issued_To_Userid] [uniqueidentifier] NULL,
	[Radio_Equipment_Type_id] [int] NULL,
	[Radio_Make_id] [int] NULL,
	[Radio_Model_id] [int] NULL,
	[Assigned_To_Vehicle] [varchar](50) NULL,
	[Inventory_Date] [datetime] NULL,
	[Service_Date] [datetime] NULL,
	[is_Spare] [bit] NULL,
	[is_Active] [bit] NULL,
	[Radio_Status_id] [int] NULL,
	[Last_Dist_Inv_Date] [datetime] NULL,
	[Last_Dist_Inv_Status_id] [int] NULL,
	[Comment] [varchar](100) NULL,
 CONSTRAINT [PK_Radio_History] PRIMARY KEY CLUSTERED 
(
	[Radio_History_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Radio_Make]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio_Make](
	[Radio_Make_id] [int] IDENTITY(1,1) NOT NULL,
	[Make_Name] [varchar](50) NULL,
	[is_Active] [bit] NULL,
 CONSTRAINT [PK_Radio_Make] PRIMARY KEY CLUSTERED 
(
	[Radio_Make_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Radio_Model]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio_Model](
	[Radio_Model_id] [int] IDENTITY(1,1) NOT NULL,
	[Radio_Make_id] [int] NULL,
	[Model_Name] [varchar](50) NULL,
	[is_Active] [bit] NULL,
 CONSTRAINT [PK_Radio_Model] PRIMARY KEY CLUSTERED 
(
	[Radio_Model_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Radio_Repair]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Radio_Repair](
	[Radio_Repair_id] [int] IDENTITY(1,1) NOT NULL,
	[Radio_Id] [int] NULL,
	[Radio_Repair_Problem_Type_id] [int] NULL,
	[Repair_Date] [datetime] NULL,
 CONSTRAINT [PK_Radio_Repair] PRIMARY KEY CLUSTERED 
(
	[Radio_Repair_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Radio_Repair_Problem_Type]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio_Repair_Problem_Type](
	[Radio_Repair_Problem_id] [int] IDENTITY(1,1) NOT NULL,
	[Repair_Problem_Name] [varchar](50) NULL,
	[is_Active] [bit] NULL,
 CONSTRAINT [PK_Radio_Repair_Problem_Type] PRIMARY KEY CLUSTERED 
(
	[Radio_Repair_Problem_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Radio]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Make] FOREIGN KEY([Radio_Make_id])
REFERENCES [dbo].[Radio_Make] ([Radio_Make_id])
GO
ALTER TABLE [dbo].[Radio] CHECK CONSTRAINT [FK_Radio_Make]
GO
ALTER TABLE [dbo].[Radio]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Model] FOREIGN KEY([Radio_Model_id])
REFERENCES [dbo].[Radio_Model] ([Radio_Model_id])
GO
ALTER TABLE [dbo].[Radio] CHECK CONSTRAINT [FK_Radio_Model]
GO
ALTER TABLE [dbo].[Radio]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Radio_Equipment_Status] FOREIGN KEY([Radio_Status_id])
REFERENCES [dbo].[Radio_Equipment_Status] ([Radio_Equipment_Status_id])
GO
ALTER TABLE [dbo].[Radio] CHECK CONSTRAINT [FK_Radio_Radio_Equipment_Status]
GO
ALTER TABLE [dbo].[Radio]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Radio_Equipment_Status1] FOREIGN KEY([Last_Dist_Inv_Status_id])
REFERENCES [dbo].[Radio_Equipment_Status] ([Radio_Equipment_Status_id])
GO
ALTER TABLE [dbo].[Radio] CHECK CONSTRAINT [FK_Radio_Radio_Equipment_Status1]
GO
ALTER TABLE [dbo].[Radio_Accessory]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Accessory_aspnet_Users] FOREIGN KEY([Userid])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[Radio_Accessory] CHECK CONSTRAINT [FK_Radio_Accessory_aspnet_Users]
GO
ALTER TABLE [dbo].[Radio_Accessory]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Accessory_Radio] FOREIGN KEY([Userid])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[Radio_Accessory] CHECK CONSTRAINT [FK_Radio_Accessory_Radio]
GO
ALTER TABLE [dbo].[Radio_Accessory]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Accessory_Radio_Equipment_Type] FOREIGN KEY([Radio_Equipment_Type_id])
REFERENCES [dbo].[Radio_Equipment_Type] ([Radio_Equipment_Type_id])
GO
ALTER TABLE [dbo].[Radio_Accessory] CHECK CONSTRAINT [FK_Radio_Accessory_Radio_Equipment_Type]
GO
ALTER TABLE [dbo].[Radio_Accessory]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Accessory_Radio_Make] FOREIGN KEY([Radio_Make_id])
REFERENCES [dbo].[Radio_Make] ([Radio_Make_id])
GO
ALTER TABLE [dbo].[Radio_Accessory] CHECK CONSTRAINT [FK_Radio_Accessory_Radio_Make]
GO
ALTER TABLE [dbo].[Radio_Accessory_Ledger]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Accessory_Ledger_Radio_Accessory] FOREIGN KEY([Radio_Accessory_id])
REFERENCES [dbo].[Radio_Accessory] ([Radio_Accessory_id])
GO
ALTER TABLE [dbo].[Radio_Accessory_Ledger] CHECK CONSTRAINT [FK_Radio_Accessory_Ledger_Radio_Accessory]
GO
ALTER TABLE [dbo].[Radio_History]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Radio_History] FOREIGN KEY([Radio_id])
REFERENCES [dbo].[Radio] ([Radio_id])
GO
ALTER TABLE [dbo].[Radio_History] CHECK CONSTRAINT [FK_Radio_Radio_History]
GO
ALTER TABLE [dbo].[Radio_Model]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Model_Radio_Make] FOREIGN KEY([Radio_Make_id])
REFERENCES [dbo].[Radio_Make] ([Radio_Make_id])
GO
ALTER TABLE [dbo].[Radio_Model] CHECK CONSTRAINT [FK_Radio_Model_Radio_Make]
GO
ALTER TABLE [dbo].[Radio_Repair]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Repair_Radio] FOREIGN KEY([Radio_Id])
REFERENCES [dbo].[Radio] ([Radio_id])
GO
ALTER TABLE [dbo].[Radio_Repair] CHECK CONSTRAINT [FK_Radio_Repair_Radio]
GO
ALTER TABLE [dbo].[Radio_Repair]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Repair_Radio_Repair_Problem_Type] FOREIGN KEY([Radio_Repair_Problem_Type_id])
REFERENCES [dbo].[Radio_Repair_Problem_Type] ([Radio_Repair_Problem_id])
GO
ALTER TABLE [dbo].[Radio_Repair] CHECK CONSTRAINT [FK_Radio_Repair_Radio_Repair_Problem_Type]
GO


truncate table Radio_History
go

alter table Radio_History
add ChangeDate datetime not null
go

alter table Radio
add Inventory_Request_Date datetime  null
go

alter table Radio_Accessory
add Inventory_Request_Date datetime null
go


-- Radio Role Setup
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'Radio Admin', 'radio admin', null)
go

INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'Radio District', 'radio district', null)
go

insert into aspnet_UsersInRoles
 select UserId,(select roleid from aspnet_Roles where LoweredRoleName = 'radio district' )from aspnet_users where right(UserName,1) = 'G' and LEFT(UserName,1) in ('M','S','Q','B') and LEN(UserName) = 5

 go

 -- Add trigger for tracking history and comments
CREATE TRIGGER TRG_InsertRadio
ON dbo.Radio
AFTER INSERT AS
BEGIN
INSERT INTO Radio_History
SELECT [Radio_id]
      ,[Radio_Serial_Number]
      ,[DSNY_Radio_id]
      ,[Trunk_id]
      ,[Issued_To_Userid]
      ,[Radio_Equipment_Type_id]
      ,[Radio_Make_id]
      ,[Radio_Model_id]
      ,[Assigned_To_Vehicle]
      ,[Inventory_Date]
      ,[Service_Date]
      ,[is_Spare]
      ,[is_Active]
      ,[Radio_Status_id]
      ,[Last_Dist_Inv_Date]
      ,[Last_Dist_Inv_Status_id]
	  , NULL -- Comments
      , GetDate() -- Change Date
  FROM INSERTED
END
GO
ALTER TABLE RADIO
ADD Last_Comments VARCHAR(MAX) NULL
GO

