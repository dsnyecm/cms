-- DSNY EXCEPTIONS
CREATE TABLE DSNYExceptions
	(
	ExceptionId int NOT NULL PRIMARY KEY IDENTITY (1, 1),
	ExceptionCategoryId int NOT NULL,
	Exception varchar(3000) NOT NULL,
	ExceptionTableName varchar(255) NULL,
	ExceptionTableKey int NULL,
	ExceptionDateTime datetime NOT NULL,
	ExceptionUser uniqueidentifier NOT NULL
	)
GO

CREATE TABLE DSNYExceptionCategories
	(
	ExceptionCategoryId int NOT NULL PRIMARY KEY IDENTITY (1, 1),
	ExceptionCategory varchar(255) NOT NULL,
	ExceptionCode varchar(255) NOT NULL,
	IsCollected bit NOT NULL,
	IsReport bit NOT NULL,
	IsCritical bit NOT NULL,
	Priority int NOT NULL
	)
GO

ALTER TABLE [dbo].[DSNYExceptions]  WITH CHECK ADD  CONSTRAINT [FK_DSNYExceptions_DSNYExceptionCategories] FOREIGN KEY([ExceptionCategoryId])
REFERENCES [dbo].[DSNYExceptionCategories] ([ExceptionCategoryId])
GO

ALTER TABLE [dbo].[DSNYExceptions] CHECK CONSTRAINT [FK_DSNYExceptions_DSNYExceptionCategories]
GO

-- DAILY REPORTS

CREATE TABLE DailyReports
	(
	DailyReportId int NOT NULL PRIMARY KEY IDENTITY (1, 1),
	DailyReportDate datetime NOT NULL,
	DailyReportWeather nvarchar(50) NOT NULL,
	DailyReportFunction nvarchar(50) NOT NULL,
	DailyReportTempLow nvarchar(50) NOT NULL,
	DailyReportTempHigh nvarchar(50) NOT NULL
	)
GO


-- DSNY EXCEPTION CATEGORIES
INSERT INTO DSNYExceptionCategories VALUES ('Purchase Order Override', 'POO', 0, 1, 0, 1);
INSERT INTO DSNYExceptionCategories VALUES ('End Book Issue', 'EBI', 0, 1, 0, 1);
INSERT INTO DSNYExceptionCategories VALUES ('Delivery Issue', 'DI', 0, 1, 0, 1);
INSERT INTO DSNYExceptionCategories VALUES ('Expected Delivery Issue', 'EDI', 0, 1, 0, 1);
INSERT INTO DSNYExceptionCategories VALUES ('On Hand Greater Than 90', 'OHE', 0, 1, 0, 1);
INSERT INTO DSNYExceptionCategories VALUES ('Qty Delivery Difference', 'QDD', 0, 1, 0, 1);	 
INSERT INTO DSNYExceptionCategories VALUES('Fuel Form Override', 'FFO', 0, 1, 0, 1)
INSERT INTO DSNYExceptionCategories VALUES('Daily Report Override', 'DRO', 0, 0, 0, 2)


-- FUEL FORM HISTORY
CREATE TABLE Fuel_Form_History
(
	Fuel_Form_History_ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Fuel_Form_ID int NOT NULL,
	Submission_Date datetime NULL,
	UserId uniqueidentifier NULL,
	Supervisor_Full_Name varchar(40) NULL,
	Supervisor_Badge_Number varchar(20) NULL,
	Remarks varchar(1000) NULL,
	Modified_By uniqueidentifier NOT NULL,
	Modified_Date datetime NOT NULL
)
GO

ALTER TABLE [dbo].[Fuel_Form_History]  WITH CHECK ADD  CONSTRAINT [FK_Fuel_Form_History_aspnet_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[Fuel_Form_History] CHECK CONSTRAINT FK_Fuel_Form_History_aspnet_Users
GO

CREATE TABLE Fuel_Form_Details_History
(
	Fuel_Form_Details_History_ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Fuel_Form_ID int NOT NULL,
	Product_ID int NOT NULL,
	On_Hand int NOT NULL,
	Spare_Drums int NULL,
	Fuel_Form_Details_Id int NOT NULL,
	Equipment_User_ID int NULL,
	Tank_Delivered int NULL,
	Tank_Dispensed int NULL,
	Modified_By uniqueidentifier NULL,
	Modified_Date datetime NULL
)

GO

ALTER TABLE [dbo].[Fuel_Form_Details_History]  WITH CHECK ADD  CONSTRAINT [FK_Fuel_Form_Details_History_aspnet_Users] FOREIGN KEY([Product_ID])
REFERENCES [dbo].[Product] ([Product_ID])
GO

ALTER TABLE [dbo].[Fuel_Form_Details_History] CHECK CONSTRAINT [FK_Fuel_Form_Details_History_aspnet_Users]
GO

CREATE TABLE Fuel_Form_Details_Delivery_History
(
	Fuel_Form_Details_Delivery_History_ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Fuel_Form_Details_Id int NOT NULL,
	Order_No int NOT NULL,
	Invoice_Number varchar(20) NULL,
	Recieve_Date datetime NULL,
	Quantity int NULL,
	Vendor_ID int NULL,
	Modified_By uniqueidentifier NULL,
	Modified_Date datetime NULL
)

GO

ALTER TABLE [dbo].[Fuel_Form_Details_Delivery_History]  WITH CHECK ADD  CONSTRAINT [FK_Fuel_Form_Details_Delivery_History_Vendor] FOREIGN KEY([Vendor_ID])
REFERENCES [dbo].[Vendor] ([Vendor_ID])
GO

ALTER TABLE [dbo].[Fuel_Form_Details_Delivery_History] CHECK CONSTRAINT [FK_Fuel_Form_Details_Delivery_History_Vendor]
GO


CREATE TABLE Fuel_Form_Equipment_Failure_History
(
	Fuel_Form_Equipment_Failure_History_ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Equipment_Failure_ID int NOT NULL,
	is_Equipment_Failure bit NOT NULL,
	Failure_Date datetime NULL,
	Fix_Date datetime NULL,
	Remarks varchar(350) NULL,
	Equipment_User_ID int NULL,
	Fuel_Form_ID int NOT NULL,
	Modified_By uniqueidentifier NULL,
	Modified_Date datetime NULL
)

GO

-- REMOVE REPORTS USER ROLE
--DELETE 
--  FROM [dbo].[aspnet_UsersInRoles]
--  WHERE RoleId = (SELECT TOP 1 RoleId FROM [dbo].[aspnet_Roles] WHERE LoweredRoleName = 'reports')

--DELETE
--  FROM [dbo].[aspnet_Roles]
--  WHERE LoweredRoleName = 'reports'


-- PURCHASE ORDERS
CREATE TABLE Purchase_Orders
(
	Purchase_Order_ID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	UserId uniqueidentifier NOT NULL,
	Product_ID int NOT NULL,
	Vendor_ID int NOT NULL,
	Order_Amount decimal NOT NULL,
	Order_Date datetime NULL,
	Is_Ordered bit NULL,
	Not_Ordered_Reason varchar(350) NULL
)

GO

ALTER TABLE [dbo].[Purchase_Orders]  WITH CHECK ADD  CONSTRAINT [FK_Purchase_Orders_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO

ALTER TABLE [dbo].[Purchase_Orders] CHECK CONSTRAINT [FK_Purchase_Orders_User]
GO

ALTER TABLE [dbo].[Purchase_Orders]  WITH CHECK ADD  CONSTRAINT [FK_Purchase_Orders_Product] FOREIGN KEY([Product_ID])
REFERENCES [dbo].[Product] ([Product_ID])
GO

ALTER TABLE [dbo].[Purchase_Orders] CHECK CONSTRAINT [FK_Purchase_Orders_Product]
GO

ALTER TABLE [dbo].[Purchase_Orders]  WITH CHECK ADD  CONSTRAINT [FK_Purchase_Orders_Vendor] FOREIGN KEY([Vendor_ID])
REFERENCES [dbo].[Vendor] ([Vendor_ID])
GO

ALTER TABLE [dbo].[Purchase_Orders] CHECK CONSTRAINT [FK_Purchase_Orders_Vendor]
GO

-- Add columns for Fuel Form Tables
ALTER TABLE Fuel_Form ADD Modified_By uniqueidentifier NULL;
ALTER TABLE Fuel_Form_Equipment_Failure ADD Modified_By uniqueidentifier NULL;
ALTER TABLE Fuel_Form_Details ADD Modified_By uniqueidentifier NULL, Tank_Delivered INT NULL, Tank_Dispensed INT NULL;
GO


-- Add columns to product table
ALTER TABLE [dbo].[Product]
	ADD Vendor_ID int NOT NULL DEFAULT 3,
		is_Reading_Required BIT NOT NULL DEFAULT 0,
		is_Water BIT NOT NULL DEFAULT 0,
		Sub_Type VARCHAR(50),
		Order_Delivery_Type VARCHAR(50),
		Order_Delivery_Distribution VARCHAR(255)

ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Vendor] FOREIGN KEY([Vendor_ID])
REFERENCES [dbo].[Vendor] ([Vendor_ID])
GO

ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Vendor]
GO

-- Set Diesel product's sub-type, otherwise clear it
--UPDATE [dbo].[Product] SET Sub_Type = 'B20'	WHERE Product_Name = 'DIESEL'
--UPDATE [dbo].[Product] SET Sub_Type = null WHERE Product_Name != 'DIESEL'

-- Add water flag to products
UPDATE PRODUCT
	SET is_Water = 1
	WHERE Product_Name LIKE '%water%'

-- Add columns to vendor table
ALTER TABLE [dbo].[Vendor]
	ADD Account_Number VARCHAR(100),
	Phone_Number VARCHAR(100)

GO

-- Add triggers for history tables
CREATE TRIGGER Fuel_Form_History_Trigger ON Fuel_Form
	FOR UPDATE
	AS
	BEGIN
		INSERT INTO Fuel_Form_History
			(Fuel_Form_ID, Submission_Date, UserId, Supervisor_Full_Name, Supervisor_Badge_Number, Remarks, Modified_By, Modified_Date)
			SELECT Fuel_Form_ID, Submission_Date, UserId, Supervisor_Full_Name, Supervisor_Badge_Number, Remarks, inserted.Modified_By, GETDATE()
			FROM inserted
	END
GO

CREATE TRIGGER Fuel_Form_Details_History_Trigger ON Fuel_Form_Details
	FOR UPDATE
	AS
	BEGIN
		INSERT INTO Fuel_Form_Details_History
			(Fuel_Form_Details_ID, Fuel_Form_ID, Product_ID, On_Hand, Spare_Drums, Equipment_User_ID, Tank_Delivered, Tank_Dispensed, Modified_By, Modified_Date)
			SELECT Fuel_Form_Details_ID, Fuel_Form_ID, Product_ID, On_Hand, Spare_Drums, Equipment_User_ID, Tank_Delivered, Tank_Dispensed,inserted.Modified_By, GETDATE()
			FROM inserted
	END
GO

CREATE TRIGGER Fuel_Form_Details_Delivery_History_Trigger ON Fuel_Form_Details_Delivery
	FOR DELETE
	AS
	BEGIN
		INSERT INTO Fuel_Form_Details_Delivery_History
			(Fuel_Form_Details_ID, Order_No, Invoice_Number, Recieve_Date, Quantity, Vendor_ID, Modified_Date)
			SELECT Fuel_Form_Details_ID, Order_No, Invoice_Number, Recieve_Date, Quantity, Vendor_ID, GETDATE()
			FROM deleted
	END
GO

CREATE TRIGGER Fuel_Form_Equipment_Failure_History_Trigger ON Fuel_Form_Equipment_Failure
	FOR UPDATE
	AS
	BEGIN
		INSERT INTO Fuel_Form_Equipment_Failure_History
			(Fuel_Form_ID, Equipment_Failure_ID, is_Equipment_Failure, Failure_Date, Fix_Date, Equipment_User_ID, Modified_By, Modified_Date)
			SELECT Fuel_Form_ID, Equipment_Failure_ID, is_Equipment_Failure, Failure_Date, Fix_Date, Equipment_User_ID, inserted.Modified_By, GETDATE()
			FROM inserted
	END
GO

/****** Object:  UserDefinedFunction [dbo].[GetProperty]    Script Date: 10/25/2015 4:57:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetProperty] (
	@UserId UNIQUEIDENTIFIER,
	@PropertyName VARCHAR(50)
)
RETURNS VARCHAR(500)
AS
BEGIN

DECLARE @RetVal VARCHAR(500)
DECLARE @PropPos INT, @Names VARCHAR(500), @Values VARCHAR(500), @start TINYINT, @length TINYINT

-- Note how we prefix the first property name with a colon. Now we know that EVERY property name will be bookended with colons:
SELECT @Names = ':' + CAST(PropertyNames AS VARCHAR(500)), @Values = PropertyValuesString FROM aspnet_Profile WHERE UserId = @UserId

SELECT @PropPos = PATINDEX('%:'+ @PropertyName +':S%', @Names)

IF @PropPos = 0
	BEGIN
		SET @RetVal = NULL
	END
ELSE
	BEGIN
		SET @PropPos = @PropPos + LEN(@PropertyName) + 4
		SELECT @Names = SUBSTRING(@Names, @PropPos, LEN(@Names))
		SELECT @start = CAST(SUBSTRING(@Names, 1, CHARINDEX(':', @Names) - 1) AS tinyint) + 1
		SELECT @length = CAST(SUBSTRING(@Names, CHARINDEX(':', @Names) + 1, ((CHARINDEX(':', @Names, CHARINDEX(':', @Names) + 1)) - (CHARINDEX(':', @Names) + 1))) AS tinyint)
		SELECT @RetVal = SUBSTRING(@Values, @start, @length)
	END

	RETURN @RetVal
END

GO

-- Add User for purchase order
--DECLARE @now datetime
--SET @now= GETDATE()
--EXEC aspnet_Membership_CreateUser '/','PurchaseOrderCopy','PoCPassword2017', '','pmessina@dsny.nyc.gov','','',1,@now,@now,0,0,null

--GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mohamed Ashry
-- Modified date: 9/17/2017
-- Description:	The procedure returns the orders that need to take place in the purchase order generation process
-- =============================================
CREATE PROCEDURE [dbo].[GetPurchaseOrderItems] 
	-- Days to compare is the number of days we go back +3 to determine the average dispensed(burn) rate of fuel at a location.
	@daysToCompare int = 90
AS
BEGIN

	
	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;

	SELECT UserId, UserName, Address, Borough, PODisplayName, District, Product_ID, Product_Name, Sub_Type, 
			Vendor_ID, Vendor_Name, Account_Number, Phone_Number, 
			--CONVERT(INT, round((case when Order_Amount > ((capacity) - End_On_Hand + Avg_Dispensed) then ((capacity) - End_On_Hand + Avg_Dispensed) else Order_Amount end)/10,0)*10) Order_Amount, 
			(case when (CONVERT(INT, round((case when Order_Amount > ((capacity) - End_On_Hand + Avg_Dispensed) then ((capacity) - End_On_Hand + Avg_Dispensed) else Order_Amount end)/10,0)*10)) > Capacity then Capacity else CONVERT(INT, round((case when Order_Amount > ((capacity) - End_On_Hand + Avg_Dispensed) then ((capacity) - End_On_Hand + Avg_Dispensed) else Order_Amount end)/10,0)*10) end) Order_Amount ,
			--If the order amount for the product is greater than the capacity minus end on hand plus the average dispensed the make the order amount equial to capacity - end on hand plus average dispensed. Otherwise just use the order amount derived.
			capacity,Measurement_Type, Avg_Dispensed, End_On_Hand
	FROM (
		SELECT UserId, UserName, dbo.GetProperty(UserId, 'Address') Address, 
			dbo.GetProperty(UserId, 'Borough') as Borough, dbo.GetProperty(UserId, 'PODisplayName') PODisplayName,
			dbo.GetProperty(UserId, 'District') District, Product_ID, Product_Name, Sub_Type, 
			Vendor_ID, Vendor_Name, Account_Number, Phone_Number, 
			sum(capacity) Capacity, 
			sum(CONVERT(INT, round((CASE WHEN (End_On_Hand - (Avg_dispensed)) < (.6 * Capacity) THEN (Capacity - End_On_Hand + (Avg_dispensed)) ELSE 0 END)/10,0)*10)) Order_Amount,
			--Order Amount = If the end on hand amount - the average dispensed is less than 60% of a tank, then put in a purchase order amount equal to End on Hand minus Average Dispensed
			Measurement_Type, SUM(Avg_Dispensed) Avg_Dispensed,  SUM(End_On_Hand) End_On_Hand 
			FROM (
				SELECT DISTINCT eu.Equipment_Description,u.UserId, u.UserName, p.Product_ID, p.Product_Name, p.Sub_Type, p.is_Drums, p.is_Water,
				v.Vendor_ID, v.Vendor_Name, v.Account_Number, v.Phone_Number,CONVERT(INT, ROUND((ISNULL((eu.Capacity * .9), (pu.Capacity*.9))),0)) as capacity,
				p.Measurement_Type, 
				(SELECT TOP (@daysToCompare) AVG(ffd.Tank_Dispensed) --Get average dispensed amount for the last xx+3 days for this user/product
					FROM Fuel_Form ff
					INNER JOIN Fuel_Form_Details ffd on ff.Fuel_Form_ID = ffd.Fuel_Form_ID
					WHERE Submission_Date >= GETDATE() - (@daysToCompare + 3)  AND
					ff.UserId = pu.UserId AND
					ffd.Product_ID = p.Product_ID and
					ffd.tank_dispensed is not null) as Avg_Dispensed,
				(SELECT ffd.On_Hand -- Get the on hand quantity from the last fuel form for the product/location/tank
					FROM Fuel_Form ff
					INNER JOIN Fuel_Form_Details ffd on ff.Fuel_Form_ID = ffd.Fuel_Form_ID
					WHERE ffd.Fuel_Form_ID = (select MAX(ff1.Fuel_Form_ID) from Fuel_Form ff1 where ff1.UserId = pu.UserId) and
					ffd.Equipment_User_ID = eu.Equipment_User_ID ) as End_On_Hand
				FROM Product_User pu 
					INNER JOIN aspnet_Users u ON u.UserId = pu.UserId
					INNER JOIN Product p ON p.Product_ID = pu.Product_ID
					INNER JOIN Vendor v ON p.Vendor_ID = v.Vendor_ID
					INNER JOIN Equipment_User eu on pu.UserId = eu.UserId and pu.Product_ID = eu.Product_ID
				WHERE p.is_Active = 1 and  eu.is_Active = 1 and p.is_Water = 0 and eu.Equipment_ID = 9 --make sure that the product is active, the tank is not down, ignore dispensors in the result set.
					  and eu.Equipment_User_ID not in(select distinct Equipment_User_ID from Fuel_Form_Equipment_Failure ffe  --Only calculate for equipment 
														where ffe.Failure_Date is not null and ffe.Fix_Date is null and       --for equipment that is not down.
														Fuel_Form_ID = (select MAX(Fuel_Form_id) 
																		from Fuel_Form_details ffe1 
																		where ffe.Equipment_User_ID = ffe1.Equipment_User_ID)) and
																		(convert(nvarchar(50),eu.USERID)+ convert(nvarchar(50),eu.Product_ID)) not in   --Only calculate where a product 
																				(SELECT convert(nvarchar(50),USERID)+ convert(nvarchar(50),product_id)  --was not already put in a purchase order within
																					from purchase_orders												--the last 30 hours T-F or 70 hours on monday
																					where Order_Date between DATEADD(hour, (case DATEPART(dw,GETDATE()) when 1 then -78 else -30 end), getdate()) and GETDATE()) 
																																) as P
			WHERE p.is_Water = 0 --ignore the tanks associated with water
			GROUP BY UserId, UserName, Product_ID, Product_Name, Vendor_ID, Measurement_Type, Sub_Type, Vendor_Name, Account_Number, Phone_Number
			HAVING (SUM(End_On_Hand) - (SUM(Avg_Dispensed)) < (.6 * sum(CONVERT(INT, ROUND((capacity * (CASE is_Drums WHEN 1 THEN 1 ELSE .9 END)),0)))) )
		) v
	WHERE Order_Amount > 0
	ORDER BY UserName
END
 
GO

-- =============================================
-- Author:		Mohamed Ashry
-- Create date: 10/6/2017
-- Description:	This procedure takes a user id and date and deletes the fuel
--                      form for that user on that given day.
-- =============================================
CREATE PROCEDURE DeleteFuelFormByUser 
	-- Add the parameters for the stored procedure here
	@UserID uniqueidentifier,
	@Date date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

delete from Fuel_Form_Details_Delivery where Fuel_Form_Details_Id in (select Fuel_Form_Details_Id from Fuel_Form_Details where Fuel_Form_ID in (select Fuel_Form_ID from Fuel_Form where convert(date,Submission_Date) = convert(date,@Date)and UserId = 'E5851857-7666-4BB5-A19C-488FB9515711'
))
delete from Fuel_Form_Details where Fuel_Form_ID in (select Fuel_Form_ID from Fuel_Form where convert(date,Submission_Date) = convert(date,@Date)and UserId = 'E5851857-7666-4BB5-A19C-488FB9515711'
)
delete from Fuel_Form_Equipment_Failure where Fuel_Form_ID in (select Fuel_Form_ID from Fuel_Form where convert(date,Submission_Date) = convert(date,@Date)and UserId = 'E5851857-7666-4BB5-A19C-488FB9515711'
)
delete from Fuel_Form where convert(date,Submission_Date) = convert(date,@Date) and UserId = @UserID
END
GO

-- Updates user's profiles with borough
DECLARE @tempUsers table (
	RowID int not null primary key identity(1,1),
	UserId uniqueidentifier,
	UserName varchar(255),
	LoweredUserName varchar(255),
	isAnonymous bit)

DECLARE @usersToProcess int,
		@userName varchar(255),
		@currentUser int

INSERT INTO @tempUsers (UserId, UserName, LoweredUserName, isAnonymous)
	SELECT UserId, UserName, LoweredUserName, IsAnonymous FROM aspnet_Users

SET @usersToProcess = @@ROWCOUNT

SET @currentUser = 0
WHILE @currentUser < @usersToProcess
BEGIN

	SET @currentUser = @currentUser+1

	SELECT
		@userName = userName
		From @tempUsers
		WHERE RowID = @CurrentUser

	DECLARE @tempProfiles table (
		RowID int not null primary key identity(1,1),
		PropertyNames varchar(max),
		PropertyValuesString varchar(max),
		PropertyValuesBinary image)

	DECLARE	@returnProfile Int, @updateProfile int,
		@pn varchar(max),
		@pvs varchar(max),
		@pvb varbinary(max),
		@profilesToProcess  int,
		@currentProfile     int

	INSERT INTO @tempProfiles (PropertyNames, PropertyValuesString, PropertyValuesBinary)
		EXEC	@returnProfile = [dbo].[aspnet_Profile_GetProperties]
				@ApplicationName = N'/',
				@UserName = @userName,
				@CurrentTimeUtc = N''

	SELECT TOP 1 @profilesToProcess = RowID
			FROM @tempProfiles
			ORDER BY RowID DESC

	SET @currentProfile = 0
	WHILE @currentProfile < @profilesToProcess
	BEGIN

		SET @currentProfile = @currentProfile + 1
		DECLARE @borough varchar(25)

		SELECT
			@pn = PropertyNames, @pvs = PropertyValuesString, @pvb = PropertyValuesBinary 
			FROM @tempProfiles
			WHERE RowID = @currentProfile

			SET @borough = CASE 
				WHEN @userName LIKE 'BK%' THEN 'Brooklyn'
				WHEN @userName LIKE 'MN%' THEN 'Manhattan'
				WHEN @userName LIKE 'QN%' THEN 'Queens'
				WHEN @userName LIKE 'SI%' THEN 'Staten Island'
				WHEN @userName LIKE 'BX%' THEN 'Bronx'
				ELSE ''
			END

		IF (@borough != '') 
		BEGIN
			SET @pn = @pn + 'Borough:S:' + CAST(LEN(@pvs) AS varchar(10)) + ':' + CAST(LEN(@borough) AS varchar(20)) + ':'
			SET @pvs = @pvs + @borough

			PRINT @userName + ' updated with borough ' + @borough

			EXEC	@updateProfile = [dbo].[aspnet_Profile_SetProperties]
					@ApplicationName = N'/',
					@PropertyNames = @pn,
					@PropertyValuesString = @pvs,
					@PropertyValuesBinary = @pvb,
					@UserName = @userName,
					@CurrentTimeUtc = N'',
					@IsUserAnonymous = false
		END
	END
END


-- Updates user's profiles with address and district
DECLARE @tempProfilesValues table (
	RowID int not null primary key identity(1,1),
	districtName varchar(50) NOT NULL,
	address varchar(100) NULL,
	subDistrict varchar(50) NULL)

DECLARE @adProfilesToProcess int, 
	@adCurrentProfile int

INSERT INTO @tempProfilesValues VALUES ('MN02' ,'353 SPRING STREET','MN')
INSERT INTO @tempProfilesValues VALUES ('MN03' ,'PIER 36 SOUTH & CLINTON','MN')
INSERT INTO @tempProfilesValues VALUES ('MN04' ,'786 12th AVE.','MN')
INSERT INTO @tempProfilesValues VALUES ('MN4A' ,'786 12th AVE.','MN')
INSERT INTO @tempProfilesValues VALUES ('MN05' ,'353 SPRING STREET','MN')
INSERT INTO @tempProfilesValues VALUES ('MN06' ,'606 WEST 30th ST.','MN')
INSERT INTO @tempProfilesValues VALUES ('MN07' ,'786 12th AVE.','MN')
INSERT INTO @tempProfilesValues VALUES ('MN08' ,'423 WEST 215TH STREET','MN')
INSERT INTO @tempProfilesValues VALUES ('MN09' ,'125 E.149th ST. BX.','MN')
INSERT INTO @tempProfilesValues VALUES ('MN10' ,'110 EAST 131st ST.','MN')
INSERT INTO @tempProfilesValues VALUES ('MN11' ,'343 EAST 99th ST','MN')
INSERT INTO @tempProfilesValues VALUES ('MN12' ,'301 WEST 215th ST.','MN')
INSERT INTO @tempProfilesValues VALUES ('MNBRS' ,'640 WEST 26TH STREET','MN')
INSERT INTO @tempProfilesValues VALUES ('BX01' ,' 650 EAST 132nd ST','BX')
INSERT INTO @tempProfilesValues VALUES ('BX02' ,'650 CASANOVA ST.','BX')
INSERT INTO @tempProfilesValues VALUES ('BX3A' ,'1661  WEST FARMS ROAD','BX')
INSERT INTO @tempProfilesValues VALUES ('BX05' ,'1330 CROMWELL AVE','BX')
INSERT INTO @tempProfilesValues VALUES ('BX06' ,'800 EAST 176th ST.','BX')
INSERT INTO @tempProfilesValues VALUES ('BX6A' ,'800  EAST  176TH  ST','BX')
INSERT INTO @tempProfilesValues VALUES ('BX07' ,'423  WEST 215th ST (MAN)','BX')
INSERT INTO @tempProfilesValues VALUES ('BX08' ,'423 WEST 215th ST (MAN)','BX')
INSERT INTO @tempProfilesValues VALUES ('BX09' ,'850  ZEREGA  AVE','BX')
INSERT INTO @tempProfilesValues VALUES ('BX10' ,'850  ZEREGA  AVE','BX')
INSERT INTO @tempProfilesValues VALUES ('BX11' ,'800  ZEREGA  AVE','BX')
INSERT INTO @tempProfilesValues VALUES ('BX12' ,'1635  EAST 233rd  ST.','BX')
INSERT INTO @tempProfilesValues VALUES ('BK01' ,'161 VARICK AVENUE ','BKN')
INSERT INTO @tempProfilesValues VALUES ('BK02' ,'465  HAMILTON  AVE','BKN')
INSERT INTO @tempProfilesValues VALUES ('BK03' ,'525 JOHNSON AVENUE','BKN')
INSERT INTO @tempProfilesValues VALUES ('BK04' ,'161 VARICK AVENUE ','BKN')
INSERT INTO @tempProfilesValues VALUES ('BK05' ,'606   MILFORD  ST','BKN')
INSERT INTO @tempProfilesValues VALUES ('BK08' ,'1755   PACIFIC  ST','BKN')
INSERT INTO @tempProfilesValues VALUES ('BKCIOF' ,'106-01 AVENUE D','BKN')
INSERT INTO @tempProfilesValues VALUES ('BKCTU' ,'459 NORTH HENRY ST.','BKN')
INSERT INTO @tempProfilesValues VALUES ('BK06A' ,'93  VAN  BRUNT  ST.','BKS')
INSERT INTO @tempProfilesValues VALUES ('BK06' ,'127 2ND AVENUE','BKS')
INSERT INTO @tempProfilesValues VALUES ('BK07' ,'5100  1ST  AVENUE','BKS')
INSERT INTO @tempProfilesValues VALUES ('BK09' ,'356 WINTHROP','BKS')
INSERT INTO @tempProfilesValues VALUES ('BK10' ,'5100  1ST  AVENUE','BKS')
INSERT INTO @tempProfilesValues VALUES ('BK11' ,'380  BAY 41ST  STREET','BKS')
INSERT INTO @tempProfilesValues VALUES ('BK12' ,'5602  19TH  AVENUE','BKS')
INSERT INTO @tempProfilesValues VALUES ('BK13' ,'2012  NEPTUNE  AVENUE','BKS')
INSERT INTO @tempProfilesValues VALUES ('BK14' ,'1397 RALPH AVE','BKS')
INSERT INTO @tempProfilesValues VALUES ('BK15' ,'1750  EAST  49TH  STREET','BKS')
INSERT INTO @tempProfilesValues VALUES ('BK15K' ,'2509  KNAPP  ST (8/4) ONLY','BKS')
INSERT INTO @tempProfilesValues VALUES ('BK16' ,'922  GEORGIA  AVENUE','BKS')
INSERT INTO @tempProfilesValues VALUES ('BK17' ,'105-02  AVENUE  D','BKS')
INSERT INTO @tempProfilesValues VALUES ('BK18' ,'105-02  FOSTER  AVENUE','BKS')
INSERT INTO @tempProfilesValues VALUES ('QN01' ,'34-28  21ST  STREET','QNW')
INSERT INTO @tempProfilesValues VALUES ('QN02' ,'52-35  58TH  STREET','QNW')
INSERT INTO @tempProfilesValues VALUES ('QN03' ,'52-35  58TH  STREET','QNW')
INSERT INTO @tempProfilesValues VALUES ('QN04' ,'52-35  58TH  STREET','QNW')
INSERT INTO @tempProfilesValues VALUES ('QN05' ,'48-01  58TH  ROAD','QNW')
INSERT INTO @tempProfilesValues VALUES ('QN05A' ,'58-02  48TH  STREET','QNW')
INSERT INTO @tempProfilesValues VALUES ('QN06' ,'52-35  58th  STREET','QNW')
INSERT INTO @tempProfilesValues VALUES ('QN09' ,'132-05  ATLANTIC  AVENUE','QNW')
INSERT INTO @tempProfilesValues VALUES ('QNCRS' ,'SPEC-CHAS 52-35 58TH ST.','QNW')
INSERT INTO @tempProfilesValues VALUES ('QNCRS' ,'SPEC-CHAS 52-35 58TH ST.','QNW')
INSERT INTO @tempProfilesValues VALUES ('QNCRS' ,'5TH  FLOOR   (BME)','QNW')
INSERT INTO @tempProfilesValues VALUES ('QN07' ,'120-15  31ST  AVENUE','QNE')
INSERT INTO @tempProfilesValues VALUES ('QN07A' ,'30-19 122ND STREET','QNE')
INSERT INTO @tempProfilesValues VALUES ('QN08' ,'130-23  150TH  AVENUE','QNE')
INSERT INTO @tempProfilesValues VALUES ('QN10' ,'130-23  150TH  AVENUE','QNE')
INSERT INTO @tempProfilesValues VALUES ('QN12B' ,'130-23  150TH  AVENUE','QNE')
INSERT INTO @tempProfilesValues VALUES ('QN12M' ,'130-23  150TH  AVENUE','QNE')
INSERT INTO @tempProfilesValues VALUES ('QN13A' ,'72-05  DOUGLASTON PKWY','QNE')
INSERT INTO @tempProfilesValues VALUES ('QN013' ,'153-67  146TH  AVENUE','QNE')
INSERT INTO @tempProfilesValues VALUES ('QN014' ,'51-10 ALMEDA AVE','QNE')
INSERT INTO @tempProfilesValues VALUES ('QNBRS' ,'QE (CRS)  52-07 58TH  ST','QNE')
INSERT INTO @tempProfilesValues VALUES ('SI01' ,'539  JERSEY  STREET','SI')
INSERT INTO @tempProfilesValues VALUES ('SI02' ,'2500  RICHMOND  AVENUE','SI')
INSERT INTO @tempProfilesValues VALUES ('SI03' ,'FT.  OF  MULDOON AVENUE','SI')


SELECT * FROM @tempProfilesValues

SET @adProfilesToProcess = @@ROWCOUNT

SET @adCurrentProfile = 0
WHILE @adCurrentProfile < @adProfilesToProcess
BEGIN

	SET @adCurrentProfile = @adCurrentProfile+1

	DECLARE @adUsersToProcess int,
		@adCurrentUser int,
		@districtName varchar(50),
		@address varchar(100),
		@district varchar(50)

	DECLARE @adTempUsers table (
		RowID int not null primary key identity(1,1),
		UserId uniqueidentifier,
		UserName varchar(255),
		LoweredUserName varchar(255),
		isAnonymous bit)

	SELECT @districtName = districtName,
		@address = address,
		@district = subDistrict
		FROM @tempProfilesValues
		WHERE RowID = @adCurrentProfile

	INSERT INTO @adTempUsers (UserId, UserName, LoweredUserName, isAnonymous)
		SELECT UserId, UserName, LoweredUserName, IsAnonymous FROM aspnet_Users
		WHERE UserName LIKE @districtName + '%'

	SELECT TOP 1 @adUsersToProcess = RowID
			FROM @tempUsers
			ORDER BY RowID DESC

	SET @adCurrentUser = 0

	WHILE @adCurrentUser < @adUsersToProcess
	BEGIN
		SET @adCurrentUser = @adCurrentUser + 1

		DECLARE @adUserName varchar(255), 
			@adUserId uniqueidentifier

		DECLARE @adTempProfiles table (
			PropertyNames varchar(max),
			PropertyValuesString varchar(max),
			PropertyValuesBinary image)

		DECLARE	@adReturnProfile Int, @adUpdateProfile int,
			@adPn varchar(max),
			@adPvs varchar(max),
			@adPvb varbinary(max)

		SELECT TOP 1 @adUserId = UserId, @adUserName = UserName
			FROM @adTempUsers

		if @adUserName IS NOT NULL
			INSERT INTO @adTempProfiles (PropertyNames, PropertyValuesString, PropertyValuesBinary)
				EXEC	@adReturnProfile = [dbo].[aspnet_Profile_GetProperties]
						@ApplicationName = N'/',
						@UserName = @adUserName,
						@CurrentTimeUtc = N''

			SELECT @adPn = PropertyNames, @adPvs = PropertyValuesString, @adPvb = PropertyValuesBinary 
				FROM @adTempProfiles

			SET @adPn = @adPn + 'Address:S:' + CAST(LEN(@adPvs) AS varchar(10)) + ':' + CAST(LEN(@address) AS varchar(20)) + ':'
			SET @adPvs = @adPvs + @address

			SET @adPn = @adPn + 'District:S:' + CAST(LEN(@adPvs) AS varchar(10)) + ':' + CAST(LEN(@district) AS varchar(20)) + ':'
			SET @adPvs = @adPvs + @district

			SELECT TOP 1 @adUserId = UserId, @adUserName = UserName
				FROM @adTempUsers

			EXEC	@adUpdateProfile = [dbo].[aspnet_Profile_SetProperties]
					@ApplicationName = N'/',
					@PropertyNames = @adPn,
					@PropertyValuesString = @adPvs,
					@PropertyValuesBinary = @adPvb,
					@UserName = @adUserName,
					@CurrentTimeUtc = N'',
					@IsUserAnonymous = false

			DELETE FROM @adTempProfiles
			DELETE TOP (1) FROM @adTempUsers
	END
END
