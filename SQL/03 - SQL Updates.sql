-- SUB TYPE
ALTER TABLE [dbo].[Product] DROP COLUMN Sub_Type;
ALTER TABLE [dbo].[Product] ADD is_Sub_Type bit NOT NULL DEFAULT 0;
GO

CREATE TABLE Product_Sub_Type
	(
	Product_Sub_Type_ID int NOT NULL PRIMARY KEY IDENTITY (1, 1),
	Product_ID int NOT NULL,
	Sub_Type varchar(255) NOT NULL
	)
GO

ALTER TABLE [dbo].[Product_Sub_Type] WITH CHECK ADD  CONSTRAINT [FK_Product_ProductSubType] FOREIGN KEY([Product_ID])
REFERENCES [dbo].[Product] ([Product_ID])
GO

ALTER TABLE [dbo].[Purchase_Orders] ADD Product_Sub_Type_ID int NULL;

ALTER TABLE [dbo].[Purchase_Orders] WITH CHECK ADD  CONSTRAINT [FK_PurchaseOrders_ProductSubType] FOREIGN KEY([Product_Sub_Type_ID])
REFERENCES [dbo].[Product_Sub_Type] ([Product_Sub_Type_ID])
GO

ALTER TABLE [dbo].[Product_User]
	ADD Product_Sub_Type_Id int NULL

ALTER TABLE [dbo].[Product_User]  WITH CHECK ADD  CONSTRAINT [FK_Product_Sub_Type] FOREIGN KEY([Product_Sub_Type_Id])
	REFERENCES [dbo].[Product_Sub_Type] ([Product_Sub_Type_ID])
GO


-- PRODUCT VENDOR
CREATE TABLE Product_Vendor
	(
	Product_Vendor_ID int NOT NULL PRIMARY KEY IDENTITY (1, 1),
	Product_ID int NOT NULL,
	Vendor_ID int NOT NULL
	)
GO

ALTER TABLE [dbo].[Product_Vendor] WITH CHECK ADD  CONSTRAINT [FK_Product] FOREIGN KEY([Product_ID])
REFERENCES [dbo].[Product] ([Product_ID])

ALTER TABLE [dbo].[Product_Vendor] WITH CHECK ADD  CONSTRAINT [FK_Vendor] FOREIGN KEY([Vendor_ID])
REFERENCES [dbo].[Vendor] ([Vendor_ID])
GO

-- Drops vendor id from product table
ALTER TABLE [dbo].[Product] 
	DROP CONSTRAINT FK_Product_Vendor;
GO

-- Copy vendor info over to Product_Vendor
INSERT INTO 
	Product_Vendor
SELECT Product_ID, Vendor_ID
	FROM Product
	WHERE Vendor_ID IS NOT NULL
GO

-- Add Vendor_ID as a nullable field to product user
ALTER TABLE [dbo].[Product_User] 
	ADD Vendor_ID int NULL

ALTER TABLE [dbo].[Product_User]  WITH CHECK ADD  CONSTRAINT [FK_Product_Vendor] FOREIGN KEY([Vendor_ID])
	REFERENCES [dbo].[Vendor] ([Vendor_ID])
GO

-- Update GetPurchaseOrderItems for multiple vendors
ALTER PROCEDURE [dbo].[GetPurchaseOrderItems] 
	-- Add the parameters for the stored procedure here
	@daysToCompare int = 90
AS
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	--Determine if its friday , if so double the burn rate(avg_dispensed)
	declare @avg_dispensed_multiplier int
	
	select @avg_dispensed_multiplier = case when DATEPART(dw,getdate()) = 6 then 2 else 1 end

	SELECT UserId, UserName, Address, Borough, PODisplayName, District, Product_ID, Product_Name, 
			(case when (CONVERT(INT, round((case when Order_Amount > ((capacity) - End_On_Hand + (Avg_Dispensed * @avg_dispensed_multiplier)) then ((capacity) - End_On_Hand + (Avg_Dispensed * @avg_dispensed_multiplier)) else Order_Amount end)/10,0)*10)) > Capacity then Capacity else CONVERT(INT, round((case when Order_Amount > ((capacity) - End_On_Hand + (Avg_Dispensed * @avg_dispensed_multiplier)) then ((capacity) - End_On_Hand + (Avg_Dispensed * @avg_dispensed_multiplier)) else Order_Amount end)/10,0)*10) end) Order_Amount ,
			--If the order amount for the product is greater than the capacity minus end on hand plus the average dispensed the make the order amount equial to capacity - end on hand plus average dispensed. Otherwise just use the order amount derived.
			capacity,Measurement_Type, (Avg_Dispensed * @avg_dispensed_multiplier) as Avg_Dispensed, End_On_Hand
	FROM (
		SELECT UserId, UserName, dbo.GetProperty(UserId, 'Address') Address, 
			dbo.GetProperty(UserId, 'Borough') as Borough, dbo.GetProperty(UserId, 'PODisplayName') PODisplayName,
			dbo.GetProperty(UserId, 'District') District, Product_ID, Product_Name, 
			sum(capacity) Capacity, 
			sum(CONVERT(INT, round((CASE WHEN (End_On_Hand - (Avg_Dispensed)) < (.6 * Capacity) THEN (Capacity - End_On_Hand + (Avg_Dispensed)) ELSE 0 END)/10,0)*10)) Order_Amount,
			Measurement_Type, SUM(Avg_Dispensed) Avg_Dispensed,  SUM(End_On_Hand) End_On_Hand 
			FROM (
				SELECT DISTINCT eu.Equipment_Description,u.UserId, u.UserName, p.Product_ID, p.Product_Name, p.is_Drums, p.is_Water,
				CONVERT(INT, ROUND((ISNULL((eu.Capacity * .9), (pu.Capacity*.9))),0)) as capacity,
				p.Measurement_Type, 
				(SELECT TOP (@daysToCompare) AVG(ffd.Tank_Dispensed) --Get average dispensed amount for the last xx+3 days for this user/product
					FROM Fuel_Form ff
					INNER JOIN Fuel_Form_Details ffd on ff.Fuel_Form_ID = ffd.Fuel_Form_ID
					WHERE Submission_Date >= GETDATE() - (@daysToCompare + 3)  AND
					ff.UserId = pu.UserId AND
					ffd.Product_ID = p.Product_ID and
					ffd.tank_dispensed is not null) as Avg_Dispensed,
				(SELECT ffd.On_Hand -- Get the on hand quantity from the last fuel form for the product/location/tank
					FROM Fuel_Form ff
					INNER JOIN Fuel_Form_Details ffd on ff.Fuel_Form_ID = ffd.Fuel_Form_ID
					WHERE ffd.Fuel_Form_ID = (select MAX(ff1.Fuel_Form_ID) from Fuel_Form ff1 where ff1.UserId = pu.UserId) and
					ffd.Equipment_User_ID = eu.Equipment_User_ID ) as End_On_Hand
				FROM Product_User pu 
					INNER JOIN aspnet_Users u ON u.UserId = pu.UserId
					INNER JOIN Product p ON p.Product_ID = pu.Product_ID
					INNER JOIN Equipment_User eu on pu.UserId = eu.UserId and pu.Product_ID = eu.Product_ID
				WHERE p.is_Active = 1 and  eu.is_Active = 1 and p.is_Water = 0 and eu.Equipment_ID = 9 --make sure that the product is active, the tank is not down, ignore dispensors in the result set.
					  and eu.Equipment_User_ID not in(select distinct Equipment_User_ID from Fuel_Form_Equipment_Failure ffe  --Only calculate for equipment 
														where ffe.Failure_Date is not null and ffe.Fix_Date is null and       --for equipment that is not down.
														Fuel_Form_ID = (select MAX(Fuel_Form_id) 
																		from Fuel_Form_details ffe1 
																		where ffe.Equipment_User_ID = ffe1.Equipment_User_ID)) and
																		(convert(nvarchar(50),eu.USERID)+ convert(nvarchar(50),eu.Product_ID)) not in   --Only calculate where a product 
																				(SELECT convert(nvarchar(50),USERID)+ convert(nvarchar(50),product_id)  --was not already put in a purchase order within
																					from purchase_orders												--the last 30 hours T-F or 70 hours on monday
																					where Order_Date between DATEADD(hour, (case DATEPART(dw,GETDATE()) when 1 then -78 else -30 end), getdate()) and GETDATE()) 
																																) as P
			WHERE p.is_Water = 0 --ignore the tanks associated with water
			GROUP BY UserId, UserName, Product_ID, Product_Name, Measurement_Type
			HAVING (SUM(End_On_Hand) - (SUM(Avg_Dispensed)) < (.6 * sum(CONVERT(INT, ROUND((capacity * (CASE is_Drums WHEN 1 THEN 1 ELSE .9 END)),0)))) )
		) v
	WHERE Order_Amount > 0
	ORDER BY UserName
END

GO

  insert into dbo.Product_Sub_Type
  select 25, 'B5';
    insert into dbo.Product_Sub_Type
  select 25, 'B20';
    insert into dbo.Product_Sub_Type
  select 25, 'B10';
    insert into dbo.Product_Sub_Type
  select 28, 'B5';

  GO

 UPDATE Product 
	SET is_sub_type = 1
	WHERE Product_ID in (25,28)

 GO

 -- Add Vendor Email_Address
ALTER TABLE [dbo].[Vendor]
	ADD Email_Address VARCHAR(255) NULL

GO

-- Update Purchase Order Column to be INT
ALTER TABLE [dbo].[Purchase_Orders] 
	ALTER COLUMN Order_Amount INT;

--ALTER TABLE [dbo].[Purchase_Orders] 
--	DROP COLUMN Not_Ordered_Reason; 

GO
