USE [DSNY_MESSAGING_PROD]
GO
/****** Object:  Table [dbo].[Radio]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio](
	[Radio_id] [int] IDENTITY(1,1) NOT NULL,
	[Radio_Serial_Number] [varchar](50) NULL,
	[DSNY_Radio_id] [varchar](50) NULL,
	[Trunk_id] [varchar](50) NULL,
	[Issued_To_Userid] [uniqueidentifier] NULL,
	[Radio_Equipment_Type_id] [int] NULL,
	[Radio_Make_id] [int] NULL,
	[Radio_Model_id] [int] NULL,
	[Assigned_To_Vehicle] [varchar](50) NULL,
	[Inventory_Date] [datetime] NULL,
	[Service_Date] [datetime] NULL,
	[is_Spare] [bit] NULL,
	[is_Active] [bit] NULL,
	[Radio_Status_id] [int] NULL,
	[Last_Dist_Inv_Date] [datetime] NULL,
	[Last_Dist_Inv_Status_id] [int] NULL,
	[is_Down] [bit] NULL,
	[is_Disposed] [bit] NULL,
	[Is_Inventory_Requested] [bit] NULL,
 CONSTRAINT [PK_Radio] PRIMARY KEY CLUSTERED 
(
	[Radio_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Radio_Accessory]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio_Accessory](
	[Radio_Accessory_id] [int] IDENTITY(1,1) NOT NULL,
	[Part_Number] [varchar](50) NULL,
	[Userid] [uniqueidentifier] NULL,
	[Radio_Equipment_Type_id] [int] NULL,
	[Radio_Make_id] [int] NULL,
	[Radio_Maintenance_id] [int] NULL,
	[Description] [varchar](200) NULL,
	[Current_Qty] [int] NULL,
	[Last_Dist_Inv_Date] [datetime] NULL,
	[Last_Dist_Inv_Qty] [int] NULL,
	[Is_Inventory_Requested] [bit] NULL,
	[Is_Spare] [bit] NULL,
 CONSTRAINT [PK_Radio_Accessory] PRIMARY KEY CLUSTERED 
(
	[Radio_Accessory_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Radio_Accessory_Ledger]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio_Accessory_Ledger](
	[Radio_Accessory_Ledger_id] [int] IDENTITY(1,1) NOT NULL,
	[Radio_Accessory_id] [int] NULL,
	[is_recieve] [bit] NULL,
	[Radio_id] [int] NULL,
	[Qty] [int] NULL,
	[Transfer_Userid] [uniqueidentifier] NULL,
	[Comment] [varchar](500) NULL,
 CONSTRAINT [PK_Radio_Accessory_Ledger] PRIMARY KEY CLUSTERED 
(
	[Radio_Accessory_Ledger_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Radio_Equipment_Status]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio_Equipment_Status](
	[Radio_Equipment_Status_id] [int] IDENTITY(1,1) NOT NULL,
	[Equipment_Status_Name] [varchar](50) NULL,
	[is_Workflow] [bit] NULL,
	[is_Inventory] [bit] NULL,
 CONSTRAINT [PK_Radio_Equipment_Status] PRIMARY KEY CLUSTERED 
(
	[Radio_Equipment_Status_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Radio_Equipment_Type]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio_Equipment_Type](
	[Radio_Equipment_Type_id] [int] IDENTITY(1,1) NOT NULL,
	[Equipment_Type_Name] [varchar](50) NOT NULL,
	[is_District_Inventory] [bit] NULL,
	[is_Active] [bit] NULL,
 CONSTRAINT [PK_Radio_Equipment_Type] PRIMARY KEY CLUSTERED 
(
	[Radio_Equipment_Type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Radio_History]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio_History](
	[Radio_History_id] [int] IDENTITY(1,1) NOT NULL,
	[Radio_id] [int] NOT NULL,
	[Radio_Serial_Number] [varchar](50) NULL,
	[DSNY_Radio_id] [varchar](50) NULL,
	[Trunk_id] [varchar](50) NULL,
	[Issued_To_Userid] [uniqueidentifier] NULL,
	[Radio_Equipment_Type_id] [int] NULL,
	[Radio_Make_id] [int] NULL,
	[Radio_Model_id] [int] NULL,
	[Assigned_To_Vehicle] [varchar](50) NULL,
	[Inventory_Date] [datetime] NULL,
	[Service_Date] [datetime] NULL,
	[is_Spare] [bit] NULL,
	[is_Active] [bit] NULL,
	[Radio_Status_id] [int] NULL,
	[Last_Dist_Inv_Date] [datetime] NULL,
	[Last_Dist_Inv_Status_id] [int] NULL,
	[Comment] [varchar](100) NULL,
 CONSTRAINT [PK_Radio_History] PRIMARY KEY CLUSTERED 
(
	[Radio_History_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Radio_Make]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio_Make](
	[Radio_Make_id] [int] IDENTITY(1,1) NOT NULL,
	[Make_Name] [varchar](50) NULL,
	[is_Active] [bit] NULL,
 CONSTRAINT [PK_Radio_Make] PRIMARY KEY CLUSTERED 
(
	[Radio_Make_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Radio_Model]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio_Model](
	[Radio_Model_id] [int] IDENTITY(1,1) NOT NULL,
	[Radio_Make_id] [int] NULL,
	[Model_Name] [varchar](50) NULL,
	[is_Active] [bit] NULL,
 CONSTRAINT [PK_Radio_Model] PRIMARY KEY CLUSTERED 
(
	[Radio_Model_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Radio_Repair]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Radio_Repair](
	[Radio_Repair_id] [int] IDENTITY(1,1) NOT NULL,
	[Radio_Id] [int] NULL,
	[Radio_Repair_Problem_Type_id] [int] NULL,
	[Repair_Date] [datetime] NULL,
 CONSTRAINT [PK_Radio_Repair] PRIMARY KEY CLUSTERED 
(
	[Radio_Repair_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Radio_Repair_Problem_Type]    Script Date: 8/28/2018 8:51:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Radio_Repair_Problem_Type](
	[Radio_Repair_Problem_id] [int] IDENTITY(1,1) NOT NULL,
	[Repair_Problem_Name] [varchar](50) NULL,
	[is_Active] [bit] NULL,
 CONSTRAINT [PK_Radio_Repair_Problem_Type] PRIMARY KEY CLUSTERED 
(
	[Radio_Repair_Problem_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Radio]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Make] FOREIGN KEY([Radio_Make_id])
REFERENCES [dbo].[Radio_Make] ([Radio_Make_id])
GO
ALTER TABLE [dbo].[Radio] CHECK CONSTRAINT [FK_Radio_Make]
GO
ALTER TABLE [dbo].[Radio]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Model] FOREIGN KEY([Radio_Model_id])
REFERENCES [dbo].[Radio_Model] ([Radio_Model_id])
GO
ALTER TABLE [dbo].[Radio] CHECK CONSTRAINT [FK_Radio_Model]
GO
ALTER TABLE [dbo].[Radio]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Radio_Equipment_Status] FOREIGN KEY([Radio_Status_id])
REFERENCES [dbo].[Radio_Equipment_Status] ([Radio_Equipment_Status_id])
GO
ALTER TABLE [dbo].[Radio] CHECK CONSTRAINT [FK_Radio_Radio_Equipment_Status]
GO
ALTER TABLE [dbo].[Radio]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Radio_Equipment_Status1] FOREIGN KEY([Last_Dist_Inv_Status_id])
REFERENCES [dbo].[Radio_Equipment_Status] ([Radio_Equipment_Status_id])
GO
ALTER TABLE [dbo].[Radio] CHECK CONSTRAINT [FK_Radio_Radio_Equipment_Status1]
GO
ALTER TABLE [dbo].[Radio_Accessory]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Accessory_aspnet_Users] FOREIGN KEY([Userid])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[Radio_Accessory] CHECK CONSTRAINT [FK_Radio_Accessory_aspnet_Users]
GO
ALTER TABLE [dbo].[Radio_Accessory]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Accessory_Radio] FOREIGN KEY([Userid])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[Radio_Accessory] CHECK CONSTRAINT [FK_Radio_Accessory_Radio]
GO
ALTER TABLE [dbo].[Radio_Accessory]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Accessory_Radio_Equipment_Type] FOREIGN KEY([Radio_Equipment_Type_id])
REFERENCES [dbo].[Radio_Equipment_Type] ([Radio_Equipment_Type_id])
GO
ALTER TABLE [dbo].[Radio_Accessory] CHECK CONSTRAINT [FK_Radio_Accessory_Radio_Equipment_Type]
GO
ALTER TABLE [dbo].[Radio_Accessory]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Accessory_Radio_Make] FOREIGN KEY([Radio_Make_id])
REFERENCES [dbo].[Radio_Make] ([Radio_Make_id])
GO
ALTER TABLE [dbo].[Radio_Accessory] CHECK CONSTRAINT [FK_Radio_Accessory_Radio_Make]
GO
ALTER TABLE [dbo].[Radio_Accessory_Ledger]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Accessory_Ledger_Radio_Accessory] FOREIGN KEY([Radio_Accessory_id])
REFERENCES [dbo].[Radio_Accessory] ([Radio_Accessory_id])
GO
ALTER TABLE [dbo].[Radio_Accessory_Ledger] CHECK CONSTRAINT [FK_Radio_Accessory_Ledger_Radio_Accessory]
GO
ALTER TABLE [dbo].[Radio_History]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Radio_History] FOREIGN KEY([Radio_id])
REFERENCES [dbo].[Radio] ([Radio_id])
GO
ALTER TABLE [dbo].[Radio_History] CHECK CONSTRAINT [FK_Radio_Radio_History]
GO
ALTER TABLE [dbo].[Radio_Model]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Model_Radio_Make] FOREIGN KEY([Radio_Make_id])
REFERENCES [dbo].[Radio_Make] ([Radio_Make_id])
GO
ALTER TABLE [dbo].[Radio_Model] CHECK CONSTRAINT [FK_Radio_Model_Radio_Make]
GO
ALTER TABLE [dbo].[Radio_Repair]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Repair_Radio] FOREIGN KEY([Radio_Id])
REFERENCES [dbo].[Radio] ([Radio_id])
GO
ALTER TABLE [dbo].[Radio_Repair] CHECK CONSTRAINT [FK_Radio_Repair_Radio]
GO
ALTER TABLE [dbo].[Radio_Repair]  WITH CHECK ADD  CONSTRAINT [FK_Radio_Repair_Radio_Repair_Problem_Type] FOREIGN KEY([Radio_Repair_Problem_Type_id])
REFERENCES [dbo].[Radio_Repair_Problem_Type] ([Radio_Repair_Problem_id])
GO
ALTER TABLE [dbo].[Radio_Repair] CHECK CONSTRAINT [FK_Radio_Repair_Radio_Repair_Problem_Type]
GO


truncate table Radio_History

alter table Radio_History
add ChangeDate datetime not null

alter table Radio
add Inventory_Request_Date datetime  null

alter table Radio_Accessory
add Inventory_Request_Date datetime null


-- Radio Role Setup
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'Radio Admin', 'radio admin', null)
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'Radio District', 'radio district', null)

insert into aspnet_UsersInRoles
 select UserId,(select roleid from aspnet_Roles where LoweredRoleName = 'radio district' )from aspnet_users where right(UserName,1) = 'G' and LEFT(UserName,1) in ('M','S','Q','B') and LEN(UserName) = 5

 -- Add trigger for tracking history and comments
CREATE TRIGGER TRG_InsertRadio
ON dbo.Radio
AFTER INSERT AS
BEGIN
INSERT INTO Radio_History
SELECT [Radio_id]
      ,[Radio_Serial_Number]
      ,[DSNY_Radio_id]
      ,[Trunk_id]
      ,[Issued_To_Userid]
      ,[Radio_Equipment_Type_id]
      ,[Radio_Make_id]
      ,[Radio_Model_id]
      ,[Assigned_To_Vehicle]
      ,[Inventory_Date]
      ,[Service_Date]
      ,[is_Spare]
      ,[is_Active]
      ,[Radio_Status_id]
      ,[Last_Dist_Inv_Date]
      ,[Last_Dist_Inv_Status_id]
	  , NULL -- Comments
      , GetDate() -- Change Date
  FROM INSERTED
END
GO
ALTER TABLE RADIO
ADD Last_Comments VARCHAR(MAX) NULL
GO