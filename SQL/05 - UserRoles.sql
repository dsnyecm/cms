
-- STREET TREATMENT COMMODITY ROLES
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'STC Borough', 'stc borough', null)
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'STC Site', 'stc site', null)
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'STC HQ', 'stc hq', null)

-- GARAGE SUPPLIES ROLES
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'GS Warehouse', 'gs warehouse', null)
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'GS Borough', 'gs borough', null)
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'GS District', 'gs district', null)
INSERT INTO aspnet_Roles VALUES('33D426E7-CB10-497B-AFA5-2EFA0F8547ED', NEWID(), 'GS HQ', 'gs hq', null)

-- ADD USERS TO ROLES
insert into aspnet_Usersinroles select UserId, RoleId from aspnet_Users u, aspnet_Roles r where u.UserName = 'bk01' and r.RoleName = 'STC Borough'
insert into aspnet_Usersinroles select UserId, RoleId from aspnet_Users u, aspnet_Roles r where u.UserName = 'advocate' and r.RoleName = 'STC Site'
insert into aspnet_Usersinroles select UserId, RoleId from aspnet_Users u, aspnet_Roles r where u.UserName = 'banderson' and r.RoleName = 'STC HQ'

insert into aspnet_Usersinroles select UserId, RoleId from aspnet_Users u, aspnet_Roles r where u.UserName = 'bccwhse' and r.RoleName like 'STC%'
