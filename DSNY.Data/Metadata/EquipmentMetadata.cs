﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace DSNY.Data
{
    /// <summary>
    /// Metadata class which defines how the Entities Framework displays/validates the Equipment table data
    /// </summary>
    [MetadataType(typeof(EquipmentMetaData))]
    public partial class Equipment
    {
        // Validation rules for the Product class
        [Bind(Exclude = "Equipment_ID")]
        public class EquipmentMetaData
        {
            [ScaffoldColumn(false)]
            public object Equipment_ID { get; set; }

            [Required(ErrorMessage = "An equipment name is required.")]
            [DisplayName("Equipment Name")]
            public object Equipment_Name { get; set; }

            [DisplayName("Active")]
            public object is_Active { get; set; }
        }
    }
}