﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace DSNY.Data
{
    /// <summary>
    /// Metadata class which defines how the Entities Framework displays/validates the Message table data
    /// </summary>
    [MetadataType(typeof(MessageMetadata))]
    public partial class Message
    {
        // Validation rules for the Product class
        [Bind(Exclude = "Message_Id")]
        public class MessageMetadata
        {
            [ScaffoldColumn(false)]
            public object Message_Id { get; set; }

            [DisplayName("Date")]
            public object Message_Date { get; set; }

            [Required(ErrorMessage = "*")]
            [DisplayName("Dept Msg #")]
            public object Dept_Msg_No { get; set; }

            [Required(ErrorMessage = "*")]
            [DisplayName("Code")]
            public object Message_Code { get; set; }

            [Required(ErrorMessage = "*")]
            [DisplayName("Subject")]
            public object Message_Subject { get; set; }

            [Required(ErrorMessage = "*")]
            [DisplayName("Message")]
            public object Message_Text { get; set; }

            [ScaffoldColumn(false)]
            [DisplayName("Sent By")]
            public object Sent_By_UserId { get; set; }

            [ScaffoldColumn(false)]
            public object is_Fuel_Form { get; set; }
        }
    }
}