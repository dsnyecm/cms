﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using DSNY.Data;

namespace DSNY.Data
{
    /// <summary>
    /// Metadata class which defines how the Entities Framework displays/validates the Roles table data
    /// </summary>
    [MetadataType(typeof(aspnet_RolesMetaData))]
    public partial class aspnet_Roles
    {
       // Validation rules for the Album class
        [Bind(Exclude = "ApplicationId, RoleId, LoweredRoleName")]
        public class aspnet_RolesMetaData
        {
            [ScaffoldColumn(false)]
            public object ApplicationId { get; set; }

            [ScaffoldColumn(false)]
            public object RoleId { get; set; }

            [ScaffoldColumn(false)]
            public object LoweredRoleName { get; set; }

            [DisplayName("Role")]
            [Required(ErrorMessage = "Role name is required")]
            [StringLength(50)]
            public object RoleName { get; set; }

            [StringLength(160)]
            public object Description { get; set; }
        }
    }
}
