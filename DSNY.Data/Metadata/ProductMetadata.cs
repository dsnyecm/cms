﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace DSNY.Data
{
    /// <summary>
    /// Metadata class which defines how the Entities Framework displays/validates the Product table data
    /// </summary>
    [MetadataType(typeof(ProductMetaData))]
    public partial class Product
    {
        // Validation rules for the Product class
        [Bind(Exclude = "Product_ID")]
        public class ProductMetaData
        {
            [ScaffoldColumn(false)]
            public object Product_ID { get; set; }

            [Required(ErrorMessage = "A product name is required.")]
            [DisplayName("Product Name")]
            public object Product_Name { get; set; }

            [DisplayName("Measurement Type")]
            public object Measurement_Type { get; set; }

            [DisplayName("Default Capacity")]
            public object Default_Capacity { get; set; }

            [DisplayName("Sort Number")]
            public object Order_Num { get; set; }

            [DisplayName("Active")]
            public object is_Active { get; set; }
            
            [DisplayName("Is Drum")]
            public object is_Drums { get; set; }

            [DisplayName("Is Reading Required")]
            public object is_Reading_Required { get; set; }

            [DisplayName("Is Water")]
            public object is_Water { get; set; }

            [DisplayName("Is Sub Type")]
            public object is_Sub_Type { get; set; }

            [DisplayName("Sub Types")]
            public List<Product_Sub_Type> Product_Sub_Type { get; set; }

            [DisplayName("Order Delivery Type")]
            public object Order_Delivery_Type { get; set; }

            [DisplayName("Internal PO Distribution")]
            public object Order_Delivery_Distribution { get; set; }
        }
    }
}